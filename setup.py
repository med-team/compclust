#!/usr/bin/env python
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

"""Setup script for compClust

The one interesting detail is the command line takes a parameter --notest
to turn off the unit tests.
"""
from getopt import getopt
import glob
import os
import re
import string
import shutil
import sys

try:
  from setuptools import setup, Extension
except ImportError:
  from distutils.core import setup, Extension


from compClust.config import Config
from setupext import findNonTestScripts

config = Config(installing=True)

class DependenciesUnmet(RuntimeError): pass

# validate dependencies
critical_dependencies_met = True
try:
  pyrex = True
  from Pyrex.Distutils import build_ext
except ImportError, e:
  print >>sys.stderr, "Please install pyrex <http://www.cosc.canterbury.ac.nz/~greg/python/Pyrex/>" 
  print >>sys.stderr, "Linear Assignment scoring will not work without it"
  print >>sys.stderr, ""
  critical_dependencies_met = False
  pyrex = False

def vc_filter(x):
  """Filter for skipping version control subdirectories
  """
  if x.find('CVS') ==-1 and x.find('.svn') == -1:
    return 1
  else:
    return 0
  
def glob_novc(pattern):
  """Return list of filenames that match pattern excluding CVS and .svn
  """
  return filter(vc_filter, glob.glob(pattern))



class AutoSetup(object):
  """Attempt to autodetect as many environment settings as possible.
  """
  def __init__(self):
    self.log = []
    # values to pass to setup
    self.args = {}
    # paths to add for extensions
    self.include_dirs = []
    # should we run tests?
    self.run_tests= True
    # should we symlink the binaries back in?
    self.__inplace = False
    # should we build the C DA module
    self.want_da = False

    self.detect_mpl()
    self.detect_quixote()
    self.detect_tkinter()

    self.initialize_setup_args()

  def append_include_dir(self, include_dir):
    """Add to list of include dirs for our extensions"""
    self.include_dirs.append(include_dir)

  def __set_inplace(self, value):
    if value:
      del self.args['packages']
      del self.args['scripts']
      del self.args['data_files']
    else:
      self.initalize_setup_args()

  def __get_inplace(self):
    return self.__inplace
  inplace = property(__get_inplace, __set_inplace)

  def initialize_setup_args(self):
    """Initalize all the parameters we need for setuptools/distutils"""
    self.args['name']="compClust"
    self.args['version']="1.3"
    self.args['description']="Comp Clust is a suite of tools for clustering  " \
      "and data mining. "
    self.args['maintainer']="Diane Trout"
    self.args['maintainer_email']="diane@caltech.edu"
    self.args['url']="http://woldlab.caltech.edu/compClust/"
    self.args['cmdclass'] = {'build_ext': build_ext}

    # used to remap source locations
    # FIXME: this is an ugly hack
    # FIXME: since io, pstat, and stats live in the root directory and
    # FIXME: I didn't want to clutter my already heavily cluttered root 
    # FIXME: directory I moved the root "" package to "extra"
    # FIXME: so if new packages get in other places other than compClust 
    # FIXME: they'd be added to this.
    self.args['package_dir'] ={"": "extra", "compClust": "compClust"}

    # package data (templates?)
    self.args['package_data']={}
    # documentation files to include
    self.args['data_files']=[]
    # Individual modules to distribute (as opposed to packages)
    self.args['py_modules']=[]
    # compiled modules 
    self.args['ext_modules'] = []
   
    # find all of the non test scripts
    self.args['scripts']= findNonTestScripts('compClust')
    # also include the web launch scripts          
    self.args['scripts'].append(os.path.abspath('compclustweb.py'))
    # set the default package list
    self.default_packages()
    # add the stats packages if needed
    self.install_stats()

  def detect_gnumake(self, make_command = "make -v"):
    """
    test to see if we have gnumake  
    this should be extended to allow the user to specify which make to use
    """
    if self.make_command is not None:
      return self.make_command

    make_version_stream = os.popen(make_command)
    make_version = make_version_stream.readlines()
    make_exit_code = make_version_stream.close()
    if make_exit_code is None and \
       len(make_version) > 0 and \
      re.match("GNU Make", make_version[0]) is not None:
      self.make_command = make_command
    else:
      print >>sys.stderr, "GNU make not found can't build c-code"
      self.make_command = None
    return self.make_command
 
  def detect_mpl(self):
    try:
      import matplotlib
      self.log.append("Detected matplotlib")
      self.matplotlib = True
    except ImportError, e:
      self.matplotlib = False
      self.log.append("Matplotlib not detected... please install from <http://matplotlib.sf.net>")

  def detect_quixote(self):
    try:
      import quixote
      version = float(quixote.__version__)
      if version < 2.0:
        raise ImportError("Need quixote 2.0")
      self.log.append("Quixote detected")
      self.quixote = True
    except ImportError, e:
      self.quixote = False
      self.log.append("Install quxiote 2.x from <http://www.mems-exchange.org/software/quixote/> to use compclustweb")

  def detect_tkinter(self):
    try: 
      import Tkinter
      self.log.append("Tkinter detected")
      self.tkinter = True
    except ImportError, e:
      self.tkinter = False
      self.log.append("TKinter not found")

  def install_stats(self):
    """Add the status packages of we don't have them
    """
    # handle the stats/pstat/io stat code
    if not self.args.has_key('modules'):
      self.args['py_modules'] = []
    try:
      import io
    except ImportError, e:
      self.args['py_modules'].append('io')
    try:
      import pstat
    except ImportError, e:
      self.args['py_modules'].append('pstat')
    try:
      import stats
    except ImportError, e:
      self.args['py_modules'].append('stats')

  def install_compclusttk_tutorial(self):
    """Add CompClustTk Tutorial to our list of packages"""
    pass
    #CompClustTk Tutorial
    # FIXME: need to properly figure out where the tutorial should go
    #tutPath = os.path.join(docPath, 'compclusttk_tutorial')
    #TUTORIAL_TUPLE=(tutPath,
    #                glob_novc('compClust/gui/Docs/compclusttk_tutorial/*'))
    #
    #self.args['data_files'].append(TUTORIAL_TUPLE)
    

  def install_example_data(self):
    """Install our sample data
    """
    docPath = config.data_install_path()
    #CompClustTk Example Data
    examplePath = os.path.join(docPath, 'Examples', 'ChoCellCycling')
    EXAMPLE_DATA_TUPLE=(examplePath,
                        glob_novc('compClust/gui/Examples/ChoCellCycling/*'))
    
    #CompClustTk Data
    if self.args.has_key('data_files'):
      self.args['data_files'].append(EXAMPLE_DATA_TUPLE)

  def install_web_templates(self):
    # web interface templates
    template_files = glob_novc('compClust/gui/*.xml')
    template_files.extend(glob_novc('compClust/gui/*.css'))
    template_files.extend(glob_novc('compClust/gui/*.js'))
    if self.args.has_key('data_files'):
      self.args['data_files'].append((config.template_dir, template_files))

  def default_packages(self): 
    """set default packages to install
    """
    self.args['packages']=["compClust",
                   "compClust.gui",
                   "compClust.gui.tests",
                   "compClust.iplot",
                   "compClust.iplot.tests",
                   "compClust.iplot.mappers",
                   "compClust.iplot.mappers.tests",
                   "compClust.mlx",
                   "compClust.mlx.tests",
                   "compClust.mlx.DA",
                   "compClust.mlx.DA.tests",
                   "compClust.mlx.datasets",
                   "compClust.mlx.datasets.tests",
                   "compClust.mlx.labelings",
                   "compClust.mlx.labelings.tests",
                   "compClust.mlx.models",
                   "compClust.mlx.models.tests",
                   "compClust.mlx.views",
                   "compClust.mlx.views.tests",
                   "compClust.mlx.wrapper",
                   "compClust.mlx.wrapper.tests",
                   "compClust.score",
                   "compClust.score.tests",
                   "compClust.util",
                   "compClust.util.tests",
                   # to beta to deal with
                   # "compClust.visualize",
                   # "compClust.visualize.dataViewer",
                   # "bioinformatics",
                   # "bioinformatics.schema",
                   # "bioinformatics.schema.datasets",
                   # "bioinformatics.schema.views",
                   # "bioinformatics.util",
                   # "bioinformatics.visualize",          
              ]
    
  def build_jpl_c(self):
    docPath = config.data_install_path()

    # build the C Code
    make_command = self.detect_gnumake()
    
    if os.path.isdir("src") and make_command is not None:
      log_stream_name = "compclust_src_build.log"
      print >>sys.stderr, "Attempting to build C code,",
      print >>sys.stderr, "look at %s for log" % (log_stream_name)
      #status = os.spawnl(os.P_WAIT, "make", "make", "-C", "src")
      build_stream = os.popen("make -C src")
      log_stream = open(log_stream_name, 'w')
      for l in build_stream:
        log_stream.write(l)
      log_stream.close()
      build_result = build_stream.close()
      if build_result is None:
      #  # we managed to build the C code
        if os.path.exists('src/mls/diagem/diagem'):
          DATA_FILES.append((os.path.join(docPath, 'bin'), 
                             ['src/mls/diagem/diagem']))
          #SCRIPTS.append('src/mls/diagem/diagem')
        if os.path.exists('src/mls/kmeans/kmeans'):
          DATA_FILES.append((os.path.join(docPath, 'bin'), 
                             ['src/mls/kmeans/kmeans']))
        if os.path.exists('src/mls/kmedians/kmedians'):
          DATA_FILES.append((os.path.join(docPath, 'bin'), 
                             ['src/mls/kmedians/kmedians']))
          #SCRIPTS.append('src/mls/kmeans/kmeans')
      else:
        print "----------"
        print "FAILED building C Code", build_result
        print "----------"
    else:
      # alas no source code, try the prebuilt binaries
      algorithm_platform = os.path.join(os.getcwd(), 'extra','clustering-alg')
      diagem_command = 'diagem'
      kmeans_command = 'kmeans'
      kmedians_command = 'kmedians'
      if sys.platform == 'linux2':
        # perhaps we should start testing which particular platform of linux?
        algorithm_platform = os.path.join(algorithm_platform, 'linux-x86')
      elif sys.platform == 'darwin':
        algorithm_platform = os.path.join(algorithm_platform, 'osx-ppc')
      elif sys.platform == 'win32':
        algorithm_platform = os.path.join(algorithm_platform, 'win32')    
        diagem_command = 'diagem.exe'
        kmeans_command = 'kmeans.exe'
        kmedians_command = 'kmedians.exe'
      else:
        raise DependenciesUnmet("No clustering algorithms available... "\
                               "this is probably a bad thing.")
      DATA_FILES.append((os.path.join(docPath, 'bin'), 
                         [os.path.join(algorithm_platform, diagem_command)]))
      DATA_FILES.append((os.path.join(docPath, 'bin'), 
                         [os.path.join(algorithm_platform, kmeans_command)]))
      DATA_FILES.append((os.path.join(docPath, 'bin'), 
                         [os.path.join(algorithm_platform, kmedians_command)]))

  def install_graphmatching(self):
    """
    Build our graphmatching pyrex extension
    """
    if len(self.include_dirs) == 0:
      import numpy
      numpy_core_path = numpy.core.__path__[0]
      # this is really specific to my machine...
      self.include_dirs.append(os.path.join(numpy_core_path,"include"))
    # Pyrex extensions
    if pyrex:
      graphmatching_dir = os.path.join(os.getcwd(), "compClust/mlx/graphmatching")
      graphmatching_modules = [ os.path.join(graphmatching_dir, x) for x in
                                ["graphmatching.pyx", "wmatch.c", "glib.c"]]
      graphmatching_package = "compClust.mlx.graphmatching"
      print "include_dirs", self.include_dirs
      self.args['ext_modules'].append(Extension(graphmatching_package, 
                                      graphmatching_modules,
                                      include_dirs=self.include_dirs))
 
  def install_da(self):
    if self.want_da:
      dapath = os.path.join(os.getcwd(), "compClust/mlx/DA/DAmodule.c")
      self.args['ext_modules'].append(Extension("compClust.mlx.DA.cDA", 
                                                [dapath],
                                                include_dirs=self.include_dirs))

      
  def test_runner(self):
    """Try to run all of our tests
    """
    if not self.run_tests:
      return
    # specify the exact location of the cho data
    # the testIPlot script looks for this environment variable
    # for grabbing the data location
    os.environ['CHO_DATA'] = os.path.join(os.getcwd(), 'compClust', 'gui', 'Examples', 'ChoCellCycling')

    testscript = 'testEverything.py'
    if not self.inplace:
      curdir = os.getcwd()
      for builddir in glob.glob('build/lib.*'):
        # the stuff after the last - should be the python version number
        version = builddir.split("-")[-1]
        scriptdir = os.path.join(os.getcwd(), builddir, '..', 'scripts-%s'%(version))
        os.environ['PYTHONPATH'] = builddir
        os.environ['COMPCLUST_DIR'] = config.base_dir
        #os.system(string.join([sys.executable, testscript], " "))
        build_testscript = os.path.join(builddir, testscript)
        shutil.copy(testscript, build_testscript)
        os.chdir(builddir)
        os.system(string.join([sys.executable, testscript], " "))
        os.chdir(curdir)
        os.unlink(build_testscript)
    else:
      os.system(string.join([sys.executable, testscript], " "))

  def build_inplace(self):
    """Link shared libraries to source tree
    """
    if not self.inplace:
      return

    builddirs = glob.glob('build/lib.*')
    if len(builddirs) > 1:
      print "too many build dirs, can't run in place"
      return
  
    basedir = os.getcwd()
    builddir = os.path.join(basedir, builddirs[0])
    os.chdir(builddir)

    for dirpath, dirnames, filenames in os.walk('.'):
      for filename in filenames:
        path, ext = os.path.splitext(filename)
        if ext == config.get_python_shared_library_extension():
          src_file = os.path.normpath(os.path.join(builddir, dirpath, filename))
          dest_file = os.path.normpath(os.path.join(basedir, dirpath, filename))
          if not os.path.islink(dest_file):
            os.symlink(src_file, dest_file)
    os.chdir(basedir)

  def setup(self):
    """actually call the setup program
    """
    self.install_example_data()
    self.install_graphmatching()
    self.install_da()

    #from pprint import pprint
    #pprint(self.args)
    setup(**self.args)
    self.build_inplace()
    #self.test_runner()

def main():
  autosetup = AutoSetup()

  # Allow user to override testingg:
  for i in xrange(len(sys.argv)-1,-1,-1):
    if re.match("--?enable-da", sys.argv[i]):
      del sys.argv[i]
      autosetup.want_da = True
    elif re.match("--?notest",sys.argv[i]):
      autosetup.run_tests = False
      del sys.argv[i]
    elif re.match("--?inplace", sys.argv[i]):
      del sys.argv[i]
      autosetup.inplace = True
    elif re.match("--?include-dir", sys.argv[i]):
      # make sure we have an optional argument
      if len(sys.argv) > i and not re.match("-",sys.argv[i+1]):
        autosetup.append_include_dir(sys.argv[i+1])
        del sys.argv[i+1]
      else:
        print >>sys.stderr, "--include-dir requires an include dir"
        return 1
      del sys.argv[i]
    elif re.match("--?help", sys.argv[i]):
      autosetup.run_tests = False
      print >>sys.stderr, "Additional setup options"
      print >>sys.stderr, "  --enable-da  : build C DA mododule"
      print >>sys.stderr, "  --notest     : don't runt tests"
      print >>sys.stderr, "  --inplace    : copy the complied extensions in to the python source"
      print >>sys.stderr, "  --include-dir: add a directory to our extension header list"
      print >>sys.stderr        

  autosetup.setup()
  return 0

if __name__ == "__main__":
  sys.exit(main())
