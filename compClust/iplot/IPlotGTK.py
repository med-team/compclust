import sys
if not sys.modules.has_key('gtk'):
  import pygtk
  pygtk.require('2.0')
import gtk

import matplotlib
matplotlib.use('GTKAgg')

from compClust.util import DistanceMetrics

from CanvasFactoryGTK import CanvasFactoryGTK

from IPlot import DatasetPlot as DatasetPlotBase
from TrajectorySummary import TrajectorySummary as TrajectorySummaryBase
from ConfusionMatrixSummary import ConfusionMatrixSummary as ConfusionMatrixSummaryBase
from ROCPlot import ROCPlot as ROCPlotBase
from PCAExplorer import PCAPlot as PCAPlotBase
from VennDiagram import VennDiagram as VennDiagramBase
from PCAGinzu import PCAGinzu as PCAGinzuBase

import CanvasFactory

  
class IPlot(DatasetPlotBase):
  """Wrap matplotlib based IPlot in a TK environment
  """
  def __init__(self, plotView=None):
    DatasetPlotBase.__init__(self, CanvasFactoryGTK(), plotView=plotView)
    self.statusbar_context = None
    # signal mapper
    signals = {
      "on_x_log_scale_toggle": self.xlogScale,
      "on_y_log_scale_toggle": self.ylogScale,
      "on_descending_toggle": self.descending,
      #"on_crosshairs_toggle", self.,
      "on_grid_toggle": self.showGrid,
      "on_legend_toggle": self.showLegend,
      "on_axis_toggle": self.showAxis,
      "on_quit1_activate": gtk.mainquit,
      }
    # FIXME: need to define a way of connecting signals to window
    self.canvasFactory.getWidgets(self.figure).signal_autoconnect(signals)
    
  def displayValue(self, line, point):
    """Display clicked on value in the status bar
    """
    x,y = point
    status = "%s: (%3.2f, %3.2f)"%(line.get_label(), x, y)
    statusbar = self.canvasFactory.getStatusbar(self.figure)
    if self.statusbar_context is None:
      self.statusbar_context = self.canvasFactory.getStatusbarContext(self.figure, "iplot")
    statusbar.push(self.statusbar_context, status)

  # The next functions configures the Crosshairs
  def mouseMove(self, event):
    self.crosshairs_configure(position="@" +str(event.x) +","+str(event.y))
    
    
  def showCrosshairs(self):
    hide = not int(self.crosshairs_cget('hide'))
    self.crosshairs_configure(hide = hide, dashes="1")
    if(hide):
      self.unbind("<Motion>")
    else:
      self.bind("<Motion>", self.mouseMove)

class DatasetPlot(DatasetPlotBase):
  def __init__(self, plotView=None, parent=None):
    DatasetPlotBase.__init__(self, CanvasFactoryGTK(parent), plotView=plotView)
       
class TrajectorySummary(TrajectorySummaryBase):
  def __init__(self, dataset, clusterLabeling, primaryLabeling=None, secondaryLabeling=None, computeROC=1, parent=None):
    TrajectorySummaryBase.__init__(self, CanvasFactoryGTK(parent), dataset, clusterLabeling, primaryLabeling, secondaryLabeling, computeROC)

class ConfusionMatrixSummary(ConfusionMatrixSummaryBase):
  def __init__(self, dataset, labeling1, labeling2, primaryLabeling=None, secondaryLabeling=None,l1Order=None, l2Order=None, parent=None):
    ConfusionMatrixSummaryBase.__init__(self, CanvasFactoryGTK(parent), dataset, labeling1, labeling2, primaryLabeling, secondaryLabeling,l1Order, l2Order, parent)

class PCAPlot(PCAPlotBase):
  def __init__(self, dataset, primaryLabeling=None, secondaryLabeling=None, parent=None):
    PCAPlotBase.__init__(self, CanvasFactoryGTK(parent), dataset, primaryLabeling, secondaryLabeling)
               
class ROCPlot(ROCPlotBase):
    def __init__(self, dataset, labeling, label, distanceMetric=DistanceMetrics.EuclideanDistance, parent=None):
      ROCPlotBase.__init__(self, CanvasFactoryGTK(parent), dataset, labeling, label, distanceMetric)
      
class VennDiagram(VennDiagramBase):
  def __init__(self, set1, set2, set3=None, set1Name='', set2Name='', set3Name='', labeling=None, displayFunction=None, parent=None):
    VennDiagramBase.__init__(self, CanvasFactoryGTK(parent), set1, set2, set3, set1Name, set2Name, set3Name, labeling, displayFunction)

class PCAGinzu(PCAGinzuBase):
  # NOTE, if you change the parameter list here, you'll need to change it in the
  # compClust.iplot.IPlot* modules, compClust.mlx.pcaGinsu, and PCAGinsu
  def __init__(self, dataset, nOutliers=None, outlierCutoff=None, sigCutoff = 0.05, 
               maxPCNum = None, verbose = False, rowPCAView = None, makeLabelings = True,
               primaryLabeling=None, secondaryLabeling=None, parent=None):
    PCAGinzuBase.__init__(self, CanvasFactoryGTK(parent), dataset, nOutliers, outlierCutoff, sigCutoff, maxPCNum, verbose, rowPCAView, makeLabelings, primaryLabeling, secondaryLabeling)

    
