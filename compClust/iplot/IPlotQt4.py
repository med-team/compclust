import matplotlib
matplotlib.use('qt4agg')

from compClust.util import DistanceMetrics

from IPlot import IPlot as IPlotBase
from IPlot import DatasetPlot as DatasetPlotBase
from TrajectorySummary import TrajectorySummary as TrajectorySummaryBase
from ConfusionMatrixSummary import ConfusionMatrixSummary as ConfusionMatrixSummaryBase
from ROCPlot import ROCPlot as ROCPlotBase
from PCAExplorer import PCAPlot as PCAPlotBase
from VennDiagram import VennDiagram as VennDiagramBase
from PCAGinzu import PCAGinzu as PCAGinzuBase

from CanvasFactoryQt4 import CanvasFactoryQt4
import sys


class IPlot(DatasetPlotBase):
  """Wrap matplotlib based IPlot in a TK environment
  """
  def __init__(self, plotView=None):
    # did someone give us a tk master?

    DatasetPlotBase.__init__(self, CanvasFactoryQt4(), plotView=plotView)
    
  def displayValue(self, line, point):
    """
    display an element's name, and its coordinates in data space

    Formely showed the the current value...

    """
    x,y = point
    status = "%s: (%3.2f, %3.2f)"%(line.get_label(), x, y)

    print status
    self.canvas.show()

class DatasetPlot(DatasetPlotBase):
  def __init__(self, plotView=None):
    DatasetPlotBase.__init__(self, CanvasFactoryQt4(), plotView=plotView)

class TrajectorySummary(TrajectorySummaryBase):
  def __init__(self, dataset, clusterLabeling, primaryLabeling=None, secondaryLabeling=None, computeROC=1):
    TrajectorySummaryBase.__init__(self, CanvasFactoryQt4(), dataset, clusterLabeling, primaryLabeling, secondaryLabeling, computeROC)

class ConfusionMatrixSummary(ConfusionMatrixSummaryBase):
  def __init__(self, dataset, labeling1, labeling2, primaryLabeling=None, secondaryLabeling=None,l1Order=None, l2Order=None):
    ConfusionMatrixSummaryBase.__init__(self, CanvasFactoryQt4(), dataset, labeling1, labeling2, primaryLabeling, secondaryLabeling,l1Order, l2Order)

class ROCPlot(ROCPlotBase):
  def __init__(self, dataset, labeling, label, distanceMetric=DistanceMetrics.EuclideanDistance):
    ROCPlotBase.__init__(self, CanvasFactoryQt4(), dataset, labeling, label, distanceMetric)

class PCAPlot(PCAPlotBase):
  def __init__(self, dataset, primaryLabeling=None, secondaryLabeling=None, parent=None):
    PCAPlotBase.__init__(self, CanvasFactoryQt4(parent), dataset, primaryLabeling, secondaryLabeling)
               

class VennDiagram(VennDiagramBase):
  def __init__(self, set1, set2, set3=None, set1Name='', set2Name='', set3Name='', labeling=None, displayFunction=None):
    VennDiagramBase.__init__(self, CanvasFactoryQt4(), set1, set2, set3, set1Name, set2Name, set3Name, labeling, displayFunction)

class PCAGinzu(PCAGinzuBase):
  # NOTE, if you change the parameter list here, you'll need to change it in the
  # compClust.iplot.IPlot* modules, compClust.mlx.pcaGinzu, and PCAGinzu
  def __init__(self, dataset, nOutliers=None, outlierCutoff=None, sigCutoff = 0.05, 
               maxPCNum = None, verbose = False, rowPCAView = None, makeLabelings = True,
               primaryLabeling=None, secondaryLabeling=None, parent=None):
    PCAGinzuBase.__init__(self, CanvasFactoryQt4(parent), dataset, nOutliers, outlierCutoff, sigCutoff, maxPCNum, verbose, rowPCAView, makeLabelings, primaryLabeling, secondaryLabeling)

    
