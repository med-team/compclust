class IPlotView:

  """
  IPlotView is a special compClust.View which implements data getters
  such that any IPlotView can be plugged into the  def __init__(self


  DatasetPlot.plot function. 
  """

  def __init__(self, dataset):

    """
    Initializes the Plot View.
    """

    # is a transient pointer to the plot which
    # is using the this PlotView
    self.__plot = None
    self.__dataset = dataset

    # these up the criticle mappers.  Only the dataMapper is required.
    self._dataMapper       = None 
    self._colorMapper      = None  
    self._markersMapper    = None
    self._annotationMapper = None
    self._bindingsMapper   = None

    #try:

    # added by sagar --- important variable to have for plotting utility
    try:
      self.__dataset.IPlotViews
    except:
      self.__dataset.IPlotViews = []
      
    #self.__dataset.IPlotViews.append(self)
    #except:
    #  pass

  def _setPlot(self, plot):
    """
    setPlot(self)

    sets the trainsient plot pointer
    """
    self.__plot = plot
    
  def _getPlot(self):
    """
    setPlot(self)

    sets the trainsient plot pointer
    """

    return(self.__plot)

  def getDataset(self):

    return(self.__dataset)

  def getDataMapper(self):

    return(self._dataMapper)

  def getColorMapper(self):

    return(self._colorMapper)
    
  def getMarkersMapper(self):

    return(self._markersMapper)
    
  def getAnnotationMapper(self):

    return(self._annotationMapper)

  def getBindingsMapper(self):

    return(self._bindingsMapper)

  def plotSetup(self):

    """
    plotSetup(self)

    this function gets called before any plotting begins. 
    """

    pass

  def plotFinishings(self):
    """
    plotEnd(self)

    this function get called last in the plot function.
    """

    pass

  def getKWArgs (self):

    """
    addKWargs(self)

    This is ment to allow for a complete pass through to the BLT
    options configuration.  It can either be none, or a list of tuples
    for each element being plotted
    """

    return(None)
    
  def refresh(self):

    """
    refresh

    signals that the view has changed and the plot should update.
    """

    if self.__plot is not None:
      self.__plot.plot()


#################
#
# The Fairly general purpose all around nice dataset plotView.
#
#########

class DatasetRowPlotView(IPlotView):

  """
  This class is a general purpose plot view.  Good for exploratory
  analysis.  It features pluggable Mappers to allow for on-the-fly
  exploration.
  
  """

  def __init__(self, dataset, primaryLabeling=None, secondaryLabeling=None):
    # the mappers are only used here so I moved the import here
    # which solves a circular import I introduced
    
    from compClust.iplot.mappers.DataMapper import RowDataMapper
    from compClust.iplot.mappers.ColorMapper import RowColorMapper
    from compClust.iplot.mappers.MarkersMapper import RowMarkersMapper
    from compClust.iplot.mappers.BindingsMapper import SimpleBindingsMapper
    from compClust.iplot.mappers.AnnotationMapper import RowAnnotationMapper


    IPlotView.__init__(self, dataset)
    self._dataMapper       = RowDataMapper    (self)
    self._colorMapper      = RowColorMapper   (self)
    self._markersMapper    = RowMarkersMapper    (self)
    self._bindingsMapper   = SimpleBindingsMapper   (self)
    self._annotationMapper = RowAnnotationMapper (self, primaryLabeling, secondaryLabeling)

  def setDataMapper(self, dataMapper):

    """
    setDataMapper(self, dataMapper)
    """

    self._dataMapper = dataMapper

  def setColorMapper(self, colorMapper):

    """
    setColorMapper(self, colorMapper)
    """

    self._colorMapper = colorMapper

  def setMarkersMapper(self, markersMapper):

    """
    setColorMapper(self, colorMapper)
    """

    self._markersMapper = markersMapper

  def getAnnotationMapper(self):
    """return annotation mapper
    """
    return self._annotationMapper
