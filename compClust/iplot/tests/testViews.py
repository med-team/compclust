#!/usr/bin/python

"""testIPlot is a base class used by all the various IPlot<toolkit> tests
"""
import os
import unittest

from compClust.mlx import datasets
from compClust.mlx import labelings

from compClust.iplot import IPlotAgg as IPlot
from compClust.iplot.views import DatasetRowPlotView

class testViews(unittest.TestCase):
    
  def makeSimpleDataset(self):
    ds = datasets.Dataset([[0,0,0,0],[1,1,1,1],[2,2,2,2],[3,3,3,3]])    
    labels = ['zero','one','two','three']
    primaryLabeling = labelings.Labeling(ds)
    primaryLabeling.labelRows(labels)
    return ds, primaryLabeling, labels
      
  def testAnnotationMapperNoLabeling(self):
    """When no labeling is set, return the row indicies
    """
    ds, primaryLabeling, labels = self.makeSimpleDataset()
    
    row_view = DatasetRowPlotView(ds)
    #iplot = IPlot.DatasetPlot(row_view)
    self.failUnless(row_view.getAnnotationMapper().getPrimaryLabeling() is None)
    #line_labels = [ l.get_label() for l in iplot.axis.get_lines() ]
    # returns the row indicies as strings
    #self.failUnless(line_labels == ['0','1','2','3'])
    
  def testAnnotationMapperDatasetRowLabeling(self):
    """Return the primary labeling, when using the dataset.primaryRowLabeling option
    """
    ds, primaryLabeling, labels = self.makeSimpleDataset()
    ds.primaryRowLabeling = primaryLabeling
    row_view = DatasetRowPlotView(ds)
    mappedPrimaryLabeling = row_view.getAnnotationMapper().getPrimaryLabeling()
    self.failUnless(mappedPrimaryLabeling.getAllRowLabels() ==  primaryLabeling.getAllRowLabels())
    
    #iplot = IPlot.DatasetPlot(row_view)    
    #line_labels = [ l.get_label() for l in iplot.axis.get_lines() ]
    #self.failUnless(line_labels == labels)
    
  def testAnnotationMapperRowAnnotation(self):
    """Return the primary labeling, when setting the primary labeling using the annotation mapper
    """
    ds, primaryLabeling, labels = self.makeSimpleDataset()
    row_view = DatasetRowPlotView(ds, primaryLabeling)
    mappedPrimaryLabeling = row_view.getAnnotationMapper().getPrimaryLabeling()
    self.failUnless(mappedPrimaryLabeling.getAllRowLabels() ==  primaryLabeling.getAllRowLabels())
    
    #iplot = IPlot.DatasetPlot(row_view)
    #line_labels = [ l.get_label() for l in iplot.axis.get_lines() ]
    #self.failUnless(line_labels == labels)
        
    
def suite(*args, **kwargs):
  return unittest.makeSuite(testViews)

if __name__ == "__main__":
  unittest.main(defaultTest="suite")
  