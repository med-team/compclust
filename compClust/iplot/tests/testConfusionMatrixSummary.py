#!/usr/bin/python

"""testIPlot is a base class used by all the various IPlot<toolkit> tests
"""
import os
import unittest

from compClust.mlx import datasets
from compClust.mlx import labelings

from compClust.iplot.views import DatasetRowPlotView
from compClust.util.LoadExample import LoadCho
from compClust.util.WrapperBuilder import WrapperBuilder
from compClust.util.WrapperPlugins import WrapperPlugins

from compClust.iplot import IPlotAgg as IPlot

class testConfusionMatrixSummary(unittest.TestCase):
  def setUp(self):
    self.ds = LoadCho()
    
  def testCM(self):
    """Test confusion matrix summary
    Try to test Ticket #20 where we have a problem where the second
    time a confusion matrix is generated the summary counts
    are very wrong.
    """
    wrappers = WrapperPlugins()
    builder = WrapperBuilder(self.ds)
    builder.append(wrappers['DiagEM'])
    builder[-1].parameters.k = 5
    builder.run()
    new_diagem = builder.getLabeling()
    
    cho=self.ds.getLabeling("cho clusters")
    diagem=self.ds.getLabeling("diagem clusters")
    
    names=self.ds.getLabeling("common name")
    cm1 = IPlot.ConfusionMatrixSummary(self.ds, cho, diagem, names)
    cm2 = IPlot.ConfusionMatrixSummary(self.ds, cho, new_diagem, names)    
    
def suite(*args, **kwargs):
  return unittest.makeSuite(testConfusionMatrixSummary)

if __name__ == "__main__":
  unittest.main(defaultTest="suite")
  
