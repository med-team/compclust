#!/usr/bin/python
import os
try:
  import Tkinter as Tk
  TkAvailable = True
except ImportError, e:
  TkAvailable = False
import unittest

from compClust.iplot.tests.testIPlot import baseTestIPlot

from compClust.iplot.IPlotTk import *

class testIPlotTk(baseTestIPlot):
  def setUp(self):
    super(testIPlotTk, self).setUp()
    self.dataset_plot = IPlot
    self.trajectory_summary_plot = TrajectorySummary
    self.confusion_matrix_plot = ConfusionMatrixSummary
    self.roc_plot = ROCPlot
    self.pca_plot = PCAPlot
    self.pca_ginzu = PCAGinzu
    
def suite(test_tk=True, **kw):
  """generate test cases, if test_tk is false, don't run the tests
  """
  if test_tk and TkAvailable:
    return unittest.makeSuite(testIPlotTk)
  else:
    print "Tk not available not running tk tests in testIPlotTk"
    return None

if __name__ == "__main__":
  suite = unittest.TestSuite()
  suite.addTest(unittest.makeSuite(testIPlotTk))
  unittest.TextTestRunner(verbosity=2).run(suite)

  Tk.mainloop()
