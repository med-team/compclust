#!/usr/bin/python
import os
try:
    import Tkinter as Tk
    TkAvailable = True
    from compClust.IPlot import DatasetPlot, DatasetRowPlotView, \
                                ConfusionMatrixSummary, pcaExplore
except ImportError, e:
    TkAvailable = False


from compClust.mlx import datasets
from compClust.mlx import labelings



def loaddata():
    dataroot="../../gui/Examples/ChoCellCycling"
    # load data
    ds = datasets.Dataset(os.path.join(dataroot,"ChoCycling.dat"))

    # load some labelings
    cho = labelings.GlobalLabeling(ds, "Cho")
    cho.labelRows(ds, os.path.join(dataroot, "ChoClassification.rlab"))
    orfs = labelings.GlobalLabeling(ds, "orfs")
    orfs.labelRows(ds, os.path.join(dataroot, "ORFs.rlab"))
    common = labelings.GlobalLabeling(ds,"common name")
    common.labelRows(ds, os.path.join(dataroot, "CommonNames.rlab"))
    times = labelings.GlobalLabeling(ds,"Time Points")
    times.labelCols(ds, os.path.join(dataroot, "times.clab"))

    return ds, common, cho

def plotdata(ds, primary, secondary):
    # plot some simple data
    dp = DatasetPlot()
    v = DatasetRowPlotView(ds, primaryLabeling=primary, secondaryLabeling=secondary)
    dp.plot(v)

def confusion_matrix(ds):
    cho_cluster = ds.getLabeling("Cho")
    diagem_cluster = ds.getLabeling("diagem")
    names = ds.getLabeling("common name")
    ConfusionMatrixSummary(ds, cho_cluster, diagem_cluster, names)

def pca_explorer(ds):
    orfs = ds.getLabeling("orfs")
    names = ds.getLabeling("common name")
    pcaExplore(ds, orfs, names)
    
if __name__ == "__main__":
    ds, primary, secondary = loaddata()
    #plotdata(ds, primary, secondary)
    #confusion_matrix(ds)
    pca_explorer(ds)
    Tk.mainloop()
