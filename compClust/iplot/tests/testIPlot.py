#!/usr/bin/python

"""testIPlot is a base class used by all the various IPlot<toolkit> tests
"""
import os
import unittest


from compClust.mlx import datasets
from compClust.mlx import labelings

from compClust.iplot.views import DatasetRowPlotView
from compClust.util.LoadExample import LoadCho
    

class baseTestIPlot(unittest.TestCase):
  def setUp(self):
    self.ds = LoadCho()
    self.names = self.ds.getLabeling('common names')
    self.cho_cluster = self.ds.getLabeling("cho clusters")
    self.diagem_cluster = self.ds.getLabeling("diagem clusters")
    self.orfs = self.ds.getLabeling("orfs")
    self.times = self.ds.getLabeling("time points")
    
  def testSimplePlot(self):
    # plot some simple data
    # if we're being run without having testcases set just exit    
    if not hasattr(self, "dataset_plot"):
      return
    dp = self.dataset_plot()
    v = DatasetRowPlotView(self.ds,
                           primaryLabeling=self.names,
                           secondaryLabeling=self.cho_cluster)
    dp.plot(v)

  def testTrajectorySummaryPlot(self):
    # if we're being run without having testcases set just exit    
    if not hasattr(self, "tranjectory_summary_plot"):
      return
    
    self.trajectory_summary_plot(self.ds, 
                                 self.cho_cluster, 
                                 primaryLabeling=self.names)
    #TrajectorySummary(ds, classification, primaryLabeling=primary)

  def testConfusionMatrixSummaryPlot(self):
    # if we're being run without having testcases set just exit    
    if not hasattr(self, "confusion_matrix_plot"):
      return
    self.confusion_matrix_plot(self.ds, 
                               self.cho_cluster, 
                               self.diagem_cluster, 
                               self.names)
    
    #self.confusion_matrix_plot(self.ds, self.choCluster, self.diagemCluster, self.commonName)

  def testROCPlot(self):
    # if we're being run without having testcases set just exit    
    if not hasattr(self, "roc_plot"):
      return

    self.roc_plot(self.ds, self.cho_cluster, 'Early G1')
    #self.roc_plot(self.ds, diagem_cluster, '3')
    
  def testPCAPlot(self):
    # if we're being run without having testcases set just exit    
    if not hasattr(self, "pca_plot"):
      return
    pcaplot = self.pca_plot(self.ds, self.orfs, self.names)
    pcaplot.ProjectionPlot(2,3)

  def testPCAGinzu(self):
    # if we're being run without having testcases set just exit    
    if not hasattr(self, "pca_ginzu"):
      return
    self.ds.primaryRowLabeling = self.orfs
    self.ds.primaryColumnLabeling = self.times
    ginzu = self.pca_ginzu(self.ds, secondaryLabeling=self.names)
    ginzu.plotPCNOutlierRowsInOriginalColumnOrder(1)
    ginzu.plotPCNOutlierRowsInSigGroupOrder(1)
    ginzu.plotPCvsPCWithOutliersInY(0,1)
    self.ds.primaryRowLabeling = None
    self.ds.primaryColumnLabeling = None

def suite(*args, **kwargs):
  # this is just a base clase and is really intended to be used
  # from testIPlot(Tk,Gtk,Agg)
  return None
