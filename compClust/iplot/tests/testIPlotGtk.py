#!/usr/bin/python
import os
import unittest

try:
  from compClust.iplot.IPlotGTK import *
  GtkAvailable = True
except ImportError, e:
  GtkAvailable = False

skip_test = False
from compClust.iplot.tests.testIPlot import baseTestIPlot

from compClust.mlx import datasets
from compClust.mlx import labelings

from compClust.iplot.IPlot import DatasetPlot
from compClust.iplot.views import DatasetRowPlotView


class testIPlotGTK(baseTestIPlot):
  def setUp(self):

    testIPlot.setUp(self)
    self.dataset_plot = IPlot
    self.trajectory_summary_plot = TrajectorySummary
    self.confusion_matrix_plot = ConfusionMatrixSummary
    self.roc_plot = ROCPlot
    self.pca_ginzu = PCAGinzu

def suite(test_gtk=True, **kw):
  return None
  if test_gtk and GtkAvailable:
    return unittest.makeSuite(testIPlotGTK)
  else:
    print "GTK not available not running gtk tests in testIPlotTk"
    return None

if __name__ == "__main__":
  suite = unittest.TestSuite()
  suite.addTest(unittest.makeSuite(testIPlotGTK))
  unittest.TextTestRunner(verbosity=2).run(suite)
  
  gtk.mainloop()
