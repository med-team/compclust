from compClust.iplot.mappers.Mapper import Mapper

from compClust.mlx import labelings
from compClust.iplot.views import IPlotView

class AnnotationMapper(Mapper):

  """
  This is a support class for the IPlotView class which descrbes
  how the elements should be annotated.
  """
  def getPrimaryLabeling(self):

    """
    getPrimaryAnnoations(self)

    returns a None or a labeling to be used as the primary element
    annotations - element names... these must each be unique.
    
    """

    return(None)

  def getSecondaryLabeling(self):

    """
    getSecondaryAnnoations(self)

    returns a None or a labeling to be used as the secondary element
    annotations - perhaps long discriptions
    
    """
    return(None)

class RowAnnotationMapper(AnnotationMapper):

  """
  provides a mapping of the primary and secondary annotations to
  labelings.

  """

  def __init__(self, plotView, labeling1 = None, labeling2 = None):

    """
    __init__(self, labeling1 = None, labeling2 = None)

    """
    super(AnnotationMapper, self).__init__(plotView)
    self.__lab1 = None
    self.__lab2 = None
    
    # see if the dataset has a primaryRowLabeling
    if labeling1 is None:
      labeling1 = self.dataset.primaryRowLabeling
      
    if labeling1:
      #labeling = labelings.castToGlobalLabeling(plotView.getDataset(), labeling1)
      self.__lab1 = labelings.castToGlobalWrapper(labeling1, dataset=plotView.getDataset(), removeLocal=False)
      #labelings.GlobalWrapper(plotView.getDataset(), glabeling=labeling1)
    if labeling2:
      #labeling = labelings.castToGlobalLabeling(plotView.getDataset(), labeling2)
      self.__lab2 = labelings.castToGlobalWrapper(labeling2, dataset=plotView.getDataset(), removeLocal=False)
      #labelings.GlobalWrapper(plotView.getDataset(), glabeling=labeling2)

  def getPrimaryLabeling(self):

    """
    getPrimaryAnnoations(self)

    returns a None or a labeling to be used as the primary element
    annotations - element names... these must each be unique.
    
    """

    return(self.__lab1)

  def getSecondaryLabeling(self):

    """
    getSecondaryAnnoations(self)

    returns a None or a labeling to be used as the secondary element
    annotations - perhaps long discriptions
    
    """

    return(self.__lab2)

  def setPrimaryLabeling(self, labeling):

    """
    setPrimaryLabeling(self, labeling):
    
    This labeling must be unique.
    """

    self.__lab1 = labeling

  def setSecondaryLabeling(self, labeling):

    """
    setSecondaryLabeling(self, labeling):
    
    """

    self.__lab2 = labeling
    
