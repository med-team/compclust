"""
Mappers provide tools to customize how attributes of a DatasetPlot are rendered. 

This includes things like 
  AnnotationMapper :- defines what annotation is attached to a particular vector.
  BindingMapper :- (mostly unimplemented, but should define a relationship between actions
                   on a vector and user interface actions.)
  ColorMapper :- Defines how a vector should be colored on the dataset plot.
  DataMapper :- Defines ways to change how a vector is plotted.
  MarkersMapper :- Defines the shape of the marker for each vector.
  
Each of those classes has a variety of subclasses specifying the details of how 
to perform those actions.
"""
import AnnotationMapper
import BindingsMapper
import ColorMapper
import DataMapper
import MarkersMapper
