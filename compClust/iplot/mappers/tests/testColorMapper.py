#!/usr/bin/python

"""testIPlot is a base class used by all the various IPlot<toolkit> tests
"""
import colorsys
import os
import unittest

from compClust.mlx import datasets
from compClust.mlx import labelings

from compClust.iplot import IPlotAgg as IPlot
from compClust.iplot.views import DatasetRowPlotView
from compClust.iplot.mappers import ColorMapper
from compClust.iplot.mappers.Mapper import scaleList

class testColorMapper(unittest.TestCase):
    
  def setUp(self):
    self.data_list = [[0,0,0,0],[1,1,1,1],[2,2,2,2],[3,3,3,3],[4,4,4,4],[5,5,5,5]]
    self.ds = datasets.Dataset(self.data_list)
    self.primary = labelings.Labeling(self.ds)
    self.primary.labelRows(['zero','one','two','three','four','five'])
    self.ds.primaryRowLabeling = self.primary
    
    self.probabilities_list = [(1.0, 0.1), (0.9, 1.0), (0.8, 0.3), (0.7, 0.7), (0.1, 0.05), (1.0, 0.95)]
    self.probabilities = labelings.Labeling(self.ds)
    self.probabilities.labelRows(self.probabilities_list)
    
    self.row_view = DatasetRowPlotView(self.ds)
    
    
  # test base class
  def testColorMap(self):
    """Can we get/set basic colors in the different color models
    """
    color_map = ColorMapper.ColorMapper(self.row_view, self.ds.getNumRows())
    # Be careful of your color choices as not all colors have one to one mappings
    # (E.g. it seems like hsv has several ways of specifying black)
    hsv_colors = [(0, 0, 0),(0, 1, 1),(0, .5, 1),(0.5, 0.5, 0.5),(0.1,0.1,0.1),(0.2,0.2,0.2)]
    rgb_colors = [ colorsys.hsv_to_rgb(h,s,v) for h,s,v in hsv_colors]
    rgba_colors = [ tuple(list(colorsys.hsv_to_rgb(h,s,v))+[1]) for h,s,v in hsv_colors]
    
    def test_set_colors():
      def test_colors(color1, color2):
        # compare two colors channel by channel
        self.failUnless(len(color1) == len(color2), 
                        msg="len(color 1)=%d, len(color 2)=%d" %(len(color1),len(color2)))
        for x,y in zip(color1, color2):
          self.failUnlessAlmostEqual(x,y)
      # test __getitem__          
      for i in xrange(len(color_map)):
        test_colors(color_map[i], rgba_colors[i])
      
      def test_color_lists(color_list1, color_list2):
        for color1, color2 in zip(color_list1, color_list2):
          test_colors(color1, color2)  
      #test getColors
      test_color_lists(color_map.getColors(), rgba_colors)
      test_color_lists(color_map.getColors('rgba'), rgba_colors)
      test_color_lists(color_map.getColors('rgb'), rgb_colors)
      test_color_lists(color_map.getColors('hsv'), hsv_colors)
      
    # now that we created all those utility functions see if setColors works
    color_map.setColors(rgba_colors)
    test_set_colors()
    color_map.setColors(rgba_colors, 'rgba')
    test_set_colors()
    color_map.setColors(rgb_colors, 'rgb')
    test_set_colors()
    color_map.setColors(hsv_colors, 'hsv')
    test_set_colors()
    
    color_map.setColors(None)
    self.failUnless(color_map.getColors() is None)
    
    # wrong number of elements specifying a color
    self.failUnlessRaises(ValueError, color_map.setColors, [[0],[.1],[.2],[.3],[.4],[.5]])
    # wrong number of elements
    self.failUnlessRaises(ValueError, color_map.setColors, [[0,0,0,0],[0.1, 0.1, 0.1, 0.1]])
    
    rgba_white = [1,1,1,1]
    color_map[0] = rgba_white
    color_map.getColor(0) == rgba_white
    color_map.setColor(0, hsv_colors[0], 'hsv')
    color_map[0] == rgba_colors[0]
    
    values = [0.1, 0.2, 0.3, 0.4, 0.5,0.6]
    ones = [1] * len(values)
    color_map.setColors(zip(values,ones,ones), 'hsv')

  
  # test RowColorMapper 
  def testColorByLabelingCounts(self):
    def count_labels(labels):
      """Return a list with each label replaced its total number of occurences
      """
      label_counts = {}
      for l in labels:
        if label_counts.has_key(l):
          label_counts[l] += 1
        else:
          label_counts[l] = 1
      return [ label_counts.get(l, 0) for l in labels]
        
    color_map = ColorMapper.RowColorMapper(self.row_view)        
    row_labels = [0,1,1,2,2,2]
    row_counts = count_labels(row_labels)
    scaled_list = scaleList(row_counts, None, None, 0, 0.7)
    counts = labelings.Labeling(self.ds)
    counts.labelRows(row_labels)
    color_map.setColorByLabelingCounts(counts)
    hsv_colors = color_map.getColors('hsv')
    
    for test_color, computed_color in zip(hsv_colors, scaled_list):
      self.failUnlessAlmostEqual(test_color[0], computed_color, 
                                 msg="didn't match %s != %s" % (test_color[0], computed_color))
                                 
                                 
    # try again with a labeling that doesn't specify a value for every row
    num_to_remove = 2
    missing_counts = count_labels(row_labels[:-num_to_remove])+[0]*num_to_remove
    scaled_missing = scaleList(missing_counts, None, None, 0, 0.7)
    counts_missing = labelings.Labeling(self.ds)
    for row in xrange(self.ds.getNumRows()-num_to_remove):
      counts_missing.addLabelToRow(row_labels[row], row)
    color_map.setColorByLabelingCounts(counts_missing)
    hsv_colors = color_map.getColors('hsv')
    
    for test_color, computed_color in zip(hsv_colors, scaled_missing):
      self.failUnlessAlmostEqual(test_color[0], computed_color, 
                                 msg="didn't match %s != %s" % (test_color[0], computed_color))
                                 
  def testColorByLabeling(self):
    color_map = ColorMapper.RowColorMapper(self.row_view)        
    row_labels = [0,1,1,2,2,2]
    scaled_list = scaleList(row_labels, None, None, 0, 0.7)
    labeling = labelings.Labeling(self.ds)
    labeling.labelRows(row_labels)
    color_map.setColorByLabeling(labeling)
    hsv_colors = color_map.getColors('hsv')
    
    for test_color, computed_color in zip(hsv_colors, scaled_list):
      self.failUnlessAlmostEqual(test_color[0], computed_color, 
                                 msg="didn't match %s != %s" % (test_color[0], computed_color))
                                                                  
    # try again with a labeling that doesn't specify a value for every row
    num_to_remove = 2
    scaled_missing = scaleList(row_labels[:-num_to_remove]+[None]*num_to_remove, None, None, 0, 0.7)
    counts_missing = labelings.Labeling(self.ds)
    for row in xrange(self.ds.getNumRows()-num_to_remove):
      counts_missing.addLabelToRow(row_labels[row], row)
    color_map.setColorByLabeling(counts_missing)
    hsv_colors = color_map.getColors('hsv')
    
    for test_color, computed_color in zip(hsv_colors, scaled_missing):
      self.failUnlessAlmostEqual(test_color[0], computed_color, 
                                 msg="didn't match %s != %s" % (test_color[0], computed_color))
      
    # Q: what happens when we specify a label?
    # A: the color scaling is based on only the included labels
    row_labels = [0,1,1,2,2,2]
    labels_to_include = [0,1]
    
    # construct a scaled list of labels like setColorByLabeling does
    filtered_labels = []
    for l in row_labels:
      if l in labels_to_include:
        filtered_labels.append(l)
      else:
        filtered_labels.append(None)
    scaled_list = scaleList(filtered_labels, None, None, 0, 0.7)
    
    labeling = labelings.Labeling(self.ds)
    labeling.labelRows(row_labels)
    color_map.setColorByLabeling(labeling, labels_to_include)
    hsv_colors = color_map.getColors('hsv')
    for test_color, computed_color in zip(hsv_colors, scaled_list):
      self.failUnlessAlmostEqual(test_color[0], computed_color, 
                                 msg="didn't match %s != %s" % (test_color[0], computed_color))
                                                                  
      
  def testColorByTupleLabeling(self):
    color_map = ColorMapper.RowColorMapper(self.row_view)        
    row_labels = [(0,0,0),(1,1,1),(1,1,1),(2,2,2),(2,2,2),(2,2,2)]
    scaled_list = scaleList([ x[0] for x in row_labels], None, None, 0, 0.7)
    labeling = labelings.Labeling(self.ds)
    labeling.labelRows(row_labels)
    color_map.setColorByTupleLabeling(labeling, element=0)
    hsv_colors = color_map.getColors('hsv')
    
    for test_color, computed_color in zip(hsv_colors, scaled_list):
      self.failUnlessAlmostEqual(test_color[0], computed_color, 
                                 msg="didn't match %s != %s" % (test_color[0], computed_color))
                                                                  
  def testColorByColValue(self):      
    color_map = ColorMapper.RowColorMapper(self.row_view)        
    column = 0
    col_data = self.ds.getColData(column)
    col_scale = scaleList(col_data, min(col_data), max(col_data),0, 0.7)
    color_map.setColorByColValue(column)
    hsv_colors =color_map.getColors('hsv')
    
    for test_color, computed_color in zip(hsv_colors, col_scale):
      self.failUnlessAlmostEqual(test_color[0], computed_color, 
                                 msg="didn't match %s != %s" % (test_color[0], computed_color))
    
  def testColorByFunction(self):
    def myfun(dataset, row):
      return sum(dataset.getRowData(row))
    color_map = ColorMapper.RowColorMapper(self.row_view)        
    fun_data = [ myfun(self.ds, row) for row in xrange(self.ds.getNumRows())]
    fun_scale = scaleList(fun_data, None, None,0, 0.7)
    color_map.setColorByFunction(myfun)
    hsv_colors =color_map.getColors('hsv')
    
    for test_color, computed_color in zip(hsv_colors, fun_scale):
      self.failUnlessAlmostEqual(test_color[0], computed_color, 
                                 msg="didn't match %s != %s" % (test_color[0], computed_color))
    
    
  # test probability colormap
  def testSetHueByPartitioning(self):
    color_map = ColorMapper.ClusterProbabilityColorMapper(self.row_view, self.probabilities)
    color_map.setHueByPartitioning(colorRange = (0,1))
    # FIXME: Any suggestions on how to check the results of this?
    
  def testSetHueByProbability(self):
    color_map = ColorMapper.ClusterProbabilityColorMapper(self.row_view, self.probabilities)
    color_map.setHueByProbability(0, colorRange = (0,1))
    # FIXME: Again the colors have gone through so many transformations i have no idea how to define what they should be.
    
def suite(*args, **kwargs):
  return unittest.makeSuite(testColorMapper)

if __name__ == "__main__":
  unittest.main(defaultTest="suite")
  