"""A canvas factory is a helper class to construct various gui specific things
like windows with canvases on them and to grab components on those
canvases like statusbars.
"""
class CanvasFactory:
  def __init__(self, container=None):
    """Initialize the gui environment needed by this canvas
    """
    self.canvass = {}
    self.statusbars = {}
      
  def createCanvas(self, figure, parent=None):
    """Create a canvas and window to hold a particular figure
    """
    raise NotImplementedError()

  def getStatusbar(self, figure):
    """Return statusbar set function
    """
    return self.statusbars.get(figure, None)

  def setStatusbar(self, figure, func):
    """Set function which can be called to set the statusbar
    """
    self.statusbars[figure] = func
    
  def getIPlot(self, *args, **kwargs):
    """Return an IPlot class object
    """
    raise NotImplementedError()

  def getWindow(self, figure, parent=None):
    """Return reference to an object we can draw on
    """
    raise NotImplementedError()

