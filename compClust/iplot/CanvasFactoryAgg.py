"""Canvas Factories are mixin classes to handel dealing with the slight
differences between each different matplotlib backend
"""

import matplotlib
matplotlib.use("Agg")

from CanvasFactory import CanvasFactory
from matplotlib.backends.backend_agg import FigureCanvasAgg


class CanvasFactoryAgg(CanvasFactory):
  """Canvas Factories are mixin classes to handel dealing with the slight
  differences between each different matplotlib backend

  This is the one for Agg
  """

  
  def __init__(self, container=None):
    """Initialize the gui environment needed by this canvas
    """
    CanvasFactory.__init__(self)
      
  def createCanvas(self, figure, parent=None):
    canvas = self.canvass.setdefault(figure, FigureCanvasAgg(figure))
    win = self.getWindow(figure, parent)
      
    return canvas

  def getIPlot(self, *args, **kwargs):
    """Return an IPlot class object
    """
    import IPlotAgg
    return apply(IPlotAgg.IPlot, *args, **kwargs)

  def getDatasetPlot(self, *args, **kwargs):
    import IPlotAgg
    return apply(IPlotAgg.DatasetPlot, args, kwargs)
  
  def getWindow(self, figure, parent=None):
    """Return reference to an object we can draw on
    """
    return None
  
