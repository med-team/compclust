import matplotlib
matplotlib.use('TkAgg')

from compClust.util import DistanceMetrics

from IPlot import IPlot as IPlotBase
from IPlot import DatasetPlot as DatasetPlotBase
from TrajectorySummary import TrajectorySummary as TrajectorySummaryBase
from ConfusionMatrixSummary import ConfusionMatrixSummary as ConfusionMatrixSummaryBase
from ROCPlot import ROCPlot as ROCPlotBase
from PCAExplorer import PCAPlot as PCAPlotBase
from PCAExplorer import PCAExplorer as PCAExplorerBase
from VennDiagram import VennDiagram as VennDiagramBase
from PCAGinzu import PCAGinzu as PCAGinzuBase

from CanvasFactoryTk import CanvasFactoryTk
import Tkinter as Tk
import sys


class IPlot(DatasetPlotBase):
  """Wrap matplotlib based IPlot in a TK environment
  """
#  # Unimplemented BLT functions
#  def marker_create(*args, **kws): return None
#  def bind(*args, **kws): return None
#  def element_bind(*args, **kws): return None
#  def legend_bind(*args, **kws): return None
#  def marker_bind(*args, **kws): return None
#  def configure(*args, **kws): return None
#  def legened_configure(*args, **kws): return None
#
  def __init__(self, plotView=None, parent=None):
    # did someone give us a tk master?

    DatasetPlotBase.__init__(self, CanvasFactoryTk(parent), plotView=plotView)
    
    self.canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)
    self.__defineMenus()
    
  def __defineMenus(self):
    # the axis option
    master = self.canvasFactory.getWindow(self.figure)
    self.axisMenu = Tk.Menu(master)
    self.axisMenu.add_command(label = 'x logscale', command = self.xlogScale)
    self.axisMenu.add_command(label = 'y logscale', command = self.ylogScale)
    self.axisMenu.add_command(label = 'descending', command = self.descending)
    #self.axisMenu.add_command(label = 'label axis', command = self.labelAxis)

    # general view options
    self.viewMenu = Tk.Menu(master)
    self.viewMenu.add_checkbutton(label = 'Toggle Crosshairs',
                                  command = self.showCrosshairs)
    self.viewMenu.add_checkbutton(label = 'Toggle Grid',
                                  command = self.showGrid)
    b = self.viewMenu.add_checkbutton(label = 'Toggle Legend',
                                      command =self.showLegend)
    b = self.viewMenu.add_checkbutton(label = 'Toggle Axis',
                                      command = self.showAxis)
    # file menu
    self.fileMenu = Tk.Menu(master)

    # options menu
    self.optionsMenu = Tk.Menu(master)
    self.optionsMenu.add_cascade(label='Axis', menu= self.axisMenu)

    # set up the main graph menu
    self.graphMenu = Tk.Menu(master)
    self.graphMenu.add_cascade(label = 'File', menu= self.fileMenu)
    self.graphMenu.add_cascade(label = 'View', menu= self.viewMenu)
    self.graphMenu.add_cascade(label = 'Settings', menu= self.optionsMenu)

# this was in here in case i had come up with some clever way of creating
# a platform independent way of accessing the scroll bar, but for the moment
# its not needed
#   def displayValue(self, line, point):
#     """
#     display an element's name, and its coordinates in data space
# 
#     Formely showed the the current value...
# 
#     """
#     x,y = point
#     status = "%s: (%3.2f, %3.2f)"%(line.get_label(), x, y)
#     statusbar = self.canvasFactory.getStatusbar(self.figure)
#     if statusbar is not None:
#       statusbar.helpmessage(status)
#     else:
#       self.axis.table([[status]], cellLoc='left', loc='top')
#       self.canvas.show()
# 
#     print status
                   
  # The next functions configure the axes
  def showAxis(self): 
    state = int(self.axis_cget("x", 'hide'))
    self.axis_configure(["x", "y"], hide = not state)
    
  def xlogScale(self):
    state = int(self.xaxis_cget('logscale'))
    self.xaxis_configure(logscale = not state)
    
  def ylogScale(self):
    state = int(self.yaxis_cget('logscale'))
    self.yaxis_configure(logscale = not state)
    
  def descending(self):
    state = int(self.axis_cget("x", 'descending'))
    self.axis_configure(["x", "y"], descending = not state)

  # The next functions configures the Grid
  def showGrid(self):
    self.grid_toggle()
    
  # The next functions configures the Legend
  def showLegend(self):
    state = int(self.legend_cget('hide'))
    self.legend_configure(hide = not state)

  # The next functions configures the Crosshairs
  def mouseMove(self, event):
    self.crosshairs_configure(position="@" +str(event.x) +","+str(event.y))
    
    
  def showCrosshairs(self):
    hide = not int(self.crosshairs_cget('hide'))
    self.crosshairs_configure(hide = hide, dashes="1")
    if(hide):
      self.unbind("<Motion>")
    else:
      self.bind("<Motion>", self.mouseMove)

class DatasetPlot(DatasetPlotBase):
  def __init__(self, plotView=None, parent=None):
    DatasetPlotBase.__init__(self, CanvasFactoryTk(parent), plotView=plotView)

class TrajectorySummary(TrajectorySummaryBase):
  def __init__(self, dataset, clusterLabeling, primaryLabeling=None, secondaryLabeling=None, computeROC=1, parent=None):
#    # did someone give us a tk master?
#    if kwargs.has_key('master'):
#      master = kwargs['master']
#      del kwargs['master']
#    else:
#      master = None

    TrajectorySummaryBase.__init__(self, CanvasFactoryTk(parent), dataset, clusterLabeling, primaryLabeling, secondaryLabeling, computeROC)

class ConfusionMatrixSummary(ConfusionMatrixSummaryBase):
  def __init__(self, dataset, labeling1, labeling2, primaryLabeling=None, secondaryLabeling=None,l1Order=None, l2Order=None, parent=None):
    ConfusionMatrixSummaryBase.__init__(self, CanvasFactoryTk(parent), dataset, labeling1, labeling2, primaryLabeling, secondaryLabeling,l1Order, l2Order, False, parent)

class ROCPlot(ROCPlotBase):
  def __init__(self, dataset, labeling, label, distanceMetric=DistanceMetrics.EuclideanDistance, parent=None):
    ROCPlotBase.__init__(self, CanvasFactoryTk(parent), dataset, labeling, label, distanceMetric)

class PCAPlot(PCAPlotBase):
  def __init__(self, dataset, primaryLabeling=None, secondaryLabeling=None, parent=None):
    PCAPlotBase.__init__(self, CanvasFactoryTk(parent), dataset, primaryLabeling, secondaryLabeling)

##
# This doesn't work yet
#class PCAExplorer(PCAExplorerBase):
#  def __init__(self, dataset, primaryLabeling=None, secondaryLabeling=None, parent=None):
#    PCAExplorerBase.__init__(self, CanvasFactoryTk(parent), dataset, primaryLabeling, secondaryLabeling)

class VennDiagram(VennDiagramBase):
  def __init__(self, set1, set2, set3=None, set1Name='', set2Name='', set3Name='', labeling=None, displayFunction=None, parent=None):
    VennDiagramBase.__init__(self, CanvasFactoryTk(parent), set1, set2, set3, set1Name, set2Name, set3Name, labeling, displayFunction)

class PCAGinzu(PCAGinzuBase):
  # NOTE, if you change the parameter list here, you'll need to change it in the
  # compClust.iplot.IPlot* modules, compClust.mlx.pcaGinzu, and PCAGinzu
  def __init__(self, dataset, nOutliers=None, outlierCutoff=None, sigCutoff = 0.05, 
               maxPCNum = None, verbose = False, rowPCAView = None, makeLabelings = True,
               primaryLabeling=None, secondaryLabeling=None, parent=None):
    PCAGinzuBase.__init__(self, CanvasFactoryTk(parent), dataset, nOutliers, outlierCutoff, sigCutoff, maxPCNum, verbose, rowPCAView, makeLabelings, primaryLabeling, secondaryLabeling)

    
