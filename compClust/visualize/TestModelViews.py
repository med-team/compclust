#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

"""
This provides some visualizations for model views

"""

# standard modules
import unittest
import os
import sys

# local modules 

from compClust.mlx.datasets import Dataset
from compClust.mlx.labelings import Labeling

#try:
from compClust.visualize import modelViews
#except:
#    pass


class testModelView(unittest.TestCase):

    def setUp(self):

        """
        constructs a dataset and a labeling then a
        distanceFromMeansModel from a file.

        I use a fairly small synthetic dataset for these purposes
        """
        datapath = os.path.join(os.environ['CODE_HOME'],'python/compClust/visualize/test')
        datasetName = os.path.join(datapath, 'synth_t_05c0_p_0075_d_10_v_0d5.txt')
        self.dataset = Dataset(datasetName)
        
        labelingName= os.path.join(datapath, 'clust_t_05c0_p_0075_d_10_v_0d5_a_reference.txt')
        self.labeling = Labeling(self.dataset)
        self.labeling.labelRows(labelingName)




    def checkBasicDistanceFromMeansModel3dView(self):

        print "\tchecking Basic functionality of DistanceFromMeansModel3dView.....",

        modelViews.distanceFromMeansModel3dView(self.dataset, self.labeling)

        print "done"

    def checkBasicMoGModelView(self):
            
        print "\tchecking Basic functionality of MoGModeView.....",

        modelViews.MoGModelView(self.dataset, self.labeling)
        
        print "done"


def suite():

    if sys.modules.has_key('visual'):
        print "Testing modelViews....",
        
        suite = unittest.TestSuite()
        suite.addTest(testModelView("checkBasicDistanceFromMeansModel3dView"))
        suite.addTest(testModelView("checkBasicMoGModelView"))
    
        print "done"

    else:
        print "Visual Module not installed....  skipping TestModelViews"
        suite = unittest.TestSuite()
        
    return(suite)
        


if __name__ == "__main__":
    unittest.main(defaultTest="suite")
    

        
