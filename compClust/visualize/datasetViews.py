########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

"""
This module contains visualization retuine which involve
projecting your data into PCA space

"""

# standard modules

# common modules
import Numeric
import MLab
import RandomArray
from scipy import plt


# mlx modules

from compClust.mlx import Dataset
from compClust.mlx import Labeling



def scatterPlot(dataset, labeling=None, dims=[0,1], labelsToPlot=None):

    """
    plot = plotPCADataset(dataset, labeling, dims=[0,1], labelsToPlot=None)


    constructs a scatter plot of the dataset coloring each class in
    the labeling a different color.  dims allows the specifications of
    the dimesions other than the first two to plot if the dataset
    constatins more that 2 dimensions

    labelsToPlot is a list of label class names which should be
    included in the plot.  If left s the default None all labels are
    plotted

    """

    classes = dataset.subsets(labeling)
    if not labelsToPlot:
        labelsToPlot = classes.keys()
    
    plotFrame = plt.figure()
    for label in labelsToPlot:
        data = classes[label].getData()
        plt.hold('on')
        plt.plot(data[:,dims[0]],data[:,dims[1]], 'o')
        
    plt.hold('off')
    
    return(plotFrame)






