#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

"""
Test Suite for the current Dataset schema.
"""
import unittest
import os
import inspect

import numpy

from compClust.util.DistanceMetrics import *

from compClust.mlx.datasets import Dataset
from compClust.mlx.labelings import Labeling
from compClust.mlx.labelings import subsetByLabeling

from compClust.mlx.views import RowSubsetView, ColumnSubsetView
from compClust.mlx.views import RowSupersetView, ColumnSupersetView
from compClust.mlx.views import SortedView
import compClust.mlx


#def read_testset(filename):
#  dataset = Dataset(test_data_files_home + "/" + filename)
#  return dataset
  
class testDataset2(unittest.TestCase):
  def setUp(self):
    self.datadir = os.path.split(inspect.getsourcefile(testDataset2))[0]

    dataset_spurious_filename = "dataset_tab_array_spurious_whitespace.txt"
    #self.original_dir    = os.getcwd()
    #os.chdir(compClust.mlx.__path__[0])

  #def tearDown(self):
  #  os.chdir(self.original_dir)

  def assertPass(self, func, args):

    try:
      apply(func, args)
      flag = 1
    except:
      flag = 0

    if not flag:
      self.fail("Function did not pass")

  def assertFail(self, func, args):

    try:
      apply(func, args)
      flag = 0
    except:
      flag = 1

    if not flag:
      self.fail("Function did not fail")
    
  def test_hard_subset(self):

    #
    # Test the view-subset-of-another-view's-labeling 
    #

    d1 = Dataset([[1,2,3],[4,5,6],[7,8,9],[10,11,12]])
    d2 = Dataset([[13,14,15],[16,17,18],[19,20,21],[22,23,24]])

    #
    # Make some supersets
    #

    s0 = RowSupersetView(d1,d1)
    s1 = ColumnSupersetView(d1,d2)
    s2 = ColumnSupersetView(d2,d1)
    s3 = RowSupersetView(d2,d2)

    #
    # Make some views
    #

    v1 = RowSubsetView(d1, [0,2,3])
    v2 = RowSubsetView(d1, [0,2,3])
    v3 = ColumnSubsetView(d2, [1,2])
    v4 = ColumnSubsetView(d2, [0,1])

    v5 = RowSubsetView(s0, [0,2,4,5,7])
    v6 = ColumnSubsetView(s1, [1,2,3,4])
    v7 = RowSubsetView(s2, [0,3])
    v8 = ColumnSubsetView(s3, [0,2])

    #
    # Try the simplest case: label between v1 and v2
    #

    l1 = Labeling(v1)
    l1.addLabelToRows("v1", [0,1,2])
    
    assert numpy.sum(
      numpy.sum(subsetByLabeling(v2, l1, "v1").getData() ==
                  numpy.array([[7,8,9]]), axis=0), axis=0) == 3

    l3 = Labeling(v3)
    l3.addLabelToCols("v3", [0,1])
    assert numpy.sum(
      numpy.sum(subsetByLabeling(v4, l3, "v3").getData() ==
                  numpy.array([[14],[17],[20],[23]]), axis=0), axis=0) == 4

   
  def test_dataset_torture(self):

    #
    # Test every method of the dataset
    #

    d1 = Dataset([[1,2,3],[4,5,6]])

    self.assertPass(d1.getRowData, [0])
    self.assertFail(d1.getRowData, [-1])
    self.assertFail(d1.getRowData, [2])
    
    self.assertPass(d1.getColData, [0])
    self.assertFail(d1.getColData, [-1])
    self.assertFail(d1.getColData, [3])

    self.assertPass(d1.getData, [])
    self.assertPass(d1.getData, [None])
    self.assertPass(d1.getData, [4])
    self.assertFail(d1.getData, [-1])
    self.assertFail(d1.getData, [5])

    self.assertPass(d1.getRowKey, [0])
    self.assertFail(d1.getRowKey, [-1])
    self.assertFail(d1.getRowKey, [2])
    
    self.assertPass(d1.getColKey, [0])
    self.assertFail(d1.getColKey, [-1])
    self.assertFail(d1.getColKey, [3])

    self.assertPass(d1.getRowKeys, [])
    self.assertPass(d1.getColKeys, [])
    self.assertPass(d1.getKeys, [0])
    self.assertPass(d1.getKeys, [1])
    self.assertPass(d1.getKeys, [-1])
    self.assertPass(d1.getKeys, [100])

    self.assertPass(d1.getViews, [])
    self.assertPass(d1.removeView, [None])

    self.assertPass(RowSubsetView, [d1, [0,1]])
    self.assertFail(RowSubsetView, [d1, [0,1,2]])
    self.assertFail(RowSubsetView, [d1, [-1,0,1]])

    self.assertPass(RowSubsetView, [d1, [0,1]])
    self.assertFail(RowSubsetView, [d1, [0,1,4]])
    self.assertFail(RowSubsetView, [d1,  [-1,0,1]])

    l1 = Labeling(d1)
    l2 = Labeling(d1)
    l3 = Labeling(d1)
    l4 = Labeling(d1)

    l2.addLabelToRows("foo", [0])
    l3.addLabelToCols("foo", [0,2])
    l4.addLabelToRows("foo", [1])
    l4.addLabelToCols("foo", [1,2])

    self.assertPass(subsetByLabeling, [d1, l1, "foo"])
    self.assertPass(subsetByLabeling, [d1, l2, "foo"])
    self.assertPass(subsetByLabeling, [d1, l3, "foo"])
    self.assertPass(subsetByLabeling, [d1, l4, "foo"])

    self.assertPass(RowSupersetView, [d1, d1])
    self.assertPass(ColumnSupersetView, [d1, d1])


    
  def test_superset(self):
    
    d1 = Dataset([[1,2],[3,4]])
    d2 = Dataset([[5,6],[7,8]])

    s1 = RowSupersetView(d1, d2)
    s2 = ColumnSupersetView(d1, d2)

    all1 = s1.getData()
    all2 = s2.getData()

    if numpy.sum(numpy.sum(all1 == numpy.array([[1.,2.],[3.,4.],[5.,6.],[7.,8.]]), axis=0), axis=0) != 8:
      self.fail("Row superset incorrect")

    if numpy.sum(numpy.sum(all2 == numpy.array([[1.,2.,5.,6.],[3.,4.,7.,8.]]), axis=0), axis=0) != 8:
      self.fail("Column superset incorrect")

    v = s1.getRowData(0)
    if numpy.sum(v == numpy.array([1., 2.]), axis=0) != 2:
      self.fail()
      
    v = s1.getRowData(1)
    if numpy.sum(v == numpy.array([3., 4.]), axis=0) != 2:
      self.fail()

    v = s1.getRowData(2)
    if numpy.sum(v == numpy.array([5., 6.]), axis=0) != 2:
      self.fail()
      
    v = s1.getRowData(3)
    if numpy.sum(v == numpy.array([7., 8.]), axis=0) != 2:
      self.fail()

    v = s1.getColData(0)
    if numpy.sum(v == numpy.array([1., 3., 5., 7.]), axis=0) != 4:
      self.fail()
      
    v = s1.getColData(1)
    if numpy.sum(v == numpy.array([2., 4., 6., 8.]), axis=0) != 4:
      self.fail()

    v = s2.getRowData(0)
    if numpy.sum(v == numpy.array([1., 2., 5., 6.]), axis=0) != 4:
      self.fail()
      
    v = s2.getRowData(1)
    if numpy.sum(v == numpy.array([3., 4., 7., 8.]), axis=0) != 4:
      self.fail()

    v = s2.getColData(0)
    if numpy.sum(v == numpy.array([1., 3.]), axis=0) != 2:
      self.fail()
      
    v = s2.getColData(1)
    if numpy.sum(v == numpy.array([2., 4.]), axis=0) != 2:
      self.fail()

    v = s2.getColData(2)
    if numpy.sum(v == numpy.array([5., 7.]), axis=0) != 2:
      self.fail()
      
    v = s2.getColData(3)
    if numpy.sum(v == numpy.array([6., 8.]), axis=0) != 2:
      self.fail()
      
  def test_subset(self):
    
    d    = Dataset([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

    l    = Labeling(d);
    l.addLabelToRows("foo", [0,2]);
    l.addLabelToCols("foo", [1,2]);

    view = subsetByLabeling(d, l, "foo")

    all = view.getData()
    if numpy.sum(numpy.sum(all == numpy.array([[2.,3.],[8.,9.]]), axis=0), axis=0) != 4:
      self.fail("Labeling subset getData() incorrect")

    r = view.getKeys()
    c = view.getKeys(1)

    v = view.getData(r[0])
    if numpy.sum(v == numpy.array([2., 3.]), axis=0) != 2:
      self.fail("View Row 0 incorrect")

    v = view.getData(r[1])
    if numpy.sum(v == numpy.array([8., 9.]), axis=0) != 2:
      self.fail("View Row 1 incorrect") 

    v = view.getData(c[0])
    if numpy.sum(v == numpy.array([2., 8.]), axis=0) != 2:
      self.fail("View Col 0 incorrect")

    v = view.getData(c[1])
    if numpy.sum(v == numpy.array([3., 9.]), axis=0) != 2:
      self.fail("View Col 1 incorrect")


  def test_col_subset(self):
    
    d    = Dataset([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    view = ColumnSubsetView(d, [2,1])

    all = view.getData()
    if numpy.sum(numpy.sum(all == numpy.array([[3., 2.],[6., 5.], [9., 8.]]), axis=0), axis=0) != 6:
      self.fail("Column subset getData() incorrect")

    r = view.getKeys()
    c = view.getKeys(1)

    v = view.getData(r[0])
    if numpy.sum(v == numpy.array([3., 2.]), axis=0) != 2:
      self.fail("View Row 0 incorrect")

    v = view.getData(r[1])
    if numpy.sum(v == numpy.array([6., 5.]), axis=0) != 2:
      self.fail("View Row 1 incorrect") 

    v = view.getData(r[2])
    if numpy.sum(v == numpy.array([9., 8.]), axis=0) != 2:
      self.fail("View Row 2 incorrect")

    v = view.getData(c[0])
    if numpy.sum(v == numpy.array([3., 6., 9.]), axis=0) != 3:
      self.fail("View Col 0 incorrect")

    v = view.getData(c[1])
    if numpy.sum(v == numpy.array([2., 5., 8.]), axis=0) != 3:
      self.fail("View Col 1 incorrect")

  def test_multi_label_subset(self):
    
    d = Dataset([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12], [13, 14, 15]])

    
    l1 = Labeling(d)
    l1.addLabelToRows("x", [0,1])
    l1.addLabelToRows("y", [1,2])
    l1.addLabelToCols("z", [0,2])
    l1.addLabelToCols(0.1, [0,2])

    view = subsetByLabeling(d, l1, "x")
    all  = view.getData()
    
    if numpy.sum(numpy.sum(all == numpy.array([[1.,2.,3.],[4.,5.,6.]]), axis=0), axis=0) != 6:
      self.fail("Multi label failed first test")

    
    view = subsetByLabeling(d, l1, ["x", "y"])
    all  = view.getData()

    if numpy.sum(numpy.sum(all == numpy.array([[1.,2.,3.],[4.,5.,6.],[7.,8.,9.]]), axis=0), axis=0) != 9:
      self.fail("Multi label failed second test")
                   
    view = subsetByLabeling(d, l1, ["x", "z"])
    all  = view.getData()

    if numpy.sum(numpy.sum(all == numpy.array([[1.,3.],[4.,6.]]), axis=0), axis=0) != 4:
      self.fail("Multi label failed third test")

    view = subsetByLabeling(d, l1, ["x", 0.1])
    all  = view.getData()

    if numpy.sum(numpy.sum(all == numpy.array([[1.,3.],[4.,6.]]), axis=0), axis=0) != 4:
      self.fail("Multi label failed fourth test")
    
  def test_row_subset(self):
    
    d    = Dataset([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    view = RowSubsetView(d, [2,1])

    all = view.getData()
    if numpy.sum(numpy.sum(all == numpy.array([[7.,8.,9.],[4.,5.,6.]]), axis=0), axis=0) != 6:
      self.fail("Row subset getData() incorrect")

    r = view.getKeys()
    c = view.getKeys(1)

    v = view.getData(r[0])
    if numpy.sum(v == numpy.array([7., 8., 9.]), axis=0) != 3:
      self.fail("View Row 0 incorrect")

    v = view.getData(r[1])
    if numpy.sum(v == numpy.array([4., 5., 6.]), axis=0) != 3:
      self.fail("View Row 1 incorrect") 

    v = view.getData(c[0])
    if numpy.sum(v == numpy.array([7., 4.]), axis=0) != 2:
      self.fail("View Col 0 incorrect")

    v = view.getData(c[1])
    if numpy.sum(v == numpy.array([8., 5.]), axis=0) != 2:
      self.fail("View Col 1 incorrect")

    v = view.getData(c[2])
    if numpy.sum(v == numpy.array([9., 6.]), axis=0) != 2:
      self.fail("View Col 2 incorrect")

  def test_sort(self):

    # Obsoleted by SortedViews

    d = Dataset([[1,2,3],[0,-1,0],[7,8,9],[0,1,0],[-1,3,2],[9,6,-3],[1,2,1],[5,6,7]])
    v = SortedView(d)
    origin = numpy.array([[0,0,0]])

    l = Labeling(v)
    l.addLabelToRows("foo",[0,2,5])

    d0 = v.getRowData(0)
    d1 = v.getRowData(5)
    d2 = v.getRowData(2)

    v.sort(lambda x : EuclideanDistance(x, origin))

    h = l.getRowsByLabel("foo")
    self.failUnless(numpy.sum(v.getRowData(h[0]) == d0, axis=0)==3)
    self.failUnless(numpy.sum(v.getRowData(h[1]) == d1, axis=0)==3)
    self.failUnless(numpy.sum(v.getRowData(h[2]) == d2, axis=0)==3)
    

  def test_label_using(self):

    d  = Dataset([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
    v1 = RowSubsetView(d, [0,1,2])
    v2 = RowSubsetView(d, [1,2,3])
    v3 = RowSubsetView(d, [3])

    l  = Labeling(d)
    l.labelCols([1,2,3])
    
    l1 = Labeling(v1)
    l1.addLabelToRows("foo",[0,1,2])
    l2 = Labeling(v2)
    l2.labelFrom(l1)
    self.failUnless(l2.getRowsByLabel("foo") == [0,1])

    l3 = Labeling(v3)
    l3.labelFrom(l1)
    self.failUnless(l3.getRowsByLabel("foo") == [])

    # Make sure the columns labels come through OK
    lu = Labeling(v1)
    lu.labelFrom(l)
    self.failUnless(lu.getLabels() == [1,2,3])
    
  def test_keys(self):

    #
    # Checks for keys and how ranges are handled
    #

    d = Dataset([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

    r = d.getKeys()
    c = d.getKeys(1)

    #
    # Try getting the data with the keys
    #

    v = d.getData(r[0])
    if numpy.sum(v == numpy.array([1., 2., 3.]), axis=0) != 3:
      self.fail("Row 0 incorrect")

    v = d.getData(r[1])
    if numpy.sum(v == numpy.array([4., 5., 6.]), axis=0) != 3:
      self.fail("Row 1 incorrect") 

    v = d.getData(r[2])
    if numpy.sum(v == numpy.array([7., 8., 9.]), axis=0) != 3:
      self.fail("Row 2 incorrect")

    v = d.getData(c[0])
    if numpy.sum(v == numpy.array([1., 4., 7.]), axis=0) != 3:
      self.fail("Col 0 incorrect")

    v = d.getData(c[1])
    if numpy.sum(v == numpy.array([2., 5., 8.]), axis=0) != 3:
      self.fail("Col 1 incorrect")

    v = d.getData(c[2])
    if numpy.sum(v == numpy.array([3., 6., 9.]), axis=0) != 3:
      self.fail("Col 2 incorrect")
    
    try:
      v = d.getData(-1)
      self.fail("Dataset allowed negative key")
    except:
      pass

    try:
      v = d.getData(10)
      self.fail("Dataset allowed too large key")
    except:
      pass
    
  def test_is_row_col_key(self):
    """Does the isRowKey and isColKey work correctly?"""
    d = Dataset([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

    r = d.getKeys()
    c = d.getKeys(1)

    for k in d.getRowKeys():
      self.failUnless(d.isRowKey(k))
      self.failIf(d.isColKey(k))
    
    for k in d.getColKeys():
      self.failIf(d.isRowKey(k))
      self.failUnless(d.isColKey(k))
    
    self.failIf(d.isRowKey(-1))
    self.failIf(d.isColKey(-1))
    
  def test_labeling(self):

    #
    # Verifies that labeling do not get confused
    # 

    d1 = Dataset([[1,2,3],[4,5,6],[7,8,9]])
    d2 = Dataset([[10,11,12],[13,14,15],[16,17,18]])
    
    s1 = RowSubsetView(d1, [1,2])
    s2 = RowSubsetView(d1, [0,1])
    s3 = RowSubsetView(d2, [1,2])
    s4 = RowSubsetView(d2, [0,1])

    s5 = RowSupersetView(s2, s3)

    l1 = Labeling(s1)
    l2 = Labeling(s2)
    l3 = Labeling(s3)
    l4 = Labeling(s4)
    l5 = Labeling(s5)

    # use strings in labeling 1
    l1.addLabelToRows('X', [0,1])

    # Use ints in labeling 2
    l2.addLabelToRows(2, [0,1])
      
  def test_constructor(self):

    #
    # Check the all the variations of args pass and others fail
    #
    
    ref = numpy.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]], float)

    d = Dataset([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    if numpy.sum(numpy.sum(d.getData() == ref, axis=0), axis=0) != 9:
      self.fail("Did not contruct from list of lists correctly")

    d = Dataset(((1, 0, 0), (0, 1, 0), (0, 0, 1)))
    if numpy.sum(numpy.sum(d.getData() == ref, axis=0), axis=0) != 9:
      self.fail("Did not contruct from tuples of tuples correctly")

    d = Dataset(os.path.join(self.datadir,"DStest.txt"))
    if numpy.sum(numpy.sum(d.getData() == ref, axis=0), axis=0) != 9:
      self.fail("Did not contruct from filename correctly") 

    stream = open(os.path.join(self.datadir,"DStest.txt"))
    d = Dataset(stream)
    stream.close()
    if numpy.sum(numpy.sum(d.getData() == ref, axis=0), axis=0) != 9:
      self.fail("Did not contruct from I/O stream correctly") 

    d = Dataset(ref)
    if numpy.sum(numpy.sum(d.getData() == ref)) != 9:
      self.fail("Did not contruct from numpy array correctly") 

    d2 = Dataset(d)
    if numpy.sum(numpy.sum(d2.getData() == ref, axis=0), axis=0) != 9:
      self.fail("Did not contruct from Dataset correctly")

    self.assertFail(Dataset, [{}])
    self.assertFail(Dataset, [None])

    #
    # Check the anomalous dataset handling (NaN, Tabs)
    #

    base = numpy.ma.array([[1,2,3],[4,5,6],[7,8,9],[10,11,12],[13,14,15],
                           [16,17,18], [19,20,21],[22,23,24]])
    
    ds3 = Dataset(os.path.join(self.datadir,"dataset-NaNs.txt"))
    ds4 = Dataset(os.path.join(self.datadir,"dataset-Tabs.txt"))
    ma = numpy.ma
    ds3sum = ma.sum(ma.sum(numpy.asarray(ds3.getData() == base, int), 0))
    ds4sum = ma.sum(ma.sum(numpy.asarray(ds4.getData() == base, int), 0))
    ds3mask = ma.sum(ma.sum(ma.getmaskarray(ds3.getData()), axis=0))
    ds4mask = ma.sum(ma.sum(ma.getmaskarray(ds4.getData()), axis=0))

    if (ds3sum + ds3mask != 24):
      self.fail("Did not read NaN data correctly")
    if (ds4sum + ds4mask != 24):
      self.fail("Did not read NaN data correctly")  

  def test_delimiter(self):
    ds_comma = Dataset(os.path.join(self.datadir,"dataset-full-comma.txt"), delimiter=",")
    ds_spaces = Dataset(os.path.join(self.datadir,"dataset-full-spaces.txt"), delimiter=" +")
    self.failUnless(numpy.allclose(ds_spaces.getData(), ds_comma.getData()))
    
  def test_mixin(self):
    class TestMixin:
      def testMixin(self):
        return "mixin"
      
    d1 = Dataset([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    d2 = Dataset([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    
    self.failIf('testMixin' in dir(d1))
    d1.addMixin(TestMixin)
    self.failUnless(d1.testMixin() == TestMixin().testMixin())

    v1 = RowSubsetView(d1, [1])
    v2 = RowSubsetView(d2, [2])
    self.failUnless(v1.testMixin() == TestMixin().testMixin())
    self.failIf('testMixin' in dir(v2))
    
    d1.removeMixin(TestMixin)
    self.failIf('testMixin' in dir(d1))

  def test_primary_labelings(self):
    """Make sure that the primary row and column labelings behave as expected

    They can only be labelings or none, and they must be unique in the axis specified.
    """
    d1 = Dataset([[1,2,3],[4,5,6],[7,8,9],[10,11,12]])

    # test row labelings
    row_label_duplicate = Labeling(d1)
    row_label_duplicate.labelRows(["row"] * 4)
    row_label_unique = Labeling(d1)
    row_label_unique.labelRows(["one", "two", "three", "four"])
    # make sure we received a  labeling
    self.failUnlessRaises(ValueError, d1.setPrimaryRowLabeling, 3)
    # make sure we are unqiue
    self.failUnlessRaises(ValueError, d1.setPrimaryRowLabeling, row_label_duplicate)
    d1.setPrimaryRowLabeling(row_label_unique)
    self.failUnless(d1.getPrimaryRowLabeling() == row_label_unique)
    d1.setPrimaryRowLabeling(None)
    
    # test column labelings
    column_label_duplicate = Labeling(d1)
    column_label_duplicate.labelCols(["col"]*3)
    column_label_unique = Labeling(d1)
    column_label_unique.labelCols(["one", "two", "three"])
    # make sure we received a  labeling
    self.failUnlessRaises(ValueError, d1.setPrimaryColumnLabeling, 3)
    # make sure we are unqiue
    self.failUnlessRaises(ValueError, d1.setPrimaryColumnLabeling, column_label_duplicate)
    d1.setPrimaryColumnLabeling(column_label_unique)
    self.failUnless(d1.getPrimaryColumnLabeling() == column_label_unique)
    d1.setPrimaryColumnLabeling(None)
    
  def test_setviewdefault(self):
    """Test to make sure that setViewDefault will return a
    previously created instance of an attached view.
    """

    d1 = Dataset([[1,2,3],[4,5,6],[7,8,9],[10,11,12]])
    # start with no views
    self.failUnless(len(d1.getViews()) == 0, "Start with no views")

    # add a couple of anonymous views
    s1 = RowSubsetView(d1, [1,2])
    s2 = RowSubsetView(d1, [0,1])
    self.failUnless(len(d1.getViews()) == 2, "Should now have 2 anonymous views")

    # add a named view via setViewDefault
    s3 = d1.addViewDefault('subset1', RowSubsetView, d1, [1,2])
    self.failUnless(len(d1.getViews()) == 3, "Should now have 3 views")

    s4 = d1.addViewDefault('subset1', RowSubsetView, d1, [1,2])
    self.failUnless(len(d1.getViews()) == 3, "Should still have 3 views")
    self.failUnless(s3 == s4, "and they should return the same instance")

    # of course now we have a new problem of potential name collision
    # so we should not allow  names to be reused (This might break old code)
    # (well anonymous ones can get through)
    s5 = RowSubsetView(d1,[1,2])
    self.failUnlessRaises(KeyError, s5.setName, 'subset1')

    # we should allow names to be repeated at different levels of the
    # hierarchy
    s6 = RowSubsetView(s5, [0])
    s6.setName('subset1')

    s7 = s5.addViewDefault('subset2', RowSubsetView, s5, [0,1])
    self.failUnless(s7 == s5.addViewDefault('subset2', RowSubsetView, s5, [3]),
                    "addViewDefault should return the same instance")
    
    # make sure that even in subviews we're testing names correctly
    s8 = RowSubsetView(s5, [0,1])
    self.failUnlessRaises(KeyError, s8.setName, 'subset2')


  def testSplitRowColKeylist(self):
    """Split row col keylist splits a list of keys into lists ofrow and 
    column keys
    """
    d1 = Dataset([[1,2,3],[4,5,6],[7,8,9],[10,11,12]])
    d1_row_keys = d1.getRowKeys()
    d1_col_keys = d1.getColKeys()
    
    row_keys, col_keys = d1.splitRowColKeylist(d1_row_keys+d1_col_keys)
    self.failUnless(row_keys == d1_row_keys)
    self.failUnless(col_keys == d1_col_keys)
    
    row_keys, col_keys = d1.splitRowColKeylist(d1_row_keys)
    self.failUnless(row_keys == d1_row_keys)
    self.failUnless(col_keys == [])
    
    row_keys, col_keys = d1.splitRowColKeylist(d1_col_keys)
    self.failUnless(row_keys == [])
    self.failUnless(col_keys == d1_col_keys)
    
    row_keys, col_keys = d1.splitRowColKeylist([])
    self.failUnless(row_keys == [])
    self.failUnless(col_keys == [])
    
    max_key = d1.getNumRows() + d1.getNumCols()
    self.failUnlessRaises(ValueError, d1.splitRowColKeylist, [max_key + 1])
    

def suite(**kw):
  suite = unittest.makeSuite(testDataset2)
  return suite

if __name__ == "__main__":
  unittest.main(defaultTest="suite")

