########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
# Last Modified: 30-May-2002, 11:00
#

from Dataset import Dataset

class PagedDataset(Dataset):
  """
  Implements a disk-based dataset

  The PagedDataset takes a file name as it's initialized and will go to
  disk to retrieve data as requested.  This allows for arbitrary sized
  datasets to be used while greatly reducing memory pressure.  *This
  class is not yet implemented*
  """
  
