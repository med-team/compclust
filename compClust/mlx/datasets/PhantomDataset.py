########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
# Last Modified: 30-May-2002, 11:00
#

import numpy

from compClust.mlx.datasets.Dataset import Dataset

class PhantomDataset(Dataset):
  """
  Provides a virtual dataset of an arbitrary size.

  Creates a dataset of numRows and numCols, but does not store any data.
  The getData() method only returns zeros.  This is the /dev/zero of the
  schema.  The utility of such a dataset is that if one is only intertested
  in manipulating labelings, then they may be attached to this dataset without
  the need to use up memory for any real data.
  """

  def __init__(self, numRows = 0, numCols = 0):

    self.numRows = numRows
    self.numCols = numCols

    self.key2lab = {}
    self.lab2key = {}
    self.counter = 0
    self.dirty   = 0
    
    self.resetVars()
    self.name = None

  def getData(self, key=None):
    numRows = self.getNumRows()
    numCols = self.getNumCols()
    if key is None:
      return numpy.zeros((numRows, numCols))
    else:
      keyMax = self.getKeyMax()
      if key < 0 or key >= keyMax:
        raise ValueError()
      if key < numRows:
        return numpy.zeros(numCols)
      else:
        return numpy.zeros(numRows)




