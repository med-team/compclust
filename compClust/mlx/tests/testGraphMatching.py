#!/usr/bin/env python
"""Test suite for the graph matching (AKA wmatch) code
"""

import os
import unittest

import numpy

import compClust.mlx.graphmatching as graphmatching

class testGraphMatching(unittest.TestCase):

  def test1(self):
    a = numpy.array(
      [[29, 20,  1,  3],
       [9,  6, 15,  3],
       [20, 10,  2,  1],
       [4, 17,  8, 20],])
    matches = [[0,1,0,0],
               [0,0,1,0],
               [1,0,0,0],
               [0,0,0,1]]
    score, best_matches = graphmatching.match(a)
    self.failUnlessAlmostEqual(score, 0.4464, places=4)
    self.failUnless(numpy.all(numpy.equal(best_matches, matches)))

  def test2(self):
    a = numpy.array(
      [[29, 20,  1,  3, 4 ],
       [ 9,  6, 15,  3, 7 ],
       [20, 10,  2,  1, 11],
       [ 4, 17,  8, 20, 40],])
    matches = [[0,1,0,0,0],
               [0,0,1,0,0],
               [1,0,0,0,0],
               [0,0,0,0,1]]
    score, best_matches = graphmatching.match(a)
    self.failUnlessAlmostEqual(score, 0.4130, places=4)
    self.failUnless(numpy.all(numpy.equal(best_matches, matches)))

  def test3(self):
    a = numpy.array(
      [[ 1,  2,  3],
       [ 4,  5,  6],
       [ 7,  8,  9],
       [10, 11, 12],
       [13, 14, 15],])
    matches = [[0,0,0],
               [0,0,0],
               [1,0,0],
               [0,1,0],
               [0,0,1]]
    score, best_matches = graphmatching.match(a)
    self.failUnlessAlmostEqual(score, 0.2750, places=4)
    self.failUnless(numpy.all(numpy.equal(best_matches, matches)))

  def test4(self):
    a = numpy.array(
      [[ 1,  2,  3],
       [ 4,  5,  6],
       [ 7,  8,  9],])
    matches = [[1,0,0],
               [0,1,0],
               [0,0,1]]
    score, best_matches = graphmatching.match(a)
    self.failUnlessAlmostEqual(score, 0.3333, places=4)
    self.failUnless(numpy.all(numpy.equal(best_matches, matches)))

  def test5(self):
    a = numpy.identity(5, dtype=int)
    matches = numpy.identity(5, dtype=int)
    score, best_matches = graphmatching.match(a)
    self.failUnlessAlmostEqual(score, 1.0)
    self.failUnless(numpy.all(numpy.allclose(best_matches, matches)))

  def test6(self):
    """
    We allow for passing in a list of lists, test to make sure it works.
    """
    a = [ [ 1, 0, 0, 0, 0 ], 
          [ 0, 1, 0, 0, 0 ], 
          [ 0, 0, 1, 0, 0 ],
          [ 0, 0, 0, 1, 0 ],
          [ 0, 0, 0, 0, 1 ] ]
    matches = numpy.identity(5, dtype=int)
    score, best_matches = graphmatching.match(a)
    self.failUnlessAlmostEqual(score, 1.0)
    self.failUnless(numpy.all(numpy.allclose(best_matches, matches)))

def suite(**kw):
  suite = unittest.TestSuite()
  suite.addTest(unittest.makeSuite(testGraphMatching))
  return suite
  
if __name__ == "__main__":
  tests = suite()
  unittest.TextTestRunner(verbosity=2).run(tests)
