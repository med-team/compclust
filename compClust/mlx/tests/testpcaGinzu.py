#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

"""
Test suite for the pcaGinzu class

"""

import unittest
import os
import random
import string

from compClust.mlx.pcaGinzu import pcaGinzu
from compClust.util.LoadExample import LoadCho

class testPCAGinzu(unittest.TestCase):
  def setUp(self):
    self.dataset = LoadCho()
    self.common_names = self.dataset.getLabeling('common name')
    self.cho_clustering = self.dataset.getLabeling('cho clusters')
    self.diagem_clustering = self.dataset.getLabeling('diagem clusters')

  def tearDown(self):
    pass

  def testPcaGinzuFixedOutliers(self):
    noutliers = 10
    ginzu = pcaGinzu(self.dataset, nOutliers=noutliers)
    
    for pcn in xrange(ginzu.maxPCNum):
      high_low = ginzu._getHighLowLabelingByPCN(pcn+1)
      self.failUnless(len(high_low.getRowsByLabel('high')) == noutliers)
      self.failUnless(len(high_low.getRowsByLabel('low')) == noutliers)
      
def suite(**kw):
  return unittest.makeSuite(testPCAGinzu)
      
if __name__ == "__main__":
  unittest.main(defaultTest="suite")
  
