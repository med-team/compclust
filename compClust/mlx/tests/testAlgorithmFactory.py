#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

"""
Test suite for the AlgorithmFactory module.
"""

import unittest
import os

from compClust.mlx.AlgorithmFactory import newInstanceByName, \
                                           newInstanceByType
class testAlgorithmFactory(unittest.TestCase):
  def setUp(self):
    pass

  def check_foo(self, class_name):
    a = newInstanceByName(class_name)
    b = newInstanceByType(a)
    assert type(a) == type(b)
    
  def test_production(self):

    self.check_foo("DiagEM")
    self.check_foo("FullEM")
    self.check_foo("Hierarchical")
    self.check_foo("HutSOM")
    self.check_foo("KMeans")
    self.check_foo("MCCV")
    self.check_foo("Monkey")
    self.check_foo("TSplit")
    self.check_foo("XClust")
    
def suite(**kw):
  suite = unittest.TestSuite()
  suite.addTest(testAlgorithmFactory("test_production"));
  return suite

if __name__ == "__main__":
  unittest.main(defaultTest="suite")
 





