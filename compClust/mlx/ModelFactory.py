########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

"""
Provides a factory for vending instances of Model objects from different
ML_Algorithm instances.
"""

import types

from compClust.mlx.interfaces import IML_Algorithm
from compClust.mlx.models import *

def getMoGModel(algorithm):

  from compClust.mlx.wrapper import *
  
  if algorithm is None or type(algorithm) != types.ClassType:
    return None
  elif not isinstance(algorithm, IML_Algorithm):
    return None
  elif isinstance(algorithm, DiagEM):
    return algorithm.getModel()
  elif isinstance(algorithm, FullEM):
    return algorithm.getModel()
  elif isinstance(algorithm, HutSOM):
    dataset = algorithm.dataset
    labeling = algorithm.getLabeling()
    return constructMixtureOfGaussiansFromLabeling(dataset, labeling)
  elif isinstance(algorithm, KMeans):
    dataset = algorithm.dataset
    labeling = algorithm.getLabeling()
    return constructMixtureOfGaussiansFromLabeling(dataset, labeling)
  elif isinstance(algorithm, XClust):
    raise NotImplementedError("XClust doesn't have a MoG represenation")

def getEstMoGModel(algorithm):

  if algorithm is None or type(algorithm) != types.ClassType:
    return None
  elif not isinstance(algorithm, IML_Algorithm):
    return None
  dataset = algorithm.dataset
  labeling = algorithm.getLabeling()
  return constructMixtureOfGaussiansFromLabeling(dataset, labeling)

def getDistanceFromMeanModel(algorithm):

  from compClust.mlx.wrapper import *

  if algorithm is None or type(algorithm) != types.InstanceType:
    return None
  elif not isinstance(algorithm, IML_Algorithm):
    return None
  elif isinstance(algorithm, DiagEM):
    dataset = algorithm.dataset
    labeling = algorithm.getLabeling()
    return DistanceFromMean(data=dataset, labels=labeling)
  elif isinstance(algorithm, FullEM):
    dataset = algorithm.dataset
    labeling = algorithm.getLabeling()
    return sodels.DistanceFromMean(data=dataset, labels=labeling)
  elif isinstance(algorithm, HutSOM):
    return algorithm.getModel()
  elif isinstance(algorithm, KMeans):
    return algorithm.getModel()
  elif isinstance(algorithm, XClust):
    raise NotImplementedError("XClust doesn't have a MoG represenation")
  else:
    print algorithm
    raise ValueError("Unrecognized algorithm type")

  
