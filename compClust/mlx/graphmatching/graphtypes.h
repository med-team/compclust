/*
 * This code was downloaded from  
 * ftp://dimacs.rutgers.edu/pub/netflow/matching/weighted/solver-1
 * and is presumed to be in the public domain
 */
#ifndef _GRAPHTYPES_H_
#define _GRAPHTYPES_H_
#ifndef INF
#define INF	100000000
#endif

#ifndef NULL
#define NULL	0
#endif

struct node_entry {
    int degree;
    int label;
    int x;
    int y;
    struct edge_ent *adj_list;
    };
typedef struct node_entry *Graph;

struct edge_ent {
    int endpoint;
    int label;
    int label2;
    struct edge_ent *nextedge;
    struct edge_ent *prevedge;
    struct edge_ent *otheredge;
    };
typedef struct edge_ent *Edge;

void SetUp(Graph g, int type);

Graph ReadGraph(int *size, char *file);
Graph NewGraph(int size);
Graph CopyGraph(Graph g);

void AddEdge(Graph g, int n, int m, int label);
int RemoveEdge(Graph graph, Edge edge);
int NumEdges(Graph g);
Edge FindEdge(Graph graph, int i, int j);

#define Degree(graph,n)    (graph[n].degree)
#define NLabel(graph,n)    (graph[n].label)
#define Xcoord(graph,n)    (graph[n].x)
#define Ycoord(graph,n)    (graph[n].y)
#define FirstEdge(graph,n) (graph[n].adj_list)

#define EndPoint(e) (e->endpoint)
#define ELabel(e)   (e->label)
#define ELabel2(e)  (e->label2)
#define Other(e)    (e->otheredge)
#define NextEdge(e) (e->nextedge)

int *Weighted_Match(Graph gptr, int type, int maximize);

/* Euclidean graph type */
typedef int (*EuclidGraph)[2];

EuclidGraph ReadEuclid(int *size, char file[]);
EuclidGraph NewEuclid(int size);
int eucdist(EuclidGraph graph, int i, int j);
int eucdistsq(EuclidGraph graph, int i, int j);
int eucdist2 (EuclidGraph graph, int i, int j);

/* Distance matrix graph type */
typedef int *MatrixGraph;

MatrixGraph NewMatrix(int size);
MatrixGraph ReadMatrix(int *size, char file[]);
void WriteMatrix (MatrixGraph graph, char file[]);


#endif
