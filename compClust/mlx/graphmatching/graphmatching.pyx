"""
Experiment with embedding Python via Pyrex
"""
# get stuff we need from C header files

cdef extern from "Python.h":
  pass

cdef extern from "numpy/ndarrayobject.h":
  struct PyArray_Descr:
    int type_num, elsize
    char type

  ctypedef class numpy.ndarray [object PyArrayObject]:
    cdef char *data
    cdef int nd
    cdef int *dimensions, *strides
    cdef object base
    cdef PyArray_Descr *descr
    cdef int flags


cdef extern from "stdio.h":

  int printf(char *format,...)

cdef extern from "graphtypes.h":
  cdef struct edge_ent

  cdef struct node_entry:
    int degree
    int label
    int x
    int y
    edge_ent *adj_list
  ctypedef node_entry *Graph

  cdef struct edge_ent:
    int endpoint
    int label
    int label2
    edge_ent *nextedge
    edge_ent *prevedge
    edge_ent *otheredge
  ctypedef edge_ent *Edge

  cdef extern void AddEdge(Graph g, int n, int m, int label)
  cdef extern Graph NewGraph(int size)
  cdef extern Graph ReadGraph(int *size, char *file)
  #cdef extern Graph Conf2Graph(int *size, char file[], int *weight)
  cdef extern int *Weighted_Match(Graph gprt, int type, int maximize)
  cdef extern int NumEdges(Graph g)

  # Degree(graph,n) -> graph[n].degree
  # FirstEdge(graph,n) -> graph[n].adj_list
  # NextEdge(e) -> e->nextedge
  # EndPoint(e) -> e->endpoint
  # Elabel(e) -> e->label

# IMPORTANT - we need to explicitly prototype the function
# 'init<mymodulename>()', where 'mymodulename' is the
# filename this code resides in. I called the file 'testpyx.pyx',
# hence the name below

cdef public void initgraphmatching()

# With all this out of the way, we can declare any amount of
# Pyrex python extension types, and regular python classes/functions

cdef Graph num2graph(int *size, ndarray array, long *total_weight):
  #Convert numeric matrix to graph
  
  cdef int edges
  cdef Graph graph

  cdef int i
  cdef int row
  cdef int col
  cdef int nrows
  cdef int ncols
  cdef long weight
  cdef long *data

  if array.nd != 2:
    raise ValueError("Array must be 2 dimensional")

  # should check to make sure array is of integers

  nrows = array.dimensions[0]
  ncols = array.dimensions[1]
  size[0] =  nrows + ncols
  edges = nrows * ncols

  # initialize graph structure
  graph = NewGraph(size[0])

  for i from 1 <= i <= size[0]:
    graph[i].label = i
    graph[i].x = 0
    graph[i].y = 0
  total_weight[0] = 0

  data = <long *>array.data
  
  # load total_weights
  for row from 1 <= row <= nrows:
    for col from 1 <= col <= ncols:
      weight = data[(row-1) * ncols + (col-1)]
      total_weight[0] = total_weight[0] + weight
      AddEdge(graph, row, col+nrows, weight)

  return graph

cdef weighted_match(int *size, Graph graph, long *total_weight):
  cdef int *Mate
  cdef int i
  cdef int j
  cdef int score
  cdef int edges
  cdef Edge p
  score = 0

  Mate = Weighted_Match(graph, 1, 1)

  best_match = []
  for i from 1 <= i <= size[0]:
    best_match.append(Mate[i])
    
  size[0] = graph[0].degree
  edges = NumEdges(graph)

  for i from 1 <=i <= size[0]:
    p = graph[i].adj_list
    for j from 1 <= j <= graph[i].degree:
      if p.endpoint == Mate[i]:
        score = score + p.label
      p = p.nextedge

  return (score/(2.0*total_weight[0]), best_match)

  
import types
import numpy

def makeAdjacencyMatrix(matrix_ref, best_matches):
  cdef int rows
  cdef int cols
  cdef int i
  rows = matrix_ref.shape[0]
  cols = matrix_ref.shape[1]

  result = numpy.zeros((rows, cols))

  for i from 1 <= i <= rows:
    x,y = i, (best_matches[i-1])

    if y == 0:
      continue
    if x > rows:
      x = x - rows
    if y > rows:
      y = y - rows
    result[x-1, y-1] = 1

  return result

def match(matrix_ref):
  #Compute the score from doing graph matching

  cdef Graph graph
  cdef int size
  cdef long total_weight

  # If matrix_ref is a list, convert it to a matrix.
  if type(matrix_ref) is types.ListType:
     matrix_ref = numpy.array(matrix_ref)

  # Matrix must be of type int
  if matrix_ref.dtype.kind != 'i':
     msg = "matrix is of type %s, requires integer type" \
           % (matrix_ref.dtype.name)
     raise ValueError(msg)
 
  # try to make sure we have a numeric array 
  matrix_ref = numpy.asarray(matrix_ref)
  graph = num2graph(&size, matrix_ref, &total_weight)

  score, best_match = weighted_match(&size, graph, &total_weight)
  adjacency_matrix = makeAdjacencyMatrix(matrix_ref, best_match)
  return score, adjacency_matrix


