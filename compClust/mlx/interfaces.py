########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
# Last Modified: 24-Oct-2001, 11:00
#

"""
Contains all the interface classes for all schema objects.
"""

import sys
from compClust.util.Mixin import Mixin

#
# IDataset
#

class IDataset(Mixin):
  """
  Interface class to the Dataset class.

  Every dataset-derived class must implement all of the methods listed.
  """
  
  def __init__(self, data):
    raise NotImplementedError()

  def writeDataset(self, stream=sys.stdout, delimiter="\t"):
    raise NotImplementedError()

  def getRowData(self, row):
    raise NotImplementedError()

  def getColData(self, col):
    raise NotImplementedError()

  def getName(self):
    raise NotImplementedError()

  def setName(self, name):
    raise NotImplementedError()

  def getData(self, key=None):
    raise NotImplementedError()

  def getRowKey(self, row):
    raise NotImplementedError()

  def getColKey(self, col):
    raise NotImplementedError()

  def getRowKeys(self):
    raise NotImplementedError()

  def getColKeys(self):
    raise NotImplementedError()
  
  def getKeys(self, axis=0):
    raise NotImplementedError()

  def getNumCols(self):
    raise NotImplementedError()

  def getNumRows(self):
    raise NotImplementedError()

  def getNumAxis(self, axis=0):
    raise NotImplementedError()

  def getView(self, name):
    raise NotImplementedError()

  def getViews(self):
    raise NotImplementedError()

  def addView(self, view):
    raise NotImplementedError()

  def removeView(self, view):
    raise NotImplementedError()
  
  def removeLabeling(self, labeling):
    raise NotImplementedError()

  def getLabeling(self, name):
    raise NotImplementedError()

  def getLabelings(self):
    raise NotImplementedError()

  def isDirty(self):
    raise NotImplementedError()

  def getLineage(self):
    raise NotImplementedError()

  def labelFrom(self, labeling):
    raise NotImplementedError()
  
  def _refresh(self):
    raise NotImplementedError()

  def _mapUIDToParent(self, uid, parent=None):
    raise NotImplementedError()

  def _mapKeysFromParent(self, key, parent=None):
    raise NotImplementedError()

  def _getKeysByUID(self, uid):
    raise NotImplementedError()

  def _getUIDsByKey(self, key=None):
    raise NotImplementedError()

  def _removeUID(self, uid, key):
    raise NotImplementedError()

  def _addUID(self, uid, key):
    raise NotImplementedError()

  def _getUID(self):
    raise NotImplementedError()

#
# ILabeling
#

class ILabeling:
  """
  Interface class to the Labeling class.

  Every Labeling-derived class must implement all of the methods listed.
  """

  def __init__(self, dataset, name=None):
    raise NotImplementedError()

  def _getLocalRef(self):
    raise NotImplementedError()

  def _getBaseRef(self):
    raise NotImplementedError()

  def getName(self):
    raise NotImplementedError()

  def setName(self, name):
    raise NotImplementedError()
  
  def labelRows(self, obj):
    raise NotImplementedError()

  def labelCols(self, obj):
    raise NotImplementedError()

  def labelKeys(self, obj):
    raise NotImplementedError()

  def writeLabels(self, stream=sys.stdout, delimiter="\t"):
    raise NotImplementedError()

  def addLabelToRow(self, label, row):
    raise NotImplementedError()
  
  def addLabelToCol(self, label, col):
    raise NotImplementedError()
  
  def addLabelToKey(self, label, key):
    raise NotImplementedError()

  def addLabelToRows(self, label, row):
    raise NotImplementedError()
  
  def addLabelToCols(self, label, col):
    raise NotImplementedError()
  
  def addLabelToKeys(self, label, key):
    raise NotImplementedError()

  def addLabelsToRow(self, labels, row):
    raise NotImplementedError()
  
  def addLabelsToCol(self, labels, col):
    raise NotImplementedError()
  
  def addLabelsToKey(self, labels, key):
    raise NotImplementedError()

  def addLabelsToRows(self, labels, rows):
    raise NotImplementedError()
  
  def addLabelsToCols(self, labels, cols):
    raise NotImplementedError()
  
  def addLabelsToKeys(self, labels, keys):
    raise NotImplementedError()

  def getDataset(self):
    raise NotImplementedError()

  def getLabels(self):
    raise NotImplementedError()

  def getLabelByRow(self, row, n):
    raise NotImplementedError()

  def getLabelByCol(self, col, n):
    raise NotImplementedError()

  def getLabelByKey(self, key, n):
    raise NotImplementedError()

  def getLabelsByRow(self, row):
    raise NotImplementedError()

  def getLabelsByCol(self, col):
    raise NotImplementedError()

  def getLabelsByKey(self, key):
    raise NotImplementedError()

  def getLabelsByRows(self, rows):
    raise NotImplementedError()

  def getLabelsByCols(self, cols):
    raise NotImplementedError()

  def getLabelsByKeys(self, keys):
    raise NotImplementedError()
  
  def getRowsByLabel(self, label):
    raise NotImplementedError()

  def getColsByLabel(self, label):
    raise NotImplementedError()

  def getKeysByLabel(self, label):
    raise NotImplementedError()

  def removeAll(self):
    raise NotImplementedError()

  def detatch(self):
    raise NotImplementedError()

  def removeLabel(self, label):
    raise NotImplementedError()

  def removeLabelFromRow(self, label, row):
    raise NotImplementedError()

  def removeLabelFromCol(self, label, col):
    raise NotImplementedError()

  def removeLabelFromKey(self, label, key):
    raise NotImplementedError()
  
  def removeLabelsFromRow(self, row):
    raise NotImplementedError()

  def removeLabelsFromCol(self, col):
    raise NotImplementedError()

  def removeLabelsFromKey(self, key):
    raise NotImplementedError()
  
  def _getUID(self, label):
    raise NotImplementedError()  

#
# IModel
#

class IModel:
  """
  Interface class defining the basic set of operations allowable on a Model.
  """

  def __init__(self):
    raise NotImplementedError()

  def evaluateFitness(self, dataset):
    raise NotImplementedError()

#
# IML_Algorithm
#

class IML_Algorithm(object):
  def __init__(self, dataset, parameters):
    raise NotImplementedError()

  def clear(self):
    raise NotImplementedError()

  def run(self, stream=sys.stdout):
    raise NotImplementedError()

  def validate(self, stream=sys.stdout):
    raise NotImplementedError()

  def getDataset(self):
    raise NotImplementedError()

  def setDataset(self, dataset):
    raise NotImplementedError()
  
  def getModel(self):
    raise NotImplementedError()

  def setModel(self):
    raise NotImplementedError()

  def getLabeling(self):
    raise NotImplementedError()
  
  def getParameters(self):
    raise NotImplementedError()

  def setParameters(self, parameters):
    raise NotImplementedError()
  
  def setMessageStream(self, stream):
    raise NotImplementedError()
  
  def getMessageStream(self):
    raise NotImplementedError()
