#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

"""
Test suite for the Model module.
"""

import unittest
import os
import numpy

from compClust.mlx.datasets import Dataset
from compClust.mlx.labelings import Labeling
from compClust.mlx.models import *

import compClust.mlx

class TestModelCases(unittest.TestCase):
  def setUp(self):
    """Construct a model to play with"""
    # since numpy's seed function doesn't work i can't get consistent
    # random numbers, so lets just generate one set and save them
    #data, partitioning = self.createData(42, 42)
    array=numpy.array
    data = [array([ 3.39459987,  2.74532444,  2.7290673 ]), 
            array([ 2.01521237,  1.46261371,  2.80453666]), 
            array([ 3.58012381,  2.15176442,  1.37563353]), 
            array([ 2.79595461,  2.95333942,  3.10728049]), 
            array([ 2.81576924,  3.12394849,  3.80157318]), 
            array([ 2.31074109,  3.01381865,  4.27450965]), 
            array([ 1.02227595,  1.87296553,  3.6198936 ]), 
            array([ 4.52656382,  4.43747666,  3.48551718]), 
            array([ 4.7701432 ,  3.8026387 ,  2.69536591]), 
            array([ 1.37108034,  1.47623914,  3.59047832]), 
            array([ 4.16468541,  3.59906672,  2.16739073]), 
            array([ 4.36080582,  4.27602917,  3.38993981]), 
            array([ 2.40329546,  1.1407812 ,  3.16616426]), 
            array([ 2.38579665,  3.69853374,  3.14189297]), 
            array([ 2.79644146,  3.57907648,  3.84424681]), 
            array([ 3.23092226,  2.9977837 ,  2.65424148]), 
            array([ 3.32525766,  2.56830363,  2.61014891]), 
            array([ 1.24364573,  1.53815142,  1.9014573 ]), 
            array([ 3.79658724,  3.6572324 ,  1.42671044]), 
            array([ 4.36323348,  1.74440402,  2.37327886]), 
            array([-0.35988992,  1.22842498, -0.36145154]), 
            array([-0.89130199, -0.63511573, -0.15481433]), 
            array([-1.31712073, -0.53551443, -2.10005358]), 
            array([-0.59609213, -0.54552082,  0.61371765]), 
            array([ 0.24518225,  0.57832253, -0.35365717]), 
            array([ 0.24129709, -1.5066323 , -0.35493471]), 
            array([-1.39849528,  0.71539882,  0.88800441]), 
            array([ 0.35434271,  0.34612053,  0.75260392]), 
            array([-0.20179417, -0.78184866, -0.03652661]), 
            array([ 2.3542371 ,  0.11739666,  0.9402952 ]), 
            array([-0.36204937,  1.13921189,  0.20814426]), 
            array([ 0.35053944, -0.24244777,  1.29385797]), 
            array([-0.20903637,  2.56331546,  0.62451744]), 
            array([ 0.41950754, -1.48039326,  0.78537942]), 
            array([ 0.27535427, -1.4695405 , -0.25470408]), 
            array([ 0.62679112,  0.08026045, -1.13917489]), 
            array([-1.04003259,  0.88042949,  1.77715346]), 
            array([ 0.68760058, -0.727773  , -0.66844878]), 
            array([-1.93731765, -0.20670518,  1.08476824]), 
            array([ 1.34783682, -0.77843172,  0.88473481]), 
            array([-2.27113917, -3.11895818, -3.26003057]), 
            array([-3.58067545, -2.70041762, -3.2544482 ]), 
            array([-3.68681487, -1.96470155, -2.74971335]), 
            array([-1.4843019 , -3.19985009, -3.36954145]), 
            array([-4.41580788, -4.56975323, -3.27245613]), 
            array([-4.74624177, -2.52903324, -2.97347868]), 
            array([-4.67288007, -4.07256452, -2.92212281]), 
            array([-3.00979044, -3.76143006, -3.35790218]), 
            array([-2.66969083, -2.2130622 , -3.52700737]), 
            array([-2.49283193, -3.44414232, -2.90945524]), 
            array([-4.17871569, -3.94256339, -3.19533665]), 
            array([-3.42511502, -3.16530904, -3.23946813]), 
            array([-1.95319574, -1.37476385, -2.97110732]), 
            array([-2.38232949, -2.8147288 , -2.98887977]), 
            array([-5.82890664, -2.66483476, -3.13391008]), 
            array([-3.80937237, -2.87195888, -3.2104018 ]), 
            array([-3.22453612, -1.94694711, -3.03123012]), 
            array([-4.04901484, -4.55537101, -3.08874318]), 
            array([-4.9041964 , -4.42531998, -2.65125716]), 
            array([-3.70786981, -3.07202461, -2.82889204])]
    partitioning = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, \
                    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, \
                    1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 
                    2, 2, 2, 2, 2, 2]
    self.dataset = Dataset(data)
    
    self.labels = Labeling(self.dataset)
    self.labels.labelRows(partitioning)
    
  def createData(self, seed1, seed2):
    num_of_samples = 20 # how many samples for each cluster to make
    
    covars = [numpy.asarray([[1, .5, .1],[.5,1,.5],[.1,.5,1]]),
              numpy.identity(3),
              numpy.asarray([[1,0,0],[0,.5,0],[0,0,.1]])]
    means = [ numpy.asarray([3.0,3.0,3.0],'d'),
              numpy.asarray([0,0,0],'d'),
              numpy.asarray([-3,-3,-3], 'd')]

    data = []
    partitioning = []
    #numpy.random.seed(60)
    for i in xrange(len(covars)):
      mean = numpy.zeros(covars[0].shape[0]) # use number of columns
      for j in xrange(num_of_samples):
        row = numpy.random.multivariate_normal(means[i],covars[i])
        mean = mean + row
        data.append(row)
        partitioning.append(i)
    return data, partitioning

  def compare_covariances(self, m1, m2, msg="covariance failed at %d %d %d"):
    self.failUnless(len(m1) == len(m2),"different number of covariant classes")
    for k in xrange(len(m1)): # choose which class to operate on
      self.failUnless(len(m1[k]) == len(m2[k]),
        "different # of rows for covariance matricies for class %d" % (k))
      for row in xrange(len(m1[k])):
        self.failUnless(len(m1[k]) == len(m2[k]),
        "different # of cols for covariance matricies for class %d %d" % (k,
                                                                          row))
        for col in xrange(len(m1[k][row])):
          self.failUnlessAlmostEqual(m1[k][row][col],
                                     m2[k][row][col],
                                     msg=msg%(k,row,col))
          
          
  def compare_means(self, m1, m2, msg="means failed at %d %d "):
    self.failUnless(len(m1) == len(m2), "different number of mean classes")
    for k in xrange(len(m1)): # choose which class to operate on
      self.failUnless(len(m1[k]) == len(m2[k]),
                      "mean vectors were different lengths in class %d" % (k))
      for col in xrange(len(m1[k])):
        self.failUnlessAlmostEqual(m1[k][col], m2[k][col], msg=msg%(k,col))

  def compare_weights(self, w1, w2, msg="weights differed at %d"):
    self.failUnless(len(w1) == len(w2), "weights matrices were different lengths")
    for k in xrange(len(w1)):
      self.failUnlessAlmostEqual(w1[k], w2[k], msg=msg%(k))
      

  def testComputeModelWeights(self):
    weights = compute_model_weights(self.labels)
    # FIXME: need actual tests
    os.chdir(self.original_dir)

  def testComputeModelMeans(self):
    computed_means = compute_model_means(self.dataset, self.labels)
    hardcoded_means = [[ 3.03365677,  2.79197458,  2.90796637],
                       [-0.07052206, -0.06305213,  0.22147055],
                       [-3.52467132, -3.12038672, -3.09676911]]
  
    
    for means in  zip(computed_means, hardcoded_means):
      for i in xrange(len(means)):
        self.failUnlessAlmostEqual(means[0][i], means[1][i],
                                   msg="failed check on %s:%s" % (means, i))
  
  def testComputeModelCovarianceAndWeights(self):
    means = compute_model_means(self.dataset, self.labels)
    covariances,weights = compute_model_covariances_weights(self.dataset,
                                                            self.labels,
                                                            means)
    for w in weights:
      self.failUnlessAlmostEqual(w, 0.33333333, msg="weights failed")

    historical_covariances = [[[ 1.24939528,  0.71578717, -0.26372756],
                               [ 0.71578717,  1.01061738,  0.11248052],
                               [-0.26372756,  0.11248052,  0.62488287],],

                              [[ 0.98758415, -0.19333388,  0.0331635 ],
                               [-0.19333388,  1.07981778,  0.20373752],
                               [ 0.0331635 ,  0.20373752,  0.84754819],],

                              [[ 1.22809448,  0.35810662, -0.07744957],
                               [ 0.35810662,  0.81281516,  0.00134542],
                               [-0.07744957,  0.00134542,  0.05040628],]]
    error_msg="ComputeModelCovariances failed on index %s %s %s"
    self.compare_covariances(covariances, historical_covariances, error_msg)

  def testDistanceFromMeanModel(self):
##     means = compute_model_means(self.dataset, self.labels)
##     kmeans_model = KMeansModel(means)
##     fitness = kmeans_model.evaluateFitness(means)
##     if fitness < .038 or fitness > .04:
##       fail("fitness of KMeansModel with test dataset changed.")
    #I3 = numpy.array([[1,1,1],[1,1,1],[1,1,1]])
    I3 = numpy.eye(3)
    variance_range = range(2,17,2)
    
    #means = [numpy.array([1,2,3]), numpy.array([6,7,8])]
    means = [numpy.array([1,2,3])]
    kmeans_model = DistanceFromMean(means)
    fitness_tbl = numpy.zeros(len(variance_range), float)

    historical_fit_table = [ 0.16071539, 0.08466076, 0.05891822, 0.04095603,
                             0.033259  , 0.02810546, 0.0243343 , 0.02084022]
    for v in xrange(len(variance_range)):
      data = []
      variance = variance_range[v]
      # construct ddata
      for i in xrange(250):
        data.append(numpy.random.multivariate_normal(means[0], I3 * variance))
        #data.append(numpy.random.multivariate_normal(means[1], I3 * variance))

      fitness = kmeans_model.evaluateFitness(data)
      fitness_tbl[v] = fitness
      self.failUnlessAlmostEqual(fitness, historical_fit_table[v], 1,
                                 msg="fitness failed for entry %d" %(v))

  def testConstructMoGModelFromLabeling(self):
    historical_covariances = [[[ 1.24939528,  0.71578717, -0.26372756],
                               [ 0.71578717,  1.01061738,  0.11248052],
                               [-0.26372756,  0.11248052,  0.62488287],],

                              [[ 0.98758415, -0.19333388,  0.0331635 ],
                               [-0.19333388,  1.07981778,  0.20373752],
                               [ 0.0331635 ,  0.20373752,  0.84754819],],

                              [[ 1.22809448,  0.35810662, -0.07744957],
                               [ 0.35810662,  0.81281516,  0.00134542],
                               [-0.07744957,  0.00134542,  0.05040628]]]

    historical_means = [[ 3.03365677,  2.79197458,  2.90796637],
                        [-0.07052206, -0.06305213,  0.22147055],
                        [-3.52467132, -3.12038672, -3.09676911]]
    historical_weights = [ 0.33333333, 0.33333333, 0.33333333,]

    historical_fit = -271.193440

    m = constructMixtureOfGaussiansFromLabeling(self.dataset, self.labels)

    self.failUnless(m.k == 3, "Wrong number of classes")
    self.failUnless(m.d == 3, "Wrong number of dimensions")
    self.compare_covariances(m.covariances, historical_covariances)
    self.compare_means(m.means, historical_means)
    self.compare_weights(m.weights, historical_weights)
    fit = m.evaluateFitness(self.dataset.getData())
    self.failUnlessAlmostEqual(fit, historical_fit, places=5,
                               msg="Fitness failed %f %f" %(fit,
                                                            historical_fit))


  def testMoDGModel(self):
    historical_covariances = [[[ 1.24939528,  0.71578717, -0.26372756],
                               [ 0.71578717,  1.01061738,  0.11248052],
                               [-0.26372756,  0.11248052,  0.62488287],],

                              [[ 0.98758415, -0.19333388,  0.0331635 ],
                               [-0.19333388,  1.07981778,  0.20373752],
                               [ 0.0331635 ,  0.20373752,  0.84754819],],

                              [[ 1.22809448,  0.35810662, -0.07744957],
                               [ 0.35810662,  0.81281516,  0.00134542],
                               [-0.07744957,  0.00134542,  0.05040628],]]

    historical_means = [[ 3.03365677,  2.79197458,  2.90796637],
                        [-0.07052206, -0.06305213,  0.22147055],
                        [-3.52467132, -3.12038672, -3.09676911],]
    historical_weights = [ 0.33333333, 0.33333333, 0.33333333,]

    historical_fit = -283.170468

    d = constructMixtureOfDiagonalGaussiansFromLabeling(self.dataset, self.labels)

    self.failUnless(d.k == 3, "Wrong number of classes")
    self.failUnless(d.d == 3, "Wrong number of dimensions")

    self.compare_covariances(d.covariances, historical_covariances,
                             "MoDGModel covariances failed at %d %d %d")
    self.compare_means(d.means, historical_means)
    self.compare_weights(d.weights, historical_weights)
    fit = d.evaluateFitness(self.dataset.getData())
    self.failUnlessAlmostEqual(fit, historical_fit, places=5,
                               msg="Fitness failed %f %f" %(fit,
                                                            historical_fit))

    
  def testMoFGModel(self):
    historical_covariances = [[[ 1.24939528,  0.71578717, -0.26372756],
                               [ 0.71578717,  1.01061738,  0.11248052],
                               [-0.26372756,  0.11248052,  0.62488287],],

                              [[ 0.98758415, -0.19333388,  0.0331635 ],
                               [-0.19333388,  1.07981778,  0.20373752],
                               [ 0.0331635 ,  0.20373752,  0.84754819],],

                              [[ 1.22809448,  0.35810662, -0.07744957],
                               [ 0.35810662,  0.81281516,  0.00134542],
                               [-0.07744957,  0.00134542,  0.05040628]]]
    historical_means = [[ 3.03365677,  2.79197458,  2.90796637],
                        [-0.07052206, -0.06305213,  0.22147055],
                        [-3.52467132, -3.12038672, -3.09676911],]
    historical_weights = [ 0.33333333, 0.33333333, 0.33333333,]
    historical_fit = -271.193440


    f = constructMixtureOfFullGaussiansFromLabeling(self.dataset, self.labels)

    self.failUnless(f.k == 3, "Wrong number of classes")
    self.failUnless(f.d == 3, "Wrong number of dimensions")

    self.compare_covariances(f.covariances, historical_covariances,
                             "MoFGModel covariances failed at %d %d %d")
    
    self.compare_means(f.means, historical_means)
    self.compare_weights(f.weights, historical_weights)

    fit = f.evaluateFitness(self.dataset.getData())
    self.failUnlessAlmostEqual(fit, historical_fit, places=5,
                               msg="Fitness failed %f %f" %(fit, 
                                                            historical_fit))
    
def suite(**kw):
  suite = unittest.TestSuite()
  #not needed#suite.addTest(TestModelCases("testComputeModelWeights"))
  suite.addTest(TestModelCases("testComputeModelMeans"               ))
  suite.addTest(TestModelCases("testComputeModelCovarianceAndWeights"))
  suite.addTest(TestModelCases("testConstructMoGModelFromLabeling"   ))
  suite.addTest(TestModelCases("testDistanceFromMeanModel"           ))
  suite.addTest(TestModelCases("testMoDGModel"                       ))
  suite.addTest(TestModelCases("testMoFGModel"                       ))
  return suite

if __name__ == "__main__":
  unittest.main(defaultTest="suite")
