########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
# Mixture of Diagonal Gaussians Model
#

from compClust.mlx.models import MixtureOfGaussians

class MixtureOfDiagonalGaussians(MixtureOfGaussians):

  """
  Mixture of Diagonal Gaussians is a subclass of Mixtureof Gaussians which
  only uses diagonal of the covariance matrices in its fitness compuations.
  """

  def __init__(self, k, means, covariances, weights=None):
    """
    Creates a new Mixture of Diagonal Gaussians (MoDG) Model, containing k
    Gaussian clusters in d-dimensional space.
    """

    MixtureOfGaussians.__init__(self, k, means, covariances, weights)

  def evaluateFitness(self, data):
    """
    Return the fitness of the model given a paricular set of data. Using only
    the diagonal covariances, if that fails, then the log-likelihood is set to
    -MAX_FLOAT =~ -1e38.
    """
    
    try:
      fitness = self.getDiagLogLikelihood(data)
    except ValueError, e:
      fitness = -1e38;

    return fitness
