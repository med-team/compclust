########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

__all__ = ['confusionMatrix',
           'ANN',
           'DiagEM',
           'FullEM',
           'Hierarchical',
           'HutSOM',
           'KMeans',
           'MCCV',
           'Monkey',
           'SClust',
           'SVM',
           'TSplit',
           'XClust',
           ]

from ANN import ANN
from DiagEM import DiagEM
from FullEM import FullEM
from Hierarchical import Hierarchical
from HutSOM import HutSOM
from KMeans import KMeans
from MCCV import MCCV
from Monkey import Monkey
from SClust import SClust
from SVM import SVM
from TSplit import TSplit
from XClust import XClust
from MultiRun import MultiRun

WRAPPER_STATUS_ERROR=-1
WRAPPER_STATUS_RUNNING=0
WRAPPER_STATUS_DONE=1


def getDefaultParameterDescriptions(classObj):
  from compClust.util.LoadModule import loadModule

  fullModuleName = classObj.__module__.split('.')
  moduleName = fullModuleName[-1:][0]
  myMod = loadModule(__path__, moduleName)

  try:
    paramDict = myMod.getDefaultWrapperParameters()
  except:
    msg = 'No getDefaultWrapperParameters() in module %s' % (myMod)
    raise NotImplementedError, msg

  return paramDict


def getDefaultParameters(classObj):
  from compClust.util.LoadModule import loadModule

  fullModuleName = classObj.__module__.split('.')
  moduleName = fullModuleName[-1:][0]
  myMod = loadModule(__path__, moduleName)

  try:
    paramDict = myMod.getDefaultParameters()
  except:
    msg = 'No getDefaultParameters() in module %s' % (myMod)
    raise NotImplementedError, msg

  return paramDict
