########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

"""
A wrapper around our wrappers which reads a parameter file and
determines which algo wrapper to call.

"""

import sys

from compClust.util import WrapperUtil
from compClust.mlx.wrapper import Launcher
from compClust.mlx.wrapper import KMeans
from compClust.mlx.wrapper import DiagEM
from compClust.mlx.wrapper import FullEM
from compClust.mlx.wrapper import HutSOM
from compClust.mlx.wrapper import KMeans
from compClust.mlx.wrapper import XClust
from compClust.mlx.wrapper import Monkey


def printUsage(long=0):
  if long:
    print
    """
    
    Usage: cluster.py parameterFilename datasetFilename resultsFilename cluster.py

    provides a convience wrapper that allows a single script to be run to launch any
    algorithm.  It requires an additional paramter 'algo_name' be provided in the
    paramter files.  Currently algo_name can be on of the following:
       "kmeans"
       "diagem"
       "xclust"
       "tsplit"
       "monkey"
       "fullem"
       "hutsom"
       
    All other paramters are specific to the algortihm being run, consult their documentation
    for more detail.
    """
    
  else:
    print "Usage: cluster.py parameterFilename datasetFilename resultsFilename"
  sys.exit()

def main(args):

  opts, args = WrapperUtil.createOptTree('h',['help'])

  if opts.has_key('-h'):
    printUsage()

  if opts.has_key('--help'):
    printUsage(long=1)
  
  try:
    params = WrapperUtil.load_parameter_file(args[0])
  except:
    printUsage()

  algo = params.get('algo_name')
  if algo is not None:

    if algo == "kmeans":
      Launcher.main(sys.argv, KMeans())
    elif algo == "diagem":
      Launcher.main(sys.argv, DiagEM())
    elif algo == "xclust":
      Launcher.main(sys.argv, XClust())
    elif algo == "tsplit":
      Launcher.main(sys.argv, TSplit())
    elif algo == "monkey":
      Launcher.main(sys.argv, Monkey())
    elif algo == "fullem":
      Launcher.main(sys.argv, FullEM())
    elif algo == "hutsom":
      Launcher.main(sys.argv, HutSOM())
    else:
      printUsage(long=1)

  else:
    printUsage(long=1)


if __name__ == "__main__":

  main(sys.argv)
