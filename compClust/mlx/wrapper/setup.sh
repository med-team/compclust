#!/bin/sh

#confusionMatrix
export WMATCH_COMMAND=${CODE_HOME}/bin/wmatch
export WMATCH_FULL_COMMAND=${CODE_HOME}/bin/wmatch-full

#diagem
export DIAGEM_COMMAND=${CODE_HOME}/bin/diagem

#fullem
export FULLEM_COMMAND=${CODE_HOME}/bin/fullem

#hutsom
export MATLAB_CODE_HOME=${CODE_HOME}/matlab
export SOM_TOOLBOX_HOME=${CODE_HOME}/matlab/somtoolbox

#kmeans
export KMEANS_COMMAND=${CODE_HOME}/bin/kmeans

#tsplit
export TSPLIT_COMMAND=${CODE_HOME}/bin/tsplit

#xclust
export XCLUST_COMMAND=${CODE_HOME}/bin/xclust

#ANN
ANN_COMMAND=${CODE_HOME}/imported/uwbp/bpLINUX

#SMV
SVM_TOOLBOX_HOME=${CODE_HOME}/matlab/mls/psvm
