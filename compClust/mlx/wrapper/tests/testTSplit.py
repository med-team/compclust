#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

import inspect
import os
import string
import sys
import tempfile
import unittest

from compClust.util import WrapperUtil
from compClust.util import Verify

import compClust.mlx.wrapper

class testTSplit(unittest.TestCase):

  def setUp(self):
    """Create temporary directory and file handles for test runs of TSplit.
    """
    source = os.path.realpath(inspect.getsourcefile(testTSplit))
    self.datadir = os.path.split(source)[0]
    self.executable=string.join([sys.executable,
                                 os.path.join(self.datadir,'..','TSplit.py')])
      
    
    self.original_dir    = os.getcwd()
    os.chdir(compClust.mlx.wrapper.__path__[0])
    
    self.temp_dir_name   = WrapperUtil.create_temporary_directory("TSplt")

    self.orig_tempdir    = tempfile.tempdir
    tempfile.tempdir     = self.temp_dir_name
    self.param_filename  = tempfile.mktemp("parameter_file")
    self.param_stream    = open(self.param_filename, "w")
    self.result_filename = tempfile.mktemp("result_file")

  def tearDown(self):
    """Clean up after ourselves.
    """
    tempfile.tempdir = self.orig_tempdir
    # FIXME: should this delete files that setUp did not create?
    
    #try:
    #    os.remove(self.result_filename)
    #except OSError, e:
    #    pass
    
    #self.param_stream.close()
    #os.remove(self.param_filename)
    #os.rmdir(self.temp_dir_name)

    os.chdir(self.original_dir)
    
  def testTSplit(self):
    print "Need to update TSplit to WrapperParameters API"
    
#    
#  def test_tsplit_small_tree_reasonable_k(self):
#    """Test TSplit using a small tree search for a number of clusters
#    that is noticably below the number of data points.
#
#    """
#    
#    parameters       = ["k                  = 5",
#                        "splitting_method   = 'Best'",
#                        "agglomerate_method = 'clusterNumber'",
#                        "distance_metric    = 'Bhattacharyya'"
#                        ]
#
#    data_filename = os.path.join(self.datadir,
#                                 "synth_t_05c0_p_0075_d_03_v_0d1.txt")
#    result_filename = os.path.join(self.datadir,
#                                   "tsplit.small")
#                        
#    self.param_stream.write(string.join(parameters, "\n"))
#    self.param_stream.close()
#
#    argv=[self.executable, self.param_filename, data_filename, self.result_filename, " > /dev/null" ]
#
#    os.system(string.join(argv, " "))
#    result = os.system("cmp -s %s "%(result_filename) + `self.result_filename`)
#
#    self.failIf(result != 0, "small tree failed")
#
#
#  def test_tsplit_medium_tree_reasonable_k(self):
#    """Test TSplit using a medium tree search for a number of clusters
#    that is noticably below the number of data points.
#
#    """
#    parameters       = ["k                  = 5",
#                        "splitting_method   = 'Best'",
#                        "agglomerate_method = 'clusterNumber'",
#                        "distance_metric    = 'Bhattacharyya'"
#                        ]
#    
#    data_filename = os.path.join(self.datadir,
#                                 "synth_t_05c0_p_0750_d_03_v_0d1.txt")
#    result_filename = os.path.join(self.datadir, "tsplit.medium")
#                        
#    self.param_stream.write(string.join(parameters, "\n"))
#    self.param_stream.close()
#
#    argv=[self.executable, self.param_filename, data_filename, self.result_filename, " > /dev/null" ]
#    
#    os.system(string.join(argv, " "))
#    result = os.system("cmp -s %s "%(result_filename)+ `self.result_filename`)
#
#    self.failIf(result != 0, "medium tree failed")
#
#  def test_tsplit_large_tree_reasonable_k(self):
#    """Test TSplit using a large tree search for a number of clusters
#    that is noticably below the number of data points.
#
#    """
#
#    parameters       = ["k                  = 5",
#                        "splitting_method   = 'Best'",
#                        "agglomerate_method = 'clusterNumber'",
#                        "distance_metric    = 'Bhattacharyya'"
#                        ]
#    
#    data_filename = os.path.join(self.datadir,
#                                 "synth_t_05c0_p_7500_d_03_v_0d1.txt")
#    result_filename = os.path.join(self.datadir, tsplit.large)
#                        
#    self.param_stream.write(string.join(parameters, "\n"))
#    self.param_stream.close()
#
#    argv=[sys.executable, "./TSplit.py", self.param_filename, data_filename, self.result_filename, " > /dev/null" ]
#    
#    os.system(string.join(argv, " "))
#    result = os.system("cmp -s " + result_filename+" "+`self.result_filename`)
#
#    self.failIf(result != 0, "large tree failed")
#    
def suite(**kw):
  print "TSplit needs to be updated, skipping tests"
  return None
  print "These tests will take several minutes to run..."
  
  suite = unittest.TestSuite()
  if os.environ.has_key('TSPLIT_COMMAND'):
    suite.addTest(testTSplit("test_tsplit_small_tree_reasonable_k"))
    suite.addTest(testTSplit("test_tsplit_medium_tree_reasonable_k"))
    # suite.addTest(testTSplit("test_tsplit_large_tree_reasonable_k"))
  else:
    print "TSPLIT_COMMAND is not available, tsplit tests skipped"

  return suite

if __name__ == "__main__":
  unittest.main(defaultTest="suite")
