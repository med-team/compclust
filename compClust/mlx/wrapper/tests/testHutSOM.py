#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

import inspect
import os
import string
import sys
import tempfile
import unittest

from compClust.util import WrapperUtil
from compClust.util import Verify

import compClust.mlx.wrapper
  
if not os.environ.has_key("MATLAB_CODE_HOME"):
  # matlab not found, can't test
  # not to mention this is using an insanely obsolete API
  pass
else:    
  class testHutSOM(unittest.TestCase):
  
    def setUp(self):
      """Create temporary directory and file handles for test runs of hutsom.
      """
      source = os.path.realpath(inspect.getsourcefile(testHutSOM))
      self.datadir = os.path.split(source)[0]
      self.executable=string.join([sys.executable,
                                   os.path.join(self.datadir,'..','HutSOM.py')])
  
      self.original_dir    = os.getcwd()
      os.chdir(compClust.mlx.wrapper.__path__[0])
  
      self.temp_dir_name   = WrapperUtil.create_temporary_directory("smtst")
  
      self.orig_tempdir    = tempfile.tempdir
      tempfile.tempdir     = self.temp_dir_name
      self.param_filename  = tempfile.mktemp("parameter_file")
      self.param_stream    = open(self.param_filename, "w")
      self.result_filename = tempfile.mktemp("result_file")
  
    def tearDown(self):
      """Clean up after ourselves.
      """
      tempfile.tempdir = self.orig_tempdir
      # FIXME: should this delete files that setUp did not create?
      
      try:
          os.remove(self.result_filename)
      except OSError, e:
          pass
      
      self.param_stream.close()
      os.remove(self.param_filename)
      os.rmdir(self.temp_dir_name)
  
      os.chdir(self.original_dir)
  
    def getParameters(self, k=5):
      return ["som_y_dimension  = %s" % (k),
              "som_x_dimension  = 1",
              "transform_method = 'none'",
              "init_method      = 'linear'",
              "num_iterations   = 100",
              ]
      
      
    def test_hutsom_small_tree_reasonable_k(self):
      """Test hutsom using a small tree search for a number of clusters
      that is noticably below the number of data points.
  
      """
     
      parameters = self.getParameters()
      
      data_filename = os.path.join(self.datadir,
                                   "synth_t_05c0_p_0075_d_03_v_0d1.txt")
      result_filename = os.path.join(self.datadir, 'hutsom.small')
                         
      self.param_stream.write(string.join(parameters, "\n"))
      self.param_stream.close()
  
      argv=[self.executable, self.param_filename, data_filename, self.result_filename, " > /dev/null" ]
      
      os.system(string.join(argv, " "))
      result = os.system("cmp -s %s "%(self.result_filename) + `self.result_filename`)
      result = os.WEXITSTATUS(result)
  
      self.failIf(result != 0, "small tree failed")
  
  
    def test_hutsom_medium_tree_reasonable_k(self):
      """Test hutsom using a medium tree search for a number of clusters
      that is noticably below the number of data points.
  
      """
      parameters = self.getParameters()
      
      data_filename = os.path.join(self.datadir,
                                   "synth_t_05c0_p_0750_d_03_v_0d1.txt")
      result_filename = os.path.join(self.datadir, 'hustom.medium')
                          
      self.param_stream.write(string.join(parameters, "\n"))
      self.param_stream.close()
  
      argv=[self.executable, self.param_filename, data_filename, self.result_filename, " > /dev/null" ]
      
      os.system(string.join(argv, " "))
      result = os.system("cmp -s %s "%(self.result_filename) + `self.result_filename`)
      result = os.WEXITSTATUS(result)
  
      self.failIf(result != 0, "medium tree failed")
  
    def test_hutsom_large_tree_reasonable_k(self):
      """Test hutsom using a large tree search for a number of clusters
      that is noticably below the number of data points.
  
      """
  
      parameters = self.getParameters()
      
      data_filename = os.path.join(self.datadir,
                                   "synth_t_05c0_p_7500_d_03_v_0d1.txt")
      result_filename = os.path.join(self.datadir, 'hutsom.large')
                          
      self.param_stream.write(string.join(parameters, "\n"))
      self.param_stream.close()
  
      argv=[self.executable, self.param_filename, data_filename, self.result_filename, "> /dev/null" ]
      
      os.system(string.join(argv, " "))
      result = os.system("cmp -s %s "%(self.result_filename) + `self.result_filename`)
      result = os.WEXITSTATUS(result)
  
      self.failIf(result != 0, "large tree failed")
  
  def isMatlabInstalled():
    """boolean = isMatlabInstalled()
  
    Using which try and determine if matlab is instlled.
    """
    path = string.split(os.environ['PATH'], os.pathsep)
    for dir in path:
      if os.path.exists(os.path.join(dir, 'matlab')):
        return True
    else:
      return False
  
def suite(**kw):
  print "HutSOM needs to be updated, skipping tests"
  return None

  print "These tests will take several minutes to run..."

  suite = unittest.TestSuite()
  
  if isMatlabInstalled():
    suite.addTest(testHutSOM("test_hutsom_small_tree_reasonable_k"))
    suite.addTest(testHutSOM("test_hutsom_medium_tree_reasonable_k"))
    suite.addTest(testHutSOM("test_hutsom_large_tree_reasonable_k"))
  else:
    print "Note: Matlab is not installed on path and so the HutSOM tests"
    print "are being skipped."

  return suite

if __name__ == "__main__":
  unittest.main(defaultTest="suite")


