#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

import inspect
import os
import string
import sys
import tempfile
import unittest

from compClust.util import WrapperUtil
from compClust.util import Verify

import compClust.mlx.wrapper

class testSClust(unittest.TestCase):

  def setUp(self):
    """Create temporary directory and file handles for test runs of sclust.
    """
    source = os.path.realpath(inspect.getsourcefile(testSClust))
    self.datadir = os.path.split(source)[0]
    self.executable=string.join([sys.executable,
                                 os.path.join(self.datadir,'..','SClust.py')])

    self.original_dir    = os.getcwd()
    os.chdir(compClust.mlx.wrapper.__path__[0])
    
    self.temp_dir_name   = WrapperUtil.create_temporary_directory("ditst")

    self.orig_tempdir    = tempfile.tempdir
    tempfile.tempdir     = self.temp_dir_name
    self.param_filename  = tempfile.mktemp("parameter_file")
    self.param_stream    = open(self.param_filename, "w")
    self.result_filename = tempfile.mktemp("result_file")

  def tearDown(self):
    """Clean up after ourselves.
    """
    tempfile.tempdir = self.orig_tempdir
    # FIXME: should this delete files that setUp did not create?
    try:
        os.remove(self.result_filename)
    except OSError, e:
        pass
    
    self.param_stream.close()
    os.remove(self.param_filename)
    os.rmdir(self.temp_dir_name)

    os.chdir(self.original_dir)
  def getParameters(self, k=5):
    """parameters_dict = self.getParameters(k)

    Return a reasonable list of parameters for the given algorithm.
    """

    return ["k               = %s" % (k),
            "seed            = 1234",
            ]
    
# FIXME: We need to updated SClust to the WrapperParameters method    
#
#  def test_sclust_small_tree(self):
#    """Test sclust using a small tree search for a number of clusters
#    that is noticably below the number of data points.
#
#    """
#    data_filename = os.path.join(self.datadir,
#                                 "synth_t_05c0_p_0075_d_03_v_0d1.txt")
#    result_filename = os.path.join(self.datadir, 'sclust.small')
#
#    
#    parameters = self.getParameters()
#    
#    self.param_stream.write(string.join(parameters, "\n"))
#    self.param_stream.close()
#
#    argv=[self.executable, self.param_filename, data_filename, self.result_filename, " >& /dev/null"]
#    
#    os.system(string.join(argv, " "))
#    result = os.system("cmp -s %s "%(self.result_filename) + `self.result_filename`)
#    result = os.WEXITSTATUS(result)
#
#    self.failIf(result != 0, "small tree failed")

def suite(**kw):

  print "SClust needs to be updated"
  return None

  print "These tests will take several minutes to run..."
  
  suite = unittest.TestSuite()
  if os.environ.has_key("SCLUST_COMMAND"):
    suite.addTest(testSClust("test_sclust_small_tree"))
  else:
    print "SCLUST_COMMAND is not available, skipping sclust tests"
    
  return suite

if __name__ == "__main__":
  unittest.main(defaultTest="suite")


