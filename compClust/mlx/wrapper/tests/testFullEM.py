#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

import os
import string
import tempfile
import unittest
import sys

from compClust.util import WrapperUtil
from compClust.util import Verify
import compClust.mlx.wrapper

class testFullEM(unittest.TestCase):

  def setUp(self):
    """Create temporary directory and file handles for test runs of fullem.
    """
    
    self.original_dir    = os.getcwd()
    os.chdir(compClust.mlx.wrapper.__path__[0])
    self.temp_dir_name   = WrapperUtil.create_temporary_directory("fmtst")

    self.orig_tempdir    = tempfile.tempdir
    tempfile.tempdir     = self.temp_dir_name
    self.result_filename = tempfile.mktemp("result_file")

  def tearDown(self):
    """Clean up after ourselves.
    """
    tempfile.tempdir = self.orig_tempdir
    # FIXME: should this delete files that setUp did not create?
    
    try:
      os.remove(self.result_filename)
    except OSError, e:
      pass
    

    os.rmdir(self.temp_dir_name)

    os.chdir(self.original_dir)

  def testFullEM(self):
    print "Need to update FullEM to WrapperParameters"
    
# FIXME: Disabling FullEM as it needs to be updated to the new wrapper parameters
#    
#  def test_fullem_small_tree_reasonable_k(self):
#    """Test fullem using a small tree search for a number of clusters
#    that is noticably below the number of data points.
#
#    """
#    data_filename    = "test/synth_t_05c0_p_0075_d_03_v_0d1.txt"
#    self.param_filename = "test/fullEM.para"                
#    
#    argv=[sys.executable, "./FullEM.py", self.param_filename, data_filename, self.result_filename, " > /dev/null" ]
#    
#    os.system(string.join(argv, " "))
#    result = os.system("cmp -s test/fullem.small " + `self.result_filename`)
#
#    self.failIf(result != 0, "small tree failed")
#
#
#  def test_fullem_medium_tree_reasonable_k(self):
#    """Test fullem using a medium tree search for a number of clusters
#    that is noticably below the number of data points.
#
#    """
#    data_filename    = "test/synth_t_05c0_p_0750_d_03_v_0d1.txt"
#    self.param_filename = "test/fullEM.para"                
#
#    argv=[sys.executable, "./FullEM.py", self.param_filename, data_filename, self.result_filename, " > /dev/null" ]
#    
#    os.system(string.join(argv, " "))
#    result = os.system("cmp -s test/fullem.medium " + `self.result_filename`)
#
#    self.failIf(result != 0, "medium tree failed")
#
#  def test_fullem_large_tree_reasonable_k(self):
#    """Test fullem using a large tree search for a number of clusters
#    that is noticably below the number of data points.
#
#    """
#    data_filename     = "test/synth_t_05c0_p_7500_d_03_v_0d1.txt"
#    self.param_filename = "test/fullEM.para"                
#
#    argv=[sys.executable, "./FullEM.py", self.param_filename, data_filename, self.result_filename, "> /dev/null" ]
#    
#    os.system(string.join(argv, " "))
#    result = os.system("cmp -s test/fullem.large " + `self.result_filename`)
#
#    self.failIf(result != 0, "large tree failed")
#    
#  def test_initmeans(self):
#
#    self.param_filename = "test/fullEMInitMeansTest.para"
#    data_filename    = "test/synth_t_05c0_p_7500_d_03_v_0d1.txt"
#    
#    argv=[sys.executable, "./FullEM.py", self.param_filename, data_filename, self.result_filename, "> /dev/null" ]
#    
#    os.system(string.join(argv, " "))
#    result = os.system("cmp -s test/fullem.initmeans " + `self.result_filename`)
#
#    self.failIf(result != 0, "Init Means From File Failed")
    

def suite(**kw):
  print "FullEM needs to be updated"
  return None

  print "These tests will take several minutes to run..."
  
  suite = unittest.TestSuite()
  if os.environ.has_key("FULLEM_COMMAND"):
    suite.addTest(testFullEM("test_fullem_small_tree_reasonable_k"))
    suite.addTest(testFullEM("test_fullem_medium_tree_reasonable_k"))
    suite.addTest(testFullEM("test_fullem_large_tree_reasonable_k"))
    suite.addTest(testFullEM("test_initmeans"))
  else:
    print "FULLEM_COMMAND is not available, skipping fullem tests"
  
  return suite

if __name__ == "__main__":
  unittest.main(defaultTest="suite")




