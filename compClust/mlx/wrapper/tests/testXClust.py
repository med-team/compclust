#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

import inspect
import os
import string
import sys
import tempfile
import unittest

from compClust.config import config
from compClust.util import WrapperUtil
from compClust.util import Verify
from compClust.util.LoadExample import LoadCho

import compClust.mlx.wrapper
from compClust.mlx.wrapper import XClust

cho_agglomerated_k_5 = [['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['3'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['4'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['2'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['0'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['2'], ['1'], ['1'], ['1'], ['0'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['0'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1'], ['1']]


class testXClust(unittest.TestCase):

  def setUp(self):
    """Create temporary directory and file handles for test runs of xclust.
    """
    source = os.path.realpath(inspect.getsourcefile(testXClust))
    self.datadir = os.path.split(source)[0]
    self.executable=string.join([sys.executable,
                                 os.path.join(self.datadir,'..','XClust.py')])
    
    self.original_dir    = os.getcwd()
    os.chdir(compClust.mlx.wrapper.__path__[0])

    self.temp_dir_name   = WrapperUtil.create_temporary_directory("xctst")

    self.orig_tempdir    = tempfile.tempdir
    tempfile.tempdir     = self.temp_dir_name
    self.param_filename  = tempfile.mktemp("parameter_file")
    self.param_stream    = open(self.param_filename, "w")
    self.result_filename = tempfile.mktemp("result_file")

  def tearDown(self):
    """Clean up after ourselves.
    """
    tempfile.tempdir = self.orig_tempdir
    # FIXME: should this delete files that setUp did not create?
    
    try:
        os.remove(self.result_filename)
    except OSError, e:
        pass
    
    self.param_stream.close()
    os.remove(self.param_filename)
    os.rmdir(self.temp_dir_name)

    os.chdir(self.original_dir)
  
  def getParameters(self, k=5, 
                          cluster_on='rows', 
                          agglomerate_method='clusterNumber'):
    return {'k': k,
            'transform_method': 'none',
            'cluster_on': cluster_on,
            'agglomerate_method': agglomerate_method,
            'distance_metric': 'euclidean'
            }
    
#  def test_xclust_small_tree_reasonable_k(self):
#    """Test xclust using a small tree search for a number of clusters
#    that is noticably below the number of data points.
#
#    """
#    return None
#    parameters_dict = self.getParameters()
#    parameters = WrapperUtil.make_parameters_list(parameters_dict)
#
#    data_filename = os.path.join(self.datadir,
#                                 "synth_t_05c0_p_0075_d_03_v_0d1.txt")
#    result_filename = os.path.join(self.datadir, 'xclust.small')
#                        
#    self.param_stream.write(string.join(parameters, "\n"))
#    self.param_stream.close()
#
#    argv=[self.executable, self.param_filename, data_filename, self.result_filename, " > /dev/null" ]
#    
#    os.system(string.join(argv, " "))
#    result = os.system("cmp -s %s "%(self.result_filename) + `self.result_filename`)
#
#    self.failIf(result != 0, "small tree failed")
#
#
#  def test_xclust_medium_tree_reasonable_k(self):
#    """Test xclust using a medium tree search for a number of clusters
#    that is noticably below the number of data points.
#
#    """
#    return None
#    parameters_dict = self.getParameters()
#    parameters = WrapperUtil.make_parameters_list(parameters_dict)
#
#    data_filename = os.path.join(self.datadir,
#                                 "test/synth_t_05c0_p_0750_d_03_v_0d1.txt")
#    result_filename = os.path.join(self.datadir, 'xclust.medium')
#                        
#    self.param_stream.write(string.join(parameters, "\n"))
#    self.param_stream.close()
#
#    argv=[self.executable, self.param_filename, data_filename, self.result_filename, " > /dev/null" ]
#    
#    os.system(string.join(argv, " "))
#    result = os.system("cmp -s %s "%(self.result_filename) + `self.result_filename`)
#    result = os.system("cmp -s test/xclust.medium " + `self.result_filename`)
#
#
#  def test_xclust_large_tree_reasonable_k(self):
#    """
#    Test xclust using a large tree search for a number of clusters
#    that is noticably below the number of data points.
#    """
#    return None # This test is a bit slow, so skip it
#  
#    parameters_dict = self.getParameters()
#    parameters = WrapperUtil.make_parameters_list(parameters_dict)
#
#    data_filename = os.path.join(self.datadir,
#                                 "synth_t_05c0_p_7500_d_03_v_0d1.txt")
#    result_filename = os.path.join(self.datadir, 'xclust.large')
#                        
#    self.param_stream.write(string.join(parameters, "\n"))
#    self.param_stream.close()
#
#    argv=[self.executable, self.param_filename, data_filename, self.result_filename, "> /dev/null" ]
#    
#    os.system(string.join(argv, " "))
#    result = os.system("cmp -s %s "%(self.result_filename) + `self.result_filename`)
#
#    self.failIf(result != 0, "large tree failed")

  def testAgglomeratedXClustPythonAPI(self):
    """Can we get an agglomerated label back using the wrapper class?

    This is more of a regression test than a unit test, but there's 
    only so much we can do
    """
    return None
    dataset = LoadCho()
    parameters = self.getParameters()

    algorithm = XClust(dataset, parameters)
    algorithm.run()

    model = algorithm.getModel()
    self.failUnless(hasattr(model, 'evaluateFitness'))
    
    labels = algorithm.getLabeling().getAllRowLabels()
    self.failUnless(len(labels) == len(cho_agglomerated_k_5))
    for i in xrange(len(labels)):
      self.failUnless(labels[i][0] == cho_agglomerated_k_5[i][0])

    
  def testXClustPythonAPI(self):
    """Can we get an label back using the wrapper class?

    without agglomerating we should just get back an ordering
    of the dataset
    """
    dataset = LoadCho()
    parameters = self.getParameters(agglomerate_method="none")

    algorithm = XClust(dataset, parameters)
    algorithm.run()

    #self.failUnless(algorithm.getModel() is None)
    #labeling = algorithm.getLabeling()
    #print "model", model
    #print "labeling", labeling
    
    
def suite(**kw):
  if config.xclust_command is not None:
    return unittest.makeSuite(testXClust)
  else:
    print "xclust is not available, xclust tests skipped"
    return unittest.TestSuite()

if __name__ == "__main__":
  unittest.main(defaultTest="suite")


