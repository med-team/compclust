########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
#                Christopher Hart
# Last Modified: Dec 13 23:41:29 PST 2001
#

import operator

from compClust.mlx.views import SubsetView


###############################################################################
#
# ColumnSubsetView
#
# Subclass of SubsetView provides subsets on the column data
#
###############################################################################

class ColumnSubsetView(SubsetView):
  """
  View a subset of the columns of a dataset

  A ColumnSubsetView is concerned only with the columns of a dataset.  The view
  created contains the columns of the the original dataset with the keys
  provided in the keyList.  The view maintains the order of te keyList and
  allows for duplicate keys.  If non-column keys exist in the keylist, a
  KeyError() is raised.

  A ColumnSubsetView is a subclass of the SubsetView.  
  """

  def __init__(self, dataset, colList, name=None):

    numRows  = dataset.getNumRows()
    numCols  = dataset.getNumCols()
    
    sanity = [ x for x in colList if 0 > x or x >= numCols ]
    
    if len(sanity) != 0:
      raise KeyError()

    # include all the rows for a column subset
    keyList = dataset.getRowKeys()
    #offset the column ids to the column key values
    keyList += [ x+numRows for x in colList ]
    SubsetView.__init__(self, dataset, keyList, name=name)
