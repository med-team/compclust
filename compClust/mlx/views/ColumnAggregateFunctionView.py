########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
#                Christopher Hart
# Last Modified: Dec 13 23:41:29 PST 2001
#

from compClust.util.unique import unique
from compClust.mlx.views import AggregateFunctionView

class ColumnAggregateFunctionView(AggregateFunctionView):

  def __init__(self, dataset, labeling, function, name=None):

    AggregateFunctionView.__init__(self, dataset, [], function, name=name)
    self.setLabeling(labeling)

  def setLabeling(self, labeling):

    self.__labeling = labeling
    self.dirty = 1
    self._makeDirtyChildren()
    self._refresh()
    
  def _refresh(self):
    
    if self.dataset.isDirty():
      self.dataset._refresh()

    labeling = self.__labeling
    keylist  = []

    if labeling is not None:
      rows = self.dataset.getNumRows()
      keys = map(labeling.getKeysByLabel, unique(labeling.getLabelByCols()))

      for keyset in keys:
        keylist.append(filter(lambda x : x >= rows, keyset))

    self.setKeylist(keylist)
    
    self.dirty = 0

