########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
#                Christopher Hart
# Last Modified: Dec 13 23:41:29 PST 2001
#
import numpy

from compClust.mlx.views import SubsetView

##############################################################################
#
# Transpose View
#
# Simply performs a transpose of the given view.
#
##############################################################################

class TransposeView(SubsetView):
  """
  Returns the data transposed

  This class inherits from SubsetView because the key manipulations it must do
  are very similar.
  """

  def __init__(self, dataset, name=None):
    all_keys = dataset.getRowKeys() + dataset.getColKeys()
    SubsetView.__init__(self, dataset, name=name)


  def getNumRows(self):
    if self.isDirty():
      self._refresh()
    return self.dataset.getNumCols()


  def getNumCols(self):
    if self.isDirty():
      self._refresh()
    return self.dataset.getNumRows()

    
  def _mapKeysFromParent(self, keyList, parent=None):

    #
    # Convert from a parent's key value to our key
    #
    
    numCols = self.getNumCols()
    numRows = self.getNumRows()
    total = numRows + numCols
    
    return map(lambda x : x + numRows - (x >= numCols)*total, keyList)
    
  def _translateKey(self, key):

    #
    # Convert from our key to the parent 
    #

    numCols = self.getNumCols()
    numRows = self.getNumRows()

    if key < numRows:
      return key + numCols
    else:
      return key - numRows
    
  def getData(self, key=None):

    if self.isDirty():
      self._refresh()
      
    v = None
    if key is None:
      v = numpy.transpose(self.dataset.getData())
    
    else:
      
      keyMax = self.getKeyMax()
      if key < 0 or key >= keyMax:
        raise ValueError()

      numRows = self.getNumRows()
      numCols = self.getNumCols()
      
      if key < numRows:

        #
        # Get a column from the original dataset
        #

        v = self.dataset.getData(numCols + key)

      else:
        
        #
        # Get a row from the original dataset
        #
        
        v = self.dataset.getData(key - numRows)

    return v
