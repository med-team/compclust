########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
#                Christopher Hart
# Last Modified: Dec 13 23:41:29 PST 2001
#

import numpy

from compClust.mlx.views import BaseView

###############################################################################
#
# FunctionView
#
# Applies a unary function to each element of the underlying dataset before
# returning it.  Possible functions could be log(), exp(), negation,
# or a lambda x : ???? function
#
# This class does not need to override _mapKeysToParent
#
###############################################################################

class FunctionView(BaseView):
  """
  Provides a view of a dataset filtered by a unary function.

  This view is useful for doing simple operations such as applying the
  log() function to the dataset, or adding a constant to all the values.
  """

  def __init__(self, dataset, function):

    self.func = function
    BaseView.__init__(self, dataset)

  def getData(self, key=None):

    if self.isDirty():
      self._refresh()

    data = self.dataset.getData(key)
    if self.func is not None:
      data = numpy.reshape(map(self.func, numpy.ravel(data)), data.shape)

    return data
