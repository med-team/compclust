########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
#                Christopher Hart
# Last Modified: Dec 13 23:41:29 PST 2001
#
import numpy

from compClust.mlx.views import BaseView

###############################################################################
#
# ColumnFunctionView
#
# Applies a function per column of a dataset.  The function should accept and
# return an N-element vector which is equal to the number of rows.
#
# This class does not need to override _mapKeysToParent
#
###############################################################################

class ColumnFunctionView(BaseView):

  """ Provides a view of a dataset passed through column-wise
  function. This function should expect a dataset and a column and
  return an arrray of length numCols

  """

  def __init__(self, dataset1, function, name=None):

    self.func = function
    BaseView.__init__(self, dataset1, name=name)

  def getData(self, key=None):

    if self.isDirty():
      self._refresh()
      
    if self.func is not None:
      numRows = self.dataset.getNumRows()
      numCols = self.dataset.getNumCols()
      if key is None:
        data = numpy.transpose(map(self.func , [self.dataset]*numCols, range(numCols)))
      elif key < numRows:
        data = self.getData()[key,:]
      elif key >= numRows and key < self.dataset.getKeyMax():
        data = self.func(self.dataset, key-numRows)
      else:
        raise ValueError
      

      
    return data
