########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
#                Christopher Hart
# Last Modified: Dec 13 23:41:29 PST 2001
#

from compClust.mlx.views import BaseView

class FilledMaskedArrayView(BaseView):

  """
  FilledMaskedArrayView(BaseView)
  
  This class simply replaces all masked values with a proper fill
  value (using the MA functionality).  This should be extended to
  provide more complete interpolation functions
  """

  def __init__(self, dataset, fillValue=None, name=None):

    BaseView.__init__(self, dataset, name=name)
    if fillValue is not None:
      self.setFillValue(fillValue)
    
    
  def getData(self, key=None):

    if self.isDirty():
      self._refresh()

    data = self.dataset.getData(key)
    
    return (data.filled())

  def setFillValue(self, value):

    self.dataset.getData().set_fill_value (value)
    self.dirty = 1
    self._makeDirtyChildren()
