########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
#                Christopher Hart
# Last Modified: Dec 13 23:41:29 PST 2001
#
import numpy

from compClust.mlx.views import TransformView
##############################################################################
#
# RowPCAView
#
# This class does not need to override _mapKeysToParent
#
##############################################################################

class RowPCAView(TransformView):

  """
  Provides a PCA rotated view of the row space of a dataset.
  """

  def __init__(self, dataset, name=None):

    TransformView.__init__(self, dataset, name=name)
    self.__variances = []
    self._refresh()
    
  def _refresh(self):

    if self.dataset.isDirty():
      self.dataset._refresh()
      
    #
    # Compute the SVD of the parent's data and use the rotation matrix
    #
    
    U, S, V = numpy.linalg.svd(numpy.cov(self.dataset.getData(), rowvar=0))
    self.setMatrix(U)
    
    #
    # Create a list of the variances
    #

    self.__variances = S / numpy.sum(S, axis=0)

    self.dirty = 0

  def getVariances(self):
    return self.__variances

