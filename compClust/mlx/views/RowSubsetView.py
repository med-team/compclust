########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
#                Christopher Hart
# Last Modified: Dec 13 23:41:29 PST 2001
#

from compClust.mlx.views import SubsetView

###############################################################################
#
# RowSubsetView
#
# Subclass of SubsetView provides subsets on the row data
#
###############################################################################

class RowSubsetView(SubsetView):
  """
  View a subset of the rows of a dataset

  A RowSubsetView is concerned only with the rows of a dataset.  The view
  created contains the rows of the the original dataset with the keys
  provided in the keyList.  The view maintains the order of te keyList and
  allows for duplicate keys.  If non-column keys exist in the keylist, a
  KeyError() is raised.

  A RowSubsetView is a subclass of the SubsetView.  
  
  If rowList is empty, all the rows will be included in the subset
  """
  
  def __init__(self, dataset, rowList, name=None):

    numRows = dataset.getNumRows()
    sanity = [ x for x in rowList if 0 > x or x >= numRows ]

    if len(sanity) != 0:
      raise KeyError("Rowids %s exceeded the number of rows %s"%\
                     (sanity,numRows))

    # include all the columns for the rows in our row subset
    all_keys = rowList + dataset.getColKeys()
    #
    # row numbers == key numbers
    #
    SubsetView.__init__(self, dataset, all_keys, name=name)
