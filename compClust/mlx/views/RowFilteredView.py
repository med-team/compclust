########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
#                Christopher Hart
# Last Modified: Dec 13 23:41:29 PST 2001
#

from compClust.mlx.views import RowSubsetView

#############################################################################
#
# Filtered Views.  
#
############################################################################

class RowFilteredView(RowSubsetView):

  """
  View a subset of rows of a dataset based on a filter.

  A RowFilteredView is concerned only with the rows of a dataset.  The
  view created contains the rows of the the original dataset which are
  allowed via the filter function. If no filter is provided then all
  rows are included in the view.  RowFilteredView is derived from
  RowSubsetView.  The filter function should be written such that it
  takes a dataset and a row and returns either 1 or 0 based on if it
  is to keep or hide the row.  See examples below.

  FilterFunction Prototype:

  FilterFunc(ds, row)
    ds is a dataset object
    row is an int.
    returns 0 or 1

  example:

  this creates a filtered view where all rows have a mean > 1

  >>> filteredDS = View.RowFilteredView (ds, lambda ds, row: mean(ds.getRowData(row) )> 1)

  # this changes the view to have all genes which match the regexp
  # cool (case insensitive) inside the label in the labeling named
  # 'names'

  >>> filteredDS.setFilter(lambda ds, row: re.compile('cool', re.I).match(ds.getLabeling('names').getLabelsByRow(row)[0]))

  """

  def __init__(self, dataset, filterFunc=None, name=None):

    RowSubsetView.__init__(self, dataset, range(dataset.getNumRows()), name=name)
    self.setFilter(filterFunc)
    
  def setFilter(self, filterFunc=None):

    """
    setFilter(self, filterFunc=None)
    
    Update the Filtered View with a new filter function.  If filter function
    is None, all data is passed through.

    Below is the FilterFunction Prototype, see the class doc-string
    for examples and more information:
    
    FilterFunc(ds, row)
      ds is a dataset object
      row is an int.
      returns 0 or 1

    """

    self.__filterFunc = filterFunc
    self.dirty = 1
    self._makeDirtyChildren()

    
  def _refresh(self):
    """
    Fill in the variables defined in RowSubsetView (and thus, SubsetView),
    to make this FilteredView appear correct.
    """

    ds = self.dataset

    if ds.isDirty():
      ds._refresh()

    filterFunc = self.__filterFunc
    
    #
    # The filtered view is not constrained by the original size of the
    # parent dataset when it was created.  If the parent grows larger,
    # we grow with it.  Thus we need to probe the valid rows every time
    
    numRows = ds.getNumRows()
    numCols = ds.getNumCols()

    self.rowKeys = ds.getRowKeys()
    if filterFunc is not None:
      self.rowKeys = filter(lambda row : filterFunc(ds, row), self.rowKeys)
    self.numRows = len(self.rowKeys)

    self.colKeys = filter(lambda x : x < numCols, self.base_cols)
    self.colKeys = map(lambda x : x + numRows, self.colKeys)
    self.numCols = len(self.colKeys)

    self.RKLUcache = None
    self.dirty = 0
    
