[Setup]
AppCopyright=2004 � California Institute of Technology
AppName=CompClustTk
AppVerName=CompClustTk 0.2.15
LicenseFile=C:\proj\compClust\compClust\gui\dist\LICENSE.txt
RestartIfNeededByRun=false
DefaultDirName={pf}\CompClustTk
DefaultGroupName=CompClustTK
ShowLanguageDialog=yes
AppPublisher=Wold Lab
AppPublisherURL=http://woldlab.caltech.edu/compClust/
AppVersion=0.2.15
AppID={{16DD41F5-5E87-48A4-9425-52D9FA6B01AC}
UninstallDisplayName=CompClustTk
OutputBaseFilename=CompClustTk-0.2.15-Docs-0.1.10
[Icons]
Name: {group}\CompClustTk; Filename: {app}\CompClustTk.exe; IconFilename: {app}\compclust2.ico; IconIndex: 0; WorkingDir: {app}
Name: {group}\Uninstall CompClustTk; Filename: {uninstallexe}
Name: {group}\CompClust - WWW; Filename: {app}\CompClust Web Site.url
Name: {group}\Wold Lab - WWW; Filename: {app}\Wold Lab.url
Name: {commondesktop}\CompClustTk; Filename: {app}\CompClustTk.exe; IconFilename: {app}\compclust2.ico; IconIndex: 0; WorkingDir: {app}
[INI]
Filename: {app}\CompClust Web Site.url; Section: InternetShortcut; Key: URL; String: http://woldlab.caltech.edu/compClust/
Filename: {app}\Wold Lab.url; Section: InternetShortcut; Key: URL; String: http://woldlab.caltech.edu/
Filename: {app}\compClust.ini; Section: Clustering Algorithms; Key: KMEANS_COMMAND; String: {app}\bin\kmeans.exe; Flags: createkeyifdoesntexist uninsdeletesection
Filename: {app}\compClust.ini; Section: Clustering Algorithms; Key: DIAGEM_COMMAND; String: {app}\bin\diagem.exe; Flags: createkeyifdoesntexist uninsdeletesection
Filename: {app}\compClust.ini; Section: Clustering Algorithms; Key: WMATCH_COMMAND; String: {app}\bin\wmatch.exe; Flags: createkeyifdoesntexist uninsdeletesection
Filename: {app}\compClust.ini; Section: Clustering Algorithms; Key: WMATCH_FULL_COMMAND; String: {app}\bin\wfull.exe; Flags: createkeyifdoesntexist uninsdeletesection
Filename: {app}\compClust.ini; Section: Help; Key: Tutorial; String: {app}\Docs\compclusttk_tutorial\compclusttk_tutorial.html; Flags: createkeyifdoesntexist uninsdeletesection
Filename: {app}\compClust.ini; Section: Icons; Key: MainIcon; String: {app}\compclust2.ico; Flags: createkeyifdoesntexist uninsdeleteentry uninsdeletesectionifempty
[UninstallDelete]
Type: files; Name: {app}\CompClust Web Site.url
Type: files; Name: {app}\Wold Lab.url
Name: {app}\compClust.ini; Type: files
[Files]
Source: dist\BLT24.dll; DestDir: {sys}; Flags: sharedfile comparetimestamp confirmoverwrite promptifolder
Source: dist\BLTlite24.dll; DestDir: {sys}; Flags: sharedfile comparetimestamp confirmoverwrite promptifolder
Source: dist\CompClustTk.exe; DestDir: {app}
Source: dist\library.zip; DestDir: {app}
Source: dist\LICENSE.txt; DestDir: {app}
Source: dist\python24.dll; DestDir: {app}
Source: dist\tcl84.dll; DestDir: {app}
Source: dist\tk84.dll; DestDir: {app}
Source: dist\umath.pyd; DestDir: {app}
Source: dist\w9xpopen.exe; DestDir: {app}
Source: dist\tcl\tcl8.4\encoding\ascii.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\big5.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp1250.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp1251.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp1252.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp1253.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp1254.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp1255.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp1256.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp1257.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp1258.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp437.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp737.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp775.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp850.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp852.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp855.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp857.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp860.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp861.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp862.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp863.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp864.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp865.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp866.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp869.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp874.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp932.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp936.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp949.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\cp950.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\dingbats.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\ebcdic.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\euc-cn.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\euc-jp.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\euc-kr.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\gb12345.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\gb1988.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\gb2312-raw.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\gb2312.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso2022-jp.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso2022-kr.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso2022.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-1.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-10.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-13.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-14.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-15.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-16.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-2.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-3.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-4.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-5.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-6.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-7.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-8.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\iso8859-9.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\jis0201.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\jis0208.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\jis0212.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\koi8-r.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\koi8-u.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\ksc5601.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macCentEuro.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macCroatian.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macCyrillic.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macDingbats.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macGreek.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macIceland.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macJapan.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macRoman.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macRomania.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macThai.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macTurkish.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\macUkraine.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\shiftjis.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\symbol.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\encoding\tis-620.enc; DestDir: {app}\tcl\tcl8.4\encoding
Source: dist\tcl\tcl8.4\http1.0\http.tcl; DestDir: {app}\tcl\tcl8.4\http1.0
Source: dist\tcl\tcl8.4\http1.0\pkgIndex.tcl; DestDir: {app}\tcl\tcl8.4\http1.0
Source: dist\tcl\tcl8.4\http2.4\http.tcl; DestDir: {app}\tcl\tcl8.4\http2.4
Source: dist\tcl\tcl8.4\http2.4\pkgIndex.tcl; DestDir: {app}\tcl\tcl8.4\http2.4
Source: dist\tcl\tcl8.4\msgcat1.3\msgcat.tcl; DestDir: {app}\tcl\tcl8.4\msgcat1.3
Source: dist\tcl\tcl8.4\msgcat1.3\pkgIndex.tcl; DestDir: {app}\tcl\tcl8.4\msgcat1.3
Source: dist\tcl\tcl8.4\opt0.4\optparse.tcl; DestDir: {app}\tcl\tcl8.4\opt0.4
Source: dist\tcl\tcl8.4\opt0.4\pkgIndex.tcl; DestDir: {app}\tcl\tcl8.4\opt0.4
Source: dist\tcl\tcl8.4\tcltest2.2\pkgIndex.tcl; DestDir: {app}\tcl\tcl8.4\tcltest2.2
Source: dist\tcl\tcl8.4\tcltest2.2\tcltest.tcl; DestDir: {app}\tcl\tcl8.4\tcltest2.2
Source: dist\tcl\tcl8.4\auto.tcl; DestDir: {app}\tcl\tcl8.4
Source: dist\tcl\tcl8.4\history.tcl; DestDir: {app}\tcl\tcl8.4
Source: dist\tcl\tcl8.4\init.tcl; DestDir: {app}\tcl\tcl8.4
Source: dist\tcl\tcl8.4\ldAout.tcl; DestDir: {app}\tcl\tcl8.4
Source: dist\tcl\tcl8.4\package.tcl; DestDir: {app}\tcl\tcl8.4
Source: dist\tcl\tcl8.4\parray.tcl; DestDir: {app}\tcl\tcl8.4
Source: dist\tcl\tcl8.4\safe.tcl; DestDir: {app}\tcl\tcl8.4
Source: dist\tcl\tcl8.4\tclIndex; DestDir: {app}\tcl\tcl8.4
Source: dist\tcl\tcl8.4\word.tcl; DestDir: {app}\tcl\tcl8.4
Source: dist\tcl\tk8.4\demos\images\earth.gif; DestDir: {app}\tcl\tk8.4\demos\images
Source: dist\tcl\tk8.4\demos\images\earthris.gif; DestDir: {app}\tcl\tk8.4\demos\images
Source: dist\tcl\tk8.4\demos\images\face.bmp; DestDir: {app}\tcl\tk8.4\demos\images
Source: dist\tcl\tk8.4\demos\images\flagdown.bmp; DestDir: {app}\tcl\tk8.4\demos\images
Source: dist\tcl\tk8.4\demos\images\flagup.bmp; DestDir: {app}\tcl\tk8.4\demos\images
Source: dist\tcl\tk8.4\demos\images\gray25.bmp; DestDir: {app}\tcl\tk8.4\demos\images
Source: dist\tcl\tk8.4\demos\images\letters.bmp; DestDir: {app}\tcl\tk8.4\demos\images
Source: dist\tcl\tk8.4\demos\images\noletter.bmp; DestDir: {app}\tcl\tk8.4\demos\images
Source: dist\tcl\tk8.4\demos\images\pattern.bmp; DestDir: {app}\tcl\tk8.4\demos\images
Source: dist\tcl\tk8.4\demos\images\tcllogo.gif; DestDir: {app}\tcl\tk8.4\demos\images
Source: dist\tcl\tk8.4\demos\images\teapot.ppm; DestDir: {app}\tcl\tk8.4\demos\images
Source: dist\tcl\tk8.4\demos\arrow.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\bind.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\bitmap.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\browse; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\button.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\check.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\clrpick.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\colors.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\cscroll.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\ctext.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\dialog1.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\dialog2.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\entry1.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\entry2.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\entry3.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\filebox.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\floor.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\form.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\hello; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\hscale.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\icon.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\image1.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\image2.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\items.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\ixset; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\label.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\labelframe.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\license.terms; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\menu.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\menubu.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\msgbox.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\paned1.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\paned2.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\plot.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\puzzle.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\radio.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\README; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\rmt; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\rolodex; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\ruler.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\sayings.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\search.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\spin.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\square; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\states.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\style.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\tclIndex; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\tcolor; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\text.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\timer; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\twind.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\unicodeout.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\vscale.tcl; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\demos\widget; DestDir: {app}\tcl\tk8.4\demos
Source: dist\tcl\tk8.4\images\logo.eps; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\logo100.gif; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\logo64.gif; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\logoLarge.gif; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\logoMed.gif; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\pwrdLogo.eps; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\pwrdLogo100.gif; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\pwrdLogo150.gif; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\pwrdLogo175.gif; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\pwrdLogo200.gif; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\pwrdLogo75.gif; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\README; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\images\tai-ku.gif; DestDir: {app}\tcl\tk8.4\images
Source: dist\tcl\tk8.4\msgs\cs.msg; DestDir: {app}\tcl\tk8.4\msgs
Source: dist\tcl\tk8.4\msgs\de.msg; DestDir: {app}\tcl\tk8.4\msgs
Source: dist\tcl\tk8.4\msgs\el.msg; DestDir: {app}\tcl\tk8.4\msgs
Source: dist\tcl\tk8.4\msgs\en.msg; DestDir: {app}\tcl\tk8.4\msgs
Source: dist\tcl\tk8.4\msgs\en_gb.msg; DestDir: {app}\tcl\tk8.4\msgs
Source: dist\tcl\tk8.4\msgs\es.msg; DestDir: {app}\tcl\tk8.4\msgs
Source: dist\tcl\tk8.4\msgs\fr.msg; DestDir: {app}\tcl\tk8.4\msgs
Source: dist\tcl\tk8.4\msgs\it.msg; DestDir: {app}\tcl\tk8.4\msgs
Source: dist\tcl\tk8.4\msgs\nl.msg; DestDir: {app}\tcl\tk8.4\msgs
Source: dist\tcl\tk8.4\msgs\ru.msg; DestDir: {app}\tcl\tk8.4\msgs
Source: dist\tcl\tk8.4\bgerror.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\button.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\choosedir.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\clrpick.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\comdlg.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\console.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\dialog.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\entry.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\focus.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\license.terms; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\listbox.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\menu.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\mkpsenc.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\msgbox.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\obsolete.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\optMenu.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\palette.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\panedwindow.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\pkgIndex.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\safetk.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\scale.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\scrlbar.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\spinbox.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\tclIndex; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\tearoff.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\text.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\tk.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\tkfbox.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\unsupported.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\tcl\tk8.4\xmfbox.tcl; DestDir: {app}\tcl\tk8.4
Source: dist\_numpy.pyd; DestDir: {app}
Source: dist\select.pyd; DestDir: {app}
Source: dist\bin\diagem.exe; DestDir: {app}\bin
Source: dist\bin\wmatch.exe; DestDir: {app}\bin
Source: dist\bin\wfull.exe; DestDir: {app}\bin
Source: dist\bin\kmeans.exe; DestDir: {app}\bin
Source: dist\Examples\ChoCellCycling\times.clab; DestDir: {app}\Examples\ChoCellCycling
Source: dist\Examples\ChoCellCycling\ChoClassification.rlab; DestDir: {app}\Examples\ChoCellCycling
Source: dist\Examples\ChoCellCycling\ChoCycling.dat; DestDir: {app}\Examples\ChoCellCycling
Source: dist\Examples\ChoCellCycling\CommonNames.rlab; DestDir: {app}\Examples\ChoCellCycling
Source: dist\Examples\ChoCellCycling\EM.rlab; DestDir: {app}\Examples\ChoCellCycling
Source: dist\Examples\ChoCellCycling\ORFs.rlab; DestDir: {app}\Examples\ChoCellCycling
Source: dist\Examples\ChoCellCycling\README; DestDir: {app}\Examples\ChoCellCycling
Source: dist\Docs\compclusttk_tutorial\WARNINGS; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\compclusttk_tutorial.css; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\compclusttk_tutorial.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\footnode.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\images.aux; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\images.log; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\images.pl; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\images.tex; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img1.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img2.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img3.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img4.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img5.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img6.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img7.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img8.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img9.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img10.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img11.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img12.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img13.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img14.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img15.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img16.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img17.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img18.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img19.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img20.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img21.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\img22.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\index.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\internals.pl; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\labels.pl; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node1.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node2.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node3.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node4.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node5.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node6.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node7.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node8.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node9.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node10.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node11.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node12.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node13.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node14.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node15.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node16.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node17.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node18.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node19.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node20.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node21.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node22.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node23.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node24.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node25.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node26.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node27.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node28.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node29.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node30.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node31.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node32.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node33.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node34.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node35.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node36.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node37.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node38.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node39.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node40.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node41.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node42.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node43.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node44.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node45.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node46.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node47.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\node48.html; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\prev_g.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\up_g.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\contents.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\next.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\up.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\crossref.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\next_g.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\Docs\compclusttk_tutorial\prev.png; DestDir: {app}\Docs\compclusttk_tutorial
Source: dist\zlib.pyd; DestDir: {app}
Source: dist\_ctypes.pyd; DestDir: {app}
Source: dist\_dotblas.pyd; DestDir: {app}
Source: dist\_na_contour.pyd; DestDir: {app}
Source: dist\_na_image.pyd; DestDir: {app}
Source: dist\_na_transforms.pyd; DestDir: {app}
Source: dist\_nc_contour.pyd; DestDir: {app}
Source: dist\_nc_image.pyd; DestDir: {app}
Source: dist\_nc_transforms.pyd; DestDir: {app}
Source: dist\_numpy.pyd; DestDir: {app}
Source: dist\_socket.pyd; DestDir: {app}
Source: dist\_ssl.pyd; DestDir: {app}
Source: dist\_tkinter.pyd; DestDir: {app}
Source: dist\cDA.pyd; DestDir: {app}
Source: dist\fftpack.pyd; DestDir: {app}
Source: dist\ft2font.pyd; DestDir: {app}
Source: dist\graphmatching.pyd; DestDir: {app}
Source: dist\lapack_lite.pyd; DestDir: {app}
Source: dist\multiarray.pyd; DestDir: {app}
Source: dist\ranlib.pyd; DestDir: {app}
Source: dist\select.pyd; DestDir: {app}
Source: dist\umath.pyd; DestDir: {app}
Source: dist\tcl\tcl8.4\blt2.4\treeview.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\bltCanvEps.pro; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\bltGraph.pro; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\dnd.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\dragdrop.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\graph.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\hierbox.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\NEWS; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\pkgIndex.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\PROBLEMS; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\README; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\tabnotebook.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\tabset.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\tclIndex; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\treeview.cur; DestDir: {app}\tcl\tcl8.4\blt2.4
Source: dist\tcl\tcl8.4\blt2.4\dd_protocols\tclIndex; DestDir: {app}\tcl\tcl8.4\blt2.4\dd_protocols
Source: dist\tcl\tcl8.4\blt2.4\dd_protocols\dd-color.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\dd_protocols
Source: dist\tcl\tcl8.4\blt2.4\dd_protocols\dd-file.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\dd_protocols
Source: dist\tcl\tcl8.4\blt2.4\dd_protocols\dd-number.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\dd_protocols
Source: dist\tcl\tcl8.4\blt2.4\dd_protocols\dd-text.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\dd_protocols
Source: dist\tcl\tcl8.4\blt2.4\html\winop.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\barchart.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\beep.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\bgexec.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\bitmap.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\BLT.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\bltdebug.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\busy.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\cutbuffer.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\dragdrop.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\eps.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\graph.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\hierbox.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\hiertable.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\htext.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\spline.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\stripchart.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\table.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\tabset.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\tile.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\tree.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\treeview.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\vector.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\html\watch.html; DestDir: {app}\tcl\tcl8.4\blt2.4\html
Source: dist\tcl\tcl8.4\blt2.4\demos\winop2.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\barchart1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\barchart2.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\barchart3.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\barchart4.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\barchart5.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\bgexec1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\bgexec2.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\bgexec3.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\bgexec4.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmap.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\busy1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\dragdrop1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\dragdrop2.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\eps.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\graph1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\graph2.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\graph3.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\graph4.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\graph5.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\graph6.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\graph7.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\hierbox1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\hierbox2.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\hierbox3.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\hierbox4.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\hiertable1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\hiertable2.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\hiertable3.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\htext1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\htext.txt; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\spline.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\stripchart1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\tabnotebook1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\tabnotebook2.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\tabnotebook3.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\tabset1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\tabset2.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\tabset3.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\tabset4.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\treeview1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\winop1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\xbob.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\face.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\greenback.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hobbes.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hobbes_mask.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\sharky.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\fish\rightm.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\fish
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\fish\left1.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\fish
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\fish\left1m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\fish
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\fish\left.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\fish
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\fish\leftm.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\fish
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\fish\mid.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\fish
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\fish\midm.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\fish
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\fish\right1.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\fish
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\fish\right1m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\fish
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\fish\right.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\fish
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand14m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand01.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand01m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand02.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand02m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand03.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand03m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand04.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand04m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand05.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand05m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand06.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand06m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand07.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand07m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand08.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand08m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand09.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand09m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand10.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand10m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand11.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand11m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand12.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand12m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand13.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand13m.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\bitmaps\hand\hand14.xbm; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Source: dist\tcl\tcl8.4\blt2.4\demos\images\txtrflag.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\blt98.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\buckskin.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\chalk.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\close2.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\close.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\clouds.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\corrugated_metal.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\folder.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\mini-book1.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\mini-book2.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\mini-display.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\mini-doc.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\mini-filemgr.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\mini-ofolder.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\mini-windows.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\ofolder.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\open2.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\open.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\out.ps; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\qv100.t.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\rain.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\sample.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\smblue_rock.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\stopsign.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\tan_paper2.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\images\tan_paper.gif; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\images
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\xcolors.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\barchart2.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\bgtest.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\clone.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\demo.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\globe.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\graph1.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\graph2.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\graph3.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\graph5.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\graph8.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\page.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\patterns.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\ps.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\send.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\tcl\tcl8.4\blt2.4\demos\scripts\stipples.tcl; DestDir: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Source: dist\compclust2.ico; DestDir: {app}
Source: dist\compclust.ico; DestDir: {app}
[Dirs]
Name: {app}\tcl
Name: {app}\tcl\tcl8.4
Name: {app}\tcl\tcl8.4\blt2.4
Name: {app}\tcl\tcl8.4\blt2.4\dd_protocols
Name: {app}\tcl\tcl8.4\blt2.4\demos
Name: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps
Name: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\fish
Name: {app}\tcl\tcl8.4\blt2.4\demos\bitmaps\hand
Name: {app}\tcl\tcl8.4\blt2.4\demos\images
Name: {app}\tcl\tcl8.4\blt2.4\demos\scripts
Name: {app}\tcl\tcl8.4\blt2.4\html
Name: {app}\tcl\tcl8.4\encoding
Name: {app}\tcl\tcl8.4\http1.0
Name: {app}\tcl\tcl8.4\http2.4
Name: {app}\tcl\tcl8.4\msgcat1.3
Name: {app}\tcl\tcl8.4\opt0.4
Name: {app}\tcl\tcl8.4\tcltest2.2
Name: {app}\tcl\tk8.4
Name: {app}\tcl\tk8.4\demos
Name: {app}\tcl\tk8.4\demos\images
Name: {app}\tcl\tk8.4\images
Name: {app}\tcl\tk8.4\msgs
Name: {app}\Examples
Name: {app}\Examples\ChoCellCycling
Name: {app}\Docs
Name: {app}\Docs\compclusttk_tutorial
