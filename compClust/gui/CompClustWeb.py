"""Use compclust from the web

TODO:
  be able to save datasources for later loading
  be able to browse saved datasources and load them

  xml-rpc/functional interface to add/remove stuff
TODO:
"""
import operator
import os
import math
import re
import shelve
import string
import StringIO
import sys
import time
import traceback
import types
import urllib
#import warnings
#from warnings import warn

# make sure we have a home
if not os.environ.has_key('HOME'):
  os.environ['HOME'] = '/tmp'

from quixote.errors import *
from quixote.html import htmlescape
from quixote import get_session_manager

from simpletal import simpleTAL, simpleTALES, simpleTALUtils

from compClust.util import DistanceMetrics

from compClust.iplot import IPlotAgg as IPlot
from compClust.iplot.PlotPage import PlotPage

from compClust.gui.PlotCache import PlotCache
from compClust.gui.DataSource import DataSource
from compClust.gui.DataManager import DataManager

from compClust.gui.ClusteringUI import ClusteringManagerUI
from compClust.gui.PlotUI import PlotUI
from compClust.gui.DatamanagerUI import DatamanagerUI
                                        
import compClust.gui.DataManager
# Setup warning manager
#class DebugWarning(Warning): pass
#class InfoWarning(Warning): pass
#
#def showwarning(message, category, filename, linenumber, f=warnfile):
#  """Write warnings to our favorite location
#  """
#  if type(f) in types.StringTypes:
#    f = open(f, 'a')
#    
#  timestamp = time.strftime("%Y %b %d %H:%M:%S ")
#  warn = warnings.formatwarning(message, category, filename, linenumber)
#  warn = string.split(warn, os.linesep)[0]
#  f.write(string.join([timestamp, warn, os.linesep]))
#warnings.showwarning = showwarning
#warn("import complete", DebugWarning)

from quixote.directory import Directory
# when launching from within an ipython shell __file__ is relative
# instead of absolute
class CompClustUI(Directory):
  _q_exports = ['', 'image', 'compclust', 'compclust_css']
  
###############
# "Pages" to return

