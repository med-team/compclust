import inspect
import os
import unittest

from compClust.mlx import datasets
import compClust.gui.DataManager as DataManager
from compClust.gui.PlotCache import PlotCache
from compClust.gui.WebPlot import WebPlot

class testPlotCache(unittest.TestCase):
  def testPlotCacheSimple(self):
    plotCache = PlotCache()
    plot = WebPlot(datasets.Dataset([[1,2,3]]*3))
    plot.pca_eigenvector()
    
    plotid = plotCache.append(plot)
    self.failUnless(plotCache.ctime(plotid) == plotCache.atime(plotid))
    self.failUnless(plotCache[plotid] == plot)
    # now that we've accessed the plot the access time should be later than
    # the ctime
    atime = plotCache.atime(plotid)
    self.failUnless(plotCache.ctime(plotid) < atime)
    # did the .items call update atime?
    plotCache.items()
    self.failUnless(plotCache.atime(plotid) > atime)

def suite(**kw):
 suite = unittest.makeSuite(testPlotCache,'test')
 return suite

if __name__ == "__main__":
  unittest.main(defaultTest="suite")

