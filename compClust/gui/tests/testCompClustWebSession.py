import inspect
import os
from StringIO import StringIO
import types
import unittest

from compClust.gui.CompClustWebSession import config_parser

class testCompClustWebSession(unittest.TestCase):
  def setUp(self):
    datadir = os.path.split(inspect.getsourcefile(testCompClustWebSession))[0]
    self.datadir = os.path.realpath(datadir)
    self.chodir = os.path.join(self.datadir, "../Examples/ChoCellCycling")
    
  def testConfig(self):
    """Test configuration loader
    """

    configfile = StringIO("""
[Cho]
path=%s
module=LoadExample
function=LoadChoSource
""" % (os.path.realpath(os.path.join(self.datadir, "../../util"))))
    
    load_functions = config_parser(configfile)
    self.failUnless(len(load_functions) == 1)
    self.failUnless(type(load_functions[0]) == types.FunctionType)
    
    cho_source = load_functions[0]()
    # just make sure we can access the datasource
    self.failUnless(cho_source.dataset.getNumRows()>0)
    
    configfile = StringIO("""
[Cho]
path=%s
module=LoadExample
function=LoadChoSource
arg=/home/diane/file
""" % (os.path.realpath(os.path.join(self.datadir, "../../util"))))
    
    load_functions = config_parser(configfile)
    self.failUnless(len(load_functions) == 1)
    self.failUnless(type(load_functions[0]) == types.FunctionType)
    
def suite(*args, **kwargs):
  return unittest.makeSuite(testCompClustWebSession)

if __name__ == "__main__":
  unittest.main(defaultTest="suite")
