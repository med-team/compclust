import inspect
import os
import unittest

from compClust.mlx import datasets
from compClust.mlx import labelings
import compClust.gui.DataManager as DataManager
from compClust.gui.DataSource import DataSource

class testDataSource(unittest.TestCase):
  def setUp(self):
    self.datadir = os.path.split(inspect.getsourcefile(testDataSource))[0]
    self.chodir = os.path.join(self.datadir, "../Examples/ChoCellCycling")

    self.cache_dir = "/tmp/testcache"
    self.shelf_name = "/tmp/testshelf"
    self.shelf_id_name = "/tmp/testshelfid"
    self.labelings = [
      ('cho', os.path.join(self.chodir,'ChoClassification.rlab'), True, None),
      ('em', os.path.join(self.chodir, 'EM.rlab'), True, None),
      ('common', os.path.join(self.chodir, 'CommonNames.rlab'), True, None),
      ('orfs', os.path.join(self.chodir, 'ORFs.rlab'), True, "http://somesite.com/orf=%s"),
      ('times', os.path.join(self.chodir, 'times.clab'), False, None)]

    self.ds = DataManager.DataSource()
    self.ds.source = os.path.join(self.chodir, 'ChoCycling.dat')

  def tearDown(self):
    if os.path.isfile(self.shelf_name):
      os.remove(self.shelf_name)
    if os.path.isfile(self.shelf_id_name):
      os.remove(self.shelf_id_name)
    
  def attachChoLabels(self, ds):
    """Attach labelings to cho dataset"""
    for name, path, isrow, url in self.labelings:
      ds.add_labeling(name, path, isrow, url)
    
  def testLabelingBeforeAccess(self):
    """Attach labelings before dataset is initialized
    """
    for name, path, isrow, url in self.labelings:
      self.ds.add_labeling(name, path, isrow, url)
    self.failUnless(len(self.ds.dataset.getLabelings()) == len(self.labelings))

  def testLabelingAfterAccess(self):
    """Attach labelings after dataset is initialized
    """
    self.failUnless(len(self.ds.dataset.getLabelings()) == 0)
    for name, path, isrow, url in self.labelings:
      self.ds.add_labeling(name, path, isrow, url)
    self.failUnless(len(self.ds.dataset.getLabelings()) == len(self.labelings))

  def testTrajectorySummary(self):
    """Test to see if we can create a trajectory summary
    """
    self.failUnlessRaises(ValueError, self.ds.cluster_trajectories, None)
                          
    self.attachChoLabels(self.ds)
    cho = self.ds.get_labeling_by_name('cho')
    iplot = self.ds.cluster_trajectories(cho)

  def testConfusionMatrixSummary(self):
    self.attachChoLabels(self.ds)
    cho = self.ds.get_labeling_by_name('cho')
    em = self.ds.get_labeling_by_name('em')
    iplot = self.ds.confusion_matrix_summary(cho, em)

  def testRocPlot(self):
    self.attachChoLabels(self.ds)
    em = self.ds.get_labeling_by_name('em')
    iplot = self.ds.roc_plot('1', em)

  def testPcaProjection(self):
    self.attachChoLabels(self.ds)
    iplot = self.ds.pca_projection()

  def testPcaProjection(self):
    self.attachChoLabels(self.ds)
    iplot = self.ds.pca_eigenvector()
    
  def testMergeDatasetLabelings(self):
    """When creating a DataSource from a dataset, we might want to include labels already attached
    """
    cho_dataset = datasets.Dataset(self.ds.source)
    for label_name, label_path, isRow, isAnnotation in self.labelings:
      l = labelings.Labeling(cho_dataset, label_name)
      if isRow:
        l.labelRows(label_path)
      else:
        l.labelCols(label_path)
    cho_source = DataSource(cho_dataset)
    self.failUnless(len(cho_dataset.getLabelings()) == len(cho_source.get_labeling_names()))
    for label_name in [ l.getName() for l in cho_dataset.getLabelings() ]:
      self.failUnless(label_name in cho_source.get_labeling_names())

  def testNameCollision(self):
    """using add_labeling should collide with labeling.Labeling labels"""
    data = datasets.Dataset([[1,1,1],[2,2,2],[3,3,3]])
    source = DataSource(data)
    row_labeling = labelings.Labeling(data, 'row_id')
    self.failUnlessRaises(ValueError, source.add_labeling, 'row_id', [1,2,3], True, True)
    

def suite(**kw):
 suite = unittest.makeSuite(testDataSource,'test')
 return suite

if __name__ == "__main__":
  unittest.main(defaultTest="suite")

