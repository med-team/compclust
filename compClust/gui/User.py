class UserLogin:
  """This class is intended to provide a login 
  """
  def __init__(self):
    self.users = {'diane': 'password'}

  def login(self, username, password):
    if not self.users.has_key(username):
      return False
    elif self.users[username] == password:
      return True
    else:
      return False
    

#######
class UserUI(UserLogin):
  _q_export = ['action', 'logout']
  def __init__(self):
    UserLogin.__init__(self)

  def _q_index(self, req):
    session = get_session()
    if session is None or session.user is None:
      context = datamanagers[self.user].create_context()
      return get_template(req, 'login', context)
    else:
      return datamanagaers.setdefault(self.user, DatamanagerUI())

  def action(self, req):
    session = get_session()
    user = req.get_form_var('user', None)
    password = req.get_form_var('password', None)
    if self.login(user, password):
      session.user = user
      return datamanagaers.setdefault(self.user, DatamanagerUI())
    else:
      context = datamanagers[self.user].create_context()
      context.addGlobal('badlogin', True)
      return get_getmplate(req, 'login', context)
      

  def logout(self, req):
    session = get_session()
    get_session_manager().expire_session(req)
    if session.user:
      return "<html>user %s logged out</html>" % (session.user)
    else:
      return "<html>logged out</html>"
    
