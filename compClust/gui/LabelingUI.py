from quixote import get_field, get_path, get_session, get_request, redirect
from quixote.errors import QueryError, TraversalError
from quixote.directory import Directory
from compClust.gui.TemplateUtils import get_template
from compClust.gui.LabelingSource import LabelingSource


class LabelingSourceUI(Directory):
  _q_exports = ['', 'edit']
  def __init__(self, datamanagerui, datasource, labelingsource):
    self.datamanagerui = datamanagerui
    self.datasource = datasource
    self.labelingsource = self.datasource.get_labelingsource_by_id(labelingsource)
    if self.labelingsource is None:
      raise TraversalError("couldn't find labeling source %s" % (labelingsource))


  def edit(self):
    name = get_field('name', None)
    source = get_field('source', None)
    isrow = get_field('isrow', None)
    url = get_field('url', None)
    description = get_field('description', None)
    
    # convert isrow radio button to proper type
    if isrow is not None:
      if isrow=="row":
        isrow = True
      elif isrow=="column":
        isrow = False
      else:
        raise QueryError("Invalid value for isrow")

    # update any values the user passed in
    if name is not None:
      self.labelingsource.name = name
    if source is not None:
      self.labelingsource.source = source
    if isrow is not None:
      self.labelingsource.isrow = isrow
    if url is not None:
      self.labelingsource.lookup_url = url
    if description is not None:
      self.labelingsource.description = description

    return self._q_index()

  def _q_index(self):
    context = self.datamanagerui.create_context(self.datasource)
  
    context.addGlobal('sourcetype', 'text')
    if self.labelingsource.isannotation:
      context.addGlobal('editlabeling_title', 'Edit Annotation')
    else:
      context.addGlobal('editlabeling_title', 'Edit Clustering')
  
    # generate error if someone's giving us a bogus id
    if self.labelingsource is not None:
      context.addGlobal('sourcetype', 'text')
      context.addGlobal('labelingsource', self.labelingsource)
      context.addGlobal('editlabeling', True)
      if self.labelingsource.isrow:
        context.addGlobal('isrow', 'checked')
      else:
        context.addGlobal('iscolumn', 'checked')
    else:
      # new upload form
      context.addGlobal('sourcetype', 'file')

    return get_template('editlabelingsource', context)

class LabelingManagerUI(Directory):
  """Class to help with lookup up and editing labeling sources
  """
  _q_exports = ['', 'add_annotation', 'add_clustering' ]
  def __init__(self, datamanagerui, datasource, baseurl):
    self.datamanagerui = datamanagerui
    self.datasource = datasource
    self.baseurl = baseurl
    
  def _q_lookup(self, name):
    """If it's not something we know about perhaps its a labelname
    """
    # FIXME: perhaps we should actually have a name in the url?
    # FIXME: doing it this way means that labelnames cant be the same
    # FIXME: as plot names
    return LabelingSourceUI(self.datamanagerui, self.datasource, name)

  def add_annotation(self):
    """Create a new labeling and mark it as an annotation (and thus
    prevent it from showing up in the clustering filter pages)
    """
    return self.add(isannotation=True)
    
  def add_clustering(self):
    """Create a new labeling and mark it as a clustering and thus
    showing up in the clustering drop downs  but not the annotation drop
    downs
    """
    return self.add(isannotation=False)
    
  def add(self, isannotation):
    """Attach new labeling source
    """
    name = get_field('name', None)
    source = get_field('source', None)
    isrow = get_field('isrow', None)
    url = get_field('url', None)
    description = get_field('description', None)
    
    # convert isrow radio button to proper type
    if isrow is not None:
      if isrow=="row":
        isrow = True
      elif isrow=="column":
        isrow = False
      else:
        raise QueryError("Invalid value for isrow")

    if name is not None and source is not None and isrow is not None:
      # FIXME: session issue
      #source = get_session().datamanager.save_temp_file(source, self.datasource)
      
      labelingsource = self.datasource.add_labeling(name, source.fp, isrow, isannotation, url, description)
      return redirect(self.baseurl )
    else:
      context = self.datamanagerui.create_context(self.datasource)
      if isannotation:
        context.addGlobal('editlabeling_title', 'Add Annotation')
      else:
        context.addGlobal('editlabeling_title', 'Add Clustering')
      context.addGlobal('editlabeling', False)
      context.addGlobal('sourcetype', 'file')
      return get_template('editlabelingsource', context)

  
