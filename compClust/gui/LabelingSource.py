class LabelingSource(object):
  """Manage a labeling source and its meta info
  """
  def __init__(self, name=None, source=None, isrow=True, isannotation=None, url=None, description=None):
    self.name = name
    self.source = source
    self.isrow = isrow
    self.isannotation = isannotation
    self.lookup_url = url
    self.description = description

  def __get_id(self):
    return self.name
  id = property(__get_id, doc="return unique id for object")
  
