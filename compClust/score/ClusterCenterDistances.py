########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

"""
This module evaluates the similarity of two labelings by how close the means of the partionings are.
"""
import numpy

from compClust.util.DistanceMetrics import NormalizedEuclideanDistance
from compClust.mlx.models import constructMixtureOfDiagonalGaussiansFromLabeling
from compClust.mlx.datasets import Dataset

def clusterMeansDistance(dataset, labeling1, labeling2):

  """
  clusterMeansDistance (dataset, labeling1, labeling2)

  This calculates the below function expressed in tex.

  \( cmd(M_{\alpha }M_{\beta })=\frac{1}{\frac{e^{\sum _{i}^{k_{\alpha }}\frac{(\bar{x}_{i}-\bar{y}_{\pi _{i}})^{2}}{\sigma ^{2}}}+e^{\sum _{i}^{k_{\beta }}\frac{(\bar{y}_{i}-\bar{x}_{\pi _{i}})^{2}}{\sigma ^{2}}}}{2}} \)

  For a nice viewable formated document see clusterMeanDistances.pdf

  """
  
  covariance = numpy.cov(dataset.getData(), rowvar=0)
  X = constructMixtureOfDiagonalGaussiansFromLabeling(dataset, labeling1).means
  Y = constructMixtureOfDiagonalGaussiansFromLabeling(dataset, labeling2).means


  return(( numpy.mean(map(lambda x: numpy.e**(-1*(min(NormalizedEuclideanDistance(x, Y, covariance))**2)), X), axis=0) +
           numpy.mean(map(lambda y: numpy.e**(-1*(min(NormalizedEuclideanDistance(y, X, covariance))**2)), Y), axis=0) ) /2 )

  
  
  
  
