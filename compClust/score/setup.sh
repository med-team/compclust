#!/bin/sh

################################################################################
#   setup.sh
################################################################################
#
#   This file contains the machine-specific information that is necessary for
#   using these shared Python codes.  You should set each environment 
#   variable, etc. as appropriate for your machine.  Individual users may 
#   copy and source a customized version of this file if user-specific 
#   variables are desired.
#
#   * * * IMPORTANT NOTICE * * *
#
#   If you are setting up these codes for the first time on your machine,
#   you may need to change any software which refers explicitly to this file,
#   so that it knows how to find this file.
#
################################################################################

################################################################################
# The following steps set environment variables which describe the directories 
# where everything is located.
################################################################################

SCORE_CODE_HOME=/proj/cluster_gazing2/src/score/confusionMatrix/:/proj/cluster_gazing2/src/score/

#
# Add Python code packages to the user's PYTHONPATH
#

if [ ${PYTHONPATH} ]; then
  PYTHONPATH="${PYTHONPATH}:${SCORE_CODE_HOME}/"; 
else
  PYTHONPATH="$SCORE_CODE_HOME/";
fi

export PYTHONPATH

