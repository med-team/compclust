########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
#
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
#
# The Original Source Code is "compClust", released 2003 September 03.
#
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

import os
import unittest
from compClust.config import config, Config

class testConfig(unittest.TestCase):
  def test_local(self):
    """How well do we find things in the source directory
    """
    self.failUnless(os.path.isdir(config.cho_data_dir))
    # Currently we're going through a reorganization processs with locating where 
    # the compclust tk docs are going
    self.failIf(os.path.isdir(config.compclusttk_tutorial_dir))
    self.failUnless(os.path.isdir(config.template_dir))
    
    self.failUnless(os.path.isfile(config.diagem_command), config.diagem_command)
    self.failUnless(os.path.isfile(config.kmeans_command), config.kmeans_command)
       
  def test_remote(self):
    """How well do we find things in elsewhere
    """
    root_dir = os.path.realpath('/')
    remote_config = Config(base_dir=os.path.join(root_dir,"hi"))
    self.failUnless(remote_config.cho_data_dir==\
		    os.path.join(root_dir,"hi","examples","ChoCellCycling"))
    self.failUnless(remote_config.compclusttk_tutorial_dir==\
		    os.path.join(root_dir,"hi","docs"))
    self.failUnless(remote_config.template_dir==\
		    os.path.join(root_dir,"hi","templates"))
    
    self.failUnless(remote_config.diagem_command==\
		    os.path.join(root_dir,"hi","bin","diagem"))
    self.failUnless(remote_config.kmeans_command==\
		    os.path.join(root_dir,"hi","bin","kmeans"))
    

def suite(**kw):
  return unittest.makeSuite(testConfig,'test')

if __name__ == "__main__":
  unittest.main(defaultTest="suite")
