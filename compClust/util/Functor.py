########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#        Author: Diane Trout
# Last Modified: $Date: 2004/01/15 08:59:54 $
#

class Functor:
  """The functor class allows permanently setting a parameter
  for when the function is called later.
  """
  def __init__(self, function, *args, **kargs):
    assert callable(function), "function should be a callable obj"
    self._function = function
    self._args = args
    self._kargs = kargs
    
  def __call__(self, *args, **kargs):
    """call function"""
    _args = list(self._args)
    _args.extend(args)
    _kargs = self._kargs.copy()
    _kargs.update(kargs)
    return apply(self._function,_args,_kargs)      

