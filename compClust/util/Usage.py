########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#        Author: Benjamin J. Bornstein
# Last Modified: 09-Apr-2001, 4:15p
#

"""
Helper functions to extract doc strings from modules at run-time.
"""

import re
import sys
import string
import inspect

def showUsage(object, stream=sys.stdout, exit=0):

  """
  showUsage(object, stream=sys.stdout, exit=0)

  prints out the first non-empty line of the object's doc
  string which should contain minimal usage information.
  """

  try:
    docString = inspect.getdoc(object)
    docString = string.split(string.strip(docString), '\n')
    print docString[0]
    
  except:
    print "no usage help found"

  if (exit):
    sys.exit(exit)
  
def showHelp (object, stream=sys.stdout, exit=0):

  """
  showHelp(object, stream=sys.stdout, exit=0)

  prints out the entire doc string of the object specified
  which should contain all the relevent information a user might need.
 
  """
  
  try:
    docString = inspect.getdoc(object)
    docString = string.split(string.strip(docString), '\n')
    print string.join(docString, '\n')
    
  except:
    print "no help found"

  if (exit):
    sys.exit(exit)









