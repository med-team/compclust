########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Diane Trout

import cPickle

def pickleObject(obj, filename, binary = 1):
  #print 'pickling [' + str(obj) + '] to file ' + filename
  handle = open(filename, 'w')
  cPickle.dump(obj, handle, binary)
  handle.close()

def unpickleObject(filename):
  print 'loading: ' + filename
  try:
    handle = open(filename, 'r')
    ds = cPickle.load(handle)
    handle.close()
  except:
    print 'could not load: ' + filename
    return 
  return ds
