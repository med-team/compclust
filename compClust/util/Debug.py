########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#        Author: Diane Trout
# Last Modified: $Date: 2003/09/04 01:51:33 $
#

"""
Useful debugging utilities.

Most of there functions delve into the innars of the python interpreter and
deserve to be written once and only once.
"""

import sys
import types
from compClust.util import proc

def getGlobalRefcounts():
  """getGlobalRefcountss()
  
  Search for all extant objects and return their refcounts.
  """
  objects = getExtantObjects()
  # build tuples of (refcount, object_ref)
  pairs = map (lambda x: (sys.getrefcount(x),x), objects)
  # sort by refcount
  pairs.sort()
  pairs.reverse()
  return pairs

def getExtantObjects():
  """getExtantObjects()
  
  Search for all extant objects and return a dictionary of references
  to them. 
  """
  objects = []
  sys.modules
  # collect all classes
  for m in sys.modules.values():
    for sym in dir(m):
      o = getattr (m, sym)
      if type(o) is types.ClassType:
        objects.append(o)
  return objects
  
def getDataSize():
  """int = getDataSize()
  
  Return the current number of kB allocated for this process.
  """
  return proc.getProcessStatus()['VmData']
