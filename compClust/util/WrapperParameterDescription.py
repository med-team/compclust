########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

class WrapperParameterDescription:

  def __init__(self, value=None, dataType=None, description=None, required=False, choices=None, min=None, max=None):
    """
    Used to define parameter values and include information about that parameter
    significant to programs like CompClustTk.py.

    value = parameter value
    dataType = types.SomeDataType (used by compClust GUI to construct parameter dialog boxes)
    description = Help or description of parameter
    required = True or False
    choices = List of possible values if dataType == types.ListType
    min = Min num of Chars for string, Min number for floats and ints
    max = Max num of Chars for string, Max number for floats and ints
    
    """
    self.value = value
    self.dataType = dataType
    self.description = description
    self.required = required
    self.choices = choices
    self.min = min
    self.max = max
