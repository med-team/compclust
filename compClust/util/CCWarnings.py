########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Diane Trout
# 

"""CompClust specific warnings

By default it hides debug warnings, you'll need to either do a resetwarning or 
execute filtewarnings("always", category=DebugWarning).

For more inforamtion about warnings see section 3.20 of the python docus

http://docs.python.org/lib/module-warnings.html
"""
import warnings

from warnings import warn, filterwarnings, resetwarnings

class DebugWarning(Warning):
  pass
  
#  
filterwarnings("ignore", category=DebugWarning)

