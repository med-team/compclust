########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

import types
import imp

def loadModule(module_path, moduleName):
  """Given a module name, load the appropriate module
  """ 	                                                
  if type(moduleName) == types.ModuleType:
    return moduleName
  elif type(moduleName) == types.StringType or type(moduleName) == types.UnicodeType: 
    file, filename, modType = imp.find_module(moduleName, module_path) 
    return imp.load_module(moduleName, file, filename, modType)
  
