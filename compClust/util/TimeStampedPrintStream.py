########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#        Author: Benjamin J. Bornstein
# Last Modified: 09-Apr-2001, 12:45p
#


import os
import sys

from time import localtime
from time import strftime
from time import time


class TimeStampedPrintStream:

  def __init__(self, format="%Y-%b-%d %H:%M: ", stream=sys.stdout):
    """TimeStampedPrintStream(format="%Y-%b-%d %H:%m: ", stream=sys.stdout)

    Creates a new TimeStampedPrintStream based on the given stream and
    time format string.  If not specified, stream defaults to
    sys.stdout and format defaults to "%Y-%b-%d %H:%m: ", which (in a
    slightly more readable format) is "yyyy-MMM-dd HH:mm: ".  The
    following directives can be embedded in the format string (based
    on ANSI C strftime):

    Directive    Meaning
    ---------    -------------------------------------------------------------
    %a           Locale's abbreviated weekday name.
    %A           Locale's full weekday name.
    %b           Locale's abbreviated month name.
    %B           Locale's full month name.
    %c           Locale's appropriate date and time representation.
    %d           Day of the month as a decimal number [01,31].
    %H           Hour (24-hour clock) as a decimal number [00,23].
    %I           Hour (12-hour clock) as a decimal number [01,12].
    %j           Day of the year as a decimal number [001,366].
    %m           Month as a decimal number [01,12].
    %M           Minute as a decimal number [00,59].
    %p           Locale's equivalent of either AM or PM.
    %S           Second as a decimal number [00,61].
    %U           Week number of the year (Sunday as the first day of the week)
                 as a decimal number [00,53]. All days in a new year preceding
                 the first Sunday are considered to be in week 0.
    %w           Weekday as a decimal number [0(Sunday),6].
    %W           Week number of the year (Monday as the first day of the week)
                 as a decimal number [00,53]. All days in a new year preceding
                 the first Sunday are considered to be in week 0.
    %x           Locale's appropriate date representation.
    %X           Locale's appropriate time representation.
    %y           Year without century as a decimal number [00,99].
    %Y           Year with century as a decimal number.
    %Z           Time zone name (or by no characters if no time zone exists).
    %%           A literal "%" character."""


    self.format = format
    self.stream = stream

  # FIXME: The following two functions are used to override the
  # FIXME: default behavior for pickling and copying of objects
  # FIXME: these empty definitions exist so we can pickle classes using
  # FIXME: TimeStampledPrintStream, since python won't let us
  # FIXME: pickle an open stream.
  def __setstate__(self, dict):
    pass


  def __getstate__(self):
    return []


  def write(self, s):
    self.stamp = strftime( self.format, localtime( time() ) )
    self.stream.write(self.stamp + s)




  def writeln(self, s):
    self.write(s + "\n")
