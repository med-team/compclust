#!/usr/bin/env python
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################
#
#       Authors: Lucas Scharenbroich
#                Christopher Hart
#                Diane Trout
#                Brandon King
# Last Modified: Mar 21 15:45:29 PST 2007
#
"""
This module was created because the implementation of standard
deviation changed from MLab.std to numpy.std. the std function
in this module is ment to implement the equivalent of MLab.std
using numpy. Here's why:

Using the data below to create a 10x5 array...

a = numpy.array([[ 0.71550518,  0.78410744,  0.93019689,  0.07352647,  0.19873096],
       [ 0.96310059,  0.58471053,  0.10085294,  0.4245936 ,  0.07288226],
       [ 0.92771516,  0.3998865 ,  0.47516746,  0.34525502,  0.70183348],
       [ 0.44345649,  0.74943013,  0.63582228,  0.67736132,  0.36241799],
       [ 0.32446836,  0.60361731,  0.25549986,  0.30648148,  0.89071801],
       [ 0.93291504,  0.92822327,  0.83330382,  0.13373875,  0.21464378],
       [ 0.99453546,  0.24230076,  0.38735515,  0.44227894,  0.04584125],
       [ 0.20619319,  0.40641068,  0.84691841,  0.42658256,  0.28968515],
       [ 0.27939693,  0.54292146,  0.48758192,  0.101297  ,  0.59024379],
       [ 0.95146076,  0.65739587,  0.42550023,  0.43582772,  0.60039243]])

When you calculate standard deviation using excel's STDEV and
STDEVP for the columns, you get the following results. The
difference between the two has to do with dividing by N vs
N-1. (See this page for more information:
http://en.wikipedia.org/wiki/Standard_deviation#Estimating_population_standard_deviation_from_sample_standard_deviation)

Excel STDEV (N-1):
0.324262408	0.204180065	0.270548017	0.188468096	0.28461152

Excel STDEVP (N):
0.30762233	0.193702217	0.256664385	0.178796534	0.270006196

When using numpy which uses the divide by N method, we get
the following:

In [29]: numpy.std(a, axis=0)
Out[29]: array([ 0.30762233,  0.19370222,  0.25666439,  0.17879653,  0.2700062 ])

When using MLab (Numeric), you can see it's using the did N-1 method.

In [31]: MLab.std(a)
Out[31]: array([ 0.32426241,  0.20418006,  0.27054802,  0.1884681 ,  0.28461152])

The std function in this module should match the output of MLab.std.
We want this because in the conversion of CompClust from Numeric to
Numpy, we don't want a change in behavior to facilitate the conversion
process. Later if it's decided that we should change to the divide by
N method, then this function will no longer be needed as numpy.std
implements that method.

According to Ken, using divide by N-1 is fine for clustering data.

The reason numpy.std uses divide by N is here:
http://projects.scipy.org/scipy/numpy/ticket/461
"""

import numpy

def std(a):
  """
  Equivelent to MLab.std (divide by N-1)
  
  See module level documentation for more information.
  
  Test using data from module level doc string:
  In [29]: numpy.allclose(Std.std(a),MLab.std(a))
  Out[29]: True
  """
  return numpy.sqrt(numpy.mean((a-numpy.mean(a, axis=0))**2, axis=0)*(float(a.shape[0])/(a.shape[0]-1)))
