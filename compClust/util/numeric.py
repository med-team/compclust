"""
Import a numeric module

Python is going through a shift in which numeric library to use
so try to import the newer numpy, and if that fails fall back to 
the older Numeric.

We could try for numarray, but numpy is based on Numeric which is what 
we originally used, and so hopefully is more compatibile.
"""
raise RuntimeError("deprecating this, numpy is so much better")
try:
  import numpy 
except ImportError:
  # try to make Numeric look like numpy
  import Numeric as numpy
  import MA as ma
  numpy.ma = ma
  numpy.amin = min
  numpy.amax= max

  # drag along functions that we used from MLab
  import MLab
  import operator
  def all(a):
    """require that all elements be true"""
    reduce(operator.__and__, a)
  numpy.all = all
  def any(a):
    """require that any elements be true"""
    reduce(operator.__or__, a)
  numpy.any = any
  numpy.cov = MLab.cov
  numpy.diag = MLab.diag
  numpy.eye = MLab.eye
  numpy.mean = MLab.mean
  numpy.median = MLab.median
  numpy.rand = MLab.rand
  numpy.std = MLab.std
  numpy.trapz = MLab.trapz

  # how about RandomArray next
  import RandomArray
  numpy.random = RandomArray

  # and of course our friend LinearAlgebra
  import LinearAlgebra
  numpy.linalg = LinearAlgebra
  numpy.linalg.svd = MLab.svd
