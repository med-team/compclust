#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

import unittest
import os

from compClust.util.WrapperBuilder import WrapperBuilder
from compClust.util.WrapperPlugins import WrapperPlugins

from compClust.util.LoadExample import LoadCho

class testWrapperBuilder(unittest.TestCase):
  def setUp(self):
    self.wrappers = WrapperPlugins()
    self.dataset = LoadCho()

  def tearDown(self):
    pass
  
  def testSimpleAlgorithm(self):
    wb = WrapperBuilder(self.dataset)
    self.failUnless(len(wb) == 0)

    w = self.wrappers['DiagEM']
    wb.append(w)
    self.failUnless(len(wb) == 1)
    self.failUnless(len(wb[-1].parameters) > 0)

    wb.run()

    labeling = wb.getLabeling()
    self.failUnless(labeling is not None)

  def testOneLevelComposedAlgorithm(self):
    wb = WrapperBuilder(self.dataset)
    wb.append(self.wrappers['MultiRun'])
    wb[-1].parameters.parameter_name = 'k'
    wb[-1].parameters.parameter_values = [5,10,15]

    self.failIf(wb.isFinalized())

    wb.append(self.wrappers['DiagEM'])
    wb[-1].parameters.init_means = 'random_means'
    wb[-1].parameters.distance_metric = 'correlation'
    self.failUnless(wb.isFinalized())
    wb.run()

    labeling = wb.getLabeling()
    self.failUnless(labeling is not None)

  def testMultiLevelComposedAlgorithm(self):
    wb = WrapperBuilder(self.dataset)
    wb.append(self.wrappers['MultiRun'])
    wb[-1].parameters.parameter_name = 'k'
    wb[-1].parameters.parameter_values = [5,10,15]

    self.failIf(wb.isFinalized())

    wb.append(self.wrappers['MultiRun'])
    wb[-1].parameters.parameter_name = 'seed'
    wb[-1].parameters.parameter_values = [3213, 21578, 23]

    self.failIf(wb.isFinalized())

    wb.append(self.wrappers['DiagEM'])
    self.failUnless(wb.isFinalized())
    wb.run()
    
    labeling = wb.getLabeling()
    self.failUnless(labeling is not None)
    
  def testSyntheticComposedAlgorithm(self):
    from compClust.util.SyntheticData import HierarchicalSyntheticData
    synth = HierarchicalSyntheticData([2,2], 2, 750, variance=1, variance_ratio=.1)
    dataset = synth.datasets[-1]
    # there's only one labeling attached to the dataset when it's first genereated
    ground_truth = dataset.getLabelings()[0]
    
    wb = WrapperBuilder(dataset)
    wb.append(self.wrappers['MultiRun'])
    wb[-1].parameters.parameter_name = 'k'
    wb[-1].parameters.parameter_values = range(2,6)

    wb.append(self.wrappers['MultiRun'])
    wb[-1].parameters.parameter_name = 'seed'
    wb[-1].parameters.parameter_values = [3213, 21578, 23, 251]

    wb.append(self.wrappers['DiagEM'])
    wb.run()
    
    clustering_labeling = wb.getLabeling()
    
    #import Tkinter
    #import compClust.iplot.IPlotTk as IPlot
    #IPlot.ConfusionMatrixSummary(dataset, ground_truth, clustering_labeling)
    #pcaplot=IPlot.PCAPlot(dataset)
    #pcaplot.ProjectionPlot()
    #Tkinter.mainloop()
    
def suite(**kw):
  return unittest.makeSuite(testWrapperBuilder)
    
if __name__ == "__main__":
  unittest.main(defaultTest="suite")
