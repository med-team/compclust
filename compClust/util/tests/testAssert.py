#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

import unittest
import os

from compClust.util import Assert
import compClust.util

class testAssert(unittest.TestCase):
  def setUp(self):
    self.original_dir    = os.getcwd()
    os.chdir(compClust.util.__path__[0])

  def tearDown(self):
    os.chdir(self.original_dir)
    
  def test_fs_objects_have_permissions_parameter(self):
    """Verify the parameter testing of fs_objects_have_permissions."""
    # make sure that we don't process an invalid parameter type
    try:
      Assert.fs_objects_have_permissions("/tmp", 0)
    except AttributeError, error:
      self.fail("should accept a the string for /tmp")



  def test_fs_objects_have_permissions_relative(self):
    """Verify the behavior of fs_objects_have_permissions in relative mode."""
    import tempfile
    from stat import S_IRUSR, S_IRGRP, S_IROTH
    
    doesnt_exist = [ 'test/foobarbaz.tzt' ]
    
    # verify that the file doesn't exist
    self.failIf(Assert.fs_objects_have_permissions(doesnt_exist, os.F_OK))
    
    # try making an unreadable tempfile
    unreadable_stream = tempfile.NamedTemporaryFile()
    os.chmod(unreadable_stream.name, 0)
    unreadable = [unreadable_stream.name]
    # verify that the file exists
    self.failUnless(Assert.fs_objects_have_permissions(unreadable, os.F_OK))
    # verify that we cant read them
    self.failIf(Assert.fs_objects_have_permissions(unreadable, os.F_OK|os.R_OK))
    unreadable_stream.close()
    
    # make some readonly tempfiles
    readonly_streams = []
    read_only = []   
    for i in range(3):
      new_tempfile = tempfile.NamedTemporaryFile()
      os.chmod(new_tempfile.name, S_IRUSR|S_IRGRP|S_IROTH)
      readonly_streams.append(new_tempfile)
      read_only.append(new_tempfile.name)
      
    # make sure we can read the files
    self.failUnless(Assert.fs_objects_have_permissions(read_only, os.F_OK|os.R_OK))
    # make sure we can't write to the files
    self.failIf(Assert.fs_objects_have_permissions(read_only, os.F_OK | os.W_OK))
    for s in readonly_streams:
      s.close()

    readwrite_stream = tempfile.NamedTemporaryFile()
    read_write = [ readwrite_stream.name ]
    # make sure we can read the files
    self.failUnless(Assert.fs_objects_have_permissions(read_write,os.F_OK|os.R_OK))
    # make sure we can write to the files
    self.failUnless(Assert.fs_objects_have_permissions(read_write,os.F_OK|os.W_OK))
    readwrite_stream.close()
    
  def test_fs_objects_have_permissions_absolute(self):
    """Verify the behavior of fs_objects_have_permissions in absolute mode."""

    # FIXME: need to test fs_objects_have_permissions in
    # FIXME: absolute mode
    pass
  
def suite(**kw):
  suite = unittest.TestSuite()
  suite.addTest(testAssert("test_fs_objects_have_permissions_parameter"))
  suite.addTest(testAssert("test_fs_objects_have_permissions_relative"))
  suite.addTest(testAssert("test_fs_objects_have_permissions_absolute"))
  return suite

if __name__ == "__main__":
  unittest.main(defaultTest="suite")
