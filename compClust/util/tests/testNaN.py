#!/usr/bin/env python2.2
########################################
# The contents of this file are subject to the MLX PUBLIC LICENSE version
# 1.0 (the "License"); you may not use this file except in
# compliance with the License.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
# the License for the specific language governing rights and limitations
# under the License.
# 
# The Original Source Code is "compClust", released 2003 September 03.
# 
# The Original Source Code was developed by the California Institute of
# Technology (Caltech).  Portions created by Caltech are Copyright (C)
# 2002-2003 California Institute of Technology. All Rights Reserved.
########################################

import unittest
import os

import compClust.util.NaN as NaN
import compClust.util

class testNaN(unittest.TestCase):
  def setUp(self):
    self.original_dir    = os.getcwd()
    os.chdir(compClust.util.__path__[0])

  def tearDown(self):
    os.chdir(self.original_dir)

  def checkNanFloat(self):
    self.failUnless(NaN.nanfloat("2.0") == float("2.0"))
    self.failUnless(NaN.nanfloat("nan") == NaN.nan)
    self.failUnless(NaN.nanfloat(" Nan  ") == NaN.nan)
    self.failUnlessRaises(ValueError, nanfloat, "bleem")

  def checkisNaN(self):
    if not NaN.isNaN(NaN.nan):
      self.fail("Couldn't successfully check for the presence of a NaN")
    if NaN.isNaN(1.23):
      self.fail("Couldn't successfully check for the absence of a NaN")

  def checkisNaNinSequence(self):
    a = [1,2,3,NaN.nan,5]
    if not NaN.isNaNinSequence(a):
      self.fail("Couldn't successfully check for the presence of a NaN")
    a = [ 1,2,3,4]
    if NaN.isNaNinSequence(a):
      self.fail("Couldn't successfully check for the absence of a NaN")

  def checkisnotNaNinSequence(self):
    a = [1,2,3,NaN.nan,5]
    if NaN.isnotNaNinSequence(a):
      self.fail("Couldn't successfully check for the presense of a NaN")
    a = [ 1,2,3,4]
    if not NaN.isnotNaNinSequence(a):
      self.fail("Couldn't successfully check for the absense of a NaN")

  def checkRemoveNaNsFromSequence(self):
    n = NaN.nan
    l = [1,2,3,n,4,5,6,7,n]
    truth = [1,2,3,4,5,6,7]
    filtered = NaN.removeNaNsFromSequence(l)
    if truth != filtered:
      self.fail("Couldn't remove NaNs from sequence")
    
  def checkRemoveSequencesWithNaN(self):
    n = NaN.nan
    l = [[1,2,3],
         [4,5,6],
         [7,8,n],
         [n,9,10],
         ]
    truth = [[1,2,3],
             [4,5,6],
             ]
    filtered = NaN.removeSequencesWithNaNs(l)
    if truth != filtered:
      self.fail("Couldn't remove sequences that contained NaNs correctly")
  
def suite(**kw):
  suite = unittest.TestSuite()
  suite.addTest(testNaN("checkisNaN"))
  suite.addTest(testNaN("checkisNaNinSequence"))
  suite.addTest(testNaN("checkisnotNaNinSequence"))
  suite.addTest(testNaN("checkRemoveNaNsFromSequence"))
  suite.addTest(testNaN("checkRemoveSequencesWithNaN"))

  return suite

if __name__ == "__main__":
  unittest.main(defaultTest="suite")
