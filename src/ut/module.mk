CURDIR = $(BASEDIR)/ut

SOURCES.c	= ut_args.c \
                  ut_error.c \
                  ut_file_io.c \
                  ut_math.c \
                  ut_memory.c \
                  ut_output.c \
                  ut_platform.c \
                  ut_string.c \
                  ut_time.c \
                  ut_types.c


UTSRC := $(addprefix $(CURDIR)/src/, $(SOURCES.c))

LIBSRC += $(UTSRC)
CFLAGS += -I$(CURDIR)/src/

UT := $(CURDIR)/libut$(LIBEXT)
LIBDIRS += -L$(dir $(UT))

TARGETLIBS += $(UT)

$(UT): $(UTSRC:.c=$(OBJEXT))
	$(AR) rv $@ $?
	$(call bless_library,$@)
