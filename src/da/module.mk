CURDIR := $(BASEDIR)/da/

                
SOURCES.c	= da_cluster.c \
                  da_geom.c \
                  da_io.c \
                  da_linalg.c \
                  da_memory.c \
                  da_nrhacks.c \
                  da_optim.c \
                  da_probstat.c \
                  da_random.c \
                  da_signal.c \
                  da_timeseg.c \
                  da_util.c 

# if MPI_H is available compile the MPI da modules
MPI_H := $(wildcard $(MPICH_INCLUDE)/mpi.h)

ifdef MPI_H
  SOURCES.c +=  da_cluster_pp.c\
                da_comm_pp.c \
                da_io_pp.c \
                da_signal_pp.c
endif
  
DASRC = $(addprefix $(CURDIR)src/, $(SOURCES.c))

SRC += $(DASRC)
CFLAGS += -I$(CURDIR)/src

TARGET = $(CURDIR)libda$(LIBEXT)
LIBDIRS += -L$(dir $(DA))

TARGETLIBS += $(TARGET)

$(TARGET): $(DASRC:.c=$(OBJEXT))
	$(AR) rv $@ $?
	$(call bless_library,$@)

