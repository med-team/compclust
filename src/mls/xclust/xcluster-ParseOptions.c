/**
 * @date   06-Apr-2000
 * @author Ben Bornstein
 *
 * The code below is from latest_cluster.c, lines 474--623.
 *
 * I've cleaned-up (re-formatted and omitted some non-essential error
 * output) the code for readability while trying to deciphering it.  The
 * pseudo-code below is the result.
 *
 * To better understand xcluster options, the following summarizes the
 * flow-control used when interactively prompting for options:
 *
 *   1. Log transform data (yes/no)?
 *
 *   2. Choose partitioning method (None, SOM, or k-means).
 *        a. If (SOM)            input x and y dimensions.
 *        b. If (k-means)        input number of clusters.
 *        c. If (SOM || k-means) randomize partitioning (yes/no)?
 *
 *   3. Choose distance metric (correlation or euclidean).
 *
 *   4. Cluster genes (yes/no)?
 *      a. If (correlation) choose centered or uncentered metric.
 *
 *   5. Cluster experiments (yes/no)?
 *      b. If (correlation) choose centered or uncentered metric.
 *
 *
 * Pseudo-code:
 *
 * -k n      k-means clusterings (where n > 0 is the number of clusters)
 *           if (-k n)
 *             gPartition = 2
 *             x          = n
 *
 * -l 0|1    Log transformation
 *           if (-l 1) getLogData = 1
 *
 * -u s      if (-u s)
 *            gPrefix = s
 *            gUID    = 1
 *
 * -f s      if (-f s)
 *            foundFileName = 1
 *            ifile         = s
 *
 * -g 0|1|2  if (-g 0) gShouldClusterGenes = 0
 *           if (-g 1) // default; do nothing
 *           if (-g 2) kgCentered = 1
 *
 * -e 0|1|2  if (-e 0) // default; do nothing
 *           if (-e 1) gShouldClusterExperiments = 1
 *           if (-e 2)
 *            gShouldClusterExperiments = 1
 *            keCentered = 1
 *
 * -p 0|1    whether to use pearson correlation (1), or Euclidean distance (0).
 *           1 is the default.
 *           if (-p 0) gUsePearson = 0
 *           if (-p 1) // default; do nothing
 *
 * -s 0|1    whether to make a SOM (1), or hierarchically cluster (0).
 *           0 is the default.
 *           if (-s 0) // default; do nothing
 *           if (-s 1) gPartition = 1
 *
 * -x n      specify x dimension of SOM
 *           if (-x n) x = n
 *
 * -y n      specify y dimension of SOM
 *           if (-y n) y = n
 *
 * -r 0|1    whether to seed the random number generator with the time when
 *           making a SOM.  1 is the default.
 *           if (-r 0) gRandomize = 0
 *           if (-r 1) // default; do nothing
 *
 * Option constraints:
 *
 * if ( !foundFileName ) ERROR
 * if ( (gPartition == 1) && (x < 0) || (y < 0) ) ERROR
 */
void ParseOptions(char *ifile, int argc, char** argv, int *x, int *y)
{  
  int i;
   int foundFileName = 0;
  int length;
  
  if ((argc - 1) % 2)
  {
    Error("Incorrect number of command line arguments");
  }

  
  for(i = 1; i < argc; i += 2)
  {
    /* k-means clustering */
    if ( !strcmp("-k", argv[i]) )
    {
      gPartition = 2;
      *x = (int) StringToReal( argv[i + 1] );
      
      if (*x <= 0)
      {
        Error("You must have more than zero clusters");
      }
    }

    /* log transformation */
    else if ( !strcmp("-l", argv[i]))
    {  
      if ( !strcmp("0", argv[i + 1]))
      { 
        /* default, no action needed */
      }
      else if ( strcmp("1", argv[i + 1]))
      {
        gLogData = 1;
      }
    }

    /* option -u */
    else if ( !strcmp("-u", argv[i]) )
    {
      length  = strlen( argv[i + 1] );
      gPrefix = malloc( (length + 1) * sizeof(char));
      
      strcpy(gPrefix, argv[i + 1]);
      
      gPrefix[length]='\0';
      
      gUID=1;
    }

    /* -f option */
    else if ( !strcmp("-f", argv[i]) )
    {
      strcpy(ifile, argv[i + 1]);
      foundFileName = 1;
      
    }

    /* genes parameter */
    else if ( !strcmp("-g", argv[i]) )
    { 
      /* they don't want to cluster genes */
      if ( !strcmp("0", argv[i + 1]) )
      {
        gShouldClusterGenes=0;
      }
      else if ( !strcmp("1", argv[i + 1]) )
      {
        /* default, no action needed */
      }
      else if ( !strcmp("2", argv[i + 1]))
      {
        kgCentered=1;
      }
    }
      
    /* experiments parameter */
    else if ( !strcmp("-e", argv[i]) )
    {
      if ( !strcmp("0", argv[i+1]) )
      {
        /* default, no action needed */
      }
      else if ( !strcmp("1", argv[i + 1]) )
      {
        gShouldClusterExperiments = 1;
      }
      else if ( strcmp("2", argv[i + 1]) )
      {
        gShouldClusterExperiments = 1;
        keCentered = 1;
      }
    }
  
    /* pearson correlation or euclidean distance metric */
    else if ( !strcmp("-p", argv[i]) )
    {
      if ( !strcmp("1", argv[i + 1]) )
      {
        /* default, no action needed */
      }
      else if ( !strcmp("0", argv[i + 1]) )
      {
        gUsePearson=0;
      }
    }

    /* SOM parameter */
    else if ( !strcmp("-s", argv[i]) )
    {
      if ( !strcmp("0", argv[i + 1]) )
      {
        /* default, no action needed */
      }
      else if ( !strcmp("1", argv[i + 1]) )
      {
        gPartition = 1; /* want to make SOMs */
      }
    }

    /* x parameter */
    else if ( !strcmp("-x", argv[i]) )
    {
      *x = (int) StringToReal( argv[i + 1] );
    }

    /* y paramter */
    else if ( !strcmp("-y", argv[i]) )
    {
      *y = (int) StringToReal( argv[i + 1] );
    } 

    /* r parameter */
    else if ( !strcmp("-r", argv[i]) )
    {
      if ( !strcmp("1", argv[i + 1]) )
      {
        /* default, no action needed */
      }
      else if ( !strcmp("0", argv[i + 1]) )
      {
        gRandomize=0;
      }
    }
  }
  
  if(!foundFileName)
  {
    Error("You passed in command line arguments without specifying a filename");
  }
  
  if ( gPartition==1 && !( (*x>0) && (*y>0) ) )
  {
    Error("You indicated you wanted to make a SOM, but didn't \nspecify \
           positive arguments for both the x and y dimensions");
  }
}
