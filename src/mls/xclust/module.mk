CURDIR := $(MLSDIR)/xclust

XCLUSTSRC := $(addprefix $(CURDIR)/, latest_cluster.c)
SRC += $(XCLUSTSRC)
CFLAGS += -I$(CURDIR)

XCLUSTLIBS = $(BASEDIR)/da/libda$(LIBEXT) \
            $(BASEDIR)/nr/libnr$(LIBEXT) \
	    $(BASEDIR)/ut/libut$(LIBEXT) \
	    $(MLSDIR)/common/libcommon$(LIBEXT) \
	    $(MLSDIR)/normalizations/libnorm$(LIBEXT) \
	    $(MLSDIR)/distances/libdist$(LIBEXT) \
	    $(MLSDIR)/initializations/libinit$(LIBEXT)
XCLUSTLIBDIRFLAGS := $(call make_libdirflag,$(XCLUSTLIBS))
XCLUSTLIBLINKFLAGS := $(call make_liblinkflag,$(XCLUSTLIBS)) -lm

XCLUST := $(CURDIR)/xclust$(BINEXT)
TARGETBINS += $(XCLUST)

$(XCLUST): $(XCLUSTSRC:.c=$(OBJEXT)) $(XCLUSTLIBS)
	$(CC) $(XCLUSTLIBDIRFLAGS) -o $@ $(XCLUSTSRC:.c=$(OBJEXT)) $(XCLUSTLIBLINKFLAGS)
