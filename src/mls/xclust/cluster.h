/* File : cluster.h : contains all function protypes, structs, #includes, #defines etc. ****************/

/********************************************************************** # includes *********************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

/********************************************************************** # defines **********************/

#define CR	0x0D
#define LF	0x0A
#define NODATA 99999.00
#define MAXBESTCORRELATIONS 13 /* keep track of up to the 13 best correlations */
#define BOTH 1 /* these three #defines are used to indicate whether subtrees are compound nodes */
#define LEFT 2
#define RIGHT 3

/************************************************ structs that are used to store data structures *****/

/* the nameRec structure is used to identify a list of correlations associated 
 * with either a gene, or a node.  In the clusterRec struct there is an array of
 * nameRec structs that are dynamically allocated.  There is enough space allocated
 * to store 2*numLines-1 nameRecs, where numLines is the number of genes for which
 * there is data.  The nameRecs from 0 to numLines-1 correspond to genes, and the 
 * nameRecs from numLines to 2*numLines-1 correspond to nodes.  This same array is
 * reused to store the correlations that are used when experiments are clustered.  In
 * this case 2*numExperiments-1 nameRecs are used, the first numExperiments worth are used for
 * experiments, the rest for compound nodes.
 */

typedef struct{
  char *orf;
  char *name;
  float rowWeight;
  int joined; /* to check whether a gene/experiment or node has been joined to another gene/experiment or node yet */
  struct correlationRec *first;
  struct correlationRec *last;
  int numCorrelations;
} nameRec;

typedef struct{
  nameRec *genes;
  int numGenes;
} clusterRec;

typedef struct correlationRec{
  int ORFnumber;
  float corr;
  struct correlationRec *next;
} correlationRec;

/* The nodeRec allows a binary tree to be built that represents the cluster
 * itself.  The nodeRec simply stores the correlation with which two nodes/genes
 * are joined, and pointers to nodes to the left and right nodeRecs.  At the 
 * leaves, these pointers will be NULL.  Thus this allows the value in the stored
 * float to correspond to a particular gene instead.  The nodeRec struct is used for
 * clustering both genes and experiments, though the memory itself is not reused.
 */

typedef struct nodeRec{
  float *summedData;
  float *dataWeights;
  float joiningCorr;
  struct nodeRec *left;
  struct nodeRec *right;
} nodeRec;

typedef struct SOM{
  int x;
  int y;
  int iterations;
} SOM;


/************************************************************** Global Variables ******************************/

int gLogData=0; /* whether to log transform the data */
int kgCentered=0; /* keeps track of whether to use a centered metric for the genes */
int keCentered=0; /* keeps track of whether to use a centered metric for the experiment */
int gShouldClusterGenes=1; /* keeps track of whether to cluster genes */
int gShouldClusterExperiments=0; /* Keeps track of whether to cluster experiments */
int gUsePearson=1; /* if they want to use Pearson correlation or Euclidean distance */
int gRandomize=1; /* if they want to seed the random number generator */
int gPartition=0; /* the method by which they want to partition the data */
int gOrganize=1; /* the method by which they want to organize the data */
char* gPrefix; /* if they want to pass in a unique identifier, as oppposed to using the filename */
int  gUID=0; /* whether they passed in a unique identifier */

/************************************************************** Function Prototypes ***************************/

/************************************************************** General Functions *****************************/

int     main(int argc, char *argv[]);
void    ParseOptions(char *ifile, int argc, char **argv, int *x, int *y);
void	MakeFileNames(char *ifile, char **gtrFileName, char **cdtFileName, char **atrFileName);
char	*GetFilePrefix(char *ifile);

void	GetUserInput(int *x, int *y);
void	GetTransformationOptions(void);
void	GetPartitionOptions(int *x, int *y);
void	GetOrganisationMethod(void);
void	GetDistanceMeasure(void);
void	GetGeneMetric(void);
void	GetExperimentOptions(void);
void	CheckYesOrNo(char *inputLine);

void	GetDataSize(FILE *istream, int *numExperiments, int *numLines);
void	DoMemoryAllocation(float **eWeights, int numExperiments, char ***experimentNames,
			   clusterRec *cluster, float **dataMatrix, int *matrixWidth, int x, int y);

float	*ReadInData(FILE *istream, clusterRec *cluster, int numExperiments, float *eWeights, 
		    char **experimentNames, float *dataMatrix, int matrixWidth);
void	InitializeArray(nameRec *names);
void	ReadOneLine(FILE *istream, float *dataMatrix, int numExperiments, int currLine, nameRec *names, int matrixWidth);
double	StringToReal(char *s);
FILE	*OpenInFile(char *ifile);
FILE	*OpenOutFile(char *ofile);
FILE    *OpenForAppend(char *ofile);
void LogTransformData(float *dataMatrix, int numGenes, int numExperiments, int matrixWidth);

int		*MakeRandomOrder(int numGenes);
void	AllocateTempMemory(int numInSOM, int numExperiments, float **tempMatrix, int *tempMatrixWidth, clusterRec *tempCluster);
void	PopulateTempMemory(int file, int numExperiments, int tempMatrixWidth, clusterRec *tempCluster, float *tempMatrix, 
						clusterRec *cluster, int *bestHits, float *dataMatrix);

/*************************************************************** Functions for Hierarchically Clustering ********/

void    HierarchicallyCluster(char *ifile, int numExperiments, clusterRec *cluster, float *dataMatrix, float *eWeights,
			      int numLines, int matrixWidth, char **experimentNames);

void	FreeCluster(clusterRec *cluster);
void	MakeCorrelations(clusterRec *cluster, float *dataMatrix, int numExperiments, float *eWeights,
			 int total, char type, int matrixWidth);
float	CalculateCorrelation(int geneCounter, int comparedToCounter, float *dataMatrix, int numExperiments,
			     float *eWeights, char type, int numGenes, clusterRec *cluster, int matrixWidth);
void	CheckToInsert(clusterRec *cluster, int geneCounter, int comparedToCounter, float pearsonCorrelation);
void	InsertSorted(correlationRec **list, correlationRec *newOne);
correlationRec *SwitchLast(correlationRec **list, correlationRec* newOne);
correlationRec	*DeleteLast(correlationRec **list);
correlationRec	*MakeNewRecord(double correlation, int geneNumber);
void	ClusterNodes(clusterRec *cluster, float *dataMatrix, int numExperiments, float *eWeights, 
		     nodeRec *nodeRecArray, char *gtrFileName, int matrixWidth);
void 	FindMaxCorrelation(clusterRec *cluster, int *node1, int *node2, int nodeCounter, int maxNumber);
void	JoinNodes(int node1, int node2, int numExperiments, float *dataMatrix, clusterRec *cluster, 
		  int nodeCounter, nodeRec *nodeRecArray, float *eWeights, 
		  int matrixWidth, int maxNumber, char type, int numDataPoints);
void	DoNode(float *eWeights, clusterRec * cluster, int node, float *dataMatrix, nodeRec *nodeRecArray, int nodeCounter, 
	       int matrixWidth, int maxNumber, int numDataPoints, char type);
void	AddInSingleData(float *eWeights, clusterRec *cluster, int node, float *dataMatrix, nodeRec *nodeRecArray, int nodeCounter, int matrixWidth, 
			int numDataPoints, char type);
void	AddInNodeData(int node, nodeRec *nodeRecArray, int nodeCounter, int numGenes, int numDataPoints);
void	HookUp(int node, char side, nodeRec *nodeRecArray, int nodeCounter, int maxNumber);
void 	ReverseIfNecessary(nodeRec *node, int number, float *dataMatrix, int numExperiments, float *eWeights, 
			   clusterRec *cluster, int matrixWidth, char type);
int		IsLeaf(nodeRec *theNode);
int 	GetLeftMost(nodeRec *node);
int 	GetRightMost(nodeRec *node);
void	PrintTreeFile(nodeRec *arrayRecArray, FILE *atrFile, char type, int numNodes);
void 	Reverse (nodeRec* node);
void 	SwitchLeftAndRight(nodeRec *node);
void 	FreeCorrelations(correlationRec **node);
void 	DeleteOldNodesFromLists(clusterRec *cluster, correlationRec **node, int gene, int *numCorrelations, 
				int nodeCounter, float *dataMatrix, int numExperiments, float *eWeights,
				int current, int matrixWidth, char type, int maxNumber);
void 	PrintCDTFile(clusterRec *cluster, float *dataMatrix, int numExperiments, nodeRec *nodeRecArray, 
		     char **experimentNames, int *experimentOrder, char *cdtFileName, int matrixWidth, 
		     int experimentsWereClustered);
void 	PrintLeaves(nodeRec *tree, int numExperiments, float *dataMatrix, clusterRec *cluster, FILE *outfile, 
		    int * experimentOrder, int matrixWidth);
void 	PrintLeaf(int correlation, int numExperiments, float *dataMatrix, clusterRec *cluster, FILE *outfile, 
		  int * experimentOrder, int matrixWidth);
void 	Error(char *msg, ...);
void 	FreeNodeRecArray(nodeRec *nodeRecArray, int numLines);
void 	ClusterExperiments(clusterRec *cluster, float *dataMatrix, int numExperiments, float *eWeights, 
			   nodeRec* arrayRecArray, char *atrFileName, int matrixWidth);
void 	ChangeExperimentOrder(int *experimentOrder, nodeRec *arrayRecArray, int *index);
void 	MakeExperimentCorrelations(clusterRec *cluster, float *dataMatrix, int numExperiments);
void 	FreeExperimentNames(char **experimentNames, int numExperiments);

/**************************************************** Functions for debugging purposes *****************************************/

void 	PrintCorrelations(clusterRec *cluster);
void 	PrintOneGene(correlationRec *list, FILE *outfile, clusterRec *cluster);
void 	PrintData(float *dataMatrix, int numExperiments, int numLines, clusterRec *cluster, float *eWeights, int matrixWidth);
void	PrintOrder(nodeRec *node);

/**************************************************** SOM function prototypes **************************************************/
 

float *MakeSOM(float *dataMatrix, clusterRec *cluster, int numExperiments, float *eWeights, int matrixWidth, char **experimentNames, char *ifile, int x, int y);

SOM *designSOM(int x, int y);

float *FilterData(float *dataMatrix, clusterRec *cluster, int numExperiments, int numNodes, char *ifile);

void MakeSOMFileNames(char **fileNames, char *ifile, int numNodes, SOM *SOMdesign);

void InitializeSOM(float *dataMatrix, clusterRec* cluster, SOM *SOMdesign, int numExperiments);

void MakeUnitVector(float *dataMatrix, int currNode, int numExperiments, int matrixOffset);

void OrganizeSOM(clusterRec* cluster, SOM *SOMdesign, float *dataMatrix, float *eWeights, int numExperiments, int matrixWidth, char *ifile);

void MakeEvolutionFileNames(char **evolutionFileNames, int numNodes, char *ifile, SOM *SOMdesign);

void PrintEvolutionFiles(char **evolutionFiles, int numNodes, float *dataMatrix, int matrixOffset, int matrixWidth, int iteration);

float PutGeneInDataMatrix(int matrixWidth, int matrixOffset, float *dataMatrix, int numNodes, int pickedGene);

void NormalizeGene(int matrixWidth, int matrixOffset, float *dataMatrix, int numNodes, float nodeLength);

void AdjustNodes(int bestNode, SOM *SOMdesign, int iteration, int numNodes, int gene, float *dataMatrix, 
				int matrixWidth, int numExperiments, int numGenes, float totalDist);

int *SortSOMData(int numExperiments, float *dataMatrix, int numGenes, int numNodes, float *eWeights, clusterRec *cluster, 
				int matrixWidth, char *ifile, SOM *SOMdesign);

void MakeCorrelationFileNames(char **correlationFilenames, char *ifile, int numNodes, SOM *SOMdesign);

void PrintSOMOrganization(int numNodes, int *bestHits, int numExperiments, char **experimentNames, clusterRec *cluster, int numGenes, float *dataMatrix,
						 int matrixWidth, char *ifile, SOM *SOMdesign, float *eWeights);
						 
void PrintSomData(float *dataMatrix, int numNodes, int matrixWidth, int numGenes, char *ifile, SOM *SOMdesign);

void PrintFirstLine(FILE* outfile, int numExperiments, char** experimentNames);

void PrintOneLine(clusterRec *cluster, int geneNumber, FILE *outfile, float *dataMatrix, int matrixWidth);

/********************************************************** K-means Functions prototypes **********************************************************/

void KMeansCluster(float *dataMatrix, clusterRec *cluster, int numExperiments, float *eWeights,
	char **experimentNames, char *ifile, int k);
	
void MakeRandomSeeds(float *dataMatrix, clusterRec* cluster, int numExperiments, int k);

void MakeClusters(float *dataMatrix, clusterRec *cluster, int numExperiments, int k, int *centroidAssignments, 
	float *eWeights, int *numInCentroids);

int  AssignGenesToCentroids(float *dataMatrix, clusterRec *cluster, int numExperiments, int k, 
	int *centroidAssignments, float *eWeights, int *numInCentroids);
	
void CalculateCentroids(float *dataMatrix, clusterRec *cluster, int numExperiments, int k, 
	int *centroidAssignments);

void OrganizeKClusters(int *centroidAssignments, int *numInCentroid, int numExperiments, char **experimentNames, clusterRec *cluster, 
int numGenes, float *dataMatrix, char *ifile, int k, float *eWeights);

void MakeKMeansFileNames(char **fileNames, char *ifile, int k);
