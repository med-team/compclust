/*
 * File : latest_cluster.c
 * This Message must remain attached to this source code at all times.
 * This source code may not be redistributed, nor may any executable
 * based upon this source code.
 * This code is copyrighted.  Software may not be based upon this code
 * without express written permission of the copyright holders.
 *
 * Authored by Gavin Sherlock 1999 (sherlock@genome.stanford.edu)
 *
 * Please notify me of any changes made to the code
 *
 * When publishing data that has been clustered using this software please cite:
 * G. Sherlock, unpublished.  http://genome-www.stanford.edu/~sherlock/cluster.html
 *
 * Version 2.51
 *
 */


#include "cluster.h"

/* Main */

int main(int argc, char *argv[]){
  
  char	ifile[1024];				/* Path names must be 1024 chars or less */
  int	numExperiments=0;
  int	numLines=0;
  float	*dataMatrix;
  clusterRec cluster;
  float  *eWeights;
  FILE	*istream;
  char **experimentNames;
  int matrixWidth;
  int x=1, y=1; /* dimensions of SOM */
  
  
  if(argc==1 || argc==0){
    printf ("Enter input file: ");
    gets (ifile);
    GetUserInput(&x, &y);
  }else{
    ParseOptions(ifile, argc, argv, &x, &y);
  }
  
  istream=OpenInFile(ifile);
  
  GetDataSize(istream, &numExperiments, &numLines);
  
  cluster.numGenes=numLines; /* put these in experiment to cut down on number of arguments that need passing */
  
  DoMemoryAllocation(&eWeights, numExperiments, &experimentNames, &cluster, &dataMatrix, &matrixWidth, x, y);
  
  rewind(istream);	
  ReadInData(istream, &cluster, numExperiments, eWeights, experimentNames, dataMatrix, matrixWidth);
  fclose (istream);
  
  if(gLogData){
    
    LogTransformData(dataMatrix, cluster.numGenes, numExperiments, matrixWidth);
    
  }
  
  switch (gPartition){
    
  case 0:	/* no partitioning */
    HierarchicallyCluster(ifile, numExperiments, &cluster, dataMatrix, eWeights, numLines, matrixWidth, 
			  experimentNames);
    break;
  case 1: /* Make SOM */
    dataMatrix=MakeSOM(dataMatrix, &cluster, numExperiments, eWeights, matrixWidth, experimentNames, ifile, x, y);
    break;
  case 2: /* k-means cluster */
    KMeansCluster(dataMatrix, &cluster, numExperiments, eWeights, experimentNames, ifile, x);
    break;
    
  }
  
  free (dataMatrix);
  free (eWeights);
  FreeExperimentNames(experimentNames, numExperiments);
  FreeCluster(&cluster);
  
  printf("Finished\n");
  fflush(stdout);
  
  return 0;
  
}


/* Function: OpenInFile
 * Usage: istream=OpenInFile(ifile)
 * ----------------------
 * This functions attempts to open a file using the name
 * passed into it, and reports an error if it fails, otherwise
 * returns a file handle for reading.
 */

FILE* OpenInFile(char *ifile){
  
  FILE *istream;
  
  if ((istream = fopen(ifile, "rb")) == NULL){ /* Open the file OK? */
    Error("\nError opening input\n");		/* NO, say so */
  } /* end if */
  return (istream);
}

/*
 * Function: MakeFileNames
 * Usage: MakeFileNames(ifile, &gtrFileName, &cdtFileName, &atrFileName)
 * -----------------------------------------------------------------------
 * This function simply determines from the name of the input file, what the
 * output files should be called.
 */

void MakeFileNames(char *ifile, char **gtrFileName, char **cdtFileName, char **atrFileName){
  
  char* prefix;
  
  prefix=GetFilePrefix(ifile);
  
  *gtrFileName=malloc((strlen(prefix)+4)*sizeof(char));
  *cdtFileName=malloc((strlen(prefix)+4)*sizeof(char));
  *atrFileName=malloc((strlen(prefix)+4)*sizeof(char));
  if(*gtrFileName==NULL||*cdtFileName==NULL||*atrFileName==NULL)Error("Not enough memory for filenames");
  strcpy(*gtrFileName, prefix);
  strcpy(*cdtFileName, prefix);
  strcpy(*atrFileName, prefix);
  strcpy(*gtrFileName+strlen(prefix), "gtr\0");
  strcpy(*cdtFileName+strlen(prefix), "cdt\0");
  strcpy(*atrFileName+strlen(prefix), "atr\0");
  
  free(prefix);
  
}

/*
 * Function : GetFilePrefix
 * Usage : prefix=GetFilePrefix(ifile)
 * -----------------------------------
 * This function returns the prefix to a filename, which
 * is all characters up to the last '.' character, or if
 * one is not encountered, then all characters.  If a unique
 * identifier was passed in, via the -u option, then the directory
 * path, plus the UID will be returned instead.
 */
 
char *GetFilePrefix(char *ifile){

  char* prefix;
  int letter=0;
  int last=0;
  int seen=0;
  int dirLet=0;

  while (ifile[letter]!='\0'){
    if (ifile[letter]=='.'){
      last=letter;
      seen=1;
    }else if (ifile[letter]=='/'){
      seen=0;
      if (gUID) dirLet=letter; /* remember the directory path */
    }
    letter++;
    if (letter==1024) Error("The name of your input file is longer than 1024 characters");
  }
  
  

  if (seen && !gUID){ /* saw a period */
    prefix=malloc((last+2)*sizeof(char));
    if (prefix==NULL) Error ("Not enough room for prefix\n");
    strncpy(prefix, ifile, last+1);
    prefix[last+1]='\0';
  }else if (!gUID){ /* no period */
    prefix=malloc((letter+2)*sizeof(char));
    if (prefix==NULL) Error ("Not enough room for prefix\n");
    strcpy(prefix, ifile);
    prefix[letter]='.';
    prefix[letter+1]='\0';
  }else{ /* a UID was passed in */
    prefix=malloc((dirLet+strlen(gPrefix)+3)*sizeof(char));
    if (dirLet) {
      strncpy(prefix, ifile, dirLet+1);
      prefix[dirLet+1]='\0';
      strcat(prefix, gPrefix);
    }else{
      strcpy(prefix, gPrefix);
    }
    strcat(prefix, ".");
    prefix[dirLet+2+strlen(gPrefix)]='\0';
  }
  
  return (prefix);
  
}

/*
 * Function: GetUserInput
 * Usage: GetUserInput(&x, &y)
 * ---------------------------
 * This function gets input form the user, as regards whether they want to cluster
 * experiments, and whether they want to use a centered metric for correlation.
 * It also checks whether they want to partition the data (and how), whether they
 * want to log transform the data, and whether they want to normalize of filter the data.
 */
 
void GetUserInput(int *x, int *y){

  	GetTransformationOptions();
  		
  	GetPartitionOptions(x, y);
  	
  	/* GetOrganisationMethod(); */ /* not yet needed, as there's only one method */
  
   	GetDistanceMeasure();
  
  	GetExperimentOptions();
  	
}


/*
 * Function: GetTransformationOptions
 * Usage:
 * ----------------------------------
 * This function checks whether they want to log transform the data
 */
 
void GetTransformationOptions(void){

	char inputLine[64];
 
 	printf("\nDo the data need to be log transformed?(yes/no)\n");
 	
 	CheckYesOrNo(inputLine);
 	
 	if ((!strcmp(inputLine, "yes"))||(!strcmp(inputLine, "YES"))){
		gLogData=1;
	}

}

/*
 * Function: GetPartitionOptions
 * Usage:
 * -----------------------------
 * This function checks if, and how they want to partition the data
 */
 
void GetPartitionOptions(int *x, int *y){

	char inputLine[64];
	
	while (1){
 
 		printf("\nWhat method do you want to use to partition the data?\n\n");
    	printf("None\t- 0\nSOMs\t- 1\nk-means\t- 2\n\n");
    
    	gets(inputLine);
    
    	if(!strcmp(inputLine, "0")) break; /* no partitioning */
      
      	if(!strcmp(inputLine, "1")){ /* they want to make a SOM */
	
			gPartition=1;
			while(1){
	  			printf("\nPlease enter the dimensions you want for your SOM.\n\nx:");
	  			gets(inputLine);
	  			*x=StringToReal(inputLine);
	  			printf("y:");
	  			gets(inputLine);
	  			*y=StringToReal(inputLine);
	  			if (((*x)*(*y))>200) {
	    			printf ("That's %d SOM nodes.  Are you kidding? (yes/no)\n", (*x)*(*y));
	    			CheckYesOrNo(inputLine);
	    			if ((!strcmp(inputLine, "no"))||(!strcmp(inputLine, "no"))) break; /* they really want >200 nodes */
	  			}else{ /* they wanted <200 nodes */
	    			break;
	  			}
			}	
	
      	}else if (!strcmp(inputLine, "2")){ /* they want to make k-means clusters */
	
			gPartition=2;
		
			printf("\nPlease enter the number of k-means clusters you'd like.\n\nk:");
			gets(inputLine);
			*x=StringToReal(inputLine);
		
   	   	}else{
   	   	
   	   		printf("Please enter \"0\", \"1\", or \"2\"\n");
   	   		continue;
   	   	}
   	   	
   	   	/* if we get here, they want either k-means or SOMs, so check whether they want to randomize or not */
   	   		
   	   	printf("\nDo you want to randomize the partioning (yes/no)\n");
		CheckYesOrNo(inputLine);
	    if ((!strcmp(inputLine, "no"))||(!strcmp(inputLine, "NO"))){
	    	gRandomize=0;
	   	}
   	   
   	   	break; /* they gave a reasonable answer if we got here */
   	   		
   	}
   	   
}

/*
 * Function: GetClusterMethod
 * Usage:
 * --------------------------
 * This function checks what organisation method they want to use.
 * At present there is only one method (Agglomerative Clustering), but in future
 * this will hopefully change (eg Divisive Clustering)
 */

void GetOrganisationMethod(void){

	char inputLine[64];
  
	while (1){
    
    	printf("\nWhat method do you want to use to organize the data?\n\n");
    	printf("Agglomerative Clustering - 1\n\n");
    
    	gets(inputLine);
    	
    	if (!(!strcmp(inputLine, "1"))){
    		printf("Please answer 1\n");
    	}else {
      		gOrganize=1;
     		break;
    	}
  	}
}

/*
 * Function: GetDistanceMeasure
 * Usage:
 * ----------------------------
 * This function checks what distance measure (Pearson correlation or
 * Euclidean distance) they want to use.  More measures will (hopefully) be added
 * in future.
 */
 
void GetDistanceMeasure(void){

	char inputLine[64];
	
	while (1){
    	printf("\nWhat distance measure do you want to use?\n\n");	
    	printf("Pearson Correlation - 0\nEuclidean Distance  - 1\n\n");	
    	gets(inputLine);		
    	if (!((!strcmp(inputLine, "0"))||(!strcmp(inputLine, "1")))){		
      		printf("Please answer '0' or '1'\n");
    	}else {
      		if (!strcmp(inputLine, "0")){ /* find out what pearson metric they want */
      			printf("\nDo you want to cluster genes (yes/no)? \n");
    			CheckYesOrNo(inputLine);
    			if ((!(strcmp(inputLine, "YES")))||(!(strcmp(inputLine, "yes")))){
		   			GetGeneMetric();
		   		}else{
   					gShouldClusterGenes=0;
   				}
      		}else{
				gUsePearson=0;
      		}
      		break;
    	}
  	}
  	
}

/*
 * Function: GetGeneMetric
 * Usage: 
 * ------------------------
 * This function checks whether they want to use a centered or 
 * uncentered metric for the genes, if pearson was chosen as the
 * distance metric.
 */
 
void GetGeneMetric(void){

	char inputLine[64];
	
    printf("Do you want to use a correlation with a centered metric for the genes (yes/no)? \n");
    CheckYesOrNo(inputLine);	
    	
    if ((!strcmp(inputLine, "yes"))||(!strcmp(inputLine, "YES"))){
		kgCentered=1;
   	}
  	
}

/*
 * Function: GetExperimentOptions
 * Usage:
 * ------------------------------
 * This functions checks whether they want to cluster by experiment as well, and if so
 * whether they want a centered or uncentered metric, if they are using Pearson Correlation
 */
 
void GetExperimentOptions(void){

	char inputLine[64];

	printf("Do you want to cluster experiments (yes/no)? \n");
    
    CheckYesOrNo(inputLine);
    
    if ((!(strcmp(inputLine, "YES")))||(!(strcmp(inputLine, "yes")))){

		gShouldClusterExperiments=1;

		if(gUsePearson){

			printf("Do you want to use a correlation with a centered metric for the experiments (yes/no)? \n");
			CheckYesOrNo(inputLine);
	  			
	  		if ((!strcmp(inputLine, "yes"))||(!strcmp(inputLine, "YES"))){
	      		keCentered=1;
	    	}
	    							
      	}
    
    }
}

/*
 * Function: GetYesOrNo
 * Usage:
 * --------------------
 * This function simply asks for an input, and checks whether it is yes or no.
 * It stays continues prompting for a yes or no until one is received.
 */
 
void CheckYesOrNo(char *inputLine){

	while(1){
		gets(inputLine);
		if (!((!strcmp(inputLine, "yes"))||(!strcmp(inputLine, "YES"))||(!strcmp(inputLine, "no"))
			||(!strcmp(inputLine, "NO")))){
	    	printf("Please answer yes or no\n");
		}else{
			break;
		}
	}
}

/*
 * Function: ParseOptions
 * Usage: ParseOptions(ifile, argc, argv, &x, &y)
 * ----------------------------------------------
 * This function parses supplied command line options.
 * Command line options are as follows:
 * -f filename
 * -g 1|2 ; 1 indicates non-centered metric, 2 indicates centered metric when clustering genes.  1 is default.
 * -e 0|1|2 ; 0 indicates no experiment clustering, see above for 1 and 2.  0 is the default.
 * -p 0|1 ; whether to use pearson correlation (1), or Euclidean distance (0).  1 is the default.
 * -s 0|1 ; whether to make a SOM (1), or hierarchically cluster (0).  0 is the default.
 * -x specify x dimension of SOM
 * -y specify y dimension of SOM
 * -r 0|1 ; whether to seed the random number generator with the time when making a SOM.  1 is the default.
 * The arguments can be passed in any order.
 */

void ParseOptions(char *ifile, int argc, char** argv, int *x, int *y){
  
	int i;
 	int foundFileName=0;
  	int length;
  
  	if ((argc-1)%2)Error("Incorrect number of command line arguments");
  
  	for(i=1; i<argc; i+=2){
    
  		if (!(strcmp("-k", argv[i]))){     /* k-means clustering */
      
      		gPartition=2;
      		*x=(int)StringToReal(argv[i+1]);
      
      		if(*x<=0) Error("You must have more than zero clusters");
      
    	}else if (!(strcmp("-l", argv[i]))){  /* log transformation */
      
      		if(!(strcmp("0", argv[i+1]))){ /* default, no action needed */
	
      		}else if (!(strcmp("1", argv[i+1]))){
	
				gLogData=1;
	
      		}else{
	
				Error("Unrecognized value for the \"-g\" option.  Only 1 and 2 are valid");
	
      		}
      
    	}else if (!(strcmp("-u", argv[i]))){
      
      		length=strlen(argv[i+1]);
      
      		gPrefix=malloc((length+1)*sizeof(char));
      
      		strcpy(gPrefix, argv[i+1]);
      
      		gPrefix[length]='\0';
      
      		gUID=1;
      
    	}else if (!(strcmp("-f", argv[i]))){
      
      		strcpy(ifile, argv[i+1]);
      
      		foundFileName=1;
      
    	}else if (!(strcmp("-g", argv[i]))){ /* genes parameter */
    	
    		if (!(strcmp("0", argv[i+1]))){ /* they don't want to cluster genes */
    		
    			gShouldClusterGenes=0;
      
      		}else if (!(strcmp("1", argv[i+1]))){ /* default, no action needed */
	
      		}else if (!(strcmp("2", argv[i+1]))){
	
				kgCentered=1;
	
      		}else{
	
				Error("Unrecognized value for the \"-g\" option.  Only 0, 1 and 2 are valid");
	
      		}
      
    	}else if (!(strcmp("-e", argv[i]))){
      
      		if(!(strcmp("0", argv[i+1]))){ /* default, no action needed */
	
      		}else if (!(strcmp("1", argv[i+1]))){
	
				gShouldClusterExperiments=1;
	
      		}else if (!(strcmp("2", argv[i+1]))){
	
				gShouldClusterExperiments=1;
	
				keCentered=1;
	
      		}else{
	
				Error("Unrecognized value for the \"-e\" option.  Only 0, 1 and 2 are valid");
	
      		}
      
    	}else if (!(strcmp("-p", argv[i]))){
      
      		if(!(strcmp("1", argv[i+1]))){ /* default, no action needed */
	
      		}else if (!(strcmp("0", argv[i+1]))){
	
				gUsePearson=0;
	
      		}else{
	
				Error("Unrecognized value for the \"-p\" option.  Only 0 and 1 are valid");
	
      		}
      
    	}else if (!(strcmp("-s", argv[i]))){
      
      		if(!(strcmp("0", argv[i+1]))){ /* default, no action needed */
	
      		}else if (!(strcmp("1", argv[i+1]))){
	
				gPartition=1; /* want to make SOMs */
	
      		}else{
	
				Error("Unrecognized value for the \"-s\" option.  Only 0 and 1 are valid");
	
      		}
      
    	}else if (!(strcmp("-x", argv[i]))){
      
      		*x=(int)StringToReal(argv[i+1]);
      
    	}else if (!(strcmp("-y", argv[i]))){
      
      		*y=(int)StringToReal(argv[i+1]);
      
    	}else if (!(strcmp("-r", argv[i]))){
      
      		if(!(strcmp("1", argv[i+1]))){ /* default, no action needed */
	
      		}else if (!(strcmp("0", argv[i+1]))){
	
				gRandomize=0;
	
      		}else{
	
				Error("Unrecognized value for the \"-s\" option.  Only 0 and 1 are valid");
	
      		}
      
    	}else{
      
      		Error("Unrecognized command line option.  Only -f, -e, -g, -p, -s, -x, -y and -r are valid (currently!)");
      
    	}
    
  	}
  
  	if(!foundFileName)Error("You passed in command line arguments without specifying a filename");
  
  	if(gPartition==1 && !((*x>0) && (*y>0)))Error("You indicated you wanted to make a SOM, but didn't \nspecify positive arguments for both the x and y dimensions");
  
}

/*
 * Function: OpenOutFile
 * Usage: outfile=OpenOutFile("outfile.txt")
 * -----------------------------------------
 * This functions attempts to open a file using the name
 * passed into it, and reports an error if it fails, otherwise
 * returns a file handle for writing.
 */
 
FILE* OpenOutFile(char *ofile){

  FILE *ostream;
  
  if ((ostream = fopen(ofile, "w")) == NULL){ /* Open the file OK? */
    Error("\nError opening output file: %s\n", ofile);		/* NO, say so */
  } /* end if */
  return (ostream);
}

/*
 * Function: OpenForAppend
 * Usage: outfile=OpenForAppend("outfile.txt")
 * -----------------------------------------
 * This functions attempts to open a file for appending using the name
 * passed into it, and reports an error if it fails, otherwise
 * returns a file handle for appending.
 */
 
FILE* OpenForAppend(char *ofile){

  FILE *ostream;
  
  if ((ostream = fopen(ofile, "a")) == NULL){ /* Open the file OK? */
    Error("\nError opening output file: %s\n", ofile);		/* NO, say so */
  } /* end if */
  return (ostream);
}

/* Function: GetDataSize
 * Usage:GetDataSize(istream, &numExperiments, &numLines)
 * --------------------------------------------------
 * This functions parses the data file, to determine the number
 * of genes and experiments represented therein.  Because the
 * variables are passed in by address then there is no return argument.
 */

void GetDataSize(FILE *istream, int *numExperiments, int *numLines){
	
  int	nextbyte;
  int	extraChar;
  
  printf("Getting size of data...\n");

  fflush(stdout);
  
  while ((nextbyte = fgetc(istream))){ /* get characters from input file */
    
    if (nextbyte=='\t'){ /* count tabs to get number of columns */
      (*numExperiments)++;
    }
    
    /* the next bit looks for the end of line, whether it's PC ('\r\n'), 
     * Mac ('\r') or UNIX ('\n').  It checks to see if the following character
     * has to do with the end of line also.  If not, it puts it back in the stream.
     */
    
    if ((nextbyte=='\r') || (nextbyte=='\n')){ /* look for end of line */
      extraChar=fgetc(istream);
      if (extraChar!='\n'  && extraChar!=EOF){
	ungetc(extraChar, istream);
      }
      break; /* only reading first line to get number of columns */
    }
     
  }
  
  (*numExperiments)-=2; /* the first two tabs didn't precede data so decrease the number */
  
  while((nextbyte = fgetc(istream)) != EOF){  /* Read char.s until end of file */
    switch (nextbyte)						/* What char was read? */
      {
      case '\r': /* on the Mac or the PC, \r will be the first thing seen to indicate an end of line */
	(*numLines)++;
	extraChar=fgetc(istream); /* get rid of extra character if needs be */
	if (extraChar!='\n'  && extraChar!=EOF){
	  ungetc(extraChar, istream);
	}
	break;
      case '\n': /* on UNIX \n will be seen as the end of line */
	(*numLines)++;
	break;
      default:
	break;					
      } /* end switch */
  } /* end while */
  
  (*numLines)--; /* because there's an extra line in the file */
  
}

/*
 * Function: DoMemoryAllocation
 * Usage: DoMemoryAllocation(&eWeights, numExperiments, &experimentNames, &cluster, &dataMatrix, &matrixWidth, x, y);
 * -------------------------------------------------------------------------------------------------------------------
 * This function does the DMA that is needed for all the data structures that are required,
 * including the dataMatrix, the experiment names, and the nameRecs.
 */
 
void DoMemoryAllocation(float **eWeights, int numExperiments, char ***experimentNames,
			clusterRec *cluster, float **dataMatrix, int *matrixWidth, int x, int y){
  
  nameRec *namePtr;
  
  *eWeights=malloc(numExperiments*sizeof(float));
  if (*eWeights==NULL) Error("Not enough memory available for eWeights");
  
  *experimentNames=malloc(numExperiments*sizeof(char*));
  if (*experimentNames==NULL) Error ("No memory available for experimentNames");
  
  if (!gPartition){
    
    if (numExperiments >cluster->numGenes && gShouldClusterExperiments){
      (*cluster).genes=malloc((2*(numExperiments)-1) * (sizeof(nameRec)));/* allocate the memory for the name records */
    }else{
      (*cluster).genes=malloc((2*(cluster->numGenes)-1) * (sizeof(nameRec)));/* allocate the memory for the name records */
    }
    
  }else{
    
    (*cluster).genes=malloc(cluster->numGenes * (sizeof(nameRec))); /* allocate the memory for the name records */
    
  }
  
  if ((*cluster).genes==NULL) Error("No memory available for nameRecs");
  
  /* now initialize the genes array */
  
  if (!gPartition){
    if (numExperiments >cluster->numGenes && gShouldClusterExperiments){
      for (namePtr=&(cluster->genes[0]); namePtr<&(cluster->genes[2*numExperiments-1]); namePtr++){
	InitializeArray(namePtr);
      }
    }else{
      for (namePtr=&(cluster->genes[0]); namePtr<&(cluster->genes[2*cluster->numGenes-1]); namePtr++){
	InitializeArray(namePtr);
      }
    }
  }else{
    for (namePtr=&(cluster->genes[0]); namePtr<&(cluster->genes[cluster->numGenes]); namePtr++){
      InitializeArray(namePtr);
    }
  }
  
  /* allocate enough memory in dataMatrix to allow 
   * experiments to be clustered, only if they've asked to.
   */
  
  if (!gPartition && gShouldClusterExperiments){
    *dataMatrix=malloc((2*numExperiments-1)*(2*cluster->numGenes-1)*sizeof(float));
    *matrixWidth=2*numExperiments-1;
  }else if (!gPartition){ /* no partitioning */
    *dataMatrix=malloc(numExperiments*(2*cluster->numGenes-1)*sizeof(float));
    *matrixWidth=numExperiments;
  }else if (gPartition==1){ /* making SOMs */
    *dataMatrix=malloc(numExperiments*(cluster->numGenes+(x*y)+1)*sizeof(float)); /* numNodes, + 1 extra for the currently considered gene, as a unit vector */
    *matrixWidth=numExperiments;
  }else if (gPartition==2){ /* partitioning by k-means */
    *dataMatrix=malloc(numExperiments*2*cluster->numGenes*sizeof(float));
    *matrixWidth=numExperiments;
  }
  
  
  if (*dataMatrix==NULL) Error("No memory available for dataMatrix");
  
}

/*
 * Function: ReadInData
 * Usage: ReadInData(istream, &cluster, numExperiments, eWeights, numLines, &experimentNames, dataMatrix, matrixWidth)
 * -------------------------------------------------------------
 * This functions parses the data file, to retrieve the log transformed data.
 * it also retrieves the eWeights, that form the second line of the file.
 */
 
float *ReadInData(FILE *istream, clusterRec *cluster, int numExperiments, float *eWeights, char **experimentNames, float *dataMatrix, int matrixWidth){
  char buffer[1024];
  char nextbyte;
  char extraChar;
  int currCol=0;
  int letter=0;
  int dataCol=0;
  int currLine=0;
  
  printf("Reading Data...\n");

  fflush(stdout);
  
  /* parse the first line for the experiment names
   * should really consolidate code for parsing first and second lines
   * by switching again, dependent on the lineNumber, 1 or 2
   */
  
  while ((nextbyte=fgetc(istream))){
    if (nextbyte=='\t'){
      buffer[letter]='\0';
      switch (++currCol){ /* depends what column we're looking at as to what we'll do */
      case 1: 
      case 2: 
      case 3: 
	letter=0;
	break;
      default: /* other columns must be data columns */
	if (letter!=0){
	  experimentNames[dataCol]=malloc((letter+1)*sizeof(char));
	}else{
	  Error("An experiment is unnamed at data column %d\n", dataCol+1);
	}
	if (experimentNames[dataCol]==NULL) Error("Not enough memory for experiment name %d", dataCol+1);
	strcpy(experimentNames[dataCol], buffer);
	letter=0;
	dataCol++;
	break;
      } /* end switch */
    }else if (nextbyte=='\r' || nextbyte=='\n'){
      buffer[letter]='\0';
      if (letter!=0){
	experimentNames[dataCol]=malloc((letter+1)*sizeof(char));
      }else{
	Error("An experiment is unnamed at data column %d\n", dataCol+1);
      }			
      if (experimentNames[dataCol]==NULL) Error("Not enough memory for experiment name %d", dataCol+1);
      strcpy(experimentNames[dataCol], buffer);
      extraChar=fgetc(istream); /* get rid of extra character if needs be */
      if (extraChar!='\n'  && extraChar!=EOF){
	ungetc(extraChar, istream);
      }
      break;
    }else{	
      if (letter>=1024) Error("An experiment name longer than 1024 bytes exists in the file on the first line.");
      buffer[letter++]=nextbyte;
    }
  }
  
  letter=0;
  dataCol=0;
  currCol=0;
  
  /* Now read in the eWeights line */
  while ((nextbyte=fgetc(istream))){
    if (nextbyte=='\t'){
      buffer[letter]='\0';
      switch (++currCol){
      case 1: 	
      case 2:	
      case 3: 
	letter=0;
	break;
      default: /* other columns must be data columns */
	eWeights[dataCol]=StringToReal(buffer);
	letter=0;
	dataCol++;
	break;
      } /* end switch */
    }else if (nextbyte=='\r' || nextbyte=='\n'){
      	buffer[letter]='\0';
      	eWeights[dataCol]=StringToReal(buffer);
      	extraChar=fgetc(istream); /* get rid of extra character if needs be */
	if (extraChar!='\n'  && extraChar!=EOF){
	  ungetc(extraChar, istream);
    	}
      	break;
    }else{	
      if (letter>=1024) Error("A token longer than 1024 bytes exists in the file on the second line.");
      buffer[letter++]=nextbyte;
    }
  }
  
  for (currLine=0; currLine<cluster->numGenes; currLine++){
    ReadOneLine(istream, dataMatrix, numExperiments, currLine, &(cluster->genes[currLine]), matrixWidth);
  }
  
  printf("Done reading data...\n");
  fflush(stdout);
  return (dataMatrix);
}

/*
 * Function: InitializeArray
 * Usage: InitializeArray(&(names[count]));
 * ----------------------------------------
 * This simply initializes the fields of each nameRec to be zero or NULL
 */

void InitializeArray(nameRec *names){	
  names->orf=NULL;
  names->name=NULL;
  names->joined=0;
  names->numCorrelations=0;
  names->last=NULL;
  names->first=NULL;
}

/* Function: ReadOneLine
 * Usage: ReadOneLine(istream, dataMatrix, &numDataPoints, numExperiments, currLine, &(names[currLine]))
 * ---------------------------------------------------
 * This function is called from ReadInData, and will parse one
 * line of data, depositing the data into dataMatrix, the space for
 * which was allocated in the ReadInData function.
 */

void ReadOneLine(FILE *istream, float *dataMatrix, int numExperiments, int currLine, nameRec *names, int matrixWidth){
  char buffer[1024];
  int currCol=0;
  int dataCol=0;
  int tabCount=0; /* if two tabs next to each other, then a null value exists */
  char nextbyte;
  char extraChar;
  int letter=0;
  
  /* initialize gene records */
  
  while ((nextbyte=fgetc(istream))!=EOF){
    
    switch (nextbyte){
    case '\t':
      tabCount++;
      if (dataCol>=numExperiments) Error("There are more columns of data than expected on data line %d.  Only %d columns were expected.\n", currLine+1, numExperiments);
      if (tabCount==2){ /* a null data point exists, or there's a missing name */
	if (currCol>2){
	  dataMatrix[currLine*matrixWidth+dataCol]=NODATA;
	  currCol++;
	  dataCol++;
	  tabCount=1;
	  letter=0;
	  break;
	}else{
	  tabCount=0;
	}
      }
      buffer[letter]='\0';
      switch (currCol++){
      case 0: /* First column is the ORF name */
	if (letter>0){
	  names->orf=malloc((letter+1)*sizeof(char));
	  if (names->orf==NULL) Error("Not Enough Memory Available for orfs\n");
	  strcpy(names->orf, buffer);
	}else{
	  /*printf("Missing Name\n");*/
	}
	letter=0;
	break;
      case 1: /* Second column is the SGD name */
	if (letter>0){
	  names->name=malloc((letter+1)*sizeof(char));
	  if (names->name==NULL) Error("Not Enough Memory Available for names\n");
	  strcpy(names->name, buffer);
	}else{
	  /*printf("Missing Name\n");*/
	}
	letter=0;
			break;
      case 2: /* Third column is the row weight */
	names->rowWeight=(float)(StringToReal(buffer));
	letter=0;
	break;
      default: /* other columns must be data columns */
	dataMatrix[currLine*matrixWidth+dataCol]=(float)(StringToReal(buffer));
	letter=0;
	dataCol++;
	break;
      }		
      break;
    case '\r':
    case '\n':
      extraChar=fgetc(istream); /* get rid of extra character if needs be */
      if (extraChar!='\n'  && extraChar!=EOF){
	ungetc(extraChar, istream);
      }
      tabCount++;
      if (dataCol>=numExperiments) Error("There are more columns of data than expected on data line %d.  Only %d columns were expected, but %d were found.\n", currLine+1, numExperiments, dataCol+1);
      if(dataCol<numExperiments-1)Error("There are less columns of data than expected on data line %d.  %d columns were expected, but only %d were found.\n", currLine+1, numExperiments, dataCol+1);
      if (tabCount==2){ /* a null data point exists */
	dataMatrix[currLine*matrixWidth+dataCol]=NODATA;
      }else{
	buffer[letter]='\0';
	dataMatrix[currLine*matrixWidth+dataCol]=(float)(StringToReal(buffer));
      }
      return;
      break;
    default:
      if (letter>=1024) Error("A token longer than 1024 bytes exists in the file on line %d", currLine);
      buffer[letter++]=nextbyte;
      tabCount=0;
      break;
    }
  }
   
}


/* 
 * Function: StringToReal
 * Usage: s=StringToReal(buffer)
 * ---------------------------
 * This function converts a string representing a real number
 * into its corresponding value.  If the string is not a legal
 * floating point number, or it contains extraneous characters,
 * StringToReal signals an Error condition.  This code was taken
 * from Eric Roberts' book "The Art and Science of C".
 */
 
double StringToReal(char *s){
	
  double result;
  char dummy;
  
  if (s==NULL) Error("NULL string passed to StringToReal");
  if (sscanf(s, " %lg %c", &result, &dummy) !=1){
    Error("StringToReal called on illegal number %s", s);
  }
  return (result);
  
}



/*
 * Function : HierarchicallyCluster
 * Usage : HierarchicallyCluster(ifile, numExperiments, &cluster, dataMatrix, eWeights, numLines, matrixWidth, experimentNames)
 * -----------------------------------------------------------------------------------------------------------------------------
 * This functions handles all the necessary calls that
 * are required to make a hierarchical cluster.
 */
 
void HierarchicallyCluster(char *ifile, int numExperiments, clusterRec *cluster, float *dataMatrix, float *eWeights, 
			   int numLines, int matrixWidth, char **experimentNames){
  
  char *gtrFileName;
  char *cdtFileName;
  char *atrFileName;
  nodeRec *arrayRecArray=NULL;
  nodeRec *nodeRecArray=NULL;
  int *experimentOrder;
  int i;
  int index=0;
  int experimentsWereClustered=0;
  
  experimentOrder=malloc(numExperiments*sizeof(int));
  if (experimentOrder==NULL) Error("Not enough memory available for experimentOrder");
  /* initialize experimentOrder to default values */
  for (i=0; i<numExperiments; i++){
    experimentOrder[i]=i;
  }
  
  MakeFileNames(ifile, &gtrFileName, &cdtFileName, &atrFileName);
  
  if (gShouldClusterGenes){					
  
  	nodeRecArray=malloc((cluster->numGenes-1)*sizeof(nodeRec));
  	if (nodeRecArray==NULL) Error("Not enough memory for the nodeRec array");
  	/*PrintData(dataMatrix, numExperiments, numLines, &cluster, eWeights, matrixWidth);*/
  
  	MakeCorrelations(cluster, dataMatrix, numExperiments, eWeights, numLines, 'G', matrixWidth);
  	/*PrintCorrelations(cluster);*/
  
  	ClusterNodes(cluster, dataMatrix, numExperiments, eWeights, nodeRecArray, gtrFileName, matrixWidth);
  	
  }
  
  if (gShouldClusterExperiments){
    for (i=0; i<numExperiments*2-1; i++){
      cluster->genes[i].joined=0;
    }
    MakeCorrelations(cluster, dataMatrix, numExperiments, eWeights, numExperiments, 'E', matrixWidth);
    printf("Clustering Experiments\n");
    fflush(stdout);
    arrayRecArray=malloc((numExperiments-1)*sizeof(nodeRec));
    if (arrayRecArray==NULL) Error("Not enough memory for the arrayRecArray");
    ClusterExperiments(cluster, dataMatrix, numExperiments, eWeights, arrayRecArray, atrFileName, matrixWidth);
    ChangeExperimentOrder(experimentOrder, &(arrayRecArray[numExperiments-2]), &index);
    printf("Done Clustering Experiments\n");
    fflush(stdout);
    experimentsWereClustered=1;
    FreeNodeRecArray(arrayRecArray, numExperiments);
  }
  
  
  PrintCDTFile(cluster, dataMatrix, numExperiments, nodeRecArray, experimentNames, experimentOrder, cdtFileName, matrixWidth, experimentsWereClustered);
  
  /* now clean up memory */
  free (cdtFileName);
  free (atrFileName);
  free (gtrFileName);
  free (experimentOrder);
  
  if (gShouldClusterGenes){	
  	FreeNodeRecArray(nodeRecArray, numLines);
  }
  
}


/* 
 * Function: MakeCorrelations
 * Usage: MakeCorrelations(&cluster, dataMatrix, numExperiments, eWeights, numGenes, 'G')
 * 	  MakeCorrelations(&cluster, dataMatrix, numExperiments, eWeights, numExperiments, 'E')
 * --------------------------------------------------
 * This function generates the correlation coefficients
 * by comparing each profile to every other profile.
 * It cuts the work in half by not comparing A vs B, and
 * B vs A, which should be identical.  It will save up to MAXBESTCORRELATIONS
 * of the top correlations.  This allows correlation scores to
 * be freed once the node has been joined.  The scores must be in
 * sorted order. There are pointers to both the top and bottom scores,
 * so that it can rapidly be decided whether a correlation is the highest,
 * or whether a newly calculated correlation should be included.
 * By passing in different "total", and "type", the function can be made to 
 * calculate correlations for experiments, or genes.
 */

void MakeCorrelations(clusterRec *cluster, float *dataMatrix, int numExperiments, float *eWeights, int total, char type, int matrixWidth){
  
  int counter;
  int comparedToCounter;
  float pearsonCorrelation;
  int numAfterWhichToPrint=400000/cluster->numGenes;
  
  printf("Making correlations\n");
  fflush(stdout);
  
  for (counter=0; counter<total-1; counter++){
    
    for (comparedToCounter=counter+1; comparedToCounter<total; comparedToCounter++){
      
      pearsonCorrelation=CalculateCorrelation(counter, comparedToCounter, dataMatrix, numExperiments, eWeights, type, cluster->numGenes, cluster, matrixWidth);
      
      /* Once we have the correlation coefficient we have to check
       * if it's greater than the last value in the linked list
       * for BOTH the geneCounter gene, and the comparedToCounter
       * gene.  This way every linked list will contain its top
       * five most similar correlations.  If you only checked the
       * geneCounter gene, then it is possible (it does happen, I  
       *  checked) that you would not be storing the top correlations 
       * for some genes anywhere.
       */
      
      CheckToInsert(cluster, counter, comparedToCounter, pearsonCorrelation);
      
    }
    
    if (!(counter%numAfterWhichToPrint)){
      printf ("%d\n", counter);
      fflush(stdout);
    }
  }
  printf("Done Making Correlations\n");
  fflush(stdout);
}

/*
 * Function: CalculateCorrelation
 * Usage: pearsonCorrelation=CalculateCorrelation(geneCounter, comparedToCounter, dataMatrix, numExperiments, eWeights, type, cluster->numGenes)
 * ------------------------------------------------------------------------------------------------------------------------
 * This function calculates the correlation between two expression profiles, whose identities are
 * indicated by geneCounter and comparedToCounter.  The algorithm calculates the pearson correlation
 * and only utilizes data where the two profiles have data (experiments) in common.  It is able
 * to keep running totals, so that it does not have to go over the datasets once again after it
 * has found which experiments are in common between two profiles.  This code is taken almost verbatim
 * from Mike Eisen's original correlation code.  One bug in that code has been fixed (the original
 * code wrongly used numExperiments, instead of Count, in the final calculation) and the data access looks
 * somewhat different to reflect the nature of the way that I have stored the data in what was a 
 * dynamically allocated array.  I have used pointers all the time to access the array data.  It looks
 * ugly and confusing, as I'm updating the pointers themselves, rather than a separate variable.  It is,
 * however, far quicker (~35%!).
 */
 
 float CalculateCorrelation(int geneCounter, int comparedToCounter, float *dataMatrix, int numExperiments, float *eWeights, char type, int numGenes, clusterRec *cluster, int matrixWidth){
   float Sum1, Sum2, Sum11, Sum22, Sum12, Ave1, Ave2, Count, Corr, norm, diff;
   float *genePtr, *cmpPtr, *colPtr;
   register float colWeight;
   register float geneVal;
   register float cmpVal;
   Sum1 = Sum2 = Sum11 = Sum22 = Sum12 = Count = Ave1 = Ave2 = 0;
   Corr=-1.0;	
   
   if(gUsePearson){
     if (type=='G'){ /* if we're correlating genes */
       for(genePtr=&dataMatrix[geneCounter*matrixWidth], cmpPtr=&dataMatrix[comparedToCounter*matrixWidth], colPtr=eWeights;
           genePtr<&dataMatrix[geneCounter*matrixWidth+numExperiments];
           genePtr++, cmpPtr++, colPtr++)
       {
	 
				 geneVal=*genePtr;
				 cmpVal=*cmpPtr;
				 if(geneVal!=NODATA && cmpVal!=NODATA){
				   colWeight=*colPtr;
				   Sum1+=colWeight * geneVal;
				   Sum2+=colWeight * cmpVal;
				   Sum11+=colWeight * geneVal * geneVal;
				   Sum22+=colWeight * cmpVal * cmpVal;
				   Sum12+=colWeight * geneVal * cmpVal;
				   Count+=colWeight;
				 }
       }
     } else { /* must be correlating experiments */
 	 	   /* NOTE: I wonder if this actually works?
          NOTE: colPtr was previously incremented as ((nameRec*)colPtr)++ but that doesn't work with gcc 4
          NOTE: so i made a new parameter that's should increment correctly, but I don't know if I'm dereferencing
          NOTE: it to a valid float. */
     	 nameRec *colPtr;
       for(genePtr=&dataMatrix[geneCounter], cmpPtr=&dataMatrix[comparedToCounter], colPtr=(nameRec*)&cluster->genes[0];
           genePtr<&dataMatrix[numGenes*matrixWidth+geneCounter];
           genePtr+=matrixWidth, cmpPtr+=matrixWidth, colPtr++)
       {
				 geneVal=*genePtr;
				 cmpVal=*cmpPtr;
				 if(geneVal!=NODATA && cmpVal!=NODATA){
				   colWeight=colPtr->rowWeight;
				   Sum1+=colWeight * geneVal;
				   Sum2+=colWeight * cmpVal;
				   Sum11+=colWeight * geneVal * geneVal;
				   Sum22+=colWeight * cmpVal * cmpVal;
				   Sum12+=colWeight * geneVal * cmpVal;
				   Count+=colWeight;
				 }
       }
     }
     
     if (Count){
       
       if((kgCentered && type=='G')||(keCentered && type =='E')){
	 Ave1 = Sum1/Count;
	 Ave2 = Sum2/Count;
	 norm=sqrt((Sum11 - 2 * Ave1 * Sum1 + Count * Ave1 * Ave1)*(Sum22 - 2 * Ave2 * Sum2 + Count * Ave2 * Ave2));
	 if (norm>0){
	   Corr = (Sum12 - Sum1 * Ave2 - Sum2 * Ave1 + Count * Ave1 * Ave2) /norm;
	 }
       }else{
	 norm=sqrt(Sum11*Sum22);
	 if (norm>0){
	   Corr=(Sum12/norm);
	 }
       }
     }else{
       Corr=0;
     }
   }else{ /* using Euclidean distance */
     if (type=='G'){ /* if we're correlating genes */
       for(genePtr=&dataMatrix[geneCounter*matrixWidth], cmpPtr=&dataMatrix[comparedToCounter*matrixWidth];
	   genePtr<&dataMatrix[geneCounter*matrixWidth+numExperiments];
	   genePtr++, cmpPtr++){
	 
	 if(*genePtr!=NODATA && *cmpPtr!=NODATA){
	   diff=(*genePtr)-(*cmpPtr);
	   Sum1 += diff * diff;
	   Count++;
	 }
       }
       
     } else { /* must be correlating experiments */
       for(genePtr=&dataMatrix[geneCounter], cmpPtr=&dataMatrix[comparedToCounter];
	   genePtr<&dataMatrix[numGenes*matrixWidth+geneCounter];
	   genePtr+=matrixWidth, cmpPtr+=matrixWidth){
	 
	 if(*genePtr!=NODATA && *cmpPtr!=NODATA){
	   diff=(*genePtr)-(*cmpPtr);
	   Sum1 += diff * diff;
	   Count++;
	 }
       }	
     }
     
     if (Sum1>0 && Count >0){
       Corr=1/(sqrt(Sum1)/Count);
     }else{
       Corr=0;
     }
   }
   
   return (Corr);
   
 }

/*
 * Function: CheckToInsert
 * Usage: CheckToInsert(cluster, geneCounter, comparedToCounter, pearsonCorrelation);
 * -------------------------------------------------------------------------------------
 * This function checks whether a correlation that was just made is
 * worthy of insertion into a list of correlations, based on it's value.
 * if so it makes a new record, which contains a number that indicates
 * the gene to which the geneCounter gene is correlated to, then calls
 * a function to insert it.  It will also check whether there are enough
 * correlations already in the list, and delete the lowest one if appropriate.
 */
 
void CheckToInsert(clusterRec *cluster, int geneCounter, int comparedToCounter, float pearsonCorrelation){
	
  correlationRec *newOne;
  
  if ((cluster->genes[geneCounter].last==NULL)||
      (pearsonCorrelation > cluster->genes[geneCounter].last->corr)){
    
    if (cluster->genes[geneCounter].numCorrelations==MAXBESTCORRELATIONS){
      
      cluster->genes[geneCounter].last->corr=pearsonCorrelation;
      cluster->genes[geneCounter].last->ORFnumber=comparedToCounter;
      
      cluster->genes[geneCounter].last=SwitchLast(&(cluster->genes[geneCounter].first), cluster->genes[geneCounter].last);
    }else{
      
      newOne=MakeNewRecord(pearsonCorrelation, comparedToCounter);
      
      InsertSorted(&(cluster->genes[geneCounter].first), newOne);
      
      if (newOne->next==NULL){
	cluster->genes[geneCounter].last=newOne;
      }
      
      ++cluster->genes[geneCounter].numCorrelations;
      
    }
  }
  
} 

/*
 * Function: SwitchLast
 */
 
correlationRec *SwitchLast(correlationRec **list, correlationRec* newOne){
  correlationRec *curr, *prev;
  prev=NULL;
  
  for (curr=*list; curr->next!=NULL; curr=curr->next){
    if (curr->corr<newOne->corr) break; /* We passed it, so now want to insert here */
    prev=curr;
  }
  
  /* now curr points to the entry to delete (we already actually knew this)
   * and prev points to the entry that will now be the end of the list
   */
  
  newOne->next=curr;
  
  if (prev!=NULL){
    prev->next=newOne;
  }else{
    *list=newOne;
  }
  
  for(;curr->next!=newOne;curr=curr->next){}
  
  curr->next=NULL;
  return curr;
  
}

/*
 * Function: MakeNewRecord
 * Usage: MakeNewRecord(pearsonCorrelation, comparedToCounter)
 * -----------------------------------------------------------
 * This function allocates space for a new correlationRec
 * and initializes its fields
 */
correlationRec *MakeNewRecord(double correlation, int geneNumber){

  correlationRec *newOne;
  newOne=malloc(sizeof(correlationRec));
  if (newOne==NULL) Error("Not enough memory available for a new correlationRec");
  newOne->corr=correlation;
  newOne->ORFnumber=geneNumber;
  newOne->next=NULL;
  return (newOne);
  
}
/*
 * Function: InsertSorted
 * Usage: InsertSorted(&(cluster->genes[geneCounter]), cp)
 * ----------------------------------------------------------
 * This function will insert a correlationRec in order (with 
 * respect to the value of the correlation it contains).  It 
 * handles the special case of their being no previous correlationRecs
 * in the list.  This is why the list has to be passed in as a double
 * pointer.
 */
 
void InsertSorted(correlationRec **list, correlationRec *newOne){
	
  correlationRec *prev, *curr;
  prev=NULL;
  for (curr=*list; curr!=NULL; curr=curr->next){
    if (curr->corr<newOne->corr) break; /* We passed it, so now want to insert here */
    prev=curr;
  }
  
  /* now, "prev" points to the one before where the newOne will be inserted,
     next "curr" points to the one after */
  
  newOne->next=curr;
  
  if (prev !=NULL){
    prev->next=newOne;
  }else{
    *list = newOne;
  }
  
}

/*
 * Function: DeleteLast
 * Usage: cluster->genes[geneCounter].last=DeleteLast(cluster->genes[geneCounter].first)
 * -------------------------------------------------------
 * This function is invoked when the last correlation in a list is to a gene
 * that has just been joined into a node.  Hence the correlationRec associated
 * with that correlation is deleted, and the one in the list previous to it
 * is returned as now being at the end of the list.
 */
 
correlationRec *DeleteLast(correlationRec **list){
  correlationRec *curr, *prev;
  prev=NULL;
  
  for (curr=*list; curr->next!=NULL; curr=curr->next){
    prev=curr;
  }
  
  /* now curr points to the entry to delete (we already actually knew this)
   * and prev points to the entry that will now be the end of the list
   */
  
  prev->next=NULL;
  free (curr);
  return prev;
  
}

	 

/*
 * Function: FreeCluster
 * Usage: FreeCluster(cluster)
 * ---------------------------------
 * This function frees the memory associated with a particular dataset,
 * including all ORF and genes names.
 */

void FreeCluster(clusterRec *cluster){
  int counter;
  
  for (counter=0; counter<cluster->numGenes; counter++){
    if (cluster->genes[counter].orf!=NULL) free(cluster->genes[counter].orf);
    if (cluster->genes[counter].name!=NULL) free(cluster->genes[counter].name);
  }
  free (cluster->genes);
  
}

/* Function: ClusterNodes
 * Usage: ClusterNodes(&cluster, dataMatrix, numExperiments, eWeights, nodeRecArray)
 * ----------------------------------------------------------------
 * This function takes the initial correlations and uses them to start
 * making nodes.  As each new node is made, a list of correlations associated
 * with it are generated.  Correlations are only generated against genes or nodes
 * that are 'still in play', ie that have not themselves been assimilated 
 * into larger nodes. In addition as each node or gene is assimilated into a 
 * larger node, the correlations that did exist for that node or gene are deleted from
 * every linked list of correlations.  This allows the program to track whether
 * the linked list of correlations for any node or gene ever reaches zero length.
 * If that case does arise, then correlations for that node or gene will be 
 * recalculated.
 */

void ClusterNodes(clusterRec *cluster, float *dataMatrix, int numExperiments, float *eWeights, nodeRec *nodeRecArray, char *gtrFileName, int matrixWidth){
  
  int nodeCounter; /* need to make numGenes-1 nodes in total */
  int node1, node2, count;
  FILE *gtrFile;
  float pearsonCorrelation;
  int column;
  int numAfterWhichToPrint=400000/cluster->numGenes;
  
  printf("Clustering genes\n");
  fflush(stdout);
  
  gtrFile=OpenOutFile(gtrFileName);
  
  for (nodeCounter=1; nodeCounter<cluster->numGenes; nodeCounter++){
    
    /* allocate space for both dataWeight and summedData arrays */
    
    nodeRecArray[nodeCounter-1].summedData=malloc(numExperiments*sizeof(float));
    nodeRecArray[nodeCounter-1].dataWeights=malloc(numExperiments*sizeof(float));
    if (nodeRecArray[nodeCounter-1].summedData==NULL) Error("Not Enough Memory for summedData");
    if (nodeRecArray[nodeCounter-1].dataWeights==NULL) Error("Not Enough Memory for dataWeights");
    
    /* initialize arrays to zeros */
    for (column=0; column<numExperiments; column++){
      
      nodeRecArray[nodeCounter-1].summedData[column]=0.0;
      nodeRecArray[nodeCounter-1].dataWeights[column]=0.0;
    }
    
    FindMaxCorrelation(cluster, &node1, &node2, nodeCounter, cluster->numGenes);
    
    if (!(nodeCounter%numAfterWhichToPrint)){
      printf("%d\n", nodeCounter);
      fflush(stdout);
    }
    JoinNodes(node1, node2, numExperiments, dataMatrix, cluster, nodeCounter, nodeRecArray, 
	      eWeights, matrixWidth, cluster->numGenes, 'G', numExperiments);
    
    /* mark the old nodes as joined, and free their associated memory */
    
    cluster->genes[node1].joined=1;
    cluster->genes[node2].joined=1;
    
    FreeCorrelations(&(cluster->genes[node1].first));
    FreeCorrelations(&(cluster->genes[node2].first));
    cluster->genes[node1].first=NULL;
    cluster->genes[node2].first=NULL;
    cluster->genes[node1].last=NULL;
    cluster->genes[node2].last=NULL;
    cluster->genes[node1].numCorrelations=0;
    cluster->genes[node2].numCorrelations=0;
    
    
    /* Now calculate correlations for the new node and add them in to the linked lists.
     * At the same time go through all linked lists and delete correlations to the nodes that have now
     * been joined.  Whilst going through, need to check whether any have no correlations left.
     */
    
    for (count=0; count<(cluster->numGenes+nodeCounter-1); count++){
      
      if (!(cluster->genes[count].joined)){
	
	pearsonCorrelation=CalculateCorrelation(count, cluster->numGenes+nodeCounter-1, dataMatrix, numExperiments, eWeights, 'G', cluster->numGenes, cluster, matrixWidth);
	CheckToInsert(cluster, count, (cluster->numGenes+nodeCounter-1), pearsonCorrelation);
	
	if (cluster->genes[count].last->ORFnumber==node1 && cluster->genes[count].first->ORFnumber!=node1){
	  cluster->genes[count].last=DeleteLast(&(cluster->genes[count].first));
	  cluster->genes[count].numCorrelations--;
	  /* need to put in code to check if more correlations need to be calculated? */
	}else{
	  DeleteOldNodesFromLists(cluster, &(cluster->genes[count].first), node1, &(cluster->genes[count].numCorrelations), nodeCounter, dataMatrix, numExperiments, eWeights, count, matrixWidth, 'G', cluster->numGenes);
	}
	
	if(cluster->genes[count].last->ORFnumber==node2 && cluster->genes[count].first->ORFnumber!=node2){
	  cluster->genes[count].last=DeleteLast(&(cluster->genes[count].first));
	  cluster->genes[count].numCorrelations--;
	}else{
	  DeleteOldNodesFromLists(cluster, &(cluster->genes[count].first), node2, &(cluster->genes[count].numCorrelations), nodeCounter, dataMatrix, numExperiments, eWeights, count, matrixWidth, 'G', cluster->numGenes);
	}
      }
    }
    
  }
  
  free(nodeRecArray[nodeCounter-2].summedData);
  free(nodeRecArray[nodeCounter-2].dataWeights);
  
  PrintTreeFile(nodeRecArray, gtrFile, 'G', cluster->numGenes-1);	
  
  fclose (gtrFile);
  
  printf("Done clustering genes\n");
  fflush(stdout);
  
  
}

/*
 * Function: FindMaxCorrelation
 * Usage: FindMaxCorrelation(cluster, &node1, &node2, nodeCounter)
 * -----------------------------------------------------
 * This function looks at the first entry in all of the linked
 * lists, and keeps track of the maximum, and the nodes/genes
 * that were responsible for generating that correlation.
 * It has no return value, because the calling function passed in
 * by reference two variables where the identity of those nodes
 * can be stored.
 */

void FindMaxCorrelation(clusterRec *cluster, int *node1, int *node2, int nodeCounter, int maxNumber){
  
  int count;
  double max=-2;
  
  for (count=0; count<maxNumber+nodeCounter-2; count++){
    
    if (!(cluster->genes[count].joined)){
      
      if (cluster->genes[count].first->corr>max){
	
	max=cluster->genes[count].first->corr;
	*node1=count;
	*node2=cluster->genes[count].first->ORFnumber;
	
      }
      
    }
  }
}

/* Function: JoinNodes
 * Usage: JoinNodes(node1, node2, numExperiments, dataMatrix, cluster, nodeCounter, nodeRecArray, eWeights)
 * -----------------------------------------------------------------------------------------------------------------
 * This function takes two nodes (or genes) and joins them.
 * In doing so it creates a new expression data series to correspond
 * to the joined node, which is calculated from the genes underlying
 * the node.  It also creates a new nodeRec, which contains pointers
 * to the underlying nodes to create the new node.  In addition,
 * when it joins two nodes, or a node and a gene, it calculates the
 * highest correlation that exists when making pairwise comparisons
 * of the outside members of the nodes, or the outside members of a
 * node and a single gene.  It uses this information to ensure that
 * the most highly correlated outside members of nodes are placed
 * adjacent to each other in the cluster as it is built.  The way
 * in which this is accomplished is to have a recursive algorithm
 * that can go reverse the order of all the leaves in a tree, by
 * switching every left and right pointer in each nodeRec.
 */

void JoinNodes(int node1, int node2, int numExperiments, float *dataMatrix, clusterRec *cluster, 
	       int nodeCounter, nodeRec *recArray, float *eWeights, 
	       int matrixWidth, int maxNumber, char type, int numDataPoints){
  
  int dataPoint;
  
  /* hook up nodes in a nodeRec */
  
  HookUp(node1, 'l', recArray, nodeCounter, maxNumber);
  HookUp(node2, 'r', recArray, nodeCounter, maxNumber);
  
  /* put in correlation */
  
  recArray[nodeCounter-1].joiningCorr=cluster->genes[node1].first->corr;
  
  /* check whether node(s) need to be reversed to bring together most similar correlations */
  
  if (node1>=maxNumber&&node2>=maxNumber){
    
    ReverseIfNecessary(&(recArray[nodeCounter-1]), BOTH, dataMatrix, numExperiments, eWeights, cluster, matrixWidth, type);
    
  }else if (node1>maxNumber){
    
    ReverseIfNecessary(&(recArray[nodeCounter-1]), LEFT, dataMatrix, numExperiments, eWeights, cluster, matrixWidth, type);
    
  }else if (node2>maxNumber){
    
    ReverseIfNecessary(&(recArray[nodeCounter-1]), RIGHT, dataMatrix, numExperiments, eWeights, cluster, matrixWidth, type);
    
  }
  
  /* now add data from each node to be joined to the arrays */
  DoNode(eWeights, cluster, node1, dataMatrix, recArray, nodeCounter, matrixWidth, maxNumber, numDataPoints, type);
  DoNode(eWeights, cluster, node2, dataMatrix, recArray, nodeCounter, matrixWidth, maxNumber, numDataPoints, type);
  
  /* and then calculate the profile for the new node	*/
  for (dataPoint=0; dataPoint<numDataPoints; dataPoint++){
    if (recArray[nodeCounter-1].dataWeights[dataPoint]){
      switch (type){
      case 'G':
	dataMatrix[(maxNumber+nodeCounter-1)*matrixWidth+dataPoint]=recArray[nodeCounter-1].summedData[dataPoint]/recArray[nodeCounter-1].dataWeights[dataPoint];
	break;
      case 'E':
	dataMatrix[dataPoint*matrixWidth+nodeCounter-1+maxNumber]=recArray[nodeCounter-1].summedData[dataPoint]/recArray[nodeCounter-1].dataWeights[dataPoint];
	break;
      }
    }else{
      switch (type){
      case 'G':
	dataMatrix[(maxNumber+nodeCounter-1)*matrixWidth+dataPoint]=NODATA;
	break;
      case 'E':
	dataMatrix[dataPoint*matrixWidth+nodeCounter-1+maxNumber]=NODATA;
	break;
      }
    }
  }
  
}

/*
 * Function: DoNode
 * Usage: DoNode(node1, numExperiments, *dataMatrix, *cluster,  summedData, dataWeights, nodeRecArray)
 * -------------------------------------------------------------------
 * This function iterates over all the genes that constitue a node, and uses
 * the data that are pertinent to those genes to build a summed profile for the 
 * those genes.  This is a slightly wasteful way to calculate compound nodes,
 * and it could be done more quickly, simply using the expression profile for the
 * node that has already been generated.  However I am initially opting for this
 * somewhat overkill method to prevent overweighting of some datapoints, which occurs
 * when a compound node contains datapoints that were calculated from less datapoints than
 * there are genes contributing to the whole node.  If the time hit is large, then
 * I can change to do it the quicker, though less accurate way.
 */
 
void DoNode(float *eWeights, clusterRec *cluster, int node, float *dataMatrix, nodeRec *nodeRecArray, int nodeCounter, int matrixWidth, int maxNumber, int numDataPoints, char type){
  
  if (node < maxNumber){ /* if the node is a single gene/experiment */
    
    AddInSingleData(eWeights, cluster, node, dataMatrix, nodeRecArray, nodeCounter, matrixWidth, numDataPoints, type);
    
  }else{ /* otherwise it's a compound node */
    
    AddInNodeData(node, nodeRecArray, nodeCounter, maxNumber, numDataPoints);
  }
  
}

/*
 * Function: AddInSingleData
 * Usage: AddInSingleData(node, numExperiments, dataMatrix, summedData, dataWeights)
 * -----------------------------------------------------------------------
 * This simple function processes the data for a single gene/experiment by adding it
 * to the values already stored in the summedData array, and incrementing
 * the dataWeights array values accordingly.
 */
		
void AddInSingleData(float *eWeights, clusterRec *cluster, int node, float *dataMatrix, nodeRec *recArray, int nodeCounter, int matrixWidth, int numDataPoints, char type){
  
  int dataPoint;
  
  for (dataPoint=0; dataPoint<numDataPoints; dataPoint++){
    
    switch (type){
      
    case 'G':
      
      if (dataMatrix[node*matrixWidth+dataPoint] !=NODATA){
	
	recArray[nodeCounter-1].summedData[dataPoint]+= dataMatrix[node*matrixWidth+dataPoint];
	recArray[nodeCounter-1].dataWeights[dataPoint]+=cluster->genes[node].rowWeight;
	
      }
      break;
      
    case 'E':
      
      if (dataMatrix[dataPoint*matrixWidth+node]!=NODATA){
	
	recArray[nodeCounter-1].summedData[dataPoint]+=dataMatrix[dataPoint*matrixWidth+node];
	recArray[nodeCounter-1].dataWeights[dataPoint]+=eWeights[node];
					
      }
      break;			 
    }	
    
  }
}

/*
 * Function: AddInNodeData
 * Usage: AddInNodeData(node, numExperiments, dataMatrix, summedData, dataWeights, nodeRecArray, nodeCounter);
 * ---------------------------------------------------------------------------------------------------
 * This function adds the data from a previous node into the relevant arrays for a new node.
 * Once the data has been added in, the arrays of data in the node can then be disposed
 * of, with the relevant calls to free.
 */

void AddInNodeData(int node, nodeRec *nodeRecArray, int nodeCounter, int maxNumber, int numDataPoints){
  
  int dataPoint;
  
  for (dataPoint=0; dataPoint<numDataPoints; dataPoint++){
    
    if ((nodeRecArray[node-maxNumber].summedData[dataPoint])!=NODATA){
      
      nodeRecArray[nodeCounter-1].summedData[dataPoint] += nodeRecArray[node-maxNumber].summedData[dataPoint];
      nodeRecArray[nodeCounter-1].dataWeights[dataPoint] += nodeRecArray[node-maxNumber].dataWeights[dataPoint];
    }
  }
  
  free(nodeRecArray[node-maxNumber].summedData);
  free(nodeRecArray[node-maxNumber].dataWeights);
  
}

/*
 * Function: IsLeaf
 * Usage: if(IsLeaf(node))
 * -----------------------
 * This simple helper function simply checks whether the node passed in is 
 * a leaf of the tree by checking to see if both its children are NULL
 */
 
int IsLeaf(nodeRec *theNode){
  
  return ((theNode->left==NULL)&&(theNode->right==NULL));
  
}


/* Function: HookUp
 * Usage: HookUp(node1, 'l', cluster, nodeRecArray, nodeCounter)
 * --------------------
 * This functionchecks to see if the node that is passed in represents
 * a single gene, and if so creates and a new nodeRec, and hooks it 
 * up to the correct nodeRec in the nodeRecArray.  If the node passed in
 * is a compound node, then the function hooks the compound node into the node.
 * The function hooks up a node1 to the left pointer, and a node2 to a right pointer.
 */

void HookUp(int node, char side, nodeRec *recArray, int nodeCounter, int maxNumber){
  
  if (node<maxNumber){
    
    switch (side){
    case 'l':	
      recArray[nodeCounter-1].left=malloc(sizeof(nodeRec));
      if (recArray[nodeCounter-1].left==NULL) Error("Not Enough Memory for nodeRecArray");
      recArray[nodeCounter-1].left->joiningCorr=node;
      recArray[nodeCounter-1].left->left=NULL;
      recArray[nodeCounter-1].left->right=NULL;
      break;
    case 'r':
      recArray[nodeCounter-1].right=malloc(sizeof(nodeRec));
      if (recArray[nodeCounter-1].right==NULL) Error("Not Enough Memory for nodeRecArray");
      recArray[nodeCounter-1].right->joiningCorr=node;
      recArray[nodeCounter-1].right->left=NULL;
      recArray[nodeCounter-1].right->right=NULL;
      break;
    }
    
  }else{
    
    switch (side){
    case 'l':	
      recArray[nodeCounter-1].left=&(recArray[node-maxNumber]);
      break;
    case 'r':
      recArray[nodeCounter-1].right=&(recArray[node-maxNumber]);
      break;
    }
  }
  
}

/*
 * Function: ReverseIfNecessary
 * Usage: ReverseIfNecessary(&(nodeRecArray[nodeCounter-1]), BOTH, dataMatrix, numExperiments, eWeights, cluster)
 * ----------------------------------------
 * This function examines the structure of the node, at least
 * one branch of which is a compound node, and calculate the up
 * to four pairwise correlations that could exists between the outside 
 * members of the nodes children.  Upon determining which of the
 * correlations is highest it reverses the nodes as appropriate, 
 * such that the highest correlated pair will be adjacent in the cluster.
 * The variable "number" indicates which of the nodes are compound.
 */

void ReverseIfNecessary(nodeRec *node, int number, float *dataMatrix, int numExperiments, float *eWeights, clusterRec *cluster, int matrixWidth, char type){

  int left1,right1,left2,right2,onlyLeft,onlyRight, count;
  double corr[4];
  int max=0;
  
  switch (number) {
    
  case BOTH:
    left1=GetLeftMost(node->left);
    right1=GetRightMost(node->left);
    left2=GetLeftMost(node->right);
    right2=GetRightMost(node->right);
    corr[0]=CalculateCorrelation(left1, left2, dataMatrix, numExperiments, eWeights, type, cluster->numGenes, cluster, matrixWidth);
    corr[1]=CalculateCorrelation(left1, right2, dataMatrix, numExperiments, eWeights, type, cluster->numGenes, cluster, matrixWidth);
    corr[2]=CalculateCorrelation(right1, left2, dataMatrix, numExperiments, eWeights, type, cluster->numGenes, cluster, matrixWidth);
    corr[3]=CalculateCorrelation(right1, right2, dataMatrix, numExperiments, eWeights, type, cluster->numGenes, cluster, matrixWidth);
    for (count=1; count<4; count++){
      if (corr[count]>corr[max]){
	max=count;
      }
    }
    switch (max){
    case 0:
      Reverse(node->left);
      break;
    case 1: /* if the two outside nodes are most similar, simply switch at the top most level */
      SwitchLeftAndRight(node);
      break;
    case 2:
      break;
    case 3:
      Reverse(node->right);
      break;
    }
    
    break;
  case LEFT:
    left1=GetLeftMost(node->left);
    right1=GetRightMost(node->left);
    onlyRight=(int)node->right->joiningCorr;
    corr[0]=CalculateCorrelation(left1, onlyRight, dataMatrix, numExperiments, eWeights, type, cluster->numGenes, cluster, matrixWidth);
    corr[1]=CalculateCorrelation(right1, onlyRight, dataMatrix, numExperiments, eWeights, type, cluster->numGenes, cluster, matrixWidth);
    if (corr[0]>corr[1]){
      SwitchLeftAndRight(node);
    }
    
  case RIGHT:
    onlyLeft=(int)node->left->joiningCorr;
    left2=GetLeftMost(node->right);
    right2=GetRightMost(node->right);
    corr[0]=CalculateCorrelation(onlyLeft, left2, dataMatrix, numExperiments, eWeights, type, cluster->numGenes, cluster, matrixWidth);
    corr[1]=CalculateCorrelation(onlyLeft, right2, dataMatrix, numExperiments, eWeights, type, cluster->numGenes, cluster, matrixWidth);
    if (corr[1]>corr[0]){
      SwitchLeftAndRight(node);
    }
    
  }
  
}

/*
 * Function: Reverse
 * Usage: Reverse(node->left)
 * --------------------------
 * This function will recursively traverse a tree to switch all of the
 * left and right pointers in the tree, thus reversing the order of the 
 * leaves on the tree.
 */
 
void Reverse (nodeRec* node){
	
  if (node!=NULL){
    Reverse(node->left);
    Reverse(node->right);
    SwitchLeftAndRight(node);
  }
} 

/*
 * Function: SwitchLeftAndRight
 * Usage: SwitchLeftAndRight(node)
 * ------------------------------
 * This simply switches the left and right pointers of a single node
 */
 
void SwitchLeftAndRight(nodeRec *node){
  nodeRec* temp;
  
  temp=node->right;
  node->right=node->left;
  node->left=temp;
  
}

/*
 * Function: GetLeftMost
 * Usage: left1=GetLeftMost(node->left)
 * ------------------------------
 * This function returns the number contained in the joiningCorr
 * field of the left most node.  This number corresponds to the 
 * gene represented by that node.
 */
 
int GetLeftMost(nodeRec *node){
  
  
  while (node->left!=NULL){
    node=node->left;
    
  }
  
  return ((int)node->joiningCorr);
  
}

/*
 * Function: GetRightMost
 * Usage: right1=GetRightMost(node->left)
 * ------------------------------
 * This function returns the number contained in the joiningCorr
 * field of the left most node.  This number corresponds to the 
 * gene represented by that node.
 */
 
int GetRightMost(nodeRec *node){
  
  while (node->right!=NULL){
    node=node->right;
  }
  
  return ((int)node->joiningCorr);
  
}

/*
 * Function : PrintTreeFile
 * Usage : PrintTreeFile(nodeRecArray, gtrFile, 'G', cluster->numGenes-1)
 * ----------------------------------------------------------------------
 * This function prints out the treefile for either the genes or 
 * experiments, by goinf through the relevant nodeRecArray, and
 * determining in what order what was joind, and with what correlation
 * This routine fixes a bug, that meant that the tree files didn't
 * correctly reflect the reordering of the nodes that had taken place.
 */
 
void PrintTreeFile(nodeRec *arrayRecArray, FILE *outFile, char type, int numNodes){

  int nodeCounter;
  int num;
  int node;
  float max;
  
  max=arrayRecArray[0].joiningCorr;
  
  for (nodeCounter=1; nodeCounter<=numNodes; nodeCounter++){
    
    fprintf(outFile, "NODE%dX\t", nodeCounter);
    
    if (!IsLeaf(arrayRecArray[nodeCounter-1].left)){
      node=arrayRecArray[nodeCounter-1].left-arrayRecArray+1;
      fprintf(outFile, "NODE%dX\t", node);
    }else{
      num=arrayRecArray[nodeCounter-1].left->joiningCorr;
      if (type=='G'){
	fprintf(outFile, "GENE%dX\t", num);
      }else{
	fprintf(outFile, "ARRY%dX\t", num);
      }
    }
    
    if (!IsLeaf(arrayRecArray[nodeCounter-1].right)){
      node=arrayRecArray[nodeCounter-1].right-arrayRecArray+1;
      fprintf(outFile, "NODE%dX\t", node);
    }else{
      num=arrayRecArray[nodeCounter-1].right->joiningCorr;
      if (type=='G'){
	fprintf(outFile, "GENE%dX\t", num);
      }else{
	fprintf(outFile, "ARRY%dX\t", num);
      }
    }
    
    if (gUsePearson){
      
      fprintf(outFile, "%f\n", arrayRecArray[nodeCounter-1].joiningCorr);
      
    }else{
      
      fprintf(outFile, "%f\n", ((2*arrayRecArray[nodeCounter-1].joiningCorr)/max)-1);
      
    }
    
  }
  
}


/*
 * Function: FreeCorrelations
 * Usage: FreeCorrelations(&(cluster->genes[node1].first))
 * ----------------------------------------------------------
 * This function walks through a linked list freeing the memory
 * associated with each stored correlation.
 */
 
void FreeCorrelations(correlationRec **node){

  correlationRec *curr, *next;
  
  for (curr=*node; curr!=NULL;){
    next=curr->next;
    free(curr);
    curr=next;
  }
}


/* Function: DeleteOldNodesFromLists
 * Usage: DeleteOldNodesFromLists(cluster, &(cluster->genes[count].first), node1, &(cluster->genes[count].numCorrelations))
 * ------------------------------------------------------------------------
 * This function takes a pointer to a linked list of correlations, and checks
 * that list to see if there are any correlations to the nodes that have just 
 * been joined.  If so it deletes them.  Because it is possible that the first
 * node in a list is to be deleted, the list is passed in as a double pointer.
 */
 
void DeleteOldNodesFromLists(clusterRec* cluster, correlationRec **node, int gene, int *numCorrelations, int nodeCounter, float *dataMatrix, int numExperiments, float *eWeights, int current, int matrixWidth, char type, int maxNumber){
  
  correlationRec* curr, *prev;
  double pearsonCorrelation;
  int count;
  int found=0;
  prev=NULL;
  
  
  for (curr=*node; curr!=NULL; curr=curr->next){
    if (curr->ORFnumber==gene){ /* We found it, so now want to delete here */
      found=1;
      break; 
    }
    prev=curr;
  }
  
  if (found){
    if (prev !=NULL){
      prev->next=curr->next;
    }else{
      *node = curr->next;
    }
    
    free(curr);
    
    if(--(*numCorrelations)==0){ /* if we've run out of correlations for a gene */
      
      cluster->genes[current].last=NULL;
      
      for (count=0; count<(maxNumber+nodeCounter); count++){
	
	if (!(cluster->genes[count].joined) && (count!=current)){
	  
	  pearsonCorrelation=CalculateCorrelation(count, current, dataMatrix, numExperiments, eWeights, type, cluster->numGenes, cluster, matrixWidth);
	  
	  CheckToInsert(cluster, current, count, pearsonCorrelation);
	  
	}
      }			
      
    }
    
  }
		
}


/*
 * Function: PrintCDTFile
 * Usage: PrintCDTFile(&cluster, dataMatrix, numExperiments, nodeRecArray, experimentNames, experimentOrder)
 * -----------------------------------------------------------------------------
 * This simple function opens ahandle to the outfile, and then calls the print leaves
 * function to print out all the necessary data.  The order in which a gene's data is 
 * printed is determined by the array experiment order.  The defaults values for
 * experimentOrder dictate hat the data is printed in the same order in which it was read,
 * however this order can be changed when experiments are clustered.
 */

void PrintCDTFile(clusterRec *cluster, float *dataMatrix, int numExperiments, nodeRec *nodeRecArray, char **experimentNames,
		  int *experimentOrder, char *cdtFileName, int matrixWidth, int experimentsWereClustered){
  
  FILE *outfile;
  int i;
  
  printf("Outputting cdt file\n");
  fflush(stdout);
  
  outfile=OpenOutFile(cdtFileName);
  
  fprintf(outfile, "GID\tYORF\tNAME\tGWEIGHT");
  
  for (i=0; i<numExperiments; i++){
    
    fprintf(outfile, "\t%s", experimentNames[experimentOrder[i]]);
    
  }
  
  fprintf(outfile, "\n");
  
  if (experimentsWereClustered){
    
    fprintf(outfile, "AID\t\t\t");
    
    for (i=0; i<numExperiments; i++){
      
      fprintf(outfile, "\tARRY%dX", experimentOrder[i]);
      
    }
    
    fprintf(outfile, "\n");
    
  }
  
  if (gShouldClusterGenes){
  
  	PrintLeaves(&(nodeRecArray[cluster->numGenes-2]), numExperiments, dataMatrix, cluster, outfile,  experimentOrder, matrixWidth);
  	
  }else{
  
  	for (i=0; i<cluster->numGenes; i++){
  	
  		PrintLeaf(i, numExperiments, dataMatrix, cluster, outfile, experimentOrder, matrixWidth);
  		
  	}
  	
  }
  	
  		
  
  fclose(outfile);
  
  printf("Done outputting\n");
  fflush(stdout);
  
}

/*
 * Function: PrintLeaves
 * Usage: PrintLeaves(&(nodeRecArray[cluster->numGenes-2]), numExperiments, dataMatrix, cluster, outfile,  experimentOrder)
 * ----------------------------------------------------------------------------------------------------
 * This function recursively steps through every node of the tree.
 * When it finds a leaf it calls the Print leaf function, to output the
 * relevant data to the .cdt file.
 */

void PrintLeaves(nodeRec *tree, int numExperiments, float *dataMatrix, clusterRec *cluster, FILE *outfile, int * experimentOrder, int matrixWidth){

  if(IsLeaf(tree)){
    PrintLeaf(tree->joiningCorr, numExperiments, dataMatrix, cluster, outfile, experimentOrder, matrixWidth);
  }else{
    PrintLeaves(tree->right, numExperiments, dataMatrix, cluster, outfile,  experimentOrder, matrixWidth);
    PrintLeaves(tree->left, numExperiments, dataMatrix, cluster, outfile,  experimentOrder, matrixWidth);
  }
  
}

/*
 * Function: PrintLeaf
 * Usage: PrintLeaf(tree->joiningCorr, numExperiments, dataMatrix, cluster, outfile,  experimentOrder)
 * ----------------------------------------------------------------------------
 * This function prints out the gene number, the orf and SGD name, the row weight
 * (at the moment it just uses 1 as the row weight), and then the experimental
 * data for a single gene, at a leaf in the tree.
 */

void PrintLeaf(int geneNumber, int numExperiments, float *dataMatrix, clusterRec *cluster, FILE *outfile, int *experimentOrder, int matrixWidth){

  int column;
  
  if (cluster->genes[geneNumber].name!=NULL){
    
    fprintf(outfile, "GENE%dX\t%s\t%s\t1", geneNumber, cluster->genes[geneNumber].orf, cluster->genes[geneNumber].name);
    
  }else{
    
    fprintf(outfile, "GENE%dX\t%s\t\t1", geneNumber, cluster->genes[geneNumber].orf);
    
  }
  
  
  for (column=0; column<numExperiments; column++){
    
    if (dataMatrix[geneNumber*matrixWidth+experimentOrder[column]]!=NODATA){
      fprintf(outfile, "\t%.2f", dataMatrix[geneNumber*matrixWidth+experimentOrder[column]]);
    }else{
      fprintf(outfile, "\t");
    }
  }
  
  fprintf(outfile, "\n");

}

/*
 * Function: Error
 * Usage: Error(msg, ...)
 * ----------------------
 * This function generates an error string, expanding % constructions
 * appearing in the error message string just as printf does.
 * After printing the error message, the program terminates,
 * This code was taken from Eric Roberts' "The Art and Science of C".
 */

void Error(char *msg, ...){

  va_list args;
  
  va_start(args, msg);
  fprintf(stderr, "Error: ");
  vfprintf(stderr, msg, args);
  fprintf(stderr, "\n");
  va_end(args);
  exit(1);
  
}

/*
 * Function: FreeNodeRecArray
 * Usage: FreeNodeRecArray(nodeRecArray, numLines);
 * ------------------------------------------------
 * This function frees all the leaves attached to the nodeRecArray,
 * and then finally frees the array itself
 */

void FreeNodeRecArray(nodeRec *nodeRecArray, int numLines){
  
  int count;
  
  for (count=0; count<numLines-1; count++){
    
    if (IsLeaf(nodeRecArray[count].left)) free(nodeRecArray[count].left);
    if (IsLeaf(nodeRecArray[count].right)) free(nodeRecArray[count].right);
  }
  
  free(nodeRecArray);
  
}

/*
 * Function: ClusterExperiments
 * Usage:  ClusterExperiments(cluster, dataMatrix, numExperiments, eWeights, arrayRecArray, atrFileName, matrixWidth)
 * ----------------------------------------------
 * This function clusters the columns, or experiments, and in the process of doing
 * so, outputs an .atr file, to generate the tree to go above the expression profiles,
 * and also overwrites an array with the new order in which the experiments should be 
 * printed.  This array was initially set-up so that if experiments are not be be 
 * clustered, then data is printed in the order in which it was read.
 */
 
void ClusterExperiments(clusterRec *cluster, float *dataMatrix, int numExperiments, float *eWeights, 
			nodeRec *arrayRecArray, char *atrFileName, int matrixWidth){
  int nodeCounter; /* need to make numExperiments-1 nodes in total */
  int node1, node2, count;
  FILE *atrFile;
  float pearsonCorrelation;
  int row;
  
  atrFile=OpenOutFile(atrFileName);
  
  for (nodeCounter=1; nodeCounter<numExperiments; nodeCounter++){
    
    /* allocate space for both dataWeight and summedData arrays */
    
    arrayRecArray[nodeCounter-1].summedData=malloc(cluster->numGenes*sizeof(float));
    arrayRecArray[nodeCounter-1].dataWeights=malloc(cluster->numGenes*sizeof(float));
    if (arrayRecArray[nodeCounter-1].summedData==NULL) Error("Not Enough Memory for summedData");
    if (arrayRecArray[nodeCounter-1].dataWeights==NULL) Error("Not Enough Memory for dataWeights");
    
    /* initialize arrays to zeros */
    for (row=0; row<cluster->numGenes; row++){
      
      arrayRecArray[nodeCounter-1].summedData[row]=0.0;
      arrayRecArray[nodeCounter-1].dataWeights[row]=0.0;
    }
    
    FindMaxCorrelation(cluster, &node1, &node2, nodeCounter, numExperiments);
    
    if (!(nodeCounter%10)) printf("%d\n", nodeCounter);
    
    JoinNodes(node1, node2, numExperiments, dataMatrix, cluster, nodeCounter, arrayRecArray, 
	      eWeights, matrixWidth, numExperiments, 'E', cluster->numGenes);
    
    /* mark the old nodes as joined, and free their associated memory */
    
    cluster->genes[node1].joined=1;
    cluster->genes[node2].joined=1;
    
    FreeCorrelations(&(cluster->genes[node1].first));
    FreeCorrelations(&(cluster->genes[node2].first));
    cluster->genes[node1].first=NULL;
    cluster->genes[node2].first=NULL;
    cluster->genes[node1].last=NULL;
    cluster->genes[node2].last=NULL;
    cluster->genes[node1].numCorrelations=0;
    cluster->genes[node2].numCorrelations=0;
    
    /* Now calculate correlations for the new node and add them in to the linked lists.
     * At the same time go through all linked lists and delete correlations to the nodes that have now
     * been joined.  Whilst going through, need to check whether any have no correlations left.
     */
    
    for (count=0; count<(numExperiments+nodeCounter-1); count++){
      
      if (!(cluster->genes[count].joined)){
	
	pearsonCorrelation=CalculateCorrelation(count, numExperiments+nodeCounter-1, dataMatrix, numExperiments, eWeights, 'E', cluster->numGenes, cluster, matrixWidth);
	CheckToInsert(cluster, count, (numExperiments+nodeCounter-1), pearsonCorrelation);
	
	if (cluster->genes[count].last->ORFnumber==node1 && cluster->genes[count].first->ORFnumber!=node1){
	  cluster->genes[count].last=DeleteLast(&(cluster->genes[count].first));
	  cluster->genes[count].numCorrelations--;
	  /* need to put in code to check if more correlations need to be calculated? */
	}else{
	  DeleteOldNodesFromLists(cluster, &(cluster->genes[count].first), node1, &(cluster->genes[count].numCorrelations), nodeCounter, dataMatrix, numExperiments, eWeights, count, matrixWidth, 'E', numExperiments);
	}
	
	if(cluster->genes[count].last->ORFnumber==node2 && cluster->genes[count].first->ORFnumber!=node2){
	  cluster->genes[count].last=DeleteLast(&(cluster->genes[count].first));
	  cluster->genes[count].numCorrelations--;
	}else{
	  DeleteOldNodesFromLists(cluster, &(cluster->genes[count].first), node2, &(cluster->genes[count].numCorrelations), nodeCounter, dataMatrix, numExperiments, eWeights, count, matrixWidth, 'E', numExperiments);
	}
      }
    }
    
    
  }
  
  free(arrayRecArray[nodeCounter-2].summedData);
  free(arrayRecArray[nodeCounter-2].dataWeights);
  
  PrintTreeFile(arrayRecArray, atrFile, 'E', numExperiments-1);
  
  fclose(atrFile);
  
}

/* Function: FreeExperimentNames
 * Usage: FreeExperimentNames(experimentNames, numExperiments)
 * --------------------------------------------------------------
 * This function frees the storage associated with keeping track of the experiment names
 */
 
void FreeExperimentNames(char **experimentNames, int numExperiments){
  int i;
  
  for (i=0; i<numExperiments; i++){
    free (experimentNames[i]);
  }
  
  free(experimentNames);
  
}

/* Function: ChangeExperimentOrder
 * Usage: ChangeExperimentOrder(experimentOrder, &(arrayRecArray[numExperiments-2]), &index)
 * --------------------------------------------------------------------
 * This function changes the order in which the experimental data will 
 * be printed to the .cdt file, by iterating over the arrayRecArray
 * and putting the order in which the experiments have been clustered
 * into the experimentOrder array
 */
 
void ChangeExperimentOrder(int *experimentOrder, nodeRec *tree, int *index){

  if(IsLeaf(tree)){
    experimentOrder[(*index)++]=(int)tree->joiningCorr;
  }else{
    ChangeExperimentOrder(experimentOrder, tree->left, index);
    ChangeExperimentOrder(experimentOrder, tree->right, index);
  }
  
}

/* Debugging Functions */

/* 
 * Function: PrintData
 * Usage: PrintData (dataMatrix, numExperiments, numLines, cluster, eWeights, matrixWidth)
 * -----------------------------------------------
 * This function is for debugging purposes only, to check that the
 * data has been read into the array correctly.
 */

void PrintData(float *dataMatrix, int numExperiments, int numLines, clusterRec *cluster, float *eWeights, int matrixWidth){
  int i,j;
  printf("\t\t");
  for (i=0; i<numExperiments; i++){
    printf("%f\t", eWeights[i]);
  }
  printf("\n");
  for (i=0; i<numLines; i++){
    printf("%d\t%s\t%s\t%f\t", i, cluster->genes[i].orf, cluster->genes[i].name, cluster->genes[i].rowWeight);
    for (j=0; j<numExperiments; j++){
      printf("%f\t", dataMatrix[i*matrixWidth+j]);
    }
    printf("\n");
  }
}

/*
 * Function: PrintCorrelations
 * Usage: PrintCorrelations(&cluster)
 * -------------------------------------
 * This function prints out the top MAXBESTCORRELATIONS correlations for each gene
 * to a file, with a format such that the gene which is being 
 * correlated is at the front of the line, and the correlations, 
 * and the genes to which they correspond are tab delimited on the 
 * same line.
 */
 
void PrintCorrelations(clusterRec *cluster){

  int counter;
  FILE *outfile;
  
  printf ("Printing Correlations\n");
  outfile=OpenOutFile("sortedCorrelations.txt");
  
  for (counter=0; counter<cluster->numGenes; counter++){
    fprintf(outfile, "%s", cluster->genes[counter].orf);
    PrintOneGene(cluster->genes[counter].first, outfile, cluster);
    
  }
  
  fclose(outfile);
  printf("Done Printing Correlations");
  
}

/*
 * Function: PrintOneGene
 * Usage: PrintOneGene(cluster->genes[counter].first, outfile)
 * --------------------------------------------------------------
 * This function will print out the correlations to a particular gene,
 * The output is tab delimited, and consists of both the gene name,
 * and the correlation value itself.
 */
 
void PrintOneGene(correlationRec *list, FILE *outfile, clusterRec *cluster){

  correlationRec *curr;
  
  for (curr=list; curr!=NULL; curr=curr->next){
    
    if (curr->corr>=0.5){
      
      fprintf(outfile, "\t%s\t%f", cluster->genes[(curr->ORFnumber)].orf, curr->corr);
      
    }
    
  }
  
  fprintf(outfile, "\n");
  
}

/*
 * Function: PrintOrder
 * Usage: PrintOrder(node->left)
 * -----------------------------
 * This function will print out the order of genes within a node.
 * It is used to debug the Reverse function, so I can see the order
 * of genes before and after switching.
 */
 
void PrintOrder(nodeRec* node){

  if(IsLeaf(node)){
    printf("%.0f\n", node->joiningCorr);
  }else{
    PrintOrder(node->left);
    PrintOrder(node->right);
  }
  
}

/*************************************************************************************************************
 *
 * SOM functions from here on down
 *
 *************************************************************************************************************/

/*
 * Function : MakeSOM
 * Usage : dataMatrix=MakeSOM(dataMatrix, &cluster, numExperiments, eWeights, matrixWidth, experimentNames, ifile, x, y)
 * ----------------------------------------------------------------------------------------------------------------------
 * This Functions calls all the routines necessary
 * to make a SOM.  It returns dataMatrix, otherwise the function would leak memory, as dataMatrix is freed in main,
 * but during the course of this function, dataMatrix may be reallocated.  MakeSOM first design the SOM, by initializing
 * its dimensions, then filters the data for expression profiles that don't vary much (2 fold).  It then generates
 * random SOM nodes, which it then organizes by goping through a randomly ordered list of the genes.
 * The function finally sorts the SOM, by seeing to which node a gene is most similar.  The contents of each 
 * node are then sent to be hierachically clustered.  Files are also printed to indicate the evolution of each
 * SOM node, and to also indicate the correlations between a gene and it's most similar node.
 */

float *MakeSOM(float *dataMatrix, clusterRec *cluster, int numExperiments, float *eWeights, int matrixWidth, char **experimentNames, char *ifile, int x, int y){
  
  SOM *SOMdesign;
  int numNodes;
  int *bestHits;
  int changedUsePearson=0;
  
  if(!gUsePearson){
    gUsePearson=1;  /* we want to use pearson for the organizing of the SOM */
    changedUsePearson=1;
  }
  
  SOMdesign=designSOM(x, y);
  
  numNodes=SOMdesign->x * SOMdesign->y;
  
  printf("Initializing SOM\n");
  
  dataMatrix=FilterData(dataMatrix, cluster, numExperiments, numNodes, ifile);
  
  InitializeSOM(dataMatrix, cluster, SOMdesign, numExperiments);
  
  printf("Organizing SOM\n");
  
  OrganizeSOM(cluster, SOMdesign, dataMatrix, eWeights, numExperiments, matrixWidth, ifile);
  
  printf("Sorting SOM\n");
  
  bestHits=SortSOMData(numExperiments, dataMatrix, cluster->numGenes, numNodes, eWeights, cluster, matrixWidth, ifile, SOMdesign);
  
  printf("Printing SOM\n");
  
  if(changedUsePearson){ /* if they want to use Euclidean, we'll use it for clustering within the SOMs */
    gUsePearson=0;
  }
  
  PrintSOMOrganization(numNodes, bestHits, numExperiments, experimentNames, cluster, cluster->numGenes, dataMatrix, matrixWidth, ifile, SOMdesign, eWeights);
  
  free(SOMdesign);
  
  return(dataMatrix);
  
}
 
 
/*
 * Function : designSom
 * Usage : SOMdesign=designSOM(x, y)
 * ---------------------------------
 * This function returns a SOM struct that
 * describes the dimensions of the SOM.
 * Currently the SOM dimensions, and the number
 * of iterations are fixed, but these will eventually 
 * be input by the user.
 */
 
SOM *designSOM(int x, int y){

  SOM *descriptions=malloc(sizeof(SOM));
  
  if (descriptions == NULL) Error ("Not enough memory for SOM\n");
  
  descriptions->x=x;
  descriptions->y=y;
  
  descriptions->iterations=100000;
  
  return(descriptions);
  
}

/*
 * Function : FilterData
 * Usage : dataMatrix=FilterData(dataMatrix, cluster, numExperiments, numNodes, ifile);
 * ----------------------
 * This functions filters the data to weed out any expression
 * patterns that are essentially constant.  In the future I will
 * allow the filter criterion to be user defined.  Once it has filtered the data
 * it sets up a new dataMatrix and nameRecArray, then frees the memory associated with the 
 * old data structures.
 */
 
float *FilterData(float *dataMatrix, clusterRec *cluster, int numExperiments, int numNodes, char *ifile){

  float max, min, value;
  float *filteredData;
  int *goodData;
  int gene, numGenes, experiment, filteredGene, ORFLength, nameLength, seen;
  int numGoodGenes=0;
  nameRec *filteredNameRecs;
  FILE *filteredFile;
  char *filteredName;
  char *prefix;
  
  numGenes=cluster->numGenes;
  
  goodData=malloc(numGenes*sizeof(int));
  if (goodData==NULL) Error("Not enough memory for good data array");
  
  for (gene=0; gene<numGenes; gene++){ /* go through each gene */
    
    experiment=0;
    seen=0; /* haven't seen a good piece of data yet */
    max=NODATA;
    min=NODATA; /* need to set max and min equal to each other */
    
    while (dataMatrix[gene*numExperiments+experiment]==NODATA && experiment<numExperiments){ /* find first good data point */
      
      experiment++;
      
    }	

    /* assign first good data point (if one exists) to min and max */

    if (dataMatrix[gene*numExperiments+experiment]!=NODATA){
      max=min=dataMatrix[gene*numExperiments+experiment]; /* assign max and min to the first good datapoint for the particular gene */
      seen++;
    }

    experiment++;

    /* now check remaing data points (if any) to find overall max and min */
    /* if we go into the loop, we must have seen at least one good data point,
     * hence max and min will have been initialized to that datapoint */
    
    for (; experiment<numExperiments; experiment++){
      
      value=dataMatrix[gene*numExperiments+experiment];
      
      if (value==NODATA) continue; /* if no data, get the next value */
      
      seen++; /* record number of good values */
  
      if (value>max) max=value;
      if (value<min) min=value;

    }
    
    if (seen<3 || ((max-min)<1)){ /* 2 fold difference between max and min */
      goodData[gene]=0;
    }else{
      goodData[gene]=1;
      numGoodGenes++;
    }
    
  }
  
  printf("numGenes: %d, numGoodGenes: %d\n", numGenes, numGoodGenes);
  
  /* NOTE early return here, if no data to be filtered out */
  
  if(numGoodGenes==numGenes){
    
    free(goodData);
    
    return (dataMatrix);
    
  }

  if (numGoodGenes==0){
    printf("No genes passed the filtering criteria of varying at least 2-fold across the experiment.\n");
    printf("A Self Organizing Map can therefore not be made.\n");
    printf("Please try with a different dataset\n");
    exit(10); /* error code for nothing passing filtering */
  }
  
  /* otherwise need to set up new matrix with the filtered data */
  
  filteredData=malloc((numGoodGenes+numNodes+1)*numExperiments*sizeof(float));
  if (filteredData==NULL) Error("Not enough memory for filtered Data array");
  
  filteredNameRecs=malloc(numGoodGenes*sizeof(nameRec));
  if (filteredNameRecs==NULL) Error("Not enough memory for filteredNameRecs");
  for(filteredGene=0; filteredGene<numGoodGenes; filteredGene++){
    InitializeArray(&filteredNameRecs[filteredGene]);
  }
  
  prefix=GetFilePrefix(ifile);
  
  filteredName=malloc((strlen(prefix)+4)*sizeof(char));
  
  strcpy(filteredName, prefix);
  
  strcpy(filteredName+strlen(prefix), "fil\0");
  
  filteredFile=OpenOutFile(filteredName);
  
  filteredGene=0;
  
  for (gene=0; gene<numGenes; gene++){
    
    if (goodData[gene]){
      
      for (experiment=0; experiment<numExperiments; experiment++){
	
	filteredData[filteredGene*numExperiments+experiment]=dataMatrix[gene*numExperiments+experiment];
	
      }
      
      ORFLength=strlen(cluster->genes[gene].orf);
      filteredNameRecs[filteredGene].orf=malloc((ORFLength+1)*sizeof(char));
      if (filteredNameRecs[filteredGene].orf==NULL) Error ("Not enough memory for orf name");
      strcpy(filteredNameRecs[filteredGene].orf, cluster->genes[gene].orf);
      
      if(cluster->genes[gene].name!=NULL){
	nameLength=strlen(cluster->genes[gene].name);
	filteredNameRecs[filteredGene].name=malloc((nameLength+1)*sizeof(char));
	if (filteredNameRecs[filteredGene].name==NULL) Error ("Not enough memory for gene name");
	strcpy(filteredNameRecs[filteredGene].name, cluster->genes[gene].name);
	
      }
      
      filteredNameRecs[filteredGene].rowWeight=cluster->genes[gene].rowWeight;
      
      filteredGene++;
      
    }else{
      
      /* print out the gene here that is being filtered out, so there is a record */
      
      PrintOneLine(cluster, gene, filteredFile, dataMatrix, numExperiments);
      
    }		
    
  }
  
  
  fclose(filteredFile);
  free(prefix);
  free(filteredName);
  FreeCluster(cluster);
  cluster->genes=filteredNameRecs;
  cluster->numGenes=numGoodGenes;
  free(dataMatrix);
  free(goodData);
  return(filteredData);
  
}


/*
 * Function : InitializeSOM
 * Usage : InitializeSOM(dataMatrix, &cluster, SOMdesign, numExperiments)
 * -------------------------------------------
 * This function initializes the SOM nodes (which are 
 * at the end of the dataMatrix) to represent the initially random
 * locations in k-dimensional space (where k is the 
 * number of experiments) that the SOM nodes occupy.
 */
 
void InitializeSOM(float *dataMatrix, clusterRec* cluster, SOM *SOMdesign, int numExperiments){

  int currNode;
  int currDataPoint;
  int matrixOffset;
  int numNodes=SOMdesign->x * SOMdesign->y;
  matrixOffset=cluster->numGenes*numExperiments;
  
  if (gRandomize){
    srand((int) time(NULL)); /* initialize pseudo random number generator using the time */
  }

  for (currNode=0; currNode<numNodes; currNode++){
    
    for (currDataPoint=0; currDataPoint<numExperiments; currDataPoint++){
      
      /* 
       * generate a random datapoint between -1 and +1
       */
      
      dataMatrix[matrixOffset+currNode*numExperiments+currDataPoint]=((((float)rand())/(float)RAND_MAX)*2)-1;
      
    }
    
    /* now make each of the random node into unit vector */
    
    MakeUnitVector(dataMatrix, currNode, numExperiments, matrixOffset);
    
  }	
  
}

/*
 * Function : MakeUnitVector
 * Usage : MakeUnitVector(dataMatrix, currNode, numExperiments, matrixOffset);
 * -------------------------------
 * This functions iterates over the data for a node, 
 * then makes it into a unit vector, by dividing
 * each value by the sum of the squares.
 */
 
void MakeUnitVector(float *dataMatrix, int currNode, int numExperiments, int matrixOffset){
	
  int currDataPoint;
  float currValue=0;
  float nodeLength=0;
  
  for (currDataPoint=0; currDataPoint<numExperiments; currDataPoint++){
    
    currValue=dataMatrix[matrixOffset+currNode*numExperiments+currDataPoint];
    
    nodeLength+=currValue*currValue; /* keep track of the sum of the squares */
    
  }
  
  nodeLength=sqrt(nodeLength);
  
  NormalizeGene(numExperiments, matrixOffset, dataMatrix, currNode, nodeLength);
  
}

/*
 * Function : OrganizeSOM
 * Usage : OrganizeSOM(cluster, SOMdesign, dataMatrix, eWeights, numExperiments, matrixWidth, ifile)
 * ---------------------
 * This function actually does the work to generate the 
 * Self Organizing Map.  It first sets up an array of
 * numbers ranging from zero to numGenes, in random order,
 * to determine the order in which the genes will be sampled.
 * It then determines which node is most similar to that genes
 * (by cacluating a pearson correlation, and adjusts the nodes 
 * accordingly.
 */

void OrganizeSOM(clusterRec* cluster, SOM *SOMdesign, float *dataMatrix, float *eWeights, int numExperiments, int matrixWidth, char *ifile){

  int *randomOrder;
  int iteration;
  int node;
  int numNodes;
  int numIterations;
  float pearsonCorrelation;
  int bestNode=-1;
  float bestScore=-1;
  float totalDist;
  float nodeLength;
  int matrixOffset=cluster->numGenes*numExperiments;
  int pickedGene;
  char **evolutionFileNames;
  
  numNodes=SOMdesign->x * SOMdesign->y;
  numIterations=SOMdesign->iterations;
  totalDist=(float)sqrt(SOMdesign->x*SOMdesign->x + (SOMdesign->y-1)*(SOMdesign->y-1)); /* distance from origin to furthest corner of last node */
  
  
  randomOrder=MakeRandomOrder(cluster->numGenes);
  evolutionFileNames=malloc(numNodes*sizeof(char*));
  if (evolutionFileNames==NULL) Error ("Not enough memory for evolution filenames");
  
  MakeEvolutionFileNames(evolutionFileNames, numNodes, ifile, SOMdesign);
	
  for (iteration=0; iteration<numIterations; iteration++){
    
    pickedGene=randomOrder[iteration%(cluster->numGenes)];
    
    nodeLength=0;
    
    if (!(iteration%10000)){
      
      printf("%d\n", iteration);

      fflush(stdout);
      
      PrintEvolutionFiles(evolutionFileNames, numNodes, dataMatrix, matrixOffset, matrixWidth, iteration);
      
    }
    
    /* copy the randomly picked gene's data into the extra row that was allocated in the dataMatrix */
    
    nodeLength=PutGeneInDataMatrix(matrixWidth, matrixOffset, dataMatrix, numNodes, pickedGene);
    
    NormalizeGene(matrixWidth, matrixOffset, dataMatrix, numNodes, nodeLength);
    
    for (node=0; node<numNodes; node++){
      
      pearsonCorrelation=CalculateCorrelation(cluster->numGenes+numNodes, cluster->numGenes+node, dataMatrix, numExperiments, 
					      eWeights, 'G', cluster->numGenes, cluster, matrixWidth);
      if (pearsonCorrelation>bestScore){
	
	bestScore=pearsonCorrelation;
	bestNode=node;
      }
      
    }
    
    AdjustNodes(bestNode, SOMdesign, iteration, numNodes, cluster->numGenes+numNodes, dataMatrix, matrixWidth, numExperiments, cluster->numGenes, totalDist);
    
    bestScore=-1;
    bestNode=-1;
    
  }
  
  for (node=0; node<numNodes;node++){
    free (evolutionFileNames[node]);
  }
  free (evolutionFileNames);
  free(randomOrder);
  
  
}

/*
 * Function : MakeEvolutionFileNames
 * Usage : MakeEvolutionFileNames(evolutionFileNames, numNodes, ifile, SOMdesign)
 * -------------------------------------------------------------------
 * This functions make the filenames for the SOM evolution data to be stored in
 */
 
void MakeEvolutionFileNames(char **fileNames, int numNodes, char *ifile, SOM* SOMdesign){

  char* prefix;
  int i;
  char num[20];
  FILE *current;
  
  prefix=GetFilePrefix(ifile);
  
  for (i=0; i<numNodes; i++){
    
    sprintf(num, "_%d_%d_som%d.evo", SOMdesign->x, SOMdesign->y, i);
    
    fileNames[i]=malloc((strlen(prefix)+strlen(num)+1)*sizeof(char));
    
    if (fileNames[i]==NULL) Error("Not enough memory for fileNames[%d]\n", i+1);
    
    strcpy(fileNames[i], prefix);
    
    strcpy(fileNames[i]+strlen(prefix)-1, num);
    
    current=OpenOutFile(fileNames[i]);
    
    fclose(current);               /* because FOPEN_MAX is set to 32, 1st clear files
				    * then later we will append to them */		
  }
  
  free(prefix);
  
}

/*
 * Function : MakeRandomOrder
 * Usage : randomOrder = MakeRandomOrder(cluster->numGenes)
 * --------------------------------------------------------
 * This function sets up an array with the index of each gene
 * in random order.  It does this by setting up an array, with
 * each number entered into it in order, then picking a random number
 * in that range, and putting the number at that index at the next
 * available slot in the random number array.  To 'delete' that number from 
 * the source array, it simply shifts everything to the left of that 
 * number one to the left, and decreases the range in which it is
 * generating random numbers by one.
 */

int *MakeRandomOrder(int numGenes){

  int *numbersToChoose;
  int *randomOrder;
  int i;
  int randNumber;
  int numsLeft=numGenes;
  
  numbersToChoose=malloc(numGenes*sizeof(int));
  if(numbersToChoose==NULL)Error("Not enought memory for numbersToChoose");
  randomOrder=malloc(numGenes*sizeof(int));
  if(randomOrder==NULL)Error("Not enought memory for randomOrder");
  
  for (i=0; i<numGenes; i++){
    
    numbersToChoose[i]=i;
    
  }
  
  while (numsLeft>0){
    
    randNumber=rand()%numsLeft;
    
    randomOrder[numGenes-numsLeft]=numbersToChoose[randNumber]; 
    
    numbersToChoose[randNumber]=numbersToChoose[numsLeft-1]; /* Switch picked number with last number in array */
    
    numsLeft--;
    
  }
  
  free(numbersToChoose);
  
  return(randomOrder);
  
}

/* Function : PrintEvolutionFiles
 * Usage : PrintEvolutionFiles(evolutionFiles, numNodes, dataMatrix, matrixOffset, matrixWidth, iteration);
 * -------------------------------------------------------------------------------------------------------
 * This function prints out the data to show the evolution of the SOMs
 */
 
void PrintEvolutionFiles(char **evolutionFileNames, int numNodes, float *dataMatrix, int matrixOffset, int matrixWidth, int iteration){

  int file, dataPoint;
  FILE *current;
  
  for(file=0; file<numNodes; file++){
    
    current=OpenForAppend(evolutionFileNames[file]);
    
    fprintf(current, "SOM%d\t%d", file, iteration);
    
    for (dataPoint=0; dataPoint<matrixWidth; dataPoint++){
      
      if (dataMatrix[matrixOffset+file*matrixWidth+dataPoint]!=NODATA){
	
	fprintf(current, "\t%f", dataMatrix[matrixOffset+file*matrixWidth+dataPoint]);
	
      }else{
	
	fprintf(current, "\t\t");
	
      }
      
    }
    
    fprintf(current, "\n");
    fclose(current);
    
  }
  
}

/*
 * Function : PutGeneInMatrix
 * Usage : nodeLength=PutGeneInDataMatrix(matrixWidth, matrixOffset, dataMatrix, numNodes, pickedGene);
 * -----------------------------
 * This function copies the data for the randomly picked gene into the last position
 * of the dataMatrix.  It calculates and returns the length of the vector.
 */

float PutGeneInDataMatrix(int matrixWidth, int matrixOffset, float *dataMatrix, int numNodes, int pickedGene){
  
  int dataPoint;
  float currValue; 
  float nodeLength=0;
  
  for(dataPoint=0; dataPoint<matrixWidth; dataPoint++){
    
    dataMatrix[matrixOffset+numNodes*matrixWidth+dataPoint]=dataMatrix[pickedGene*matrixWidth+dataPoint];
    
    currValue=dataMatrix[matrixOffset+numNodes*matrixWidth+dataPoint];
    
    if (currValue!=NODATA){
      
      nodeLength+=currValue*currValue;
      
    }
    
  }
  
  nodeLength=sqrt(nodeLength);
  
  return nodeLength;
  
}

/* Function : NormalizeGene
 * Usage : NormalizeGene(matrixWidth, matrixOffset, dataMatrix, numNodes, nodeLength)
 * ------------------------
 * This function normalizes a gene or nodes vector,
 * such that it length is 1, ie it's a unit vector,
 * by dividing each value by the vector length.
 */
 
void NormalizeGene(int matrixWidth, int matrixOffset, float *dataMatrix, int node, float nodeLength){

  int dataPoint;
  
  for (dataPoint=0; dataPoint<matrixWidth; dataPoint++){
    
    if (dataMatrix[matrixOffset+node*matrixWidth+dataPoint]!=NODATA){
      
      dataMatrix[matrixOffset+node*matrixWidth+dataPoint]/=nodeLength;
      
    }
    
  }
  
}
/*
 * Function : AdjustNodes
 * Usage : AdjustNodes(bestNode, SOMdesign, iteration, numNodes, cluster->numGenes+numNodes, dataMatrix, matrixWidth, numExperiments, cluster->numGenes, totalDist);
 * --------------------------------------------------------------------------------------------------------------------------------------
 * This function adjust the nodes, given the node closest to the 
 * datapoint of interest.
 */
 
void AdjustNodes(int bestNode, SOM *SOMdesign, int iteration, int numNodes, int gene, float *dataMatrix, int matrixWidth, int numExperiments, int numGenes, float totalDist){

  float dist;
  float nodeDist;
  int xj, yj; /* coordinates of best node */
  int xi, yi;
  int k;
  int val;
  float tau;
  int matrixOffset=numGenes*numExperiments;
  
  xj=bestNode%SOMdesign->x;/* work out x and y coordinates of the best node */
  yj=bestNode/SOMdesign->x;
  
  /* calculate a distance, based on size of SOM, and iteration we're on */
  
  dist = (float) (totalDist*(0.75-((0.75*iteration)/SOMdesign->iterations)));
  
  if(iteration<1000){ /* higher rate of learning during the first 1000 iterations */
    
    tau = (float)(0.9*(1-iteration/1000));
    
  }else{
    
    tau = (float)(0.02*SOMdesign->iterations)/(SOMdesign->iterations+(100*iteration));
    
  }
  
  for (k=0; k<numNodes; k++){
    
    xi=k%SOMdesign->x; /* work out x and y coordinates of the current node */
    yi=k/SOMdesign->x;
    
    /* calculate Euclidean distance between current node and best node in 2D space */
    
    nodeDist=sqrt((xi - xj)*(xi-xj) + (yi - yj)*(yi-yj));
    
    if (nodeDist< dist) { /* if it's closer than a certain distance */
      
      for (val=0; val<numExperiments; val++){
	
	if (dataMatrix[gene*matrixWidth+val]!=NODATA){
	  
	  /* move reference vector of node towards gene, one datapoint at a time */
	  
	  dataMatrix[(numGenes+k)*matrixWidth+val] += (float)(tau * (dataMatrix[gene*matrixWidth+val]-dataMatrix[(numGenes+k)*matrixWidth+val]));
	  
	}
	
      }
      
    }
    
    MakeUnitVector(dataMatrix, k, numExperiments, matrixOffset);
    
  }
  
}

/*
 * Function : SortSOMData
 * Usage : bestHits=SortSOMData(numExperiments, dataMatrix, cluster->numGenes, numNodes, eWeights, cluster, matrixWidth, ifile, SOMdesign);
 * --------------------------------------------------------------------------------------------------------------------------------------
 * This function generates an array of ints that says which
 * SOM node a gene is closest to.
 */
 
int *SortSOMData(int numExperiments, float *dataMatrix, int numGenes, int numNodes, float *eWeights, clusterRec *cluster, int matrixWidth, char *ifile, SOM *SOMdesign){
  
  int *bestHits;
  int gene, node;
  float bestScore;
  float pearsonCorrelation;
  char **correlationFilenames;
  FILE *current;
  
  correlationFilenames=malloc(numNodes*sizeof(char*));
  if (correlationFilenames==NULL) Error("Not enough room for correlation filenames\n");
  
  MakeCorrelationFileNames(correlationFilenames, ifile, numNodes, SOMdesign);
  
  bestHits=malloc(numGenes*sizeof(int));
  if (bestHits==NULL) Error("Not enough memory for bestHits");
  
  for (gene=0; gene<numGenes; gene++){
    
    bestScore=-2;
		
    for (node=0; node<numNodes; node++){
      
      pearsonCorrelation=CalculateCorrelation(gene, numGenes+node, dataMatrix, numExperiments, 
					      eWeights, 'G', numGenes, cluster, matrixWidth);
      if (pearsonCorrelation>bestScore){
	
	bestScore=pearsonCorrelation;
	bestHits[gene]=node;
	
      }
      
    }
        
    current=OpenForAppend(correlationFilenames[bestHits[gene]]);
    
    if(cluster->genes[gene].name!=NULL){
      
      fprintf(current, "%s\t%s\t%f\n", cluster->genes[gene].orf, cluster->genes[gene].name, bestScore);
      
    }else{
      
      fprintf(current, "%s\t\t%f\n", cluster->genes[gene].orf, bestScore);
      
    }
    
    fclose(current);
    
  }
  
  for (node=0; node<numNodes; node++){
    free(correlationFilenames[node]);
  }
  free(correlationFilenames);
  
  return(bestHits);
  
}

/*
 * Function: MakeCorrelationFileNames
 * Usage : MakeCorrelationFileNames(correlationFilenames, ifile, numNodes, SOMdesign)
 * ----------------------------------------------------------------------------------
 * This function makes the filenames needed to record the correlations that genes have to
 * the node with which they become associated.
 */
 
void MakeCorrelationFileNames(char **fileNames, char *ifile, int numNodes, SOM *SOMdesign){

  char *prefix;
  int i;
  char num[32];
  FILE *current;
  
  prefix=GetFilePrefix(ifile);
  
  for (i=0; i<numNodes; i++){
    
    sprintf(num, "_%d_%d_som%d.cor", SOMdesign->x, SOMdesign->y, i);
    
    fileNames[i]=malloc((strlen(prefix)+strlen(num)+1)*sizeof(char));
    
    if (fileNames[i]==NULL) Error("Not enough memory for fileNames[%d]\n", i+1);
    
    strcpy(fileNames[i], prefix);
    
    strcpy(fileNames[i]+strlen(prefix)-1, num);
    
    current=OpenOutFile(fileNames[i]);
    
    fclose(current);               /* because FOPEN_MAX is set to 32, 1st clear files
				    * then later we will append to them */				
  }
  
  free(prefix);
  
}

/*
 * Function : PrintSOMOrganization
 * Usage : PrintSOMOrganization(numNodes, bestHits, numExperiments, experimentNames, cluster, cluster->numGenes, dataMatrix, matrixWidth, ifile, SOMdesign, eWeights);
 * -------------------------------
 * This functions prints out the contents of each SOM,
 * simply in the order that the genes appear in the original dataset.
 * The functions prints out one big file, with the SOMs concatenated,
 * separated by a blank line, and also a separate file for each SOM,
 * which can subsequently be clustered.  The filenames for the each file
 * are passed in in an array, fileNames, with the name of the large file being the
 * first entry.
 */
 
void PrintSOMOrganization(int numNodes, int *bestHits, int numExperiments, char **experimentNames, clusterRec *cluster, int numGenes, float *dataMatrix, int matrixWidth, char *ifile, SOM *SOMdesign, float *eWeights){

  FILE *current;
  int file;
  int gene;
  char** fileNames;
  int* remember;
  int* numInSOM;
  int num=0;
  float *tempMatrix;
  clusterRec tempCluster;
  int tempMatrixWidth;
  char *cdtFileName;
  char *gtrFileName;
  char *atrFileName;
  int changedUIDstatus=0;
  
  remember=malloc(numNodes*sizeof(int));
  numInSOM=malloc(numNodes*sizeof(int));
  if (remember==NULL) Error("Not enough memory for 'remember array'\n");
  if (numInSOM==NULL) Error("Not enoght memory for 'numInSOM array'\n");
  
  fileNames = malloc((numNodes+1)*sizeof(char*));
  if (fileNames==NULL) Error("Not enough memory for filenames");
  
  MakeSOMFileNames(fileNames, ifile, numNodes, SOMdesign);
  
  for (file=0; file<numNodes; file++){
    
    numInSOM[file]=0;
    
    remember[file]=0;
    
    for (gene=0; gene<numGenes; gene++){
      
      if (bestHits[gene]==file){
	
	numInSOM[file]++;
	
	remember[file]=1;
	
      }
      
    }
    
  }
  
  if (gUID){
    
    gUID=0;
    changedUIDstatus=1;

  }
  
  for (file=0; file<numNodes; file++){
    
    if(numInSOM[file]>2){ /* only need to do if have more than 2 genes in a SOM */
      
      AllocateTempMemory(numInSOM[file], numExperiments, &tempMatrix, &tempMatrixWidth, &tempCluster);
      
      PopulateTempMemory(file, numExperiments, tempMatrixWidth, &tempCluster, tempMatrix, cluster, bestHits, dataMatrix);
      
      HierarchicallyCluster(fileNames[file+1], numExperiments, &tempCluster, tempMatrix, eWeights, 
			    numInSOM[file], tempMatrixWidth, experimentNames);
      
      free(tempMatrix);
      FreeCluster(&tempCluster);
      
      
    }else{
      
      MakeFileNames(fileNames[file+1], &gtrFileName, &cdtFileName, &atrFileName);
      
      current=OpenOutFile(cdtFileName);
      
      PrintFirstLine(current, numExperiments, experimentNames);
      
      if (numInSOM[file]){
	
	for (gene=0; gene<numGenes; gene++){
	  
	  if (bestHits[gene]==file){
	    
	    PrintOneLine(cluster, gene, current, dataMatrix, numExperiments);
	    
	  }
	  
	}
	
      }
      
      fclose(current);
      
      free (cdtFileName);
      free (atrFileName);
      free (gtrFileName);
      
    }					
    
    
  }
  
  if (changedUIDstatus){
    
    gUID=1;

  }
  
  PrintSomData(dataMatrix, numNodes, matrixWidth, numGenes, ifile, SOMdesign);
  free(fileNames[0]);
  for(file=1; file<=numNodes; file++){
    if (remember[file-1]) num++;
    
    free(fileNames[file]);
  }
  printf("%d have hits\n", num);
  free(fileNames);
  free(bestHits);
  free(numInSOM);
  free(remember);
  
}

/*
 * Function : PrintSomData
 * Usage : PrintSomData(dataMatrix, numNodes, matrixWidth, numGenes, ifile, SOMdesign)
 * ------------------------------------------------------------------------
 * This function print the data from the final SOM nodes to a file.
 */
 
void PrintSomData(float *dataMatrix, int numNodes, int matrixWidth, int numGenes, char *ifile, SOM *SOMdesign){

  char *prefix;
  char *somFileName;
  FILE *somFile;
  char num[20];
  int i, j;
  int matrixOffset=numGenes*matrixWidth;
  
  prefix=GetFilePrefix(ifile);
  
  sprintf(num, "_%d_%d.som", SOMdesign->x, SOMdesign->y);
  
  somFileName=malloc((strlen(prefix)+strlen(num)+1)*sizeof(char));
  
  strcpy(somFileName, prefix);
  
  strcpy(somFileName + strlen(prefix)-1, num);
  
  somFile=OpenOutFile(somFileName);
  
  for (i=0; i<numNodes; i++){
    
    fprintf(somFile, "SOM%d\t%d", i, SOMdesign->iterations);
    
    for	(j=0; j<matrixWidth; j++){
      
      fprintf(somFile, "\t%.2f", dataMatrix[matrixOffset+i*matrixWidth+j]);
      
    }
    
    fprintf(somFile, "\n");
    
  }
  
  fclose(somFile);	
  free(prefix);
  free(somFileName);
  
}


/*
 *
 */

void MakeSOMFileNames(char **fileNames, char *ifile, int numNodes, SOM *SOMdesign){

  char* prefix;
  int i;
  char num[20];
  
  prefix=GetFilePrefix(ifile);
  
  fileNames[0]=malloc((strlen(prefix)+4)*sizeof(char)); /* space for the big fileName */
  if (fileNames[0]==NULL) Error("Not enough memory for fileName[0]\n");
  strcpy(fileNames[0], prefix);
  strcpy(fileNames[0]+strlen(prefix), "cdt\0");
  
  for (i=0; i<numNodes; i++){
    
    sprintf(num, "_%d_%d_som%d.txt", SOMdesign->x, SOMdesign->y, i);
    
    fileNames[i+1]=malloc((strlen(prefix)+strlen(num)+1)*sizeof(char));
    
    if (fileNames[i+1]==NULL) Error("Not enough memory for fileNames[%d]\n", i+1);
    
    strcpy(fileNames[i+1], prefix);
    
    strcpy(fileNames[i+1]+strlen(prefix)-1, num);
    
  }
  
  free(prefix);
	
}

/*
 * Function : PrintFirstLine
 * Usage : PrintFirstLine(current, numExperiments, experimentNames);
 * -------------------------
 * This function prints out the first line of the output files.
 * I put it into a separate function, as it gets used from
 * several places.
 */
 
void PrintFirstLine(FILE* outfile, int numExperiments, char** experimentNames){

  int i;
  
  fprintf(outfile, "UID\tYORF\tNAME\tGWEIGHT");
  
  for (i=0; i<numExperiments; i++){
    
    fprintf(outfile, "\t%s", experimentNames[i]);
    
  }
  
  fprintf(outfile, "\n");
  
}

/*
 * Function : PrintOneLine
 * Usage : PrintOneLine(cluster, gene, current, dataMatrix, numExperiments)
 * --------------------------------------------------------------------
 * This function print out one line from the SOM
 * organized data.
 */
 
void PrintOneLine(clusterRec *cluster, int geneNumber, FILE *outfile, float *dataMatrix, int matrixWidth){

  int column;
  
  if (cluster->genes[geneNumber].name!=NULL){
    
    fprintf(outfile, "GENE0X\t%s\t%s\t1\t", cluster->genes[geneNumber].orf, cluster->genes[geneNumber].name);
    
  }else{
    
    fprintf(outfile, "GENE0X\t%s\t\t1\t", cluster->genes[geneNumber].orf);
    
  }
  
  for (column=0; column<matrixWidth; column++){
    
    if (dataMatrix[geneNumber*matrixWidth+column]!=NODATA){
      fprintf(outfile, "%.2f\t", dataMatrix[geneNumber*matrixWidth+column]);
      
    }else{
      fprintf(outfile, "\t");
    }
  }
  
  fprintf(outfile, "\n");

}

/*
 * Function : AllocateTempMemory
 * Usage : AllocateTempMemory(numInSOM[file], numExperiments, &tempMatrix, &tempMatrixWidth, &tempCluster)
 * ---------------------------------------------------------------------------------------------------
 * This function allocates necessary data structures to hierarchically cluster
 * the gene expression data associated with each SOM.
 */
 

void AllocateTempMemory(int numInSOM, int numExperiments, float **tempMatrix, int *tempMatrixWidth, clusterRec *tempCluster){

  int count;
  
  /* first allocate memory for temporary data */
  
  tempCluster->numGenes=numInSOM; /* put these in experiment to cut down on number of arguments that need passing */
  
  if (numExperiments >tempCluster->numGenes && gShouldClusterExperiments){
    (*tempCluster).genes=malloc((2*(numExperiments)-1) * (sizeof(nameRec)));/* allocate the memory for the name records */
  }else{
    (*tempCluster).genes=malloc((2*(tempCluster->numGenes)-1) * (sizeof(nameRec)));/* allocate the memory for the name records */
  }
  
  if ((*tempCluster).genes==NULL) Error("No memory available for nameRecs");
  
  /* now initialize the genes array */
  
  if (numExperiments >tempCluster->numGenes && gShouldClusterExperiments){
    for (count=0; count<2*numExperiments-1; count++){
      InitializeArray(&(tempCluster->genes[count]));
    }
  }else{	
    for (count=0; count<2*tempCluster->numGenes-1; count++){
      InitializeArray(&(tempCluster->genes[count]));
    }
  }
  
  if (gShouldClusterExperiments){
    *tempMatrix=malloc((2*numExperiments-1)*(2*tempCluster->numGenes-1)*sizeof(float));
    *tempMatrixWidth=2*numExperiments-1;
  }else{
    *tempMatrix=malloc(numExperiments*(2*tempCluster->numGenes-1)*sizeof(float));
    *tempMatrixWidth=numExperiments;
  }
  
  if (*tempMatrix==NULL) Error("No memory available for dataMatrix");
    
}

/*
 * Function : PopulateTempMemory
 * Usage : PopulateTempMemory(file, numExperiments, tempMatrixWidth, &tempCluster, tempMatrix, cluster, bestHits, dataMatrix)
 * ----------------------------------------------------------------------------------------------------------------------
 * This function should populate the temporary data structures
 * with the relevant gene data for clustering.
 */

void PopulateTempMemory(int file, int numExperiments, int tempMatrixWidth, clusterRec *tempCluster, float *tempMatrix, clusterRec *cluster, int *bestHits, float *dataMatrix){
	
  int gene;
  int row=0;
  int column;
  int ORFLength;
  int nameLength;
  int numGenes=cluster->numGenes;
  
  for (gene=0; gene<numGenes; gene++){
    
    if (bestHits[gene]==file){
      
      for (column=0; column<numExperiments; column++){ /* copy across actual data */
	
	tempMatrix[row*tempMatrixWidth+column]=dataMatrix[gene*numExperiments+column];
	
      }
      
      /* now get gene name stuff */
      
      ORFLength=strlen(cluster->genes[gene].orf);
      tempCluster->genes[row].orf=malloc((ORFLength+1)*sizeof(char));
      if (tempCluster->genes[row].orf==NULL) Error ("Not enough memory for orf name");
      strcpy(tempCluster->genes[row].orf, cluster->genes[gene].orf);
      
      if(cluster->genes[gene].name!=NULL){
	nameLength=strlen(cluster->genes[gene].name);
	tempCluster->genes[row].name=malloc((nameLength+1)*sizeof(char));
	if (tempCluster->genes[row].name==NULL) Error ("Not enough memory for gene name");
	strcpy(tempCluster->genes[row].name, cluster->genes[gene].name);
      }
      
      tempCluster->genes[row].rowWeight=cluster->genes[gene].rowWeight;
      
      row++;
      
    }
    
  }
  
}

/*
 * All Methods below are involved in the generation of k-means clusters
 */
 
/*
 * Function: KMeansCluster
 * Usage : 
 * ------------------------------------------------------------------------------------------------
 * This function handles all the functions and data structures necessary to partition the data
 * into k different clusters, and to then cluster the genes that map to those clusters hierarchically.
 * Want to write an algorithm that will optimize the number of clusters into which the data are split,
 * by looking at average correlation of a gene to it's centroid, using the real data vs using randomized
 * data, and maximizing the difference between them.
 */
 
void KMeansCluster(float *dataMatrix, clusterRec *cluster, int numExperiments, float *eWeights,
		   char **experimentNames, char *ifile, int k){
  
  int *centroidAssignments;
  int *numInCentroids;
  int i;
  
  /* keep a record of to which centroid a gene is assigned */
  centroidAssignments=malloc(cluster->numGenes*sizeof(int));
  if (centroidAssignments==NULL) Error("Not enough memory for cetroidAssignments");
  /* initially assign genes to no centroid (-1) */
  for (i=0; i<cluster->numGenes; i++){
    centroidAssignments[i]=-1;
  }
  
  numInCentroids=malloc(cluster->numGenes*sizeof(int));
  if (numInCentroids==NULL) Error("Not enough memory for centroid assignments");
  
  MakeRandomSeeds(dataMatrix, cluster, numExperiments, k);

  MakeClusters(dataMatrix, cluster, numExperiments, k, centroidAssignments, eWeights, numInCentroids);
  
  OrganizeKClusters(centroidAssignments, numInCentroids, numExperiments, experimentNames, cluster, 
		    cluster->numGenes, dataMatrix, ifile, k, eWeights);
  
  free (numInCentroids);
  free (centroidAssignments);
  
}

/*
 * Function : MakeRandomSeeds
 * Usage :
 * -------------------------------------------------------------------------------------
 * This function sets up random centroids to begin the clustering process with
 */
 
void MakeRandomSeeds(float *dataMatrix, clusterRec* cluster, int numExperiments, int k){

  int currCentroid;
  int currDataPoint;
  int matrixOffset;
  matrixOffset=cluster->numGenes*numExperiments;
  
  if (gRandomize){
    srand((int) time(NULL)); /* initialize pseudo random number generator using the time */
  }
  
  for (currCentroid=0; currCentroid<k; currCentroid++){
    
    for (currDataPoint=0; currDataPoint<numExperiments; currDataPoint++){
      
      /* 
       * generate a random datapoint between -1 and +1
       */
      
      dataMatrix[matrixOffset+currCentroid*numExperiments+currDataPoint]=((((float)rand())/(float)RAND_MAX)*2)-1;
      
    }
    
  }
  
}

/*
 * Function: MakeClusters
 * Usage :
 * --------------------------------------------------------------------
 * This function takes whatever dataMatrix was passed in, and makes k-means
 * clusters from it.  It then returns the 'quality' of the clusters to the
 * calling function.  The quality is just the average pearson correlation of 
 * each gene to the centroid to which it is finally assigned.
 */ 

void MakeClusters(float *dataMatrix, clusterRec *cluster, int numExperiments, int k, int *centroidAssignments, 
		   float *eWeights, int *numInCentroids){
  
  int change=1;
  int iteration=1;
  
  /* now continually refine centroids until there's no change */
  
  while(change){
    
    printf("Assigning Genes to Centroids: iteration %d\n", iteration);
    
    change=AssignGenesToCentroids(dataMatrix, cluster, numExperiments, k, centroidAssignments, eWeights, numInCentroids);
    if (change){
      CalculateCentroids(dataMatrix, cluster, numExperiments, k, centroidAssignments);
    }
    
    iteration++;
    
  }
  
  printf("Converged\n");
	
}
 
/*
 * Function : AssignGenesToCentroids
 * Usage :
 * ----------------------------------
 * This functions checks the contents of the current centroids, and determines to which 
 * centroid a gene is most similar.  It then assigns that gene to belonging to that 
 * centroid.
 */
 
int  AssignGenesToCentroids(float *dataMatrix, clusterRec *cluster, int numExperiments, int k, int *centroidAssignments, 
			    float *eWeights, int *numInCentroids){
  
  int gene;
  int change=0;
  int centroid;
  float max;
  int best=-1;
  float measure;
  
  /* reset the number of genes assigned to each centroid */
  
  for (centroid=0; centroid<k; centroid++){
    numInCentroids[centroid]=0;
  }
  
  for (gene=0; gene<cluster->numGenes; gene++){ /* look at each gene */
    
    max=-1;
    
    for (centroid=cluster->numGenes; centroid<cluster->numGenes+k; centroid++){ /* compare to each centroid */
      
      measure=CalculateCorrelation(gene, centroid, dataMatrix, numExperiments, eWeights, 'G', cluster->numGenes, cluster, numExperiments);
      
      if (measure>max){
	
	max=measure;	
	best=centroid-cluster->numGenes;
	
      }
      
    }
    
    numInCentroids[best]++;

    if (centroidAssignments[gene]!=best){ /* see if the gene's assignment has changed */
      change=1;                         /* record it if it has */
      centroidAssignments[gene]=best;	
    }
    
  }
  
  return change;
  
} 

/*
 * Function : CalculateCentroids
 * Usage :
 * ----------------------------------------------------------------------------------------
 * Given the centroids to which each gene has been assigned, and the data for those genes,
 * this function calculates new centroids.
 */

void CalculateCentroids(float *dataMatrix, clusterRec *cluster, int numExperiments, int k, int *centroidAssignments){
  
  int centroid;
  int gene;
  int i;
  float *total;
  int *numPoints;
  int matrixOffset;
  
  matrixOffset=cluster->numGenes*numExperiments;
  
  total=malloc(numExperiments*sizeof(float));
  if (total==NULL) Error ("Not enough memory for total\n");
  
  numPoints=malloc(numExperiments*sizeof(int));
  if (numPoints==NULL) Error("Not enough room for numPoints\n");
  
  
  for (centroid=0; centroid<k; centroid++){ /* look at each centroid */
    
    /* initialize the arrays keeping track of the data contributing to the new centroids */
    
    for (i=0; i<numExperiments; i++){
      total[i]=0;
      numPoints[i]=0;
    }
    
    for(gene=0; gene<cluster->numGenes; gene++){ /* compare it to each gene's assignment */
      
      if (centroidAssignments[gene]==centroid){ /* if they're the same */
	
	for (i=0; i<numExperiments; i++){ /* add in the data */
	  
	  if (dataMatrix[gene*numExperiments+i]!=NODATA){
	    
	    total[i]+=dataMatrix[gene*numExperiments+i];
	    numPoints[i]++;
	    
	  }
	  
	}
	
      }
      
    }
    
    /* now calculate new centroid data */
    
    for (i=0; i<numExperiments; i++){
      
      if (numPoints[i]){
	
	dataMatrix[matrixOffset+centroid*numExperiments+i]=total[i]/numPoints[i];
	
      }else{ /* pretty unlikely, but nevertheless */
	
	dataMatrix[matrixOffset+centroid*numExperiments+i]=NODATA;
	
      }
      
    }
    
  }
  
  free(total);
  free(numPoints);	
  
}

/*
 * Function : OrganizeKClusters
 * Usage : 
 * -------------------------------------------------------
 * This function takes the data that indicates to which centroid
 * each gene is most similar to, to set-up structures to send to 
 * the HierarchicallyCluster function.
 * Note this is almost identical to the code in PrintSOMOrganization,
 * and should be effectively consolidated.
 */
 
void OrganizeKClusters(int *centroidAssignments, int *numInCentroid, int numExperiments, char **experimentNames, 
		       clusterRec *cluster, int numGenes, float *dataMatrix, char *ifile, int k, float *eWeights){
  
  FILE *current;
  int centroid;
  float *tempMatrix;
  clusterRec tempCluster;
  int tempMatrixWidth;
  int gene;
  char** fileNames;
  char *cdtFileName;
  char *gtrFileName;
  char *atrFileName;
  
  fileNames = malloc(k*sizeof(char*));
  if (fileNames==NULL) Error("Not enough memory for filenames");
  
  MakeKMeansFileNames(fileNames, ifile, k);
  
  for (centroid=0; centroid<k; centroid++){
    
    if(numInCentroid[centroid]>2){ /* only need to do if have more than 2 genes in a SOM */
      
      AllocateTempMemory(numInCentroid[centroid], numExperiments, &tempMatrix, &tempMatrixWidth, &tempCluster);
      
      PopulateTempMemory(centroid, numExperiments, tempMatrixWidth, &tempCluster, tempMatrix, cluster, centroidAssignments, dataMatrix);
      
      HierarchicallyCluster(fileNames[centroid], numExperiments, &tempCluster, tempMatrix, eWeights, 
			    numInCentroid[centroid], tempMatrixWidth, experimentNames);
      
      free(tempMatrix);
      FreeCluster(&tempCluster);
      
    }else{
      
      MakeFileNames(fileNames[centroid], &gtrFileName, &cdtFileName, &atrFileName);
      
      current=OpenOutFile(cdtFileName);
      
      PrintFirstLine(current, numExperiments, experimentNames);
      
      if (numInCentroid[centroid]){
	
	for (gene=0; gene<numGenes; gene++){
	  
	  if (centroidAssignments[gene]==centroid){
	    
	    PrintOneLine(cluster, gene, current, dataMatrix, numExperiments);
	    
	  }
	  
	}
	
      }
      
      fclose(current);
      
      free (cdtFileName);
      free (atrFileName);
      free (gtrFileName);					
      
    }
    
  }
  
  for(centroid=0; centroid<k; centroid++){
    free(fileNames[centroid]);
  }
  free(fileNames);
  
}
 
 /*
  * Function : MakeKMeansFileNames
  * Usage :
  * ------------------------------
  * This function makes the filenames used for the clustered k-means files
  */
  
void MakeKMeansFileNames(char **fileNames, char *ifile, int k){
 
  char* prefix;
  int i;
  char num[20];
  
  prefix=GetFilePrefix(ifile);
  
  for (i=0; i<k; i++){
    
    sprintf(num, "_%d_k%d.txt", k, i);
    
    fileNames[i]=malloc((strlen(prefix)+strlen(num)+1)*sizeof(char));
    
    if (fileNames[i]==NULL) Error("Not enough memory for fileNames[%d]\n", i+1);
    
    strcpy(fileNames[i], prefix);
    
    strcpy(fileNames[i]+strlen(prefix)-1, num);
    
  }
  
  free(prefix);
  
}

/*
 * Function : LogTransformData
 * Usage : LogTransformData(dataMatrix, numGenes, numExperiments)
 * --------------------------------------------------------------
 * This function transforms all the data in the dataMatrix into
 * into log base2
 */
 
void LogTransformData(float *dataMatrix, int numGenes, int numExperiments, int matrixWidth){

  int gene;
  int experiment;
  float log2=log(2);
  
  printf("Log Transforming Data\n");
  
  for (experiment=0; experiment<numExperiments; experiment++){
    
    for (gene=0; gene<numGenes; gene++){
      
      if (dataMatrix[gene*matrixWidth+experiment]!=NODATA){
	
	if (dataMatrix[gene*matrixWidth+experiment]<=0){ 
	  
	  Error("There's is a value in gene %d, experiment %d, at or below zero, that can't be logged", gene, experiment);
	  
	}else{
	  
	  dataMatrix[gene*matrixWidth+experiment]=log(dataMatrix[gene*matrixWidth+experiment])/log2;
	  
	}
	
      }
      
    }
    
  }
  
}
