MLSDIR:=$(BASEDIR)/mls

LIBMODULES := anchors common distances getopt initializations normalizations 
MODULES := $(LIBMODULES) diagem graph-matching kmeans kmedians sclust \
	   tsplit xclust

include $(patsubst %,$(MLSDIR)/%/module.mk,$(MODULES))
