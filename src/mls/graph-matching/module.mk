CURDIR := $(MLSDIR)/graph-matching

WMATCHSRC := $(addprefix $(CURDIR)/, main.c wmatch.c glib.c )
WMATCHFULLSRC := $(addprefix $(CURDIR)/, main-full.c wmatch.c glib.c)
SRC += $(WMATCHSRC) $(WMATCHFULLSRC)
CFLAGS += -I$(CURDIR)

WMATCHLIBS = $(BASEDIR)/da/libda$(LIBEXT) \
            $(BASEDIR)/nr/libnr$(LIBEXT) \
	    $(BASEDIR)/ut/libut$(LIBEXT) \
	    $(MLSDIR)/common/libcommon$(LIBEXT) \
	    $(MLSDIR)/normalizations/libnorm$(LIBEXT) \
	    $(MLSDIR)/distances/libdist$(LIBEXT) \
	    $(MLSDIR)/initializations/libinit$(LIBEXT)
WMATCHLIBDIRFLAGS := $(call make_libdirflag,$(WMATCHLIBS))
WMATCHLIBLINKFLAGS := $(call make_liblinkflag,$(WMATCHLIBS)) -lm

WMATCH := $(CURDIR)/wmatch$(BINEXT)
WMATCHFULL := $(CURDIR)/wmatch-full$(BINEXT)
TARGETBINS += $(WMATCH) $(WMATCHFULL)

$(WMATCH): $(WMATCHSRC:.c=$(OBJEXT)) $(LIBS)
	$(CC) $(WMATCHLIBDIRFLAGS) -o $@ $(WMATCHSRC:.c=$(OBJEXT)) $(WMATCHLIBLINKFLAGS)

$(WMATCHFULL): $(WMATCHFULLSRC:.c=$(OBJEXT)) $(LIBS)
	$(CC) $(WMATCHLIBDIRFLAGS) -o $@ $(WMATCHFULLSRC:.c=$(OBJEXT)) $(WMATCHLIBLINKFLAGS)
