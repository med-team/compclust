/**************************************************************

  Title:    matlab.c
  Author:   Becky Castano
  Date:     8/30/00
  Function: 


  Compile:  make matlab
  Usage:    

  Modifications:

***************************************************************/
#include "graphtypes.h"

#include <stdio.h>
#include <mex.h>
#include <stdlib.h>
#include <math.h>


Graph translateMatrix2Graph(int *size, const mxArray *matrix, int *total_weight);


/**
 * Entry point for Matlab function maxFlowAdjacencyMatrix().
 * Serves the same role as main.c.
 *
 * @author Benjamin J. Bornstein
 *         Becky Castano
 */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
  int i      = 0;
  int j      = 0;
  int edges  = 0;
  int score  = 0;
  int size   = 0;
  int weight = 0;
 
  int    *Mate = (int *)    NULL;
  double *ptr  = (double *) NULL;

  Graph graph = (Graph) NULL;
  Edge  p     = (Edge)  NULL;



  if ( nrhs != 1 )
  {
    mexErrMsgTxt("Usage: matchBPAdjacenyMatrix( matrix )\n");
  }


  graph = (Graph) translateMatrix2Graph(&size, prhs[0], &weight);

  /* find highest weighted matching */
  Mate = Weighted_Match(graph, 1, 1);

  /* calculate score */

  size  = Degree  ( graph, 0 );
  edges = NumEdges( graph );

  for (i = 1; i <= size; i++)
  {
    p = FirstEdge(graph, i);

    for (j = 1; j <= Degree(graph, i); ++j)
    {
      if ( EndPoint(p) == Mate[i] )
      {
        score += ELabel(p);
      }

      p = NextEdge(p);
    }
  }

  /* return score, free memory */
  plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
  ptr     = mxGetPr(plhs[0]);
  *ptr    = (double) score / (2.0 * weight);
}


/**
 * Convert a confusion matrix in Matlab array format to a graph.
 *
 * Becky Castano
 * August 30, 2000
 */
Graph translateMatrix2Graph(int *size, const mxArray *matrix, int *total_weight)
{
	int edges    = 0;
  int degree   = 0;
  int vlabel   = 0;
  int elabel   = 0;
  int adj_node = 0;
  int i        = 0;
  int j        = 0;
	int classes1 = 0;
  int classes2 = 0;
	int weight   = 0;

 	char   c    = 0;
	double *ptr = (double *) NULL;

  Graph graph = (Graph) NULL;


#ifdef MEX_DEBUG
  mexPrintf("Debug: Entering translateMatrix2Graph().\n");
#endif


	classes1 = mxGetN(matrix);
  classes2 = mxGetM(matrix);
	ptr      = mxGetPr(matrix);

	*size = classes1 + classes2;  /* number of nodes in graph */
	edges = classes1 * classes2;
       
	graph = NewGraph( *size );

	for (i = 1; i <= *size; ++i)
  {
    // NOTE: Already done in NewGraph().
    // NOTE: Commented out by Ben Bornstein.
    // NOTE: NLabel(graph, i) = i;

    Xcoord(graph, i) = 0;  
    Ycoord(graph, i) = 0;
  }

	*total_weight = 0;

#ifdef MEX_DEBUG
  mexPrintf("Debug: Will call AddEdge() %d times.\n", edges);
#endif

	for (i = 1; i <= classes1; ++i)
  {
    for (j = 1; j <= classes2; ++j)
    {
      weight         = (int) *ptr++;
      *total_weight += weight;

      AddEdge(graph, i, j + classes1, weight);
    }
  }

#ifdef MEX_DEBUG
  mexPrintf("Debug: Done calling AddEdge() %d times.\n", edges);
#endif

#ifdef MEX_DEBUG
  mexPrintf("Debug: Leaving  translateMatrix2Graph().\n");
#endif

	return graph;
}
