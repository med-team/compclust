#include "graphtypes.h"

main(argc,argv)
int argc;
char *argv[];

{
	Graph graph;
	int *Mate;
	int i,size;

	int score = 0;
	int weight;
	int j,edges;
	Edge p;

	if (argc < 2){
		printf("Usage: wmatch confusion_matrix_file \n");
		exit(1);
	}

  /*	graph = ReadGraph(&size,argv[1]);  */
	graph = (Graph) Conf2Graph(&size, argv[1], &weight);  
	Mate  = Weighted_Match(graph, 1, 1);

	/*
	for (i=1; i<=size; i++)
		printf("%d %d\n",i,Mate[i]);
	*/
	/*        WriteGraph(graph);  */

        /* print score */

	size = Degree(graph,0);
	edges = NumEdges(graph);

	for (i = 1; i <= size; i++) {
		p = FirstEdge(graph,i);
		for (j = 1; j <= Degree(graph,i); ++j) {
		        if (EndPoint(p) == Mate[i] )
			    score += ELabel(p);
			p = NextEdge(p);
			}
		}
	/*        printf("score %d weight %d\n",score/2,weight); */
        printf("score %5.4f\n",(float)score/(2.0*weight));
	
}


