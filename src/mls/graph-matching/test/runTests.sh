#!/bin/bash

#
# Runs and reports the results for all matchBPAdjacencyMatrix tests.
# Test are driven by the existence of input and output files:
#
#   test-NUM.in
#   test-NUM.out
#
# Where NUM is the test number.
#

COMMAND="../wmatch";
NUM=1;


while [ -r test-${NUM}.in ]; do
  echo -ne "Running test ${NUM} ... \t";

  ${COMMAND} test-${NUM}.in > test-${NUM}.tmp;
  diff test-${NUM}.out test-${NUM}.tmp > /dev/null;

  if [ $? -eq 0 ]; then
    echo "[ OK ]";
  else
    echo "[ FAILED ]";
  fi

  /bin/rm -rf test-${NUM}.tmp;

  NUM=$[ ${NUM} + 1 ];
done

