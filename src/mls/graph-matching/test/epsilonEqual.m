function result = epsilonEqual(X, Y, epsilon)
% epsilonEqual    Compares two matrices for equality within some epsilon.
%     result = equal(X, Y, epsilon) compares X and Y element by
%     element.  If each element in X is within epsilon of Y, result is 
%     1, otherwise 0.
%

%
% Copyright 2000, California Institute of Technology.
% ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
%

  if (nargin == 2)
    epsilon = eps;
  end
  
  result = sum(X - Y) < epsilon;
  return;
