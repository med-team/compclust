function result = readResult(filename)
% readResult    Reads and returns the wmatch result from ASCII file filename.
%     result = readResult(filename) reads and returns the wmatch result
%     from ASCII file filename, where the first line of the file
%     contains the result in the following format:
%
%       score %f
%

%
% Copyright 2000, California Institute of Technology.
% ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
%

  result     = [];
  fileHandle = fopen(filename);
  

  if (fileHandle == -1)
    error( sprintf('Could not open file "%s" for reading.', filename) );
  end

  currentLine = fgetl( fileHandle );

  currentLine = deblank( currentLine );
  currentLine = strrep ( currentLine, 'score', '' );

  result = str2num( currentLine );
  
  return;
