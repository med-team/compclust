function exists = fileExists(filename)
% fileExits    Returns 1 if filename exits, 0 otherwise.
%     fileExists(filename) returns 1 if filename exits, 0 otherwise.
%

%
% Copyright 2000, California Institute of Technology.
% ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
%

  file   = dir ( filename );
  exists = size( file, 1  );

  if (exists ~= 0)
    exists = 1;
  end

  return;
