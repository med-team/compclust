function runTests()
% runTests    Runs all matchBPAdjacencyMatrix tests.
%     runTests()  Runs and reports the results for all
%     matchBPAdjacencyMatrix tests.  Test are driven by the
%     existence of input and output files:
%
%       test-NUM.in
%       test-NUM.out
%
%     Where NUM is the test number.
%

%
% Copyright 2000, California Institute of Technology.
% ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
%

  num           = 1;
  inputFilename = strcat( 'test-', num2str(num), '.in' );

  while ( fileExists(inputFilename) )
    outputFilename = strcat( 'test-', num2str(num), '.out' );
    
    matrix         = readMatrix( inputFilename  );
    expectedResult = readResult( outputFilename );
    actualResult   = matchBPAdjacencyMatrix( matrix );

    if ( epsilonEqual(expectedResult, actualResult) )
      disp( sprintf('Running test %d ... \t[ OK ]',     num) );
    else
      disp( sprintf('Running test %d ... \t[ FAILED ]', num) );
    end

    num           = num + 1;
    inputFilename = strcat( 'test-', num2str(num), '.in' );
  end

  return;
