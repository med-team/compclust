function matrix = readMatrix(filename)
% readMatrix    Reads and returns a matrix from ASCII file filename.
%      matrix = readMatrix(filename) reads and returns matrix from ASCII
%      file filename, where the first line of the file contains the
%      dimensions of the matrix in row, column order.
%
%      A file containing the 3-by-3 identity matrix would look like:
%
%        3 3
%        1 0 0
%        0 1 0
%        0 0 1
%

%
% Copyright 2000, California Institute of Technology.
% ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
%

  matrix     = [];
  fileHandle = fopen(filename);
  

  if (fileHandle == -1)
    error( sprintf('Could not open file "%s" for reading.', filename) );
  end
  

  currentLine = fgetl( fileHandle );
  
  dimensions = str2num( currentLine );
  numRows    = dimensions(1);


  for currentRow = 1:numRows

    if ( feof(fileHandle) )
      error( 'Encountered premature end-of-file (EOF).');
    end
    
    currentLine = fgetl( fileHandle );
    currentLine = deblank( currentLine );

    statement = sprintf( 'matrix(%d, :) = [ %s ];', currentRow, currentLine );
    eval(statement);
  end
  
  fclose( fileHandle );
  
  return;
