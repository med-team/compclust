/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * @author Ben Bornstein
 */

#ifndef __KMEANS_H
#define __KMEANS_H


#define DEBUG_KMEANS 0

struct kmeans_data
{
  int         datapoints;
  int         features;
  int         classes;
  int         *classifications;
  int         *class_count_array;
  double      **data;
  double      **class_means;
  double      *feature_max;
  double      *feature_min;
  long        *seed;
};


struct kmeans_parameters
{
  int  distance_metric;
  int  init_means;
  int  k;
  int  k_strict;
  int  max_iterations;
  int  max_restarts;
  int  num_mean_samples;
  int  cols;
  int  rows;
  long seed;
  char *dataset_filename;
  char *result_filename;
  char *means_filename;
};


/**
 * Safely allocates (via safe_malloc()) memory for a kmeans_data structure.
 * The resulting structure is initialized with values from the given
 * kmeans_parameters structure. A pointer to the structure is returned.
 *
 * @see safe_malloc
 */
struct kmeans_data *allocate_kmeans_data(struct kmeans_parameters *p);

/**
 * Safely allocates (via safe_malloc()) memory for a kmeans_parameters
 * structure.  The resulting structure is initialized with default values.
 * A pointer to the structure is returned.
 *
 * @see safe_malloc
 */
struct kmeans_parameters *allocate_kmeans_parameters(void);

/**
 * Returns 1 if the N element array contains value at least once, 0
 * otherwise.
 */
int array_contains(int *array, int N, int value);

/**
 * Classifies each datum from 1 to k according to the nearest mean.
 *
 * Where:
 *   k           = d->classes
 *   means       = d->class_means
 *   memberships = d->classifications
 *   datum       = d->data[i][]
 *   i           = [0 ... (d->datapoints - 1)]
 *   j           = [0 ... (d->features   - 1)]
 */
void classify_data(struct kmeans_data *d, struct kmeans_parameters *p);

/**
 * Compares the contents of the classification field of the kmeans_data
 * structure to classifications.  If they are equal by content zero
 * is returned.  Otherwise, the number of elements by which they differ
 * is returned.
 */
int compare_classifications(struct kmeans_data *d, int *classifications);

/**
 * Copies the contents of the classification field of the kmeans_data
 * structure to destination.
 */
void copy_classifications(struct kmeans_data *d, int *destination);

/**
 * Copies the contents of source to destination,  both with N elements.
 */
void copy_datum(double *source, double *destination, int N);

/**
 * Computes the distance between the two given datapoints, both with N
 * elements.
 *
 * The distance is the square of the Euclidean distance.  Since, in k-means,
 * distance is used only for comparison purposes, this is correct (and
 * faster than computing the true Euclidean distance).  That is, for
 * a, b > 0, a > b, implies sqrt(a) > sqrt(b).
 */
/*double distance(double *datum1, double *datum2, int N);*/

/**
 * Estimates the means of k classes using the class membership of each
 * datum.
 *
 * Where:
 *   k           = d->classes
 *   memberships = d->classifications
 *   datum       = d->data[i][]
 *   i           = [0 ... (d->datapoints - 1)]
 *   j           = [0 ... (d->features   - 1)]
 */
void estimate_means(struct kmeans_data *d);

/**
 * Computes the centroid of the data and places the result in center.
 *
 * Where:
 *   data   = d->data[i][j]
 *   i      = [0 ... (d->datapoints - 1)]
 *   j      = [0 ... (d->features   - 1)]
 *
 *   center   must be a pointer to an array of doubles with d->features
 *            number of elements.
 *
 * @see init_means_church
 */
void find_centroid(struct kmeans_data *d, double *centroid);

/**
 * Returns the sum of all distances of datum to the first N means.
 *
 * Where:
 *   means = d->class_means[i][j]
 *   i     = [0 ... (N           - 1)]
 *   j     = [0 ... (d->features - 1)]
 *
 *   datum   must be a pointer to an array of doubles with d->features
 *           number of elements.
 *
 * @see init_means_church
 */
double find_distance_to_means(double *datum, struct kmeans_data *d, int N);

/**
 * Returns the index of the datum farthest from the first N means.  Indices
 * contains the indices of the N datum already chosen as means, to avoid
 * choosing them again.
 *
 *
 * Where:
 *   data  = d->data[i][j]
 *   means = d->class_means[i][j]
 *   i     = [0 ... (d->datapoints - 1)]
 *   j     = [0 ... (d->features   - 1)]
 *
 *
 * @see init_means_church
 */
int find_farthest_datum_from_means(struct kmeans_data *d, int *indices, int N);

/**
 * Finds the minimum and maximum values of the data (d->data) along
 * each dimension.
 *
 * Where:
 *   data = d->data[i][j]
 *   i    = [0 ... (d->datapoints - 1)]
 *   j    = [0 ... (d->features   - 1)]
 *
 *   min    must be a pointer to an array of doubles with d->features
 *          number of elements.
 *
 *   max    must be a pointer to an array of doubles with d->features
 *          number of elements.
 *
 * @see init_means_random kmeans
 *
 * void find_min_max(struct kmeans_data *d, double *min, double *max);
 */

/**
 * Frees the memory associated with the given kmeans_data structure.
 */
void free_kmeans_data(struct kmeans_data *d);

/**
 * Frees the memory associated with the given kmeans_parameters structure.
 */
void free_kmeans_parameters(struct kmeans_parameters *p);

/**
 * Returns the number of empty classes in d->class_count_array.
 */
int get_num_empty_classes(struct kmeans_data *d);

/**
 * Initializes the classification field of the kmeans_data structure.
 * For each datum, a classification is chosen at random.
 *
 * @see init_means_random, kmeans
 */
void init_classifications_random(struct kmeans_data *d);

/**
 * Initializes the the means prior to classification, either
 * placing them randomly, or using Church style initialization,
 * depending on the value of p->init_means.
 *
 * @see init_means_random init_means_church
 */
/*void init_means(struct kmeans_data *d, struct kmeans_parameters *p);*/

/**
 * Initializes the means prior to classification using Church style
 * initialization.
 *
 * According to page 285 of the following paper:
 *
 *   Systematic Determination of Genetic Network Architecture
 *   Tavozoie, Hughes, Campbell, Cho, and Church
 *   Nature Genetics, Volume 22, July 1999, pp. 281--285
 *
 *   "We used an implementation of k-means in the statistical software
 *    package SYSSTAT 7.0 (SPSS).  The first cluster centre was chosen as
 *    the centroid of the entire data set and subsequent centres were chosen
 *    by finding the data point farthest from the centres already chosen."
 *
 * @see init_means
 */
void init_means_church(struct kmeans_data *d, struct kmeans_parameters *p);

/**
 * Initializes the means prior to classification, by placing them
 * randomly throughout the data.
 */
void init_means_random(struct kmeans_data *d, struct kmeans_parameters *p);

/**
 * Initializes the means prior to classification by placing them randomly
 * throughout the data.  The variables p->feature_min and p->feature_max
 * must contain the minimum and maximum values of the data along each
 * dimension.  This is important to limit the range of the means.
 *
 * @see find_min_max
 */
void
init_means_random_range(struct kmeans_data *d, struct kmeans_parameters *p);

/**
 * Initializes the means prior to classification by sampling
 * p->num_mean_samples datapoints (without replacement) for each class and
 * using those samples to estimate the initial class means.
 */
void
init_means_random_sample(struct kmeans_data *d, struct kmeans_parameters *p);

/**
 * Performs kmeans clustering on the data field of the kmeans_data structure.
 *
 * If p->k_strict is 1, kmeans attempts to find only k clusters.
 *
 * Otherwise, if p->k_strict is 0 and k clusters could not be found, k - 1
 * clusters are tried.  This process continues until either a clustering
 * with no collapsed clusters is achieved, or k is 1.  In this case,
 * d->classes can be thought of as an upper-bound (maximum) on k.  When
 * done, d->classes will contain the final k found.
 *
 * Returns the number of empty classes if p->k_strict == 1, zero otherwise.
 *
 * Where:
 *   k     = d->classes
 *   datum = d->data[i][j]
 *   i     = [0 ... (d->datapoints - 1)]
 *   j     = [0 ... (d->features   - 1)]
 *
 * @see kmeans_internal
 */
int kmeans(struct kmeans_data *d, struct kmeans_parameters *p);

/**
 * Performs kmeans clustering on the data field of the kmeans_data structure.
 *
 * Returns the number of empty classes.  Zero indicates complete success,
 * since no collapsed clusters occurred.
 *
 * NOTE: This function is internal to kmeans.c and should only be called by
 * NOTE: kmeans().
 *
 * Where:
 *   k     = d->classes
 *   datum = d->data[i][j]
 *   i     = [0 ... (d->datapoints - 1)]
 *   j     = [0 ... (d->features   - 1)]
 *
 * @see kmeans
 */
int kmeans_internal(struct kmeans_data *d, struct kmeans_parameters *p);

/**
 * Prints the data vectors in the kmeans_data structure to the given stream.
 */
void print_data(FILE *stream, struct kmeans_data *d);

/**
 * Pretty-prints the values of given kmeans_parameters structure to
 * the given stream.
 */
void print_kmeans_parameters(FILE *stream, struct kmeans_parameters *p);

/**
 * Prints the mean vectors for each class in the kmeans_data structure to
 * the given stream.
 */
void print_means(FILE *stream, struct kmeans_data *d);

/**
 * Updates the P_j field of the kmeans_data structure.
 *
 * void update_p_of_j(struct kmeans_data *d);
 */

/**
 * Updates the P_j_x field of the kmeans_data structure.
 *
 * void update_p_of_j_given_x(struct kmeans_data *d);
 */

/**
 * Updates the P_j and P_j_x fields of the kmeans_data structure.
 *
 * void update_probabilities(struct kmeans_data *d);
 */

/**
 * Zeros the classification field of the kmeans_data structure.
 */
void zero_classifications(struct kmeans_data *d);

/**
 * Zeros the class_count_array field of the kmeans_data structure.
 */
void zero_class_counts(struct kmeans_data *d);

/**
 * Zeros the class_means field of the kmeans_data structure.
 */
void zero_class_means(struct kmeans_data *d);


#endif
