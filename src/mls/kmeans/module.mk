CURDIR := $(MLSDIR)/kmeans

KMEANSRC := $(addprefix $(CURDIR)/, kmeans.c matrix.c random.c util.c main.c)
SRC += $(KMEANSRC)
CFLAGS += -I$(CURDIR)

KMEANLIBS = $(BASEDIR)/da/libda$(LIBEXT) \
            $(BASEDIR)/nr/libnr$(LIBEXT) \
	    $(BASEDIR)/ut/libut$(LIBEXT) \
	    $(MLSDIR)/common/libcommon$(LIBEXT) \
	    $(MLSDIR)/normalizations/libnorm$(LIBEXT) \
	    $(MLSDIR)/distances/libdist$(LIBEXT) \
	    $(MLSDIR)/initializations/libinit$(LIBEXT) \
            $(MLSDIR)/getopt/libgetopt$(LIBEXT)
KMEANLIBDIRFLAGS := $(call make_libdirflag,$(KMEANLIBS))
KMEANLIBLINKFLAGS := $(call make_liblinkflag,$(KMEANLIBS)) -lm

KMEANS := $(CURDIR)/kmeans$(BINEXT)
TARGETBINS += $(KMEANS)

$(KMEANS): $(KMEANSRC:.c=$(OBJEXT)) $(KMEANLIBS)
	$(CC) $(KMEANLIBDIRFLAGS) -o $@ $(KMEANSRC:.c=$(OBJEXT)) $(KMEANLIBLINKFLAGS)
