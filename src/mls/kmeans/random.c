/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 */

/* Andres and Becky 5/26/96 */
/* these are from numerical recipes in C */

/*   modified 10-11-96 to include routines for parallel use */
/*      this modification involves eliminating static variables */
/*      and making them parameters passed to the function */

/*   modified 4-26-97   added function seed()         */
/*                      moved constant definitions to random.h */
/*   modified 5-26-97   fixed gammadev_pll uses Numerical Recipes      */
/*                       if alpha is an integer otherwise              */
/*                       uses a weighted bootstrap (not very efficient) */ 

#include <stdio.h>
#include <stdlib.h>
#include "random.h"
#include <math.h>
#include <time.h>   /* for seed()*/

/*
#define	IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define NTAB 32
#define NDIV (1+(IM-1)/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)
*/

#ifndef PI
#define PI 3.14159265358979323846	/* for poison distribution */
#endif

float
gammln(float xx)
/*
 * Returns the value ln(Gamma(xx)) for xx>0
 * NR pg. 214
 */
{
	double x,y,tmp,ser;
	static double cof[6]={76.18009172947146,-86.50532032941677,
		24.01409824083091,-1.231739572450155,
		0.1208650973866179e-2,-0.5395239384953e-5};
	int j;

	y=x=xx;
	tmp=x+5.5;
	tmp -= (x+0.5)*log(tmp);
	ser=1.000000000190015;
	for (j=0;j<=5;j++)
		ser += cof[j]/++y;
	return -tmp+log(2.5066282746310005*ser/x);
}

float
ran1(long *idum)
/*
 this random number generator was copied from numerical recipies in C,
 chap7, pg 280.  This is the Park and Miller generator with Bays-Durham shuffle.
 Returns a uniform random deviate between 0.0 and 1.0 (exclusive the endpoint 
 values).  First call ran1 with a neg number to initialize it.  After that,
 don't change the value to idum. e.g.,
 
 period ~= 10^8

	declare a long global:	long seed = -1;
	inialize the generatr:	random1(&seed);
	get a random number:	a = random1(&seed);

	DO NOT CHANGE THE 'SEED' DURING CALLS OF THE SAME SEQUENCE
 */
{
	int j;
	long k;
	static long iy=0;
	static long iv[NTAB];
	float temp;

	if (*idum <= 0 || !iy)
	{
		if ( -(*idum) < 1)
			*idum=1;
		else
			*idum = -(*idum);
		for (j=NTAB+7; j>=0; j--)
		{
			k=(*idum)/IQ;
			*idum=IA*(*idum-k*IQ)-IR*k;
			if (*idum < 0)
				*idum += IM;
			if (j < NTAB)
	 			iv[j] = *idum;
		}
		iy=iv[0];
	}

	k=(*idum)/IQ;
	*idum=IA*(*idum-k*IQ)-IR*k;
	if (*idum < 0)
		*idum += IM;
	j = iy/NDIV;
	iy = iv[j];
	iv[j] = *idum;
	if ((temp=AM*iy)>RNMX)
		return RNMX;
	else
		return temp;
}

float
expdev(long *idum)
/* 
 * Returns an exponentially distributed, positive random deviate
 * of unit mean, using ran1(idum) as the source of uniform deviates
 * NR, pg. 287
 */
{
	float dum;

	do
		dum=ran1(idum);
	while (dum == 0.0);
		return -log(dum);
}

float
gasdev(long *idum)
/* 
 * Returns a normally distributed deviate with 0 mean and unit
 * variance, using ran1(idum) as the source of uniform deviates.
 * NR, pg. 289
 */
{
	static int iset=0;
	static float gset;
	float fac,rsq,v1,v2;

	if  (iset == 0)
	{
		do
		{
			v1=2.0*ran1(idum)-1.0;
			v2=2.0*ran1(idum)-1.0;
			rsq=v1*v1+v2*v2;
		} while (rsq >= 1.0 || rsq == 0.0);
		fac=sqrt(-2.0*log(rsq)/rsq);
		gset=v1*fac;
		iset=1;
		return v2*fac;
	}
	else
	{
		iset=0;
		return gset;
	}
}

float
gamdev(int ia, long *idum)
/* 
 * Returns a deviate distributed as a gamma distribution of integer
 * order ia, i.e., a waiting time to the ia event in a poisson 
 * process of unit mean, using ran1(idum) as the source of uniform
 * deviates
 * NR, pg. 292
 */
{
    int j;
    float am,e,s,v1,v2,x,y;

    if (ia < 1)
    {
	fprintf(stderr,"Error in gamdev: ia = %d < 1\n", ia);
	exit(-1);
    }
    if (ia < 6)
    {
	x=1.0;
	for (j=1;j<=ia;j++) x *= ran1(idum);
	    x = -log(x);
    }
    else
    {
	do
	{
	    do
	    {
		do
		{
		    v1=2.0*ran1(idum)-1.0;
		    v2=2.0*ran1(idum)-1.0;
		} while (v1*v1+v2*v2 > 1.0);
		y=v2/v1;
		am=ia-1;
		s=sqrt(2.0*am+1.0);
		x=s*y+am;
	    } while (x <= 0.0);
	    e=(1.0+y*y)*exp(am*log(x/am)-s*y);
	} while (ran1(idum) > e);
    }
    return x;
}

float
poidev(float xm, long *idum)
/* 
 * Returns as a floating point number an integer value that is a
 * random deviate drawn from a poisson distribution of mean xm
 * using ran1(idum) as a source of uniform random deviates
 * NR, pg. 294
 */
{
	static float sq,alxm,g,oldm=(-1.0);
	float em,t,y;

	if (xm < 12.0)
	{
		if (xm != oldm)
		{
			oldm=xm;
			g=exp(-xm);
		}
		em = -1;
		t=1.0;
		do
		{
			++em;
			t *= ran1(idum);
		} while (t > g);
	}
	else
	{
		if (xm != oldm)
		{
			oldm=xm;
			sq=sqrt(2.0*xm);
			alxm=log(xm);
			g=xm*alxm-gammln(xm+1.0);
		}
		do
		{
			do
			{
				y=tan(PI*ran1(idum));
				em=sq*y+xm;
			} while (em < 0.0);
			em=floor(em);
			t=0.9*(1.0+y*y)*exp(em*alxm-gammln(em+1.0)-g);
		} while (ran1(idum) > t);
	}
	return em;
}

float
bnldev(float pp, int n, long *idum)
/*
 * Returns as a floating point number an integer value that is a
 * random deviate drawn from a binomial distribution of n trials
 * each of probability pp, using ran1(idum) as a source of uniform
 * deviates
 * NR, pg 295
 */
{
	int j;
	static int nold=(-1);
	float am,em,g,angle,p,bnl,sq,t,y;
	static float pold=(-1.0),pc,plog,pclog,en,oldg;

	p=(pp <= 0.5 ? pp : 1.0-pp);
	am=n*p;
	if (n < 25)
	{
		bnl=0.0;
		for (j=1;j<=n;j++)
			if (ran1(idum) < p)
				++bnl;
	}
	else
		if (am < 1.0)
		{
			g=exp(-am);
			t=1.0;
			for (j=0;j<=n;j++)
			{
				t *= ran1(idum);
				if (t < g) break;
			}
			bnl=(j <= n ? j : n);
		}
		else
		{
			if (n != nold)
			{
				en=n;
				oldg=gammln(en+1.0);
				nold=n;
			}
			if (p != pold)
			{
				pc=1.0-p;
				plog=log(p);
				pclog=log(pc);
				pold=p;
			}
			sq=sqrt(2.0*am*pc);
			do
			{
				do
				{
					angle=PI*ran1(idum);
					y=tan(angle);
					em=sq*y+am;
				} while (em < 0.0 || em >= (en+1.0));
				em=floor(em);
				t=1.2*sq*(1.0+y*y)*exp(oldg-gammln(em+1.0)
					-gammln(en-em+1.0)+em*plog+(en-em)*pclog);
			} while (ran1(idum) > t);
			bnl=em;
		}
	if (p != pp)
		bnl=n-bnl;
	return bnl;
}

int
iran(int min, int max, long *idum)
/* 
 * Returns a random integer between between min and max (inclusive) using
 * ran1(idum) as the source of uniform deviates
 * usage:
 *     	long    seed = -1  (or any negative number);
 *   	a = irandom(&seed,2,6); (returns a random int between 2,3,4,5,6)
 */
{
	if ( max < min)
	{
		fprintf(stderr,"Error in iran boundaries (random.c): max (%d) < min (%d)\n", max, min);
		exit(-1);
	}
	return (int)(ran1(&(*idum))*(max-min+1)) + min;
}


/**
 * Returns a random double between min and max (inclusive) using
 * ran1(idum) as the source of the uniform deviates.
 *
 * @see iran, ran1
 *
 * @author Ben Bornstein
 * @date   Wednesday, January 24, 2001, 8:46a
 */
double
dran(double min, double max, long *idum)
{
  if (max < min)
  {
    fprintf( stderr,
             "Error in dran boundries (random.c): max (%f) < mix (%f)\n",
             max,
             min );
  }

  return (double) ( ran1(idum) * (max - min + 1) ) + min;
}


/********************************************************************
Random number generators for use in parallelized (shared memory) code
*********************************************************************/

float
gammln_pll(float xx)
/*
 * Returns the value ln(Gamma(xx)) for xx>0
 * NR pg. 214
 */
{
	double x,y,tmp,ser;
	double cof[6]={76.18009172947146,-86.50532032941677,
		24.01409824083091,-1.231739572450155,
		0.1208650973866179e-2,-0.5395239384953e-5};
	int j;

	y=x=xx;
	tmp=x+5.5;
	tmp -= (x+0.5)*log(tmp);
	ser=1.000000000190015;
	for (j=0;j<=5;j++)
		ser += cof[j]/++y;
	return -tmp+log(2.5066282746310005*ser/x);
}

/****************************************************
*  Function:    ran1_pll
*  Purpose:     To generate a uniform random deviate 
*               between 0 and 1.
*               This random number generator was copied from numerical 
*		recipies in C, Chp. 7, pg 280.  This is the Park and Miller 
*		generator with Bays-Durham shuffle.
* 		Returns a uniform random deviate between 0.0 and 1.0 
*		(exclusive the endpoint values).  
*		First call ran1_pll with a neg number to initialize it.  
*		After that, don't change the value of idum. 
*		The value of iy should be initialized to zero
* 		before calling and then never changed from the 
*		calling program.  Never change the value of iv
*		from the calling program.
*               These were supposed to be static, but
* 		had to be changed to account for shared memory
* 		functionality.
*  Usage:       
*		declare a long global:	long seed = -1;
*		declare as long :	long iy = 0;
*		declare as long :	long iv[NTAB] ;
*		inialize the generator:	ran1_pll(&seed,&iy,iv);
*		get a random number:	a = ran1_pll(&seed,&iy,iv);
*
*  Parameters:  long *idum
*               long *iy
*               long iv[NTAB] 
*  Returns:     random float between 0 and 1 (exclusive of endpoints)
*  Calls:       none
*  Called By:	expdev()
*		gasdev()
*		gamdev()
*		poidev()
*		bnldev()
*		iran()
*****************************************************/

float
ran1_pll(long *idum,long *iy,long *iv)
{
    int j;
    long k;
    float temp;

    if (*idum <= 0 || !(*iy))
    {
	if ( -(*idum) < 1)
	    *idum=1;
	else
	    *idum = -(*idum);
	for (j=NTAB+7; j>=0; j--)
	{
	    k=(*idum)/IQ;
	    *idum=IA*(*idum-k*IQ)-IR*k;
	    if (*idum < 0)
		*idum += IM;
	    if (j < NTAB)
	 	iv[j] = *idum;
	}
	    *iy=iv[0];
    }

    k=(*idum)/IQ;
    *idum=IA*(*idum-k*IQ)-IR*k;
    if (*idum < 0)
	*idum += IM;
    j = *iy/NDIV;
    *iy = iv[j];
    iv[j] = *idum;
    if ((temp=AM*(*iy))>RNMX)
	return RNMX;
    else
	return temp;
}

float
expdev_pll(long *idum, long *iy, long *iv)
/* 
 * Returns an exponentially distributed, positive random deviate
 * of unit mean, using ran1_pll(idum,iy,iv) as the source of uniform deviates
 * NR, pg. 287
 */
{
    float dum;

    do
	dum=ran1_pll(idum,iy,iv);
    while (dum == 0.0);

    return -log(dum);
}

/****************************************************
*  Function:    gasdev_pll
*  Purpose:     To generate a random deviate from
*               a Gaussian distribution:      N(0,1).  
* 		Returns a normally distributed deviate with 0 mean and unit
* 		variance, using ran1_pll() as the source of uniform deviates.
* 		NR, pg. 289.
*  Usage:       before calling: long  seed = -1 (or any negative number); 
*               		long  iy = 0;
*               		int   iset = 0;
*               call:   gasdev_pll()
*  Parameters:  idum, iy, iv        for use by ran1_pll 
*               iset, gset          supposed to be static in gasdev 
*  Returns:     Deviate distributed as N(0,1) 
*  Calls:       ran1_pll
*  Called By:
*****************************************************/

float
gasdev_pll(long *idum, long *iy, long *iv, int *iset, float *gset)

{
    float fac,rsq,v1,v2;

    if  (*iset == 0)
    {
	do
	{
	    v1=2.0*ran1_pll(idum,iy,iv)-1.0;
	    v2=2.0*ran1_pll(idum,iy,iv)-1.0;
	    rsq=v1*v1+v2*v2;
	} while (rsq >= 1.0 || rsq == 0.0);
	fac=sqrt(-2.0*log(rsq)/rsq);
	*gset=v1*fac;
	*iset=1;
	return v2*fac;
    }
    else
    {
	*iset=0;
	return *gset;
    }
}

/****************************************************
*  Function:    gammadev
*  Purpose:     To generate a random deviate from
*               a gamma distribution:      gamma(a,1).  
*         
*  Notes:       If X ~ gamma(a,b) then cX ~ gamma(a,cb) (for c > 0).
*               gamma(1, theta) is an exponential distribution.
*
*               Algorithm from: Non-Uniform Random Variate Generation 
*                   by Luc Devroye  pg. 410.
*               
*  Usage:       long  seed = -1 (or any negative number); 
*               long  iy;
*               long  iv[NTAB];
*               gammadev_pll( a, &idum, &iy, iv );
*  Parameters:  a      need not be an integer as in gamdev_pll()
*               
*               idum , iy, iv
*  Returns:     Deviate distributed as gamma(a,1) 
*  Calls:       ran1_pll, gamdev_pll
*  Called By:
*****************************************************/

/*  from web page 
*            http://wwwmath.uni-muenster.de/math/inst/info/u/holger/sather-mirror/Documentation/Library/LibraryBrowser/lined-rnd.sa.gen.html#Line164
*   gamma(a:FLTD):FLTD pre a > 0.0d is
*      -- Samples from the Gamma distribution: `p(x)=(x^(a-1)e^(-x))/G(a)'.
*      -- Mean is `a>0'. Uses Cheng's algorithm from Devroye, "Non-Uniform
*      -- Random Variate Generation", Springer-Verlag, (1986) p. 413.
*      -- This is much less restrictive than the Numerical Recipes algorithm.
*      res: FLTD;
*      b:FLTD:=a-(4.0d).log;
*      c:FLTD:=a+(2.0d*a-1.0d).sqrt;
*      loop
*         u:FLTD:=uniform; v:FLTD:=uniform;
*         if v=0.0d or v=1.0d then v:=0.5d end;
*         y:FLTD:=a*(v/(1.0d-v)).log;
*         res:=a*(v).exp;
*         z:FLTD:=u*v*v;
*         r:FLTD:=b+c*y-res;
*         if r>=(9.0d/2.0d)*z-(1.0d+(9.0d/2.0d).log) then break! end;
*         if r>=(z).log then break! end;
*      end; -- loop
*      return res;
*   end; -- gamma
*/

float
gammadev_pll(float a, long *idum, long *iy, long *iv)
{
    float c;
    /* float b,v,w; */
    float d,e;
    float u,x,y,z;
    /* int   accept; */

    int i;
    double sum = 0.0;
    double boot[100][2];  /* boot[][0] is the sample point*/
	 /* boot[][1] is the weight for each sample*/
    double gam_f, gam_g;
    void quickSortD( double a[100][2], int min, int max );

    /* float res, r; */

    if (a < 1.0)
    {
	fprintf(stderr,"Problem in gammadev_pll:  a = %.4f\n",a);
	exit(-1);
    }
    else if (a < 1.0)    /* this doesn't work correctly */
                         /* from pg. 415 of 
			     Non-uniform Random Variate Generation */
    {
	c = 1.0/a;
	d = pow(a,a/(1-a))*(1-a);

	do
	{
	    z = expdev_pll(idum,iy,iv);
	    e = expdev_pll(idum,iy,iv);
	    x = pow(z,c);

	}  while (z+e > d+x);
    }
    else
    {
	/*   based on web page (didn't work) */
	/*
	b = a - log(4.0);
	c = a + sqrt(2*a - 1);

	do {
	    u = ran1_pll(idum,iy,iv);
	    v = ran1_pll(idum,iy,iv);

	    if (v == 0.0 || v == 1.0)
	        v = 0.5;
	    y = a * log( v/ (1.0 - v) );
	    res = a * exp(v);
	    z = u*v*v;
	    r = b + c * y - res;
	    if ( r >= ( 9/2 *z - log(1 + 9.0/2.0)) )
	        break;
	    if ( r >= log(z) )
	       break;
	    }  while( 1 );

	return res;
	*/
	    

	/*   base on book (didn't work - negative deviates) */
	/*
	b = a-1;
	c = 3*a - 0.75;

	do
	{
	    u = ran1_pll(idum,iy,iv);
	    v = ran1_pll(idum,iy,iv);

	    w = u*(1.0-u);
	    y = sqrt(c/w) * (u-0.5);
	    x = b + y;
	    if (x >= 0.0)
	    {
		z = 64.0*w*w*w*v*v;
		if ( z <= 1 - (2*y*y)/x)
		    accept = 1;
		else if (log(z) <= 2*(b*log(x/b)-y) )
		    accept = 1;
		else 
		    accept = 0;
	    }
	} while (!accept);
	*/
	/*  weighted bootstrap  from A.F.M. Smith and A.E. Gelfand
	    Bayesian Statistics Without Tears:  A sampling-resampling
	    perspective,  American Statistician, 1992, vol 46, no 2
	*/

		  if ( (int) a == a )         /* use Numerical Recipes function */
	    x = gamdev_pll( (int)a, idum, iy, iv);
        else
	{
	    /* generate 100 samples from g() */
	    for (i = 0; i < 100; i++)
	    {
	        y = gamdev_pll( (int)a, idum, iy, iv );
	        boot[i][0] = y;
		gam_f = exp( - lgamma( a ) + (a-1)* log( y ) - y );
		gam_g = exp( - lgamma( (int) a) + ( (int)a -1 )* log(y) - y );
	        boot[i][1] = gam_f/gam_g;
		sum += boot[i][1];
	    }

	    /* sort samples */
	    quickSortD( boot, 0, 99 );

	    /* cumulate weights */
	    boot[0][1] /= sum ;
	    for (i = 1; i < 100; i++)
		boot[i][1] = boot[i][1]/sum + boot[i-1][1];

	    u = ran1_pll( idum, iy, iv);

	    i = 0;

	    while ( boot[i][1] < u )
	        i++;

	    x = boot[i][0];

	}

    }
    return x;
}

/****************************************************
*  Function:   quickSort
*
*  Notes:      This implements the quicksort algorithm.
*              Quicksort is an in-place sorting algorithm
*              with worst-case running time O(n^2), and
*              average-case running time O(n lg n).
*  Parameters:  a     vector to sort
*               min   starting index of vector to sort
*               max   max index of vector
*
*  Called by:  gammadev_pll
*  Calls :     partition
*****************************************************/

void quickSortD( double a[100][2], int min, int max )

{
    int   q;
    int partitionD( double a[100][2], int p, int r );

    if ( min < max )
    {
        q = partitionD( a, min, max);
        quickSortD( a, min, q);
        quickSortD(a, q+1, max);

    }

}

/****************************************************
*  Function:   partition
*
*  Notes:      Places all elements with a key value less
*              than the first element before all element
*              with a key value greater than the first
*              element.
*              Returns the final index of what was initially
*              the first key value.
*
*  Called by:  quickSort
*****************************************************/

int partitionD( double a[100][2], int p, int r )

{
    double x; 
    int i, j;
    double tmp, tmp2;

    x = a[p][0];
    i = p - 1;
    j = r + 1;

    while ( 1 )
    {
        do {
            j -= 1;
        } while ( a[j][0] > x );

        do {
            i += 1;
        } while ( a[i][0] < x );

        if ( i < j )
        {
            tmp = a[i][0];
	    tmp2 = a[i][1];
            a[i][0] = a[j][0];
            a[i][1] = a[j][1];
            a[j][0] = tmp;
            a[j][1] = tmp2;
        }
        else
            return j;

    }

}


/****************************************************
*  Function:    poidev_pll
*  Purpose:     To generate a random deviate from
*               a Poisson distribution with mean xm
* 		Returns as a floating point number an integer value that is a
* 		random deviate drawn from a poisson distribution of mean xm
*		using ran1_pll() as a source of uniform random deviates
* 		NR, pg. 294
*  Usage:       In calling program: long  seed = -1 (or any negative number); 
*    				    long  iy  = 0
* 				    float oldm = -1.0;
*		Call:	p = poidev_pll(xm, &idum, &iy, iv,&sq,
*                                      &alxm, &g, &oldm);
*
*  Parameters:  idum, iy, iv          used by ran1_pll 
*               sq, alxm, g, oldm     used in poidev (fomerly static)
*               xm                    Poisson mean
*  Returns:     Random deviate from Poisson distribution with mean xm 
*  Calls:   	ran1_pll
*  Called By:
****************************************************/

float
poidev_pll(float xm, long *idum, long *iy, long *iv,
           float *sq, float *alxm, float *g, float *oldm)
{
    float em,t,y;

    if (xm < 12.0)
    {
	if (xm != *oldm)
	{
	    *oldm=xm;
	    *g=exp(-xm);
	}
	em = -1;
	t=1.0;
	do
	{
	    ++em;
	    t *= ran1_pll(idum, iy,iv);
	} while (t > *g);
    }
    else
    {
	if (xm != *oldm)
	{
	    *oldm=xm;
	    *sq=sqrt(2.0*xm);
	    *alxm=log(xm);
	    *g=xm*(*alxm)-gammln(xm+1.0);
	}
	do
	{
	    do
	    {
		y=tan(PI*ran1_pll(idum, iy,iv));
		em=(*sq)*y+xm;
	    } while (em < 0.0);
	    em=floor(em);
	    t=0.9*(1.0+y*y)*exp(em*(*alxm)-gammln(em+1.0)-(*g));
	} while (ran1_pll(idum, iy,iv) > t);
    }
    return em;
}

/****************************************************
*  Function:    bnldev_pll
*  Purpose:     To generate a random deviate from
*               a binomial distribution:         bin(n,pp)
* 		Returns as a floating point number an integer value that is a
* 		random deviate drawn from a binomial distribution of n trials
* 		each of probability pp, 
*		using ran1_pll() as a source of uniform deviates
* 		NR, pg 295
*               mean:  	   n*pp
*		variance:  n*p*(1-p)
*  Usage:       In calling program: long  seed = -1 (or any negative number); 
*    				    long  iy  = 0
* 				    float nold = -1.0;
* 				    float pold = -1.0;
*		Call:	bnldev_pll(pp, n, &idum, &iy, iv, &nold,
*				&pold, &pc, &plog, &pclog, &en, &oldg);
*
*  Parameters:  idum, iy, iv		used by ran1_pll 
*               nold, pold, pc, plog, pclog, en, oldg  
*					used in bnldev 	(fomerly static)
*               pp			probability of success on a trial
*		n			number of trials
*  Returns:     Random deviate from binomial distribution bin(n,pp)
*  Calls:   	ran1_pll
*  Called By:
****************************************************/

float
bnldev_pll(float pp, int n, long *idum, long *iy, long *iv,
           int *nold, float *pold, float *pc, float *plog,
	   float *pclog, float *en,float *oldg)
{
    int j;
    float am,em,g,angle,p,bnl,sq,t,y;

    p=(pp <= 0.5 ? pp : 1.0-pp);
    am=n*p;
    if (n < 25)
    {
	bnl=0.0;
	for (j=1;j<=n;j++)
	    if (ran1_pll(idum,iy,iv) < p)
	++bnl;
    }
    else
	if (am < 1.0)
	{
	    g=exp(-am);
	    t=1.0;
	    for (j=0;j<=n;j++)
	    {
		t *= ran1_pll(idum,iy,iv);
		if (t < g) 
		    break;
	    }
	    bnl=(j <= n ? j : n);
	}
	else
	{
	    if (n != *nold)
	    {
		*en=n;
		*oldg=gammln(*en+1.0);
		*nold=n;
	    }
	    if (p != *pold)
	    {
		*pc=1.0-p;
		*plog=log(p);
		*pclog=log(*pc);
		*pold=p;
	    }
	    sq=sqrt(2.0*am*(*pc));
	    do
	    {
		do
		{
		    angle=PI*ran1_pll(idum,iy,iv);
		    y=tan(angle);
		    em=sq*y+am;
		} while (em < 0.0 || em >= (*en+1.0));
		em=floor(em);
		t=1.2*sq*(1.0+y*y)*exp(*oldg-gammln(em+1.0)
			-gammln(*en-em+1.0)+em*(*plog)+(*en-em)*(*pclog));
	    } while (ran1_pll(idum,iy,iv) > t);
	    bnl=em;
	}
    if (p != pp)
	bnl=n-bnl;
    return bnl;
}

/****************************************************
*  Function:    gamdev_pll
*  Purpose:     generate a random deviate from a gamma(alpha, 1.0)
*               distribution, where alpha must be an integer 
*               uses ran1_pll as a source of uniform deviates.
*  Usage:       long  seed = -1 (or any negative number); 
*               long  iy;
*               long iv[NTAB];
*               a = gamdev_pll(ia, &seed, &iy, iv)  
*  Parameters:   ia              mean ( order )
*                idum, iy, iv    seed numbers  
*  Returns:     float from gamma( ia, 1.0 ) 
*  Calls:       ran1_pll
*  Called By:
*****************************************************/

float
gamdev_pll(int ia, long *idum, long *iy, long *iv)
{
    int j;
    float am,e,s,v1,v2,x,y;

    if (ia < 1)
    {
	fprintf(stderr,"Error in gamdev: ia = %d < 1\n", ia);
	exit(-1);
    }
    if (ia < 6)
    {
	x=1.0;
	for (j=1;j<=ia;j++) x *= ran1_pll(idum, iy, iv);
	    x = -log(x);
    }
    else
    {
	do
	{
	    do
	    {
		do
		{
		    v1=2.0*ran1_pll(idum, iy, iv)-1.0;
		    v2=2.0*ran1_pll(idum, iy, iv)-1.0;
		} while (v1*v1+v2*v2 > 1.0);
		y=v2/v1;
		am=ia-1;
		s=sqrt(2.0*am+1.0);
		x=s*y+am;
	    } while (x <= 0.0);
	    e=(1.0+y*y)*exp(am*log(x/am)-s*y);
	} while (ran1_pll(idum, iy, iv) > e);
    }
    return x;
}

/****************************************************
*  Function:    iran_pll
*  Purpose:     To generate a random integer between
*               min and max (inclusive).  Uses ran1(idum)
*               as a source of uniform deviates.
*  Usage:       long  seed = -1 (or any negative number); 
*               a = iran(&seed,2,6)  [returns an integer from {2,3,4,5,6}]
*  Parameters:  min
*               max
*               idum 
*  Returns:     random integer between min and max (inclusive)
*  Calls:       ran1_pll
*  Called By:
*****************************************************/

int
iran_pll(int min, int max, long *idum, long *iy, long *iv)
{
    if ( max < min)
    {
	fprintf(stderr,"Error in irandom boundaries: max (%d) < min (%d)\n", 
		max, min);
	exit(-1);
    }
    return (int)(ran1_pll(&(*idum),iy,iv)*(max-min+1)) + min;
}

/****************************************************
*  Function:    seed
*  Purpose:     generate seed to be used by ran1() and other functions
*                added rand() to calculation.
*  Parameters:  none
*  Returns:     long
*  Calls:       time,localtime
*  Called by:   
*****************************************************/

long seed()
{
    struct tm *t;
    time_t tp;
    long    idum;     /* the seed  */

    tp = time(NULL);
    t = localtime(&tp);
    idum = (*t).tm_hour + (*t).tm_min + (*t).tm_sec +(*t).tm_wday
           + (*t).tm_year + (*t).tm_yday + rand();

/* negative seed reinitializes random sequence*/

    idum = (-1)*(idum);

    return idum;
}

