/**
 * Filename     : util.c
 * Description  : Utility functions for memory management and I/O
 * Author(s)    : Ben Bornstein
 * Organization : Machine Learning Systems, Jet Propulsion Laboratory
 * Created      : September 2002
 * Revision     : $Id: util.c,v 1.5 2002/09/24 23:01:03 bornstei Exp $
 * Source       : $Source: /proj/CVS/code/c/mls/kmeans/util.c,v $
 *
 * Copyright 2002, California Institute of Technology. ALL RIGHTS RESERVED.
 * U.S. Government Sponsorship acknowledged.
 */


#include <stdio.h>
#include <stdlib.h>

#include "util.h"


/**
 * The error messages printed by these routines have the form:
 *
 *   "%s: error: message\n"
 *
 * Where %s will be replaced by the value of PROGRAM_NAME.
 */
const char *PROGRAM_NAME = "kmeans";


/**
 * Prints the given string, followed by a newline.
 */
void
println (char *string)
{
  printf(string);
  printf("\n");
}


/**
 * Attempts to open filename for the given access mode and return a pointer
 * to it.  If the filename could not be opened, prints an error message and
 * exits.
 */
FILE *
safe_fopen (const char *filename, const char *mode)
{
  const char *format  = "%s: error: Could not open file '%s' for %s.\n";
  const char *modestr = strcmp(mode, "r") ? "writing" : "reading";
  FILE       *fp      = fopen(filename, mode);


  if (fp == (FILE *) NULL)
  {
    fprintf(stderr, format, PROGRAM_NAME, filename, modestr);
    exit(-1);
  }

  return fp;
}


/**
 * Allocates memory for an array of nmemb elements of size bytes each and
 * returns a pointer to the allocated memory.  The memory is set to zero.
 * If the memory could not be allocated, prints an error message and exits.
 */
void *
safe_calloc (size_t nmemb, size_t size)
{
  const char *format   = "%s: error: Out of Memory!\n";
  void       *location = (void *) calloc(nmemb, size);



  if (location == (void *) NULL)
  {
    fprintf(stderr, format, PROGRAM_NAME);
    exit(-1);
  }

  return location;
}


/**
 * Allocates size bytes of memory and returns a pointer to the allocated
 * memory.  If the memory could not be allocated, prints an error message
 * and exits.
 */
void *
safe_malloc (size_t size)
{
  const char *format   = "%s: error: Out of Memory!\n";
  void       *location = (void *) malloc(size);


  if (location == (void *) NULL)
  {
    fprintf(stderr, format, PROGRAM_NAME);
    exit(-1);
  }

  return location;
}
