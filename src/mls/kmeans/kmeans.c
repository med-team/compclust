/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * @author Ben Bornstein
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "kmeans.h"
#include "matrix.h"
#include "random.h"
#include "util.h"

#include "nr_util.h"

#include "dist_metrics.h"
#include "init_means.h"
#include "norm_data.h"

/**
 * Safely allocates (via safe_malloc()) memory for a kmeans_data structure.
 * The resulting structure is initialized with values from the given
 * kmeans_parameters structure. A pointer to the structure is returned.
 *
 * @see safe_malloc
 */
struct kmeans_data *allocate_kmeans_data(struct kmeans_parameters *p)
{
  struct kmeans_data *d = (struct kmeans_data *) NULL;


  d = (struct kmeans_data *) safe_malloc( sizeof(struct kmeans_data) );

  d->datapoints = p->rows;
  d->features   = p->cols;
  d->classes    = p->k;

  d->classifications   = NR_ivector(1, d->datapoints);
  d->class_count_array = NR_ivector(1, d->classes);
  d->data              = NR_dmatrix(1, d->datapoints, 1, d->features);
  d->class_means       = NR_dmatrix(1, d->classes, 1, d->features);
  d->seed              = &(p->seed);

  return d;
}


/**
 * Safely allocates (via safe_malloc()) memory for a kmeans_parameters
 * structure.  The resulting structure is initialized with default values.
 * A pointer to the structure is returned.
 *
 * @see safe_malloc
 */
struct kmeans_parameters *allocate_kmeans_parameters(void)
{
  struct kmeans_parameters *p = (struct kmeans_parameters *)
                                safe_malloc( sizeof(struct kmeans_parameters) );


  p->distance_metric  = -1;
  p->init_means       = -1;
  p->k                = -1;
  p->k_strict         =  0;
  p->max_iterations   = -1;
  p->max_restarts     =  0;
  p->num_mean_samples =  3;
  p->cols             = -1;
  p->rows             = -1;
  p->seed             = 42;
  p->dataset_filename = (char *) NULL;
  p->result_filename  = (char *) NULL;
  p->means_filename   = (char *) NULL;

  return p;
}


/**
 * Frees the memory associated with the given kmeans_data structure.
 */
void free_kmeans_data(struct kmeans_data *d)
{
  NR_free_ivector(d->classifications, 1, d->datapoints);
  NR_free_ivector(d->class_count_array, 1, d->classes);
  NR_free_dmatrix(d->data, 1, d->datapoints, 1, d->features);
  NR_free_dmatrix(d->class_means, 1, d->classes, 1, d->features);

  free( d );
}


/**
 * Frees the memory associated with the given kmeans_parameters structure.
 */
void free_kmeans_parameters(struct kmeans_parameters *p)
{
  if (p->dataset_filename != NULL)
    free( p->dataset_filename );

  if (p->result_filename != NULL)
    free( p->result_filename );

  if (p->means_filename != NULL)
    free(p->means_filename);

  free(p);
}


/**
 * Classifies each datum from 1 to k according to the nearest mean.
 *
 * Where:
 *   k           = d->classes
 *   means       = d->class_means
 *   memberships = d->classifications
 *   datum       = d->data[i][]
 *   i           = [1 ... d->datapoints]
 *   j           = [1 ... d->features  ]
 */
void classify_data(struct kmeans_data *d, struct kmeans_parameters *p)
{
  double *dist;
  int    class;
  int    i, j;


  if (DEBUG_KMEANS)
  {
    fprintf( stderr, "  Entering classify_data()\n");
  }

  dist = NR_dvector(1, d->classes);

  zero_class_counts(d);

  for (i = 1; i <= d->datapoints; i++)
  {
    /**
     * Compute all the datapoint to class means values and find the min.
     */

    distance2(d->data[i], d->class_means, d->features, d->classes, dist,
              p->distance_metric);

    class = 1;

    for (j = 2; j <= d->classes; j++)
    {
      if (dist[j] < dist[class]) 
      {
        class = j;
      }
    }

    d->classifications[i] = class;
    d->class_count_array[class]++;
  }

  if (DEBUG_KMEANS)
  {
    fprintf( stderr, "  Exiting  classify_data()\n");
  }

  NR_free_dvector(dist, 1, d->classes);
}


/**
 * Compares the contents of the classification field of the kmeans_data
 * structure to classifications.  If they are equal by content zero
 * is returned.  Otherwise, the number of elements by which they differ
 * is returned.
 */
int compare_classifications(struct kmeans_data *d, int *classifications)
{
  int differences = 0;
  int i;


  for (i = 1; i <= d->datapoints; i++)
  {
    if (d->classifications[i] != classifications[i])
    {
      differences++;
    }
  }

  return differences;
}


/**
 * Copies the contents of the classification field of the kmeans_data
 * structure to destination.
 */
void copy_classifications(struct kmeans_data *d, int *destination)
{
  int i;


  for (i = 1; i <= d->datapoints; i++)
  {
    destination[i] = d->classifications[i];
  }
}


/**
 * Estimates the means of k classes using the class membership of each
 * datum.
 *
 * Where:
 *   k           = d->classes
 *   memberships = d->classifications
 *   datum       = d->data[i][]
 *   i           = [1 ... d->datapoints]
 *   j           = [1 ... d->features  ]
 */
void estimate_means(struct kmeans_data *d)
{
  int class, i, j;


  if (DEBUG_KMEANS)
  {
    fprintf( stderr, "  Entering estimate_means()\n");
  }


  zero_class_means(d);

  /*
   * Accumulate running sum of d->data[i] based on class membership.
   */
  for (i = 1; i <= d->datapoints; i++)
  {
    class = d->classifications[i];

    for (j = 1; j <= d->features; j++)
    {
      d->class_means[class][j] += d->data[i][j];
    }
  }

  /*
   * Divide running sum by class size to estimate means.
   */
  for (i = 1; i <= d->classes; i++)
  {
    for (j = 1; j <= d->features; j++)
    {
      d->class_means[i][j] /= ((double) d->class_count_array[i]);
    }
  }


  if (DEBUG_KMEANS)
  {
    fprintf( stderr, "  Exiting  estimate_means()\n");
  }
}


/**
 * Returns the number of empty classes in d->class_count_array.
 */
int get_num_empty_classes(struct kmeans_data *d)
{
  int num_empty_classes = 0;
  int i;


  for (i = 1; i <= d->classes; i++ )
  {
    if (d->class_count_array[i] == 0)
    {
      num_empty_classes++;
    }
  }

  return num_empty_classes;
}


/**
 * Performs kmeans clustering on the data field of the kmeans_data structure.
 *
 * If p->k_strict is 1, kmeans attempts to find only k clusters.
 *
 * Otherwise, if p->k_strict is 0 and k clusters could not be found, k - 1
 * clusters are tried.  This process continues until either a clustering
 * with no collapsed clusters is achieved, or k is 1.  In this case,
 * d->classes can be thought of as an upper-bound (maximum) on k.  When
 * done, d->classes will contain the final k found.
 *
 * Returns the number of empty classes if p->k_strict == 1, zero otherwise.
 *
 * Where:
 *   k     = d->classes
 *   datum = d->data[i][j]
 *   i     = [1 ... d->datapoints]
 *   j     = [1 ... d->features  ]
 *
 * @see kmeans_internal
 */
int kmeans(struct kmeans_data *d, struct kmeans_parameters *p)
{
  int empty_classes = 0;


  /*
   * The Numerical Recipes random number generator requires a negative long
   * for initialization.
   */
  if ( *(d->seed) > 0)
  {
    *(d->seed) *= -1;
  }

  ran1( d->seed );

  /*
   * In the case of a Pearson Correlation distance metric, normalize
   * the data to a unit sphere.  Then, standard Euclidean distance
   * can be used as though it were Correlation.
   */
  if (p->distance_metric == CORRELATION)
  {
    norm_data(d->data, d->datapoints, d->features, HYPERSPHERE_NORM, 0);
  }

  /*
   * Perform a single run of kmeans and save the result (whether or
   * not any empty classes occurred).
   */
  empty_classes = kmeans_internal(d, p);

  /*
   * If any empty classes occurred (empty_classes > 0) and k_strict is not
   * turned-on (p->k_strict == 0), then continually "step-down" k
   * (d->classes) until a suitable k is found or k is 1.
   */
  if (p->k_strict == 0)
  {
    while (empty_classes > 0)
    {
      d->classes--;

      if (DEBUG_KMEANS)
      {
        printf( "failed for k = %d, with empty_classes = %d.\n",
                d->classes + 1, empty_classes );
        printf( "Trying for k = %d.\n", d->classes );
      }

      if (d->classes > 1)
      {
        empty_classes = kmeans_internal(d, p);
      }
      else
      {
        zero_classifications(d);
        empty_classes = 0;
      }
    }
  }


  return empty_classes;
}


/**
 * Performs kmeans clustering on the data field of the kmeans_data structure.
 *
 * Returns the number of empty classes.  Zero indicates complete success,
 * since no collapsed clusters occurred.
 *
 * NOTE: This function is internal to kmeans.c and should only be called by
 * NOTE: kmeans().
 *
 * Where:
 *   k     = d->classes
 *   datum = d->data[i][j]
 *   i     = [1 ... d->datapoints]
 *   j     = [1 ... d->features  ]
 *
 * @see kmeans
 */
int kmeans_internal(struct kmeans_data *d, struct kmeans_parameters *p)
{
  int iterations       = 0;
  int restarts         = 0;
  int max_iterations   = p->max_iterations;
  int max_restarts     = p->max_restarts;
  int *classifications = (int *) NULL;
  int done             = 0;
  int empty_classes    = 0;


  if (DEBUG_KMEANS)
  {
    fprintf( stderr, "Entering function kmeans_strict().\n"    );
    fprintf( stderr, "  k              = %d\n", d->classes     );
    fprintf( stderr, "  max_iterations = %d\n", max_iterations );
    fprintf( stderr, "  max_restarts   = %d\n", max_restarts   );
  }

  classifications = NR_ivector(1, d->datapoints);

  /*
   * INIT_MEANS_CHURCH:
   *   If means were initialized Church style, then restarting make no sense
   *   (Church style is completely deterministic), so turn restarting off by
   *   setting restarts to the maximum allowable.  Thus, the first for loop
   *   below will be executed only once.
   */
  if (p->init_means == CHURCH_MEANS)
  {
    restarts = max_restarts;
  }

  /*
   * The termination condition for this loop, restarts <= max_restarts,
   * is correct.  Since the first time through the loop (restarts = 0)
   * is not a restart, but the first run of k-means.
   */
  for (restarts = 0; !done && (restarts <= max_restarts); restarts++)
  {
    init_means(d->data, d->class_means, d->datapoints, d->features, d->classes,
               p->init_means, p->seed, p->num_mean_samples, p->means_filename);

    zero_classifications(d);

    for (iterations = 0; !done && (iterations < max_iterations); iterations++)
    {
      /*
       * Store the previous set of classifications, to test for
       * convergence later.
       */
      copy_classifications(d, classifications);

      /*
       * In the case of Pearson Correlation distance metric,
       * normalize the means to the unit sphere.  Then, standard Euclidean
       * distance can be used as though it were Pearson Correlation.
       *
       * This must be done every time the means are initialized or
       * (re)estimated.
       */
      if (p->distance_metric == CORRELATION)
      {
        norm_data(d->class_means, d->classes, d->features, 
                  HYPERSPHERE_NORM, 0);
      }

      classify_data(d, p);

      /*
       * If some clusters collapsed during classification, break out of
       * this set of kmeans iterations to restart.
       */
      empty_classes = get_num_empty_classes(d);
      if (empty_classes > 0)
      {
        break;
      }

      estimate_means(d);

      /*
       * Test for convergence.  If the classifications have not changed
       * since the previous iteration of kmeans, we are done.
       *
       * The function compare_classifications() returns a zero if
       * kmeans_data->classifications and classifications are equal by content,
       * greater than zero otherwise.
       */
      done = !compare_classifications(d, classifications);
    }
  }

  NR_free_ivector(classifications, 1, d->datapoints);

  if (DEBUG_KMEANS)
  {
    if (done)
    {
      fprintf( stderr, "Solution converged after\n" );
    }
    else
    {
      fprintf( stderr, "Solution DID NOT converge after\n" );
    }

    restarts--;

    fprintf( stderr, "  %d restart(s), and\n", restarts   );
    fprintf( stderr, "  %d iterations(s).\n" , iterations );

    print_data (stderr, d);
    print_means(stderr, d);

    fprintf(stderr, "Exiting function kmeans_strict().\n\n");
  }

  return empty_classes;
}


/**
 * Prints the data vectors in the kmeans_data structure to the given stream.
 */
void print_data(FILE *stream, struct kmeans_data *d)
{
  int i, j;


  fprintf(stream, "Data:\n");

  for (i = 1; i <= d->datapoints; i++)
  {
    for (j = 1; j <= d->features; j++)
    {
      fprintf(stream, "  %6.4f", d->data[i][j]);
    }

    fprintf(stream, "\n");
  }
}


/**
 * Pretty-prints the values of given kmeans_parameters structure to
 * the given stream.
 */
void print_kmeans_parameters(FILE *stream, struct kmeans_parameters *p)
{
  char *null             = "(null)";
  char *unknown          = "(unknown)";
  char *distance_metric  = unknown;
  char *init_means       = unknown;
  char *dataset_filename = null;
  char *result_filename  = null;


  if (p->distance_metric == CORRELATION)
  {
    distance_metric = "correlation";
  }
  else if (p->distance_metric == EUCLIDEAN)
  {
    distance_metric = "euclidean";
  }


  if (p->init_means == CHURCH_MEANS)
  {
    init_means = "church_means";
  }
  else if (p->init_means == RANDOM_MEANS)
  {
    init_means = "random_means";
  }
  else if (p->init_means == RANDOM_RANGE)
  {
    init_means = "random_range";
  }
  else if (p->init_means == RANDOM_SAMPLE)
  {
    init_means = "random_sample";
  }


  if (p->dataset_filename != NULL)
  {
    dataset_filename = p->dataset_filename;
  }

  if (p->result_filename != NULL)
  {
    result_filename = p->result_filename;
  }


  fprintf( stream, "kmeans parameters:\n" );
  fprintf( stream, "  distance_metric  = [%s].\n" , distance_metric     );
  fprintf( stream, "  init_means       = [%s].\n" , init_means          );
  fprintf( stream, "  k                = [%d].\n" , p->k                );
  fprintf( stream, "  max_iterations   = [%d].\n" , p->max_iterations   );
  fprintf( stream, "  max_restarts     = [%d].\n" , p->max_restarts     );
  fprintf( stream, "  num_mean_samples = [%d].\n" , p->num_mean_samples );
  fprintf( stream, "  cols             = [%d].\n" , p->cols             );
  fprintf( stream, "  rows             = [%d].\n" , p->rows             );
  fprintf( stream, "  seed             = [%ld].\n", p->seed             );
  fprintf( stream, "  dataset_filename = [%s].\n" , dataset_filename    );
  fprintf( stream, "  result_filename  = [%s].\n" , result_filename     );
}


/**
 * Prints the mean vectors for each class in the kmeans_data structure to
 * the given stream.
 */
void print_means(FILE *stream, struct kmeans_data *d)
{
  int i, j;


  fprintf(stream, "Means:\n");

  for (i = 1; i <= d->classes; i++)
  {
    for (j = 1; j <= d->features; j++)
    {
      fprintf(stream, "  %6.4f", d->class_means[i][j]);
    }

    fprintf(stream, "\n");
  }
}

/**
 * Zeros the classification field of the kmeans_data structure.
 */
void zero_classifications(struct kmeans_data *d)
{
  zero_1D_int_matrix(d->classifications, d->datapoints);
}


/**
 * Zeros the class_count_array field of the kmeans_data structure.
 */
void zero_class_counts(struct kmeans_data *d)
{
  zero_1D_int_matrix(d->class_count_array, d->classes);
}


/**
 * Zeros the class_means field of the kmeans_data structure.
 */
void zero_class_means(struct kmeans_data *d)
{
  zero_2D_double_matrix(d->class_means, d->classes, d->features);
}
