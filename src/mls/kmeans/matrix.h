/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * @author Ben Bornstein
 */

#ifndef __MATRIX_H
#define __MATRIX_H


#define NR_END 1


/**
 * Safely allocates (via safe_malloc()) memory for a double matrix with the
 * given low and high row and column indices.  A pointer to the matrix is
 * returned.
 *
 * Taken from Numerical Recipes in C.
 *
 * @see safe_malloc
 */
double **dmatrix(int rl, int rh, int cl, int ch);

/**
 * Frees the memory associated with the given double matrix and its low and
 * high row and column indices.
 *
 * Taken from Numerical Recipes in C.
 */
void free_dmatrix(double **m, int rl, int rh, int cl, int ch);

/**
 * Divides each value in the given MxN matrix by its row's Euclidean norm.
 *
 * In the case of Pearson Correlation distance metric, this must be done
 * (the means are normalized to the unit sphere).  Then, standard Euclidean
 * distance can be used as though it were Pearson Correlation.
 */
void normalize_double_matrix(double **matrix, int M, int N);

/**
 * Zeros the contents of the given 1xN matrix of doubles.
 */
void zero_1D_double_matrix(double *matrix, int N);

/**
 * Zeros the contents of the given 1xN matrix of integers.
 */
void zero_1D_int_matrix(int *matrix, int N);

/**
 * Zeros the contents of the given MxN matrix.
 */
void zero_2D_double_matrix(double **matrix, int M, int N);

#endif
