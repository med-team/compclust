/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * @author Ben Bornstein
 */


#include <math.h>
#include <stdlib.h>
#include "util.h"
#include "matrix.h"


/**
 * Safely allocates (via safe_malloc()) memory for a double matrix with the
 * given low and high row and column indices.  A pointer to the matrix is
 * returned.
 *
 * Taken from Numerical Recipes in C.
 *
 * @see safe_malloc
 */
double **dmatrix(int rl, int rh, int cl, int ch)
{
	int	i;
  int nrow = rh - rl + 1;
  int ncol = ch - cl + 1;
	double **m;


	m  = (double **) safe_malloc( (nrow + NR_END) * sizeof(double *) );
	m += NR_END;
	m -= rl;

	m[rl]  = (double *) safe_malloc( (nrow * ncol + NR_END) * sizeof(double) );
	m[rl] += NR_END;
	m[rl] -= cl;

	for (i = rl + 1; i <= rh; i++)
  {
		m[i] = m[i - 1] + ncol;
  }

	return m;
}


/**
 * Frees the memory associated with the given double matrix and its low and
 * high row and column indices.
 *
 * Taken from Numerical Recipes in C.
 */
void free_dmatrix(double **m, int rl, int rh, int cl, int ch)
{
	free( (void *) (m[rl] + cl - NR_END) );
	free( (void *) (m     + rl - NR_END) );
}


/**
 * Divides each value in the given MxN matrix by its row's Euclidean norm.
 *
 * In the case of Pearson Correlation distance metric, this must be done
 * (the means are normalized to the unit sphere).  Then, standard Euclidean
 * distance can be used as though it were Pearson Correlation.
 */
void normalize_double_matrix(double **matrix, int M, int N)
{
  double euclidean_norm;
  int    r, c;


  for (r = 0; r < M; r++)
  {
    euclidean_norm = 0.0;

    for (c = 0; c < N; c++)
    {
      euclidean_norm += (matrix[r][c] * matrix[r][c]);
    }

    euclidean_norm = sqrt(euclidean_norm);

    for(c = 0; c < N; c++)
    {
      matrix[r][c] /= euclidean_norm;
    }
  }
}

/**
 * Zeros the contents of the given 1xN matrix of doubles.
 */
void zero_1D_double_matrix(double *matrix, int N)
{
  int i;


  for (i = 1; i <= N; i++)
  {
    matrix[i] = 0.0;
  }
}


/**
 * Zeros the contents of the given 1xN matrix of integers.
 */
void zero_1D_int_matrix(int *matrix, int N)
{
  int i;


  for (i = 1; i <= N; i++)
  {
    matrix[i] = 0;
  }
}


/**
 * Zeros the contents of the given MxN matrix.
 */
void zero_2D_double_matrix(double **matrix, int M, int N)
{
  int i, j;
 

  for (i = 1; i <= M; i++)
  {
    for (j = 1; j <= N; j++)
    {
      matrix[i][j] = 0.0;
    }
  }
}
