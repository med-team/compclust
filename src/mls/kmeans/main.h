/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * @author Ben Bornstein
 */

#ifndef __MAIN_H
#define __MAIN_H


/**
 * Provides a command-line interface to the kmeans() function.
 * See Usage and Full_Usage for more details.
 *
 * Returns (to the operating system):
 *
 *   The number of empty classes (collapsed clusters) that occurred as a
 *   result of this kmeans run.  This number can only be greater than zero
 *   when --k-strict is specified.  A value of zero indicates success.
 *   That is, no empty classes occurred.
 *    
 *   -1  If there was a fatal internal error (e.g. malloc() or fopen()
 *       failed.)
 *
 *   -2  If there was an error reading command-line options.
 */
int main(int argc, char *argv[]);

/**
 * Returns the name of the given option.
 */
char *get_option_name(int option);

/**
 * Returns 1 if the given option is required, 0 otherwise.
 */
int is_option_required(int option);

/**
 * Parses command-line options (using GNU getopt, part of glibc) and sets
 * the given kmeans_parameters structure accordingly.  In case of errors,
 * this function will print a usage statement (via print_usage()) and exit
 * the program.
 *
 * @see print_usage
 */
void parse_command_line_options(int argc, char *argv[],
                                struct kmeans_parameters *p);

/**
 * Reads the contents of p->dataset_filename into d->data[][].  Only
 * p->rows rows and p->cols columns will be read.
 */
void read_dataset(struct kmeans_data *d, struct kmeans_parameters *p);

/**
 * Writes the contents of d->classifications[] (one per line) to
 * p->result_filename.  Only d->datapoints lines will be written.
 */
void write_result(struct kmeans_data *d, struct kmeans_parameters *p);


#endif
