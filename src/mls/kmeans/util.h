/**
 * Filename     : util.h
 * Description  : Utility functions for memory management and I/O
 * Author(s)    : Ben Bornstein
 * Organization : Machine Learning Systems, Jet Propulsion Laboratory
 * Created      : September 2002
 * Revision     : $Id: util.h,v 1.4 2002/09/24 23:02:54 bornstei Exp $
 * Source       : $Source: /proj/CVS/code/c/mls/kmeans/util.h,v $
 *
 * Copyright 2002, California Institute of Technology. ALL RIGHTS RESERVED.
 * U.S. Government Sponsorship acknowledged.
 */


#ifndef __UTIL_H
#define __UTIL_H

#include <stdio.h>

/**
 * Prints the given string, followed by a newline.
 */
void
println (char *string);

/**
 * Attempts to open filename for the given access mode and return a pointer
 * to it.  If the filename could not be opened, prints an error message and
 * exits.
 */
FILE *
safe_fopen (const char *filename, const char *mode);

/**
 * Allocates memory for an array of nmemb elements of size bytes each and
 * returns a pointer to the allocated memory.  The memory is set to zero.
 * If the memory could not be allocated, prints an error message and exits.
 */
void *
safe_calloc (size_t nmemb, size_t size);

/**
 * Allocates size bytes of memory and returns a pointer to the allocated
 * memory.  If the memory could not be allocated, prints an error message
 * and exits.
 */
void *
safe_malloc (size_t size);


#endif
