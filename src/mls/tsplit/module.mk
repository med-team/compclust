CURDIR := $(MLSDIR)/tsplit

TSPLITSRC := $(wildcard $(CURDIR)/*.c)
SRC += $(TSPLITSRC)
CFLAGS += -I$(CURDIR)

TSPLITLIBS = $(BASEDIR)/da/libda$(LIBEXT) \
            $(BASEDIR)/nr/libnr$(LIBEXT) \
	    $(BASEDIR)/ut/libut$(LIBEXT) \
	    $(MLSDIR)/common/libcommon$(LIBEXT) \
	    $(MLSDIR)/normalizations/libnorm$(LIBEXT) \
	    $(MLSDIR)/distances/libdist$(LIBEXT) \
	    $(MLSDIR)/initializations/libinit$(LIBEXT)
TSPLITLIBDIRFLAGS := $(call make_libdirflag,$(TSPLITLIBS))
TSPLITLIBLINKFLAGS := $(call make_liblinkflag,$(TSPLITLIBS)) -lm

TARGET := $(CURDIR)/tsplit$(BINEXT)
TARGETBINS += $(TARGET)

$(TARGET): $(TSPLITSRC:.c=$(OBJEXT)) $(TSPLITLIBS)
	$(CC) $(TSPLITLIBDIRFLAGS) -o $@ $(TSPLITSRC:.c=$(OBJEXT)) $(TSPLITLIBLINKFLAGS)
