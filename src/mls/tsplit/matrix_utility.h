#define TINY 1.0e-20;

float invdet(float **a, int n, float **inv);
void  ludcmp(float **a, int n,int *indx, float *d);
void  lubksb(float **a, int n, int *indx, float b[]);
void  jacobi(float **x, int n, float *d, float **vec);


/****
 *
 * return determinant of matrix a[nxn] and its inverse inv[nxn]
 *
 * This uses an LU decomposition which will not fail for real, symmetric,
 * positive definite matricies.
 *
 ***/

float invdet(float **a, int n, float **inv) 
{
  int i,j,*indx;
  float d,*col;

  col  = alloc1df(n);
  indx = alloc1di(n);

  ludcmp(a, n, indx, &d);
  for (j = 0; j < n; j++) {
    d *= a[j][j];
    for (i=0; i<n; i++) 
      col[i] = 0.0;
    col[j] = 1.0;
    lubksb(a, n, indx, col);
    for (i = 0; i < n; i++) 
      inv[i][j] = col[i];
  }

  free(col);
  free(indx);
  return d;
}


void ludcmp(float **a, int n,int *indx, float *d)
{
  int i,imax = 0,j,k;
  float big,dum,sum,temp;
  float *vv;

  vv = alloc1df(n);
  
  *d = 1.0;
  for (i = 0; i < n; i++) {
    big = 0.0;
    for (j = 0; j < n; j++) 
      if ((temp = fabs(a[i][j])) > big) 
        big = temp;
    if (big == 0.0) 
      printf("Singular matrix\n");
    vv[i]=1.0/big;
  }

  for (j=0; j<n; j++) {
    for (i=0; i<j; i++) {
      sum = a[i][j];
      for (k = 0; k < i; k++) sum -= a[i][k]*a[k][j];
      a[i][j] = sum;
    }
    big=0.0;
    for (i=j; i<n; i++) {
      sum=a[i][j];
      for (k=0; k<j; k++)
        sum -= a[i][k]*a[k][j];
      a[i][j]=sum;
      if ( (dum=vv[i]*fabs(sum)) >= big) {
        big=dum;
        imax=i;
      }
    }
    if ( j != imax) {
      for (k=0; k<n; k++) {
        dum=a[imax][k];
        a[imax][k]=a[j][k];
        a[j][k]=dum;
      }
      *d = -(*d);
      vv[imax]=vv[j];
    }
    indx[j]=imax;
    if (a[j][j] == 0.0) a[j][j]=TINY;
    if (j != n-1) {
      dum=1.0/a[j][j];
      for (i=j+1; i<n; i++) a[i][j] *= dum;
    }
  }
  free(vv);
}

void lubksb(float **a, int n, int *indx, float b[])
{
  int i,ii=0,ip,j;
  float sum;
  
  for (i=0; i<n; i++) {
    ip=indx[i];
    sum=b[ip];
    b[ip]=b[i];
    if (ii>=0) 
      for (j=ii; j<=i-1; j++) sum -= a[i][j]*b[j];
    else if (sum) ii=i;
    b[i]=sum;
  }
  for (i=n-1; i>=0; i--) {
    sum=b[i];
    for (j=i+1; j<n; j++) sum -= a[i][j]*b[j];
    b[i]=sum/a[i][i];
  }
}


void jacobi(float **x, int n, float *d, float **vec)

/* The input to this funciton are (a) an n by n symmetric matrix x, and
   (b) an integer n, the dimension of x. The output of the funciton are 
   (a) an n by 1 vector d with all the eigenvalues of x arranged in descending
   order, and (b) an n by n matrix vec, whose columns are the eigenvectors of 
   x arranged in the same order as the eigenvalues in d. 
*/
{
  int i,j,k,l,m,ic,it,iz;
  float *a,*b,*z;
  float c,s,t,tr,ta,g,g1,h,sm;
  
  /*
    a=vector(0,n*(n+1)/2);
    b=vector(0,n-1);
    z=vector(0,n-1);
  */

  a=alloc1df(n*(n+1)/2+1);
  b=alloc1df(n+1);
  z=alloc1df(n+1);

  m=0;
  for (i=0; i<n; i++) 
    for (j=i; j<n; j++) {
      m+=1; 
      a[m]=x[i][j]; 
    }
  for (i=0; i<n; i++) {
    z[i]=0;
    for (j=0; j<n; j++) {
      vec[i][j]=0.0;
      if (i == j) vec[i][j]=1.0;
    }
  }
  iz=1;
  for (i=0; i<n; i++){
    b[i]=d[i]=a[iz];          /* b and d hold the diagonal elements */
    iz+=n-i;
  }
  for (l=1; l<=10; l++){           /* the main iteration */
    /*    printf("l=%d  ",l); */
    sm=0.0; 
    iz=1;
    
    for (i=1; i<n; i++){
      for (j=1; j<n-i; j++)
        sm+=fabs(a[iz+j]);        /* sm: sum of all elements above diagonal */
      iz+=n-i+1;
    }
    
    /*    printf("l=%d, sm=%f\n",l,sm); */
    if (sm < 10.e-7) goto l0;
    
    if (l < 4) tr=0.2*sm/(n*n);
    else tr=0.0;
    iz=1;
    for (i=1; i<n; i++) {
      for (j=i+1; j<=n; j++) {
        g1=a[iz+j-i];
        g=100*g1;
        if (l > 4 && g == 0.0) goto l1;
        if (fabs(g1) <= tr) goto l2;
        h=d[i-1]-d[j-1];
        if (2*fabs(g1) < fabs(h)){
          t=2*g1/h;
          c=1.0/sqrt(1+t*t);
          s=t*c;
        }
        else {
          t=0.5*h/g1;
          c=fabs(t)/sqrt(1+t*t);
          if (t == 0) s=1.0;
          else s=c/t;
        }
        c=sqrt((1.0+c)/2);
        s=0.5*s/c;
        h=g1*s/c;
        ta=s/(1+c);
        z[i-1]=z[i-1]+h;
        z[j-1]=z[j-1]-h;
        d[i-1]=d[i-1]+h;
        d[j-1]=d[j-1]-h;
        ic=i;
        it=j;
        for (k=1; k<=n; k++) {
          g=a[ic];
          h=a[it];
          a[ic]=g+s*(h-g*ta);
          a[it]=h-s*(g+h*ta);
          if (k < i) ic+=n-k;
          else   ic+=1;
          if (k < j) it+=n-k;
          else it+=1;
        }
        for (k=0; k<n; k++) {
          g=vec[k][i-1];
          h=vec[k][j-1];
          vec[k][i-1]=g+s*(h-g*ta);
          vec[k][j-1]=h-s*(g+h*ta);
        }
      l1:
        a[iz+j-i]=0.0;
      l2:
        g=g;
      }       /* close for loop for (j=i+1, n-1) */
      iz+=n-i+1;
    }       /* close for loop for (i=0, n-2) */
    
    for (i=0; i<n; i++) {
      b[i]+=z[i];
      d[i]=b[i];
      z[i]=0.0;
    }
  }         /* close for loop for l=1,10 */
  
 l0:
  /*
    printf("\nrearranging eigen values and vectors in descending order...\n");
  */
  for (i=0; i<n-1; i++)           
    for (j=i+1; j<n; j++) 
      if ( d[i] < d[j] ) {
        g=d[i];
        d[i]=d[j];
        d[j]=g;
        for (k=0; k<n; k++) {
          g=vec[k][i];
          vec[k][i]=vec[k][j];
          vec[k][j]=g;
        }
      }
  free(a); free(b); free(z);
  return;
}  
