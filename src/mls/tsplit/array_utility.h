/*************************************************

utility funtions for array operations.

*************************************************/

unsigned char      *alloc1db();
unsigned char     **alloc2db();
unsigned char    ***alloc3db();
unsigned char   ****alloc4db();
unsigned char  *****alloc5db();
unsigned char ******alloc6db();

int       *alloc1di();
int      **alloc2di();
int     ***alloc3di();
int    ****alloc4di();
int   *****alloc5di();
int  ******alloc6di();
int *******alloc7di();

float       *alloc1df();
float      **alloc2df();
float     ***alloc3df();
float    ****alloc4df();
float   *****alloc5df();
float  ******alloc6df();
float *******alloc7df();

char fname[50];
FILE *fp;

void pause(void)
{ 
  /*
  printf("\npaused... enter 1 to continue, enter 0 to quit: ");
  scanf("%d",&k);
  if(!k) exit;
  */
}

void msg_pause(char *msg)
{ 
  printf("\n%s",msg);
  /*
  printf("\npaused... enter 1 to continue, enter 0 to quit: ");
  scanf("%d",&k);
  if(!k) exit;
  */
}


/*
char *alloc1dc(n)
int n;
{ int i;
  char *array;
  if ( (array = (char*)malloc(n * sizeof(char)) )==NULL)
    { printf("Unable to allocate memory for 1D char array...\n"); exit(0); }
  return array;
}

char **alloc2dc(m, n)
int m,n;
{ int i,j; 
  char **array;
  if ( (array = (char **) malloc(m * sizeof(char*)) )==NULL) 
    { printf("Unable to allocate memory for 2D char array...\n"); exit(0); }
  for (i=0; i<m; i++)    array[i]=alloc1dc(n);
  return array;
}
*/

float *alloc1df(int n)
{ 
  float *array;
  
  if ((array = (float*) calloc(n, sizeof(float))) == NULL) {
    printf("Unable to allocate memory for 1D float array...\n"); 
    exit(0); 
  }

  return array;
}

float **alloc2df(int m, int n)
{ 
  int i; 
  float **array;
  
  if ((array = (float **) malloc(m * sizeof(float*))) == NULL) {
    printf("Unable to allocate memory for 2D float array...\n"); 
    exit(0); 
  }

  for (i = 0; i < m; i++)
    array[i] = alloc1df(n);

  return array;
}


float ***alloc3df(int l, int m, int n)
{ 
  int i;
  float ***array;

  if ((array = (float ***) malloc(l * sizeof(float**))) == NULL) {
    printf("Unable to allocate memory for 3D float array...\n"); 
    exit(0); 
  }
  
  for (i = 0; i < l; i++)    
    array[i] = alloc2df(m, n);
  
  return array;
}

float ****alloc4df(int k, int l, int m, int n)
{ 
  int i;
  float ****array;
  
  if ((array = (float ****) malloc(k * sizeof(float***))) == NULL) { 
    printf("Unable to allocate memory for 4D float array...\n"); 
    exit(0); 
  }

  for (i = 0; i < k; i++)
    array[i] = alloc3df(l, m, n);

  return array;
}

float *****alloc5df(int j, int k, int l, int m, int n)
{ 
  int i;
  float *****array;
  
  if ((array = (float *****) malloc(j * sizeof(float****))) == NULL) {
    printf("Unable to allocate memory for 5D float array...\n"); 
    exit(0); 
  }
  
  for (i = 0; i < j; i++)
    array[i] = alloc4df(k, l, m, n);

  return array;
}

float ******alloc6df(int i, int j, int k, int l, int m, int n)
{ 
  int h;
  float ******array;
  
  if ((array = (float ******) malloc(i * sizeof(float*****))) == NULL) { 
    printf("Unable to allocate memory for 6D float array...\n");
    exit(0); 
  }
  
  for (h = 0; h < i; h++)    
    array[h] = alloc5df(j, k, l, m, n); 

  return array;
}

float *******alloc7df(int i, int j, int k, int l, int m, int n, int o)
{ 
  int h;
  float *******array;
  
  if ((array = (float *******) malloc(i * sizeof(float******))) == NULL) { 
    printf("Unable to allocate memory for 7D float array...\n"); 
    exit(0);
  }
  
  for (h = 0; h < i; h++)    
    array[h] = alloc6df(j, k, l, m, n, o); 
  
  return array;
}


void dealloc2df(float **array, int m,int n)
{ 
  int i;
  
  for (i = 0; i < m; i++) 
    free(array[i]);
  free(array);
}

void dealloc3df(float ***array, int l, int m, int n)
{ 
  int i;
  for (i = 0; i < l; i++)
    dealloc2df(array[i], m, n);
  free(array);
}

void dealloc4df(float ****array, int k, int l, int m, int n)
{ 
  int i;
  for (i = 0; i < k; i++)
    dealloc3df(array[i], l, m, n);
  free(array);
}


void dealloc5df(float *****array, int j, int k, int l, int m, int n)
{ 
  int i;
  for (i = 0; i < j; i++)
    dealloc4df(array[i], k, l, m, n);
  free(array);
}


void dealloc6df(float ******array, int i, int j, int k, int l, int m, int n)
{ 
  int h;
  for (h = 0; h < i; h++)
    dealloc5df(array[h], j, k, l, m, n);
  free(array);
}


int *alloc1di(int n)
{ 
  int *array;
  
  if ((array = (int *) calloc(n, sizeof(int))) == NULL) {
    printf("Unable to allocate memory for 1D int array...\n"); 
    exit(0); 
  }
  
  return array;
}

int **alloc2di(int m, int n)
{ 
  int i,**array;

  if ((array = (int **) malloc(m * sizeof(int*))) == NULL) { 
    printf("Unable to allocate memory for 2D int array...\n"); 
    exit(0); 
  }

  for (i = 0; i < m; i++)
    array[i] = alloc1di(n);
  
  return array;
}


int ***alloc3di(int l, int m, int n)
{ 
  int i, ***array;
  
  if ((array = (int ***) malloc(l * sizeof(int**))) == NULL) { 
    printf("Unable to allocate memory for 3D int array...\n"); 
    exit(0); 
  }
  
  for (i = 0; i < l; i++)
    array[i] = alloc2di(m, n);
  
  return array;
}

int ****alloc4di(int k, int l, int m, int n)
{ 
  int i, ****array;
  
  if ((array = (int ****) malloc(k * sizeof(int***))) == NULL) { 
    printf("Unable to allocate memory for 4D int array...\n"); 
    exit(0);
  }
  
  for (i = 0; i < k; i++)    
    array[i] = alloc3di(l, m, n);
  
  return array;
}

int *****alloc5di(int j, int k, int l, int m, int n)
{
  int i, *****array;
  
  if ((array = (int *****) malloc(j * sizeof(int****))) == NULL) { 
    printf("Unable to allocate memory for 5D int array...\n"); 
    exit(0); 
  }
  
  for (i = 0; i < j; i++)    
    array[i] = alloc4di(k, l, m, n);
  
  return array;
}

int ******alloc6di(int i, int j, int k, int l, int m, int n)
{ 
  int h, ******array;
  
  if ((array = (int ******) malloc(i * sizeof(int*****))) == NULL) {
    printf("Unable to allocate memory for 6D int array...\n"); 
    exit(0); 
  }
  
  for (h = 0; h < i; h++)    
    array[h] = alloc5di(j, k, l, m, n); 
  
  return array;
}

int *******alloc7di(int i, int j, int k, int l, int m, int n, int o)
{ 
  int h, *******array;
  
  if ((array = (int *******) malloc(i * sizeof(int******))) == NULL) {
    printf("Unable to allocate memory for 7D int array...\n"); 
    exit(0); 
  }
  
  for (h = 0; h < i; h++)    
    array[h] = alloc6di(j, k, l, m, n, o); 
  
  return array;
}


void dealloc2di(int **array, int m, int n)
{ 
  int i;
  
  for (i = 0; i < m; i++) 
    free(array[i]);
  free(array);
}

void dealloc3di(int ***array, int l, int m, int n)
{ 
  int i;
  
  for (i = 0; i < l; i++) 
    dealloc2di(array[i], m, n);
  free(array);
}

void dealloc4di(int ****array, int k, int l, int m, int n)
{ 
  int i;
  
  for (i = 0; i < k; i++) 
    dealloc3di(array[i], l, m, n);
  free(array);
}


void dealloc5di(int *****array, int j, int k, int l, int m, int n)
{ 
  int i;
  
  for (i = 0; i < j; i++) 
    dealloc4di(array[i], k, l, m, n);
  free(array);
}

void dealloc6di(int ******array, int i, int j, int k, int l, int m, int n)
{ 
  int h;
  
  for (h = 0; h < i; h++)
    dealloc5di(array[h], j, k, l, m, n);
  free(array);
}

unsigned char *alloc1db(n)
int n;
{ unsigned char i, *array;
  if ( (array = (unsigned char*)malloc(n * sizeof(unsigned char)) )==NULL)
    { printf("Unable to allocate memory for 1D int array...\n"); exit(0); }
  for (i=0; i<n; i++) array[i]=0.0;
  return array;
}

unsigned char **alloc2db(m, n)
int m,n;
{ unsigned char i,**array;
  if ( (array = (unsigned char **) malloc(m * sizeof(unsigned char*)) )==NULL) 
    { printf("Unable to allocate memory for 2D int array...\n"); exit(0); }
  for (i=0; i<m; i++)    array[i]=alloc1db(n);
  return array;
}


unsigned char ***alloc3db(l, m, n)
int l,m,n;
{ unsigned char i, ***array;
  if ( (array = (unsigned char ***) malloc(l * sizeof(unsigned char**)) )==NULL) 
    { printf("Unable to allocate memory for 3D int array...\n"); exit(0); }
  for (i=0; i<l; i++)    array[i]=alloc2db(m,n);
  return array;
}

unsigned char ****alloc4db(k, l, m, n)
int k,l,m,n;
{ unsigned char i, ****array;
  if ( (array = (unsigned char ****) malloc(k * sizeof(unsigned char***)) )==NULL) 
    { printf("Unable to allocate memory for 4D int array...\n"); exit(0); }
  for (i=0; i<k; i++)    array[i]=alloc3db(l,m,n);
  return array;
}

unsigned char *****alloc5db(j, k, l, m, n)
int j,k,l,m,n;
{ unsigned char i, *****array;
  if ( (array = (unsigned char *****) malloc(j * sizeof(unsigned char****)) )==NULL) 
    { printf("Unable to allocate memory for 5D int array...\n"); exit(0); }
  for (i=0; i<j; i++)    array[i]=alloc4db(k,l,m,n);
  return array;
}

unsigned char ******alloc6db(i, j, k, l, m, n)
int i,j,k,l,m,n;
{ unsigned char h, ******array;
  if ( (array = (unsigned char ******) malloc(i * sizeof(unsigned char*****)) )==NULL) 
    { printf("Unable to allocate memory for 6D int array...\n"); exit(0); }
  for (h=0; h<i; h++)    array[h]=alloc5db(j,k,l,m,n); 
  return array;
}


void dealloc2db(array,m,n)
unsigned char **array;
int m,n;
{ int i;
  for (i=0; i<m; i++) free(array[i]);
  free(array);
}

void dealloc3db(array,l,m,n)
unsigned char ***array;
int l,m,n;
{ int i;
  for (i=0; i<l; i++) dealloc2db(array[i],m,n);
  free(array);
}

void dealloc4db(array,k,l,m,n)
unsigned char ****array;
int k,l,m,n;
{ int i;
  for (i=0; i<k; i++) dealloc3db(array[i],l,m,n);
  free(array);
}


void dealloc5db(array,j,k,l,m,n)
unsigned char *****array;
int j,k,l,m,n;
{ int i;
  for (i=0; i<j; i++) dealloc4db(array[i],k,l,m,n);
  free(array);
}

void dealloc6db(array,i,j,k,l,m,n)
unsigned char ******array;
int i,j,k,l,m,n;
{ int h;
  for (h=0; h<i; h++) dealloc5db(array[h],j,k,l,m,n);
  free(array);
}

void write_image(filename,tmp,m,n)
float **tmp;
char filename[];
int m,n;
{ int i,j;
  float v, min, max;
  unsigned char w;
  FILE *fp;

/*  printf("\nDisplay black/white image %s of size (%d, %d).\n",filename,m,n); */

  min=9e9; max=-min;
  for (i=0; i<m; i++)
    for (j=0; j<n; j++)
      { v=tmp[i][j]; if (min>v) min=v; if (max<v) max=v;  }
  v=255.0/(max-min);

  printf("Write image:  min=%f, max=%f, v=%f\n", min,max,v); 

  if ( (fp = fopen(filename, "w")) == NULL) 
    { fprintf(stderr, "Unable to open image %s for writing, exiting.\n",filename);
      exit(0);
    }
  fprintf(fp,"%s\n%d %d\n%d\n", "P5", n,m, 255);
  for (i=0; i<m; i++)
    for (j=0; j<n; j++)
      putc(w=(tmp[i][j]-min)*v,fp);
  fclose(fp);      
}

void display_image(fname,tmp,m,n)
float **tmp;
int m,n;
char *fname;
{ int i,j;
  float min,max,v;
  unsigned char w;
  FILE *fp;

  strcat(fname,".pgm");
  printf("\nDisplay 2D image: '%s'...\n",fname);
  min=9e9; max=-min;
  for (i=0; i<m; i++)
    for (j=0; j<n; j++)
      { v=tmp[i][j]; if (min>v) min=v; if (max<v) max=v; }
  v=255/(max-min);

  if ( (fp = fopen(fname, "w")) == NULL) 
    { fprintf(stderr, "Unable to open %s for writing, exiting.\n",fname);
      exit(0);
    }
  fprintf(fp,"%s\n%d %d\n%d\n", "P5", n,m, 255);
  for (i=0; i<m; i++)
    for (j=0; j<n; j++)
      putc(w=(tmp[i][j]-min)*v,fp);
  fclose(fp);      
}



void print2d_1(a,m,n)
     int m,n;
     float **a;
{
  int i,j;
  for (i=0; i<m; i++)
    {
      for (j=0; j<n; j++)
	printf("%8.4f  ",a[i][j]);
      printf("\n");
    }
}

void print2d_2(a,b,m,n)
     int m,n;
     float **a, **b;
{
  int i,j;
  for (i=0; i<m; i++)
    {
      for (j=0; j<n; j++)
	printf("%7.4f ",a[i][j]);
      printf("    ");
      for (j=0; j<n; j++)
	printf("%7.4f  ",b[i][j]);
      printf("\n");
    }
}

void print4d(a,m,n,M,N)
     int m,n,M,N;
     float ****a;
{
  int i,j,I,J;
/*  printf("print4d: %d %d %d %d\n",m,n,M,N); */
  for (I=0; I<M; I++)
    {
      for (i=0; i<m; i++)
	{
	  for (J=0; J<N; J++)
	    {
	      for (j=0; j<n; j++)
		printf("%6.3f ",a[i][j][I][J]);
	      printf("  ");
	    }
	  printf("\n");
	}
      printf("\n");
    }
}




void display3df(fname,data,N,m,n,bd)
float ***data;
int m,n,N;
char fname[];
{
  unsigned char w;
  int i,j,k,l,I,J,mc,nc;
  float min,max,v;

  strcat(fname,".pgm");
  printf("Display 3D image: %s\n",fname);
  min=9e9; max=-min;
  for (J=0; J<N; J++)
    for (i=0; i<m; i++)
      for (j=0; j<n; j++)
	{ v=data[J][i][j];
	  if (min>v) min=v;  if (max<v) max=v;
	}
  v=255.0/(max-min);
  /*  printf("Display_3D: min=%f, max=%f, v=%f\n",min,max,v);  */

  if ( (fp = fopen(fname, "w")) == NULL) 
    { fprintf(stderr, "Unable to open image %s for writing, exiting.\n",fname);
      exit(0);
    }

  mc=bd+m+bd;  nc=bd+N*(n+bd); 
  fprintf(fp,"%s\n%d %d\n%d\n", "P5", nc,mc, 255);
  for (k=0; k<mc; k++)
    for (l=0; l<nc; l++)
      {
	I=k/(m+bd); i=k-I*(m+bd)-bd;
	J=l/(n+bd); j=l-J*(n+bd)-bd;
	if (i<0 || j<0) w=255;
	else  w=(data[J][i][j]-min)*v;
	putc(w,fp);
      }

  fclose(fp);      
}

void display4df(fname,data,m,n,M,N,order,bd)
float ****data;
int m,n,M,N,order;
char fname[];
{
  FILE *fp;
  unsigned char w;
  int i,j,k,l,I,J,mc,nc;
  float min,max,v;

  /*  strcat(fname, ".pgm"); */
  printf("Display 4D image: %s\n",fname);
  min=9e9; max=-min;
  for (I=0; I<M; I++)
    for (J=0; J<N; J++)
      for (i=0; i<m; i++)
	for (j=0; j<n; j++)
	  { v=data[i][j][I][J];
	    if (min>v) min=v;  if (max<v) max=v;
	  }
  v=255.0/(max-min);
  printf("Display_4D: min=%f, max=%f, v=%f\n",min,max,v);  

  if ( (fp = fopen(fname, "w")) == NULL) 
    { fprintf(stderr, "Unable to open image %s for writing, exiting.\n",fname);
      exit(0);
    }
  if (order)
    { mc=bd+M*(m+bd);  nc=bd+N*(n+bd); 
      fprintf(fp,"%s\n%d %d\n%d\n", "P5", nc,mc, 255);
      for (k=0; k<mc; k++)
	for (l=0; l<nc; l++)
	  {
	    I=k/(m+bd); i=k-I*(m+bd)-bd;
	    J=l/(n+bd); j=l-J*(n+bd)-bd;
	    if (i<0 || j<0) w=255;
	    else  w=(data[i][j][I][J]-min)*v;
	    putc(w,fp);
	  }
    }
  else
    { mc=bd+m*(M+bd);  nc=bd+n*(N+bd); 
      fprintf(fp,"%s\n%d %d\n%d\n", "P5", nc,mc, 255);
      for (k=0; k<mc; k++)
	for (l=0; l<nc; l++)
	  {
	    I=k/(M+bd); i=k-I*(M+bd)-bd;
	    J=l/(N+bd); j=l-J*(N+bd)-bd;
	    if (i<0 || j<0) w=255;
	    else  w=(data[I][J][i][j]-min)*v;
	    putc(w,fp);
	  }
    }

  fclose(fp);      
}


void display5df(fname,data,K,m,n,M,N,order,bd)
float *****data;
int m,n,M,N,K,order;
char fname[];
{
  FILE *fp;
  unsigned char w;
  int i,j,k,l,I,J,ik,mc,nc;
  float min,max,v;

  strcat(fname,".pgm");
  printf("Display D image: %s (%d %d %d %d %d)\n",fname,K,m,n,M,N);
  min=9e9; max=-min;
  for (k=0; k<K; k++)
    for (I=0; I<M; I++)
      for (J=0; J<N; J++)
	for (i=0; i<m; i++)
	  for (j=0; j<n; j++)
	    { v=data[k][i][j][I][J];
	      if (min>v) min=v;  if (max<v) max=v;
	    }
  v=255.0/(max-min);
  printf("Display_5D: min=%f, max=%f, v=%f\n",min,max,v);  

  if ( (fp = fopen(fname, "w")) == NULL) 
    { fprintf(stderr, "Unable to open image %s for writing, exiting.\n",fname);
      exit(0);
    }
  if (order)
    { mc=bd+M*(m+bd);  nc= K*(bd+N*(n+bd));
      fprintf(fp,"%s\n%d %d\n%d\n", "P5", nc,mc, 255);
      for (k=0; k<mc; k++)
	for (l=0; l<nc; l++)
	  {
	    I=k/(m+bd); i=k-I*(m+bd)-bd; ik=l/(bd+N*(n+bd));
	    J=(l-ik*(bd+N*(n+bd)))/(n+bd); j=l-ik*(bd+N*(n+bd))-J*(n+bd)-bd;
	    /*	    printf("%d %d %d %d\n", i, j, I, J); */
	    if (i<0 || j<0) w=255;
	    else  w=(data[ik][i][j][I][J]-min)*v;
	    putc(w,fp);
	  }
    }
  else
    { mc=bd+m*(M+bd);  nc=K*(bd+n*(N+bd));
      fprintf(fp,"%s\n%d %d\n%d\n", "P5", nc,mc, 255);
      for (k=0; k<mc; k++)
	for (l=0; l<nc; l++)
	  {
	    I=k/(M+bd); i=k-I*(M+bd)-bd; ik=l/(bd+n*(N+bd));
	    J=(l-ik*(bd+n*(N+bd)))/(N+bd); j=l-ik*(bd+n*(N+bd))-J*(N+bd)-bd;
	    if (i<0 || j<0) w=255;
	    else  w=(data[ik][I][J][i][j]-min)*v;
	    putc(w,fp);
	  }
    }

  fclose(fp);      


}


void display6df_old(fname,data,p,q,P,Q,PP,QQ,order)
float ******data;
int p,q,P,Q,PP,QQ,order;
char fname[];
{
  unsigned char w;
  int i,j,k,l,I,J,II,JJ,mc,nc,Wi,Wj,Di,Dj;
  int m,n,M,N,MM,NN;
  int bd1=1,bd2=3;
  float min,max,v,******temp;

  i = j = I = J = 0;

  printf("Display6D file: %s, [%d %d %d %d %d %d], order=%d\n",fname,p,q,P,Q,PP,QQ,order);

  if (order)
    { MM=p; NN=q; M=P; N=Q; m=PP; n=QQ;
      temp=alloc6df(MM,NN,M,N,m,n);
    }
  else
    { MM=PP; NN=QQ; M=P; N=Q; m=p; n=q;
      temp=alloc6df(m,n,M,N,MM,NN);
    }

  min=9e9; max=-min;
  for (II=0; II<MM; II++)
    for (JJ=0; JJ<NN; JJ++)
      for (I=0; I<M; I++)
        for (J=0; J<N; J++)
          for (i=0; i<m; i++)
            for (j=0; j<n; j++)
            { 
              if (order)
                v=temp[II][JJ][I][J][i][j]=data[II][JJ][I][J][i][j];
              else
                v=temp[i][j][I][J][II][JJ]=data[i][j][I][J][II][JJ];
              if (min>v) min=v;  if (max<v) max=v;
            }

  v=255.0 / (max-min);
  /*  printf("Display_6D: min=%f, max=%f, v=%f\n",min,max,v);  */
  
  strcat(fname,".pgm");
  if ( (fp = fopen(fname, "w")) == NULL) 
  { 
    fprintf(stderr, "Unable to open image %s for writing, exiting.\n",fname);
    exit(0);
  }

  mc=bd2+MM*(bd2+M*(m+bd1)-bd1);  nc=bd2+NN*(bd2+N*(n+bd1)-bd1);
  printf("Dimensions of input: (%d %d)x(%d %d)x(%d %d)\n",m,n,M,N,MM,NN);
  printf("Dimensions of output: (%d %d)\n",mc,nc);
  
  fprintf(fp,"%s\n%d %d\n%d\n", "P5", nc,mc, 255);
  
  Di=bd2+M*(m+bd1)-bd1; Dj=bd2+N*(n+bd1)-bd1;
  /*  printf("Di=%d, Dj=%d\n",Di,Dj); */
  
  for (k=0; k<mc; k++)
    for (l=0; l<nc; l++) {
      w=160;
      II=k/Di;  Wi=k-II*Di-bd2; 
      if (Wi >= 0) { 
        I=Wi/(m+bd1); 
        i=Wi-I*(m+bd1); 
      } 
      JJ=l/Dj; Wj=l-JJ*Dj-bd2;
      if (Wj >= 0) { 
        J=Wj/(n+bd1); 
        j=Wj-J*(n+bd1); 
      } 
      if (Wi>=0 && Wj>=0 && i<m && j<n) {
        if (order)
          w=(temp[II][JJ][I][J][i][j]-min)*v;  
        else
          w=(temp[i][j][I][J][II][JJ]-min)*v;  
        putc(w,fp);
      }
    }
  fclose(fp);      
}


void display6df(fname,data,p,q,P,Q,PP,QQ,order)
float ******data;
int p,q,P,Q,PP,QQ,order;
char fname[];
{
  unsigned char w;
  int i,j,k,l,I,J,II,JJ,mc,nc,Wi,Wj,Di,Dj;
  int m,n,M,N,MM,NN;
  int bd1=1,bd2=3;
  float min,max,v;

  i = j = I = J = 0;

  printf("Display6D file: %s, [%d %d %d %d %d %d], order=%d\n",fname,p,q,P,Q,PP,QQ,order);

  min=9e9; max=-min;
  for (II=0; II<PP; II++)
    for (JJ=0; JJ<QQ; JJ++)
      for (I=0; I<P; I++)
	for (J=0; J<Q; J++)
	  for (i=0; i<p; i++)
	    for (j=0; j<q; j++)
	      { v=data[i][j][I][J][II][JJ];
		if (min>v) min=v;  if (max<v) max=v;
	      }
  v=255.0/(max-min);
  printf("Display_6D: min=%f, max=%f, v=%f\n",min,max,v); 

  if (order)
    { MM=p; NN=q; M=P; N=Q; m=PP; n=QQ;    }
  else
    { MM=PP; NN=QQ; M=P; N=Q; m=p; n=q;    }

  /*
    strcat(fname,".pgm"); 
    printf("FILEname: %s\n",fname); pause();
  */
  if ( (fp = fopen(fname, "w")) == NULL) 
    { printf("Unable to open image %s for writing, exiting.\n",fname);
      exit(0);
    }

  mc=bd2+MM*(bd2+M*(m+bd1)-bd1);  nc=bd2+NN*(bd2+N*(n+bd1)-bd1);
  printf("Dimensions of input: (%d %d)x(%d %d)x(%d %d)\n",m,n,M,N,MM,NN);
  printf("Dimensions of output: (%d %d)\n",mc,nc);
  fprintf(fp,"%s\n%d %d\n%d\n", "P5", nc,mc, 255);

  Di=bd2+M*(m+bd1)-bd1; Dj=bd2+N*(n+bd1)-bd1;
  /*  printf("Di=%d, Dj=%d\n",Di,Dj); */

  for (k=0; k<mc; k++)
    for (l=0; l<nc; l++)
      {
        w=200;
        II=k/Di;  Wi=k-II*Di-bd2; 
        if (Wi >= 0) { I=Wi/(m+bd1); i=Wi-I*(m+bd1); } 
        JJ=l/Dj; Wj=l-JJ*Dj-bd2;
        if (Wj >= 0) { J=Wj/(n+bd1); j=Wj-J*(n+bd1); } 
        if (Wi>=0 && Wj>=0 && i<m && j<n) {
          if (order)
            w=(data[II][JJ][I][J][i][j]-min)*v;  
          else
            w=(data[i][j][I][J][II][JJ]-min)*v;  
          putc(w,fp);
        }
      }
  fclose(fp);      
}


void display7df(fname,data,p,q,P,Q,PP,QQ,RR,order)
float *******data;
int p,q,P,Q,PP,QQ,RR,order;
char fname[];
{
  unsigned char w;
  int i,j,k,l,I,J,II,JJ,L,mc,nc,Wi,Wj,Di,Dj,DI,DJ;
  int m,n,M,N,MM,NN,LL;
  int bd1=1, bd2=3;
  float min,max,v;

  i = j = I = J = 0;

  printf("Display6D file: %s, [%d %d %d %d %d %d], order=%d\n",fname,p,q,P,Q,PP,QQ,order);

  min=9e9; max=-min;
  for (L=0; L<RR; L++)
    for (II=0; II<PP; II++)
      for (JJ=0; JJ<QQ; JJ++)
	for (I=0; I<P; I++)
	  for (J=0; J<Q; J++)
	    for (i=0; i<p; i++)
	      for (j=0; j<q; j++)
		{ v=data[i][j][I][J][II][JJ][L];
		  if (min>v) min=v;  if (max<v) max=v;
		}
  if (max>min) v=255.0/(max-min); else v=1;
  printf("Display_7D: min=%f, max=%f, v=%f\n",min,max,v);  

  if (order)
    { LL=p; MM=q; NN=P; M=Q; N=PP; m=QQ; n=RR;    }
  else
    { LL=RR; MM=PP; NN=QQ; M=P; N=Q; m=p; n=q;    }

  /*  strcat(fname,".pgm"); */
  if ( (fp = fopen(fname, "w")) == NULL) 
    { fprintf(stderr, "Unable to open image %s for writing, exiting.\n",fname);
      exit(0);
    }

  Di=bd2+M*(m+bd1)-bd1; Dj=bd2+N*(n+bd1)-bd1;
  /*  printf("Di=%d, Dj=%d\n",Di,Dj); */

  DI=bd2+MM*Di; DJ=bd2+NN*Dj;
  mc=DI; nc=LL*DJ;

  printf("Dimensions of input: (%d %d)x(%d %d)x(%d %d)x%d\n",p,q,P,Q,PP,QQ,RR);
  printf("Dimensions of output: (%d %d)\n",mc,nc);

  fprintf(fp,"%s\n%d %d\n%d\n", "P5", nc,mc, 255);

  for (k=0; k<mc; k++)
    for (l=0; l<nc; l++)
      {
	w=160;
	II=k/Di;  Wi=k-II*Di-bd2; 
	if (Wi >= 0) { I=Wi/(m+bd1); i=Wi-I*(m+bd1); } 

	L=l/DJ; JJ=(l-L*DJ)/Dj; Wj=l-L*DJ-JJ*Dj-bd2;
	if (Wj >= 0) { J=Wj/(n+bd1); j=Wj-J*(n+bd1); } 

	/*	printf("i=%d j=%d I=%d J=%d II=%d jj=%d L=%d\n",i,j,I,J,II,JJ,L); */

	if (Wi>=0 && Wj>=0 && i<m && j<n) {
	  if (order) 
	    w=(data[L][II][JJ][I][J][i][j]-min)*v;  
	  else
	    w=(data[i][j][I][J][II][JJ][L]-min)*v;  
  	putc(w,fp);
    }
      }
  fclose(fp);  
}


void display4di(fname,data,m,n,M,N,order,bd)
int ****data;
int m,n,M,N,order;
char fname[];
{
  FILE *fp;
  unsigned char w;
  int i,j,k,l,I,J,mc,nc;
  float min,max,v;

  strcat(fname,".pgm");
  printf("\nDisplay 4D image: %s\n",fname);
  min=9e9; max=-min;
  for (I=0; I<M; I++)
    for (J=0; J<N; J++)
      for (i=0; i<m; i++)
	for (j=0; j<n; j++)
	  { v=data[i][j][I][J];
	    if (min>v) min=v;  if (max<v) max=v;
	  }
  v=255.0/(max-min);

  if ( (fp = fopen(fname, "w")) == NULL) 
    { fprintf(stderr, "Unable to open image %s for writing, exiting.\n",fname);
      exit(0);
    }
  
  if (order)          /* (MxN) subimages of size (mxn) */
    { mc=bd+M*(m+bd);  nc=bd+N*(n+bd); 
      fprintf(fp,"%s\n%d %d\n%d\n", "P5", nc,mc, 255);
      for (k=0; k<mc; k++)
	for (l=0; l<nc; l++)
	  {
	    I=k/(m+bd); i=k-I*(m+bd)-bd;
	    J=l/(n+bd); j=l-J*(n+bd)-bd;
	    if (i<0 || j<0) w=255;
	    else  w=(data[i][j][I][J]-min)*v;
	    putc(w,fp);
	  }
    }
  else                /* (mxn) subimages of size (MxN) */
    { mc=bd+m*(M+bd);  nc=bd+n*(N+bd); 
      fprintf(fp,"%s\n%d %d\n%d\n", "P5", nc,mc, 255);
      for (k=0; k<mc; k++)
	for (l=0; l<nc; l++)
	  {
	    I=k/(M+bd); i=k-I*(M+bd)-bd;
	    J=l/(N+bd); j=l-J*(N+bd)-bd;
	    if (i<0 || j<0) w=255;
	    else  w=(data[I][J][i][j]-min)*v;
	    putc(w,fp);
	  }
    }

  fclose(fp);      
}

void display4db(fname,data,m,n,M,N,bd)
unsigned char ****data;
int m,n,M,N;
char fname[];
{
  FILE *fp;
  unsigned char w;
  int i,j,k,l,I,J,mc,nc;
  float min,max,v;

  strcat(fname,".pgm");
  min=9e9; max=-min;
  for (I=0; I<M; I++)
    for (J=0; J<N; J++)
      for (i=0; i<m; i++)
	for (j=0; j<n; j++)
	  { v=data[i][j][I][J];
	    if (min>v) min=v;  if (max<v) max=v;
	  }
  v=255.0/(max-min);
/*  printf("Display_4D: min=%f, max=%f, v=%f\n",min,max,v);  */

  if ( (fp = fopen(fname, "w")) == NULL) 
    { fprintf(stderr, "Unable to open image %s for writing, exiting.\n",fname);
      exit(0);
    }

  mc=bd+M*(m+bd);  nc=bd+N*(n+bd); 
/*  printf("[%d %d] = (%d %d) x (%d %d)\n",mc,nc,M,N,m,n);   */
  fprintf(fp,"%s\n%d %d\n%d\n", "P5", nc,mc, 255);

  for (k=0; k<mc; k++)
    for (l=0; l<nc; l++)
      {
	I=k/(m+bd); i=k-I*(m+bd)-bd;
	J=l/(n+bd); j=l-J*(n+bd)-bd;
	if (i<0 || j<0) w=255;
	else  w=(data[i][j][I][J]-min)*v;
	putc(w,fp);
      }
  fclose(fp);      
}


