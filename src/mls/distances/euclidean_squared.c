/*****************************************************************************
 *
 * euclidean_squared.c
 *
 * Computes the Euclidean squared distance which is faster than Euclidean
 * and just as useful where only magnitude _comparison_ is needed. i.e.
 * KMeans
 *
 ****************************************************************************/

#include "euclidean_squared.h"

/* Note the _pre_ increment to start at index position 1 */

inline double euclidean_squared(double *v1, double *v2, int n)
{
  double diff, sum = 0.0;

  while (n-- > 0) {
    diff = *(++v2) - *(++v1);
    sum += diff*diff;
  }

  return sum;
}

inline void euclidean_squared2(double *v1, double **m2, int n, int m, 
                               double *v3)
{
  while (m-- > 0) 
    *(++v3) = euclidean_squared(v1, *(++m2), n);
}
