#ifndef _EUCLIDEAN_H
#define _EUCLIDEAN_H

inline double euclidean(double *v1, double *v2, int n);
inline void euclidean2(double *v1, double **m2, int n, int m, double *v3);
inline void euclidean_mean(double *v1, double *v2, double *v3, int n);
inline void euclidean_weighted_mean(double a1, double *v1, double a2, 
                                    double *v2, double *v3, int n);
#endif
