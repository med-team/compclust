#ifndef _DIST_METRICS_H
#define _DIST_METRICS_H

enum {
  EUCLIDEAN = 0,               /* Euclidean distance */
  EUCLIDEAN_SQUARED,           /* Euclidean squared  */
  CORRELATION,                 /* Correlation metric */
  CANBERRA                     /* Canberra metric    */
};

double distance(double *v1, double *v2, int n, int type);
void   distance2(double *v1, double **m2, int n, int m, double *v3, int type);
int    find_metric(const char *s);
void   test_metrics(void);

#endif
