CURDIR := $(MLSDIR)/distances

DISTSRC := $(wildcard $(CURDIR)/*.c)

SRC += $(DISTSRC)
CFLAGS += -I$(CURDIR)

TARGET := $(CURDIR)/libdist$(LIBEXT)
TARGETLIBS += $(TARGET)

$(TARGET): $(DISTSRC:.c=$(LIBOBJEXT))
	$(AR) rv $@ $?
	$(call bless_library,$@)
