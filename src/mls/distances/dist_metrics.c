/*****************************************************************************
 *
 * dist_metrics.c
 *
 * Perform one of a variaety of distance metric measurements
 *
 ****************************************************************************/

#include <stdio.h>
#include <string.h>
#include "dist_metrics.h"
#include "euclidean.h"
#include "euclidean_squared.h"
#include "correlation.h"
#include "canberra.h"

/*****************************************************************************
 *
 * double distance(double *v1, double *v2, int n, int type)
 *
 * Computes a vector-vector distance product and returns the value.  The
 * type parameters choses the particular metric.  Both vectors must be
 * the same size.
 *
 ****************************************************************************/

double distance(double *v1, double *v2, int n, int type)
{
  double dist;

  switch(type) {
  case EUCLIDEAN:
    dist = euclidean(v1, v2, n);
    break;

  case EUCLIDEAN_SQUARED:
    dist = euclidean_squared(v1, v2, n);
    break;
    
  case CORRELATION:
    dist = 1.0 - correlation(v1, v2, n);
    break;

  case CANBERRA:
    dist = canberra(v1, v2, n);
    break;

  default:
    dist = 0.0;
    fprintf(stderr, "Unknown distance metric: %d\n", type);
    break;
  }

  return dist;
}

/*****************************************************************************
 *
 * double distance2(double *v1, double **m2, int n, int m, double *v3, 
 *                  int type)
 *
 * Computes a vector-matrix distance product and stored the value in v3.  The
 * type parameters choses the particular metric.  The matrix must have the same
 * number of columns as the length of the vector. i.e. m2 is an mxn matrix.
 *
 * This function may be more efficient than repeatedly calling distance().
 *
 ****************************************************************************/

void distance2(double *v1, double **m2, int n, int m, double *v3, int type)
{
  int i;

  switch(type) {
    
  case EUCLIDEAN:
    euclidean2(v1, m2, n, m, v3);  
    break;

  case EUCLIDEAN_SQUARED:
    euclidean_squared2(v1, m2, n, m, v3);
    break;
    
    /***
     *
     * By default fall back to the distance() function 
     *
     **/

  default:
    for (i = 1; i <= m; i++) 
      v3[i] = distance(v1, m2[i], n, type);
    break;
  }
}

/******************************************************************************
 *
 * int find_metric(const char *s)
 *
 * Tries to find a distance metric matching the string name.  Returns -1
 * if no appropriate metric match is found.
 *
 *****************************************************************************/

int find_metric(const char *s)
{
  int type = -1;

  if (strncasecmp(s, "euclidean", 9) == 0)
    type = EUCLIDEAN;

  if (strncasecmp(s, "euclidean_squared", 17) == 0)
    type = EUCLIDEAN_SQUARED;
  
  if (strncasecmp(s, "correlation", 11) == 0)
    type = CORRELATION;

  if (strncasecmp(s, "canberra", 8) == 0)
    type = CANBERRA;

  return type;
}

/******************************************************************************
 *
 * test_metrics
 *
 * Perform basic test that the distances satisfy the metric criteria
 *
 *****************************************************************************/

void test_metrics(void)
{
  const int num_metrics = 4;
  const char *metrics[] = {"euclidean",
                           "euclidean_squared",
                           "correlation",
                           "canberra"};
  int i, type;

  /* NR style vectors */
  double f[4] = {0.0, 1.0, 1.0, 1.0};
  double g[4] = {0.0, 1.0, 2.0, 3.0};
  double tmp;

  for (i = 0; i < num_metrics; i++) {

    type = find_metric(metrics[i]);

    fprintf(stderr, "%s = %d\n", metrics[i], type);
    
    if (type == -1) {
      fprintf(stderr, "Invalid Metric ID\n");
      continue;
    }

    /* Assert the d(f,g) >= 0 */

    if (distance(f, g, 3, type) < 0.0) {
      fprintf(stderr, "Failed positivity\n");
      continue;
    }
    
    /* Assert d(f,f) == 0 */

    if (abs(distance(f, f, 3, type)) != 0.0) {
      fprintf(stderr, "distance = %f\n", distance(f, f, 3, type)); 
      fprintf(stderr, "Failed identity\n");
      continue;
    }

    /* Assert d(f,g) == d(g,f) */

    tmp = abs(distance(f, g, 3, type) - distance(g, f, 3, type));
    if (tmp != 0) {
      fprintf(stderr, "diff = %f\n", tmp);
      fprintf(stderr, "Failed symmetry\n");
      continue;
    }
  }
}


