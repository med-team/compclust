/******************************************************************************
 *
 * canberra.c
 *
 * Compute the Canberra distance metric
 *
 *                  n  |x[i] - y[i]|
 * Canberra(x,y) = SUM ------------- 
 *                 i=0 |x[i]|+|y[i]|
 *
 *
 * This metric is very sensitive about entries which are near-zero.
 *
 * If both x[i] and y[i] are equal to zero, the value is 0 since
 * 
 * lim  Canberra(x, 0) = 0
 * x->0
 *
 * lim  Canberra(0, y) = 0
 * y->0
 *
 * By L'Hopital's rule.  Substitue sqrt(x^2) for |x| and then apply as normal.
 *
 * Also, for any metric D(0,0) = 0, so the above can be directly inferred
 *
 *****************************************************************************/

#include <math.h>
#include "canberra.h"

double canberra(double *v1, double *v2, int n)
{
  double sum = 0.0, denom;

  while (n-- > 0) {
    ++v1; ++v2;

    denom = (fabs(*v1) + fabs(*v2));
    if (denom == 0.0)
      continue;

    sum += fabs(*v1 - *v2) / denom;
  }

  return sum;
}
