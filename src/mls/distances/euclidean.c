/******************************************************************************
 *
 * euclidean.c
 *
 * Compute Euclidean distance between two vecotors
 *
 ****************************************************************************/

#include <math.h>
#include "euclidean.h"
#include "euclidean_squared.h"

inline double euclidean(double *v1, double *v2, int n)
{
  return sqrt(euclidean_squared(v1, v2, n));
}

inline void euclidean2(double *v1, double **m2, int n, int m, double *v3)
{
  while (m-- > 0) 
    *(++v3) = sqrt(euclidean_squared(v1, *(++m2), n));
}

/* Note the _pre_ increment to start at index position 1 */
inline void euclidean_mean(double *v1, double *v2, double *v3, int n)
{
  while (n-- > 0)
    *(++v3) = (*(++v1) + *(++v2)) * 0.5;
}

inline void euclidean_weighted_mean(double a1, double *v1, double a2, 
                                    double *v2, double *v3, int n)
{
  double denom = 1.0 / (a1 + a2);
  while (n-- > 0)
    *(++v3) = ((a1 * (*(++v1))) + (a2 * (*(++v2)))) * denom;
}
