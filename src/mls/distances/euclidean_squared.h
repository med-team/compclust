#ifndef _EUCLIDEAN_SQUARED_H
#define _EUCLIDEAN_SQUARED_H

inline double euclidean_squared(double *v1, double *v2, int n);
inline void euclidean_squared2(double *v1, double **m2, int n, int m,
                               double *v3);

#endif
