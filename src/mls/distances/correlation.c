/******************************************************************************
 *
 * correlation.c
 *
 * Compute the correlation of two vectors 
 *
 *              x . y
 * Corr(x,y) = ------- = cos t
 *             |x| |y|
 *
 *****************************************************************************/

#include <math.h>
#include "correlation.h"

double correlation(double *v1, double *v2, int n)
{
  double mag_x, mag_y, dot_xy;
  
  /**
   *
   * This could be written in terms of Euclidean distance calls, but in the
   * interest of speed it is done here
   *
   *
   *            euclidean_squared(v1,v2)
   *  --------------------------------------------  = Corr(v1,v2) 
   *  (euclidean(v1,origin) * euclidean(v2, origin)
   **/

  mag_x = mag_y = dot_xy = 0.0;
  while (n-- > 0) {
    ++v1; ++v2;
    dot_xy += (*v1) * (*v2);
    mag_x  += (*v1) * (*v1);
    mag_y  += (*v2) * (*v2);
  }
  
  /* OLD CODE
  for (j = 1; j <= n; j++) {
    dot_xy += (v1[j] * v2[j]);
    mag_x  += (v1[j] * v1[j]);
    mag_y  += (v2[j] * v2[j]);
  }
  */

  return dot_xy / (sqrt(mag_x) * sqrt(mag_y));
}
