/*****************************************************************************
 *
 * random_sample.c
 *
 * Initializes means by sampling a subset of points and estimating the mean
 * from those points
 *
 * Copyright 2001, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * Authors: Ben Bornstein & Lucas Scharenbroich
 *
 * @see init_means_random_sample
 *
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "random_sample.h"

/*****************************************************************************
 *
 * init_means_random_sample(struct init_means_data *d, int samples)
 *
 * Initializes the means prior to classification by sampling samples 
 * datapoints (without replacement) for each class and using those samples
 * to estimate the initial class means.
 *
 ****************************************************************************/

void random_sample(double **data, double **class_means, int datapoints, 
                   int features, int classes, long seed, int samples)
{
  int base, class, feature, i, j, index, N, sample, temp;
  int *indices;

  SRAND(seed);

  /**
   *
   * Cannot sample more points than exist in the dataset
   *
   **/

  if (samples > datapoints)
    samples = datapoints;
  
  /**
   *
   * Indices will contains the index of each datapoint in d->data.  This
   * can be used to efficiently sample N datapoints without replacement.
   *
   **/
      
  indices = (int *) malloc(sizeof(int) * (datapoints + 1));

  /**
   *
   * Initialize indices[] to contain the indexes of each datapoint
   * in d->data[][].
   *
   **/

  for (i = 1; i <= datapoints; i++)
  {
    indices[i] = i;
  }

  /**
   *
   * Sample (d->classes * samples) datapoints without replacement.  
   * To do this:
   *
   *   1. Swap an element indices chosen at random with the first element.
   *   2. Repeat for the second element, and on, up to N elements.
   *
   * Be sure not to try to sample more datapoints than are in the dataset
   *
   **/
 
  N = classes * samples;
  if (N > datapoints)
    N = datapoints;

  for (i = 1; i <= N; i++)
  {
    index = (RAND() % datapoints) + 1;

    /* Swap */
    temp           = indices[i];
    indices[i]     = indices[index];
    indices[index] = temp;
  }

  /**
   *
   * The first N elements of indices[] are the indexes of chosen datapoints.
   * Use these points to estimate the initial class means.
   *
   **/

  for (i = 1; i <= classes; i++)
  {
    for (j = 1; j <= features; j++)
    {
      class_means[i][j] = 0.0;
    }
  }

  for (class = 1; class <= classes; class++)
  {

    /**
     *
     * The base adjusts to maximize the number of distinct samples per
     * class
     *
     **/

    if (N < datapoints)
      base = (class - 1) * samples;
    else
      base = ((datapoints - samples) * (class - 1)) / classes;

    for (sample = 1; sample <= samples; sample++)
    {
      index = indices[base + sample];

      for (feature = 1; feature <= features; feature++)
      {
        class_means[class][feature] += data[index][feature];
      }
    }
  }

  /**
   *
   * Divide running sum by samples to estimate means.
   *
   **/

  for (class = 1; class <= classes; class++)
  {
    for (feature = 0; feature <= features; feature++)
    {
      class_means[class][feature] /= (double) samples;
    }
  }

  free(indices);
}
