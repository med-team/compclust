/*****************************************************************************
 *
 * from_file.c
 *
 * Reads the means in from the specified file.  The file must contain
 * <classes> rows of <features> tab-separated numbers.  Each line should
 * end with a newline
 *
 ****************************************************************************/

#include <stdio.h>
#include "from_file.h"

void from_file(double **means, int features, int classes, char *file)
{
  FILE *fp = NULL;
  int feature, class;
  int status = 0;
  float val;

  if (file == NULL) {
    fprintf(stderr, "NULL filename\n");
    status = 1;
  }

  if (!status) {
    fp = fopen(file, "r");
    if (fp == NULL) {
      fprintf(stderr, "Could not open file %s\n", file);
      status = 1;
    }
  }

  if (!status) {
    for (class = 1; class <= classes; class++) 
      for (feature = 1; feature <= features; feature++) {
        fscanf(fp, "%f", &val);
        means[class][feature] = (double) val;
      }
    fclose(fp);
  }

  if (status) 
    for (class = 1; class <= classes; class++) 
      for (feature = 1; feature <= features; feature++) 
        means[class][feature] = 0.0;
    
}

