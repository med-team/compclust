#ifndef __CHURCH_MEANS_H
#define __CHURCH_MEANS_H

#define DEBUG_CHURCH 0

struct church_data {
  double **data;
  double **class_means;
  int datapoints;
  int features;
  int classes;
};

void church_means(double **data, double **means, int num_datapoints, 
                  int num_features, int num_classes);

#endif
