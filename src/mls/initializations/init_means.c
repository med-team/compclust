/*****************************************************************************
 *
 * init_methods.c
 *
 * A collection of different mean initializtion methods
 *
 * Copyright 2001, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * Authors: Ben Bornstein & Lucas Scharenbroich
 *
 *****************************************************************************/

#include <stdio.h>

#include "church_means.h"
#include "random_means.h"
#include "random_sample.h"
#include "random_range.h"
#include "init_means.h"
#include "from_file.h"

void init_means(double **data, double **means, int datapoints, 
                int features, int classes, int type, long seed, 
                int samples, char *file)
{
  switch(type) {
  case CHURCH_MEANS:
    church_means(data, means, datapoints, features, classes);
    break;

  case RANDOM_MEANS:
    random_means(data, means, datapoints, features, classes, seed);
    break;

  case RANDOM_POINT:
    random_sample(data, means, datapoints, features, classes, seed, 1);
    break;

  case RANDOM_RANGE:
    random_range(data, means, datapoints, features, classes, seed);
    break;

  case RANDOM_SAMPLE:
    random_sample(data, means, datapoints, features, classes, seed, samples);
    break;

  case FROM_FILE:
    from_file(means, features, classes, file);
    break;

  default:
    fprintf(stderr, "Unknown mean initializtion type: %d\n", type);
    break;
  }
}

