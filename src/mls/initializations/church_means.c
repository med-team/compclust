/*****************************************************************************
 *
 * church.c
 *
 * Contains code to perform Church means initialization
 *
 * @see init_means_church
 *
 * Copyright 2001, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * Authors: Ben Bornstein & Lucas Scharenbroich
 *
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "church_means.h"

/*****************************************************************************
 *
 * array_contains(int *array, int N, int value)
 *
 * Returns 1 if the N element array contains value at least once, 0
 * otherwise.
 *
 ****************************************************************************/

static int array_contains(int *array, int N, int value)
{
  int i;
  int found = 0;


  for (i = 1; i <= N; i++)
  {
    if (array[i] == value)
    {
      found = 1;
      break;
    }
  }

  return found;
}


/*****************************************************************************
 *
 * copy_datum(double *source, double *destination, int N)
 *
 * Copies the contents of source to destination, both with N elements.
 *
 ****************************************************************************/

static void copy_datum(double *source, double *destination, int N)
{
  int i;


  for (i = 1; i <= N; i++)
  {
    destination[i] = source[i];
  }
}

/*****************************************************************************
 *
 * find_distance_to_means(double *datum, strict church_data *d, int N)
 *
 * Returns the sum of all distances of datum to the first N means.
 *
 * Where:
 *   means = d->class_means[i][j]
 *   i     = [0 ... (N           - 1)]
 *   j     = [0 ... (d->features - 1)]
 *
 *   datum   must be a pointer to an array of doubles with d->features
 *           number of elements.
 *
 * @see init_means_church
 *
 ****************************************************************************/

static double local_euclidean(double *v1, double *v2, int n)
{
  double diff, sum = 0.0;
  int i;
  
  for (i = 1; i <= n; i++) {
    diff = v1[i] - v2[i];
    sum += diff*diff;
  }

  return sqrt(sum);
}

static double find_distance_to_means(double *datum, struct church_data *d, int N)
{
  double dist = 0.0;
  int    i;


  if (DEBUG_CHURCH > 1)
  {
    fprintf( stderr, "  Entering find_distance_to_means()\n" );
    fprintf( stderr, "    N = [%d].\n", N );
  }


  for (i = 1; i <= N; i++)
  {
    dist += local_euclidean(datum, d->class_means[i], d->features);
  }


  if (DEBUG_CHURCH > 1)
  {
    fprintf( stderr, "  Exiting  find_distance_to_means()\n" );
  }


  return dist;
}


/*****************************************************************************
 *
 * find_farthest_dataum_from_means(struct church_data *d, int *indices, 
 *                                 int N)
 *
 * Returns the index of the datum farthest from the first N means.  Indices
 * contains the indices of the N datum already chosen as means, to avoid
 * choosing them again.
 *
 *
 * Where:
 *   data  = d->data[i][j]
 *   means = d->class_means[i][j]
 *   i     = [0 ... (d->datapoints - 1)]
 *   j     = [0 ... (d->features   - 1)]
 *
 *
 * @see init_means_church
 *
 *****************************************************************************/

static int find_farthest_datum_from_means(struct church_data *d, int *indices, 
                                   int N)
{
  double dist    = -1.0;
  double maximum = -1.0;
  int    index   = -1;
  int    i;


  if (DEBUG_CHURCH > 1)
  {
    fprintf( stderr, "  Entering find_farthest_datum_from_means()\n" );
    fprintf( stderr, "    N = [%d].\n", N );
  }


  for (i = 1; i <= d->datapoints; i++)
  {
    if (array_contains(indices, N, i) == 1)
    {
      continue;
    }

    dist = find_distance_to_means(d->data[i], d, N);

    if (dist > maximum)
    {
      index   = i;
      maximum = dist;
    }
  }


  if (DEBUG_CHURCH > 1)
  {
    fprintf( stderr, "  Exiting  find_farthest_datum_from_means()\n");
  }

  return index;
}


/*****************************************************************************
 *
 * find_centroid(struct church_data *d, long double *centroid)
 *
 * Computes the centroid of the data and places the result in center.
 *
 * Where:
 *   data   = d->data[i][j]
 *   i      = [0 ... (d->datapoints - 1)]
 *   j      = [0 ... (d->features   - 1)]
 *
 *   center   must be a pointer to an array of long doubles with d->features
 *            number of elements.
 *
 * @see init_means_church
 *
 ****************************************************************************/

static void find_centroid(struct church_data *d, double *centroid)
{
  int i, j;

  for (i = 1; i <= d->features; i++)
  {
    centroid[i] = 0;
  }

  /**
   *
   * Accumulate running sum of d->data[i].
   *
   **/

  for (i = 1; i <= d->datapoints; i++)
  {
    for (j = 1; j <= d->features; j++)
    {
      centroid[j] += d->data[i][j];
    }
  }

  /**
   *
   * Divide running sum by the number of datapoints to find the centroid of
   * the data.
   *
   **/

  for (j = 1; j <= d->features; j++)
  {
    centroid[j] /= (double) d->datapoints;
  }
}

/*****************************************************************************
 *
 * Initializes the means prior to classification using Church style
 * initialization.
 *
 * According to page 285 of the following paper:
 *
 *   Systematic Determination of Genetic Network Architecture
 *   Tavozoie, Hughes, Campbell, Cho, and Church
 *   Nature Genetics, Volume 22, July 1999, pp. 281--285
 *
 *   "We used an implementation of k-means in the statistical software
 *    package SYSSTAT 7.0 (SPSS).  The first cluster centre was chosen as
 *    the centroid of the entire data set and subsequent centres were chosen
 *    by finding the data point farthest from the centres already chosen."
 *
 * @see init_means
 *
 *****************************************************************************/

void church_means(double **data, double **means, int datapoints, 
                  int features, int classes)
{
  int index = 0;
  int i;
  int *indices;
  struct church_data d;


  if (DEBUG_CHURCH)
  {
    fprintf( stderr, "  Entering init_means_church()\n");
  }

  /**
   *
   * Initialize the internal data structure
   *
   **/

  d.class_means = means;
  d.data        = data;
  d.datapoints  = datapoints;
  d.features    = features;
  d.classes     = classes;

  /**
   *
   * Stores the indices of datapoints chosen as means (centers),
   * to avoid choosing them again.
   *
   **/

  indices = (int *) malloc(sizeof(int) * (d.classes + 1));

  /**
   *
   * The first mean chosen is not an actual data-point, so its index
   * is -1.
   *
   **/

  find_centroid(&d, d.class_means[1]);
  indices[1] = -1;

  for (i = 2; i <= d.classes; i++)
  {
    index = find_farthest_datum_from_means(&d, indices, i);

    indices[i] = index;
    copy_datum(d.data[index], d.class_means[i], d.features);
  }

  free(indices);

  if (DEBUG_CHURCH)
  {
    fprintf( stderr, "  Exiting  init_means_church()\n");
  }
}
