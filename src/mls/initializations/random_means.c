/*****************************************************************************
 *
 * random_means.c
 *
 * Initialize means by randomly classifying all the datapoints and then
 * estimating the class means.
 *
 * Copyright 2001, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * Authors: Ben Bornstein & Lucas Scharenbroich
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "random_means.h"

/*****************************************************************************
 *
 * init_classifications_random(struct random_data *d)
 *
 * Initializes the classification field of the kmeans_data structure.
 * For each datum, a classification is chosen at random.
 *
 * @see init_means_random kmeans
 *
 ****************************************************************************/

static void init_classifications_random(struct random_means_data *d)
{
  int class  = 1;
  int point  = 1;
  int index1 = 1;
  int index2 = 1;

  if (DEBUG_RANDOM_MEANS)
  {
    fprintf( stderr, "  Entering init_classifications_random()\n");
  }

  /**
   *
   * Sequentially assign each datapoint to a class.
   *
   **/

  for (point = 1; point <= d->datapoints; point++)
  {
    d->classifications[point] = class;
    d->class_count_array[class]++;

    class++;

    if (class > d->classes)
    {
      class = 1;
    }
  }

  /**
   *
   * Shuffle datapoint classifications by swapping classifications
   * d->datapoints times.
   *
   * NOTE:
   *   d->class_count_array[] need not be updated, as the number
   *   of datapoints in each class remains constant.  Only the
   *   classification of each datapoint changes.
   *
   **/

  for (point = 1; point <= d->datapoints; point++)
  {
    index1 = (RAND() % d->datapoints) + 1;
    index2 = (RAND() % d->datapoints) + 1;

    /* Swap */
    class                      = d->classifications[index1];
    d->classifications[index1] = d->classifications[index2];
    d->classifications[index2] = class;
  }


  if (DEBUG_RANDOM_MEANS)
  {
    fprintf( stderr, "  Exiting  init_classifications_random()\n");
  }
}

/*****************************************************************************
 *
 * Estimates the means of k classes using the class membership of each
 * datum.
 *
 * Where:
 *   k           = d->classes
 *   memberships = d->classifications
 *   datum       = d->data[i][]
 *   i           = [0 ... (d->datapoints - 1)]
 *   j           = [0 ... (d->features   - 1)]
 *
 *****************************************************************************/

static void estimate_means(struct random_means_data *d)
{
  int class, i, j;


  if (DEBUG_RANDOM_MEANS)
  {
    fprintf( stderr, "  Entering estimate_means()\n");
  }

  for (i = 1; i <= d->classes; i++)
  {
    for (j = 1; j <= d->features; j++)
    {
      d->class_means[i][j] = 0.0;
    }
  }

  /**
   *
   * Accumulate running sum of d->data[i] based on class membership.
   *
   **/

  for (i = 1; i <= d->datapoints; i++)
  {
    class = d->classifications[i];

    for (j = 1; j <= d->features; j++)
    {
      d->class_means[class][j] += d->data[i][j];
    }
  }

  /**
   *
   * Divide running sum by class size to estimate means.
   *
   **/

  for (i = 1; i <= d->classes; i++)
  {
    for (j = 1; j <= d->features; j++)
    {
      d->class_means[i][j] /= (double) d->class_count_array[i];
    }
  }


  if (DEBUG_RANDOM_MEANS)
  {
    fprintf( stderr, "  Exiting  estimate_means()\n");
  }
}


/****************************************************************************
 *
 * init_means_random(double **data, double **means, int datapoints, 
 *                   int features, int classes)
 *
 * Initializes the means prior to classification, by placing them
 * randomly throughout the data.
 *
 ****************************************************************************/

void random_means(double **data, double **means, int datapoints, 
                  int features, int classes, long seed)
{
  struct random_means_data d;

  d.data              = data;
  d.class_means       = means;
  d.datapoints        = datapoints;
  d.features          = features;
  d.classes           = classes;
  d.classifications   = (int *) calloc(sizeof(int), (datapoints + 1));
  d.class_count_array = (int *) calloc(sizeof(int), (classes + 1));

  SRAND(seed);

  init_classifications_random(&d);
  estimate_means(&d);

  free(d.classifications);
  free(d.class_count_array);
}




