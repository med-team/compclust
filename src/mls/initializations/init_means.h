#ifndef __INIT_MEANS_H
#define __INIT_MEANS_H

enum {
  CHURCH_MEANS = 1,
  RANDOM_MEANS,
  RANDOM_POINT,
  RANDOM_RANGE,
  RANDOM_SAMPLE,
  FROM_FILE
};

void init_means(double **data, double **means, int num_datapoints, 
                int num_features, int num_classes, int type, long seed,
                int samples, char *file);

#endif
