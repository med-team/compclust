#ifndef __RANDOM_SAMPLE_H
#define __RANDOM_SAMPLE_H

#include "rand.h"

void random_sample(double **data, double **class_means, int datapoints, 
                   int features, int classes, long seed, int samples);

#endif
