CURDIR := $(MLSDIR)/initializations

INITSRC := $(addprefix $(CURDIR)/, init_means.c church_means.c random_means.c\
                                   random_range.c random_sample.c from_file.c)
SRC += $(INITSRC)
CFLAGS += -I$(CURDIR)

TARGET := $(CURDIR)/libinit$(LIBEXT)
TARGETLIBS += $(TARGET)

$(TARGET): $(INITSRC:.c=$(OBJEXT))
	$(AR) rv $@ $?
	$(call bless_library,$@)
