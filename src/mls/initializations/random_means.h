#ifndef __RANDOM_MEANS_H
#define __RANDOM_MEANS_H

#define DEBUG_RANDOM_MEANS 0

#include "rand.h"

struct random_means_data {
  double **data;
  double **class_means;
  int datapoints;
  int features;
  int classes;
  int *classifications;
  int *class_count_array;
};

void random_means(double **data, double **means, int datapoints, 
                  int features, int classes, long seed);

#endif
