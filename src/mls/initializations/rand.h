#ifndef __RAND_H_
#define __RAND_H

/* allow changing which rand function to use. */
#if 1
#define SRAND srand
#define RAND rand
#define DRAND (double)rand
#else
#define SRAND srand48
#define RAND lrand48
#define DRAND drand48
#endif

#endif
