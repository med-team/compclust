#ifndef __FROM_FILE_H
#define __FROM_FILE_H

void from_file(double **means, int features, int classes, char *file);

#endif
