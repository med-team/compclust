#ifndef __RANDOM_RANGE_H
#define __RANDOM_RANGE_H

#define DEBUG_RANDOM_RANGE 0

#include "rand.h"

struct range_data {
  double **data;
  double **class_means;
  int datapoints;
  int features;
  int classes;
};

void random_range(double **data, double **means, int datapoints, 
                  int features, int classes, long seed);

#endif
