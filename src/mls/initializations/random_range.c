
/******************************************************************************
 *
 * void find_min_max(struct range_data *d, double *min, double *max)
 *
 * Finds the minimum and maximum values of the data (m->data) along
 * each dimension.
 *
 * Where:
 *   data = d->data[i][j]
 *   i    = [0 ... (d->datapoints - 1)]
 *   j    = [0 ... (d->features   - 1)]
 *
 *   min    must be a pointer to an array of doubles with d->features
 *          number of elements.
 *
 *   max    must be a pointer to an array of doubles with d->features
 *          number of elements.
 *
 * @see init_means_random kmeans
 * 
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "random_range.h"

static void find_min_max(struct range_data *d, double *min, double *max)
{
  int i, j;

  /**
   *
   * Seed min and max with the values (along each dimension) of the first
   * datum.
   *
   **/

  for (j = 1; j <= d->features; j++)
  {
    max[j] = d->data[1][j];
    min[j] = d->data[1][j];
  }

  /**
   *
   * Loop through the rest of the data and update min and max accordingly.
   *
   **/

  for (i = 2; i <= d->datapoints; i++)
  {
    for (j = 1; j <= d->features; j++)
    {
      if (d->data[i][j] > max[j])
      {
        max[j] = d->data[i][j];
      }
      else if (d->data[i][j] < min[j])
      {
        min[j] = d->data[i][j];
      }
    }
  }
}

/*****************************************************************************
 *
 * init_means_random_range(struct init_means_data *d)
 *
 * Initializes the means prior to classification by placing them randomly
 * throughout the data.  The variables p->feature_min and p->feature_max
 * must contain the minimum and maximum values of the data along each
 * dimension.  This is important to limit the range of the means.
 *
 * @see find_min_max
 *
 ****************************************************************************/

void random_range(double **data, double **means, int datapoints, 
                  int features, int classes, long seed)
{
  int i, j;
  double *min = NULL, *max = NULL;
  struct range_data d;

  d.data        = data;
  d.class_means = means;
  d.datapoints  = datapoints;
  d.features    = features;
  d.classes     = classes;

  SRAND(seed);

  min = (double *) malloc(sizeof(double) * (features + 1));
  max = (double *) malloc(sizeof(double) * (features + 1));

  find_min_max(&d, min, max);

  /**
   * Compute the range and store it in max[]
   */

  for (i = 1; i <= features; i++)
  {
    max[i] -= min[i];
  }

  for (i = 1; i <= classes; i++)
  {
    for (j = 1; j <= features; j++)
    {
      means[i][j] = min[j] + (DRAND() * max[j]);
    }
  }

  free(max);
  free(min);
}
