/**************************************************************
                 
  Function:  em_frontend.c
  Author:   Lucas Scharenbroich
  Date:     12/11/2001
  Purpose:  A stripped-down version of cross_v.c.  Intended to parse the
            parameters and the pass off to em.  No internal cross
            validation is performed.

  Returns:  None

  Modifications:  

***************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <string.h>

#include "nr_util.h"

#include "em_fe.h"

#include "init_means.h"
#include "random.h"

/****************************************************************************
 *
 * void debuglog(char* message)
 *
 * Echo messages to a debug log
 *
 ***************************************************************************/

void debuglog(char* message)
{
  FILE* stream = stderr;


  if (DEBUG)
  {
    if (DEBUGFILE)
    {
      stream = fopen( "/tmp/EMdebugLog", "a" );
    }

    fprintf( stream, "cross validate debug message: %s\n", message );

    if (DEBUGFILE)
    {
      fclose ( stream );
    }
  }
}

/****************************************************************************
 *
 * void assert(int expr, char* errormessage)
 *
 * Assert than an expression is true, if not, the exit immediately
 *
 ****************************************************************************/

void assert(int expr, char* errormessage)
{
  char message[MESSAGELENGTH];
  
  if(! expr){
    sprintf(message, "assertion FAILURE: %s", errormessage);
    debuglog(message);
    exit(-1);
  }
}

/* allocate a float 3tensor with range t[nrl..nrh][ncl..nch][ndl..ndh] */
double ***NR_d3tensor(long nrl, long nrh, long ncl, long nch, 
                      long ndl, long ndh)
{
  long i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1;
  double ***t;
  
  /* allocate pointers to pointers to rows */
  t=(double ***) malloc((size_t)((nrow+NR_END)*sizeof(float**)));
  if (!t) NR_error("allocation failure 1 in NR_d3tensor()");
  t += NR_END;
  t -= nrl;
  
  /* allocate pointers to rows and set pointers to them */
  t[nrl]=(double **) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double*)));
  if (!t[nrl]) NR_error("allocation failure 2 in NR_d3tensor()");
  t[nrl] += NR_END;
  t[nrl] -= ncl;
  
  /* allocate rows and set pointers to them */
  t[nrl][ncl]=(double *) malloc((size_t)((nrow*ncol*ndep+NR_END)*sizeof(double)));
  if (!t[nrl][ncl]) NR_error("allocation failure 3 in NR_d3tensor()");
  t[nrl][ncl] += NR_END;
  t[nrl][ncl] -= ndl;
  
  for(j=ncl+1;j<=nch;j++) t[nrl][j]=t[nrl][j-1]+ndep;
  for(i=nrl+1;i<=nrh;i++) {
    t[i]=t[i-1]+ncol;
    t[i][ncl]=t[i-1][ncl]+ncol*ndep;
    for(j=ncl+1;j<=nch;j++) t[i][j]=t[i][j-1]+ndep;
  }
  
  /* return pointer to array of pointers to rows */
  return t;
}

void NR_free_d3tensor(double ***t, long nrl, long nrh, long ncl, long nch,
                      long ndl, long ndh)
/* free a float d3tensor allocated by NR_d3tensor() */
{
        free((NR_FREE_ARG) (t[nrl][ncl]+ndl-NR_END));
        free((NR_FREE_ARG) (t[nrl]+ncl-NR_END));
        free((NR_FREE_ARG) (t+nrl-NR_END));
}


/******************************************************************************
 *
 * alloc / dealloc variances
 *
 * creates different sized arrays based on the type of em being run
 *
 *****************************************************************************/

void alloc_inverse_variances(struct em_memory *m, struct em_parameters *p)
{
  switch (p->em_type) {
  case SCALAR:
    m->inv_covariance.scalar = 
      NR_dvector(1, p->num_classes);
    break;
  case DIAG:
    m->inv_covariance.diagonal = 
      NR_dmatrix(1, p->num_classes, 1, p->num_features);
    break;
  case FULL:
    m->inv_covariance.full = 
      NR_d3tensor(1, p->num_classes, 1, p->num_features, 1, p->num_features);
    break;
  }
}

void alloc_class_variances(struct em_memory *m, struct em_parameters *p)
{
  switch (p->em_type) {
  case SCALAR:
    m->class_variances.scalar = 
      NR_dvector(1, p->num_classes);
    break;
  case DIAG:
    m->class_variances.diagonal = 
      NR_dmatrix(1, p->num_classes, 1, p->num_features);
    break;
  case FULL:
    m->class_variances.full = 
      NR_d3tensor(1, p->num_classes, 1, p->num_features, 1, p->num_features);
   break;
  }
}

void dealloc_inverse_variances(struct em_memory* m)
{
  switch(m->params->em_type) {
  case SCALAR:
    NR_free_dvector(m->inv_covariance.scalar, 1, m->params->num_classes);
    break;
  case DIAG:
    NR_free_dmatrix(m->inv_covariance.diagonal,
                    1, m->params->num_classes, 1, m->params->num_features);
    break;
  case FULL:
    NR_free_d3tensor(m->inv_covariance.full,
                     1, m->params->num_classes, 1, m->params->num_features, 
                     1, m->params->num_features);
    break;
  }
}

void dealloc_class_variances(struct em_memory* m)
{
  switch(m->params->em_type) {
  case SCALAR:
    NR_free_dvector(m->class_variances.scalar, 1, m->params->num_classes);
    break;
  case DIAG:
    NR_free_dmatrix(m->class_variances.diagonal,
                    1, m->params->num_classes, 1, m->params->num_features);
    break;
  case FULL:
    NR_free_d3tensor(m->class_variances.full,
                     1, m->params->num_classes, 1, m->params->num_features, 
                     1, m->params->num_features);
    break;
  }
}
/******************************************************************************
 *
 * struct cv_package*  allocate_em_memory(struct em_parameters *emp)
 *
 * Allocates memory for the em parameters, and does a bit of parameter 
 * checking
 *
 *****************************************************************************/

struct em_memory* allocate_em_memory(struct em_parameters *p)
{
  struct em_memory *m;

  /**
   *
   * Be sure to initialize _every_ field explicitly
   *
   **/

  m = (struct em_memory *) malloc(sizeof(struct em_memory));

  m->params            = p;

  m->classifications   = NR_ivector(1, p->num_data);
  m->data              = NR_dmatrix(1, p->num_data, 1, p->num_features);
  m->P_j               = NR_dvector(1, p->num_classes);
  m->class_means       = NR_dmatrix(1, p->num_classes, 1, p->num_features);
  m->P_j_x             = NR_dmatrix(1, p->num_classes, 1, p->num_data);
  m->p_x_j             = NR_dvector(1, p->num_classes);
  alloc_class_variances(m, p);
  alloc_inverse_variances(m, p);

  /* log_det and log_p_j were previously _fast_ options, but because of */
  /* their low risk, the have been merged in.                           */

  m->log_det           = NR_dvector(1, p->num_classes);
  m->log_p_j           = NR_dvector(1, p->num_classes);
  
  if (p->fast == 1) {
    /*
    m->class_count_array = NR_ivector(1, p->num_classes);
    m->P_diff            = NR_dmatrix(1, p->num_classes, 1, p->num_data);
    m->diff_index        = NR_imatrix(1, p->num_classes, 1, p->num_data);
    m->mean_sum          = NR_dmatrix(1, p->num_classes, 1, p->num_features);
    */
  }

  /**
   *
   * Copy data from the parameters
   *
   **/

  m->features        = p->num_features;
  m->datapoints      = p->num_data;
  m->classes         = p->num_classes;
  m->distance_metric = p->distance_metric;
  m->schedule_scalar = p->schedule_scalar;
  m->init_temp       = p->init_temp;
  
  return m;
}

/*****************************************************************************
 *
 * void deallocate_cv_package(struct em_memory* m)
 *
 * Deallocate all memory used
 *
 ****************************************************************************/

void deallocate_em_memory(struct em_memory* m)
{
  /**
   *
   * Check for set parameters
   *
   **/

  if (m->params->means_file != NULL)
    free(m->params->means_file);

  NR_free_dmatrix(m->data, 
                  1, m->params->num_data, 
                  1, m->params->num_features);
  
  NR_free_ivector(m->classifications, 1, m->params->num_data);

  NR_free_dmatrix(m->class_means, 
                  1, m->params->num_classes, 
                  1, m->params->num_features);

  dealloc_class_variances(m);

  NR_free_dvector(m->P_j, 1, m->params->num_classes);
  
  NR_free_dmatrix(m->P_j_x, 
                  1, m->params->num_classes, 
                  1, m->params->num_data);

  NR_free_dvector(m->p_x_j, 1, m->params->num_classes);
  
  NR_free_dvector(m->log_det, 1, m->params->num_classes);
  NR_free_dvector(m->log_p_j, 1, m->params->num_classes);

  dealloc_inverse_variances(m);
  
  if (m->params->fast == 1) {
    /*
    NR_free_ivector(m->class_count_array, 1, m->params->num_classes);
    NR_free_dmatrix(m->P_diff,
                    1, m->params->num_classes, 
                    1, m->params->num_data);
    NR_free_imatrix(m->diff_index,
                    1, m->params->num_classes, 
                    1, m->params->num_data);
    NR_free_dmatrix(m->mean_sum, 
                    1, m->params->num_classes, 
                    1, m->params->num_features);
    */
  }
}

/*****************************************************************************
 *
 * FILE* open_file(char* filename, char* permissions, char* error_message)
 *
 * Open a file in a paranoid fashion
 *
 *****************************************************************************/

FILE* open_file(char* filename, char* permissions, char* error_message)
{
  FILE* f;
  char message[MESSAGELENGTH];
  
  f = fopen(filename, permissions);
  sprintf(message, "%s is not openable, errno is %i\n",
          filename, errno);
  
  assert(f != NULL, message);
  
  return f;
}


/****************************************************************************
 * 
 * int parse_bool(const char *str)
 *
 * return a bool from a string:
 *    "1", "yes", "true", "y" --> true, else false
 *
 ***************************************************************************/

int parse_bool(const char *str) 
{  
  int val = 0;

  if (str != NULL) {
    if (strcmp(str, "1") == 0)    val = 1;
    if (strcmp(str, "yes") == 0)  val = 1;
    if (strcmp(str, "true") == 0) val = 1;
    if (strcmp(str, "y") == 0)    val = 1;
  }
  return val;
}

/****************************************************************************
 *
 * int match(char* str1, char* str2){
 *
 * Return true is two string match, false otherwise.  This is a convenient
 * alternative to testing for 0 for a strcmp()
 *
 ****************************************************************************/

int match(char* str1, char* str2)
{
  return !strcmp((const char*) str1, (const char*) str2);
}

/****************************************************************************
 *
 * 
 * int parse_param_pair(char *key, char *value, struct em_parameters *p)
 *
 * Matches keys to parameters in the em_parameters structure.
 * returns 1 if it sees valid data, else 0
 *
 *****************************************************************************/

int parse_param_pair(char *key, char *value, struct em_parameters *p)
{
  int return_value = 0;

  if (match(key, "rows")) {
    p->num_data = atoi(value);
  }

  else if (match(key, "cols")) {
    p->num_features = atoi(value);
  }

  else if (match(key, "max_em_iterations")) {
    p->max_em_iterations = atoi(value);
  }

  else if (match(key, "num_clusters")) {
    p->num_classes = atoi(value);
  }
  
  else if(match(key, "distance_metric"))
  {
    if (match(value, "correlation"))
    {
      p->distance_metric = CORRELATION;
    } 
    else if(match(value, "euclidean"))
    {
      p->distance_metric = EUCLIDEAN;
    }
    else if (match(value, "correlation_centered"))
    {
      p->distance_metric = CORRELATION;
      p->center_data = 1;
    }
    else
    {
      fprintf(stderr, "Unsupported distance metric: %s\n", value);
      fprintf(stderr, "Defaulting to Euclidean\n");
      p->distance_metric = EUCLIDEAN;
    }
  }
  else if (match(key, "em_type"))
  {
    if (match(value, "scalar"))
    {
      p->em_type = SCALAR;
    }
    else if (match(value, "diagonal"))
    {
      p->em_type = DIAG;
    }
    else if (match(value, "full"))
    {
      p->em_type = FULL;
    }
    else 
    {
      fprintf(stderr, "Unsupported em type: %s", value);
      fprintf(stderr, "Defaulting to Diagonal EM\n");
      p->em_type = DIAG;
    }
  }
  else if (match(key, "algorithm"))
  {
    if (match(value, "em"))
    {
      p->algorithm = EM;
    }
    else if (match(value, "em_mixture_of_t"))
    {
      p->algorithm = EM_MOT;
    }
    else if (match(value, "em_annealing"))
    {
      p->algorithm = EM_DET_ANNEAL;
    }
    else
    {
      fprintf(stderr, "Unsupported algorithm: %s", value);
      fprintf(stderr, "Defaulting to EM\n");
      p->algorithm = EM;
    }
  }
  else if (match(key, "schedule_scalar"))
  {
    p->schedule_scalar = atof(value);
  }
  else if (match(key, "init_temp"))
  {
    p->init_temp = atof(value);
  }
  else if (match(key, "t"))
  {
    p->t = (long double) atof(value);
  }
  else if (match(key, "random_seed"))
  {
    p->random_seed = atoi(value);
  }
  else if ( match(key, "init_method") )
  {
    if ( match(value, "church_means") )
    {
      p->init_method = CHURCH_MEANS;
    }
    else if ( match(value, "random_means") )
    {
      p->init_method = RANDOM_MEANS;
    }
    else if ( match(value, "random_point") ) 
    {
      p->init_method = RANDOM_POINT;
    }
    else if ( match(value, "random_range") ) 
    {
      p->init_method = RANDOM_RANGE;
    }
    else if ( match(value, "random_sample") ) 
    {
      p->init_method = RANDOM_SAMPLE;
    }
    else if ( match(value, "file") ) 
    {
      p->init_method = FROM_FILE;
    }
    else {
      fprintf(stderr, "Unsupported mean init method: %s\n", value);
      fprintf(stderr, "Defaulting to Church Means\n");
      p->distance_metric = CHURCH_MEANS;
    }
  }
  else if (match(key, "means_file"))
  {
    p->means_file = strdup(value);
  }

  else if (match(key, "num_samples"))
  {
    p->samples = atoi(value);
  }
  else if (match(key, "output_model"))
  {
    p->output_model = parse_bool(value);
  }
  else if (match(key, "output_stepsize"))
  {
    p->output_stepsize = atoi(value);
  }
  else if (match(key, "output_internal"))
  {
    p->output_internal = parse_bool(value);
  }
  else if (match(key, "fast"))
  {
    p->fast = 1;
    p->fast_tol = fabs(atof(value));
  }
  else if (match(key, "begin"))
  {
    return_value = 1;
  }
  else
  {
    fprintf(stderr, "Unsupported parameters: %s\n", key);
  }

  return return_value;
}

/*****************************************************************************
 *
 * Reads EM parameters, allocates memory, and does some parameter checking.
 *
 ****************************************************************************/

struct em_memory* read_parameters(char* filename, char* output_filename)
{
  struct em_parameters *em_params;
  struct em_memory *m;

  FILE *params_fin;

  char message[MESSAGELENGTH];
  char *str1, *str2;
  int r_idx, c_idx, close_result;


  em_params  = (struct em_parameters *) malloc(sizeof(struct em_parameters));

  /**
   *
   * Be sure to explicitly initialize _all_ the parameters
   *
   **/

  em_params->num_data          = 0;            /* Number of datapoints */
  em_params->num_features      = 0;            /* Number of dimensions */    
  em_params->random_seed       = 42;           /* random number seed   */
  em_params->max_em_iterations = 0;            /* Iterate up to this many */
  em_params->algorithm         = EM;           /* Algorithm to use     */
  em_params->distance_metric   = EUCLIDEAN;    /* Distance metric to use */
  em_params->center_data       = 0;            /* Don't center the data */
  em_params->num_classes       = 0;            /* Maximum number of classes */
  em_params->schedule_scalar   = 0.9;          /* For annealing */
  em_params->init_temp         = 10.0;         /* For annealing */
  em_params->t                 = 1.0;          /* For mixture-of-t's */
  em_params->samples           = 0;            /* samples for random means */
  em_params->init_method       = CHURCH_MEANS; /* mean initialization */
  em_params->output_model      = 0;            /* By default do not output */
  em_params->output_internal   = 0;            /*  a lot of additional */
  em_params->output_stepsize   = 0;            /*  files               */
  em_params->output_filename   = output_filename;
  em_params->means_file        = NULL;
  em_params->em_type           = DIAG;
  
  str1       = (char *) malloc(MESSAGELENGTH * sizeof(char));
  str2       = (char *) malloc(MESSAGELENGTH * sizeof(char));

  params_fin = open_file(filename, "r", filename);

  /**
   *
   * Parse param pair will return 0 until it reaches the part of the file
   * where the data begins.
   *
   **/

  do {
    fscanf(params_fin, "%s %s", str1, str2);    
  } while(!parse_param_pair(str1, str2, em_params));
     

  /**
   *
   * Allocate the structure and read the data 
   *
   **/

  m = allocate_em_memory(em_params);

  for(r_idx = 1; r_idx <= em_params->num_data; r_idx++) {
    for(c_idx = 1; c_idx <= em_params->num_features; c_idx++) {
      assert(1 == fscanf( params_fin, "%lf", &m->data[r_idx][c_idx] ),
             "Error in parameter file: Could not read data!\n");
    }
  }

  close_result = fclose(params_fin);
  sprintf(message, "Error: Could not close %s, errno is %i\n", filename, errno);
  assert(EOF != close_result, message);

  return m;
}

/*****************************************************************************
 *
 * void run(struct cv_package *p)
 *
 * Runs the algorithm.
 *
 *****************************************************************************/

void run(struct em_memory *m)
{
  /**
   *
   * The NR random number generator requires a negative long for 
   * initialization.
   *
   **/

  if (m->params->random_seed > 0)
    m->params->random_seed *= -1;
  ran1(&(m->params->random_seed));


  /**
   *
   * Sanity check the parameters
   *
   ***/

  if (m->classes > m->datapoints) {
    fprintf(stderr, 
            "WARNING: Number of classes exceeds datapoints. "
            "Adjusting\n");
    m->classes = m->datapoints;
  }

  run_clustering_algorithm(m);  
  find_classifications(m);
  find_P_j_x(m);
}


/*****************************************************************************
 *
 * Runs the clustering algorithm indicated by em_parameters->algorithm
 * using the contents of em_memory.
 *
 * @author Ben Bornstein
 *
 ****************************************************************************/

void run_clustering_algorithm(struct em_memory *m)
{
  switch (m->params->algorithm)
  {
    case EM_MOT:
      debuglog("Running EM (MoT) algorithm...");
      /*mot_em(m, m->params->max_em_iterations, &(m->params->random_seed));*/
      break;
    case EM:
      debuglog("Running EM (MoG) algorithm...");
      em(m);
      break;
    case EM_DET_ANNEAL:
      debuglog("Running DA-EM (MoG) algorithm...");
      em_da(m);
      break;
    default:
      fprintf(stderr, "Unrecognized algorithm.\n");
      break;
  }
}

/*****************************************************************************
 *
 * void create_all_output_files(struct cv_package *p)
 *
 * Dumps a lot of auxillizry information besides just the cluster
 * assignments.
 *
 * Mean:      Mean vectors of the clusters
 * Variances: Diagonal of the covariance matrix 
 * Weights:   Weight of each class
 * Probs:     Probability of each datapoint belonging to a class
 *
 *****************************************************************************/

void create_all_output_files(struct em_memory *m)
{
  int i, c_idx;

  FILE *fp;

  char *output_means     = NULL;
  char *output_variances = NULL;
  char *output_weights   = NULL; 
  char *output_probs     = NULL;
  char *base;

  double **variances;

  /***
   *
   * find the base name of the output file ...
   *
   ***/

  base = m->params->output_filename;
  if (base == NULL) 
    return;

  /***
   *
   * Create the names for the output files
   *
   ***/

  output_means = create_filename(base, ".means");
  output_variances = create_filename(base, ".variances");
  output_weights = create_filename(base, ".weights");
  output_probs = create_filename(base, ".probs");

  /***
   *
   * Write out all the information about the final clustering 
   *
   ***/

  /** Means **/
  fp = fopen(output_means, "w");
  if (fp != NULL) {
    for (c_idx = 1; c_idx <= m->params->num_classes; c_idx++) {
      for (i = 1; i <= m->params->num_features; i++) {
        fprintf(fp, "%f", (float) m->class_means[c_idx][i]);
        if (i != m->params->num_features)
          fprintf(fp, "\t");
      }
      fprintf(fp, "\n");
    }
    fclose(fp);
  }

  /** Variances **/
  variances = NR_dmatrix(1, m->classes, 1, m->features);
  class_variances(m, variances);

  fp = fopen(output_variances, "w");
  if (fp != NULL) {
    for (c_idx = 1; c_idx <= m->params->num_classes; c_idx++) {
      for (i = 1; i <= m->params->num_features; i++) {
        fprintf(fp, "%f", (float) variances[c_idx][i]);
        if (i != m->params->num_features)
          fprintf(fp, "\t");
      }
      fprintf(fp, "\n");
    }
    fclose(fp);
  }
  NR_free_dmatrix(variances, 1, m->classes, 1, m->features);

  /** weights **/
  fp = fopen(output_weights, "w");
  if (fp != NULL) {
    for (c_idx = 1; c_idx <= m->params->num_classes; c_idx++) {
      fprintf(fp, "%f", (float) m->P_j[c_idx]);
      fprintf(fp, "\n");
    }
    fclose(fp);
  }

  /** Probability matrix **/
  fp = fopen(output_probs, "w");
  if (fp != NULL) {
    for (i = 1; i <= m->params->num_data; i++) {
      for (c_idx = 1; c_idx <= m->params->num_classes; c_idx++) {
        fprintf(fp, "%f", (float) m->P_j_x[c_idx][i]);
        if (c_idx != m->params->num_classes)
          fprintf(fp, "\t");
      }
      fprintf(fp, "\n");
    }
    fclose(fp);
  }
  
  free(output_means);
  free(output_variances);
  free(output_weights);
  free(output_probs);
}

/*****************************************************************************
 *
 * void report_results(struct cv_package *p)
 *
 * Dump the final parameters to the output files.  Means, weights, 
 * variances and clusterings are all produced.  The files use the root name
 * of the output file and append appropriate extensions.
 *
 *****************************************************************************/

void report_results(struct em_memory *m)
{
  FILE* fp = NULL;
  int c_idx, num_data = m->params->num_data;

  /***
   *
   * If requested, output all the final cluster information
   *
   ***/

  if (m->params->output_model == 1)
    create_all_output_files(m);

  fp = fopen(m->params->output_filename, "w");

  if (fp == (FILE *) NULL)
  {
    fprintf( stderr, "Error: Could not open results filename " );
    fprintf( stderr, "[%s] for writing.\n", m->params->output_filename );
    fprintf( stderr, "Aborting..." );
    return;
  }

  /**
   *
   * Output Classifications
   *
   **/

  for (c_idx = 1; c_idx <= num_data; c_idx++)
  {
    fprintf( fp, "%i\n", m->classifications[c_idx] );
  }

  fclose(fp);
}

/*****************************************************************************
 *
 * int main(int argc, char *argv[])
 *
 * Point of entry for the Diagonal Clustering code
 *
 *****************************************************************************/

int main (int argc, char* argv[])
{
  struct em_memory *m;

  if (argc < 3) {
    fprintf(stderr, "Usage: %s <input_file> <output_file>\n", argv[0]);
    exit(-1);
  }

  if (DEBUG) 
    debuglog("Reading in parameters.\n");
  
  /**
   *
   * Read in the parameters and data from the input file
   *
   **/

  m = read_parameters(argv[1], argv[2]);

  if (DEBUG) 
    debuglog("Running DiagEM.");
  
  /**
   *
   * Run the EM algorithm
   *
   **/

  run(m);

  if (DEBUG) 
    debuglog("Calling report_results.\n");

  /**
   *
   * Dump the output files
   *
   **/
  
  report_results(m);

  if (DEBUG) 
    debuglog("Freeing memory.\n");
  
  /**
   *
   * Clean up the memory we allocated
   *
   **/

  deallocate_em_memory(m);

  exit(0);
}
