/**************************************************************

  Title:    emol.h
  Author:   Becky Castano 
  Date:     9/29/98
  Function: Performs EM segmentation of an array of feature 
  	    vectors.  The algorithm is from Bishop's "Neural
	    Networks for Pattern Recognition", page 65.  This 
	    particular EM algorithm fits Gaussians to the data.
	    Each element of the feature vector is assumed to
	    be independent (i.e. independent channels). 
	    This version includes outlier rejection.

	    YST is the array of feature vectors
	    P is the array with seed points for each class
	    max_iter maximum number of iterations to perform
	             if convergence not reached
	    nf   number of feature vectors
	    nst  number of elements in each feature vector
	    nc   number of classes
            ol_flag  1 if outliers are to be removed
                     0 if outliers are to be ignored
            aol   the percent of samples that are considered outliers

  Modifications:  
    11/19/98   added return variables pj, means, vars, so that the
               likelihood of other data sets occurring under the 
	       estimated model can be computed

***************************************************************/

/* algorithms */

#ifndef __EM_H
#define __EM_H

#define EM_DET_ANNEAL 0
#define EM            1
#define KMEANS        2
#define EM_MOT        3

#define TRACE         4  /** Trace size for convergence test */

enum {
  SCALAR = 0,
  DIAG,
  FULL
};

struct em_parameters {
  int num_data;
  int num_features;
  long int random_seed;
  int max_em_iterations;
  int algorithm;
  int distance_metric;
  int center_data;       /** should data be mean subtracted? */
  int num_classes;
  double schedule_scalar;
  double init_temp;

  int samples;

  double t;              /** For mixture of t distributions. **/
  int init_method;       
  int em_type;           /** Scalar, Diagonal, or Full covariance */

  int output_model;      /** Output the model files? (yes/no) */
  int output_internal;   /** Output the transformed data? (yes/no) */
  int output_stepsize;   /** detail every nth iteration, 0 means skip */

  char *output_filename;
  char *means_file;      /** Optionally read the means from this file */

  int fast;              /** use the fast EM */
  double fast_tol;       /** tolerence for fast EM ( >= 0.0) */
};


struct em_memory{
  struct em_parameters *params;

  int features;
  int datapoints;
  int classes;
  int distance_metric;       /* distance metric to use          */

  double t; /*for mixture of t distributions*/
  int *classifications;

  double  **data;
  double schedule_scalar;
  double init_temp;
  double inv_temp;           /* == beta == inverse temperature for annealing */
  double *P_j;
  double **class_means;
  union {
    double *scalar;
    double **diagonal;
    double ***full;
  } class_variances;
  
  /* used in mixture of t */
  double **u;               
  double *p_x_j;
  double **P_j_x;

  /* fields used in the fast version */
  
  /*double **P_diff;           /* probability step difference              */
  /*int    **diff_index;       /* data index of differences                */
  /*int    *class_count_array; /* number of changed points per class       */
  /*double **mean_sum;         /* running weighted sum for means           */
  /*union {
    double *scalar;
    double **diagonal;
    double ***full;
  } cov_sum;                 /* running weighted sum for variance        */
  union {
    double *scalar;
    double **diagonal;
    double ***full;
  } inv_covariance;          /* used to cache the inverse matricies      */
  double *log_det;           /* cache of the log of the cov. determinant */
  double *log_p_j;           /* cache of log P_j, used for strength red. */

  /* for conjugate gradient acceleration */

  double *grad_P_j;
  double **grad_class_means;
  double **grad_class_variances;

  /* function pointers to avoid lots of case statements */

  void   (*update_variances)(struct em_memory *m);
  void   (*update_means)(struct em_memory *m);
  double (*update_P_j)(struct em_memory *m);
  void   (*find_P_j_x)(struct em_memory *m);
  double (*log_determinant)(struct em_memory *m, int c);
  double (*mahalanobis_distance)(struct em_memory *m, int x, int c);
  void   (*build_inv_covariance)(struct em_memory *m, int c);
};
	 

/* a NR function for calculating the gamma function*/

int need_norm(struct em_memory *m);
double NR_dgammln(double xx);

double log_determinant_scalar(struct em_memory *m, int c);
double log_determinant_diag(struct em_memory *m, int c);
double log_determinant_full(struct em_memory *m, int c);

double mahalanobis_scalar(struct em_memory *m, int x, int c);
double mahalanobis_diag(struct em_memory *m, int x, int c);
double mahalanobis_full(struct em_memory *m, int x, int c);

void update_variances_scalar(struct em_memory *m);
void update_variances_diag(struct em_memory *m);
void update_variances_full(struct em_memory *m);

void update_means(struct em_memory *m);

void find_P_j_x(struct em_memory *m);

double update_P_j(struct em_memory *m);


void mot_find_P_j_x(struct em_memory *m);
void mot_update_u(struct em_memory *m);
void mot_update_means(struct em_memory *m);
void mot_update_variances(struct em_memory *m);
void mot_em(struct em_memory *m, int iterations, long* NR_seed);
double find_P_x_j(struct em_memory *m,int data_idx,int k_idx);
double find_closest(int i, struct em_memory *m);
void initialize_parameters(struct em_memory *m);
double find_log_likelihood(struct em_memory *m);
void find_classifications(struct em_memory *m);
int reasonable_number(double num);
void print_2d_array(double** array, int r, int c, FILE* fid, char* string);
void print_2d_array_transpose(double** array, int r, int c, FILE* fid, char* string);
void print_2d_float_array(float** array, int r, int c, FILE* fid, char* string);
void print_1d_array(double* array, int c, FILE* fid, char* string);
void em(struct em_memory *m);
void dumpdata(struct em_memory *m);
int em_run(struct em_memory *m, int start_index);
void class_variances(struct em_memory *m, double **v);

void normalize_matrix(double** matrix, int rows,int cols);
void normalize_double_matrix(double** matrix, int rows,int cols);
double update_inv_temp(double old_temp,double scalar);

char *create_filename(char *base, char *ext);
int metric_need_norm(int metric);

#define CONVERGENCE_TEST 0
#define EM_LOOP_DEBUG 0
#define PARAMETER_CONVERGENCE_TEST 0
#define CONVERGENCE_RATIO .9

#define MINIMUM_VARIANCE 0.001
#define EMEPS    1E-6        /* very small number (could be smaller) */
#define LOG_EPSILON 20
#define BIGNUM 1E30
#define SMALLNUM -1E30
#define TIMING 0
#define PRINT 0
#define DEEP_DEBUG 0
#define TEMP_DEBUG 0
#define INIT_DEBUG 0
#define UPDATE_PJX_DEBUG 0
#define UPDATE_U_DEBUG 0
#define UPDATE_MEANS_DEBUG 0
#define UPDATE_VARIANCES_DEBUG 0
#define UPDATE_PJ_DEBUG 0
#define LIKELIHOOD_DEBUG 0
#define EM_SCALE_FACTOR .4

#define DIVIDE_PXJ 1
#define DIVIDE_PMAX 0
#define LOG_PXJ 0
#define LOGFILE "two_class_meth1.m"

#endif
