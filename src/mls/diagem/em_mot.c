/**************************************************************
                 
 This code implements code for mixture of T distributions.

***************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <values.h>
#include "array.h"
#include "em.h"
#include "ord_stats.h"
#include "check_input_params.h"
#include "cross_v.h"
/* #include <ieeefp.h> */

#ifndef MATLAB
#define mxMalloc malloc
#define mxCalloc calloc
#define mxFree free
#endif

extern unsigned long SECS, MSECS, USECS;
extern long u_initialize;
extern long u_compute_pxj;
extern long u_compute_px;
extern long u_compute_pjx;
extern long u_compute_vars;
extern long u_compute_q;

double NR_dgammln(double xx)
{
	double x,y,tmp,ser;
	static double cof[6]={76.18009172947146,-86.50532032941677,
		24.01409824083091,-1.231739572450155,
		0.1208650973866179e-2,-0.5395239384953e-5};
	int j;

	y=x=xx;
	tmp=x+5.5;
	tmp -= (x+0.5)*log(tmp);
	ser=1.000000000190015;
	for (j=0;j<=5;j++) ser += cof[j]/++y;
	return -tmp+log(2.5066282746310005*ser/x);
}


/*modified to be accurate with high dimensional data

 the modification is:

P(j|x)P(x) = P(x|j)P(J)
P(j|x) = P(x|j)P(J)/P(x) (1)

In high dimensions, the determinant gets really big or
really small, since it is effectively the product of d real
numbers, where d is the dimensionality.  So, I multiply the
numerator and the denominator of the right side of (1)
by exp(-s*), where s* is sup(P(x|j)), so that if one term 
dominates, it will force the others to zero, and P(j|x) will go
to 1 for the dominant class*/

void mot_find_P_j_x(struct em_memory *m){
  int data_idx, j_idx, k_idx, feature_idx,maxidx;
  double denom,log_max_pxj,p_x_j,log_p_x_j;
  double t,d;
  d = (double) (m->features);
  t = (double) (m->t);

  for(data_idx = 0; data_idx < m->datapoints; data_idx++){
    for(j_idx = 0; j_idx < m->classes; j_idx++)
      m->P_j_x[j_idx][data_idx] = 0;
    /*first, find the log P_x_j terms*/
    denom = 0;
    for(j_idx = 0; j_idx < m->classes; j_idx++){
      log_p_x_j = NR_dgammln((t+d)/2)-.5*(double)log_determinant(m,j_idx)-
        d/2*log(M_PI*t)-
        NR_dgammln(t/2)*(t+d)*log(1+(double)mahalanobis_distance(m,data_idx,j_idx)/t)/2;
      p_x_j = exp(log_p_x_j);
      m->P_x_j[j_idx] = m->P_j[j_idx]*p_x_j;
      denom += m->P_x_j[j_idx];
    }
    
    for(j_idx = 0; j_idx < m->classes; j_idx++){
      m->P_j_x[j_idx][data_idx] = m->P_x_j[j_idx]/denom;
    }
  }
  if(DEEP_DEBUG || UPDATE_PJX_DEBUG)
    print_2d_array(m->P_j_x, m->classes, m->datapoints, stderr, "P_j_x");
}



void mot_update_u(struct em_memory *m){
  int class_idx, data_idx;
  float d = (float) m->features;
  for(class_idx = 0; class_idx < m->classes; class_idx++){
    for(data_idx = 0; data_idx < m->datapoints; data_idx++){
      m->u[class_idx][data_idx] = (1+d)/
        (1+mahalanobis_distance(m,data_idx,class_idx));
    }
  }
  if(DEEP_DEBUG || UPDATE_U_DEBUG)
    print_2d_array(m->u, m->classes, m->datapoints, stderr, "U");

}

void mot_update_means(struct em_memory *m){
  int class_idx, data_idx, feature_idx;
  long double denom,mdist,factor;
  long double features_d = (long double) m->features;
  long double u_denom;
  for(class_idx = 0; class_idx < m->classes; class_idx++){
    for(feature_idx = 0; feature_idx < m->features; feature_idx++){
      m->class_means[class_idx][feature_idx] = 0;
      u_denom = 0;
      for(data_idx = 0; data_idx < m->datapoints; data_idx++)
        u_denom += m->P_j_x[class_idx][data_idx]*m->u[class_idx][data_idx];
      for(data_idx = 0; data_idx < m->datapoints; data_idx++){
        m->class_means[class_idx][feature_idx] +=
          m->P_j_x[class_idx][data_idx]*
          m->u[class_idx][data_idx]*
          m->data[data_idx][feature_idx];
      }
      m->class_means[class_idx][feature_idx] /= u_denom;
		  
    }
    if(need_norm(m))
      normalize_matrix(&m->class_means[class_idx],1,m->features);
  }
  if(DEEP_DEBUG || UPDATE_MEANS_DEBUG)
    print_2d_array(m->class_means, m->classes, m->features, stderr, "Class Means");
}

void mot_update_variances(struct em_memory *m){
  int class_idx, data_idx, feature_idx;
  long double diff,factor,denom,mdist;
  long double features_d = (long double) m->features;
  for(class_idx = 0; class_idx < m->classes; class_idx++){
    for(feature_idx = 0; feature_idx < m->features; feature_idx++){
      m->class_variances[class_idx][feature_idx] = 0;
      denom = 0;
      for(data_idx = 0; data_idx < m->datapoints; data_idx++)
        denom += m->P_j_x[class_idx][data_idx];
      for(data_idx = 0; data_idx < m->datapoints; data_idx++){
        diff = m->data[data_idx][feature_idx] - m->class_means[class_idx][feature_idx];
        m->class_variances[class_idx][feature_idx] += 
          diff*diff*m->u[class_idx][data_idx]*m->P_j_x[class_idx][data_idx];
      }
      m->class_variances[class_idx][feature_idx] /= denom;
      /*enforce a minimum variance in case a class collapses to a single point*/
      if(m->class_variances[class_idx][feature_idx] < MINIMUM_VARIANCE)
        m->class_variances[class_idx][feature_idx] = MINIMUM_VARIANCE;
    }
  }
  
  if(DEEP_DEBUG || UPDATE_VARIANCES_DEBUG){
    fprintf(stderr,"rows is %i, cols is %i\n",m->classes,m->datapoints);
    print_2d_array(m->class_variances, m->classes, m->features, stderr,
                   "Class Variances");
  }

}

/**/
void mot_em(struct em_memory *m, int iterations, long* NR_seed)
{
  int iteration_idx;

  if ( need_norm(m) )
  {
    normalize_double_matrix(m->data, m->datapoints, m->features);
  }

  initialize_parameters(m);

  /* Do em style updates of parameters. */
  for (iteration_idx = 0; iteration_idx < iterations; iteration_idx++)
  {
    update_P_j(m);
    mot_update_u(m);
    mot_update_means(m);
    mot_update_variances(m);
    mot_find_P_j_x(m);
  }
}
