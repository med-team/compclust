#ifndef EM_FAST_H
#define EM_FAST_H

void find_P_j_x_fast(struct em_memory *m);

void update_means_fast(struct em_memory *m);
void update_variances_scalar_fast(struct em_memory *m);
void update_variances_diag_fast(struct em_memory *m);
void update_variances_fast(struct em_memory *m);

double update_P_j_fast(struct em_memory *m);
int em_run_fast(struct em_memory *m, int start_index);

#endif
