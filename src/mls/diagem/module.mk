CURDIR := $(MLSDIR)/diagem

DIAGEMSRC := $(addprefix $(CURDIR)/, em.c em_da.c em_fe.c)
SRC += $(DIAGEMSRC)
CFLAGS += -I$(CURDIR)

DIAGEMLIBS = $(BASEDIR)/da/libda$(LIBEXT) \
            $(BASEDIR)/nr/libnr$(LIBEXT) \
	    $(BASEDIR)/ut/libut$(LIBEXT) \
	    $(MLSDIR)/common/libcommon$(LIBEXT) \
	    $(MLSDIR)/normalizations/libnorm$(LIBEXT) \
	    $(MLSDIR)/distances/libdist$(LIBEXT) \
	    $(MLSDIR)/initializations/libinit$(LIBEXT)
DIAGEMLIBDIRFLAGS := $(call make_libdirflag,$(DIAGEMLIBS))
DIAGEMLIBLINKFLAGS := $(call make_liblinkflag,$(DIAGEMLIBS)) -lm

DIAGEM := $(CURDIR)/diagem$(BINEXT)
TARGETBINS += $(DIAGEM)

$(DIAGEM): $(DIAGEMSRC:.c=$(OBJEXT)) $(DIAGEMLIBS)
	$(CC) $(DIAGEMLIBDIRFLAGS) -o $@ $(DIAGEMSRC:.c=$(OBJEXT)) $(DIAGEMLIBLINKFLAGS)
