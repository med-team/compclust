function [classification,means,sigmas,test_likelihoods] = em(data, runs, samples, endruns, min_clust, ...
	 max_clust, steps, seed, maxit,test_frac,train_frac,algorithm,distance_metric,t);
  
  % this is a matlab wrapper for a c implementation of em. Algorithms
  % are:
  % data: rows are points
  % runs: cross validaton runs
  % samples: number of times each k is tried
  % endruns: number of runs to try final k
  % min_clust: smallest k for cross validation
  % max_clust: largest k for cross validation
  % stepsize: increment of k
  % seed: random seed for png
  % maxit: iterations for EM algorithm
  % test_denom: proportion of data for likelihood testing [0,1]
  % train_denom: proportion of data for estimating parameters [0,1]
  % algorithm: which variant of em to use. one of:
  %        1: EM, mixture of gaussians
  %        2: KMeans
  %        3: EM, mixture of T
  % distance_metric: how to define distances between points
  %        0: Correlation
  %        1: Euclidean
  % t: value of t for mixture of t (optional); 1 yields mixture of lorentzians

  if(nargin == 13)
    t = 0;
  end
  
