/*****************************************************************************
 
  Function: em_da.c
  Author:   Lucas Scharenbroich
  Date:     1/8/2001
  Purpose:  A Deterministic Annealer wrapper around the EM algorithm.  This
            code is patterned after the paper "Deterministic Annealing for
            Clustering, Compression, Classification, Regression and Related
            Optimization Problems" by Kenneth Rose, IEEE Fellow, 1998

  Returns:  None

  Modifications:  

******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "nr.h"
#include "ut.h"
#include "da.h"

#include "em.h"
#include "em_da.h"

/*****************************************************************************
 *
 * mean_dmat(double **mat, double *mean, int rows, int cols)
 * est_covar_dmat(
 *
 * Some functions not currently in DA.  These should be rolled into the DA
 * library.
 *
 ****************************************************************************/

int DA_mean_dmat(double **mat, double *mean, int rows, int cols)
{
  int   i,j;
  double *mat_row_i;

  /* initialize */
  set_dvec(mean, cols, 0.0);
  
  /* for each attribute, find the sum */
  for (i=1; i<=rows; i++)
  {
    mat_row_i = mat[i];
    for (j=1; j<=cols; j++)
      mean[j] += mat_row_i[j];
  }
    
  /* for each attribute, normalize the sum */
  for (j=1; j<=cols; j++)
    mean[j] = mean[j] / rows;

  return UT_OK;
}

/*****************************************************************************
 *
 * int est_weighted_covar(double **mat, double **cov, double *mean, 
 *                        double *probs, int rows, int cols)
 *
 * Compute a covariance matrix weighted by probabilities.  For our needs,
 * EM should probably compute a full covariance matrix for each class and
 * only use the diagonal.
 *
 ****************************************************************************/

int est_weighted_covar(double **cov, double **mat, double *mean, 
                       double *probs, int rows, int cols)
{
  int row, j1, j2;
  double *data, prob, prob_sum;

  prob_sum = 0.0;
  for (row = 1; row <= rows; row++) {

    data      = mat[row];
    prob      = probs[row];
    prob_sum += prob;

    for (j1 = 1; j1 <= cols; j1++) 
      for (j2 = j1; j2 <= cols; j2++) 
        cov[j1][j2] += prob * (data[j1] - mean[j1]) * (data[j2] - mean[j2]);
  }

  for (j1 = 1; j1 <= cols; j1++) 
    for (j2 = j1; j2 <= cols; j2++) 
      cov[j1][j2] /= (prob_sum - 1.0);

  /* fill in the lower triangle of the matrix based on upper triangle */
  for (j1 = 1; j1 <= cols; j1++)
    for (j2 = 1; j2 < j1; j2++)
      cov[j1][j2] = cov[j2][j1];

  return UT_OK;
}

/*****************************************************************************
 *
 * void compute_pc(int i, int j, struct em_memory *m, double **eigenvector)
 *
 * Computes the principle components of the cluster defined by the *two*
 * clusters i and j.  This is done because in DA they will split along the 
 * first prnciple axis of they cluster they both parented.
 *
 *****************************************************************************/

void compute_pc(int i, int j, struct em_memory *m, double **eigenvectors)
{
  int num_rows, num_dims, nrot, n, max_idx;
  double **cov, *eigenvalues, *mean, *probs, *class1, *class2;
  double max_val, dtmp;
  
  num_rows = m->datapoints;
  num_dims = m->features;

  /* Allocate memory */
  cov          = NR_dmatrix(1, num_rows, 1, num_dims);
  eigenvalues  = NR_dvector(1, num_dims);
  mean         = NR_dvector(1, num_dims);
  probs        = NR_dvector(1, num_rows);

  /* Fill in the sum probabilities of the two classes */
  class1 = m->P_j_x[i];
  class2 = m->P_j_x[j];
  for (n = 1; n < num_rows; n++)
    probs[n] = class1[n] + class2[n];

  /* Compute the mean of the two classes */
  class1 = m->class_means[i];
  class2 = m->class_means[j];
  for (n = 1; n < num_dims; n++)
    mean[n]  = (class1[n] + class2[n]) / 2.0;

  /* Compute the covariance */
  est_weighted_covar(cov, m->data, mean, probs, num_rows, num_dims);

  /* Compute the eigenvectors via Jacobian */
  NR_djacobi(cov, num_dims, eigenvalues, eigenvectors, &nrot);

  /* Now fond the largest eigenvector/value */
  max_val = eigenvalues[1];
  max_idx = 1;

  for (n = 2; n <= num_dims; n++) {
    if (eigenvalues[n] > max_val) {
      max_val = eigenvalues[n];
      max_idx = n;
    }
  }

  /* Move the maximum eigenvector to the first index */
  if (max_idx != 1) {
    for (n = 1; n < num_dims; n++) {
      dtmp = eigenvectors[1][n];
      eigenvectors[1][n] = eigenvectors[max_idx][n];
      eigenvectors[max_idx][n] = dtmp;
    }
  }

  /* Deallocate the memory we used */
  NR_free_dmatrix(cov, 1, num_rows, 1, num_dims);
  NR_free_dvector(eigenvalues, 1, num_dims);
  NR_free_dvector(mean, 1, num_dims);
  NR_free_dvector(probs, 1, num_rows);
}

/******************************************************************************
 *
 * void perturbe(int i, int j, struct em_memory *m)
 *
 * Give the means a little nudge orthogonal to the first principle axis of 
 * their respective paired clusters.  The nudge is scaled by the largest 
 * variance.
 *
 *****************************************************************************/

void perturbe(int i, int j, struct em_memory *m)
{
  int n;
  double delta, sum, maxvar;
  double **eigenvectors, **variances;

  /* get the PCA of the class pair */
  eigenvectors = NR_dmatrix(1, m->features, 1, m->features);
  variances    = NR_dmatrix(1, m->classes, 1, m->features);

  compute_pc(i, j, m, eigenvectors);
  class_variances(m, variances);

  /* normalize the first principal axis */
  sum = 0.0;
  for (n = 1; n <= m->features; n++) 
    sum += eigenvectors[1][n] * eigenvectors[1][n];
  sum = sqrt(sum);

  for (n = 1; n <= m->features; n++) 
    eigenvectors[1][n] /= sum;

  /* find the largest variance */
  maxvar = variances[i][1];
  for (n = 2; n <= m->features; n++) {
    if (maxvar < variances[i][n])
      maxvar = variances[i][n];
  }
  
  /* convert to std */
  maxvar = sqrt(maxvar);

  /* Do a small nudge */
  for (n = 1; n <= m->features; n++) {
    delta = 0.01 * maxvar * eigenvectors[1][n];
    m->class_means[i][n] += delta;
    m->class_means[j][n] -= delta;
  }

  NR_free_dmatrix(variances, 1, m->classes, 1, m->features);
  NR_free_dmatrix(eigenvectors, 1, m->features, 1, m->features);
}

/*****************************************************************************
 *
 * int find_max_variance_cluster(struct em_memory *m)
 *
 * Return the cluster which has the largest variance as computed by the 
 * radius of a n-sphere defined by the diagonal covariance.
 *
 *****************************************************************************/

int find_max_variance_cluster(struct em_memory *m)
{
  int clust_num = 1, i, j;
  double max_trace = 0.0, trace, **variances;
  
  variances = NR_dmatrix(1, m->classes, 1, m->features);
  class_variances(m, variances);

  for (i = 1; i <= m->classes; i++) {
    trace = 1.0;
    for (j = 1; j <= m->features; j++) 
      trace *= variances[i][j];
    if (trace > max_trace) {
      max_trace = trace;
      clust_num  = i;
    }
  }

  NR_free_dmatrix(variances, 1, m->classes, 1, m->features);
  return clust_num;
}

/******************************************************************************
 *
 * int the_same(int i, struct em_memory *m)
 *
 * Returns the class which has the same mean as i.  Returns 0 is no class
 * is the same
 *
 *****************************************************************************/

int the_same(int i, struct em_memory *m)
{
  int j, k, class = 0, same;
  double dist, diff, **variances, *mean1, *mean2, *var1, *var2;

  variances = NR_dmatrix(1, m->classes, 1, m->features);
  class_variances(m, variances);

  mean1 = m->class_means[i];
  var1  = variances[i];

  for (j = 1; j <= m->classes; j++) {
    if (j == i)
      continue;

    mean2 = m->class_means[j];
    var2  = variances[j];

    /* compute the distance squared between class i and j */
    dist = 0.0;
    for (k = 1; k <= m->features; k++) {
      diff = mean1[k] - mean2[k];
      dist += diff*diff;
    }
    
    /* make sure the distance is greater than the smallest variance */
    same = 1;
    for (k = 1; k <= m->features; k++) {
      same &= ((dist < 0.01 * var1[k]) && (dist < 0.01 * var2[k]));
      if (!same)
        break;
    }
 
    if (same) {
      class = j;
      break;
    }
  }
  
  NR_free_dmatrix(variances, 1, m->classes, 1, m->features);
  return class;
}

/******************************************************************************
 *
 * void em_da(struct em_memory *m)
 *
 * Start with k = 2 and perturbe them after each convergence.  If they 
 * diverge from each other, double their datapoints and continue.  Keep
 * this process going until k == K.
 *
 *****************************************************************************/

void em_da(struct em_memory *m)
{
  double T_min, alpha, temperature, **diag_vars, ***full_vars;
  int current_step = 0, change;
  int K_max, K, i, j, class1, class2;

  initialize_parameters(m);

  if (m->params->output_internal == 1)
    dumpdata(m);

  alpha        = m->schedule_scalar;
  temperature  = m->init_temp;
  K_max        = m->classes;
  K            = 1;
  T_min        = 1.0;

  /* Now start the temperature schedule. */
  m->inv_temp = 0.0;
  class1 = 1;
  class2 = 0;

  for(;;) {
    change = 0;
    m->classes = K;

    /* 
       printf("Beta = %f, step = %d, K= %d\n", m->inv_temp, current_step, K); 
    */

    /* Do an EM run */
    current_step = em_run(m, current_step);

    /* Find the classes which are the same */
    for (class1 = 1; class1 <= K; class1++) {
      class2 = the_same(class1, m);
      if (class2 != 0) 
        break;
    }
    
    /*
      If we did a run below T_min, then we're done
    */

    if (class2 == 0 && K == K_max && temperature <= T_min)
      break;

    /* If the clusters diverged otherwise, split up a new cluster */
    if (class2 == 0 && K < K_max) {

      /*
        Find the cluster most likely to split and divide it into two
        classes. 
      */
      
      class1 = find_max_variance_cluster(m);

      K += 1;
      m->classes = K;

      class2 = K;
      change = 1;
      
      /* split the class probabilities */
      m->P_j[class1] *= 0.5;
      m->P_j[K]       = m->P_j[class1];
      
      /* split the posterior probabilities */
      for (j = 1; j <= m->datapoints; j++) {
        m->P_j_x[class1][j] *= 0.5;
        m->P_j_x[K][j]       = m->P_j_x[class1][j];
      }
      
      /* copy the mean */
      for (j = 1; j <= m->features; j++) 
        m->class_means[K][j] = m->class_means[class1][j];

      /* copy the variance */
      switch(m->params->em_type) {
      case SCALAR:
        m->class_variances.scalar[K] = m->class_variances.scalar[class1];
        break;
      case DIAG:
        diag_vars = m->class_variances.diagonal;
        for (j = 1; j <= m->features; j++) 
          diag_vars[K][j] = diag_vars[class1][j];
        break;
      case FULL:
        full_vars = m->class_variances.full;
        for (i = 1; i <= m->features; i++) 
          for (j = 1; j <= m->features; j++)
            full_vars[K][i][j] = full_vars[class1][i][j];
        break;
      }
    }

    /* 
       Perturb the datapoints to get them moving. 
       A little Brownian motion along the first priciple component....
    */

    if (class2 > 0 && K > 0)
      perturbe(class1, class2, m);

    /* Set the next temperature (only if we didn't add a class) */
    if (change == 0) {
      m->inv_temp = 1.0 / temperature;
      temperature *= alpha;
    }
  }
  
  /* reset m->classes */
  m->classes = K_max;
}
