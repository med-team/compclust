#ifndef __FRONTEND_HEADER
#define __FRONTEND_HEADER

#include "em.h"
#include "em_da.h"
#include "dist_metrics.h"

/* this flag controls whether or not to print debug messages */

#define DEBUG 0
#define DEBUGFILE 0 

/* this is the size of the message string */

#define MESSAGELENGTH 512

/* function definitions */

void debuglog(char* message);
void assert(int expr, char* errormessage);
struct em_memory *allocate_em_memory(struct em_parameters *p);
void deallocate_em_memory(struct em_memory* m);
FILE* open_file(char* filename, char* permissions, char* error_message);
int parse_bool(const char *str);
int match(char* str1, char* str2);
int parse_param_pair(char *key, char *value, struct em_parameters *p);
struct em_memory* read_parameters(char* filename, char* output_filename);
void run(struct em_memory *m);
void run_clustering_algorithm(struct em_memory *m);
void create_all_output_files(struct em_memory *m);
void report_results(struct em_memory *m);


#endif
