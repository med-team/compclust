/**************************************************************

   Title:    ord_stats.h
   Author:   Becky Castano
   Date:     October 1, 1998
   Purpose:  routines to compute arbitary order statistics for a vector

***************************************************************/
#if !defined _OSTATS_
#define _OSTATS_

int irandomized_select (int *A, int p, int r, int i);
int irandomized_partition (int *A, int p, int r);
int ipartition (int *A, int p, int r);

float frandomized_select (float *A, int p, int r, int i);
int frandomized_partition (float *A, int p, int r);
int fpartition (float *A, int p, int r);

double drandomized_select (double *A, int p, int r, int i);
int drandomized_partition (double *A, int p, int r);
int dpartition (double *A, int p, int r);

#endif
