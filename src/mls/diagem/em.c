/**************************************************************
                 
  Title:    emol.c
  Author:   Becky Castano - converted from Matlab code written
   	        by Roberto Manduchi - Bug Fixes and enhancements
            by Lucas Scharenbroich.
  Date:     10/1/98
  Updated:  7/22/02

  Function: Performs EM segmentation of an array of feature 
  	    vectors.  The algorithm is from Bishop's "Neural
	    Networks for Pattern Recognition", page 65.  This 
	    particular EM algorithm fits Gaussians to the data.
	    Each element of the feature vector is assumed to
	    be independent (i.e. independent channels). 

	    -> The ol is for outlier rejection.  The outlier rejection
	    procedure was developed by Roberto Manduchi.
	    There is now one more class than the number nc, specified.
	    The additional class is a slack class containing those
	    pixels that do not fit reasonably well into any of the
	    other classes.


	    YST is the array of feature vectors
	    P is the array with seed points for each class
	    max_iter maximum number of iterations to perform
	             if convergence not reached
	    nf   number of feature vectors, i.e. the number of samples 
	    nst     number of elements in each feature vector
	    nc      number of classes
            ol_flag  1 if outliers are to be removed
                     0 if outliers are to be ignored
            aol   the percent of samples that are considered outliers
	    means  [nc,nst]  mean of each class for each channel (element of feature vector)
	    vars  [nc,nst]   variance of each class for each channel
	    pj [nc+1]     probability of each class

  Notes:    An improvement for the future would
	    be to enter initial mean and variance estimation that
	    has been estimated rather than using the seed points
	    for the initial mean of each class and a fixed .1 for
	    the variance of each class.

  Modifications:  
	    11/2/98  Changed computation of pxj to reduce numerical problems.  
      11/9/98  Added calculation of likelihood to return with
	             clustering results.  Changed function to return the likelihood.
      12/11/98 Added checks for NaN after every divide.  If a number is NaN,
	             it is changed to EPS.  This allows the program to continue 
		           running, but is theorectically not sound.  If a NaN occurs, it
		           is not known what the actual value should be.
      11/30/01 Fixed bug in the Expectation formulat in find_P_j_x
      11/31/01 Fixed improper ordering in the main loop, small performance
               enhancements (~5% speed up)
      12/04/01 Added Documentation comments

***************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
//#include <values.h>
#include <limits.h>
#include "nr_util.h"
#include "da_linalg.h"
#include "em.h"
#include "init_means.h"
#include "norm_data.h"
#include "dist_metrics.h"

extern unsigned long SECS, MSECS, USECS;
extern long u_initialize;
extern long u_compute_pxj;
extern long u_compute_px;
extern long u_compute_pjx;
extern long u_compute_vars;
extern long u_compute_q;

/*****************************************************************************
 *
 * int metric_need_norm(int metric)
 *
 * Some metrics need to be normalized after every step.  This function
 * returns true or false depending on the needs of the currently chosen
 * metric 
 *
 ****************************************************************************/

int metric_need_norm(int metric)
{
  return metric == CORRELATION;
}

int need_norm(struct em_memory *m){
  return metric_need_norm(m->distance_metric);
}

void print_2d_array(double** array, int r, int c, FILE* fid, char* string){
  int r_idx, c_idx;

  fprintf(fid, "----START %s\n", string);

  for(r_idx = 1; r_idx <= r; r_idx++){
    for(c_idx = 1; c_idx <= c; c_idx++){
      fprintf(fid, "%.5f ", (float) array[r_idx][c_idx]);
    }
    fprintf(fid, "\n");
  }
  fprintf(fid, "\n----STOP %s\n", string);
}

void print_2d_array_transpose(double** array, int r, int c, FILE* fid, char* string){
  int r_idx, c_idx;

  fprintf(fid, "----START %s\n", string);


  for(c_idx = 1; c_idx <= c; c_idx++){
    for(r_idx = 1; r_idx <= r; r_idx++){
      fprintf(fid, "%.5f ", (float) array[r_idx][c_idx]);
    }
    fprintf(fid, "\n");
  }
  fprintf(fid, "\n----STOP %s\n", string);
}

void print_2d_float_array(float** array, int r, int c, FILE* fid, char* string){
  int r_idx, c_idx;

  fprintf(fid, "----START %s\n", string);

  for(r_idx = 1; r_idx <= r; r_idx++){
    for(c_idx = 1; c_idx <= c; c_idx++){
      fprintf(fid, "%.5f ", (float) array[r_idx][c_idx]);
    }
    fprintf(fid, "\n");
  }
  fprintf(fid, "\n----STOP %s\n", string);
}

void print_1d_array(double* array, int c, FILE* fid, char* string){
  int c_idx;

  fprintf(fid, "----START %s\n", string);
	 
  for(c_idx = 1; c_idx <= c; c_idx++){
    fprintf(fid, "%.5f ", (float) array[c_idx]);
  }
  fprintf(fid, "\n");

  fprintf(fid, "\n----STOP %s\n", string);
}

void print_em_memory(struct em_memory *m)
{
  char *algorithms[] = { "DA-EM (MoG)", "EM (MoG)", "KMeans", "EM (MoT)" };
  char *metrics[]    = { "Correlation", "Euclidean" };
  FILE *stream       = stderr;

  fprintf( stream, "EM Memory Structure:\n" );
  fprintf( stream, "  m->features        = %d\n"   , m->features        );
  fprintf( stream, "  m->datapoints      = %d\n"   , m->datapoints      );
  fprintf( stream, "  m->classes         = %d\n"   , m->classes         );
  fprintf( stream, "  m->t               = %6.2f\n", m->t               );
  fprintf( stream, "  m->schedule_scalar = %6.2f\n", m->schedule_scalar );
  fprintf( stream, "  m->init_temp       = %6.2f\n", m->init_temp       );
  fprintf( stream, "  m->inv_temp        = %6.2f\n", m->inv_temp        );

  fprintf( stream,
           "  m->algorithm       = [%s] (%d)\n",
            algorithms[ m->params->algorithm ],
            m->params->algorithm );

  fprintf( stream,
           "  m->distance_metric = [%s] (%d)\n",
           metrics[ m->distance_metric ],
           m->distance_metric );

  fprintf( stream, "\n" );
}

/*****************************************************************************
 *
 * void class_variances(struct em_memory *m, double **v)
 *
 * Fills in a diagonal variance matrix regardless of scalar, diag, or
 * full EM
 *
 *****************************************************************************/

void class_variances(struct em_memory *m, double **v)
{
  int i, j;

  for (i = 1; i <= m->classes; i++) {
    for (j = 1; j <= m->features; j++) {
      switch(m->params->em_type) {
      case SCALAR:
        v[i][j] = m->class_variances.scalar[i];
        break;
      case DIAG:
        v[i][j] = m->class_variances.diagonal[i][j];
        break;
      case FULL:
        v[i][j] = m->class_variances.full[i][j][j];
        break;
      }
    }
  }
}

/*****************************************************************************
 *
 * char *create_filename(char *base, char *ext)
 *
 * Finds the base name (characters up to the first '.') and then appends
 * the specified extension to the base and returns a pointer to the newly
 * created string.  Returns NULL on error.
 *
 ****************************************************************************/

char *create_filename(char *base, char *ext)
{
  int len, i, last_dot;
  char *fname;

  last_dot = strlen(base);
  for (i = 0; i < strlen(base); ++i)
    if (base[i] == '.')
      last_dot = i;

  len = last_dot + strlen(ext) + 2;
  fname = (char *) malloc(sizeof(char) * len);
  if (fname != NULL) {
    strncpy(fname, base, last_dot);
    fname[last_dot] = '\0';
    strcat(fname, ext); 
  }
  return fname;
}

/**
 * From the numerical recipies in C code, on the
 * JPL machines: /proj/code/c/nr/src/nr_dgammaln.c
 */

int reasonable_number(double num){
#ifdef MAXFLOAT
  return (! isnan(num) && num > -MAXFLOAT && num < MAXFLOAT);
#else 
  return (! isnan(num) );
#endif
}

/******************************************************************************
 *
 * Set of subroutine to compute the mahalanobis distance for the scalar,
 * diagonal, and full covariance cases.
 *
 *****************************************************************************/

double mahalanobis_scalar(struct em_memory *m, int x, int c)
{
  int feature_idx;
  double m_dist = 0.0, dist;
  double *means, inv_variance;
  double *data;

  data  = m->data[x];
  means = m->class_means[c];
  inv_variance = m->inv_covariance.scalar[c];

  for(feature_idx = 1; feature_idx <= m->features; feature_idx++){
    dist = (data[feature_idx] - means[feature_idx]);
    m_dist += (dist*dist);
  }
  m_dist *= inv_variance;

  return m_dist;
}

double mahalanobis_diag(struct em_memory *m, int x, int c)
{
  int feature_idx;
  double m_dist = 0.0, dist;
  double *means, *inv_variance;
  double *data;

  data  = m->data[x];
  means = m->class_means[c];
  inv_variance = m->inv_covariance.diagonal[c]; 
  for(feature_idx = 1; feature_idx <= m->features; feature_idx++){
    dist = (data[feature_idx] - means[feature_idx]);
    m_dist += (dist*dist) * inv_variance[feature_idx];
  }

  return m_dist;
}

double mahalanobis_full(struct em_memory *m, int x, int c)
{
  int n;
  double *result, *diff, *data, dist;
  double *means, **inv_variance;

  n       = m->features;
  result  = NR_dvector(1, n);
  diff    = NR_dvector(1, n);

  data    = m->data[x];
  means   = m->class_means[c];
  inv_variance = m->inv_covariance.full[c];

  subtract_dvec(data, means, diff, m->features);
  left_mult_dmatrix(inv_variance, n, n, diff, result);
  dist = dot_product_dvec(result, diff, n);

  NR_free_dvector(result, 1, n);
  NR_free_dvector(diff, 1, n);

  return dist;
}

/******************************************************************************
 *
 * Set of subroutine to compute the inverse of the covariance matrix for
 * the scalar, diagonal, and full covariance cases.
 *
 *****************************************************************************/

void inv_covariance_scalar(struct em_memory *m, int c)
{
  m->inv_covariance.scalar[c] = 1.0 / m->class_variances.scalar[c];
}

void inv_covariance_diag(struct em_memory *m, int c)
{
  int feature_idx;
  double *inv, *var;

  inv = m->inv_covariance.diagonal[c];
  var = m->class_variances.diagonal[c];

  for (feature_idx = 1; feature_idx <= m->features; feature_idx++)
    inv[feature_idx] = 1.0 / var[feature_idx];
}

void inv_covariance_full(struct em_memory *m, int c)
{
  double **inv, **var;

  inv = m->inv_covariance.full[c];
  var = m->class_variances.full[c];

  invert_copy_alloc_sym_dmat(var, m->features, inv);
}

/******************************************************************************
 *
 * Set of subroutine to compute the log determinant of the covariance
 * matrix for the scalar, diagonal and full covariance cases.
 *
 *****************************************************************************/

double log_determinant_scalar(struct em_memory *m, int c)
{
  return m->features * log(m->class_variances.scalar[c]);
}

double log_determinant_diag(struct em_memory *m, int c)
{
  int feature_idx;
  double log_det = 0.0, *variances;
  
  variances = m->class_variances.diagonal[c];
  for(feature_idx = 1; feature_idx <= m->features; feature_idx++)
    log_det += log(variances[feature_idx]);
	 
  return log_det;
}

double log_determinant_full(struct em_memory *m, int c)
{
  return log(det_copy_alloc_dmat(m->class_variances.full[c], m->features));
}

/******************************************************************************
 *
 * modified to be accurate with high dimensional data
 *
 * The modification is:
 *
 * P(j|x)P(x) = P(x|j)P(J)
 * P(j|x) = P(x|j)P(J)/P(x) (1)
 *
 * In high dimensions, the determinant gets really big or really small,
 * since it is effectively the product of d real numbers, where d is the
 * dimensionality.  So, I multiply the numerator and the denominator of the
 * right side of (1) by exp(-s*), where s* is sup(P(x|j)), so that if one
 * term dominates, it will force the others to zero, and P(j|x) will go to 1
 * for the dominant class
 * 
 ****************************************************************************/

void find_P_j_x(struct em_memory *m)
{
  int data_idx, j_idx;
  double denom, log_max_pxj, d_log_2pi, p_x_j;
  double anneal_exponent;

  /*
   * This is a constant in the inner loop 
   */

  d_log_2pi = m->features * log(2*M_PI);
  
  /*
   * Determine what the annealing exponnent is
   */

  anneal_exponent = (double) 1.0;
  
  if(m->params->algorithm == EM_DET_ANNEAL)
    anneal_exponent = (double) m->inv_temp;

  /* 
   * build a table of log_determinants, get the log of the class
   * probability and cache the inverse covariances.
   */
  
  for(j_idx = 1; j_idx <= m->classes; j_idx++) {
    m->log_det[j_idx] = m->log_determinant(m, j_idx) + d_log_2pi;
    m->log_p_j[j_idx] = log(m->P_j[j_idx]);
    m->build_inv_covariance(m, j_idx);
  }

  for(data_idx = 1; data_idx <= m->datapoints; data_idx++) {
    
    /*  
     * first, find the log p_x_j terms (Gaussian pdf) and the
     * largest p_x_j.
     */
    
    p_x_j = -.5*(m->log_det[1] + m->mahalanobis_distance(m, data_idx, 1));
    m->p_x_j[1] = log_max_pxj = p_x_j;
    
    for(j_idx = 2; j_idx <= m->classes; j_idx++) {
      p_x_j = -.5*(m->log_det[j_idx] + 
                   m->mahalanobis_distance(m, data_idx, j_idx));
      
      m->p_x_j[j_idx] = p_x_j;
      if (p_x_j > log_max_pxj)
        log_max_pxj = p_x_j;
    }
    
    /* find the denominator and cache some (complex) calculation */

    denom = 0.0;
    for(j_idx = 1; j_idx <= m->classes; j_idx++) {
      m->p_x_j[j_idx] = 
        exp(anneal_exponent * 
            (m->p_x_j[j_idx] - log_max_pxj + m->log_p_j[j_idx]));
      
      denom += m->p_x_j[j_idx];
    }

    /* multiplication is faster than division, so invert the denom */
    denom = 1.0 / denom;

    /* now we have the biggest p_x_j */
    for(j_idx = 1; j_idx <= m->classes; j_idx++) {
      m->P_j_x[j_idx][data_idx] = m->p_x_j[j_idx] * denom;
    }
  }
}

/*want max of p(x|j) wich is also max(log(p(x|j))) */

void find_classifications(struct em_memory *m)
{
  int data_idx, k_idx;
  int cur_idx;
  double cur_prob;
		
  for(data_idx = 1; data_idx <= m->datapoints; data_idx++) {
    cur_idx = 1;
    cur_prob = m->P_j_x[1][data_idx];
    for(k_idx = 2; k_idx <= m->classes; k_idx++)
      if(m->P_j_x[k_idx][data_idx] > cur_prob){
        cur_prob = m->P_j_x[k_idx][data_idx];
        cur_idx = k_idx;
      }

    m->classifications[data_idx] = cur_idx;
  }
}


/**
 * Find the vector closest to mean i and return the distance squared
 */

double find_closest(int i, struct em_memory *m)
{
  int j;
  double dist, min_dist;

  if (m->classes == 1)
    min_dist = MINIMUM_VARIANCE;
  else
    min_dist = 1e10;
 
  for (j = 1; j <= m->classes; j++) {
    if (i == j) 
      continue;
    
    dist = distance(m->class_means[i], m->class_means[j], 
                    m->features, EUCLIDEAN_SQUARED);
    
    if (dist < min_dist)
      min_dist = dist;
  }

  return min_dist;
}

/**
 * Initializes parameters by choosing some points and
 * estimating means and standard deviations.
 */

void initialize_parameters(struct em_memory *m)
{
  int class_idx, data_idx, feature_idx, i;
  double *diag_vars, **full_vars, dist;

  /**
   *
   * First step to normalization:  Translate the entire dataset by it's
   * mean (if requested).  This places it about the origin and, when 
   * projected onto a hypersphere, makes more intuitive sense.  The center
   * of the hypersphere is the same as the mean of the dataset.  This also 
   * helps to avoid the issue of one part of the hypersphere surface being 
   * overpopulated.
   * 
   * We defer the normalization to the surface of the hypersphere until
   * later to avoid the initial means (weighted average of datapoints)
   * being chosen in the empty center of the hypersphere.
   *
   * This normalization strategy is only for then a Correlation metric is
   * requested as this provides an approximation to correlation using 
   * Euclidean distances.  If other normalization are introduces later, they
   * should override this normalization code.
   *
   **/

  if (m->params->center_data)
    norm_data(m->data, m->datapoints, m->features, MEAN_NORM, 0);

  init_means(m->data, m->class_means, m->datapoints, m->features, m->classes,
             m->params->init_method, m->params->random_seed, 
             m->params->samples, m->params->means_file);

  if (need_norm(m)) {
    norm_data(m->data, m->datapoints, m->features, EUCLIDEAN_NORM, 0);
    norm_data(m->class_means, m->classes, m->features, EUCLIDEAN_NORM, 0);
  }

  /* Set the probabilities and variances as described in Bishop p.69 */

  for (class_idx = 1; class_idx <= m->classes; class_idx++) {

    /* All classes have equal probability */
    m->P_j[class_idx] = 1.0 / (double) m->classes;

    /* As priors have equal probability */
    for (data_idx = 1; data_idx <= m->datapoints; data_idx++) {
      m->P_j_x[class_idx][data_idx] = 1.0 / (double) (m->classes);
    }

    /* Set the variance equal to the distance to the nearest other cluster */
    dist = find_closest(class_idx, m);
    switch(m->params->em_type) {
    case SCALAR:
      m->class_variances.scalar[class_idx] = dist;
      break;
    case DIAG:
      diag_vars = m->class_variances.diagonal[class_idx];
      for (feature_idx = 1; feature_idx <= m->features; feature_idx++) {
        diag_vars[feature_idx] = dist;
      }
      break;
    case FULL:
      full_vars = m->class_variances.full[class_idx];
      for (i = 1; i <= m->features; i++) {
        for (feature_idx = 1; feature_idx <= m->features; feature_idx++) {
          if (i == feature_idx)
            full_vars[i][feature_idx] = dist;
          else
            full_vars[i][feature_idx] = 0.0;
        }
      }
    }
  }

  /* update_variances(m); */


  if (DEEP_DEBUG || INIT_DEBUG)
  {
    print_2d_array(m->class_means,
                   m->classes,
                   m->features,
                   stderr,
                   "Class Means Init");

    /*
      print_2d_array(m->class_variances,
      m->classes,
      m->features,
      stderr,
      "Class Variances Init");
    */

    print_1d_array(m->P_j,
                   m->classes,
                   stderr,
                   "P(J) Init");
  }
}


/***
 *
 * Note on the "if P_j == 0.0 continue" fix in update_means() and
 * update_variances().
 *
 * The only time this will be triggered is if on the previous step the 
 * probability densities were _all_ zero for a particular class.  If this
 * is so, the that class has a zero probability of existing and should be
 * ignored.
 *
 * Note also that none of the data structures are altered, so if the zero
 * probability comes from round off errors that are corrected later, then
 * the class should 'magically' reappear in the calculation as well.
 *
 * In short, this seems to be a fix which is, at least, intuitively correct
 * and also the least intrusive.
 *
 * That said, if issues crop up in the future with regard to collapsed
 * clusters, this fix-up code might be a good first place to look for a
 * problem.
 *
 **/

void no_op(struct em_memory *m)
{
}

/******************************************************************************
 *
 * update_means_and_variances(struct em_memory *m)
 *
 * An optimized version of the update equations which computes the mean
 * and variances of a class in a single pass, rather than computing the
 * mean and then the variance.  The number of useful calculations is
 * not reduced, but the loop overhead is combined.
 *
 * This optimization sped up the diagonal case by 30 percent in benchmarks.
 * A possible issue is that the single-pass version of variance calculation
 * is not as numerically stable as the traditional way.  There are a few
 * options for computing sample varianve (in order of numerical robustness)
 *
 * 1) variance = E[X^2] - E[X]
 * 2) same as (1), but subtract x[0] for all x.  i.e. use x[0] as an 
 *    estimate of the mean.
 * 3) variance = (1/N) * SUM (x[i] - mean)^2
 * 4) variance = (3) + corrective term (see Numerical Recipes)
 *
 * If numerical issues arise, I recommend using option (2) as a first
 * attempt to fix.  Since we have the mean of the class from the previous
 * time step, and the mean will not change dramatically, we have a very
 * good estimate on the actual mean of the class.
 *
 ****************************************************************************/ 

void update_means_and_variances_scalar(struct em_memory *m)
{
  /* UNTESTED */
  int class_idx, data_idx, feature_idx;
  double tmp, datum, datum_prob, denom, variance, *P_j_x, *mean, *data;

  for (class_idx = 1; class_idx <= m->classes; class_idx++) {
    
    denom = m->datapoints * m->P_j[class_idx];
    if (denom == 0.0)
      continue;

    denom = 1.0 / denom;

    P_j_x = m->P_j_x[class_idx];
    mean  = m->class_means[class_idx];

    variance = 0.0;
    for(feature_idx = 1; feature_idx <= m->features; feature_idx++) 
      mean[feature_idx] = 0.0;

    for(data_idx = 1; data_idx <= m->datapoints; data_idx++) {
      if (P_j_x[data_idx] == 0.0) 
        continue;
      
      data = m->data[data_idx];
      datum_prob = P_j_x[data_idx];

      for(feature_idx = 1; feature_idx <= m->features; feature_idx++) {
        datum = data[feature_idx];
        tmp = datum * datum_prob;
        mean[feature_idx] += tmp;
        variance += tmp * datum;
      }
    }
    
    /* Finish computing the mean and variance */
    tmp = 0.0;
    for(feature_idx = 1; feature_idx <= m->features; feature_idx++) {
      tmp += mean[feature_idx];
      mean[feature_idx] *= denom;
    }

    /* The variance is scalar, so we need to add num_features in.        */
    denom /= (double) m->features;
    
    /* E[X]   = tmp * denom                                              */
    /* E[X^2] = variance * denom                                         */
    /*                                                                   */
    /* This is correct because the least-squares value for a 1D problem  */
    /* is just the average of the values.                                */

    variance *= denom;
    tmp      *= denom;
    variance -= (tmp * tmp);
    if (variance < MINIMUM_VARIANCE)
      variance = MINIMUM_VARIANCE;

    m->class_variances.scalar[class_idx] = variance;
  }

  if(need_norm(m))
    norm_data(m->class_means, m->classes, m->features, EUCLIDEAN_NORM, 0);
}

void update_means_and_variances_diag(struct em_memory *m)
{
  int class_idx, data_idx, feature_idx;
  double tmp, datum, datum_prob, denom, *P_j_x, *variance, *mean, *data;

  for (class_idx = 1; class_idx <= m->classes; class_idx++) {
    
    denom = m->datapoints * m->P_j[class_idx];
    if (denom == 0.0)
      continue;

    denom = 1.0 / denom;

    P_j_x    = m->P_j_x[class_idx];
    variance = m->class_variances.diagonal[class_idx];
    mean     = m->class_means[class_idx];

    /***
     *
     * Use the formula Var(X) = E[(X - mu)^2] = E[X^2] - E[X]^2
     * 
     ***/

    for(feature_idx = 1; feature_idx <= m->features; feature_idx++) {
      variance[feature_idx] = 0.0;
      mean[feature_idx] = 0.0;
    }

    for(data_idx = 1; data_idx <= m->datapoints; data_idx++) {
      if (P_j_x[data_idx] == 0.0) 
        continue;
      
      data = m->data[data_idx];
      datum_prob = P_j_x[data_idx];

      for(feature_idx = 1; feature_idx <= m->features; feature_idx++) {
        datum = data[feature_idx];
        tmp = datum * datum_prob;
        mean[feature_idx] += tmp;
        variance[feature_idx] += tmp * datum;
      }
    }
    
    /* Finish computing the mean and variance */
    for(feature_idx = 1; feature_idx <= m->features; feature_idx++) {
      mean[feature_idx] *= denom;
      variance[feature_idx] *= denom;
      variance[feature_idx] -= (mean[feature_idx] * mean[feature_idx]);
      if (variance[feature_idx] < MINIMUM_VARIANCE)
        variance[feature_idx] = MINIMUM_VARIANCE;
    }
  }

  if(need_norm(m))
    norm_data(m->class_means, m->classes, m->features, EUCLIDEAN_NORM, 0);
}

/******************************************************************************
 *
 * void update_means(struct em_memory *m)
 *
 * Means are updated using the update equation from Bishop p.67
 *
 *
 *                N   old
 *               SUM P   (j | x[n]) * x[n]
 *        new    n=1
 * mean(j)    =  -------------------------
 *                    N   old
 *                   SUM P   (j | x[n])
 *                   n=1
 *
 * In the implementation, P_j is precomputed and used in the denominator
 *
 *****************************************************************************/

void update_means(struct em_memory *m)
{
  int class_idx, data_idx, feature_idx;
  double denom, *P_j_x, *mean, *datum, datum_prob;

  for(class_idx = 1; class_idx <= m->classes; class_idx++) {    
    
    /**
     *
     * If the class probability is zero, then skip this mean.  This
     * assumes that the mean from the previous step was a good mean, so it
     * should just be left alone (orphaned) for this step
     *
     **/

    denom = ((double) m->datapoints) * m->P_j[class_idx];
    if (denom == 0.0) 
      continue;

    denom = 1.0 / denom;

    /**
     *
     * Cache some pointers
     *
     **/

    P_j_x = m->P_j_x[class_idx];
    mean  = m->class_means[class_idx];

    for(feature_idx = 1; feature_idx <= m->features; feature_idx++) 
      mean[feature_idx] = 0.0;

    for(data_idx = 1; data_idx <= m->datapoints; data_idx++) {
      if (P_j_x[data_idx] == 0.0) continue;

      datum = m->data[data_idx];
      datum_prob = P_j_x[data_idx];
      for(feature_idx = 1; feature_idx <= m->features; feature_idx++) {
        mean[feature_idx] += datum_prob * datum[feature_idx];
      }
    }

    for(feature_idx = 1; feature_idx <= m->features; feature_idx++)
      mean[feature_idx] *= denom;
  }

  if(need_norm(m))
    norm_data(m->class_means, m->classes, m->features, EUCLIDEAN_NORM, 0);

  if(DEEP_DEBUG || UPDATE_MEANS_DEBUG)
    print_2d_array(m->class_means, m->classes, m->features, stderr, "Class Means Out");
}

/*****************************************************************************
 *
 * void update_variances(struct em_memory *m)
 *
 * Recompute the variances using the update equation on p. 67 of Bishop
 *
 *
 *            N   old                                 2
 *           SUM P   (j | x[n]) * || x[n] - mean[n] ||
 *    new    n=1
 * var    =  -------------------------------------------
 *                         N   old
 *                        SUM P   (j | x[n])
 *                        n=1
 *
 * In the implementation, P_j is precomputed and used in the denominator
 *
 *****************************************************************************/

void update_variances_scalar(struct em_memory *m)
{
  int class_idx, data_idx, feature_idx;
  double diff, dist, denom, *P_j_x, *mean, *data, variance;
  
  /**
   *
   * The scalar variance is computed as the value which minimizes the
   * least-squares difference with the diagonal variances.  Since this
   * is a 1D problem, the solution is just the average of the diagonal
   * variances.
   *
   **/

  for(class_idx = 1; class_idx <= m->classes; class_idx++) {
    
    /**
     *
     * If the class probability is 0.0, then skip the class.  This relies
     * on the assumption that the previous step was valid
     *
     **/

    denom = m->datapoints * m->features * m->P_j[class_idx];
    if (denom == 0.0)
      continue;

    /**
     *
     * Cache some pointers
     *
     **/
    
    P_j_x = m->P_j_x[class_idx];
    mean  = m->class_means[class_idx]; 
    
    variance = 0.0; 

    for(data_idx = 1; data_idx <= m->datapoints; data_idx++) {
      if (P_j_x[data_idx] == 0.0) continue;
      
      data = m->data[data_idx];
      dist = 0.0;
      for(feature_idx = 1; feature_idx <= m->features; feature_idx++) {
        diff  = (mean[feature_idx] - data[feature_idx]);
        dist += (diff * diff);
      }
      variance += (dist * P_j_x[data_idx]);
    }

    variance /= denom;
    if (variance < MINIMUM_VARIANCE)
      variance = MINIMUM_VARIANCE;
  
    m->class_variances.scalar[class_idx] = variance;
  }
}

void update_variances_diag(struct em_memory *m)
{
  int class_idx, data_idx, feature_idx;
  double diff, denom, *P_j_x, *variance, *mean, *data;
  
  for(class_idx = 1; class_idx <= m->classes; class_idx++) {
    
    /**
     *
     * If the class probability is 0.0, then skip the class.  This relies
     * on the assumption that the previous step was valid
     *
     **/

    denom = m->datapoints * m->P_j[class_idx];
    if (denom == 0.0)
      continue;

    denom = 1.0 / denom;

    /**
     *
     * Cache some pointers
     *
     **/
    
    P_j_x    = m->P_j_x[class_idx];
    variance = m->class_variances.diagonal[class_idx];
    mean     = m->class_means[class_idx];
    
    for(feature_idx = 1; feature_idx <= m->features; feature_idx++)
      variance[feature_idx] = 0.0;
    
    for(data_idx = 1; data_idx <= m->datapoints; data_idx++) {
      if (P_j_x[data_idx] == 0.0) 
        continue;
      
      data = m->data[data_idx]; 
      for(feature_idx = 1; feature_idx <= m->features; feature_idx++) {      
        diff = (mean[feature_idx] - data[feature_idx]);
        variance[feature_idx] += (diff * diff * P_j_x[data_idx]);
      }
    }
    
    for(feature_idx = 1; feature_idx <= m->features; feature_idx++) {
      variance[feature_idx] *= denom;
      if (variance[feature_idx] < MINIMUM_VARIANCE)
        variance[feature_idx] = MINIMUM_VARIANCE;
    }
  }
}

void update_variances_full(struct em_memory *m)
{
  int class_idx, data_idx, feature_idx, row;
  double diff, denom, *P_j_x, **variance, *mean, *data;
  double *variance_row;

  for(class_idx = 1; class_idx <= m->classes; class_idx++) {
    
    /**
     *
     * If the class probability is 0.0, then skip the class.  This relies
     * on the assumption that the previous step was valid
     *
     **/

    denom = m->datapoints * m->P_j[class_idx];
    if (denom == 0.0)
      continue;

    denom = 1.0 / denom;

    /**
     *
     * Cache some pointers
     *
     **/
    
    P_j_x    = m->P_j_x[class_idx];
    variance = m->class_variances.full[class_idx];
    mean     = m->class_means[class_idx];
    
    /* zero the upper half */
    for (row = 1; row <= m->features; row++) { 
      variance_row = variance[row];
      for(feature_idx = row; feature_idx <= m->features; feature_idx++)
        variance_row[feature_idx] = 0.0;
    }

    for(data_idx = 1; data_idx <= m->datapoints; data_idx++) {
      if (P_j_x[data_idx] == 0.0) 
        continue;
      
      /* compute the upper half of the matrix */
      data = m->data[data_idx];
      for (row = 1; row <= m->features; row++) {
        variance_row = variance[row];
        diff = (mean[row] - data[row]);
        for (feature_idx = row; feature_idx <= m->features; feature_idx++) {
          variance_row[feature_idx] += P_j_x[data_idx] * diff * 
            (mean[feature_idx] - data[feature_idx]);
        }
      }
    }
    
    /* divide the variances */
    for (row = 1; row <= m->features; ++row) {
      variance_row = variance[row];
      for(feature_idx = row; feature_idx <= m->features; feature_idx++) {
        variance_row[feature_idx] *= denom;
      }
    }

    /* check along the diagonal for a minimum variance */
    for (row = 1; row <= m->features; ++row) {
      if (variance[row][row] < MINIMUM_VARIANCE) {
        variance[row][row] = MINIMUM_VARIANCE;
      }
    }

    /* copy the upper half into the lower half and don't copy the diagonal */
    for (row = 1; row <= m->features; ++row) {
      for (feature_idx = row + 1; feature_idx <= m->features; feature_idx++) {
        variance[feature_idx][row] = variance[row][feature_idx];
      }
    }
  }
}

/******************************************************************************
 *
 * int update_P_j(struct em_memory *m)
 *
 * update the class probabilities (class weights).  Each P_j is equal to
 * the fraction of datapoints 'owned' but this class.
 *
 * The values in P_j can be used in the means and variances update equation
 * if they are computed beforehand.  This differs from the order that
 * appears in Bishop p. 67
 *
 *     new    1  N   old 
 * P(j)    =  - SUM P(j | x[n])
 *            N n=1
 *
 ****************************************************************************/

double update_P_j(struct em_memory *m)
{
  int data_idx, class_idx;
  double *P_j_x, *P_j, sum, points, delta = 0.0;

  P_j    = m->P_j;
  points = (double) m->datapoints;

  for (class_idx = 1; class_idx <= m->classes; class_idx++) {

    sum = 0.0;
    P_j_x = m->P_j_x[class_idx];

    for (data_idx = 1; data_idx <= m->datapoints; data_idx++)
      sum += P_j_x[data_idx];

    sum /= points;
    
    delta += fabs(sum - P_j[class_idx]);
    P_j[class_idx] = sum;
  }

  if(DEEP_DEBUG || UPDATE_PJ_DEBUG)
    print_1d_array(m->P_j, m->classes, stderr, "P(J)");

  return delta;
}

/*algebraic trickery is used here to allow calculation of likelihood
  without getting nans and infs*/

double find_log_likelihood(struct em_memory *m)
{
  double likelihood, point_likelihood, log_two_pi_d;
  int data_idx, class_idx;
  double *log_determinants;

  likelihood = 0;
  log_two_pi_d = m->features * log(2*M_PI);

  log_determinants = NR_dvector(1, m->classes);
  
  for(class_idx = 1; class_idx <= m->classes; class_idx++)
    log_determinants[class_idx] = m->log_determinant(m, class_idx);
  

  for(data_idx = 1; data_idx <= m->datapoints; data_idx++){

    point_likelihood = 0;
    for(class_idx = 1; class_idx <= m->classes; class_idx++){

      /*what is going on here is:
        
      -.5*(mahalanobis_distance(m, data_idx, class_idx) +
      log_determinants[class_idx] + log_two_pi_d)
      
      is equal to:
      
      -.5*mahalanobis_distance(m, data_idx, class_idx) -
      -.5*log_determinants[class_idx] - .5*log_two_pi_d
      
      is equal to:
      
      -.5*mahalanobis_distance(m, data_idx, class_idx) -
      log(sqrt(determinant * two_pi^d)) 
      
      exponentiate this, and you end up with:
      
      1/(sqrt((two_pi^d)*det)) * exp(-.5 * M(x,c))
      
      Contact Tobias Mann at mann@aig.jpl.nasa.gov
      if this is unclear.

      Update: 12/3/01
      
      The log_two_pi_d term may, and possibly should, be dropped from the
      calculation.  In very high dimensions with many datapoints, it is a 
      very large number that will dominate the term.  When it is
      exponentiated, this could very likely force the likelihood to 0.
      */
      
      point_likelihood += m->P_j[class_idx] *
        exp(-.5*(log_determinants[class_idx] + 
                 m->mahalanobis_distance(m, data_idx, class_idx)));
      
    }
    if(LIKELIHOOD_DEBUG){
      fprintf(stderr, "likelihood for point %i is %f\n", 
              data_idx, (float) point_likelihood);
			
    } 

    /***
     *
     * Reintroduce the log_two_pi_d term.  Since it is a constance it has
     * been factored out of the inner loop
     *
     ***/

    likelihood += (log(point_likelihood) - 0.5*log_two_pi_d);
  }
  
  NR_free_dvector(log_determinants, 1, m->classes);

  return likelihood;
}

double update_inv_temp(double old_temp,double scalar)
{
  if(DEEP_DEBUG || TEMP_DEBUG)
    fprintf(stderr,"Temp is %g, inv temp is %g\n", 1.0/old_temp,old_temp);
  return old_temp/scalar; /* = T*scalar */
}

void dumpvars(struct em_memory *m,  int step)
{
  char *meanfile,   meanext[]   = "00000.means";
  char *varfile,    varext[]    = "00000.variances";
  char *labelsfile, labelsext[] = "00000.labels";
  struct em_parameters *p = NULL;
  FILE *fp = NULL;
  
  int class_idx, feature_idx, data_idx;
  double **variances;

  if (m != NULL)
    p = m->params;

  if (m == NULL || p == NULL || p->output_filename == NULL)
    return;
  
  snprintf(meanext, strlen(meanext), "%05d.means", step);
  meanfile   = create_filename(p->output_filename, meanext);

  snprintf(varext, strlen(varext), "%05d.variances", step);
  varfile    = create_filename(p->output_filename, varext);

  snprintf(labelsext, strlen(labelsext), "%05d.labels", step);
  labelsfile = create_filename(p->output_filename, labelsext);


  if (meanfile != NULL) {
    if ((fp = fopen(meanfile, "w")) != NULL) {
      for(class_idx = 1; class_idx <= m->classes; class_idx++) {
        for(feature_idx = 1; feature_idx <= m->features; feature_idx++) {
          fprintf(fp,"%4.4f",(float) m->class_means[class_idx][feature_idx]);
          if (feature_idx != m->features)
            fprintf(fp,"\t");
        }
        fprintf(fp,"\n");
      }
      fclose(fp);
    }
    free(meanfile);
  }
  
  if (varfile != NULL) {
    if ((fp = fopen(varfile, "w")) != NULL) {

      variances = NR_dmatrix(1, m->classes, 1, m->features);
      class_variances(m, variances);

      for(class_idx = 1; class_idx <= m->classes; class_idx++) {
        for(feature_idx = 1; feature_idx <= m->features; feature_idx++){
          fprintf(fp,"%4.4f", (float) variances[class_idx][feature_idx]);
          if (feature_idx != m->features)
            fprintf(fp,"\t");
        }
        fprintf(fp,"\n");
      }

      NR_free_dmatrix(variances, 1, m->classes, 1, m->features);
      fclose(fp);
    }
    free(varfile);
  }
  
  if (labelsfile != NULL) {
    if ((fp = fopen(labelsfile, "w")) != NULL) {
      find_classifications(m);
      for (data_idx = 1; data_idx <= m->datapoints; data_idx++) {
        fprintf(fp, "%d", m->classifications[data_idx]);
        fprintf(fp,"\n");
      }
      fclose(fp);
    }
    free(labelsfile);
  }  
}

/*****************************************************************************
 *
 * void dumpdata(struct em_memory *m)
 *
 * Dump the internal state of the clustering to a file
 *
 *****************************************************************************/

void dumpdata(struct em_memory *m)
{
  char *datafile = NULL;
  int data_idx, feature_idx;
  struct em_parameters *p = NULL;
  FILE *fp = NULL;

  /***
   *
   * Find the base name and construct an appropriate filename
   *
   **/

  if (m != NULL)
    p = m->params;

  if (m == NULL || p == NULL || p->output_filename == NULL)
    return;

  datafile = create_filename(p->output_filename, ".internal");

  if (datafile != NULL) {
    if ((fp = fopen(datafile,"w")) != NULL) {

      for (data_idx = 1; data_idx <= m->datapoints; data_idx++){
        for(feature_idx = 1; feature_idx <= m->features; feature_idx++){
          fprintf(fp,"%f", m->data[data_idx][feature_idx]);
          if (feature_idx != m->features)
            fprintf(fp,"\t");
        }
        if (data_idx != m->datapoints)
          fprintf(fp,"\n");
      }

      fclose(fp);
    }
    free(datafile);
  }
}

/*****************************************************************************
 *
 * void em_run(struct em_memory *m)
 *
 * Do the inner EM loop, executing the update equations.  Early-out testing
 * could be added in here 
 *
 ****************************************************************************/

int em_run(struct em_memory *m, int start_index)
{
  int i, iteration_idx, iterations, step;
  double delta, sum, trace[TRACE];

  for (i = 0; i < TRACE; i++)
    trace[i] = i*10;

  iterations = m->params->max_em_iterations;
  step       = m->params->output_stepsize;

  /**
   *
   * Set up pointers to functions
   *
   **/

  m->update_means = update_means;
  m->update_P_j   = update_P_j;
  m->find_P_j_x   = find_P_j_x;

  switch (m->params->em_type) {
  case SCALAR:
    m->log_determinant      = log_determinant_scalar;
    m->mahalanobis_distance = mahalanobis_scalar;
    m->update_variances     = update_variances_scalar;
    m->build_inv_covariance = inv_covariance_scalar;
    break;
  case DIAG:
    m->log_determinant      = log_determinant_diag;
    m->mahalanobis_distance = mahalanobis_diag;
    m->update_means         = no_op;
    m->update_variances     = update_means_and_variances_diag;
    m->build_inv_covariance = inv_covariance_diag;
    break;
  case FULL:
    m->log_determinant      = log_determinant_full;
    m->mahalanobis_distance = mahalanobis_full;
    m->update_variances     = update_variances_full;
    m->build_inv_covariance = inv_covariance_full;
    break;
  }

  /** 
   *
   * Do em style updates of parameters. 
   *
   **/

  for (iteration_idx = start_index; 
       iteration_idx < start_index + iterations; 
       iteration_idx++)
  {
    if (step > 0 && (iteration_idx % step) == 0)
      dumpvars(m, iteration_idx);

    m->find_P_j_x(m);           /* Expectation */

    /**
     *
     * It is OK to compute P_j_new here because P_j is not actually used
     * in any of the update equations.  The P_j seen in update_means and
     * update_variances is actually used as a cache of SUM P_old(j | x)
     *
     **/

    delta = m->update_P_j(m);           

    /**
     *
     * Update the mean and variances using the cached SUM in P_j
     *
     **/
    
    m->update_means(m);         /* Maximization */
    m->update_variances(m);

    /**
     *
     * Convergence test:
     *    Sum the class probability deltas over the last n steps.  If the
     * total change is less than epsilon, break out.
     * 
     **/

    trace[iteration_idx % TRACE] = delta;
    sum = 0.0;
    for (i = 0; i < TRACE; i++)
      sum += trace[i];

    if (sum < EMEPS) {
      break;
    }
  }

  if (step > 0 && (iteration_idx % step) == 0)
    dumpvars(m, iteration_idx);

  return iteration_idx;
}

/*****************************************************************************
 *
 * void em(struct em_memory *m)
 *
 * Initialize and run the EM algorithm 
 *
 *****************************************************************************/

void em(struct em_memory *m)
{
  if (DEEP_DEBUG) {
    fprintf(stderr, "Entering em().\n");
    print_em_memory(m);
  }

  /**
   *
   * initialize the means, variances and P(j) 
   *
   **/

  initialize_parameters(m);

  if (m->params->output_internal == 1)
    dumpdata(m);

  /**
   *
   * Run EM
   *
   **/
 
  em_run(m, 0);
  
  /**
   *
   * Dump the parameters for the final iteration
   *
   **/

  if (DEEP_DEBUG)
    fprintf(stderr, "Exiting em().\n");
}

