/**************************************************************
                 
  Function:   check_input_params
  Author:     Becky Castano 
  Date:       10/2/98
  Purpose:    to check that several of the parameters input to 
              emol are within valid ranges

              some checks on the input parameters 

  Returns:    1 if no problems are encountered
              0 if some parameters are invalid
**************************************************************/

int check_input_params( int *P, int max_iter, int nf, int nst, 
                        int nc, float aol);
