@date   Tuesday, December 4, 2001, 4:02p
@author Lucas Scharenbroich

The fillowing problems were identified in em.c and cross_v.c and fixed

Problem #1
----------

The P(x|j) function was incorrect

wrong:
	m->P_x_j[j_idx] = -.5*(d/2*log(2*M_PI)+log_determinant(m,j_idx)+
                             mahalanobis_distance(m,data_idx,j_idx));

correct:
	m->P_x_j[j_idx] = -.5*(d*log(2*M_PI)+log_determinant(m,j_idx) +
                             mahalanobis_distance(m,data_idx,j_idx));

Bishop: p. 35 (2.4)


Problem #2
----------

EM Step in wrong order

wrong:

    update_P_j(m);
    update_means(m);
    update_variances(m);

    find_P_j_x(m);

correct:

    find_P_j_x(m);       /* Expectation */

    update_means(m);     /* Maximization */
    update_variances(m);
    update_P_j(m);


Problem #3
----------

In find_log_likelihood, the log((2*pi)^d) is being computed but never
used in the computation.  Gives artificially high values

wrong: Line 523

	point_likelihood += m->P_j[class_idx]*
			exp(-.5*(mahalanobis_distance(m, data_idx, class_idx) + 
		 	log_determinants[class_idx]));
correct:
	point_likelihood += m->P_j[class_idx] *
        		exp(-.5*(log_two_pi_d + log_determinants[class_idx] + 
                 	mahalanobis_distance(m, data_idx, class_idx)));


Problem #4
----------
	
The dataset is never normalized for the pseudo-correlation metric.  This
is a BIG problem.

fix: add a if (need_norm(m)) normalize_matrix() to the beginning of
initialize_parameters()


Problem #5
----------

The distance metric value is getting reset to zero which results in
Correlation being used every time.  The subroutine allocate_full_mem()
in cross_v.c failed to set the value

Problem #6
----------

When clusters collapsed they resulted in divide-by-zero errors
propogating through the calculation.  Adding an early-out test to 
update_means and update_variances fixes this.

if (m->P_j[class_idx] == 0.0) continue;

Problem #7
----------

diagem would segfault if not given the exact number of parameters on the
command line.  The fix was to add a check for the proper number of
parameters before using the arrays.  cross_v.c, line 1066

Fix:

if (argc < 3) {
    fprintf(stderr, "Usage: %s <input_file> <output_file>\n", argv[0]);
    exit(-1);
}

@date   Sunday, January 21, 2001, 6:08p
@author Ben Bornstein

BUG:

  The following could lead to an overflow in class_means:

  /**
   * Estimates the means of k classes using the class membership of each
   * datum.
   *
   * Where:
   *   k           = m->classes
   *   memberships = m->classifications
   *   datum       = m->data[i][]
   *   i           = [0 ... (m->datapoints - 1)]
   *   j           = [0 ... (m->features   - 1)]
   */
  void estimate_means(struct em_memory *m)
  {
    int class, i, j;


    zero_class_means(m);

    /* Accumulate running sum of m->data[i] based on class membership. */
    for (i = 0; i < m->datapoints; i++)
    {
      class = m->classifications[i];

      for (j = 0; j < m->features; j++)
      {
        m->class_means[class][j] += m->data[i][j];
      }
    }

    /* Divide running sum by class size to estimate means. */
    for (i = 0; i < m->classes; i++)
    {
      for (j = 0; j < m->features; j++)
      {
        m->class_means[i][j] /= ((long double) m->class_count_array[i]);
      }
    }
  }


FIX:

  /**
   * Estimates the means of k classes using the class membership of each
   * datum.
   *
   * Where:
   *   k           = m->classes
   *   memberships = m->classifications
   *   datum       = m->data[i][]
   *   i           = [0 ... (m->datapoints - 1)]
   *   j           = [0 ... (m->features   - 1)]
   */
  void estimate_means(struct em_memory *m)
  {
    int class, i, j;


    zero_class_means(m);

    /**
     * Accumulate running sum of each datum (m->data[i]) based on class
     * membership and divide running sum by class size to estimate means.
     *
     * Each datum is immediately divided by class size to guard against a
	   * possible overflow in m->class_means[][].
     */
    for (i = 0; i < m->datapoints; i++)
    {
      class = m->classifications[i];

      for (j = 0; j < m->features; j++)
      {
        m->class_means[class][j] +=
          ( m->data[i][j] / ((long double) m->class_count_array[class]) );
      }
    }
  }




@date   Friday, January 19, 2001, 9:52p
@author Ben Bornstein

 * Where:
 *   k           = m->classes
 *   means       = m->class_means
 *   memberships = m->classifications
 *   datum       = m->data[i][j]
 *   i           = [0 ... (m->datapoints - 1)]
 *   j           = [0 ... (m->features   - 1)]


  m[0] = center(datum)

  for i = 1:N
    x[i] = find_farthest(x[0], i, m->data)
  end


find_farthest(struct em_memory *m)
{
  double dist    = 0.0;
  double maximum = 0.0;

  maximum = find_average_distance_to_means(m->means, m->data[0]);

  for (i = 1; i < m->datapoints; i++)
  {
    dist = find_average_distance_to_means(m->means, m->data[i]);

    if (dist > maximum)
    {
      maximum = dist;
    }
  }

  return dist;
}


find_average_distance_to_means(double **means, double *datum, int N)
{
  double distance = 0.0;


  for (i = 0; i < num_means; i++)
  {
    distance = distance(datum, means[i], N);
  }

  distance = distance / num_means;

  return distance;
}


dump data matrix and classifications.


BUG:

  The following piece of code is in error.  Tobias and I added the while
  loop long after the original function was written.  m->class_count_array[]
  should be zeroed if the loop is executed more than once, otherwise
  m->class_count_array will contain improper counts.


  /**
   * Initializes the classification field of the em_memory structure.
   * For each datum, a classification is chosen at random.
   */
  void init_classifications(struct em_memory *m)
  {
    int class = 0;
    int i, j;

    zero_classifications(m);
    zero_class_counts(m);

    /** Large block of commented code omitted. **/


    while (get_num_empty_classes(m) != 0)
    {
      for (i = 0; i < m->datapoints; i++)
      {
        class                 = iran(0, m->classes, m->seed);
        m->classifications[i] = class;

        m->class_count_array[class]++;
      }
    }
  }


LESSON LEARNED:

  Invariant to test for is:

    The sum of all class counts should equal the number of datapoints.

         N
      ( Sum  m->class_count_array[i] ) == m->datapoints
       i = 0

    where N = (m->classes - 1)


FIX:


  /**
   * Initializes the classification field of the em_memory structure.
   * For each datum, a classification is chosen at random.
   */
  void init_classifications(struct em_memory *m)
  {
    int class = 0;
    int i, j;


    /**
     * It may be necessary to repeat this initialization step, if
     * each class does not have at least one datum.  Hence, the loop.
     */

    do
    {
      zero_classifications(m);
      zero_class_counts(m);

      for (i = 0; i < m->datapoints; i++)
      {
        class                 = iran(0, m->classes, m->seed);
        m->classifications[i] = class;

        m->class_count_array[class]++;
      }

    } while (get_num_empty_classes(m) != 0);
  }


BUG:

  In the above function, the line:

    class = iran(0, m->classes, m->seed);

  should be:

    class = iran(0, m->classes - 1, m->seed);
