I implemented mixture of T distributions thanks to
update equations by John Murphy. The *murphy* files
are data supplied by him, with ground truth supplied by him; 
classifications are identical.
