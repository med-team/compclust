function p = normalize_points(m)
  for idx = 1:size(m,1)
    norm = sqrt(sum(m(idx,:).^2));
    p(idx,:) = m(idx,:)./norm;
  end
  
	