#ifndef __EM_DA_H
#define __EM_DA_H

int DA_mean_dmat(double **mat, double *mean, int rows, int cols);
int est_weighted_covar(double **cov, double **mat, double *mean, 
                       double *probs, int rows, int cols);

void compute_pc(int i, int j, struct em_memory *m, double **eigenvectors);

void perturbe(int i, int j, struct em_memory *m);
int find_max_variance_cluster(struct em_memory *m);
int the_same(int j, struct em_memory *m);
void em_da(struct em_memory *m);

#endif
