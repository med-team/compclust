/******************************************************************************
 *
 * em_fast.c
 *
 * Very fast but very memory intensive version of EM.  This code path uses
 * alomst triple the memory of em.c
 *
 *****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <values.h>
#include "nr_util.h"
#include "em.h"
#include "em_fast.h"
#include "init_means.h"
#include "norm_data.h"
#include "dist_metrics.h"

/******************************************************************************
 *
 * double dinvdet(double **a, int n, double **inv)
 *
 * return determinant of matrix a[nxn] and its inverse inv[nxn]
 *
 * This uses an LU decomposition which will not fail for real, symmetric,
 * positive definite matricies.
 *
 *****************************************************************************/

double dinvdet(double **a, int n, double **inv) 
{
  int i, j, *indx;
  double d, *col;
  
  col  = NR_dvector(1, n);
  indx = NR_ivector(1, n);
  
  NR_dludcmp(a, n, indx, &d);
  for (j = 1; j <= n; j++) {
    d *= a[j][j];
    for (i = 0; i <= n; i++) 
      col[i] = 0.0;
    col[j] = 1.0;
    NR_dlubksb(a, n, indx, col);
    for (i = 1; i <= n; i++) 
      inv[i][j] = col[i];
  }
  
  NR_free_dvector(col, 1, n);
  NR_free_ivector(indx, 1, n);

  return d;
}

/*****************************************************************************
 *
 * Compute the mahalanobis distance.  Specialize for the scalar, diagonal,
 * and full covariance cases
 *
 ****************************************************************************/

double mahalanobis_scalar_fast(struct em_memory *m, int x, int c)
{
  int feature_idx;
  double m_dist = 0.0, dist, *means, *data;

  data     = m->data[x];
  means    = m->class_means[c];

  for(feature_idx = 1; feature_idx <= m->features; feature_idx++){
    dist = (data[feature_idx] - means[feature_idx]);
    m_dist += (dist * dist);
  }
  m_dist *= m->inv_covariance.scalar[c];

  return m_dist;
}

double mahalanobis_diag_fast(struct em_memory *m, int x, int c)
{
  int feature_idx;
  double m_dist = 0.0, dist;
  double *means, *inv_variance, *data;

  data  = m->data[x];
  means = m->class_means[c];
  inv_variance = m->inv_covariance.diagonal[c];

  for(feature_idx = 1; feature_idx <= m->features; feature_idx++){
    dist = (data[feature_idx] - means[feature_idx]);
    m_dist += (dist * dist * inv_variance[feature_idx]);
  }

  return m_dist;
}

double mahalanobis_full_fast(struct em_memory *m, int x, int c)
{
  int feature_idx, col;
  double m_dist = 0.0, *dist, *temp;
  double *means, **inv_variance, *data;

  data  = m->data[x];
  means = m->class_means[c];
  dist  = NR_dvector(1, m->features);
  temp  = NR_dvector(1, m->features);

  inv_variance = m->inv_covariance.full[c];
  
  subtract_dvec(data, means, dist, m->features);

  /* compute xA */
  for (col = 1; col <= m->features; col++) 
    for (feature_idx = 1; feature_idx <= m->features; feature_idx++)
      temp[col] = 
        dist[feature_idx] * dist[feature_idx] * inv_variance[feature_idx][col];
  
  /* complete xAx */
  for (feature_idx = 1; feature_idx <= m->features; feature_idx++)
    m_dist += temp[feature_idx] * dist[feature_idx];

  NR_free_dvector(temp, 1, m->features);
  NR_free_dvector(dist, 1, m->features);

  return m_dist;
}

/*****************************************************************************
 *
 * Build an inverse covariance matrix for use in the inner loop.  Also 
 * specialize for the scalar, diagonal, and full covariance cases.  Really
 * most useful in the full covariance case.  Also since the determinant
 * comes for free with the inverse, return it.
 *
 *****************************************************************************/

double build_inv_covariance_scalar(struct em_memory *m, int c)
{
  double v;

  v = m->class_variances.scalar[c];

  m->inv_covariance.scalar[c] = 1.0 / v;
  return m->features * log(v);
}

double build_inv_covariance_diag(struct em_memory *m, int c)
{
  int feature_idx;
  double *inv_variance, *variance, log_det = 0.0;

  inv_variance = m->inv_covariance.diagonal[c];
  variance     = m->class_variances.diagonal[c];

  for (feature_idx = 1; feature_idx <= m->features; feature_idx++) {
    inv_variance[feature_idx] = 1.0 / variance[feature_idx];
    log_det += log(variance[feature_idx]);
  }

  return log_det;
}

double build_inv_covariance_full(struct em_memory *m, int c)
{
  double **mat_copy, det;
  int n;

  n = m->features;

  mat_copy = NR_dmatrix(1, n, 1, n);
  copy_dmat(m->class_variances.full[c], mat_copy, n, n);

  det = dinvdet(mat_copy, n, m->inv_covariance.full[c]);

  NR_free_dmatrix(mat_copy, 1, n, 1, n);

  return log(det);
}

/******************************************************************************
 *
 * memory intensive version of find_P_j_x
 *
 *****************************************************************************/

void find_P_j_x_fast(struct em_memory *m)
{
  int data_idx, j_idx, count;
  double denom, log_max_pxj;
  double beta, d_log_2pi, p_x_j;
  double old_prob, curr_prob;

  /*
   * This is a constant in the inner loop 
   */

  d_log_2pi = m->features * log(2*M_PI);

  /*
   * Determine what the annealing exponnent is
   */

  beta = (double) 1.0;
  if(m->params->algorithm == EM_DET_ANNEAL)
    beta = (double) m->inv_temp;

  /* 
   * build a table of log_determinants and precompute the inverse
   * covariance matricies at the same time
   */

  for(j_idx = 1; j_idx <= m->classes; j_idx++) {
    m->log_det[j_idx] = m->log_determinant(m, j_idx) + d_log_2pi;
    m->log_p_j[j_idx] = log(m->P_j[j_idx]);
  }

  /*
   * Run over all the data points
   */

  for(data_idx = 1; data_idx <= m->datapoints; data_idx++) {  
 
    /*  
     * first, find the log p_x_j terms (Gaussian pdf) and the
     * largest p_x_j.
     */
    
    p_x_j = -.5*(m->log_det[1] + m->mahalanobis_distance(m, data_idx, 1));
    m->p_x_j[1] = log_max_pxj = p_x_j;

    for(j_idx = 2; j_idx <= m->classes; j_idx++) {
      p_x_j = -.5*(m->log_det[j_idx] + 
                   m->mahalanobis_distance(m, data_idx, j_idx));
      
      m->p_x_j[j_idx] = p_x_j;
      if (p_x_j > log_max_pxj)
        log_max_pxj = p_x_j;
    }
    
    /* find the denominator and cache some (complex) calculation */
    denom = 0.0;
    for(j_idx = 1; j_idx <= m->classes; j_idx++) {
      m->p_x_j[j_idx] = 
        exp(beta * (m->p_x_j[j_idx] - log_max_pxj + m->log_p_j[j_idx]));

      denom += m->p_x_j[j_idx];
    }

    /* multiplication is faster than division, so invert the denom */
    denom = 1.0 / denom;

    /* now we have the biggest p_x_j */
    for(j_idx = 1; j_idx <= m->classes; j_idx++) 
      m->P_j_x[j_idx][data_idx] = m->p_x_j[j_idx] * denom;
  }
}

/*****************************************************************************
 *
 * void em_run_fast(struct em_memory *m)
 *
 * Do the inner EM loop, executing the update equations.  Early-out testing
 * could be added in here 
 *
 ****************************************************************************/

int em_run_fast(struct em_memory *m, int start_index)
{
  int i, iteration_idx, iterations, step;
  double delta, sum, trace[TRACE];

  for (i = 0; i < TRACE; i++)
    trace[i] = i*10;

  /**
   *
   * Set up pointers to functions
   *
   **/

  m->update_means = update_means;
  m->update_P_j   = update_P_j;
  m->find_P_j_x   = find_P_j_x_fast;

  switch (m->params->em_type) {
  case SCALAR:
    m->log_determinant      = build_inv_covariance_scalar;
    m->mahalanobis_distance = mahalanobis_scalar_fast;
    m->update_variances     = update_variances_scalar;
    break;
  case DIAG:
    m->log_determinant      = build_inv_covariance_diag;
    m->mahalanobis_distance = mahalanobis_diag_fast;
    m->update_variances     = update_variances_diag;
    break;
  case FULL:
    m->log_determinant      = build_inv_covariance_full;
    m->mahalanobis_distance = mahalanobis_full_fast;
    m->update_variances     = update_variances_full;
    break;
  }
   
  iterations = m->params->max_em_iterations;
  step       = m->params->output_stepsize;

  /** 
   *
   * Do em style updates of parameters. 
   *
   **/

  for (iteration_idx = start_index; 
       iteration_idx < start_index + iterations; 
       iteration_idx++)
  {
    if (step > 0 && (iteration_idx % step) == 0)
      dumpvars(m, iteration_idx);

    m->find_P_j_x(m);           /* Expectation */

    /**
     *
     * It is OK to compute P_j_new here because P_j is not actually used
     * in any of the update equations.  The P_j seen in update_means and
     * update_variances is actually used as a cache of SUM P_old(j | x)
     *
     **/

    delta = m->update_P_j(m);           

    /**
     *
     * Update the mean and variances using the cached SUM in P_j
     *
     **/
    
    m->update_means(m);             /* Maximization */
    m->update_variances(m);

    /**
     *
     * Convergence test:
     *    Sum the class probability deltas over the last n steps.  If the
     * total change is less than epsilon, break out.
     * 
     **/

    trace[iteration_idx % TRACE] = delta;
    sum = 0.0;
    for (i = 0; i < TRACE; i++)
      sum += trace[i];

    if (sum < EMEPS) {
      break;
    }
  }

  if (step > 0 && (iteration_idx % step) == 0)
    dumpvars(m, iteration_idx);

  return iteration_idx;
}
