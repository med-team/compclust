numpoints = [2500 2500 2500 2500];
means = [0 0; 0 4; -3 1; 4 4];
covars = cell(2,1);
covars{1} = [1 0; 0 1];
covars{2} = [2 -0.3; -0.3 0.1];
covars{3} = [0.5 0.2; 0.2 1.5];
covars{4} = [0.1 0; 0 0.1];
data = gaussdata(numpoints, means, covars);
plot(data(:,1), data(:,2),'.');
save synth.data data -ascii
