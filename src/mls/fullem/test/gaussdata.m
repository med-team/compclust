function data = gaussdata(numpoints, means, covars);

[nclusters, dim] = size(means);

start = 1;

for cluster = 1:nclusters,
  clustdata = randn(numpoints(cluster), dim);
  clustdata = clustdata * sqrtm(covars{cluster});
  clustdata = clustdata + ones(numpoints(cluster),1) * means(cluster,:);
  data(start:(start + numpoints(cluster) - 1),:) = clustdata; 
  start = start + numpoints(cluster);
end;
