/* kmeans.h */

#ifndef lint
static char kmeans_hdr_rcsid[] = "$Id: k.h,v 1.1 2001/05/21 23:36:19 diane Exp $";
#endif
/* $Log: k.h,v $
 * Revision 1.1  2001/05/21 23:36:19  diane
 * Initial revision
 *
 * */

/* constants */

#define NUM_ARGS 7

/* 
#define UT_VERBOSE     UT_TRUE
*/

/* function declarations */

#ifdef __STDC__

void initialize_args(arg args[]);
float euclid_dist(float *v1, float *v2, int length);
int randomize_means(float **means, int nr, int nc);

#else 

void initialize_args();
float euclid_dist();
int randomize_means();

#endif


