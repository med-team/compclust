/*******************************************************************************
  Title:     Parallel K-means
  Author:    Alexander Gray, Robert Granat
  Function:  Perform the k-means algorithm on a dataset, returning as output
             a file containing the mean vectors corresponding to the k centroids
             hopefully corresponding to the densest portions of the attribute
             space of the input data.
  Reference: Duda and Hart, ch. 6 (p. 201)
  How used:  For implementation on a parallel computer.  Parallel commands
	     are in pvm, but command usage is in certain cases specific to
	     the Cray T3D version of pvm.
  Compile:   make
  Example:   kmeans -i test.data -rows 500 -cols 10 -o test.means -k 4 -seed 13
  Notes:     Do not attempt to run this program in other parallel environments
	     without revision.
*******************************************************************************/
#ifndef lint
static char rcsid[] = "$Id: k.c,v 1.1 2001/05/21 23:36:19 diane Exp $";
#endif
/* $Log: k.c,v $
 * Revision 1.1  2001/05/21 23:36:19  diane
 * Initial revision
 *
 * */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <pvm3.h>
#include <time.h>
#include "util.h"
#include "nr.h"
#include "da.h"
#include "k.h"

/*******************************************************************************
 MAIN
 Driver of program.
*******************************************************************************/
main(argc, argv)
    int   argc;
    char  *argv[];
{
    arg            args[NUM_ARGS+1];
    char           *datafile, *meansfile, *labelsfile;
    int            numrows, numcols;
    int            startrow, extrarows;
    float          **data, **means, **oldmeans, **tempmeans, **othermeans;
    float          *minval, *maxval, *range, *otherminval, *othermaxval;
    int            *clustsize, *otherclustsize;
    int            *class;
    int            i, j, k;
    int            K, bestclass;
    float          dist, mindist;
    bool           stable = UT_FALSE, zero_cluster = UT_FALSE;
    int            otherstable;
    int            numiters = 0;
    int            seed = 1234;
    int            numreconverge = 0;

    /* Variables for use in parallel aspects */

    int		   pvm_tid;
    int		   pe;
    int 	   cc;
    int 	   buf;
    int    	   numPE;
    double         abs_starttime, abs_endtime, time1, time2;

/*----------------------------------------------------------------------------*/
    /* Get information about processes and current process */

    pvm_tid = pvm_mytid(); /*Find out which process number*/
    pe = pvm_get_PE(pvm_tid); /*Get the virtual PE number*/
 
    numPE = pvm_gsize(0); /*Do the head count*/

/*----------------------------------------------------------------------------*/
    /* Get command line arguments */
    abs_starttime = time_now();
/*    printf("%d: The absolute time now is %lf seconds.\n",pe,abs_starttime);*/
    initialize_args(args);

    if (Get_Args(argc-1, argv, args, NUM_ARGS) != Get_Arg_OK) {
      return(-1);
    }

    if (args[1].exists) datafile   = args[1].arg_str;
    if (args[2].exists) meansfile  = args[2].arg_str;
    if (args[3].exists) labelsfile = args[3].arg_str;
    if (args[4].exists) numrows    = atoi(args[4].arg_str);
    if (args[5].exists) numcols    = atoi(args[5].arg_str);
    if (args[6].exists) K          = atoi(args[6].arg_str);
    if (args[7].exists) seed       = atoi(args[7].arg_str);

/*----------------------------------------------------------------------------*/
    /* read in the data matrix */

    extrarows = numrows%numPE;
    if (extrarows > pe)
      {
      numrows = (int)(numrows/numPE)+1;
      startrow = pe*numrows + 1;
      }
    else
      {
      numrows = (int)(numrows/numPE);
      startrow = pe*numrows + extrarows + 1;
      }

/*
    read_subset2matrix(datafile, startrow, numrows, numcols, &data);
*/

    time1 = time_now();
    read_binary2matrix(datafile, startrow, numrows, numcols, &data);
    time2 = time_now();
    printf("%d: i/o time = %lf\n",pe,time2-time1);

/*
    read_matrix(datafile, numrows, numcols, &data);
*/

    /* create storage for the K mean vectors */
    means = matrix(1,K,1,numcols);
    oldmeans = matrix(1,K,1,numcols);
    clustsize = ivector(1,K);

    /* create storage for the passed values */
    othermeans = matrix(1,K,1,numcols);
    otherclustsize = ivector(1,K);

    /* create storage for the classes for each datum */
    class = ivector(1,numrows);

/*----------------------------------------------------------------------------*/
 
/*----------------------------------------------------------------------------*/
    /* normalize each attribute to a 0-1 scale */

    /* create storage for the normalization */
    minval = vector(1,numcols);
    maxval = vector(1,numcols);
    range  = vector(1,numcols);

    /* create storage for the passed min/max values */
    otherminval = vector(1,numcols);
    othermaxval = vector(1,numcols);

    /* for each attribute, find the min and max */
    for (j=1; j<=numcols; j++)
      minval[j] = maxval[j] = data[1][j];

    for (i=1; i<=numrows; i++)
      for (j=1; j<=numcols; j++)
      {
      if (data[i][j] < minval[j])
        minval[j] = data[i][j];
      if (data[i][j] > maxval[j])
        maxval[j] = data[i][j];
      }

      /* Pass mimumim and maximum values in from each process to the controlling
         process */

    if (pe != 0) /* This is not the controlling process */
    {	
      /* Do the minimum value */
      cc = pvm_initsend(PvmDataRaw); 
      cc = pvm_pkfloat(&minval[1],numcols,1); 
      cc = pvm_send(0,pe); 
	  
      /* Now block until receive broadcast message from the controlling
         process giving global minimum values */
	  
      buf = pvm_recv(0,0);
      cc = pvm_upkfloat(&minval[1],numcols,1);
      cc = pvm_freebuf(buf);
	  
      /* Do the maximum value */
      cc = pvm_initsend(PvmDataRaw);
      cc = pvm_pkfloat(&maxval[1],numcols,1);
      cc = pvm_send(0,pe);
	  
      /* Now block until receive broadcast message from the controlling
         process giving global maximum values */
 
      buf = pvm_recv(0,0);
      cc = pvm_upkfloat(&maxval[1],numcols,1);
      cc = pvm_freebuf(buf);

      barrier();

      buf = pvm_recv(0,0);
      cc = pvm_upkfloat(&oldmeans[1][1],K*numcols,1);
      cc = pvm_freebuf(buf);
    }
    else /* This is the controlling process */
    {
      /* Now receive the minimum values from each PE */
      time1 = time_now();
      for(i=1;i<numPE;i++)
      {
        buf = pvm_recv(i,i);
        cc = pvm_upkfloat(&otherminval[1],numcols,1);
        cc = pvm_freebuf(buf);
	      
        for(j=1; j<=numcols; j++)
          if (otherminval[j] < minval[j])
             minval[j] = otherminval[j];
      } 
      /* Now broadcast the global minimum values */
      cc = pvm_initsend(PvmDataRaw);
      cc = pvm_pkfloat(&minval[1],numcols,1);
      cc = pvm_bcast(0,0);

      /* Now receive the maximum values from each PE */
      for(i=1;i<numPE;i++)
      {
        buf = pvm_recv(i,i);
        cc = pvm_upkfloat(&othermaxval[1],numcols,1);
        cc = pvm_freebuf(buf);
 
        for(j=1; j<=numcols; j++)
          if (othermaxval[j] < maxval[j])
	    maxval[j] = othermaxval[j];
      }
      /* Now broadcast the global minimum values */
      cc = pvm_initsend(PvmDataRaw);
      cc = pvm_pkfloat(&maxval[1],numcols,1);
      cc = pvm_bcast(0,0);
      time2 = time_now();
      printf("%d: time to pass min/max values = %lf\n",pe,time2-time1);

      /* choose initial values for means arbitrarily */
      utSeedRandomByTimeOfDay();
      randomize_means(oldmeans, K, numcols);

      barrier();

      time1 = time_now();
      cc = pvm_initsend(PvmDataRaw);
      cc = pvm_pkfloat(&oldmeans[1][1],K*numcols,1);
      cc = pvm_bcast(0,0);
      time2 = time_now();
      printf("%d: time to pass starting means = %lf\n",pe,time2-time1);
    }

    /* scale each attribute value by its range and translate by its minval */
    for (j=1; j<=numcols; j++)
      range[j] = maxval[j] - minval[j];

    for (i=1; i<=numrows; i++)
      for (j=1; j<=numcols; j++)
        data[i][j] = (data[i][j] - minval[j]) / range[j];

/*----------------------------------------------------------------------------*/
    /* perform k-means iterations */
    while (stable == UT_FALSE)
    {
      time1 = time_now();
      stable = UT_TRUE;
  
      /* zero out new means */
      for (k=1; k<=K; k++)
        for (j=1; j<=numcols; j++)
          means[k][j] = 0;
	  
      /* zero out new cluster sizes */
      for (k=1; k<=K; k++)
        clustsize[k] = 0;
	  
      /* compute distance of each datum to each mean */
      for (i=1; i<=numrows; i++)
      {
        mindist = euclid_dist(data[i],oldmeans[1],numcols);
        bestclass = 1;
        for (k=2; k<=K; k++)
        {
          if ((dist = euclid_dist(data[i],oldmeans[k],numcols)) < mindist)
          {
            mindist = dist;
            bestclass = k;
          }
        }

        /* assign class and see if different from last iteration */
        if (class[i] != bestclass)
        {
          class[i] = bestclass;
          stable = UT_FALSE;
        }

        /* contribute to the new mean based on class assignment */
        for (j=1; j<=numcols; j++)
          means[class[i]][j] += data[i][j];
        clustsize[class[i]] += 1;
      }
      time2 = time_now();
      printf("%d: time to do actual calculation = %lf.\n",pe,time2-time1);

      /* keep track of num iterations */
      numiters++;
      /* printf("iter# %d\n", numiters); */
  

      /* Do parallel communication */
      if (pe != 0) /* This is not the controlling process */
      {
        /* printf("%d: This is not the controlling process.\n",pe); */
     
        /* Send out the calculated cluster sizes */
	      
        cc = pvm_initsend(PvmDataRaw);
        cc = pvm_pkint(&clustsize[1],K,1);
        cc = pvm_send(0,pe);

        /* Now block until receive broadcast message from the controlling
           process giving the total cluster sizes */

        buf = pvm_recv(0,0);
        cc = pvm_upkint(&clustsize[1],K,1);
        cc = pvm_freebuf(buf);

        /* Check to see if a cluster has been eliminated */

        for (k=1; k<=K; k++)
          if (clustsize[k] == 0)
          {
            zero_cluster = UT_TRUE;
            break;
          }

        /* If a cluster has been eliminated, break out of the while loop
           and start over */

        if (zero_cluster == UT_TRUE)
        {
          printf("Restarted convergence after %d iterations.\n", numiters);
          buf = pvm_recv(0,0);
          cc = pvm_upkfloat(&oldmeans[1][1],K*numcols,1);
          cc = pvm_freebuf(buf);
          numiters = 0;
          stable = UT_FALSE;
          numreconverge += 1;
          printf("This is the %dth reconvergence attempt.\n", numreconverge);
          continue;
        }

        /* normalize the means by the cluster sizes */
        for (k=1; k<=K; k++)
          for (j=1; j<=numcols; j++)
            means[k][j] /= (float) clustsize[k];

        /* Send out the local means */

        cc = pvm_initsend(PvmDataRaw);
        cc = pvm_pkfloat(&means[1][1],numcols*K,1);
        cc = pvm_send(0,pe);

        /* Now block until receive broadcast message from the controlling 
           process giving the global means */

        buf = pvm_recv(0,0);
        cc = pvm_upkfloat(&means[1][1],numcols*K,1);
        cc = pvm_freebuf(buf);

        /* Send the stability measurement to the controlling processor */

        cc = pvm_initsend(PvmDataRaw);
        cc = pvm_pkint(&stable,1,1);
        cc = pvm_send(0,pe); /* Send data to the controlling process using own
                                PE number as the message tag */

 
        /* Now block until receive broadcast message from controlling process
           giving the stability */
 
        buf = pvm_recv(0,0);
        cc = pvm_upkint(&stable,1,1);
        cc = pvm_freebuf(buf);
      }
      else /* This is the controlling process */
      {
        /* printf("%d: This is the controlling process.\n",pe); */

        time1 = time_now();
        /* Receive the other cluster sizes */
        for(i=1;i<numPE;i++)
        {
          buf = pvm_recv(i,i);
          cc = pvm_upkint(&otherclustsize[1],K,1);
          cc = pvm_freebuf(buf);

          for(k=1;k<=K;k++)
          clustsize[k] += otherclustsize[k];
        }
        /* Now broadcast the new cluster sizes */
        cc = pvm_initsend(PvmDataRaw);
        cc = pvm_pkint(&clustsize[1],K,1);
        cc = pvm_bcast(0,0);
    
        /* Check to see if a cluster has been eliminated */
        for (k=1; k<=K; k++)
          if (clustsize[k] == 0)
          {
            zero_cluster = UT_TRUE;
            break;
          }
 
        /* If a cluster has been eliminated, break out of the while loop
           and start over */
        if (zero_cluster == UT_TRUE)
        {
          printf("Restarted convergence after %d iterations.\n", numiters);
          randomize_means(oldmeans, K, numcols);
          cc = pvm_initsend(PvmDataRaw);
          cc = pvm_pkfloat(&oldmeans[1][1],K*numcols,1);
          cc = pvm_bcast(0,0);
          stable = UT_FALSE;
          numreconverge += 1;
          printf("This is the %dth reconvergence attempt.\n", numreconverge);
          continue;
        }
 
        /* normalize the means by the cluster sizes */
        for (k=1; k<=K; k++)
          for (j=1; j<=numcols; j++)
            means[k][j] /= (float) clustsize[k];

        /* Receive the other means */
        for(i=1;i<numPE;i++)
        {
          buf = pvm_recv(i,i);
          cc = pvm_upkfloat(&othermeans[1][1],numcols*K,1);
          cc = pvm_freebuf(buf);
  
          for(k=1;k<=K;k++)
            for(j=1;j<=numcols;j++)
              means[k][j] += othermeans[k][j];
        }
        /* Now broadcast the new weighted means */
        cc = pvm_initsend(PvmDataRaw);
        cc = pvm_pkfloat(&means[1][1],numcols*K,1);
        cc = pvm_bcast(0,0);
     
        /* Receive the other stability measurements */
        for (i=1;i<numPE;i++)
        {
          buf = pvm_recv(i,i);
          cc = pvm_upkint(&otherstable,1,1);
          cc = pvm_freebuf(buf);

          if ((stable != UT_TRUE) || (otherstable != UT_TRUE))
            stable = UT_FALSE;
        }
        /* Now broadcast the overall stability */
        cc = pvm_initsend(PvmDataRaw);
        cc = pvm_pkint(&stable,1,1);
        cc = pvm_bcast(0,0);
        time2 = time_now();
        printf("%d: time to do message passing = %lf \n",pe,time2-time1);
      }

      /* make the new means into the oldmeans for next iteration */
      tempmeans = oldmeans; oldmeans = means; means = tempmeans;
    }

/*----------------------------------------------------------------------------*/
    /* unnormalize the means */
    for (k=1; k<=K; k++)
      for (j=1; j<=numcols; j++)
        means[k][j] = means[k][j] * range[j] + minval[j];

/*----------------------------------------------------------------------------*/
    /* Done */

    /* write output file(s) */
    printf("%d: writing output files.\n",pe);

    /* write class information */

/*
    time1 = time_now();
    write_bin_icol(labelsfile, startrow, numrows, class, "w");
    time2 = time_now();
    printf("%d: time to write labels file = %lf \n",pe,time2-time1);
*/

    /* wait until previous processes finish, then append class information
       to labelsfile */

    time1 = time_now();
    for(i=0;i<pe;i++)
    {
      buf = pvm_recv(i,i);
      cc = pvm_freebuf(buf);
    }
    write_icol(labelsfile, numrows, class, "a");
    if (numPE > 0)
    {
      cc = pvm_initsend(PvmDataRaw);
      cc = pvm_pkint(&pe,1,1);
      cc = pvm_bcast(0,pe);
    }
    time2 = time_now();
    printf("%d: time to write labels file = %lf \n",pe,time2-time1);

    /* write the means */

    time1 = time_now();
    if (pe == 0)
      write_matrix(meansfile, K, numcols, means, "w");
    time2 = time_now();
    printf("%d: time to write means file = %lf \n",pe,time2-time1);

    /* */
    write_row("range", numcols, range, "w");
    /* */

    /* clean up memory */
    free_vector(minval, 1, numcols);
    free_vector(maxval, 1, numcols);
    free_vector(otherminval, 1, numcols);
    free_vector(othermaxval, 1, numcols);
    free_vector(range, 1, numcols);
    free_ivector(class, 1, K);
    free_ivector(clustsize, 1, K);
    free_ivector(otherclustsize, 1, K);
    free_matrix(data, 1, numrows, 1, numcols);
    free_matrix(means, 1, K, 1, numcols);
    free_matrix(othermeans, 1, K, 1, numcols);
    free_matrix(oldmeans, 1, K, 1, numcols);

    abs_endtime = time_now();
    printf("%d: The absolute end time is %lf seconds.\n",pe,abs_endtime);
    printf("%d: The difference between absolute start and end times is %lf seconds.\n",pe,abs_endtime-abs_starttime);
    return(UT_OK);
}

/*******************************************************************************
 EUCLID_DIST
 Compute the Euclidean distance from one vector to another
*******************************************************************************/
float euclid_dist(v1,v2,length)
    float *v1, *v2;
    int length;
{
    int i;
    float dist = 0, diff;

          /* 
print_row(stdout, length, v1);
print_row(stdout, length, v2);
          */

    for (i=1;i<=length;i++)
    {
      diff = v1[i] - v2[i];
      dist += diff * diff;
    }

    return (dist);
}


/*******************************************************************************
 RANDOMIZE_MEANS
 Randomly set the mean vectors, drawing from 0-1 uniform distribution for each
 attribute value
*******************************************************************************/
int randomize_means(means, nr, nc)
    float **means;
    int nr, nc;
{
    int j,k;

    for (k=1; k<=nr; k++)
      for (j=1; j<=nc; j++)
        means[k][j] = (float) utScaledRand(0.0,1.0);

    /*
      write_matrix("randmeans", K, numcols, oldmeans, "w");
      */

    return (UT_OK);
}


/*******************************************************************************
 INITIALIZE_ARGS
 Initialize structures that store command-line argument information.
*******************************************************************************/
void initialize_args(args)
    arg args[];
{
    Init_Arg(args, 1, "i",     FALSE, 1, "input data matrix file");
    Init_Arg(args, 2, "m",     FALSE, 1, "output mean vectors file");
    Init_Arg(args, 3, "c",     FALSE, 1, "output cluster labels file");
    Init_Arg(args, 4, "rows",  FALSE, 1, "number of rows of input matrix");
    Init_Arg(args, 5, "cols",  FALSE, 1, "number of columns of input matrix");
    Init_Arg(args, 6, "k",     FALSE, 1, "number of means to find");
    Init_Arg(args, 7, "seed",   TRUE, 1, "random seed to use");

    return;
}
