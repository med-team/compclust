CURDIR := $(MLSDIR)/fullem

FULLEMSRC := $(addprefix $(CURDIR)/, mix_em_pp_wrapper.c mix_em_pp.c )
SRC += $(FULLEMSRC)
CFLAGS += -I$(CURDIR)

FULLEMLIBS = $(BASEDIR)/da/libda$(LIBEXT) \
            $(BASEDIR)/nr/libnr$(LIBEXT) \
            $(BASEDIR)/ut/libut$(LIBEXT) \
            $(BASEDIR)/cp/libcp$(LIBEXT) \
            $(MPIHOME) 
#	    $(MLSDIR)/common/libcommon$(LIBEXT) \
#	    $(MLSDIR)/normalizations/libnorm$(LIBEXT) \
#	    $(MLSDIR)/distances/libdist$(LIBEXT) \
#	    $(MLSDIR)/initializations/libinit$(LIBEXT) \

FULLEMLIBDIRFLAGS := $(call make_libdirflag,$(FULLEMLIBS))
FULLEMLIBLINKFLAGS := $(call make_liblinkflag,$(FULLEMLIBS)) -lm -lnsl -lmpi

FULLEM := $(CURDIR)/fullem$(BINEXT)
TARGETBINS += $(FULLEM)

$(FULLEM): $(FULLEMSRC:.c=$(OBJEXT)) $(FULLEMLIBS)
	$(CC) $(FULLEMLIBDIRFLAGS) -o $@ $(FULLEMSRC:.c=$(OBJEXT)) $(FULLEMLIBLINKFLAGS)
