/* mix_em_pp_wrapper.h */

#ifndef lint
static char mix_em_pp_wrapper_hdr_rcsid[] = "$Id: mix_em_pp_wrapper.h,v 1.2 2001/12/05 00:47:59 hart Exp $";
#endif
/* 
 * $Log: mix_em_pp_wrapper.h,v $
 * Revision 1.2  2001/12/05 00:47:59  hart
 * Modifications to allow users to specify the exact locations to initialize
 * cluster means.  This is in effort to build in an ability to chain FullEM as
 * post clustering clusteirng algo.
 *
 * Revision 1.1.1.1  2001/05/21 23:36:19  diane
 * Import of various mls & woldlab source packages
 *
 * Revision 1.1  2001/01/26 18:17:41  granat
 * Initial revision
 *
 * Revision 1.2  1996/07/19 18:03:12  agray
 * added number of channels as argument
 *
 * Revision 1.1  1996/07/11 18:29:41  agray
 * Initial revision
 *
 * */

/* constants */

#define EM_NUM_ARGS         25

#define EM_DEFAULT_PARAMS_FILENAME  "em.params"
#define EM_DEFAULT_LABELS_FILENAME  "em.labels"
#define EM_DEFAULT_PROBS_FILENAME  "em.probs"
#define EM_DEFAULT_LOG_FILENAME     "em.log"

#define EM_DEFAULT_SEED        12345
#define EM_DEFAULT_MIN_DIAG     .001
#define EM_DEFAULT_MIN_WGT      .001
#define EM_DEFAULT_THRESH       .001
#define EM_DEFAULT_TRIES          10
#define EM_DEFAULT_MAX_RECONVERGE  5
#define EM_DEFAULT_MAX_RESTARTS    5
#define EM_DEFAULT_NUM_CHANNELS    8
#define EM_DEFAULT_TRIALS         10
#define EM_DEFAULT_MAX_DIMS        0

#define EM_DEFAULT_DEBUG_LEVEL 0
#define EM_DEFAULT_LOG_LEVEL   0


/* function declarations */

#ifdef __STDC__

void initialize_args(arg args[]);

#else 

void initialize_args();

#endif
