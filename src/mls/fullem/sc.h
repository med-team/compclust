/* sc.h */

#ifndef lint
static char sc_hdr_rcsid[] = "$Id: sc.h,v 1.1 2001/05/21 23:36:19 diane Exp $";
#endif
/* 
 * $Log: sc.h,v $
 * Revision 1.1  2001/05/21 23:36:19  diane
 * Initial revision
 *
 * Revision 1.2  1996/07/19 18:03:12  agray
 * added number of channels as argument
 *
 * Revision 1.1  1996/07/11 18:29:41  agray
 * Initial revision
 *
 * */

/* constants */

#define EM_NUM_ARGS         18

#define EM_DEFAULT_PARAMS_FILENAME  "em.params"
#define EM_DEFAULT_LABELS_FILENAME  "em.labels"
#define EM_DEFAULT_LOG_FILENAME     "em.log"

#define EM_DEFAULT_SEED        12345
#define EM_DEFAULT_MIN_DIAG     .001
#define EM_DEFAULT_MIN_WGT      .001
#define EM_DEFAULT_THRESH       .001
#define EM_DEFAULT_TRIES          10
#define EM_DEFAULT_MAX_RECONVERGE  5
#define EM_DEFAULT_MAX_RESTARTS    5
#define EM_DEFAULT_NUM_CHANNELS    8

#define EM_DEFAULT_DEBUG_LEVEL 0
#define EM_DEFAULT_LOG_LEVEL   0


/* function declarations */

#ifdef __STDC__

void initialize_args(arg args[]);

#else 

void initialize_args();

#endif
