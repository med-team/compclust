#ifndef lint
static char rcsid[] = "$Id: mix_em_pp.c,v 1.2 2001/12/05 00:47:59 hart Exp $";
#endif
/* 
 * $Log: mix_em_pp.c,v $
 * Revision 1.2  2001/12/05 00:47:59  hart
 * Modifications to allow users to specify the exact locations to initialize
 * cluster means.  This is in effort to build in an ability to chain FullEM as
 * post clustering clusteirng algo.
 *
 * Revision 1.1.1.1  2001/05/21 23:36:19  diane
 * Import of various mls & woldlab source packages
 *
 * Revision 1.5  2001/03/22 19:35:51  granat
 * in cross-validation function, removed function call to actually calculate
 * results for the best k.
 * reinstated early stopping heuristic (likelihood decreases three times in a
 * row)
 *
 * Revision 1.4  2001/01/26 21:27:56  granat
 * fixed numerical problem relating to k-means initialization
 *
 * Revision 1.3  2001/01/26 17:59:46  granat
 * added annealing version of the method, fixed some bugs
 *
 * Revision 1.2  2000/02/28 18:49:52  granat
 * changed search strategy over k
 * still buggy
 *
 * */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <float.h>

#include "mpi.h"

#include "cp.h"

#include "ut.h"

#include "nr.h"
#include "nr_util.h"

#include "da.h"
#include "da_pp.h"
#include "da_util.h" 

#include "mix_em_pp.h"

/*******************************************************************************
MIX_EM_ALLOC_PP
Parallel version of mix_em_alloc().
Perform the EM clustering algorithm on a dataset of continuous values,
returning the matrix of k mean vectors, a set of k covariance matrices,
a matrix of class probabilities, a vector of class weights, and a class
labelling for each of the data.

The input matrices and vectors are all assumed to have been allocated
before calling this function.  The data is assumed to be stored in row 
vector form (i.e. each row is a datum), divided among the different 
processes.  The input variable numtries is the number of times k-means is
run to determine the initials means for the EM algorithm.  The input
variables max_reconverge and max_restarts control the maximum number of
times the k-means and EM algorithms respectively try in the event that
there is an empty cluster.

NOTE: In practice, it is a good idea to normalize the columns of the data to
the same scale before performing EM (see the function normalize_data()).
AG, RG

*******************************************************************************/
int mix_em_alloc_pp(double **data, int numrows, int numcols, double **means, 
                    double ***covars, double **probs, double *weights, 
                    int *class, int K, double threshold, double min_weight,
                    double min_diag, int numtries, int max_reconverge, 
                    int max_restarts, boolean empty_cluster_ok, boolean means_already_initialized)

{
  double          *data_i, *data_j, *data_j2;
  double          **transposed_data, **initmeans;
  double          **newmean;
  double          *minval, *maxval, *range;
  int            i, j, j2, k, l, p;
  int            *clustsize;
  boolean        converged;
  int            numiters, seed;
  int            posdef;
  double         cond;
  int            num_reconverge, num_empty_clusters;
  int            num_restarts;
  int            num_bad_kmeans;
  double          **best_means, ***best_covars;
  double          *mean_k, **covar_k;
  double          *prob_data, *sum_probs, *probs_k;
  double          max_prob, *best_weights;
  double          log_likelihood, last_log_likelihood, best_log_likelihood;
  int            best_class, best_try;
  int	           pe;
  int            cc;
  int            buf;
  int            numPE;
  int            total_numrows;
  double          *otherminval, *othermaxval;
  double          *tempminval, *tempmaxval;
  double          **other_covar_k, *otherweights;
  double          **temp_covar_k, *tempweights;
  double          **tag_covar_k, *tagweights;


  printf("Here we goo...\n");
  /* Get information about processes and current process */
  DA_get_pe_info(&pe, &numPE);

  /* create storage for the K mean vectors and covariance matrices */

  initmeans = NR_dmatrix(1,K,1,numcols); 

  print_dmatrix(stdout, K, numcols, initmeans);

  best_means = NR_dmatrix(1,K,1,numcols);
  best_covars = set_of_dmatrices(K, numcols, numcols);

  /* create storage for information regarding each class */
  clustsize = NR_ivector(1,K);  /* used only by k-means */
  best_weights = NR_dvector(1,K);

  /* create storage for intermediate computed quantities */
  prob_data = NR_dvector(1,numrows);
  sum_probs = NR_dvector(1,K);

  /* create storage for transposed data */
  transposed_data = NR_dmatrix(1,numcols,1,numrows);

  /* create storage related to message passing */
  other_covar_k = NR_dmatrix(1,numcols,1,numcols);
  tag_covar_k = other_covar_k;
  otherweights  = NR_dvector(1,K);
  tagweights = otherweights;

  /* get the total number of rows of data */
  total_data_pp(numrows, &total_numrows, pe, numPE);

  num_restarts = 0;

  /* this label is here for the case when EM gets a collapsed class;
     we just begin the whole thing from scratch, getting new means
     from k-means */

 restart:  

  /* do one or more k-means runs, taking the outcome with the highest 
     likelihood as the initial starting point for EM */

  best_log_likelihood = -DBL_MAX;

  num_bad_kmeans = 0;
  for (l=1; l<=numtries; l++)
    {
        num_reconverge = 0;
        while (num_reconverge <= max_reconverge)
          {
            if (means_already_initialized == UT_FALSE)
            /* choose random points as starting means for this run */
            /* use of random means appears to have poor performance */
            /*  random_means_pp(initmeans, K, numcols, pe); */
            
              random_means_from_data_pp(initmeans, K, numcols, data, numrows, pe, 
                                        numPE);
            
            else
              // use the means to initialize
              copy_dmat(means, initmeans, K, numcols);
              
            
            /* perform k-means */
            k_means_pp(data, numrows, numcols, means, initmeans, clustsize, 
                       class, K, &numiters, &num_empty_clusters, empty_cluster_ok,
                       pe, numPE);

            /* check for collapsed clusters */
            if (num_empty_clusters > 0)
              {
                if (empty_cluster_ok == UT_FALSE)
                  {
                    num_reconverge++;
                  }
                else
                  {
                    /*K -= num_empty_clusters;*/  /* caller must also do this */
                    if (pe == 0) {
                      printf("K-means returned one or more empty clusters.\n");
                      fflush(stdout);
                    }
                    break;
                    /*
                      return (UT_ERROR);  
                    */
                  }
              }
            else
              break;
          } /* end k-means loop over reconvergences */
      
     
      
      /* if we couldn't converge at all, skip the rest of this loop */
      /*
        if (num_empty_clusters > 0)
        {
        continue;
        }
      */

      /* now we have the initial means to give to EM; compute the other 
         clustering parameters based on the class assignments */

      fast_zero_dvec(weights, K);
      for (k=1; k<=K; k++)
        fast_zero_dmat(covars[k], numcols, numcols);
      
      for (i=1; i<=numrows; i++)
        {
          k = class[i];
        
          /* get the parameters corresponding to the class of this datum */
          covar_k = covars[k];
          mean_k  = means[k];

          /* estimate initial weights given members of k-means clusters */
          /* add the contribution of this datum to its class */
          weights[k]++;
        
          /* estimate initial covariance matrices given members of k-means 
             clusters */
          /* add the square of the deviation vectors to the right position in the
             covariance */
          for (j=1; j<=numcols; j++)
            {
              data_i = data[i];
              for (j2=j; j2<=numcols; j2++)
                covar_k[j][j2] += (data_i[j] - mean_k[j])*(data_i[j2] - mean_k[j2]);
            }
        }

      /* Do parallel communication to collate weights */
      MPI_Allreduce(&weights[1], &otherweights[1], K, MPI_DOUBLE, MPI_SUM, 
                    MPI_COMM_WORLD);
      copy_dvec(otherweights, weights, K);
      
      /*
        DA_barrier();
        if (pe != 0) 
        {
        cc = DA_send_msg(&weights[1],K,0,pe,DA_DOUBLE);
        cc = DA_broadcast_msg(&weights[1],K,DA_ALL_PES,0,DA_DOUBLE);
        }
        else 
        {
        for(p=1;p<numPE;p++)
        {
        cc = DA_recv_msg(&otherweights[1],K,p,p,DA_DOUBLE);
        add_dvec(weights, otherweights, weights, K);
        }
        cc = DA_broadcast_msg(&weights[1],K,DA_ALL_PES,0,DA_DOUBLE);
        }     
      */

      /* normalize the covariance matrices by the membership of each class */
      for (k=1; k<=K; k++)
        scalar_div_dmat(covars[k], numcols, numcols, weights[k]);

      /* normalize weights by the total number of data */
      scalar_div_dvec(weights, K, total_numrows);

      /* normalize the covariance matrices by the total number of data */
      /* FIXME: this is incorrect, because we want the expectation within
         each class, so we need to normalize by the size of the class */
      /*
        for (k=1; k<=K; k++)
        scalar_div_dmat(covars[k], numcols, numcols, (double) total_numrows);
      */

      /* Do parallel communication to collate covariance matrices */
      for (k = 1; k <= K; k++) {
        covar_k = covars[k];
        MPI_Allreduce(&covar_k[1][1], &other_covar_k[1][1], numcols * numcols,
                      MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        copy_dmat(other_covar_k, covar_k, numcols, numcols);
      }
        
      /*
        DA_barrier();
        if (pe != 0) 
        {
        for (k=1; k<K; k++)
        {
        covar_k = covars[k];
        cc = DA_send_msg(&covar_k[1][1],numcols*numcols,0,k,DA_DOUBLE);
        }

        for (k=1; k<K; k++)
        {
        covar_k = covars[k];
        cc = DA_broadcast_msg(&covar_k[1][1],numcols*numcols,DA_ALL_PES,0,
			  DA_DOUBLE);
        DA_barrier();
        }

        }
        else
        {
        for(p=1;p<numPE;p++)
        {
        for (k=1; k<K; k++)
        {
        covar_k = covars[k];
        cc = DA_recv_msg(&other_covar_k[1][1],numcols*numcols,p,k,DA_DOUBLE);
        add_dmat(covar_k, other_covar_k, covar_k, numcols, numcols);
        }
        }

        for (k=1; k<K; k++)
        {
        covar_k = covars[k];
        cc = DA_broadcast_msg(&covar_k[1][1],numcols*numcols,DA_ALL_PES,0,
        DA_DOUBLE);
        DA_barrier();
        }
        }     
      */

      /* fill in the lower triangle of each covariance matrix based on upper 
         triangle */
      for (k=1; k<=K; k++)
        {
          covar_k = covars[k];
          for (j=1; j<=numcols; j++)
            for (j2=1; j2<j; j2++)
              covar_k[j][j2] = covar_k[j2][j];
          /* check to see if the was a numerical error */
          posdef_sym_alloc_dmat(covars[k], numcols, &posdef);
          if (posdef == UT_FALSE)
            break;
        }

      /* if there was a numerical error, then try again without counting
         this attempt as one of the "tries" */
      /* FIXME: there is a possibility of entering into an infinite loop
         here if the data is pathological.  One way to get out of this is
         to have a variable that record the number of numerical errors.
         When this variable exceeds some threshold, simply generate the
         means and covariances randomly (but make sure the covariances
         are positive definite!) */
      if (posdef == UT_FALSE) {
        /* FIXME: this is an arbitrary cutoff */
        if (num_bad_kmeans <= 50) {
          l--;
          num_bad_kmeans++;
          continue;
        }
      }

      /* given the parameters, compute the likelihood of this model */
      if (posdef == UT_TRUE)
        log_likelihood = mixture_likelihood_pp(K, means, covars, weights, 
                                               data, probs, prob_data, 
                                               sum_probs, numrows, numcols, 
                                               "rows", min_diag, pe, numPE);
      else
        log_likelihood = -DBL_MAX;

      /* save the current best set of parameters */
      if ((log_likelihood > best_log_likelihood) || (l == 1))
        {
          best_try = l;
          best_log_likelihood = log_likelihood;
          for (k=1; k<=K; k++)
            {
              copy_dvec(means[k], best_means[k], numcols);
              copy_dmat(covars[k], best_covars[k], numcols, numcols);
            }
          copy_dvec(weights, best_weights, K);
        }
    }  /* end k-means loop over numtries */

  /* create temporary storage */
  newmean = NR_dmatrix(1, 1, 1, numcols);

  /* use the best parameters found by k-means to initialize EM */
  for (k=1; k<=K; k++)
    {
      copy_dvec(best_means[k], means[k], numcols);
      copy_dmat(best_covars[k], covars[k], numcols, numcols);
      posdef_sym_alloc_dmat(covars[k], numcols, &posdef);
      if (posdef == UT_FALSE) {
        printf("k-means produced non-positive-definite covariance matrix.\n");
        printf("generating new (random) mean and covariance matrix.\n");
        random_means_from_data_pp(newmean, 1, numcols, data, numrows, pe, 
                                  numPE);
        for (j = 1; j <= numcols; j++)
          means[k][j] = newmean[1][j];

        if (pe == 0)
          generate_rand_covar_dmat(covars[k], numcols);

        MPI_Bcast(&covars[k][1][1], numcols * numcols, MPI_DOUBLE, 0, 
                  MPI_COMM_WORLD);
      }
    }
  copy_dvec(best_weights, weights, K);

  /* convert data matrix to be indexed by column rather than by rows */
  transpose_dmatrix(data, numrows, numcols, transposed_data);

  /* check covariances to see if any are poorly conditioned; replace those
     with random covariance matrices */
  for (k=1; k<=K; k++)
    {
      cond_alloc_dmat(covars[k], numcols, numcols, &cond);
      if (cond > 1.0e6) {
        printf("k-means produced poorly conditioned covariance matrix\n");
        printf("generating new (random) mean and covariance matrix.\n");
        random_means_from_data_pp(newmean, 1, numcols, data, numrows, pe, 
                                  numPE);
        for (j = 1; j <= numcols; j++)
          means[k][j] = newmean[1][j];

        if (pe == 0)
          generate_rand_covar_dmat(covars[k], numcols);

        MPI_Bcast(&covars[k][1][1], numcols * numcols, MPI_DOUBLE, 0, 
                  MPI_COMM_WORLD);
      }
    }

  /* de-allocate temporary storage */
  NR_free_dmatrix(newmean, 1, 1, 1, numcols);

  /* perform EM iterations */
  printf("Finished seeding; performing EM iterations\n");

  /* set initial conditions for the convergence */
  numiters = 0;
  num_reconverge = 0;
  converged = UT_FALSE;
  log_likelihood = -DBL_MAX;

  while (converged == UT_FALSE)
    {
      /* FIXME: debugging information */
      printf("numiters = %d\n", numiters);
      for (k=1; k<=K; k++)
        {
          posdef_sym_alloc_dmat(covars[k], numcols, &posdef);
          if (posdef == UT_FALSE)
            printf("EM produced non-positive-definite covariance matrix.\n");
          cond_alloc_dmat(covars[k], numcols, numcols, &cond);
          printf("Inside EM: condition number for covariance matrix %d: %g\n", k, cond);
        }

      /* initialize for this iteration */
      last_log_likelihood = log_likelihood;

      /* M-step - "Maximization" */
      /* I. For each class, compute the posterior probability of the class
         given each datum and the class parameters, p(w|d) */
      log_likelihood = mixture_likelihood_pp(K, means, covars, weights, 
                                             transposed_data, 
                                             probs, prob_data, sum_probs, 
                                             numrows, numcols, "cols", 
                                             min_diag, pe, numPE);

      /* check for convergence */
      if ((double) fabs( ((double)(log_likelihood - last_log_likelihood)) / ((double) log_likelihood)) 
          <= threshold)
        {
          converged = UT_TRUE;
          continue;
        }

      /* E-step - "Expectation" */
      /* II. For each class, compute the parameters given the posterior
         probability of each datum */
      estimate_mixture_params_pp(K, means, covars, weights, transposed_data, 
                                 probs, sum_probs, total_numrows, numrows, 
                                 numcols, min_weight, &num_empty_clusters, 
                                 empty_cluster_ok, pe, numPE);

      /* check for collapsed clusters */
      if (num_empty_clusters > 0)
        {
          if (empty_cluster_ok == UT_FALSE)
            {
              num_restarts++;
              if (num_restarts > max_restarts)
                /*
                  return (UT_ERROR);
                */
                break;
              else {
                /* start the whole process over, including k-means runs */
                if (pe == 0) {
                  printf("restarting EM because of empty clusters\n");
                  fflush(stdout);
                }
                goto restart;
              }
            }
          else
            {
              /*K -= num_empty_clusters;*/  /* caller must also do this */
              if (pe == 0) {
                printf("Empty clusters detected in EM, concluding EM.\n");
                fflush(stdout);
              }
              /*
                return (UT_ERROR);  
              */
              break;
            }
        }

      /* keep track of num iterations */
      numiters++;

      /* heuristic to avoid limit cycles */
      /* hopefully a new random seed will solve the problem */
      if (numiters > NR_imax(200, numrows / 1000)) {
        num_restarts++;
        if (num_restarts > max_restarts)
          break;
        else {
          if (pe == 0) {
            printf("limit cycle detected, restarting EM %d\n", numiters);
            fflush(stdout);
          }
          goto restart;
        }
      }

    } /* end of main em loop over iterations */

  /* determine the class assignments for each datum */
  for (i=1; i<=numrows; i++)
    {
      max_prob = probs[1][i];
      best_class = 1;
      for (k=2; k<=K; k++)
        if (probs[k][i] > max_prob)
          {
            max_prob = probs[k][i];
            best_class = k;
          }
      class[i] = best_class;
    }

  /* Clean up allocated memory */

  NR_free_dmatrix(initmeans, 1, K, 1, numcols);
  NR_free_dmatrix(best_means, 1, K, 1, numcols);
  NR_free_ivector(clustsize, 1, K);
  free_set_of_dmatrices(best_covars, K, numcols, numcols);

  NR_free_dmatrix(transposed_data, 1, numcols, 1, numrows);

  NR_free_dvector(prob_data, 1, numrows);
  NR_free_dvector(sum_probs, 1, K);

  NR_free_dmatrix(other_covar_k,1,numcols,1,numcols);
  NR_free_dvector(otherweights,1,K);
  NR_free_dvector(best_weights,1,K);

  return(UT_OK);
}


/*******************************************************************************
MIX_EM_ANNEAL_ALLOC_PP
Parallel version of mix_em_alloc(), as one step in a deterministic annealing
process.

Perform the EM clustering algorithm on a dataset of continuous values,
returning the matrix of k mean vectors, a set of k covariance matrices,
a matrix of class probabilities, a vector of class weights, and a class
labelling for each of the data.

The input matrices and vectors are all assumed to have been allocated
before calling this function.  The data is assumed to be stored in row 
vector form (i.e. each row is a datum), divided among the different 
processes.  The input variable numtries is the number of times k-means is
run to determine the initials means for the EM algorithm.  The input
variables max_reconverge and max_restarts control the maximum number of
times the k-means and EM algorithms respectively try in the event that
there is an empty cluster.

NOTE: In practice, it is a good idea to normalize the columns of the data to
the same scale before performing EM (see the function normalize_data()).
AG, RG
*******************************************************************************/
int mix_em_anneal_alloc_pp(double **data, int numrows, int numcols, 
                           double **means, double ***covars, double **probs, 
                           double *weights, int *class, int K, 
                           double threshold, double min_weight, 
                           double min_diag, int numtries, int max_reconverge, 
                           int max_restarts, boolean empty_cluster_ok)
{
  double          *data_i, *data_j, *data_j2;
  double          **transposed_data, **initmeans;
  double          *minval, *maxval, *range;
  int            i, j, j2, k, l, p;
  int            *clustsize;
  boolean        converged;
  int            numiters, seed;
  int            num_reconverge, num_empty_clusters;
  int            num_restarts;
  double          **best_means, ***best_covars;
  double          *mean_k, **covar_k;
  double          *prob_data, *sum_probs, *probs_k;
  double          max_prob, *best_weights;
  double          log_likelihood, last_log_likelihood, best_log_likelihood;
  double          diff;
  int            best_class, best_try;
  int	           pe;
  int            cc;
  int            buf;
  int            numPE;
  int            total_numrows;
  double          *otherminval, *othermaxval;
  double          *tempminval, *tempmaxval;
  double          **other_covar_k, *otherweights;
  double          **temp_covar_k, *tempweights;
  double          **tag_covar_k, *tagweights;
  double          factor, temperature, start_temp, last_temp_log_likelihood;

  /* Get information about processes and current process */
  DA_get_pe_info(&pe, &numPE);

  /* create storage for the K mean vectors and covariance matrices */
  initmeans = NR_dmatrix(1,K,1,numcols);
  best_means = NR_dmatrix(1,K,1,numcols);
  best_covars = set_of_dmatrices(K, numcols, numcols);

  /* create storage for information regarding each class */
  clustsize = NR_ivector(1,K);  /* used only by k-means */
  best_weights = NR_dvector(1,K);

  /* create storage for intermediate computed quantities */
  prob_data = NR_dvector(1,numrows);
  sum_probs = NR_dvector(1,K);

  /* create storage for transposed data */
  transposed_data = NR_dmatrix(1,numcols,1,numrows);

  /* create storage related to message passing */
  other_covar_k = NR_dmatrix(1,numcols,1,numcols);
  tag_covar_k = other_covar_k;
  otherweights  = NR_dvector(1,K);
  tagweights = otherweights;

  /* get the total number of rows of data */
  total_data_pp(numrows, &total_numrows, pe, numPE);

  num_restarts = 0;

  start_temp = 0.001;

  /* this label is here for the case when EM gets a collapsed class;
     we just begin the whole thing from scratch, getting new means
     from k-means */

 restart:  

  /* do one or more k-means runs, taking the outcome with the highest 
     likelihood as the initial starting point for EM */

  best_log_likelihood = -DBL_MAX;
  for (l=1; l<=numtries; l++)
    {
      num_reconverge = 0;
      while (num_reconverge <= max_reconverge)
        {
          /* choose random points as starting means for this run */
          /*
            random_means_pp(initmeans, K, numcols, pe);
          */
          random_means_from_data_pp(initmeans, K, numcols, data, numrows, pe, 
                                    numPE);

          /* perform k-means */
          k_means_pp(data, numrows, numcols, means, initmeans, clustsize, 
                     class, K, &numiters, &num_empty_clusters, empty_cluster_ok,
                     pe, numPE);
      
          /* check for collapsed clusters */
          if (num_empty_clusters > 0)
            {
              if (empty_cluster_ok == UT_FALSE)
                {
                  num_reconverge++;
                }
              else
                {
                  /*K -= num_empty_clusters;*/  /* caller must also do this */
                  if (pe == 0) {
                    printf("K-means returned one or more empty clusters.\n");
                    fflush(stdout);
                  }
                  break;
                  /*
                    return (UT_ERROR);  
                  */
                }
            }
          else
            break;
        } /* end k-means loop over reconvergences */

      /* now we have the initial means to give to EM; compute the other 
         clustering parameters based on the class assignments */

      fast_zero_dvec(weights, K);
      for (k=1; k<=K; k++)
        fast_zero_dmat(covars[k], numcols, numcols);
      
      for (i=1; i<=numrows; i++)
        {
          k = class[i];
        
          /* get the parameters corresponding to the class of this datum */
          covar_k = covars[k];
          mean_k  = means[k];

          /* estimate initial weights given members of k-means clusters */
          /* add the contribution of this datum to its class */
          weights[k]++;
        
          /* estimate initial covariance matrices given members of k-means 
             clusters */
          /* add the square of the deviation vectors to the right position in the
             covariance */
          for (j=1; j<=numcols; j++)
            {
              data_i = data[i];
              for (j2=j; j2<=numcols; j2++)
                covar_k[j][j2] += (data_i[j] - mean_k[j])*(data_i[j2] - mean_k[j2]);
            }
        }

      /* Do parallel communication to collate weights */
      MPI_Allreduce(&weights[1], &otherweights[1], K, MPI_DOUBLE, MPI_SUM, 
                    MPI_COMM_WORLD);
      copy_dvec(otherweights, weights, K);
      
      /* normalize weights by the total number of data */
      scalar_div_dvec(weights, K, total_numrows);

      /* normalize the covariance matrices by the total number of data */
      for (k=1; k<=K; k++)
        scalar_div_dmat(covars[k], numcols, numcols, (double) total_numrows);

      /* Do parallel communication to collate covariance matrices */
      for (k = 1; k <= K; k++) {
        covar_k = covars[k];
        MPI_Allreduce(&covar_k[1][1], &other_covar_k[1][1], numcols * numcols,
                      MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        copy_dmat(other_covar_k, covar_k, numcols, numcols);
      }
        
      /* fill in the lower triangle of each covariance matrix based on upper 
         triangle */
      for (k=1; k<=K; k++)
        {
          covar_k = covars[k];
          for (j=1; j<=numcols; j++)
            for (j2=1; j2<j; j2++)
              covar_k[j][j2] = covar_k[j2][j];
        }

      /* given the parameters, compute the likelihood of this model */
      log_likelihood = mixture_likelihood_pp(K, means, covars, weights, 
                                             data, probs, prob_data, 
                                             sum_probs, numrows, numcols, 
                                             "rows", min_diag, pe, numPE);

      /* save the current best set of parameters */
      if ((log_likelihood > best_log_likelihood) || (l == 1))
        {
          best_try = l;
          best_log_likelihood = log_likelihood;
          for (k=1; k<=K; k++)
            {
              copy_dvec(means[k], best_means[k], numcols);
              copy_dmat(covars[k], best_covars[k], numcols, numcols);
            }
          copy_dvec(weights, best_weights, K);
        }
    }  /* end k-means loop over numtries */

  /* use the best parameters found by k-means to initialize EM */
  for (k=1; k<=K; k++)
    {
      copy_dvec(best_means[k], means[k], numcols);
      copy_dmat(best_covars[k], covars[k], numcols, numcols);
    }
  copy_dvec(best_weights, weights, K);

  /* convert data matrix to be indexed by column rather than by rows */
  transpose_dmatrix(data, numrows, numcols, transposed_data);

  /* perform EM iterations with deterministic annealing */

  /* heuristic for determining the temperature parameter */

  /* FIXME: the step size here is arbitrary (but smaller is better?) */
  /* FIXME: the current maximum temperature parameter here is arbitrary */
  for (last_temp_log_likelihood = -DBL_MAX, temperature = start_temp; 
       temperature <= 10.0; temperature += 0.05) {

    /* check to see if annealing method is collapsing */
    /* FIXME: this is a poor way to determine whether the annealing
       is collapsing. */

    if (last_temp_log_likelihood > log_likelihood) {
      /* FIXME: the step size here is arbitrary (but smaller is better?) */
      start_temp += 0.05;
      /* FIXME: need elegant way to avoid this goto */
      goto restart;
    }
    else
      last_temp_log_likelihood = log_likelihood;

    /* set initial conditions for the convergence */
    numiters = 0;
    num_reconverge = 0;
    converged = UT_FALSE;
    log_likelihood = -DBL_MAX;

    while (converged == UT_FALSE)
      {
        /* initialize for this iteration */
        last_log_likelihood = log_likelihood;

        /* M-step - "Maximization" */
        /* I. For each class, compute the posterior probability of the class
           given each datum and the class parameters, p(w|d) */
        log_likelihood = mixture_likelihood_anneal_pp(K, means, covars, 
                                                      weights, transposed_data, 
                                                      probs, prob_data, 
                                                      sum_probs, numrows, 
                                                      numcols, "cols", 
                                                      min_diag, temperature, 
                                                      pe, numPE);

        /* check for convergence */
        diff = fabs((log_likelihood - last_log_likelihood) / log_likelihood);
        if (diff <= threshold)
          {
            converged = UT_TRUE;
            continue;
          }

        /* E-step - "Expectation" */
        /* II. For each class, compute the parameters given the posterior
           probability of each datum */
        estimate_mixture_params_pp(K, means, covars, weights, transposed_data, 
                                   probs, sum_probs, total_numrows, numrows, 
                                   numcols, min_weight, &num_empty_clusters, 
                                   empty_cluster_ok, pe, numPE);

        /* keep track of num iterations */
        numiters++;
      } /* end of main em loop over iterations */
  } /* end main deterministic annealing em loop over temperatures */

  /* determine the class assignments for each datum */
  for (i=1; i<=numrows; i++)
    {
      max_prob = probs[1][i];
      best_class = 1;
      for (k=2; k<=K; k++)
        if (probs[k][i] > max_prob)
          {
            max_prob = probs[k][i];
            best_class = k;
          }
      class[i] = best_class;
    }

  /* Clean up allocated memory */

  NR_free_dmatrix(initmeans, 1, K, 1, numcols);
  NR_free_dmatrix(best_means, 1, K, 1, numcols);
  NR_free_ivector(clustsize, 1, K);
  free_set_of_dmatrices(best_covars, K, numcols, numcols);

  NR_free_dmatrix(transposed_data, 1, numcols, 1, numrows);

  NR_free_dvector(prob_data, 1, numrows);
  NR_free_dvector(sum_probs, 1, K);

  NR_free_dmatrix(other_covar_k,1,numcols,1,numcols);
  NR_free_dvector(otherweights,1,K);
  NR_free_dvector(best_weights,1,K);

  return(UT_OK);
}


/*******************************************************************************
CROSSVAL_MIX_EM_ALLOC_PP
Determine optimal number of clusters through monte carlo cross validation.
The maximum number of clusters, K_max, and the number of trials to conduct, 
numtrials, are additional input arguments.  The optimal number of clusters 
is returned in k_max.

Reference: P Smyth.  "Model Selection for Probabilistic Clustering Using
Cross-Validated Likelihood."  UCI_ICS Tech Report, No 98-09, September 1998.
RG
*******************************************************************************/
int crossval_mix_em_alloc_pp(double **data, int numrows, int numcols,
                             double **means, double ***covars, double **probs,
                             double *weights, int *class, int K_max,
                             int numtrials, int *k_best, double threshold,
                             double min_weight, double min_diag, int numtries,
                             int max_reconverge, int max_restarts)
{
  int      i, m, k;
  int      k_start;
  int      pe, numPE;
  int      train_index;
  int      test_index;
  int      total_numrows;
  int      startrow;
  double   best_likelihood;
  double   *likelihoods;
  double   best_likelihood_dd;
  double   *likelihoods_dd;
  double   best_so_far;
  double   **train_data;
  double   **test_data;
  double   *prob_data;
  double   *sum_probs;
  boolean  *selected;

  /* get parallel information */
  DA_get_pe_info(&pe, &numPE);

  /* get the total number of rows of data */
  total_data_pp(numrows, &total_numrows, pe, numPE);

  /* allocate temporary storage */
  likelihoods = NR_dvector(1, K_max);
  likelihoods_dd = NR_dvector(1, K_max);
  train_data = NR_dmatrix(1, numrows, 1, numcols);
  test_data = NR_dmatrix(1, numrows, 1, numcols);
  selected = (boolean *) NR_cvector(1, total_numrows);
  prob_data = NR_dvector(1, numrows);
  sum_probs = NR_dvector(1, K_max);

  /* initialize */
  for (k = 1; k <= K_max; k++)
    likelihoods[k] = -DBL_MAX;

  /* For each k to be tested, partition the local data.  Train on one half
     of the data, then test on the other half.  Record the likelihoods
     from a single trial, to get a rough estimate of the optimal number of
     clusters. THIS IS A QUICK HACK */
  if (numtrials > 0) {
    for (k = 1; k <= K_max; k++) {
      if (pe != 0)
        DA_broadcast_msg(&selected[1], total_numrows, DA_ALL_PES, 0, DA_UCHAR);
      else {
        random_mark_vector(total_numrows / 2, (total_numrows >> 1) << 1,
                           selected);
        DA_broadcast_msg(&selected[1], total_numrows, DA_ALL_PES, 0, DA_UCHAR);
      }
      split_data_pp(&startrow, &numrows, total_numrows, pe, numPE);
      test_index = 0;
      train_index = 0;
      for (i = startrow;
           i <= NR_imin((total_numrows >> 1) << 1, startrow + numrows - 1);
           i++)
        if (selected[i] == UT_TRUE) {
          train_index++;
          grab_row_dmat(data, i - startrow + 1, numcols,
                        train_data[train_index]);
        }
        else {
          test_index++;
          grab_row_dmat(data, i - startrow + 1, numcols, 
                        test_data[test_index]);
        }
      mix_em_alloc_pp(train_data, train_index, numcols, means, covars, 
                      probs, weights, class, k, threshold, min_weight, 
                      min_diag, numtries, max_reconverge, max_restarts,
                      UT_TRUE, UT_FALSE);
      likelihoods[k] = mixture_likelihood_pp(k, means, covars, weights,
                                             test_data, probs, prob_data,
                                             sum_probs, test_index,
                                             numcols, "rows", min_diag, pe,
                                             numPE);
      if (pe == 0) {
        printf("Fast estimate completed for k = %d\n", k);
        fflush(stdout); 
      }
    }
  }

  /* Determine the estimate of the best choice for k by finding the maximum 
     likelihood from the fast trials */
  *k_best = 1;
  best_likelihood = likelihoods[1];

  for (k = 2; k <= K_max; k++)
    if (likelihoods[k] > best_likelihood) {
      best_likelihood = likelihoods[k];
      *k_best = k;
    }

  k_start = 1;

  /* Use a new starting k near the estimated peak */
  /* it's unclear whether this really helps */
  /*
    for (k = 2; k <= *k_best; k++)
    if (fabs((best_likelihood - likelihoods[k]) / best_likelihood) < 0.1) {
    k_start = k;
    break;
    }
  */

  /* For each k to be tested, partition the local data.  Train on one half
     of the data, then test on the other half.  Record the summed likelihoods
     over a number of trials.  NOTE: this is flawed because data is not
     partitioned randomly over all the data, but only over the locally 
     stored data. */
  for (k = k_start; k <= K_max; k++) {
    for (m = 1; m < numtrials; m++) {
      if (pe != 0)
        DA_broadcast_msg(&selected[1], total_numrows, DA_ALL_PES, 0, DA_UCHAR);
      else {
        random_mark_vector(total_numrows / 2, (total_numrows >> 1) << 1, 
                           selected);
        DA_broadcast_msg(&selected[1], total_numrows, DA_ALL_PES, 0, DA_UCHAR);
      }
      split_data_pp(&startrow, &numrows, total_numrows, pe, numPE);
      test_index = 0;
      train_index = 0;
      for (i = startrow; 
           i <= NR_imin((total_numrows >> 1) << 1, startrow + numrows - 1); 
           i++) 
        if (selected[i] == UT_TRUE) {
          train_index++;
          grab_row_dmat(data, i - startrow + 1, numcols, 
                        train_data[train_index]);
        }
        else {
          test_index++;
          grab_row_dmat(data, i - startrow + 1, numcols, 
                        test_data[test_index]);
        }
      mix_em_alloc_pp(train_data, train_index, numcols, means, covars, 
                      probs, weights, class, k, threshold, min_weight, 
                      min_diag, numtries, max_reconverge, max_restarts, 
                      UT_TRUE, UT_FALSE);
      likelihoods[k] += mixture_likelihood_pp(k, means, covars, weights,
                                              test_data, probs, prob_data,
                                              sum_probs, test_index, 
                                              numcols, "rows", min_diag, pe, 
                                              numPE);
      if (pe == 0) {
        printf("trial %d completed for k = %d\n", m+1, k);
        fflush(stdout);
      }
    }
    if (numtrials > 0) {
      likelihoods[k] /= (double) numtrials;
      if (pe == 0) {
        printf("Completed cross-validation test for k = %d\n", k);
        printf("likelihoods[%d] = %f\n", k, likelihoods[k]);
        fflush(stdout);
      }
      /* heuristic to end search for early; assumes single peak */
      if (k >= 4)
        if ((likelihoods[k] < likelihoods[k-1]) && 
            (likelihoods[k-1] < likelihoods[k-2]) && 
            (likelihoods[k-2] < likelihoods[k-3])) {
          K_max = k;
          break;
        }
    }
  }

  /* determine the best choice for k by finding the maximum summed likelihood */
  *k_best = 1;
  best_likelihood = likelihoods[1];

  for (k = 2; k <= K_max; k++)
    if (likelihoods[k] > best_likelihood) {
      best_likelihood = likelihoods[k];
      *k_best = k;
    }

  /* calculate the 2nd derivative of the likelihoods */
  for (k = 2; k < K_max; k++)
    likelihoods_dd[k] = 2.0 * likelihoods[k] - likelihoods[k-1] - 
      likelihoods[k+1];
  
  /* unless there is a reason to think that k = 1 or k = K_max, look for
     the last peak in the second derivative (i.e., the one associated with
     the greatest k */
  if ((*k_best != 1) && ((*k_best != K_max) || 
                         ((likelihoods[K_max] - likelihoods[K_max - 1]) / 
                          likelihoods[K_max] < 0.01))) {
    best_so_far = likelihoods[1];
    for (k = k_start; k < K_max; k++) {
      if ((likelihoods_dd[k] > 0.1 * best_likelihood) && 
          (likelihoods[k] > best_so_far))
        *k_best = k;
      if (likelihoods[k] > best_so_far)
        best_so_far = likelihoods[k];
    }
  }

  /* if there is a plateau, the best conservative choice of k corresponds 
     to the leading edge */
  /*
    for (k = 2; k <= *k_best; k++)
    if (fabs((best_likelihood - likelihoods[k]) / best_likelihood) < 0.01) {
    *k_best = k;
    best_likelihood = likelihoods[k];
    break;
    }
  */
  
  if (numtrials == 0)
    *k_best = K_max;

  if ((pe == 0) && (numtrials > 0))
    for (k = 1; k <= K_max; k++)
      printf("Avg log likelihood for k = %d: %g\n", k, 
             likelihoods[k]);

  if (pe == 0) {
    printf("Performing full EM clustering using k = %d\n", *k_best);
    fflush(stdout);
  }

  /* perform EM clustering on all the data using the optimum k */
  /* This is now handled outside of this function call */
  /*
    mix_em_alloc_pp(data, numrows, numcols, means, covars, probs, weights,
    class, *k_best, threshold, min_weight, min_diag, numtries,
    max_reconverge, max_restarts, UT_FALSE);
  */

  /* write out the likelihood curve for examination */
  if ((pe == 0) && (numtrials > 0)) {
    write_dcol("temp_likelihood_curve.dat", K_max, likelihoods, "w");
  }

  /* clean up temporarily allocated memory */
  NR_free_dvector(likelihoods, 1, K_max);
  NR_free_dvector(likelihoods_dd, 1, K_max);
  NR_free_dmatrix(train_data, 1, numrows / 2, 1, numcols);
  NR_free_dmatrix(test_data, 1, numrows / 2, 1, numcols);
  NR_free_cvector(selected, 1, numrows);
  NR_free_dvector(prob_data, 1, numrows);
  NR_free_dvector(sum_probs, 1, K_max);

  return (UT_OK);
}


/*******************************************************************************
HIERARCHY_MIX_EM_ALLOC_PP
Merge clusters as determined via EM clustering using mix_em_alloc_pp().  
Returned is a matrix describing the hierarchy of merges.

Reference: C Fraley.  "Algorithms For Model-Based Gaussian Hierarchical 
Clustering."  SIAM J. Sci. Comput., Vol 20, No 1, pp 270-281, August 1998.

***UNTESTED***

RG
*******************************************************************************/
int hierarchy_mix_em_alloc_pp(double **data, int numrows, int numcols,
                              double **means, double ***Ws, int *class, 
                              int **hierarchy, int K)
{
  int      i, j, j2, k, k2, k_max, k2_max;
  int      p, cc;
  int      pe, numPE;
  int      *num_elements;
  double   **W_k, **W_k2, *mean_k, *mean_k2, *data_j, *data_j2;
  double   **other_W_k;
  double   eta, delta_max;
  int      level;
  int      *other_num_elements;
  double   **delta;
  double   **W_merge;
  
  /* some allocation */
  num_elements = NR_ivector(1, K);
  other_num_elements = NR_ivector(1, K);
  other_W_k = NR_dmatrix(1, numcols, 1, numcols);
  W_merge = NR_dmatrix(1, numcols, 1, numcols);
  
  
  /* calculate the local number of elements in each class */
  fast_zero_ivec(num_elements, K);

  for (i = 1; i <= numrows; i++)
    num_elements[class[i]]++;
  
  /* calculate the local sample cross-product matrices, the Ws */
  for (k=1; k<=K; k++)
    {
      W_k = Ws[k];
      mean_k  = means[k];

      for (j=1; j<=numcols; j++)
        {
          data_j = data[j];
          for (j2=j; j2<=numcols; j2++)
            {
              data_j2 = data[j2];
        
              W_k[j][j2] = 0.0;
              for (i=1; i<=numrows; i++)
                W_k[j][j2] += (data_j[i] - mean_k[j]) * (data_j2[i] - mean_k[j2]);
            }
        }

      /* fill in the lower triangle of the matrix based on upper triangle */
      for (j=1; j<=numcols; j++)
        for (j2=1; j2<j; j2++)
          W_k[j][j2] = W_k[j2][j];
    }
  
  /* Do parallel communication to collate number of class elements */
  DA_barrier();
  if (pe != 0) /* This is not the controlling process */
    {
      /* Send out the locally computed weights */
      cc = DA_send_msg(&num_elements[1],K,0,pe,DA_INT);
    
      /* Now block until receive broadcast message from the controlling
         process giving the global sums of posteriors */
      cc = DA_broadcast_msg(&num_elements[1],K,DA_ALL_PES,0,DA_INT);
    }
  else /* This is the controlling process */
    {
      /* Receive the other numbers of elements, do collation */
      for(p=1;p<numPE;p++)
        {
          cc = DA_recv_msg(&other_num_elements[1],K,p,p,DA_INT);
          for (k = 1; k <= K; i++)
            num_elements[k] += other_num_elements[k];
        }

      /* Now broadcast the global sums of posteriors */
      cc = DA_broadcast_msg(&num_elements[1],K,DA_ALL_PES,0,DA_INT);
    }

  /* Do parallel communication to collate sample cross product matrices */
  DA_barrier();
  if (pe != 0) /* This is not the controlling process */
    {
      /* Send out the locally computed sample cross product matrices */
      for (k=1; k<=K; k++)
        {
          W_k = Ws[k];
          cc = DA_send_msg(&W_k[1][1],numcols*numcols,0,k,DA_DOUBLE);
        }

      /* Now block until receive broadcast message from the controlling
         process giving the global sample cross product matrices */
      for (k=1; k<=K; k++)
        {
          W_k = Ws[k];
          cc = DA_broadcast_msg(&W_k[1][1],numcols*numcols,DA_ALL_PES,0,
                                DA_DOUBLE);
          DA_barrier();
        }
    }
  else /* This is the controlling process */
    {
      /* Receive the other sets of sample cross product matrices, do collation */
      for(p=1;p<numPE;p++)
        {
          for (k=1; k<=K; k++)
            {
              W_k = Ws[k];
              cc = DA_recv_msg(&other_W_k[1][1],numcols*numcols,p,k,DA_DOUBLE);
              add_dmat(W_k, other_W_k, W_k, numcols, numcols);
            }
        }

      if (debug_output() && (pe == 0))
        log_printf("%d: after receiving local sample cross product matrices\n",pe);

      /* Now broadcast the global sample cross product matrices */
      for (k=1; k<=K; k++)
        {
          W_k = Ws[k];
          cc = DA_broadcast_msg(&W_k[1][1],numcols*numcols,DA_ALL_PES,0,
                                DA_DOUBLE);
          DA_barrier();
        }

      if (debug_output() && (pe == 0))
        log_printf("%d: after broadcasting global sample cross product matrices\n",pe);
    }     

  if (debug_output() && (pe == 0))
    log_printf("%d: after parallel communication to collate global sample cross product matrices\n",pe);

  /* calculate initial deltas in objective function */ 
  for (k = 1; k <= K; k++)
    for (k2 = k + 1; k2 <= K; k2++) {
      mean_k = means[k];
      mean_k2 = means[k2];
      W_k = Ws[k];
      W_k2 = Ws[k2];

      /* calculate the merged sample cross product matrix */
      eta = ((double) (num_elements[k] * num_elements[k2])) / 
        ((double) (num_elements[k] + num_elements[k2]));

      for (j = 1; j <= numcols; j++)
        for (j2 = j; j <= numcols; j2++)
          W_merge[j][j2] = W_k[j][j2] + W_k2[j][j2] + eta * 
            (mean_k[j] - mean_k2[j]) * 
            (mean_k[j2] - mean_k2[j2]);

      /* fill in the lower triangle of the matrix based on the upper triangle */
      for (j = 1; j <= numcols; j++)
        for (j2 = 1; j2 < j; j2++)
          W_merge[j][j2] = W_merge[j2][j];
      
      /* calculate this objective function delta */
      delta[k][k2] += ((double) num_elements[k]) * 
        log(det_copy_alloc_dmat(W_k, numcols) / 
            NR_dsqr((double) num_elements[k]) + DBL_MIN);
      delta[k][k2] += ((double) num_elements[k2]) * 
        log(det_copy_alloc_dmat(W_k2, numcols) / 
            NR_dsqr((double) num_elements[k2]) + DBL_MIN);
      delta[k][k2] -= ((double) (num_elements[k] + num_elements[k2])) * 
        log(det_copy_alloc_dmat(W_merge, numcols) / 
            NR_dsqr((double)(num_elements[k] + num_elements[k2])) 
            + DBL_MIN);
    }
  
  /* now create the hierarchy */
  for (level = 1; level < K; level++) {
    delta_max = 0.0;
  
    /* locate the maximum delta */
    for (k = 1; k <= K; k++)
      for (k2 = k + 1; k2 <= K; k++)
        if ((hierarchy[level][k] == k) && (hierarchy[level][k2] == k2)) 
          if (delta[k][k2] > delta_max) {
            delta_max = delta[k][k2];
            k_max = k;
            k2_max = k2;
          }
    
    /* calculate the merged sample cross product matrix for that delta */
    mean_k = means[k_max];
    mean_k2 = means[k2_max];
    W_k = Ws[k_max];
    W_k2 = Ws[k2_max];

    eta = ((double) (num_elements[k] * num_elements[k2])) / 
      ((double) (num_elements[k] + num_elements[k2]));

    for (j = 1; j <= numcols; j++)
      for (j2 = j; j <= numcols; j2++)
        W_merge[j][j2] = W_k[j][j2] + W_k2[j][j2] + eta * 
          (mean_k[j] - mean_k2[j]) * 
          (mean_k[j2] - mean_k2[j2]);

    /* fill in the lower triangle of the matrix based on the upper triangle */
    for (j = 1; j <= numcols; j++)
      for (j2 = 1; j2 < j; j2++)
        W_merge[j][j2] = W_merge[j2][j];
    
    /* update the number of elements in the merged group */
    num_elements[k] += num_elements[k2];

    /* update the Ws */
    copy_dmat(W_merge, Ws[k_max], numcols, numcols);
    
    /* update the deltas */
    for (k = 1; k < k_max; k++) {
      W_k = Ws[k];
      W_k2 = Ws[k_max];
      delta[k][k_max] += ((double) num_elements[k]) * 
        log(det_copy_alloc_dmat(W_k, numcols) / 
            NR_dsqr((double) num_elements[k]) + DBL_MIN);
      delta[k][k_max] += ((double) num_elements[k2]) * 
        log(det_copy_alloc_dmat(W_k2, numcols) / 
            NR_dsqr((double) num_elements[k2]) + DBL_MIN);
      delta[k][k_max] -= ((double) (num_elements[k] + num_elements[k2])) * 
        log(det_copy_alloc_dmat(W_merge, numcols) / 
            NR_dsqr((double)(num_elements[k] + num_elements[k2])) 
            + DBL_MIN);
    }

    for (k = k_max + 1; k <= K; k++)
      if ((hierarchy[level][k] != k_max) && (hierarchy[level][k] != k2_max)) {
        W_k = Ws[k_max];
        W_k2 = Ws[k];
        delta[k][k_max] += ((double) num_elements[k]) * 
          log(det_copy_alloc_dmat(W_k, numcols) / 
              NR_dsqr((double) num_elements[k]) + DBL_MIN);
        delta[k][k_max] += ((double) num_elements[k2]) * 
          log(det_copy_alloc_dmat(W_k2, numcols) / 
              NR_dsqr((double) num_elements[k2]) + DBL_MIN);
        delta[k][k_max] -= ((double) (num_elements[k] + num_elements[k2])) * 
          log(det_copy_alloc_dmat(W_merge, numcols) / 
              NR_dsqr((double)(num_elements[k] + 
                               num_elements[k2])) 
              + DBL_MIN);
      }
    
    /* record the merge in the hierarchy table */
    for (k = 1; k <= K; k++)
      if (hierarchy[level][k] == k2_max)
        hierarchy[level + 1][k] = k_max;
      else
        hierarchy[level + 1][k] = hierarchy[level][k];
  }
  
  /* clean up memory */

  return(UT_OK);
}


/*******************************************************************************
MIN_DIST_CLASS_ASSIGN_PP
Assign classes to data according to which mean is closest.  This is for use
as a crude "0 iteration" version of k-means.  If the means are randomly chosen
from the data, then this guarantees that each cluster will have at least one
data that belongs to it.
*******************************************************************************/
int min_dist_class_assign_pp(double **data, int numrows, int numcols, 
                             double **means, int *clustsize, int *class, 
                             int K, int pe, int numPE)
{
  int            *otherclustsize;
  int            i, j, k, p, cc;
  int            bestclass;
  double          dist, mindist;
  int            stable, otherstable;
  int            empty_clusters, iters;
    
  /* make storage related to message passing */
  otherclustsize = NR_ivector(1,K);

  /* compute distance of each datum to each mean */
  for (i=1; i<=numrows; i++)
    {
      mindist = euclid_dist_dvec(data[i],means[1],numcols);
      bestclass = 1;

      for (k=2; k<=K; k++)
        {
          if ((dist = euclid_dist_dvec(data[i],means[k],numcols)) < mindist)
            {
              mindist = dist;
              bestclass = k;
            }
        }

      /* assign class and see if different from last iteration */
      if (class[i] != bestclass)
        class[i] = bestclass;

      /* contribute to the cluster sizes based on class assignment */
      clustsize[class[i]] += 1;
    }

  /* Do parallel communication to collate cluster sizes */
  MPI_Allreduce(&clustsize[1], &otherclustsize[1], K, MPI_INT, MPI_SUM,
                MPI_COMM_WORLD);
  copy_ivec(otherclustsize, clustsize, K);
    
  /* free temporary storage */
  NR_free_ivector(otherclustsize,1,K);

  return(UT_OK);
}

/*******************************************************************************
COND_ALLOC_DMAT
Calculate the condition number of a matrix using the SVD.
FIXME: the svd routine only seems to work for single precision!
RG
*******************************************************************************/
int cond_alloc_dmat(double **A, int nr, int nc, double *cond)
{
  float  **U, **V, *w;
  int    maxiter, i, j;
  
  /* allocate storage */
  U = NR_matrix(1, nr, 1, nc);
  V = NR_matrix(1, nc, 1, nc);
  w = NR_vector(1, nc);
  
  fast_zero_mat(U, nr, nc);
  fast_zero_mat(V, nc, nc);
  fast_zero_vec(w, nc);
  
  /* copy the input matrix */
  /*
    copy_dmat(A, U, nr, nc);
  */
  for (i = 1; i <= nc; i++)
    for (j = 1; j <= nr; j++)
      U[i][j] = (float) A[i][j];
  
  /* FIXME: debugging */
  /*
    write_matrix("/var/tmp/covar.debug", nr, nc, U, "w");
  */

  /* heuristic to set the number of svd iterations */
  maxiter = NR_imax(30, (int) (nr * nc / 10000));

  /* compute the svd */
  DA_svdcmp(U, nr, nc, w, V, maxiter);
  
  /* compute the condition number */
  *cond = (double) w[1] / min_vec(w, nc);
  
  /* free the allocated memory */
  NR_free_matrix(U, 1, nr, 1, nc);
  NR_free_matrix(V, 1, nc, 1, nc);
  NR_free_vector(w, 1, nc);

  return(UT_OK);
}


/*******************************************************************************
GENERATE_RAND_COVAR_DMAT
Generate a random covariance matrix in double precision.
RG
*******************************************************************************/
int generate_rand_covar_dmat(double **covar, int num_dims)
{
  int     i, j, k;
  int     posdef;
  int     sing;
  double  sum, tau;
  double  *c, *d, *w;
  double  **a, **qt;
  
  /* FIXME: should re-use some of this memory to reduce memory usage */
  c = NR_dvector(1, num_dims);
  d = NR_dvector(1, num_dims);
  w = NR_dvector(1, num_dims);
  qt = NR_dmatrix(1, num_dims, 1, num_dims);
  
  /* rename so that it doesn't become confusing */
  a = covar;

  while (1) {
    /* this gives us positive values between zero and one */
    random_dmatrix(a, num_dims, num_dims);

    /* perform a qr decomposition */
    NR_dqrdcmp(a, num_dims, c, d, &sing);

    /* check for singular matrix */
    if (sing)
      continue;

    /* initialize qt as the identity matrix */
    fast_zero_dmat(qt, num_dims, num_dims);
    for (i = 1; i <= num_dims; i++)
      qt[i][i] = 1.0;

    /* construct the q^T matrix FIXME: inefficient; many mulitplies by zero */
    for (k = 1; k <= num_dims; k++)
      for (j = 1; j < num_dims; j++) {
        for (sum = 0.0, i = j; i <= num_dims; i++)
          sum += a[i][j] * qt[i][k];
        tau = sum / c[j];
        for (i = j; i <= num_dims; i++)
          qt[i][k] -= tau * a[i][j]; 
      }
			
    /* generate a random vector of variances */
    random_dvector(w, num_dims);

    /* zero out the covariance matrix */
    fast_zero_dmat(covar, num_dims, num_dims);

    /* perform the q * diag(w) * q^T multiplication */
    for (i = 1; i <= num_dims; i++)
      for (j = 1; j <= num_dims; j++)
        for (k = 1; k <= num_dims; k++)
          covar[i][j] += qt[k][i] * qt[k][j] * w[k];

    /* check if the matrix is positive definite */
    posdef_sym_alloc_dmat(covar, num_dims, &posdef);
    
    if (posdef == UT_FALSE)
      continue; /* try another random matrix */
    else
      break; /* this matrix is okay */
  }
  
  NR_free_dvector(c, 1, num_dims);
  NR_free_dvector(d, 1, num_dims);
  NR_free_dvector(w, 1, num_dims);
  NR_free_dmatrix(qt, 1, num_dims, 1, num_dims);
  
  return(UT_OK);
}
