/*******************************************************************************

  Title:     Parallel EM
  Author:    Alexander Gray, Robert Granat
  Function:  Perform the EM algorithm, in the context of mixture modelling, on 
             a dataset, returning as output a file containing the model para-
             meters found by the algorithm.  The number of components and
             initial random seed value are specified by the user.
  Reference: Duda and Hart, ch. 6 (p. 200)
  How used:  For implementation on a parallel computer.
  Compile:   make
  Example:   sc -i test.data -rows 500 -cols 10 -p test.params -c test.labels
                 -k 4 -seed 13
  Notes:     Do not attempt to run this program in other parallel environments
             without revision.

             - performance - to allow passing of all K covar. matrices in one 
                 big message, maybe use submatrix() nr function
             - performance - only send the upper triangle of a covar. matrix

             - clean source - separate out things into parallel functions
                 - normalize_data_pp()
                 - divide_data()
                 - write_icol_cascade()

*******************************************************************************/
#ifndef lint
static char rcsid[] = "$Id: main_mix_em_pp.c,v 1.2 2001/12/05 00:47:59 hart Exp $";
#endif
/* 
 * $Log: main_mix_em_pp.c,v $
 * Revision 1.2  2001/12/05 00:47:59  hart
 * Modifications to allow users to specify the exact locations to initialize
 * cluster means.  This is in effort to build in an ability to chain FullEM as
 * post clustering clusteirng algo.
 *
 * Revision 1.1.1.1  2001/05/21 23:36:19  diane
 * Import of various mls & woldlab source packages
 *
 * Revision 1.2  1996/07/19 18:02:34  agray
 * functionalized lots of code sections, added number of channels as argument
 *
 * Revision 1.1  1996/07/11 18:29:10  agray
 * Initial revision
 *
 * */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>

#include "cp.h"

#include "ut.h"

#include "nr.h"
#include "nr_util.h"

#include "da_platform.h"
#include "da_io.h"
#include "da_io_pp.h"
#include "da_cluster.h"
#include "da_cluster_pp.h"
#include "da_util.h"
#include "da_linalg.h"
#include "da_signal_pp.h"
#include "da_comm_pp.h"
#include "da_random.h"

#include "main_mix_em_pp.h"

/*******************************************************************************
 MAIN
 Driver of program.
*******************************************************************************/
main(int argc, char *argv[])
{
  arg            args[EM_NUM_ARGS+1];
  char           *datafile, *paramsfile, *labelsfile, *logfile, *inputmeansfile;
  float          **data, *data_i, *data_j, *data_j2;
  float          **initdata, **initmeans;
  float          *minval, *maxval, *range;
  int            *clustsize, *class;
  int            i, j, j2, k, K, l, p, numrows, numcols;
  boolean        converged, empty_cluster_ok;
  int            numiters, seed, numtries;
  int            num_reconverge, max_reconverge, num_empty_clusters;
  int            num_restarts, max_restarts;
  float          min_diag, threshold, min_weight;
  float          **means, ***covars, **best_means, ***best_covars;
  float          *mean_k, **covar_k;
  float          **other_covar_k, *otherweights;
  float          **probs, *prob_data, *sum_probs, *probs_k;
  float          max_prob, *weights, *best_weights;
  float          log_likelihood, last_log_likelihood, best_log_likelihood;
  int            best_class, best_try, loglevel;
  FILE           *log_fp, *params_fp, *fp;
  int            dummy;

  /* Variables for use in parallel aspects */

  int	       pe;
  int        cc;
  int        buf;
  int        numPE;
  int        num_channels;
  int        my_numrows;
  int        startrow;
  float      *otherminval, *othermaxval;

  /* Variables for use in timing */

  double     abs_starttime, abs_endtime, time1, time2;

  /* Variables for debugging */
    
  int        DebugLevel;
  /*----------------------------------------------------------------------------*/
  /* Get information about processes and current process */

  /*
    printf("beginning of program\n");
  */
  DA_init_pp(&argc, &argv);

  DA_get_pe_info(&pe, &numPE);

  if (pe == 0) {
    printf("%d: numPE = %d\n",pe,numPE);
    fflush(stdout);
  }

  /*----------------------------------------------------------------------------*/
  /* Get timing information */
  /*
    abs_starttime = time_now();
  */
  /*    printf("%d: The absolute time now is %lf seconds.\n",pe,abs_starttime);*/

  /*----------------------------------------------------------------------------*/
  /* Get command line arguments */
  initialize_args(args);

  if (Get_Args(argc-1, argv, args, EM_NUM_ARGS) != Get_Arg_OK) {
    return(-1);
  }

  if (args[1].exists) datafile   = args[1].arg_str;
  if (args[2].exists) paramsfile = args[2].arg_str;
  else paramsfile = strdup(EM_DEFAULT_PARAMS_FILENAME);
  if (args[3].exists) logfile    = args[3].arg_str;
  else logfile    = strdup(EM_DEFAULT_LOG_FILENAME);
  if (args[4].exists) labelsfile = args[4].arg_str;
  else labelsfile = strdup(EM_DEFAULT_LABELS_FILENAME);
  if (args[5].exists) numrows    = atoi(args[5].arg_str);
  if (args[6].exists) numcols    = atoi(args[6].arg_str);
  if (args[7].exists) K          = atoi(args[7].arg_str);
  if (args[8].exists) seed       = atoi(args[8].arg_str);
  else seed       = EM_DEFAULT_SEED;
  if (args[9].exists) min_diag   = atof(args[9].arg_str);
  else min_diag   = EM_DEFAULT_MIN_DIAG;
  if (args[10].exists) min_weight = atof(args[10].arg_str);
  else min_weight = EM_DEFAULT_MIN_WGT;
  if (args[11].exists) threshold  = atof(args[11].arg_str);
  else threshold  = EM_DEFAULT_THRESH;
  if (args[12].exists) numtries   = atoi(args[12].arg_str);
  else numtries   = EM_DEFAULT_TRIES;
  if (args[13].exists) empty_cluster_ok = UT_TRUE;
  else empty_cluster_ok = UT_FALSE;
  if (args[14].exists) DebugLevel = atoi(args[14].arg_str);
  else DebugLevel = EM_DEFAULT_DEBUG_LEVEL;
  if (args[15].exists) loglevel     = atoi(args[15].arg_str);
  else loglevel     = EM_DEFAULT_LOG_LEVEL;
  if (args[16].exists) max_reconverge = atoi(args[16].arg_str);
  else max_reconverge = EM_DEFAULT_MAX_RECONVERGE;
  if (args[17].exists) max_restarts   = atoi(args[17].arg_str);
  else max_restarts   = EM_DEFAULT_MAX_RESTARTS
  if (args[18].exists) num_channels   = atoi(args[17].arg_str);
  else num_channels   = EM_DEFAULT_NUM_CHANNELS;
  if (args[19].exists) inputmeansfile  = args[19].arg_str;
  else inputmeansfile = NULL;

  if (pe==0)
    {
      printf("params = %s\n",paramsfile);
      printf("log = %s\n",logfile);
      printf("labels = %s\n",labelsfile);
      if inputmeansfile  printf("input means file = %s\n", inputmeansfile)
      fflush(stdout);
    }

  /*----------------------------------------------------------------------------*/
  /* seed random number generation sequence, based on seed supplied */
  set_rand(seed);

  if (pe==0) {
    printf("%d: after set_rand()\n",pe);
    fflush(stdout);
  }

  /*----------------------------------------------------------------------------*/
  /* read in the data matrix */

  /* compute the start and end points depending on which processor you are */
  split_data_pp(&startrow, &my_numrows, numrows, pe, numPE);

  if (pe==0) {
    printf("%d: before reading data\n",pe);
    fflush(stdout);
  }

  /*    time1 = time_now(); */
  read_subset2matrix(datafile, startrow, my_numrows, numcols, &initdata);
  /*    time2 = time_now(); */
  /*    printf("%d: i/o time = %lf\n",pe,time2-time1); */

  if (pe==0) {
    printf("%d: after reading data\n",pe);
    fflush(stdout);
  }
  /*
    read_matrix(datafile, my_numrows, numcols, &initdata);
  */

  /*----------------------------------------------------------------------------*/
  /* create storage for the K mean vectors and covariance matrices */
  means = NR_matrix(1,K,1,numcols);
  initmeans  = NR_matrix(1,K,1,numcols);
  best_means = NR_matrix(1,K,1,numcols);

  /*
    covars =      (float***) malloc(K * sizeof(float**));
    best_covars = (float***) malloc(K * sizeof(float**));
    for (k=1; k<=K; k++)
    {
    covars[k] = matrix(1,numcols,1,numcols);
    best_covars[k] = matrix(1,numcols,1,numcols);
    }
  */
  covars = set_of_matrices(K, numcols, numcols);
  best_covars = set_of_matrices(K, numcols, numcols);

  /* create storage for the classes for each datum */
  class = NR_ivector(1,my_numrows);

  /* create storage for information regarding each class */
  clustsize = NR_ivector(1,K);  /* used only by k-means */
  weights = NR_vector(1,K);
  best_weights = NR_vector(1,K);

  /* create storage for all computed probabilities; one row for each class */
  probs = NR_matrix(1,K,1,my_numrows);

  /* create storage for intermediate computed quantities */
  prob_data = NR_vector(1,my_numrows);
  sum_probs = NR_vector(1,K);

  /* create storage related to message passing */
  other_covar_k = NR_matrix(1,numcols,1,numcols);
  otherweights  = NR_vector(1,K);

  if (pe==0) {
    printf("%d: after making storage\n",pe);
    fflush(stdout);
  }

  /*----------------------------------------------------------------------------*/
  /* normalize each attribute to a 0-1 scale */

  /* create storage for the normalization */
  minval = NR_vector(1,numcols);
  maxval = NR_vector(1,numcols);
  range  = NR_vector(1,numcols);

  range_normalize_cols_pp(initdata, minval, maxval, range, my_numrows, 
                          numcols, pe, numPE);

  /*----------------------------------------------------------------------------*/
  /* open output report for writing */
  if ((loglevel > 0) && (pe == 0))
    {
      /*
        log_fp = fopen(logfile, "w");
        if (log_fp == (FILE*) NULL) {
        printf("Error in trying to open a file.\n");
        return (UT_ERROR);
        }
      */
      log_fp = stdout;

      fprintf(log_fp,"====== K-MEANS PHASE:\n\n");
      fprintf(log_fp, "%d tries of k-means clustering of file %s...\n\n", 
              numtries, datafile);
    }

  if (pe==0) {
    printf("%d: after opening report\n",pe);
    fflush(stdout);
  }

  /*----------------------------------------------------------------------------*/
  num_restarts = 0;

 restart:  /* this label is here for the case when EM gets a collapsed class;
              we just begin the whole thing from scratch, getting new means
              from k-means */

  /*----------------------------------------------------------------------------*/
  /* do one or more k-means runs, taking the outcome with the highest 
     likelihood as the initial starting point for EM */


  best_log_likelihood = 0.0;
  for (l=1; l<=numtries; l++)
    {
    
      if ((loglevel > 0) && (pe == 0))
        {
          fprintf(log_fp, "--> Try # %d of k-means:\n", l);
        }

      num_reconverge = 0;
      while (num_reconverge <= max_reconverge)
        {
          /* choose random points as starting means for this run */

          random_means_pp(initmeans, K, numcols, pe);

          /* choose random points in the data as starting means for this run */
          /*
            random_means_from_data_pp(initmeans, K, numcols, initdata, numrows, 
            "rows", pe);
          */

          if ((loglevel > 0) && (pe == 0))
            {
              fprintf(log_fp, "Initial means given to k-means try # %d\n", l);
              print_unnorm_matrix(log_fp, K, numcols, initmeans, range, minval);
            }

          if (pe==0) {
            printf("%d: after message passing for choosing random means\n",pe);
            fflush(stdout);
          }
          if (pe==0)
            {
              printf("%d: initmeans: \n",pe);
              print_matrix(stdout, K, numcols, initmeans);
              fflush(stdout);
            }

          /*----------------------------------------------------------------------------*/
          /* perform k-means */

          if (pe==0) {
            printf("%d: before kmeans_pp()\n",pe);
            fflush(stdout);
          }
      
          k_means_pp(initdata, my_numrows, numcols, means, initmeans, clustsize, 
                     class, K, &numiters, &num_empty_clusters, empty_cluster_ok,
                     pe, numPE);
        
          if (pe==0) {
            printf("%d: after kmeans_pp()\n",pe);
            fflush(stdout);
          }

          if (pe==0) {
            printf("%d: num_reconverge = %d,num_empty_clusters = %d\n",pe,num_reconverge,
                   num_empty_clusters);
            fflush(stdout);
          }

          if (pe==0) {
            printf("%d: empty_cluster_ok = %d\n",pe,empty_cluster_ok);
            fflush(stdout);
          }

      
          /* check for collapsed clusters */
          if (num_empty_clusters > 0)
            {
              if (empty_cluster_ok == UT_FALSE)
                {

                  num_reconverge++;
                  if (pe==0) {
                    printf("%d: num_reconverge = %d,num_empty_clusters = %d\n",pe,num_reconverge,
                           num_empty_clusters);
                    fflush(stdout);
                  }

                }
              else
                {
                  K -= num_empty_clusters;  /* caller must also do this */
                  printf("This option does not work yet.\n");
                  return (UT_ERROR);  /* for now... when this works, just continue
                                         with smaller K */
                }
            }
          else
            break;

          if (pe==0) {
            printf("%d: after checking for collapsed clusters\n",pe);
            fflush(stdout);
          }

          if (pe==0) {
            printf("%d: num_reconverge = %d,num_empty_clusters = %d\n",pe,num_reconverge,
                   num_empty_clusters);
            fflush(stdout);
          }

        } /* end k-means loop over reconvergences */

      if ((loglevel > 0) && (pe == 0))
        {
          /* if we couldn't converge at all */
          if (num_empty_clusters > 0)
            {
              fprintf(log_fp, "Couldn't converge at all on try # %d\n", l);
              fprintf(log_fp, "Tried %d times!\n", max_reconverge);
            }
          else
            {
              fprintf(log_fp, "Num. reconvergences required for try # %d = %d\n",
                      l, num_reconverge);
              fprintf(log_fp, "Num. iterations required for try # %d = %d\n",
                      l, numiters);
              fprintf(log_fp,"Means determined by k-means try # %d\n", l);
              print_unnorm_matrix(log_fp, K, numcols, means, range, minval);
            }
        }

      if (pe==0) {
        printf("%d: after printing kmeans means and other stuff\n",pe);
        fflush(stdout);
      }

      /* if we couldn't converge at all, skip the rest of this loop */
      if (num_empty_clusters > 0)
        {
          continue;
        }

      /*----------------------------------------------------------------------------*/
      /* now we have the initial means to give to EM; compute the other 
         clustering parameters based on the class assignments */

      fast_zero_vec(weights, K);
      for (k=1; k<=K; k++)
        fast_zero_mat(covars[k], numcols, numcols);
      
      for (i=1; i<=my_numrows; i++)
        {
          k = class[i];
        
          /* get the parameters corresponding to the class of this datum */
          covar_k = covars[k];
          mean_k  = means[k];

          /* estimate initial weights given members of k-means clusters */
          /* add the contribution of this datum to its class */
          weights[k]++;
        
          /* estimate initial covariance matrices given members of k-means 
             clusters */
          /* add the square of the deviation vectors to the right position in the
             covariance */
          for (j=1; j<=numcols; j++)
            {
              data_i = initdata[i];
              for (j2=j; j2<=numcols; j2++)
                covar_k[j][j2] += (data_i[j] - mean_k[j])*(data_i[j2] - mean_k[j2]);
            }
        }

      if (pe==0) {
        printf("%d: after local computation of covar. and weights from kmeans\n",pe);
        fflush(stdout);
      }

      /* Do parallel communication to collate weights */
      DA_barrier();
      if (pe != 0) /* This is not the controlling process */
        {
          /* printf("%d: This is not the controlling process.\n",pe); */

          /* Send out the locally computed weights */
          cc = DA_send_msg(&weights[1],K,0,pe,DA_FLOAT);

          /* Now block until receive broadcast message from the controlling
             process giving the global weights */
          cc = DA_broadcast_msg(&weights[1],K,DA_ALL_PES,0,DA_FLOAT);

        }
      else /* This is the controlling process */
        {
          /* printf("%d: This is the controlling process.\n",pe); */

          /* Receive the other sets of weights, do collation */
          for(p=1;p<numPE;p++)
            {
              cc = DA_recv_msg(&otherweights[1],K,p,p,DA_FLOAT);
              add_vec(weights, otherweights, weights, K);
            }

          /* Now broadcast the global weights */
          cc = DA_broadcast_msg(&weights[1],K,DA_ALL_PES,0,DA_FLOAT);

        }     

      if (pe==0) {
        printf("%d: after message passing to collate weights\n",pe);
        fflush(stdout);
      }

      /* normalize weights by the total number of data */
      scalar_div_vec(weights, K, numrows);

      /* normalize the covariance matrices by the total number of data */
      for (k=1; k<=K; k++)
        scalar_div_mat(covars[k], numcols, numcols, (float) numrows);

      if (pe==0)
        printf("%d: after normalizing by numrows\n",pe);

      /* Do parallel communication to collate covariance matrices */
      DA_barrier();
      if (pe != 0) /* This is not the controlling process */
        {
          /* printf("%d: This is not the controlling process.\n",pe); */
     
          /* Send out the locally computed covariance matrices */
          for (k=1; k<K; k++)
            {
              covar_k = covars[k];
              cc = DA_send_msg(&covar_k[1][1],numcols*numcols,0,k,DA_FLOAT);
              /*
                cc = send_msg(&covar_k[1][1],numcols*numcols,0,K*pe+k,
                DA_FLOAT);
              */
            }

          /* Now block until receive broadcast message from the controlling
             process giving the global covariance matrices */
          for (k=1; k<K; k++)
            {
              covar_k = covars[k];
              cc = DA_broadcast_msg(&covar_k[1][1],numcols*numcols,DA_ALL_PES,0,DA_FLOAT);
              DA_barrier();
            }

        }
      else /* This is the controlling process */
        {
          /* printf("%d: This is the controlling process.\n",pe); */

          /* Receive the other sets of covariance matrices, do collation */
          for(p=1;p<numPE;p++)
            {
              for (k=1; k<K; k++)
                {
                  covar_k = covars[k];
                  cc = DA_recv_msg(&other_covar_k[1][1],numcols*numcols,p,k,DA_FLOAT);
                  /*
                    cc = recv_msg(&other_covar_k[1][1],numcols*numcols,p,K*p+k,
                    DA_FLOAT);
                  */
                  add_mat(covar_k, other_covar_k, covar_k, numcols, numcols);
                }
            }

          /* Now broadcast the global cluster sizes */
          for (k=1; k<K; k++)
            {
              covar_k = covars[k];
              cc = DA_broadcast_msg(&covar_k[1][1],numcols*numcols,DA_ALL_PES,0,
                                    DA_FLOAT);
              DA_barrier();
            }
        }     

      if (pe==0)
        printf("%d: after message-passing to collate covariance matrices\n",pe);

      /* fill in the lower triangle of each covariance matrix based on upper 
         triangle */
      for (k=1; k<=K; k++)
        {
          covar_k = covars[k];
          for (j=1; j<=numcols; j++)
            for (j2=1; j2<j; j2++)
              covar_k[j][j2] = covar_k[j2][j];
        }

      if (pe==0)
        printf("%d: after filling in lower triangle of covariance matrix\n",pe);

      if ((loglevel > 0) && (pe == 0))
        {
          fprintf(log_fp,"Covariance matrices from k-means run # %d\n", l);
          for (k=1; k<=K; k++)
            print_unnorm_cov_matrix(log_fp, numcols, covars[k], range);
          fprintf(log_fp,"Class weights from k-means run # %d\n", l);
          print_row(log_fp, K, weights);
        }

      if (pe==0)
        printf("%d: after printing covariance and weights from kmeans\n",pe);

      /*----------------------------------------------------------------------------*/
      /* given the parameters, compute the likelihood of this model */
      log_likelihood = mixture_likelihood_pp(K, means, covars, weights, 
                                             initdata, probs, prob_data, 
                                             sum_probs, my_numrows, numcols, 
                                             "rows", min_diag, pe, numPE);

      if (pe==0)
        printf("%d: after computing mixture_likelihood from kmeans\n",pe);

      /* save the current best set of parameters */
      if (log_likelihood > best_log_likelihood)
        {
          best_try = l;
          best_log_likelihood = log_likelihood;
          for (k=1; k<=K; k++)
            {
              copy_vec(means[k], best_means[k], numcols);
              copy_mat(covars[k], best_covars[k], numcols, numcols);
            }
          copy_vec(weights, best_weights, K);
        }

      if (pe==0)
        printf("%d: after copying the best params from kmeans\n",pe);

      if ((loglevel > 0) && (pe == 0))
        {
          fprintf(log_fp, "log-likelihood for try # %d = %g\n", l,log_likelihood);
        }

    }  /* end k-means loop over numtries */

  if (pe==0)
    printf("%d: after kmeans loop\n",pe);

  /*----------------------------------------------------------------------------*/
  /* report the best parameters found */

  if ((loglevel > 0) && (pe == 0))
    {
      fprintf(log_fp, "\nBEST K-MEANS RESULTS:\n");
      fprintf(log_fp, "best try was # %d\n", best_try);
      fprintf(log_fp, "log-likelihood for best try = %g\n",best_log_likelihood);
      fprintf(log_fp,"\nbest k-means mixture model params.\n");
      fprintf(log_fp,  "----------------------------------\n\n");
      
      print_unnorm_gauss_parms_set(log_fp, numcols, K, best_means, best_covars,
                                   range, minval);
      fprintf(log_fp,"\nclass weights from best try\n");
      print_row(log_fp, K, best_weights);
    }

  if (pe==0)
    printf("%d: after reporting the best kmeans params\n",pe);

  /*----------------------------------------------------------------------------*/
  /* use the best parameters found by k-means to initialize EM */
  NR_free_matrix(means, 1, K, 1, numcols);
  /*
    for (k=1; k<=K; k++)
    NR_free_matrix(covars[k], 1, numcols, 1, numcols);
  */
  free_set_of_matrices(covars, K, numcols, numcols);
  NR_free_vector(weights, 1, K);

  means = best_means;
  covars = best_covars;
  weights = best_weights;

  /* prepare for estimate_mixture_params() to */

  /*----------------------------------------------------------------------------*/
  /* convert data matrix to be indexed by column rather than by rows */
  data = NR_matrix(1,numcols,1,my_numrows);

  transpose_matrix(initdata, my_numrows, numcols, data);

  /* throw away k-means-related storage */
  NR_free_matrix(initdata, 1, my_numrows, 1, numcols);
  NR_free_matrix(initmeans, 1, K, 1, numcols);
  NR_free_ivector(clustsize, 1, K);

  /*----------------------------------------------------------------------------*/
  /* perform EM iterations */

  /* set initial conditions for the convergence */
  numiters = 0;
  num_reconverge = 0;
  converged = UT_FALSE;
  log_likelihood = 0.0;

  if ((loglevel > 0) && (pe == 0))
    {
      fprintf(log_fp,"====== EM PHASE:\n\n");
    }

  while (converged == UT_FALSE)
    {

      if (pe==0)
        {
          printf("weights:\n");
          print_row(stdout, K, weights);
          printf("sum_probs:\n");
          print_row(stdout, K, sum_probs);
        }

      /* initialize for this iteration */
      last_log_likelihood = log_likelihood;

      /* M-step - "Maximization" */
      /* I. For each class, compute the posterior probability of the class
         given each datum and the class parameters, p(w|d) */
      log_likelihood = mixture_likelihood_pp(K, means, covars, weights, data, 
                                             probs, prob_data, sum_probs, 
                                             my_numrows, numcols, "cols", 
                                             min_diag, pe, numPE);

      if (pe==0) {
        printf("%d: after mixture_likelihood_pp()\n",pe);
        fflush(stdout);
      }

      if (pe==0)
        {
          printf("weights:\n");
          print_row(stdout, K, weights);
          printf("sum_probs:\n");
          print_row(stdout, K, sum_probs);
        }

      if ((loglevel > 1) && (pe == 0))
        {
          fprintf(log_fp,"log-likelihood for iteration #%d = %g\n", numiters,
                  log_likelihood);
          fprintf(log_fp, 
                  "log-likelihood - last log-likelihood for iteration #%d = %g\n",
                  numiters, (float) fabs( (double)(log_likelihood - 
                                                   last_log_likelihood)) );
          fprintf(log_fp,"threshold = %g\n", threshold);
          fflush(log_fp);
        }

      /* check for convergence */
      if ((float) fabs( (double)(log_likelihood - last_log_likelihood)) 
          <= threshold)
        {
          if ((loglevel > 1) && (pe == 0))
            {
              fprintf(log_fp,"Converged.\n");
              fflush(log_fp);
            }
          converged = UT_TRUE;
          continue;
        }

      if (pe==0) {
        printf("%d: after convergence check\n",pe);
        fflush(stdout);
      }


      /* E-step - "Expectation" */
      /* II. For each class, compute the parameters given the posterior
         probability of each datum */
      estimate_mixture_params_pp(K, means, covars, weights, data, probs, 
                                 sum_probs, numrows, my_numrows, numcols, 
                                 min_weight, &num_empty_clusters, 
                                 empty_cluster_ok, pe, numPE);

      if (pe==0) {
        printf("%d: after estimate_mixture_params_pp()\n",pe);
        fflush(stdout);
      }

      if (pe==0)
        {
          printf("weights:\n");
          print_row(stdout, K, weights);
          printf("sum_probs:\n");
          print_row(stdout, K, sum_probs);
        }
      /* check for collapsed clusters */
      if (num_empty_clusters > 0)
        {
          if (empty_cluster_ok == UT_FALSE)
            {
              num_restarts++;
              if (num_restarts > max_restarts)
                return (UT_ERROR);
              else
                /* start the whole process over, including k-means runs */
                /* since this really should start by recomputing initdata (since
                   it was freed), for example, it must restart at an earlier point
                   than it does; this means existing stuff must be freed also...*/
                /* for now, just exit */
                if (pe == 0)
                  printf("%d: We have collapsed clusters from em; \n",pe);
              if (pe == 0)
                printf("%d:   restart not implemented yet - exiting\n",pe);
              return(UT_ERROR);

              goto restart;
            }
          else
            {
              K -= num_empty_clusters;  /* caller must also do this */
              printf("This option does not work yet.\n");
              return (UT_ERROR);  /* for now... when this works, just continue
                                     with smaller K */
            }
        }

      if (pe==0) {
        printf("%d: after checking for collapsed clusters\n",pe);
        fflush(stdout);
      }

      /* keep track of num iterations */
      numiters++;
      if ((loglevel > 1) && (pe == 0))
        {
          fprintf(log_fp,"finished iteration# %d\n", numiters);
        }

    } /* end of main em loop over iterations */

  if (pe==0) {
    printf("%d: reached end of em loop after %d iterations\n",pe,numiters);
    fflush(stdout);
  }

  /*----------------------------------------------------------------------------*/
  /* determine the class assignments for each datum */
  for (i=1; i<=my_numrows; i++)
    {
      max_prob = probs[1][i];
      best_class = 1;
      for (k=2; k<=K; k++)
        if (probs[k][i] > max_prob)
          {
            max_prob = probs[k][i];
            best_class = k;
          }
      class[i] = best_class;
    }

  if (pe==0) {
    printf("%d: after making class assignments\n",pe);
    fflush(stdout);
  }

  /*----------------------------------------------------------------------------*/
  /* Done */
  /* report final EM parameters */

  if (pe == 0)
    {
      /* open file for writing */
      params_fp = fopen(paramsfile, "w");
      if (params_fp == (FILE*) NULL) {
        printf("Error in trying to open a file.\n");
        return (UT_ERROR);
      }
      
      fprintf(params_fp, "EM clustering of file %s...\n\n", datafile);
      fprintf(params_fp, "Number of reconvergences required = %d\n",
              num_reconverge);
      fprintf(params_fp, "Number of iterations required = %d\n\n", numiters);
      fprintf(params_fp, "EM mixture model parameters\n");
      fprintf(params_fp, "---------------------------\n\n");
      
      print_unnorm_gauss_parms_set(params_fp, numcols, K, means, covars, 
                                   range, minval);
      fprintf(params_fp,"\nclass weights\n");
      print_row(params_fp, K, best_weights);
      fprintf(params_fp, "log-likelihood of model = %g\n",log_likelihood);
    }

  if (pe==0) {
    printf("%d: after reporting final em params\n",pe);
    fflush(stdout);
  }

  /* labels file */
  write_icol_cascade_pp(labelsfile, my_numrows, class, "w", pe, numPE);

  if (pe==0) {
    printf("%d: after writing labels file\n",pe);
    fflush(stdout);
  }

  if (pe==0)
    write_matrix("probs.0", K, my_numrows, probs, "w");
  if (pe==1)
    write_matrix("probs.1", K, my_numrows, probs, "w");

  /*----------------------------------------------------------------------------*/
  /* clean up memory */
  NR_free_vector(minval, 1, numcols);
  NR_free_vector(maxval, 1, numcols);
  NR_free_vector(range, 1, numcols);

  NR_free_ivector(class, 1, my_numrows);
  NR_free_matrix(data, 1, numcols, 1, my_numrows);

  NR_free_matrix(probs, 1, K, 1, my_numrows);
  NR_free_vector(prob_data, 1, my_numrows);
  NR_free_vector(sum_probs, 1, K);
    
  NR_free_vector(weights, 1, K);
  NR_free_matrix(means, 1, K, 1, numcols);
  /*
    for (k=1; k<=K; k++)
    NR_free_matrix(covars[k], 1, numcols, 1, numcols);
  */
  free_set_of_matrices(covars, K, numcols, numcols);

  /* free storage related to message passing */
  NR_free_matrix(other_covar_k,1,numcols,1,numcols);
  NR_free_vector(otherweights,1,K);

  if (pe==0) {
    printf("%d: after freeing memory\n",pe);
    fflush(stdout);
  }

  /* close all files */
  /*
    fclose(log_fp);
  */
  if (pe == 0)
    fclose(params_fp);

  if (pe==0) {
    printf("%d: after closing files\n",pe);
    fflush(stdout);
  }

  DA_finalize_pp();

  return(UT_OK);
}

/*******************************************************************************
 INITIALIZE_ARGS
 Initialize structures that store command-line argument information.
*******************************************************************************/
void initialize_args(args)
     arg args[];
{
  Init_Arg(args,1, "i",     FALSE, 1, "input data matrix file");
  Init_Arg(args,2, "p",      TRUE, 1, "output Gaussian parameters file");
  Init_Arg(args,3, "l",      TRUE, 1, "output log file");
  Init_Arg(args,4, "c",      TRUE, 1, "output cluster labels file");
  Init_Arg(args,5, "rows",  FALSE, 1, "number of rows of input matrix");
  Init_Arg(args,6, "cols",  FALSE, 1, "number of columns of input matrix");
  Init_Arg(args,7, "k",     FALSE, 1, "number of means to find");
  Init_Arg(args,8, "seed",   TRUE, 1, "random seed to use");
  Init_Arg(args,9, "mindiag",TRUE, 1, "minimum value on covariance diagonal");
  Init_Arg(args,10,"minwgt", TRUE, 1, "minimum class weight");
  Init_Arg(args,11,"thresh", TRUE, 1, "convergence threshold");
  Init_Arg(args,12,"tries",  TRUE, 1, "number of optimization attempts");
  Init_Arg(args,13,"collapse",TRUE,0, "okay to collapse clusters");
  Init_Arg(args,14,"debug",  TRUE, 1, "debug print level");
  Init_Arg(args,15,"loglev", TRUE, 1, "log print level");
  Init_Arg(args,16,"nreconv",TRUE, 1, "maximum num. reconvergence attempts");
  Init_Arg(args,17,"nrestart",TRUE,1, "maximum num. restart attempts");
  Init_Arg(args,18,"channels",TRUE,1, "number of i/o channels to use");
  Init_Arg(args,19,"means", TRUE, 1, "file containing mean vectors to initialize EM with");  

  return;
}









