function k_best = findpeak(likelihoods);

k_max = length(likelihoods);

likelihoods_dd = conv([-1 2 -1], likelihoods);
likelihoods_dd = likelihoods_dd(2:(k_max+1));

[max, k_best] = max(likelihoods);

if ((k_best ~= 1) & ...
    ((k_best ~= k_max) | ...
     ((likelihoods(k_max) - likelihoods(k_max-1))/likelihoods(k_max) < 0.01)))
  max2 = likelihoods(1);
  for k = 2:(k_max-1)
    if ((likelihoods_dd(k) > 0.05 * max) & (likelihoods(k) > max2))
      k_best = k;
    end
    if (likelihoods(k) > max2)
      max2 = likelihoods(k);
    end
  end
end

figure(1)
plot(1:k_max, likelihoods)
figure(2)
plot(1:k_max, likelihoods_dd)
