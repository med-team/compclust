/*******************************************************************************

  Title:     Parallel EM
  Author:    Alexander Gray, Robert Granat
  Function:  Perform the EM algorithm, in the context of mixture modelling, on 
             a dataset, returning as output a file containing the model para-
             meters found by the algorithm.  The number of components and
             initial random seed value are specified by the user.
  Reference: Duda and Hart, ch. 6 (p. 200)
  How used:  For implementation on a parallel computer.
  Compile:   make
  Example:   mix_em_pp -i test.data -rows 500 -cols 10 -p test.params -c 
             test.labels -k 4 -seed 13
  Notes:     Do not attempt to run this program in other parallel environments
             without revision.

             - performance - to allow passing of all K covar. matrices in one 
                 big message, maybe use submatrix() nr function
             - performance - only send the upper triangle of a covar. matrix

             - clean source - separate out things into parallel functions
                 - normalize_data_pp()
                 - divide_data()
                 - write_icol_cascade()

*******************************************************************************/
#ifndef lint
static char rcsid[] = "$Id: mix_em_pp_wrapper.c,v 1.2 2001/12/05 00:47:59 hart Exp $";
#endif
/* 
 * $Log: mix_em_pp_wrapper.c,v $
 * Revision 1.2  2001/12/05 00:47:59  hart
 * Modifications to allow users to specify the exact locations to initialize
 * cluster means.  This is in effort to build in an ability to chain FullEM as
 * post clustering clusteirng algo.
 *
 * Revision 1.1.1.1  2001/05/21 23:36:19  diane
 * Import of various mls & woldlab source packages
 *
 * Revision 1.3  2001/03/22 19:34:02  granat
 * added logic so that -anneal flag also works with -xval flag by using
 * method on best choice of k
 * removed calculation of results for k less than the best k
 * function call to calculate results for best k is now outside of the cross-
 * validation function call
 *
 * Revision 1.2  2001/01/26 18:17:11  granat
 * added flag to not perform normalization of data
 *
 * Revision 1.1  2001/01/26 18:00:10  granat
 * Initial revision
 *
 * Revision 1.2  1996/07/19 18:02:34  agray
 * functionalized lots of code sections, added number of channels as argument
 *
 * Revision 1.1  1996/07/11 18:29:10  agray
 * Initial revision
 *
 * */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>

#include "cp.h"

#include "ut.h"

#include "nr.h"
#include "nr_util.h"

#include "da_platform.h"
#include "da_io.h"
#include "da_io_pp.h"
#include "da_cluster.h"
#include "da_cluster_pp.h"
#include "da_util.h"
#include "da_linalg.h"
#include "da_signal_pp.h"
#include "da_comm_pp.h"
#include "da_random.h"

#include "mix_em_pp.h"
#include "mix_em_pp_wrapper.h"

/*******************************************************************************
 MAIN
 Driver of program.
*******************************************************************************/



main(int argc, char *argv[])
{
  arg            args[EM_NUM_ARGS+1];
  char           *datafile, *paramsfile, *labelsfile, *logfile, *probsfile, *inputmeansfile;
  char           new_labelsfile[80];
  char           stringk[10];
  double         **data, *data_i, *data_j, *data_j2;
  double         **initdata, **initmeans;
  double         *minval, *maxval, *range;
  int            *clustsize, *class;
  int            i, j, j2, k, K, l, p, numrows, numcols;
  boolean        converged, empty_cluster_ok, do_pca, do_anneal, do_xval, means_already_initialized;
  boolean        do_norm;
  int            numiters, seed, numtries;
  int            num_reconverge, max_reconverge, num_empty_clusters;
  int            num_restarts, max_restarts;
  double         min_diag, threshold, min_weight, max_dims;
  double         **means, ***covars, **best_means, ***best_covars;
  double         *mean_k, **covar_k;
  double         **other_covar_k, *otherweights;
  double         **probs, *prob_data, *sum_probs, *probs_k;
  double         max_prob, *weights, *best_weights;
  double         *class_prob;
  double         log_likelihood, last_log_likelihood, best_log_likelihood;
  int            best_class, best_try, loglevel;
  FILE           *log_fp, *params_fp, *fp;
  int            dummy;
  int            K_max, numtrials, k_best;
  int            status;
  /* Variables for use in parallel aspects */

  int	       pe;
  int        cc;
  int        buf;
  int        numPE;
  int        num_channels;
  int        my_numrows;
  int        startrow;
  double     *otherminval, *othermaxval;

  /* Variables for use in timing */

  double     abs_starttime, abs_endtime, time1, time2;

  /* Variables for debugging */
    
  int        DebugLevel;
  /*----------------------------------------------------------------------------*/
  /* Get information about processes and current process */

  /*
    printf("beginning of program\n");
  */
  DA_init_pp(&argc, &argv);

  DA_get_pe_info(&pe, &numPE);

  if (pe == 0) {
    printf("%d: numPE = %d\n",pe,numPE);
    fflush(stdout);
  }

  /*----------------------------------------------------------------------------*/
  /* Get timing information */
  /*
    abs_starttime = time_now();
  */
  /* printf("%d: The absolute time now is %lf seconds.\n",pe,abs_starttime); */

  /*----------------------------------------------------------------------------*/
  /* Get command line arguments */
  initialize_args(args);

  if (Get_Args(argc-1, argv, args, EM_NUM_ARGS) != Get_Arg_OK) {
    return(-1);
  }

  if (args[1].exists) datafile   = args[1].arg_str;
  if (args[2].exists) paramsfile = args[2].arg_str;
  else            paramsfile = strdup(EM_DEFAULT_PARAMS_FILENAME);
  if (args[3].exists) logfile    = args[3].arg_str;
  else            logfile    = strdup(EM_DEFAULT_LOG_FILENAME);
  if (args[4].exists) labelsfile = args[4].arg_str;
  else            labelsfile = strdup(EM_DEFAULT_LABELS_FILENAME);
  if (args[5].exists) numrows    = atoi(args[5].arg_str);
  if (args[6].exists) numcols    = atoi(args[6].arg_str);
  if (args[7].exists) K          = atoi(args[7].arg_str);
  if (args[8].exists) seed       = atoi(args[8].arg_str);
  else            seed       = EM_DEFAULT_SEED;
  if (args[9].exists) min_diag   = atof(args[9].arg_str);
  else            min_diag   = EM_DEFAULT_MIN_DIAG;
  if (args[10].exists) min_weight = atof(args[10].arg_str);
  else             min_weight = EM_DEFAULT_MIN_WGT;
  if (args[11].exists) threshold  = atof(args[11].arg_str);
  else             threshold  = EM_DEFAULT_THRESH;
  if (args[12].exists) numtries   = atoi(args[12].arg_str);
  else             numtries   = EM_DEFAULT_TRIES;
  if (args[13].exists) empty_cluster_ok = UT_TRUE;
  else             empty_cluster_ok = UT_FALSE;
  if (args[14].exists) numtrials = atoi(args[14].arg_str);
  else             numtrials = EM_DEFAULT_TRIALS;
  if (args[15].exists) loglevel     = atoi(args[15].arg_str);
  else             loglevel     = EM_DEFAULT_LOG_LEVEL;
  if (args[16].exists) max_reconverge = atoi(args[16].arg_str);
  else             max_reconverge = EM_DEFAULT_MAX_RECONVERGE;
  if (args[17].exists) max_restarts   = atoi(args[17].arg_str);
  else             max_restarts   = EM_DEFAULT_MAX_RESTARTS;
  if (args[18].exists) num_channels   = atoi(args[18].arg_str);
  else             num_channels   = EM_DEFAULT_NUM_CHANNELS;
  if (args[19].exists) do_pca   = UT_TRUE;
  else             do_pca   = UT_FALSE;
  if (args[20].exists) max_dims   = atoi(args[20].arg_str);
  else             max_dims   = EM_DEFAULT_MAX_DIMS;
  if (args[21].exists) probsfile = args[21].arg_str;
  else             probsfile = strdup(EM_DEFAULT_PROBS_FILENAME);
  if (args[22].exists) do_anneal = UT_TRUE;
  else             do_anneal = UT_FALSE;
  if (args[23].exists) do_xval = UT_TRUE;
  else             do_xval = UT_FALSE;
  if (args[24].exists) do_norm = UT_FALSE;
  else             do_norm = UT_TRUE;
  if (args[25].exists) inputmeansfile = args[25].arg_str;
  else inputmeansfile = NULL;

  if (pe==0)
    {
      printf("params = %s\n",paramsfile);
      printf("log = %s\n",logfile);
      printf("labels = %s\n",labelsfile);
      fflush(stdout);
    }

  /*----------------------------------------------------------------------------*/
  /* seed random number generation sequence, based on seed supplied */
  if (seed == 0)
    set_rand_by_time_of_day();
  else
    set_rand(seed);

  if (pe==0) {
    printf("%d: after set_rand()\n",pe);
    fflush(stdout);
  }

  /*----------------------------------------------------------------------------*/
  /* read in the data matrix */

  /* compute the start and end points depending on which processor you are */
  split_data_pp(&startrow, &my_numrows, numrows, pe, numPE);

  if (pe==0) {
    printf("%d: before reading data\n",pe);
    fflush(stdout);
  }

  /*    time1 = time_now(); */
  read_subset2dmatrix(datafile, startrow, my_numrows, numcols, &initdata);
  /*    time2 = time_now(); */
  /*    printf("%d: i/o time = %lf\n",pe,time2-time1); */

  if (pe==0) {
    printf("%d: after reading data\n",pe);
    fflush(stdout);
  }
  /*
    read_dmatrix(datafile, my_numrows, numcols, &initdata);
  */

  /*----------------------------------------------------------------------------*/
  /* create storage for the K mean vectors and covariance matrices */

  initmeans  = NR_dmatrix(1,K,1,numcols);
  best_means = NR_dmatrix(1,K,1,numcols);


  if (inputmeansfile) {
    printf ("reading means file %s\n ", inputmeansfile,K, numcols);
    // print_dmatrix(stdout, K, numcols, means);
    status = read_dmatrix(inputmeansfile, K, numcols, &means);
    print_dmatrix(stdout, K, numcols, means);
    means_already_initialized = UT_TRUE;
  }

  else{
    means = NR_dmatrix(1,K,1,numcols);
    means_already_initialized = UT_FALSE;
  }

  covars = set_of_dmatrices(K, numcols, numcols);
  best_covars = set_of_dmatrices(K, numcols, numcols);

  /* create storage for the classes for each datum */
  class = NR_ivector(1,my_numrows);
  class_prob = NR_dvector(1,my_numrows);

  /* create storage for information regarding each class */
  clustsize = NR_ivector(1,K);
  weights = NR_dvector(1,K);
  best_weights = NR_dvector(1,K);

  /* create storage for all computed probabilities; one row for each class */
  probs = NR_dmatrix(1,K,1,my_numrows);

  /* create storage for intermediate computed quantities */
  prob_data = NR_dvector(1,my_numrows);
  sum_probs = NR_dvector(1,K);

  /* create storage related to message passing */
  other_covar_k = NR_dmatrix(1,numcols,1,numcols);
  otherweights  = NR_dvector(1,K);

  if (pe==0) {
    printf("%d: after making storage\n",pe);
    fflush(stdout);
  }

  /*----------------------------------------------------------------------------*/
  /* normalize each attribute to a 0-1 scale */

  if (do_norm == UT_TRUE) {
    /* create storage for the normalization */
    minval = NR_dvector(1,numcols);
    maxval = NR_dvector(1,numcols);
    range  = NR_dvector(1,numcols);

    range_normalize_dcols_pp(initdata, minval, maxval, range, my_numrows, 
                             numcols, pe, numPE);
    if (means_already_initialized == UT_TRUE)
      range_normalize_dcols_pp(means, minval, maxval, range, K, 
                               numcols, pe, numPE);
  }

  /*----------------------------------------------------------------------------*/
  /* perform dimensionality reduction using pca, if selected */
  if (do_pca == UT_TRUE) {
    printf("pca does not work yet... need to work out parallel issues\n");
  }
  /*----------------------------------------------------------------------------*/
  /* open output report for writing */
  if ((loglevel > 0) && (pe == 0))
    {
      log_fp = stdout;
    }

  if (pe==0) {
    printf("%d: after opening report\n",pe);
    fflush(stdout);
  }

  /*----------------------------------------------------------------------------*/
  /* perform the actual mixture model EM */
  if (do_xval == UT_FALSE) {
    if (do_anneal == UT_TRUE)
      mix_em_anneal_alloc_pp(initdata, my_numrows, numcols, means, covars, 
                             probs, weights, class, K, threshold, 
                             min_weight, min_diag, numtries, 
                             max_reconverge, max_restarts, UT_TRUE);
    else 
      mix_em_alloc_pp(initdata, my_numrows, numcols, means, covars, probs, 
                      weights, class, K, threshold, min_weight, min_diag, 
                      numtries, max_reconverge, max_restarts, UT_TRUE, 
                      means_already_initialized );
  }
  else {
    K_max = K;
    crossval_mix_em_alloc_pp(initdata, my_numrows, numcols, means, covars, 
                             probs, weights, class, K_max, numtrials, 
                             &k_best, threshold, min_weight, min_diag, 
                             numtries, max_reconverge, max_restarts);
    if (do_anneal == UT_TRUE)
      mix_em_anneal_alloc_pp(initdata, my_numrows, numcols, means, covars, 
                             probs, weights, class, k_best, threshold, 
                             min_weight, min_diag, numtries, 
                             max_reconverge, max_restarts, UT_TRUE);
    else
      mix_em_alloc_pp(initdata, my_numrows, numcols, means, covars, probs, 
                      weights, class, k_best, threshold, min_weight, 
                      min_diag, numtries, max_reconverge, max_restarts, 
                      UT_TRUE, means_already_initialized );
  }

  /*----------------------------------------------------------------------------*/
  /* Done */
  /* report final EM parameters */

  if (do_xval == UT_TRUE)
    K = k_best;
  else 
    k_best = K;

  if (pe == 0)
    {
      /* open file for writing */
      params_fp = fopen(paramsfile, "w");
      if (params_fp == (FILE*) NULL) {
        printf("Error in trying to open a file.\n");
        return (UT_ERROR);
      }

      fprintf(params_fp, "EM clustering of file %s...\n\n", datafile);
      fprintf(params_fp, "EM mixture model parameters\n");
      fprintf(params_fp, "---------------------------\n\n");

      if (do_norm == UT_TRUE)
        print_unnorm_gauss_parms_set(params_fp, numcols, k_best, means, covars, 
                                     range, minval);
      else  
        print_gauss_parms_set(params_fp, numcols, k_best, means, covars);

      fprintf(params_fp,"\nclass weights\n");
      print_drow(params_fp, k_best, weights);
    } 

  log_likelihood = mixture_likelihood_pp(K, means, covars, weights,
                                         initdata, probs, prob_data,
                                         sum_probs, my_numrows, numcols,
                                         "rows", min_diag, pe, numPE);
      
  if (pe == 0)
    fprintf(params_fp, "log-likelihood of model = %g\n",log_likelihood);

  if (pe==0) {
    printf("%d: after reporting final em params\n",pe);
    fflush(stdout);
  }

  /* labels file */
  write_icol_cascade_pp(labelsfile, my_numrows, class, "w", pe, numPE);
    
  /* record probability of each classification */
  for (i = 1; i <= my_numrows; i++)
    class_prob[i] = probs[class[i]][i];
      
  /* probabilities file */
  write_dcol_cascade_pp(probsfile, my_numrows, class_prob, "w", pe, numPE);

  /* no need to do extra work like this - not particularly useful */
  /*
    if (do_xval == UT_TRUE)
    for (k = 1; k < k_best; k++) {
    mix_em_alloc_pp(initdata, my_numrows, numcols, means, covars, probs, 
    weights, class, k, threshold, min_weight, min_diag, 
    numtries, max_reconverge, max_restarts, UT_FALSE);
    sprintf(stringk, "%d", k);
    strcpy(new_labelsfile, labelsfile);
    strcat(new_labelsfile, stringk);
    write_icol_cascade_pp(new_labelsfile, my_numrows, class, "w", pe, 
    numPE);
    }
  */


  if (pe==0) {
    printf("%d: after writing labels file\n",pe);
    fflush(stdout);
  }

  /*----------------------------------------------------------------------------*/
  /* clean up memory */
  if (do_norm == UT_TRUE) {
    NR_free_dvector(minval, 1, numcols);
    NR_free_dvector(maxval, 1, numcols);
    NR_free_dvector(range, 1, numcols);
  }

  NR_free_ivector(class, 1, my_numrows);
  NR_free_dvector(class_prob, 1, my_numrows);

  NR_free_dmatrix(probs, 1, K_max, 1, my_numrows);
  NR_free_dvector(prob_data, 1, my_numrows);
  NR_free_dvector(sum_probs, 1, K_max);

  NR_free_dvector(weights, 1, K_max);
  NR_free_dmatrix(means, 1, K_max, 1, numcols);
  free_set_of_dmatrices(covars, K_max, numcols, numcols);
  free_set_of_dmatrices(best_covars, K_max, numcols, numcols);

  NR_free_dmatrix(other_covar_k,1,numcols,1,numcols);
  NR_free_dvector(otherweights,1,K_max);

  NR_free_dmatrix(initdata, 1, my_numrows, 1, numcols);

  if (pe==0) {
    printf("%d: after freeing memory\n",pe);
    fflush(stdout);
  }

  /* close all files */
  /*
    fclose(log_fp);
  */
  if (pe == 0)
    fclose(params_fp);

  if (pe==0) {
    printf("%d: after closing files\n",pe);
    fflush(stdout);
  }

  DA_finalize_pp();

  return(UT_OK);
}

/*******************************************************************************
 INITIALIZE_ARGS
 Initialize structures that store command-line argument information.
*******************************************************************************/
void initialize_args(arg args [])
{
  Init_Arg(args,1, "i",     FALSE, 1, "input data matrix file");
  Init_Arg(args,2, "p",      TRUE, 1, "output Gaussian parameters file");
  Init_Arg(args,3, "l",      TRUE, 1, "output log file");
  Init_Arg(args,4, "c",      TRUE, 1, "output cluster labels file");
  Init_Arg(args,5, "rows",  FALSE, 1, "number of rows of input matrix");
  Init_Arg(args,6, "cols",  FALSE, 1, "number of columns of input matrix");
  Init_Arg(args,7, "k",     FALSE, 1, "maximum number of clusters to find");
  Init_Arg(args,8, "seed",   TRUE, 1, "random seed to use");
  Init_Arg(args,9, "mindiag",TRUE, 1, "minimum value on covariance diagonal");
  Init_Arg(args,10,"minwgt", TRUE, 1, "minimum class weight");
  Init_Arg(args,11,"thresh", TRUE, 1, "convergence threshold");
  Init_Arg(args,12,"tries",  TRUE, 1, "number of optimization attempts");
  Init_Arg(args,13,"collapse",TRUE,0, "okay to collapse clusters");
  Init_Arg(args,14,"trials", TRUE, 1, "number of cross validation trials");
  Init_Arg(args,15,"loglev", TRUE, 1, "log print level");
  Init_Arg(args,16,"nreconv",TRUE, 1, "maximum num. reconvergence attempts");
  Init_Arg(args,17,"nrestart",TRUE,1, "maximum num. restart attempts");
  Init_Arg(args,18,"channels",TRUE,1, "number of i/o channels to use");
  Init_Arg(args,19,"pca",     TRUE,0, "perform dimensionality reduction");
  Init_Arg(args,20,"maxdim",  TRUE,1, "maximum number of dimensions");
  Init_Arg(args,21,"probs",   TRUE,1, "output label probabilities file");
  Init_Arg(args,22,"anneal",  TRUE,0, "perform deterministic annealing");
  Init_Arg(args,23,"xval",    TRUE,0, "perform monte carlo x-validation");
  Init_Arg(args,24,"nonorm",  TRUE,0, "don't normalize the data along dims");
  Init_Arg(args,25,"initmeans",TRUE, 1, "ininital means file");
  return;
}









