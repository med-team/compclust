/* mix_em_pp.h */

#ifndef lint
static char mix_em_pp_hdr_rcsid[] = "$Id: mix_em_pp.h,v 1.2 2001/12/05 00:47:59 hart Exp $";
#endif
/* 
 * $Log: mix_em_pp.h,v $
 * Revision 1.2  2001/12/05 00:47:59  hart
 * Modifications to allow users to specify the exact locations to initialize
 * cluster means.  This is in effort to build in an ability to chain FullEM as
 * post clustering clusteirng algo.
 *
 * Revision 1.1.1.1  2001/05/21 23:36:19  diane
 * Import of various mls & woldlab source packages
 *
 * */

/* constants */

/* function declarations */

int mix_em_alloc_pp(double **data, int numrows, int numcols, double **means, 
	            double ***covars, double **probs, double *weights, 
		    int *class, int K, double threshold, double min_weight,
		    double min_diag, int numtries, int max_reconverge, 
		    int max_restarts, boolean empty_cluster_ok, boolean means_alread_initialized);

int mix_em_anneal_alloc_pp(double **data, int numrows, int numcols, 
		           double **means, double ***covars, double **probs, 
			   double *weights, int *class, int K, 
			   double threshold, double min_weight, 
			   double min_diag, int numtries, int max_reconverge, 
			   int max_restarts, boolean empty_cluster_ok);

int crossval_mix_em_alloc_pp(double **data, int numrows, int numcols, 
		             double **means, double ***covars, double **probs, 
			     double *weights, int *class, int K_max, 
			     int numtrials, int *k_best, double threshold, 
			     double min_weight, double min_diag, int numtries, 
			     int max_reconverge, int max_restarts);

int min_dist_class_assign_pp(double **data, int numrows, int numcols, 
		             double **means, int *clustsize, int *class, 
			     int K, int pe, int numPE);

int cond_alloc_dmat(double **A, int nr, int nc, double *cond);

int generate_rand_covar_dmat(double **covar, int num_dims);
