/*****************************************************************************
 *
 * per_feature.c
 *
 * Normalizes the data to the range [0.0, 1.0] on each of the dimensions.
 * The will remove any scale-dependent behavior
 *
 ****************************************************************************/

#include "per_feature.h"
#include "nr_util.h"

void per_feature(double **data, int datapoints, int features)
{
  double *range, *max, *min, *drow;
  int i,j;


  max = NR_dvector(1, features);
  min = NR_dvector(1, features);
  range = NR_dvector(1, features);

  /**
   *
   * Find the max/min for each vector component
   *
   */

  for (j = 1; j <= features; j++)
    max[j] = min[j] = data[1][j];

  for (i = 2; i <= datapoints; ++i) {
    drow = data[i];
    for (j = 1; j <= features; ++j) {
      if (drow[j] > max[j])
        max[j] = drow[j];
      else if (drow[j] < min[j])
        min[j] = drow[j];
    }
  }
   
  /**
   *
   * compute the range vector
   *
   **/

  for (j = 1; j <= features; j++) {
    range[j] = (max[j] - min[j]);
    
    /**
     *
     * Catch the case where min == max --> one data value
     *
     **/

    if (range[j] == 0.0)
      range[j] = 1.0;
  }

  /**
   *
   * Now normalize each data vector
   *
   **/

  for (i = 1; i <= datapoints; i++) {
    drow = data[i];
    for (j = 1; j <= features; j++) {
      drow[j] = (drow[j] - min[j]) / range[j];
    }
  }

  /**
   *
   * Free the temporary variables
   *
   **/

  NR_free_dvector(range, 1, features);
  NR_free_dvector(min, 1, features);
  NR_free_dvector(max, 1, features);
}
