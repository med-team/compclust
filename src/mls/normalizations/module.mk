CURDIR := $(MLSDIR)/normalizations

NORMSRC := $(wildcard $(CURDIR)/*.c)
LIBSRC += $(NORMSRC)
CFLAGS += -I$(CURDIR)

# local customization
NORM := $(CURDIR)/libnorm$(LIBEXT)
LIBDIRS += -L$(dir $(NORM))
TARGETLIBS += $(LIBRARY)

$(NORM): $(NORMSRC:.c=$(LIBOBJEXT))
#	libtool --mode=link gcc $(CFLAGS) -c $@ $?
	$(AR) rv $@ $?
	$(call bless_library,$@)
