#ifndef __EUCLIDEAN_NORM_H
#define __EUCLIDEAN_NORM_H

void euclidean_norm(double **data, int datapoints, int features);

#endif
