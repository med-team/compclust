/*****************************************************************************
 *
 * euclidean_norm.c
 *
 * Normalize each vector of the dataset to its euclidean norm, i.e. make
 * every vector a unit vector
 *
 *****************************************************************************/

#include "euclidean_norm.h"
#include "nr_util.h"
#include "euclidean.h"

void euclidean_norm(double **data, int datapoints, int features)
{
  int i, j;
  double *drow, *origin, magnitude;

  origin = NR_dvector(1, features);
  for (j = 1; j <= features; j++)
    origin[j] = 0.0;

  for (i = 1; i <= datapoints; i++) {
    drow = data[i];
    magnitude = euclidean(origin, drow, features);
    for (j = 1; j <= features; j++)
      drow[j] /= magnitude;
  }
}
