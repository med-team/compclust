#include <stdio.h>
#include <math.h>
#include "log_transform.h"

void log_transform(double **data, int datapoints, int features, double parm) 
{
  int i, j;
  double val, log_base;

  if (parm <= 1.0) {
    fprintf(stderr, "Cannot transform to a log base <= 1.0\n");
    return;
  }

  log_base = log(parm);

  for (i = 1; i <= datapoints; i++) {
    for (j = 1; j <= features; j++) {
      val = data[i][j];
      if (val <= 0.0)
        fprintf(stderr,"Cannot take the log of a number <= 0.0\n");
      else
        data[i][j] = log(val) / log_base;
    }
  }
}
