/****************************************************************************
 *
 * mean_norm.c
 *
 * Subtracts the mean of the data from the data.  Eliminates the first
 * moment.  This is a first step to the hypersphere normalization
 *
 ****************************************************************************/

#include "mean_norm.h"
#include "nr_util.h"

void mean_norm(double **data, int datapoints, int features)
{
  double *mean, *drow;
  int i, j;

  mean = NR_dvector(1, features);

  /**
   *
   * Find the mean for each vector component
   *
   */

  for (j = 1; j <= features; j++)
    mean[j] = data[1][j];

  for (i = 2; i <= datapoints; ++i) {
    drow = data[i];
    for (j = 1; j <= features; ++j) {
      mean[j] += drow[j];
    }
  }

  for (j = 1; j <= features; j++)
    mean[j] /= (double) datapoints;


  /**
   *
   * Subtract the mean from each datapoint
   *
   **/

  for (i = 1; i <= datapoints; i++) {
    drow = data[i];
    for (j = 1; j <= features; j++) {
      drow[j] -= mean[j];
    }
  }

  /**
   *
   * Free the allocated data
   *
   **/

  NR_free_dvector(mean, 1, features);
}
