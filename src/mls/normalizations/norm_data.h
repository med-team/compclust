#ifndef __NORM_DATA_H
#define __NORM_DATA_H

enum {
  NO_NORM = 1,          /* Do nothing ot the data                       */
  LOG_TRANSFORM,        /* Log-transform the data                       */
  PER_FEATURE_NORM,     /* Normalize to [0,1] on a per-feature basis    */
  HYPERSPHERE_NORM,     /* Normalize to the unit hypersphere, for
                           psudo-correlation                            */
  EUCLIDEAN_NORM,       /* force each vector to a unit vector           */
  MEAN_NORM,            /* subtract the mean from the dataset           */
  PCA_VAR,              /* PCA, capture variance                        */
  PCA_DIM,              /* PCA, keep top dimensions                     */
  ICA                   /* Independent Component Analysis               */
};

void norm_data(double **data, int datapoints, int features, int type,
               double parm);

#endif
