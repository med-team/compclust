
#include <stdio.h>
#include "log_transform.h"
#include "per_feature.h"
#include "mean_norm.h"
#include "euclidean_norm.h"
#include "norm_data.h"

void norm_data(double **data, int datapoints, int features, int type,
               double parm)
{
  switch(type) {
  case NO_NORM:
    break;
    
  case LOG_TRANSFORM:

    /***
     *
     * For LOG_NORM, parm is the log base to transform to
     *
     ***/

    log_transform(data, datapoints, features, parm);
    break;

  case PER_FEATURE_NORM:
    per_feature(data, datapoints, features);
    break;

  case MEAN_NORM:
    mean_norm(data, datapoints, features);
    break;

  case EUCLIDEAN_NORM:
    euclidean_norm(data, datapoints, features);
    break;

  case HYPERSPHERE_NORM:
    mean_norm(data, datapoints, features);
    euclidean_norm(data, datapoints, features);
    break;

  case PCA_VAR:

    /***
     *
     * For PCA_VAR, parm is the percent of variance to capture [0.0, 1.0]
     *
     ***/

    break;

  case PCA_DIM:

    /***
     *
     * For PCA_DIM, parm is the number of top dimensions to keep
     *
     ***/

    break;

  case ICA:
    fprintf(stderr, "ICA is not implemented\n");
    break;

  default:
    fprintf(stderr,"Unknown normalization method: %d\n", type);
    break;
  }
}
