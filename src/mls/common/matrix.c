/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * @author Ben Bornstein
 */

#include <math.h>
#include "matrix.h"

/**
 * Divides each value in the given MxN matrix by its row's Euclidean norm.
 *
 * In the case of Pearson Correlation distance metric, this must be done
 * (the means are normalized to the unit sphere).  Then, standard Euclidean
 * distance can be used as though it were Pearson Correlation.
 */

void normalize_double_matrix(double **matrix, int M, int N)
{
  double euclidean_norm;
  int    r, c;


  for (r = 1; r <= M; r++)
  {
    euclidean_norm = 0.0;

    for (c = 1; c <= N; c++)
    {
      euclidean_norm += (matrix[r][c] * matrix[r][c]);
    }

    euclidean_norm = sqrt(euclidean_norm);

    for(c = 1; c <= N; c++)
    {
      matrix[r][c] /= euclidean_norm;
    }
  }
}


/**
 * Divides each value in the given MxN matrix by its row's Euclidean norm.
 *
 * In the case of Pearson Correlation distance metric, this must be done
 * (the means are normalized to the unit sphere).  Then, standard Euclidean
 * distance can be used as though it were Pearson Correlation.
 */
void normalize_long_double_matrix(long double **matrix, int M, int N)
{
  long double euclidean_norm;
  int    r, c;


  for (r = 1; r <= M; r++)
  {
    euclidean_norm = 0.0;

    for (c = 1; c <= N; c++)
    {
      euclidean_norm += (matrix[r][c] * matrix[r][c]);
    }

    euclidean_norm = sqrt(euclidean_norm);

    for(c = 1; c <= N; c++)
    {
      matrix[r][c] /= euclidean_norm;
    }
  }
}


/**
 * Zeros the contents of the given 1xN matrix of doubles.
 */
void zero_1D_double_matrix(double *matrix, int N)
{
  int i;


  for (i = 1; i <= N; i++)
  {
    matrix[i] = 0.0;
  }
}


/**
 * Zeros the contents of the given 1xN matrix of integers.
 */
void zero_1D_int_matrix(int *matrix, int N)
{
  int i;


  for (i = 1; i <= N; i++)
  {
    matrix[i] = 0;
  }
}


/**
 * Zeros the contents of the given 1xN matrix of long doubles.
 */
void zero_1D_long_double_matrix(long double *matrix, int N)
{
  int i;


  for (i = 1; i <= N; i++)
  {
    matrix[i] = 0.0;
  }
}



/**
 * Zeros the contents of the given MxN matrix.
 */
void zero_2D_double_matrix(double **matrix, int M, int N)
{
  int i, j;
 

  for (i = 1; i <= M; i++)
  {
    for (j = 1; j <= N; j++)
    {
      matrix[i][j] = 0.0;
    }
  }
}


/**
 * Zeros the contents of the given MxN matrix.
 */
void zero_2D_long_double_matrix(long double **matrix, int M, int N)
{
  int i, j;
 

  for (i = 1; i <= M; i++)
  {
    for (j = 1; j <= N; j++)
    {
      matrix[i][j] = 0.0;
    }
  }
}
