CURDIR := $(MLSDIR)/common

COMMONSRC := $(wildcard $(CURDIR)/*.c)
LIBSRC += $(COMMONSRC)
CFLAGS += -I$(CURDIR)

# local customization
COMMON = $(CURDIR)/libcommon$(LIBEXT)
TARGET_LIBS += $(COMMON)
LIBDIRS += -L$(dir $(COMMON))

$(COMMON): $(COMMONSRC:.c=$(LIBOBJEXT))
#	libtool --mode=link gcc $(CFLAGS) -c $@ $?
	$(AR) rv $@ $?
	$(call bless_library,$@)
