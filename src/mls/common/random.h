/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 */

/* Andres and Becky 5/18/96 */

#if !defined _RANDOM_H_
#define	_RANDOM_H_

#define	IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define NTAB 32
#define NDIV (1+(IM-1)/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

float	ran1(long *idum);
float 	expdev(long *idum);
float 	gasdev(long *idum);
float 	gamdev(int ia, long *idum);
float 	poidev(float xm, long *idum);
float 	bnldev(float pp, int n, long *idum);
int	iran(int min, int max, long *idum);

float	gammln_pll(float xx);
float 	ran1_pll(long *idum,long *iy,long *iv);
float 	expdev_pll(long *idum, long *iy, long *iv);
float 	gasdev_pll(long *idum, long *iy, long *iv, int *iset, float *gset);
float	gammadev_pll(float a, long *idum, long *iy, long *iv);
float	poidev_pll(float xm, long *idum, long *iy, long *iv,
           float *sq, float *alxm, float *g, float *oldm);
float	bnldev_pll(float pp, int n, long *idum, long *iy, long *iv,
           int *nold, float *pold, float *pc, float *plog,
	   float *pclog, float *en,float *oldg);
int 	iran_pll(int min, int max, long *idum, long *iy, long *iv);
float   gamdev_pll(int ia, long *idum, long *iy, long *iv);


long    seed();

/**
 * Returns a random double between min and max (inclusive) using
 * ran1(idum) as the source of the uniform deviates.
 *
 * @see iran, ran1
 *
 * @author Ben Bornstein
 * @date   Wednesday, January 24, 2001, 8:46a
 */
double dran(double min, double max, long *idum);

#endif
