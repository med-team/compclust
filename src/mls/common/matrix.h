/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * @author Ben Bornstein
 */

#ifndef __MATRIX_H
#define __MATRIX_H

/**
 * Divides each value in the given MxN matrix by its row's Euclidean norm.
 *
 * In the case of Pearson Correlation distance metric, this must be done
 * (the means are normalized to the unit sphere).  Then, standard Euclidean
 * distance can be used as though it were Pearson Correlation.
 */
void normalize_double_matrix(double **matrix, int M, int N);

/**
 * Divides each value in the given MxN matrix by its row's Euclidean norm.
 *
 * In the case of Pearson Correlation distance metric, this must be done
 * (the means are normalized to the unit sphere).  Then, standard Euclidean
 * distance can be used as though it were Pearson Correlation.
 */
void normalize_long_double_matrix(long double **matrix, int M, int N);

/**
 * Zeros the contents of the given 1xN matrix of doubles.
 */
void zero_1D_double_matrix(double *matrix, int N);

/**
 * Zeros the contents of the given 1xN matrix of integers.
 */
void zero_1D_int_matrix(int *matrix, int N);

/**
 * Zeros the contents of the given 1xN matrix of long doubles.
 */
void zero_1D_long_double_matrix(long double *matrix, int N);

/**
 * Zeros the contents of the given MxN matrix.
 */
void zero_2D_double_matrix(double **matrix, int M, int N);

/**
 * Zeros the contents of the given MxN matrix.
 */
void zero_2D_long_double_matrix(long double **matrix, int M, int N);


#endif
