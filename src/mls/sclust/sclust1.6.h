/**************************************************************
                 
  File:     sclust1.6.h
  Title:    clustering header file 
  Author:   Becky Castano 
  Date:     1/24/01  - modifed from nssclust8.h 
  Modified:

**************************************************************/

#if !defined _SCLUST_1_6_
#define _SCLUST_1_6_

typedef struct {
  float **data_array;     /* X - data array */
  int   n_points;         /* N - number of data vectors */
  int	  n_features;       /* D - number of elements in each data vector */
  int   n_clusters;       /* K */
  float   mu;
  float   sigma;
  float *control_vector;
  float  beta;
  float betamin;
  float betamax;
  float beta_factor;
  float Mthreshfac;  /* used in sparse M structure */
  float commitrew;
  int slack_flag;
  long int seed;
   /* results */
  double  **membership_probs;
  float **means;
  float **vars;
  float  var;
} sclust_params;

typedef struct {
    float **data_array;
    int   n_data_points;
    int   n_points;
    int   n_features;
} data_struct;

typedef struct {
    float **means;
    float var;
    float **vars;
    int   n_clusters;
    int   n_features;
    double  *sums;
} model_struct;

typedef struct {
  float *beta;
  float betamin;
  float betamax;
  float beta_factor;
  float Mthreshfac;
  float commit_reward;
  int slack_flag;
  long int seed;
} control_struct;

typedef struct {
    int *ijM;        /* index to sparse membership probability matrix */ 
    double *sM;      /* sparse membership probability matrix */ 
    double **M;      /* membership probability matrix */ 
    int n_points;
    int n_clusters;
    int n_cols;  /* n_clusters + slack_flag */
} mem_prob_struct;

typedef struct {
    float *x;
    model_struct *model;
    float *energy_params;
    float *control; 
} mem_prob_energy_func_args_struct;

/* void sclust (sclust_params *params); */
sclust_params  *allocate_sclust_params(
      int datapoints, 
      int features,
      int clusters,
      float p,
      float mu,
      float sigma);

/* old structures */

typedef struct {
    float **data_array;       /* X - data array */
    int	  n_samples;         /* N - number of data vectors */
    int	  n_features;        /* D - number of elements in each data vector */
    int   n_clusters;        /* K */
    float   p;
    float   mu;
    float *control_vector;
} data_in_struct;

typedef struct {
  double  **cluster_membership_probs;
  float **means;
  float **vars;
  int  history;
  int n_classes;
} data_out_struct;

/*
void sclust (data_in_struct in_data, data_out_struct *out_data);
void sclust (sclust_params *cl_params);
*/
void sclust (data_struct *data,sclust_params cl_params,
             data_out_struct *out_data);


/*  
 Suggestion by Joe was to have four parameters to pass to sclust
  (1) input data
         data_array
	 n_samples
	 n_features
  (2) control information
         n_clusters
	 p
	 m
	 control_vector
  (3) output data ( membership probabilities that are dependent on input data)
         cluster_membership_probs
	 likelihoods
  (4) model information ( parameters for the estimated models )
         means
	 stds

*/


void  UpdateMeanEstimates( model_struct *model,
              mem_prob_struct *mem_probs,
              data_struct *data, control_struct *control);
void  UpdateVarEstimates( model_struct    *model,
                          mem_prob_struct *mem_probs, 
                          data_struct     *data,
                          control_struct  *control);
void  UpdateMemProbEstimates( model_struct    *model,
                              mem_prob_struct *mem_probs, 
                              data_struct     *data,
                              control_struct  *control);
void MSparseToFull(mem_prob_struct *mem_probs);
int Initialize( data_struct *data, model_struct *model, 
                mem_prob_struct *mem_probs, 
                control_struct *control_params, 
                sclust_params *params,
                data_out_struct *out_data);

#endif

