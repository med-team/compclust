/**************************************************************
                 
  Function:  CrossValidate.c
  Author:   Becky Castano
  Date:     11/19/98
  Purpose: Determine the optimal number of clusters for a given
            data set using cross validation.   Clustering is performed
	    using a number of different cluster numbers.  For each
	    number of clusters, a likelihood for the data coming from
	    the estimated model is determined.   The number of clusters
	    with the highest likelihood on test data is considered to be the 
	    most likely 

  Parameters:
    cross_v params
      input
        data       - array of data elements
        n_points   - number of data sample points
        n_features - number of elements associated with each data point
       
        max_clusters - the maximum number of clusters to consider
        min_clusters 
        cluster_step_size 
        n_runs       - number of times to partition the data set into a test
                         set and a training set 
        n_samples    - number of times to run sclust for each partition of the
                         data set 
        random_seed  - a seed for the random number generator 

      result variables
        classification  - vector with the final class label for each data point
        likelihoods     - vector with the average max log likelihood 
                            for each number of clusters tried
        final_n_classes - the number of classes selected as optimal
        classification_likelihood - log likelihood of the classfication result
        
    clustering params

  Returns:   none 

  Modifications:  2/12/2001  BC  - updated for sclust rather than em

***************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
//#include <values.h>  /* mindouble */
#include <limits.h>
#include <float.h>
#include "cross_v.h"
#include "array.h"
#include "bc_err.h"

void  CrossValidate(struct cross_v_params *cv_params, 
                    sclust_params *clustering_params)
{
  int nc_idx,run_idx,sample_idx;
  int n_classes;
  int pnt_idx, class_idx;
  int max_clusters,min_clusters,cluster_step_size;
  int n_runs,n_samples,n_test,n_train,n_features,n_points;
  data_struct *train_data, *test_data, *full_data;
  double likelihood, ***train_likelihood, ***test_likelihood;
  data_out_struct cluster_out;
  char message[MESSAGELENGTH];

  double max_prob;
  int max_class;

  /* initialize */
  max_clusters = cv_params->max_clusters;
  min_clusters = cv_params->min_clusters;
  cluster_step_size = cv_params->cluster_step_size;
  n_points= cv_params->n_points;
  n_runs = cv_params->n_runs;
  n_samples = cv_params->n_samples;
  n_test = n_points/2; 
  n_train = n_points - n_test;
  n_features = cv_params->n_features;
  srand48(cv_params->random_seed);
  full_data = (data_struct *)calloc(1,sizeof(data_struct));
  train_data = (data_struct *)calloc(1,sizeof(data_struct));
  test_data = (data_struct *)calloc(1,sizeof(data_struct));
  full_data->n_data_points = n_points;
  full_data->n_features = n_features;
  full_data->data_array = cv_params->data;
  train_data->n_data_points = n_train;
  test_data->n_data_points = n_test;
  test_data->n_features = n_features;
  train_data->n_features = n_features;
  train_data->data_array = fmatrix(0,n_train,0,n_features);
  test_data->data_array  = fmatrix(0,n_test,0,n_features);
  cluster_out.cluster_membership_probs = dmatrix(0,n_points,0,max_clusters);
  cluster_out.means = fmatrix(0,max_clusters,0,n_features);
  cluster_out.vars = fmatrix(0,max_clusters,0,n_features);
  train_likelihood = d3Dmatrix(0,n_runs,0,max_clusters,0,n_samples);
  test_likelihood = d3Dmatrix(0,n_runs,0,max_clusters,0,n_samples);
  clustering_params->n_points = n_train;
  clustering_params->n_features = n_features;
  cv_params->classification = ivector(0,n_points);
  cv_params->likelihoods = dvector(min_clusters,max_clusters);

  sprintf(message, "Error: cluster_step_size is %d (must be greater than 1)",cluster_step_size);
  assert(cluster_step_size > 0 ,message);
  sprintf(message, "Error: min_clusters (%d) is more than max_clusters (%d)", min_clusters,max_clusters); 
  assert(min_clusters<=max_clusters,message);

  sprintf(message, "n_runs %d n_samples %d\n",n_runs,n_samples);
  debuglog(message);
  sprintf(message, "cluster_step_size %d \n",cluster_step_size);
  debuglog(message);

  for (run_idx = 0; run_idx < n_runs; run_idx++)
  {
    for (nc_idx = min_clusters; nc_idx <= max_clusters; nc_idx +=
         cluster_step_size)
    {
      DivideData(full_data,train_data,test_data);
      clustering_params->data_array = train_data->data_array;
      clustering_params->n_clusters = nc_idx;
      cluster_out.n_classes = nc_idx;
      for (sample_idx = 0; sample_idx < n_samples; sample_idx++)
      {
        sclust(train_data,*clustering_params,&cluster_out);

        likelihood = ComputeLikelihood(train_data, &cluster_out);
        train_likelihood[run_idx][nc_idx][sample_idx] = likelihood;
        likelihood = ComputeLikelihood(test_data, &cluster_out);
        test_likelihood[run_idx][nc_idx][sample_idx] = likelihood;

        sprintf(message, "test_likelihood %g ",likelihood);
        debuglog(message);
      }
    }
  }

  /* pick num clusters with max likelihood */
  n_classes = FindBestNumClasses(test_likelihood,cv_params); 

  /* compute results  for selected num clusters*/
  cluster_out.n_classes = n_classes;
  clustering_params->n_clusters = n_classes;
  sclust(full_data,*clustering_params,&cluster_out);
  likelihood = ComputeLikelihood(full_data, &cluster_out);

  /* compile results */
  cv_params->classification_likelihood = likelihood;
  cv_params->final_n_classes = n_classes;

  for (pnt_idx=0; pnt_idx < n_points; pnt_idx++)
  {
    max_prob = cluster_out.cluster_membership_probs[pnt_idx][0];
    max_class = 0;
    for (class_idx=1; class_idx < n_classes; class_idx++)
    {
      if (max_prob < cluster_out.cluster_membership_probs[pnt_idx][class_idx])
      {
        max_prob =cluster_out.cluster_membership_probs[pnt_idx][class_idx];
        max_class = class_idx;
      }
    }
    cv_params->classification[pnt_idx] = max_class;
  }

#if 0
  /* print membership probs */
  for (pnt_idx=0; pnt_idx < n_points; pnt_idx++)
  {
    fprintf(stderr,"data[%d]  ",pnt_idx);
    for (class_idx=0; class_idx < n_classes; class_idx++)
    {
      fprintf(stderr,"%6.2f \t", 
         cluster_out.cluster_membership_probs[pnt_idx][class_idx]);
    }
    fprintf(stderr,"\n");
  }
#endif

  free_dmatrix(cluster_out.cluster_membership_probs,0,n_points,0,max_clusters);
  free_fmatrix(cluster_out.means,0,max_clusters,0,n_features);
  free_fmatrix(cluster_out.vars,0,max_clusters,0,n_features);
  free_fmatrix(train_data->data_array,0,n_train,0,n_features);
  free_fmatrix(test_data->data_array,0,n_test,0,n_features);
  free_d3Dmatrix(train_likelihood,0,n_runs,0,max_clusters,0,n_samples);
  free_d3Dmatrix(test_likelihood,0,n_runs,0,max_clusters,0,n_samples);
  free((data_struct *)full_data);
  free((data_struct *)train_data);
  free((data_struct *)test_data);

}


/**************************************************************
  Function: DivideData 
  Author:   Becky  Castano
  Date:     11/19/98
  Purpose:  Randomly split an array of points into two sets of
             a specified size

  Modified:
  Returns:  none 
***************************************************************/

void DivideData(data_struct *data, data_struct *data1, data_struct *data2)
{
  int n_points = data->n_data_points;
  int n_features = data->n_features;
  int n_points1 = data1->n_data_points;
  int n_points2 = data2->n_data_points;

  int pnt_idx, pnt1_idx = 0, pnt2_idx = 0;
  int j;

  for (pnt_idx=0; pnt_idx < n_points; pnt_idx++)
  {
    if (  (lrand48() % 2 && pnt1_idx < n_points1) 
       ||   pnt2_idx >= n_points2 )
    {  
      for ( j=0; j < n_features; j++)
        data1->data_array[pnt1_idx][j] = data->data_array[pnt_idx][j];
      pnt1_idx++;
    }  
    else
    {  
      for ( j=0; j < n_features; j++)
        data2->data_array[pnt2_idx][j] = data->data_array[pnt_idx][j];
      pnt2_idx++;
    }  
  }
}


/**************************************************************
                 
  Function:  ComputeLikelihood
  Author:    Becky  Castano
  Date:     11/19/98
  Purpose:  Calculate the likelihood of a set of data occurring
            given a mixure of Gaussians model.  

  Modified: February 1, 2001
  Returns:   the log of the likelihood of the data 

***************************************************************/

double ComputeLikelihood(data_struct *data, data_out_struct *model)
{
  int pnt_idx, c_idx, f_idx;
  double likel;
  double ll=0;
  double pxj, diff,var;
  double c = 1.0/(sqrt(2.0*M_PI));
  int  n_features = data->n_features;
  int  n_points = data->n_data_points;
  int  n_classes = model->n_classes; 
  char message[MESSAGELENGTH];
  float  pj = 1.0/(float)n_classes;  /* uniform priors */
  double val;

  for (pnt_idx =0 ; pnt_idx < n_points; pnt_idx++)
  {
    likel = 0;
    for (c_idx =0; c_idx < n_classes; c_idx++)
    {
      pxj = 0.0;
      for (f_idx = 0; f_idx < n_features; f_idx++)
      {
        diff = data->data_array[pnt_idx][f_idx] - model->means[c_idx][f_idx];
        var = model->vars[c_idx][f_idx];
	val = c*exp(- (diff * diff)/ (2.0*var))/sqrt(var);
	if (val > 1.0)  /* not sure if this is the best solution */
	  val = 1.0;
        if ( pxj > 0.0 )
          pxj *=  val;
        else
          pxj  =  val; 
       }
       likel += pxj*pj; 
    }
    if ( likel > 1.0 )
    {
      sprintf(message,"ComputeLikelihood: Warning likelihood = %f > 1",likel);
      debuglog(message);
    }

    /* this is where small numbers become a problem */
    if (likel < DBL_MIN )  
      likel = DBL_MIN;
    ll += log( likel );
  }

  /* want to check for neg inf, but isinf does not seem to be universal */
  /*  if ( isnan(ll) || isinf(ll)) */
  if ( isnan(ll) ) 
  {
    sprintf(message,"ComputeLikelihood: Warning loglikelihood %f, setting to max negative\n",ll);
    debuglog(message);
    ll = -DBL_MAX;
  }
  return ll;
}


/**************************************************************
                 
  Function:  FindBestNumClasses
  Author:    Becky  Castano
  Date:     11/19/98
  Purpose:  Determine the most likely number of classes based on 
            clustering likelihood results for a variety of numbers of
            clusters.  The best number of classes is the one s.t.
            the mean over the number of runs of the max
            of the test likelihoods is maximized) 

  Modified: February 1, 2001
  Returns:   the number of classes with the highest average max likelihood 

***************************************************************/

int  FindBestNumClasses(double ***test_likelihood,
 cross_v_struct *cv_params)
{
  /* int cluster_idx; */
  int run_idx, sample_idx;
  int min_clusters = cv_params->min_clusters;
  int max_clusters = cv_params->max_clusters;
  int cluster_step_size = cv_params->cluster_step_size;
  int n_runs = cv_params->n_runs;
  int n_samples = cv_params->n_samples;
  double run_sum  = 0;
  int k;
  double max_likelihood;
  int    best_clusters = min_clusters;
  double best_clusterlikelihood = - DBL_MAX;

  /*for each cluster number, find the sum of the max of the
                samples, which is proportional to the mean*/
  for(k = min_clusters; k <= max_clusters; k+=cluster_step_size)
  {
    run_sum = 0;
    for( run_idx = 0; run_idx < n_runs; run_idx++)
    {
      max_likelihood = test_likelihood[run_idx][k][0];
      for (sample_idx=1; sample_idx < n_samples; sample_idx++)
        if ( max_likelihood < test_likelihood[run_idx][k][sample_idx])
          max_likelihood = test_likelihood[run_idx][k][sample_idx];
      run_sum += max_likelihood; 
    }
    cv_params->likelihoods[k] = run_sum/n_runs;

    if(run_sum > best_clusterlikelihood)
    {
      best_clusterlikelihood = run_sum;
      best_clusters = k;
    }
  }
  return best_clusters;
}

