/* last modified: may 25/96 */

#ifndef _ARRAY_H_
#define _ARRAY_H_

#ifndef ANSI
#if defined (__STDC__)
#define ANSI
#endif
#endif



void	data_type_info();

/* <>VECTOR */

signed char    		*scvector(int nl, int nh);
unsigned char	*ucvector(int nl, int nh);
short   		*svector(int nl, int nh);
int     		*ivector(int nl, int nh);
unsigned long   *ulvector(int nl, int nh);
float   		*fvector(int nl, int nh);
double  		*dvector(int nl, int nh);

/* FREE_<>VECTOR */

void    free_scvector(signed char *v, int nl, int nh);
void	free_ucvector(unsigned char *v, int nl, int nh);
void    free_svector(short *v, int nl, int nh);
void    free_ivector(int *v, int nl, int nh);
void    free_ulvector(unsigned long *v, int nl, int nh);
void	free_fvector(float *v, int nl, int nh);
void	free_dvector(double *v, int nl, int nh);

/* FILL_<>VECTOR */

void    fill_scvector(signed char *v, int nl, int nh, signed char val);
void    fill_ucvector(unsigned char *v, int nl, int nh, unsigned char val);
void    fill_svector(short *v, int nl, int nh, short val);
void    fill_ivector(int *v, int nl, int nh, int val);
void    fill_ulvector(unsigned long *v, int nl, int nh, unsigned long val);
void    fill_fvector(float *v, int nl, int nh, float val);
void    fill_dvector(double *v, int nl, int nh, double val);

/* READ_BIN_<>VECTOR */

void    read_bin_scvector(signed char *v, int nl, int nh, char *file);
void    read_bin_ucvector(unsigned char *v, int nl, int nh, char *file);
void    read_bin_svector(short *v, int nl, int nh, char *file);
void    read_bin_ivector(int *v, int nl, int nh, char *file);
void    read_bin_ulvector(unsigned long *v, int nl, int nh, char *file);
void    read_bin_fvector(float *v, int nl, int nh, char *file);
void	read_bin_dvector(double *v, int nl, int nh, char *file);

/* WRITE_BIN_<>VECTOR */

void    write_bin_scvector(signed char *v, int nl, int nh, char *file);
void    write_bin_ucvector(unsigned char *v, int nl, int nh, char *file);
void    write_bin_svector(short *v, int nl, int nh, char *file);
void    write_bin_ivector(int *v, int nl, int nh, char *file);
void    write_bin_ulvector(unsigned long *v, int nl, int nh, char *file);
void    write_bin_fvector(float *v, int nl, int nh, char *file);
void    write_bin_dvector(double *v, int nl, int nh, char *file);

/* READ_TEXT_<>VECTOR */

void    read_text_scvector(signed char *v, int nl, int nh, char *file);
void    read_text_ucvector(unsigned char *v, int nl, int nh, char *file);
void    read_text_svector(short *v, int nl, int nh, char *file);
void    read_text_ivector(int *v, int nl, int nh, char *file);
void    read_text_ulvector(unsigned long *v, int nl, int nh, char *file);
void    read_text_fvector(float *v, int nl, int nh, char *file);
void	read_text_dvector(double *v, int nl, int nh, char *file);

/* WRITE_TEXT_<>VECTOR */

void    write_text_scvector(signed char *v, int nl, int nh, char *file);
void    write_text_ucvector(unsigned char *v, int nl, int nh, char *file);
void    write_text_svector(short *v, int nl, int nh, char *file);
void    write_text_ivector(int *v, int nl, int nh, char *file);
void    write_text_ulvector(unsigned long *v, int nl, int nh, char *file);
void    write_text_fvector(float *v, int nl, int nh, char *file);
void    write_text_dvector(double *v, int nl, int nh, char *file);

/* PRINT_<>VECTOR */

void    print_scvector(signed char *v, int nl, int nh);
void    print_ucvector(unsigned char *v, int nl, int nh);
void    print_svector(short *v, int nl, int nh);
void    print_ivector(int *v, int nl, int nh);
void    print_ulvector(unsigned long *v, int nl, int nh);
void    print_fvector(float *v, int nl, int nh);
void    print_dvector(double *v, int nl, int nh);

/* <>MATRIX */

signed char    		**scmatrix(int rl, int rh, int cl, int ch);
unsigned char		**ucmatrix(int rl, int rh, int cl, int ch);
short   		**smatrix(int rl, int rh, int cl, int ch);
int     		**imatrix(int rl, int rh, int cl, int ch);
float   		**fmatrix(int rl, int rh, int cl, int ch);
double  		**dmatrix(int rl, int rh, int cl, int ch);

/* FREE_<>MATRIX */

void    free_scmatrix(signed char **m, int rl, int rh, int cl, int ch);
void	free_ucmatrix(unsigned char **m, int rl, int rh, int cl, int ch);
void    free_smatrix(short **m, int rl, int rh, int cl, int ch);
void    free_imatrix(int **m, int rl, int rh, int cl, int ch);
void	free_fmatrix(float **m, int rl, int rh, int cl, int ch);
void	free_dmatrix(double **m, int rl, int rh, int cl, int ch);

/* FILL_<>MATRIX */

void    fill_scmatrix(signed char **m, int rl, int rh, int cl, int ch, signed char val);
void    fill_ucmatrix(unsigned char **m, int rl, int rh, int cl, int ch, unsigned char val);
void    fill_smatrix(short **m, int rl, int rh, int cl, int ch, short val);
void    fill_imatrix(int **m, int rl, int rh, int cl, int ch, int val);
void    fill_fmatrix(float **m, int rl, int rh, int cl, int ch, float val);
void    fill_dmatrix(double **m, int rl, int rh, int cl, int ch, double val);

/* PRINT_<>MATRIX */

void    print_scmatrix(signed char **a, int rl, int rh, int cl, int ch);		
void	print_ucmatrix(unsigned char **a, int rl, int rh, int cl, int ch);
void    print_smatrix(short **a, int rl, int rh, int cl, int ch);
void    print_imatrix(int **a, int rl, int rh, int cl, int ch);
void    print_fmatrix(float **a, int rl, int rh, int cl, int ch);
void    print_dmatrix(double **a, int rl, int rh, int cl, int ch);

/* READ_BIN_<>MATRIX */

void    read_bin_scmatrix(signed char **m, int rl, int rh, int cl, int ch, char *file);
void	read_bin_ucmatrix(unsigned char **m, int rl, int rh, int cl, int ch, char *file);
void    read_bin_smatrix(short **m, int rl, int rh, int cl, int ch, char *file);
void    read_bin_imatrix(int **m, int rl, int rh, int cl, int ch, char *file);
void    read_bin_fmatrix(float **m, int rl, int rh, int cl, int ch, char *file);
void    read_bin_dmatrix(double **m, int rl, int rh, int cl, int ch, char *file);

/* WRITE_BIN_<>MATRIX */

void    write_bin_scmatrix(signed char **m, int rl, int rh, int cl, int ch, char *file);
void	write_bin_ucmatrix(unsigned char **m, int rl, int rh, int cl, int ch, char *file);
void    write_bin_smatrix(short **m, int rl, int rh, int cl, int ch, char *file);
void    write_bin_imatrix(int **m, int rl, int rh, int cl, int ch, char *file);
void    write_bin_fmatrix(float **m, int rl, int rh, int cl, int ch, char *file);
void    write_bin_dmatrix(double **m, int rl, int rh, int cl, int ch, char *file);

/* <>3DMATRIX */

signed char	***sc3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh);
unsigned char   ***uc3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh);
short           ***s3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh);
int             ***i3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh);
float           ***f3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh);
double          ***d3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh);

/* FREE_<>3DMATRIX */

void    free_sc3Dmatrix(signed char ***m, int rl, int rh, int cl, int ch, int dl, int dh);
void    free_uc3Dmatrix(unsigned char ***m, int rl, int rh, int cl, int ch, int dl, int dh);
void    free_s3Dmatrix(short ***m, int rl, int rh, int cl, int ch, int dl, int dh);
void    free_i3Dmatrix(int ***m, int rl, int rh, int cl, int ch, int dl, int dh);
void    free_f3Dmatrix(float ***m, int rl, int rh, int cl, int ch, int dl, int dh);
void    free_d3Dmatrix(double ***m, int rl, int rh, int cl, int ch, int dl, int dh);



#endif
