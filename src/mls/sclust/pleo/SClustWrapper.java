// import gov.nasa.jpl.aig.Pleo.*;
import mls.pleo.*;
import mls.pleo.io.*;

import java.io.*;
import java.text.*;
import java.util.*;


/**
 * Pleo wrapper for the cv_sclust program.
 *
 * @author Ben Bornstein
 */
public class SClustWrapper
{
  static final boolean DEBUG = false;

  /**
   * Field Name Constants
   */
  static final String FIELD_ANNOTATIONS       = "annotations";
  static final String FIELD_COLNAMES          = "colnames";
  static final String FIELD_CHILD             = "Class_";
  static final String FIELD_LIKELIHOOD        = "likelihood";
  static final String FIELD_ROWNAMES          = "rownames";
  static final String FIELD_SCLUST_PARAMETERS = "sclust_parameters";
  static final String FIELD_SUBCLUSTERS       = "subclusters";
  static final String FIELD_VALUES            = "values";

  /**
   * Field Name Constants: SClust Parameters
   */
  static final String FIELD_SC_BETA_FACTOR    = "beta_update_factor";
  static final String FIELD_SC_BETA_MAX       = "betamax";
  static final String FIELD_SC_BETA_MIN       = "betamin";
  static final String FIELD_SC_MU             = "commit_reward";
  static final String FIELD_SC_M_DELTA_FACTOR = "membership_threshold_factor";
  static final String FIELD_SC_SEED           = "seed";
  static final String FIELD_SC_SLACK_FLAG     = "use_slack_class";

  /**
   * Field Name Constants: Monte-Carlo Cross-Validation Parameters
   */
  static final String FIELD_MCCV_LIKELIHOODS  = "mccv.likelihoods";
  static final String FIELD_MCCV_MIN_CLUSTERS = "mccv.minClusters";
  static final String FIELD_MCCV_MAX_CLUSTERS = "mccv.maxClusters";
  static final String FIELD_MCCV_STEP_SIZE    = "mccv.stepSize";
  static final String FIELD_MCCV_RUNS         = "mccv.runs";
  static final String FIELD_MCCV_SAMPLES      = "mccv.samples";

  /**
   * Field Name Constants: Heirarchical Clustering
   */
  static final String FIELD_HC_CLUSTER_MAX_SIZE = "hc.clusterMaxSize";
  static final String FIELD_HC_DO_HC            = "hc.doHC";
  static final String FIELD_HC_STOP_CRITERIA    = "hc.stopCriteria";

  /**
   * Heirarchical Clustering Stop Criteria Constants
   */
  static final int      HC_STOP_CRITERIA_LIKELIHOOD = 0;
  static final int      HC_STOP_CRITERIA_SIZE       = 1;
  static final String[] HCStopCriteria = { "Likelihood", "ClusterSize" };

  /**
   * File Constants
   */
  static final String INPUT_FILE_PREFIX  = "sclust_input_";
  static final String INPUT_FILE_SUFFIX  = ".tmp";
  static final String OUTPUT_FILE_PREFIX = "sclust_output_";
  static final String OUTPUT_FILE_SUFFIX = ".tmp";

  /**
   * Property Constants
   */
  static final String PROPERTY_COMMAND      = "command";
  static final String PROPERTY_POLL_STREAMS = "pollStreams";

  /**
   * Type Constants
   */
  static final String TYPE_GENE_EXPRESSION_ARRAY = "gene_expression_array";
  static final String TYPE_SCLUST_PARAMETERS     = "sclust_parameters";

  static final String TYPE_ROW_CLUSTERED_GENE_EXPRESSION_ARRAY =
    "row_clustered_gene_expression_array";

  /**
   * Default Values: SClust
   */
  static final double  DEFAULT_SC_BETA_FACTOR    =  1.001;
  static final double  DEFAULT_SC_BETA_MAX       = 10.0;
  static final double  DEFAULT_SC_BETA_MIN       =  0.01;
  static final double  DEFAULT_SC_MU             = 10.0;
  static final double  DEFAULT_SC_M_DELTA_FACTOR =  0.01;
  static final double  DEFAULT_SC_SEED           = 42.0;
  static final boolean DEFAULT_SC_SLACK_FLAG     = false;

  /**
   * Default Values: Monte-Carlo Cross-Validation (MCCV)
   */
  static final int DEFAULT_MCCV_MIN_CLUSTERS =  1;
  static final int DEFAULT_MCCV_MAX_CLUSTERS = 10;
  static final int DEFAULT_MCCV_STEP_SIZE    =  1;
  static final int DEFAULT_MCCV_RUNS         = 10;
  static final int DEFAULT_MCCV_SAMPLES      =  3;

  /**
   * Default Values: Heirarchical Clustering
   */
  static final int     DEFAULT_HC_CLUSTER_MAX_SIZE = 30;
  static final boolean DEFAULT_HC_DO_HC            = false;
  static final int     DEFAULT_HC_STOP_CRITERIA    = HC_STOP_CRITERIA_SIZE;

  /**
   * Default Values: Miscellaneous
   */
  static final String  DEFAULT_COMMAND = "sclust";


  /**
   * Main
   */
  public static void main(String[] args)
  {
    if (args.length != 2)
    {
      System.err.print("usage: ");
      System.err.print("SClustWrapper <input pleo file> <output pleo file>");
      System.err.println();

      System.exit(1);
    }


    String        pleoInputFilename  = args[0];
    String        pleoOutputFilename = args[1];
    Vector        inputPleoInstances = null;
    Instance      outputPleoInstance = null;
    SClustWrapper wrapper            = new SClustWrapper();

    try
    {
      FileInputStream stream = new FileInputStream( pleoInputFilename );
      PleoReader      reader = new PleoReader( stream );

      inputPleoInstances = reader.readPleoData();
      stream.close();

      outputPleoInstance = wrapper.run( inputPleoInstances );

      FileWriter     fw  = new FileWriter(pleoOutputFilename);
      BufferedWriter bw  = new BufferedWriter(fw);
      PrintWriter    out = new PrintWriter(bw);

      PleoWriter.write(out, outputPleoInstance, false);

      out.close();
    }
    catch(FileNotFoundException e)
    {
      wrapper.fatalError("Could not find file " + pleoInputFilename);
    }
    catch(mls.pleo.io.ParseException e)
    {
      wrapper.fatalError("ParseException: " + e.getMessage());
    }
    catch(Exception e)
    {
      e.printStackTrace( System.err );
      wrapper.fatalError("Exception: " + e.getMessage());
    }
  }


  /**
   *
   */
  public Instance act(Instance instance)
  {
    return act(instance, 0);
  }


  /**
   *
   */
  public Instance act(Instance instance, int level)
  {
    File     inputFile      = null;
    File     outputFile     = null;
    String   inputFilename  = null;
    String   outputFilename = null;
    String   command        = null;


    readGeneExpressionArray( instance );
    sanityCheckParameters();

    inputFile  = createTempFile( INPUT_FILE_PREFIX , INPUT_FILE_SUFFIX  );
    outputFile = createTempFile( OUTPUT_FILE_PREFIX, OUTPUT_FILE_SUFFIX );

    inputFilename  = inputFile .getAbsolutePath();
    outputFilename = outputFile.getAbsolutePath();

    command = createCommandLine( inputFilename, outputFilename );

    writeInputFile( inputFile );

    log ( "act():    level: {0}",
          new Object[] { new Integer(level) } );

    log ( "act(): numDatum: {0}",
          new Object[] { new Integer(dataRowNames.length) } );

    log ( "act(): running " + command );
    exec( command );

    return createPleoFromLabels( outputFile );
  }


  /**
   *
   */
  public Instance actHierarchically(Instance instance, int level)
  {
    Instance result = null;


    //
    //  TYPE_GENE_EXPRESSION_ARRAY or
    //  TYPE_ROW_CLUSTERED_GENE_EXPRESSION_ARRAY
    //
    if ( TYPE_GENE_EXPRESSION_ARRAY.equals( instance.getType() ) )
    {
      result = actHierarchicallyOnExternalNode( instance, ++level );
    }
    else
    {
      result = actHierarchicallyOnInternalNode( instance, ++level );
    }

    return result;
  }


  /**
   *
   */
  Field actHierarchically(Field field, int level)
  {
    Instance instance = (Instance) field.getValue().getValue();
    Field    result   = new Field();
    Value    value    = new Value();


    result.setName( field.getName() );

    value .setValue( actHierarchically( instance, level ) );
    result.addValue( value );

    return result;
  }


  /**
   *
   */
  Instance actHierarchicallyOnExternalNode(Instance instance, int level)
  {
    Instance      result  = instance;
    Field         field   = null;
    SClustWrapper wrapper = null;


    if ( shouldCluster( instance ) )
    {
      wrapper = (SClustWrapper) clone();
      result  = wrapper.act( instance, level );

      if ( hasSubclusters( result ) )
      {
        result = actHierarchically( result, level );
      }
      else
      {
        field  = result.getField( FIELD_CHILD + '0' );
        result = (Instance) field.getValue().getValue();
      }
    }

    return result;
  }


  /**
   *
   */
  Instance actHierarchicallyOnInternalNode(Instance instance, int level)
  {
    Instance result = new Instance();
    Field[]  fields = instance.getFields();
    String   name   = null;

    int       i         = 0;
    final int numFields = fields.length;


    result.setType( TYPE_ROW_CLUSTERED_GENE_EXPRESSION_ARRAY );

    for (i = 0; i < numFields; i++)
    {
      name = fields[i].getName();

      if ( (name != null) && name.startsWith( FIELD_CHILD ) )
      {
        result.addField( actHierarchically( fields[i], level ) );
      }
      else
      {
        result.addField( fields[i] );
      }
    }

    return result;
  }


  /**
   *
   */
  void addSubclusters(Instance   rowClusteredGeneExpressionArray,
                      Collection subclusters )
  {
    Instance instance = null;
    Field    field    = null;
    Value    value    = null;
    Iterator iter     = null;

    int childIndex     = 0;
    int numSubclusters = subclusters.size();


    rowClusteredGeneExpressionArray.addField(
      FieldUtilities.makeField( numSubclusters, FIELD_SUBCLUSTERS )
    );

    iter = subclusters.iterator();

    try
    {
      while ( iter.hasNext() )
      {
        instance = (Instance) iter.next();
        field    = new Field();
        value    = new Value();

        field.setName( FIELD_CHILD + childIndex );

        field.addValue( value    );
        value.setValue( instance );

        rowClusteredGeneExpressionArray.addField( field );
        childIndex++;
      }
    }
    catch (NoSuchElementException e)
    {
      fatalError( e.getMessage() );
    }
  }


  /**
   *
   */
  public Object clone()
  {
    SClustWrapper wrapper = new SClustWrapper();


    wrapper.hcClusterMaxSize = this.hcClusterMaxSize;
    wrapper.hcDoHC           = this.hcDoHC;
    wrapper.hcStopCriteria   = this.hcStopCriteria;

    wrapper.mccvMinClusters = this.mccvMinClusters;
    wrapper.mccvMaxClusters = this.mccvMaxClusters;
    wrapper.mccvStepSize    = this.mccvStepSize;
    wrapper.mccvRuns        = this.mccvRuns;
    wrapper.mccvSamples     = this.mccvSamples;

    wrapper.scBetaFactor   = this.scBetaFactor;
    wrapper.scBetaMin      = this.scBetaMin;
    wrapper.scBetaMax      = this.scBetaMax;
    wrapper.scBetaMu       = this.scBetaMu;
    wrapper.scMDeltaFactor = this.scMDeltaFactor;
    wrapper.scMu           = this.scMu;
    wrapper.scSeed         = this.scSeed;
    wrapper.scSlackFlag    = this.scSlackFlag;

    wrapper.dataAnnotations = null;
    wrapper.dataColNames    = null;
    wrapper.dataRowNames    = null;
    wrapper.dataValues      = null;

    return wrapper;
  }


  /**
   * @return a new BufferedReader, wrapped around the given InputStream.
   */
  BufferedReader createBufferedReader(InputStream is)
  {
    return new BufferedReader( new InputStreamReader(is) );
  }


  /**
   * Constructs and returns the command-line for sclust.
   *
   * The sclust command itself is obtained from the Java system property
   * PROPERTY_COMMAND.
   */
  String createCommandLine(String inputFilename, String outputFilename)
  {
    StringBuffer buffer  = new StringBuffer();
    String       command = null;


    command = System.getProperty( PROPERTY_COMMAND, DEFAULT_COMMAND );

    buffer.append( command ).append( ' ' );
    buffer.append( inputFilename ).append( ' ' ).append( outputFilename );

    return buffer.toString();
  }


  /**
   * @return a new Instance of type TYPE_GENE_EXPRESSION_ARRAY with the
   * given objectID.  
   *
   * This method is used to help construct Pleodata output while processing
   * raw sclust output.
   */
  Instance createGeneExpressionArray(String objectID)
  {
    Instance i = new Instance();


    i.setType( TYPE_GENE_EXPRESSION_ARRAY );
    i.setObjectID( objectID );

    i.addField( FieldUtilities.makeField( dataColNames, FIELD_COLNAMES ) );
    i.addField( FieldUtilities.makeField( new Vector(), FIELD_ROWNAMES ) );
    i.addField( FieldUtilities.makeField( new Vector(), FIELD_VALUES   ) );

    if (dataAnnotations != null)
    {
      i.addField( FieldUtilities.makeField( new Vector(), FIELD_ANNOTATIONS ) );
    }

    return i;
  }


  /**
   * Creates and returns a temporary file, with the given prefix and
   * suffix, ready for reading and/or writing.  The temporary file will be
   * deleted (is flagged for deletion) when the wrapper exits.
   *
   * @return the File
   */
  File createTempFile(String prefix, String suffix)
  {
    File file = null;

    try
    {
      file = File.createTempFile(prefix, suffix, null);
    }

    catch (IllegalArgumentException e)
    {
      //
      // Thrown only if the first argument to createTempFile() is a string
      // with less than three characters.
      //
      fatalError( e.getMessage() );
    }

    catch (IOException e)
    {
      //
      // Thrown only if the file could not be created.
      //
      fatalError( e.getMessage() );
    }

    catch (SecurityException e)
    {
      //
      // Thrown only if a SecurityManager is in place which will not allow
      // writing to a file.
      //
      fatalError( e.getMessage() );
    }


    file.deleteOnExit();

    return file;
  }


  /**
   * Given an sclust output filename this method proccess the output into
   * Pleo instances and returns the resulting instances.
   */
  Instance createPleoFromLabels(File outputFile)
  {
    Double    value             = null;
    Hashtable pleoInstanceTable = null;
    Instance  instance          = null;
    Instance  result            = null;
    String[]  columns           = null;
    String    line              = null;
    String    objectID          = null;

    double    likelihood      = Double.NaN;
    double[]  mccvLikelihoods = null;
    int       i               = 0;
    int       numClusters     = 0;
    int       row             = 0;
    final int numRows         = dataRowNames.length;

    // log( "createPleoFromLabels(): numRows: [{0}]",
    //      new Object[] { new Integer(numRows) } );


    numClusters       = (mccvMaxClusters - mccvMinClusters + 1) / mccvStepSize;
    mccvLikelihoods   = new double[ numClusters ];
    pleoInstanceTable = new Hashtable();
    result            = createRowClusteredGeneExpressionArray( "root" );

    try
    {
      BufferedReader stream = new BufferedReader( new FileReader(outputFile) );

      //
      // Skip Header Line
      //
      line = stream.readLine();

      for (row = 0; row < numRows; row++)
      {
        line     = stream.readLine();
        objectID = "class" + line;

        //
        // If a TYPE_GENE_EXPRESSION_ARRAY Instance has not yet
        // been created for this class (label), create one.
        //
        if ( !pleoInstanceTable.containsKey( objectID ) )
        {
          instance = createGeneExpressionArray( objectID );
          pleoInstanceTable.put( objectID, instance );
        }
        else
        {
          instance = (Instance) pleoInstanceTable.get( objectID );
        }

        populateGeneExpressionArray( instance, row );
      }

      //
      // Skip Comment Line
      //
      line = stream.readLine();

      //
      // Final Classification Likelihood
      //
      try
      {
        line       = stream.readLine();
        value      = Double.valueOf( line );
        likelihood = value.doubleValue();
      }
      catch (NumberFormatException e)
      {
        log("Warning: Final classification likelihood: [{0}] is not a double.",
            new Object[] { line } );
      }

      //
      // Skip Comment Line
      //
      line = stream.readLine();

      //
      // MCCV Likelihoods
      //
      for (i = 0; i < numClusters; i++)
      {
        try
        {
          line    = stream.readLine();
          columns = mls.util.Parse.delimitedStringToArray( line, ' ' );

          value              = Double.valueOf( columns[1] );
          mccvLikelihoods[i] = value.doubleValue();
        }
        catch (NumberFormatException e)
        {
          log("Warning: MCCV Likelihood: [{0}] is not a double.",
              new Object[] { line } );
        }
      }

      result.addField(
        FieldUtilities.makeField( likelihood, FIELD_LIKELIHOOD ) );

      result.addField(
        FieldUtilities.makeField( mccvMinClusters, FIELD_MCCV_MIN_CLUSTERS ) );

      result.addField(
        FieldUtilities.makeField( mccvMaxClusters, FIELD_MCCV_MAX_CLUSTERS ) );

      result.addField(
        FieldUtilities.makeField( mccvStepSize, FIELD_MCCV_STEP_SIZE ) );

      result.addField(
        FieldUtilities.makeField( mccvLikelihoods, FIELD_MCCV_LIKELIHOODS ) );
      
      stream.close();
    }
    catch (FileNotFoundException e)
    {
      fatalError( e.getMessage() );
    }
    catch (IOException e)
    {
      fatalError( e.getMessage() );
    }

    addSubclusters( result, pleoInstanceTable.values() );

    return result;
  }


  /**
   *
   */
  Instance createRowClusteredGeneExpressionArray(String objectID)
  {
    Instance i = new Instance();


    i.setType( TYPE_ROW_CLUSTERED_GENE_EXPRESSION_ARRAY );
    i.setObjectID( objectID );

    return i;
  }


  /**
   * Executes the given (sclust) command as a separate subprocess,
   * blocking until the subprocess is finished.  The standard output and
   * standard error streams of the subprocess are re-directed to
   * System.out.
   *
   * @return the exit status of the subprocess.
   */
  int exec(String command)
  {
    int     status      = 0;
    boolean done        = false;
    boolean pollStreams = false;
    Process process     = null;


    pollStreams = Boolean.getBoolean( PROPERTY_POLL_STREAMS );

    try
    {
      process = Runtime.getRuntime().exec( command );

      if (pollStreams)
      {
        BufferedReader stdout = null;
        BufferedReader stderr = null;

        stdout = createBufferedReader( process.getInputStream() );
        stderr = createBufferedReader( process.getErrorStream() );

        while (!done)
        {
          while ( stdout.ready() ) log("sclust: " + stdout.readLine());
          while ( stderr.ready() ) log("sclust: " + stderr.readLine());

          try
          {
            status = process.exitValue();
            done   = true;
          }
          catch (IllegalThreadStateException e) { }
        }

        stdout.close();
        stderr.close();
      }
      else
      {
        try
        {
          status = process.waitFor();
        }
        catch (InterruptedException e)
        {
          fatalError( e.getMessage() );
        }
      }
    }
    catch (IOException e)
    {
      fatalError( e.getMessage() );
    }

    return status;
  }


  /**
   * Outputs message to System.err and exits with the default status.
   */
  void fatalError(String message)
  {
    fatalError(message, null, 1);
  }


  /**
   * Outputs message to System.err and exits with status.
   */
  void fatalError(String message, int status)
  {
    fatalError(message, null, status);
  }


  /**
   * Outputs message to System.err and exits with the default status.
   * Message and arguments are passed through MessageFormat.format() before
   * output.
   */
  void fatalError(String message, Object[] arguments)
  {
    fatalError(message, arguments, 1);
  }


  /**
   * Outputs message to System.err and exits with the default status.
   * Message and arguments are passed through MessageFormat.format() before
   * output.
   */
  void fatalError(String message, Object[] arguments, int status)
  {
    log(message, arguments, System.err);
    log("Exiting...", null, System.err);

    System.exit(status);
  }


  /**
   * @return the value of the given Field as a boolean.  In this case, the
   * Field is specified by Instance and field name.
   *
   * If the Field specified by Instance and field name does not exist, or
   * the corresponding value cannot be interepeted as a boolean,
   * defaultValue is returned.
   */
  boolean getBoolean(Instance instance, String fieldName, boolean defaultValue)
  {
    boolean  result    = defaultValue;
    Object[] arguments = { fieldName, Boolean.FALSE };
    String   message   = null;


    if (defaultValue) arguments[1] = Boolean.TRUE;

    try
    {
      result = FieldUtilities.getBoolean(instance, fieldName);
    }
    catch (IllegalArgumentException e)
    {
      message = "Field {0} cannot be interpreted as a boolean value.";
    }
    catch (NoSuchPleoFieldException e)
    {
      message = "Field {0} does not exist.";
    }

    if (message != null)
    {
      log(message, arguments);
      log("{0} will default to \"{1}\".", arguments);
    }

    return result;
  }


  /**
   * @return the value of the given Field as a double.  The Field is
   * specified by Instance and field name.
   *
   * If the Field specified by Instance and field name does not exist, or
   * the corresponding value cannot be interepeted as an double, defaultValue
   * is returned.
   */
  double getDouble(Instance instance, String fieldName, double defaultValue)
  {
    double   result    = defaultValue;
    Object[] arguments = { fieldName, new Double(defaultValue) };
    String   message   = null;

    try
    {
      result = FieldUtilities.getDouble(instance, fieldName);
    }
    catch (IllegalArgumentException e)
    {
      message = "Field {0} cannot be interpreted as an integer value.";
    }
    catch (NoSuchPleoFieldException e)
    {
      message = "Field {0} does not exist.";
    }

    if (message != null)
    {
      log(message, arguments);
      log("{0} will default to {1}.", arguments);
    }

    return result;
  }


  /**
   * @return the first occurrence of a Pleo Instance in the given Instance
   * list whose type is TYPE_GENE_EXPRESSION_ARRAY.
   */
  Instance getGeneExpressionArray(Vector pleoInstanceList)
  {
    Object[] arguments = { TYPE_GENE_EXPRESSION_ARRAY };

    Instance result =
      PleoUtilities.getInstanceOfType( TYPE_GENE_EXPRESSION_ARRAY,
                                       pleoInstanceList );


    if (result == null)
    {
      fatalError("No Instance of type {0} found.", arguments);
    }

    return result;
  }


  /**
   * @return the FIELD_HC_STOP_CRITERIA parameter contained in a Pleo
   * instance of type TYPE_SCLUST_PARAMETERS.  One of:
   *
   *   1. HC_STOP_CRITERIA_LIKELIHOOD, or
   *   2. HC_STOP_CRITERIA_SIZE
   */
  int getHCStopCriteria(Instance parameters)
  {
    boolean found    = false;
    boolean list     = false;
    int     result   = DEFAULT_HC_STOP_CRITERIA;
    String  criteria = null;

    String   message   = null;
    Object[] arguments = { FIELD_HC_STOP_CRITERIA,
                           HCStopCriteria[ DEFAULT_HC_STOP_CRITERIA ] };


    try
    {
      criteria = FieldUtilities.getString( parameters, FIELD_HC_STOP_CRITERIA );

      for (int i = 0; (i < HCStopCriteria.length) && !found; i++)
      {
        if ( criteria.equalsIgnoreCase( HCStopCriteria[i] ) )
        {
          found  = true;
          result = i;
        }
      }

      if (!found)
      {
        list    = true;
        message = "Field {0} is not one of the following:";
          
      }
    }
    catch (IncompatiblePleoTypeException e)
    {
      message = "Field {0} is not a String.";
    }
    catch (NoSuchPleoFieldException e)
    {
      message = "Field {0} does not exist.";
    }

    if (message != null)
    {
      log(message, arguments);
      if (list) log( toCSV( HCStopCriteria ) );
      log("{0} will default to {1}.", arguments);
    }

    return result;
  }


  /**
   * @return the value of the given Field as an int.  The Field is
   * specified by Instance and field name.
   *
   * If the Field specified by Instance and field name does not exist, or
   * the corresponding value cannot be interepeted as an int, defaultValue
   * is returned.
   */
  int getInt(Instance instance, String fieldName, int defaultValue)
  {
    int      result    = defaultValue;
    Object[] arguments = { fieldName, new Integer(defaultValue) };
    String   message   = null;


    try
    {
      result = FieldUtilities.getInt(instance, fieldName);
    }
    catch (IllegalArgumentException e)
    {
      message = "Field {0} cannot be interpreted as an integer value.";
    }
    catch (NoSuchPleoFieldException e)
    {
      message = "Field {0} does not exist.";
    }

    if (message != null)
    {
      log(message, arguments);
      log("{0} will default to {1}.", arguments);
    }

    return result;
  }


  /**
   * @return the value of the field FIELD_SCLUST_PARAMETERS within the
   * given Instance.  If the value is an Object ID, it is dereferenced
   * using the scope of the given Pleo Instance list.
   *
   * The resulting Instance, if one exists, is guaranteed to be of type
   * TYPE_SCLUST_PARAMETERS, as this method exits with a fatalError()
   * otherwise.
   */
  Instance getSClustParameters(Instance geneExpressionArray,
                               Vector   pleoInstanceList)
  {
    Field    field    = null;
    Instance instance = null;
    String   objectId = null;
    Value    value    = null;

    Object[] arguments = { FIELD_SCLUST_PARAMETERS,
                           TYPE_SCLUST_PARAMETERS,
                           null };


    field = geneExpressionArray.containsField( FIELD_SCLUST_PARAMETERS );

    if (field == null)
    {
      fatalError("Field {0} not found.", arguments);
    }

    value = field.getValue();

    if ( value.isObjectID() )
    {
      objectId = (String) value.getValue();
      instance = PleoUtilities.getInstanceObject(objectId, pleoInstanceList);
    }
    else if ( value.isInstance() )
    {
      instance = (Instance) value.getValue();
    }
    else
    {
      fatalError("Field {0} is not an Object ID or an Instance.", arguments);
    }

    if (instance == null)
    {
      arguments[2] = objectId;

      fatalError("Field {0} has the Object ID {2} which could not be found.",
                 arguments);
    }

    if ( !TYPE_SCLUST_PARAMETERS.equals( instance.getType() ) )
    {
      fatalError("Field {0} is not an instance of type {1}.", arguments);
    }

    return instance;
  }


  /**
   *
   */
  boolean hasSubclusters(Instance instance)
  {
    boolean result         = false;
    int     numSubclusters = 0;
    Field   field          = instance.getField( FIELD_SUBCLUSTERS );


    if ( field != null )
    {
      numSubclusters = FieldUtilities.extractInt( field );

      if ( numSubclusters > 1 )
      {
        result = true;
      }
    }

    return result;
  }


  /**
   * Outputs message to System.out.
   */
  void log(String message)
  {
    log(message, null, System.out);
  }


  /**
   * Outputs message to the given PrintStream.
   */
  void log(String message, PrintStream out)
  {
    log(message, null, out);
  }


  /**
   * Outputs message to System.out.  Message and arguments are passed
   * through MessageFormat.format() before output.
   */
  void log(String message, Object[] arguments)
  {
    log(message, arguments, System.out);
  }


  /**
   * Outputs message to the given PrintStream.  Message and arguments are
   * passed through MessageFormat.format() before output.
   */
  void log(String message, Object[] arguments, PrintStream out)
  {
    String formatted = message;


    if (arguments != null)
    {
      formatted = MessageFormat.format(message, arguments);
    }

    out.println("SClustWrapper: " + formatted);
  }


  /**
   * Popultes the given Gene Expression Array Instance with data from row
   * of dataRowNames[], dataValues[] and, if applicable, dataAnnotations[].
   * 
   * This method is used to help construct Pleodata output while processing
   * raw sclust output.
   */
  void populateGeneExpressionArray(Instance geneExpressionArray, int row)
  { 
    Field  field  = null;
    Value  value  = null;
    Vector colVec = null;
    Vector rowVec = null;

    int       i       = 0;
    final int numCols = dataColNames.length;


    //
    // FIELD_ROWNAMES
    //
    field = geneExpressionArray.containsField( FIELD_ROWNAMES );

    if (field != null)
    {
      value  = field.getValue();
      rowVec = (Vector) value.getValue();

      value = new Value();
      value.setValue( dataRowNames[row] );

      rowVec.addElement( value );
    }

    //
    // FIELD_VALUES
    //
    field = geneExpressionArray.containsField( FIELD_VALUES );

    if (field != null)
    {
      value  = field.getValue();
      rowVec = (Vector) value.getValue();
      colVec = new Vector( numCols );

      for (i = 0; i < numCols; i++)
      {
        value = new Value();
        value.setValue( dataValues[row][i] );

        colVec.addElement( value );
      }

      value = new Value();
      value .setValue  ( colVec );
   
      rowVec.addElement( value  );
    }

    //
    // FIELD_ANNOTATIONS
    //
    if (dataAnnotations != null)
    {
      field = geneExpressionArray.containsField( FIELD_ANNOTATIONS );

      if (field != null)
      {
        value  = field.getValue();
        rowVec = (Vector) value.getValue();

        value = new Value();
        value.setValue( dataAnnotations[row] );
   
        rowVec.addElement( value );
      }
    }
  }


  /**
   * Reads the Gene Expression Array parameters from the given Instance and 
   * stores the results in the appropriate SClustWrapper variables.
   */
  void readGeneExpressionArray(Instance geneExpressionArray)
  {
    Field annotations = geneExpressionArray.containsField( FIELD_ANNOTATIONS );
    Field colnames    = geneExpressionArray.containsField( FIELD_COLNAMES    );
    Field rownames    = geneExpressionArray.containsField( FIELD_ROWNAMES    );
    Field values      = geneExpressionArray.containsField( FIELD_VALUES      );

    Object[] arguments = { null,
                           TYPE_GENE_EXPRESSION_ARRAY,
                           geneExpressionArray.getObjectID() };


         if (colnames == null) arguments[0] = FIELD_COLNAMES;
    else if (rownames == null) arguments[0] = FIELD_ROWNAMES;
    else if (values   == null) arguments[0] = FIELD_VALUES;

    if (arguments[0] != null)
    {
      fatalError("No field {0} in {1} with Object ID {2}.", arguments);
    }

    if (annotations != null)
    {
      dataAnnotations = FieldUtilities.extractStringArray( annotations );
    }

    dataColNames = FieldUtilities.extractStringArray  ( colnames   );
    dataRowNames = FieldUtilities.extractStringArray  ( rownames   );
    dataValues   = FieldUtilities.extractDouble2DArray( values     );
  }


  /**
   * Reads the SClust parameters from the given Instance and stores the
   * results in the appropriate SClustWrapper variables.
   */
  void readSClustParameters(Instance parameters)
  {
    scBetaMin =
      getDouble( parameters,
                 FIELD_SC_BETA_MIN,
                 DEFAULT_SC_BETA_MIN );

    scBetaMax =
      getDouble( parameters,
                 FIELD_SC_BETA_MAX,
                 DEFAULT_SC_BETA_MAX );

    scMu =
      getDouble( parameters,
                 FIELD_SC_MU,
                 DEFAULT_SC_MU );

    scSeed =
      getDouble( parameters,
                 FIELD_SC_SEED,
                 DEFAULT_SC_SEED );

    scSlackFlag =
      getBoolean( parameters,
                  FIELD_SC_SLACK_FLAG,
                  DEFAULT_SC_SLACK_FLAG );

    scBetaFactor =
      getDouble( parameters,
                 FIELD_SC_BETA_FACTOR,
                 DEFAULT_SC_BETA_FACTOR );

    scMDeltaFactor =
      getDouble( parameters,
                 FIELD_SC_M_DELTA_FACTOR,
                 DEFAULT_SC_M_DELTA_FACTOR );

    mccvMinClusters =
      getInt( parameters,
              FIELD_MCCV_MIN_CLUSTERS,
              DEFAULT_MCCV_MIN_CLUSTERS );

    mccvMaxClusters =
      getInt( parameters,
              FIELD_MCCV_MAX_CLUSTERS,
              DEFAULT_MCCV_MAX_CLUSTERS );

    mccvStepSize =
      getInt( parameters,
              FIELD_MCCV_STEP_SIZE,
              DEFAULT_MCCV_STEP_SIZE );

    mccvRuns =
      getInt( parameters,
              FIELD_MCCV_RUNS,
              DEFAULT_MCCV_RUNS );

    mccvSamples =
      getInt( parameters,
              FIELD_MCCV_SAMPLES,
              DEFAULT_MCCV_SAMPLES );

    hcClusterMaxSize =
      getInt( parameters,
              FIELD_HC_CLUSTER_MAX_SIZE,
              DEFAULT_HC_CLUSTER_MAX_SIZE );

    hcDoHC =
      getBoolean( parameters,
                  FIELD_HC_DO_HC,
                  DEFAULT_HC_DO_HC );

    hcStopCriteria =
      getHCStopCriteria( parameters );
  }


  /**
   * Runs cv_sclust.  Input data and parameters will be extracted from the
   * given pleoInstanceList.
   *
   * @return the results of cv_sclust, formatted as a single Pleo
   * Instance object.
   */
  public Instance run(Vector pleoInstanceList)
  {
    Instance array  = null;
    Instance params = null;
    Instance result = null;


    array  = getGeneExpressionArray( pleoInstanceList );
    params = getSClustParameters   ( array, pleoInstanceList );

    readSClustParameters( params );

    if ( hcDoHC )
    {
      result = actHierarchically( array, 0 );
    }
    else
    {
      result = act( array );
    }

    return result;
  }


  /**
   *
   */
  void sanityCheckParameters()
  {
    final int numValues = dataValues.length;
    Object[]  arguments = { FIELD_MCCV_MIN_CLUSTERS,
                            FIELD_MCCV_MAX_CLUSTERS,
                            new Integer( mccvMinClusters ),
                            new Integer( mccvMaxClusters ),
                            new Integer( numValues ) };


    if ( numValues < mccvMaxClusters )
    {
      log( "Field {1} ({3}) is < the number of datum ({4}).", arguments );
      log( "Field {1} ({3}) will be reset to {4}."          , arguments );

      mccvMaxClusters = numValues;
    }

    if ( numValues < mccvMinClusters )
    {
      log( "Field {0} ({2}) is < the number of datum ({4}).", arguments );
      log( "Field {0} ({2}) will be reset to {4}."          , arguments );

      mccvMinClusters = numValues;
    }
  }


  /**
   *
   */
  boolean shouldCluster(Instance instance)
  {
    int     size           = 0;
    int     numSubclusters = 0;
    boolean result         = false;
    Field   field          = null;
    Vector  vec            = null;


    if ( hcStopCriteria == HC_STOP_CRITERIA_LIKELIHOOD )
    {      
      field = instance.getField( FIELD_SUBCLUSTERS );

      if ( field != null )
      {
        numSubclusters = FieldUtilities.extractInt( field );
        if ( numSubclusters > 1 )
        {
          result = true;
        }
      }
      else
      {
        result = true;
      }
    }
    else if ( hcStopCriteria == HC_STOP_CRITERIA_SIZE )
    {
      field = instance.getField( FIELD_VALUES );
      vec   = (Vector) field.getValue().getValue();
      size  = vec.size();

      if ( size > hcClusterMaxSize )
      {
        result = true;
      }
    }

    return result;
  }


  /**
   * @return the given array of Strings as a single string of
   * comma-separated values.
   */
  String toCSV(String[] array)
  {
    int i;
    final int    length = array.length;
    StringBuffer buffer = new StringBuffer();


    buffer.append(array[0]);

    for (i = 1; i < length; i++)
    {
      buffer.append(',').append(' ').append(array[i]);
    }

    return buffer.toString();
  }


  /**
   * Writes cv_sclust input data to the given File.
   *
   * Before calling this method, initalize gene expression data by calling
   * readGeneExpressionArray().
   */
  void writeInputFile(File inputFile)
  {
    int row;
    int col;
    final int numRows = dataRowNames.length;
    final int numCols = dataColNames.length;


    try
    {
      FileWriter     fw     = new FileWriter( inputFile );
      BufferedWriter bw     = new BufferedWriter( fw );
      PrintWriter    stream = new PrintWriter   ( bw );

      stream.print(
        "# cross-validation parameters\n"                             +
        "#   n_points (rows of data), n_features (columns of data)\n" +
        "#   min_clusters, max_clusters, cluster_step_size\n"         +
        "#   n_runs, n_samples, random_seed\n"
      );

      stream.print  ( numRows );
      stream.print  ( ' ' );
      stream.println( numCols );

      stream.print  ( mccvMinClusters );
      stream.print  ( ' ' );
      stream.print  ( mccvMaxClusters );
      stream.print  ( ' ' );
      stream.println( mccvStepSize    );

      stream.print  ( mccvRuns    );
      stream.print  ( ' ' );
      stream.print  ( mccvSamples );
      stream.print  ( ' ' );
      stream.println( scSeed      );
      stream.println();

      stream.print(
        "# sclust parameters\n"                              +
        "#   mu  (commit reward)\n"                          +
        "#   betamin (1/max temperature)\n"                  +
        "#   betamax (1/min temperature)\n"                  +
        "#   beta_factor (annealing rate)\n"                 +
        "#   membership_threshold_factor (M_delta_thresh)\n" +
        "#   seed - can use seed for cv\n"                   +
        "#   slack_flag  (1 use slack class, 0 no slack class)\n"
      );

      stream.println( scMu                );
      stream.println( scBetaMin           );
      stream.println( scBetaMax           );
      stream.println( scBetaFactor        );
      stream.println( scMDeltaFactor      );
      stream.println( scSeed              );
      stream.println( scSlackFlag ? 1 : 0 );

      //
      // Data Values -- Do not print NaNs.
      //
      for (row = 0; row < numRows; row++)
      {
        for (col = 0; col < numCols; col++)
        {
          stream.print('\t');
          if (dataValues[row][col] == dataValues[row][col])
          {
            stream.print(dataValues[row][col]);
          }
        }
      
        stream.println();
      }

      stream.close();
    }
    catch (IOException e)
    {
      fatalError( e.getMessage() );
    }
  }


  int     hcClusterMaxSize;
  boolean hcDoHC;
  int     hcStopCriteria;

  int mccvMinClusters;
  int mccvMaxClusters;
  int mccvStepSize;
  int mccvRuns;
  int mccvSamples;

  double  scBetaFactor;
  double  scBetaMin;
  double  scBetaMax;
  double  scBetaMu;
  double  scMDeltaFactor;
  double  scMu;
  double  scSeed;
  boolean scSlackFlag;

  String[] dataAnnotations;
  String[] dataColNames;
  String[] dataRowNames;

  double[][] dataValues;
}
