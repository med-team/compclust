#include <stdio.h>
#include <stdlib.h>

void M_Transpose_to_Sparse(float **M,double *sM, int *ijM, int rows, int cols)
{
    int i,j,k;

    for (j=0; j < rows;j++)
        sM[j] = M[j][j];

    ijM[0] = rows+1;
    k = rows;
    for (i=0; i< rows;i++)
    {
        for (j=0; j < cols;j++)
	    if ( i != j )
	    {
	        k++;
	        sM[k] = M[j][i];
		ijM[k] = j;
	    }
        ijM[i+1] = k+1;
    }
}

void M_to_Sparse(float **M, double *sM, int *ijM, int rows, int cols)
{
    int i,j,k;

    for (j=0; j < rows;j++)
        sM[j] = M[j][j];

    ijM[0] = rows+1;
    k = rows;
    for (i=0; i< rows;i++)
    {
        for (j=0; j < cols;j++)
	    if ( i != j )
	    {
	        k++;
	        sM[k] = M[i][j];
		ijM[k] = j;
	    }
        ijM[i+1] = k+1;
    }
}

void PrintSparseM( double *sM, int *ijM, int nrows, int ncols)
{
    int i;
    int sM_length;

    sM_length = ijM[nrows]; 

    printf("    ");
    for (i=0; i < sM_length ; i++)
        printf( "%5d  ",i);  
    printf("\n");
    printf("sM  ");
    for (i=0; i < sM_length ; i++)
        printf( "%5.2f  ",sM[i]);  
    printf("\n");
    printf("ijM ");
    for (i=0; i < sM_length ; i++)
        printf( "%5d  ",ijM[i]);  
    printf("\n");
    printf("\n");

}

void Create_sM(double *sM, int *ijM, int rows, int cols)
{
    int i,j,k;

    for (j=0; j < rows;j++)
        sM[j] = (1.0 + 0.5*drand48() - 0.5)/((float)rows);  

    sM[rows] = 0;  /* this is an unused entry, initialize for completeness */

    ijM[0] = rows+1;
    k = rows;
    for (i=0; i< rows;i++)
    {
        for (j=0; j < cols;j++)
	    if ( i != j )
	    {
	        k++;
	        sM[k] = (1.0 + 0.5*drand48() - 0.5)/((float)rows);
		ijM[k] = j;
	    }
        ijM[i+1] = k+1;
    }
}

void UpdateSparsityStructure( double *sM, int *ijM, int rows, int cols, double thresh)
{
    int current_j, start_j;
    int i,j;

    current_j = ijM[0];
    start_j = ijM[0];
    for (i=0; i < rows; i++)   
    {
	if ( sM[i] < thresh )
	    sM[i] = 0.0;

        for(j= start_j; j < ijM[i+1]; j++) 
        {
            if ( sM[j] > thresh)
            {
                sM[current_j] = sM[j];
                ijM[current_j] = ijM[j];
                current_j++;
            }
        }
        start_j = ijM[i+1];
        ijM[i+1] = current_j;
    }
}

