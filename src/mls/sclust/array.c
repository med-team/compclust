/* last modified: may 30/96 */
/* Andres Castano */

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include "array.h"

#define	NR_END 1
#define FREE_ARG char*
int	max_num_per_row_in_vec = 4;

void
data_type_info()
{
	fprintf(stderr,"\n*** size of data types in bytes (and expected ANSI values) ***\n\n");
	fprintf(stderr,"signed char:\t%d\t(1: -128 -> 127)\n", (int)sizeof(signed char));
	fprintf(stderr,"unsigned char:\t%d\t(1: 0 -> 255)\n", (int)sizeof(unsigned char));
	fprintf(stderr,"short:\t\t%d\t(2: -32,767 -> 32,767)\n", (int)sizeof(short));
	fprintf(stderr,"int:\t\t%d\t(2: -32,768 -> 32,767)\n", (int)sizeof(int));
	fprintf(stderr,"unsigned int:\t%d\t(2: 0 -> 65,535)\n", (int)sizeof(unsigned int));
	fprintf(stderr,"long:\t\t%d\t(4: -2,147,483,648 -> 2,147,483,647)\n", (int)sizeof(long));
	fprintf(stderr,"unsigned long:\t%d\t(4: 0 -> 4,294,967,295)\n", (int)sizeof(unsigned long));
	fprintf(stderr,"float:\t\t%d\t(4, with 6 digits of precision)\n", (int)sizeof(float));
	fprintf(stderr,"double:\t\t%d\t(8 or 10, with 10 digits of precision)\n", (int)sizeof(double));
	fprintf(stderr,"long double:\t%d\t(10 or 12, with 10 digits of precision)\n\n", (int)sizeof(long double));
}


/* ************************  < >vector   ***********************
 *
 * a has to be declared as: <type> *a (e.g., int *a);
 * function call:  a = <>vector(nl, nh);
 * returns the vector a[nl..nh]
 */

signed char *
scvector(int nl, int nh)
{
	signed char	*v;

	v = (signed char *) malloc((unsigned) (nh - nl + 1 + NR_END) * sizeof(signed char));
	if (!v)
		fprintf(stderr,"allocation failure in scvector()");
	return v - nl + NR_END;
}


unsigned char *
ucvector(int nl, int nh)
{
	unsigned char	*v;

	v = (unsigned char *) malloc((unsigned) (nh - nl + 1 + NR_END) * sizeof(unsigned char));
	if (!v)
		fprintf(stderr,"allocation failure in ucvector()");
	return v - nl + NR_END;
}


short *
svector(int nl, int nh)
{
	short	*v;

	v = (short *) malloc((unsigned) (nh - nl + 1 + NR_END) * sizeof(short));
	if (!v)
		fprintf(stderr,"allocation failure in ucvector()");
	return v - nl + NR_END;
}


int *
ivector(int nl, int nh)
{
	int	*v;

	v = (int *) malloc((unsigned) (nh - nl + 1 + NR_END) * sizeof(int));
	if (!v)
		fprintf(stderr,"allocation failure in ivector()");
	return v - nl + NR_END;
}


unsigned long *
ulvector(int nl, int nh)
{
	unsigned long	*v;

	v = (unsigned long *) malloc((unsigned) (nh - nl + 1 + NR_END) * sizeof(unsigned long));
	if (!v)
		fprintf(stderr,"allocation failure in ulvector()");
	return v - nl + NR_END;
}


float *
fvector(int nl, int nh)
{
	float	*v;

	v = (float *) malloc((unsigned) (nh - nl + 1 + NR_END) * sizeof(float));
	if (!v)
		fprintf(stderr,"allocation failure in fvector()");
	return v - nl + NR_END;
}


double *
dvector(int nl, int nh)
{
	double	*v;

	v = (double *) malloc((unsigned) (nh - nl + 1 + NR_END) * sizeof(double));
	if (!v)
		fprintf(stderr,"allocation failure in dvector()");
	return v - nl + NR_END;
}


/* ************************  free_< >vector   ***********************
 *
 * a has to be declared as: <type> *a (e.g., int *a);
 * function call:  free_<>vector(a, nl, nh);
 * releases the vector a[nl..nh]
 */

void
free_scvector(signed char *v, int nl, int nh)
{
	free((FREE_ARG)(v + nl - NR_END));
}


void
free_ucvector(unsigned char *v, int nl, int nh)
{
	free((FREE_ARG)(v + nl - NR_END));
}


void
free_svector(short *v, int nl, int nh)
{
	free((FREE_ARG)(v + nl - NR_END));
}


void
free_ivector(int *v, int nl, int nh)
{
	free((FREE_ARG)(v + nl - NR_END));
}


void
free_ulvector(unsigned long *v, int nl, int nh)
{
	free((FREE_ARG)(v + nl - NR_END));
}


void
free_fvector(float *v, int nl, int nh)
{
	free((FREE_ARG)(v + nl - NR_END));
}


void
free_dvector(double *v, int nl, int nh)
{
	free((FREE_ARG)(v + nl - NR_END));
}


/* ************************** fill_<>vector **************************  
 *  
 * v[nl..nh] = val
 * fills a vector with a value
 */

void
fill_scvector(signed char *v, int nl, int nh, signed char val)
{
	int	i;
    
	for(i=nl; i<= nh; i++)
		v[i] = val;
}   
        
    
void
fill_ucvector(unsigned char *v, int nl, int nh, unsigned char val)
{
	int	i;

	for(i=nl; i<= nh; i++)
		v[i] = val;
}
    
    
void    
fill_svector(short *v, int nl, int nh, short val)
{
	int	i;

	for(i=nl; i<= nh; i++)
		v[i] = val;
}


void
fill_ivector(int *v, int nl, int nh, int val)
{
	int	i;

	for(i=nl; i<= nh; i++)
		v[i] = val;
}

void
fill_ulvector(unsigned long *v, int nl, int nh, unsigned long val)
{
	int	i;

	for(i=nl; i<= nh; i++)
		v[i] = val;
}

void
fill_fvector(float *v, int nl, int nh, float val)
{
	int	i;

	for(i=nl; i<= nh; i++)
		v[i] = val;
}


void
fill_dvector(double *v, int nl, int nh, double val)
{
	int	i;

	for(i=nl; i<= nh; i++)
		v[i] = val;
}


/* ************************  read_bin_< >vector   ***********************
 *
 * a has to be declared as: <type> *a (e.g., double *a);
 * function call:  read_bin_<>vector (a, rl, rh, filename);
 * the file is opened as a readable-binary file, i.e., "rb"
 */

void
read_bin_vector(void *v, int nl, int nh, size_t size, char *file, char *type)
{
	FILE	*fp;
	int	num = nh-nl+1;

	if  ((fp = fopen (file, "rb")) == NULL)
	{
		fprintf(stderr,"read_bin_%svector: Problem opening file %s\n", type, file);
		exit(-1);
	}
        if (fread (v, size, num, fp) != num)
	{
		fprintf (stderr, "read_bin_%svector: Problem reading from file: %s\n", type, file);
		exit(-1);
	}
	fclose (fp);
}

void
read_bin_scvector(signed char *v, int nl, int nh, char *file)
{
	read_bin_vector((void *)(&(v[nl])), nl, nh, sizeof(signed char), file, "sc");
}

void
read_bin_ucvector(unsigned char *v, int nl, int nh, char *file)
{
	read_bin_vector((void *)(&(v[nl])), nl, nh, sizeof(unsigned char), file, "uc");
}

void
read_bin_svector(short *v, int nl, int nh, char *file)
{
	read_bin_vector((void *)(&(v[nl])), nl, nh, sizeof(short), file, "s");
}

void
read_bin_ivector(int *v, int nl, int nh, char *file)
{
	read_bin_vector((void *)(&(v[nl])), nl, nh, sizeof(int), file, "i");
}

void
read_bin_ulvector(unsigned long *v, int nl, int nh, char *file)
{
	read_bin_vector((void *)(&(v[nl])), nl, nh, sizeof(unsigned long), file, "ul");
}

void
read_bin_fvector(float *v, int nl, int nh, char *file)
{
	read_bin_vector((void *)(&(v[nl])), nl, nh, sizeof(float), file, "f");
}

void
read_bin_dvector(double *v, int nl, int nh, char *file)
{
	read_bin_vector((void *)(&(v[nl])), nl, nh, sizeof(double), file, "d");
}


/* ***********************  write_bin_< >vector  **************************
 *
 * function call:  write_bin_<>vector(a, rl, rh, file);
 * write the matrix a[nl..nh] to file <file>
 * the file is opened as a writeable-binary file, i.e., "wb"
 */

void
write_bin_vector (void *v, int nl, int nh, size_t size, char *file, char *type)
{
	FILE	*fp;
	int	num = nh-nl+1;

	if  ((fp = fopen (file, "wb")) == NULL)
	{
		fprintf(stderr,"write_bin_%svector: Problem opening file %s\n", type, file);
		exit(-1);
	}
        if (fwrite (v, size, num, fp) != num)
	{
		fprintf (stderr, "write_bin_%svector: Problem writing to file: %s\n", type, file);
		exit(-1);
	}
	fclose (fp);
}

void
write_bin_scvector (signed char *v, int nl, int nh, char *file)
{
	write_bin_vector ((void *)(&(v[nl])), nl, nh, sizeof(signed char), file, "sc");
}

void
write_bin_ucvector (unsigned char *v, int nl, int nh, char *file)
{
	write_bin_vector ((void *)(&(v[nl])), nl, nh, sizeof(unsigned char), file, "uc");
}

void
write_bin_svector (short *v, int nl, int nh, char *file)
{
	write_bin_vector ((void *)(&(v[nl])), nl, nh, sizeof(short), file, "s");
}

void
write_bin_ivector (int *v, int nl, int nh, char *file)
{
	write_bin_vector ((void *)(&(v[nl])), nl, nh, sizeof(int), file, "i");
}

void
write_bin_ulvector (unsigned long *v, int nl, int nh, char *file)
{
	write_bin_vector ((void *)(&(v[nl])), nl, nh, sizeof(unsigned long), file, "ul");
}

void
write_bin_fvector (float *v, int nl, int nh, char *file)
{
	write_bin_vector ((void *)(&(v[nl])), nl, nh, sizeof(float), file, "f");
}

void
write_bin_dvector (double *v, int nl, int nh, char *file)
{
	write_bin_vector ((void *)(&(v[nl])), nl, nh, sizeof(double), file, "d");
}


/* ************************  read_text_< >vector   ***********************
 *
 * a has to be declared as: <type> *a (e.g., double *a);
 * function call:  read_text_<>vector (a, rl, rh, filename);
 * the file is opened as a readable-text file, i.e., "rt"
 */

void
read_text_scvector(signed char *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	num = nh-nl+1, dum;

	if  ((fp = fopen (file, "rt")) == NULL)
	{
		fprintf(stderr,"read_text_scvector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (num=nl; num<=nh; num++)
	{
		fscanf (fp, "%d", &dum);
		v[num] = (signed char)dum;
	}
	fclose (fp);
}


void
read_text_ucvector(unsigned char *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	num = nh-nl+1, dum;

	if  ((fp = fopen (file, "rt")) == NULL)
	{
		fprintf(stderr,"read_text_ucvector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (num=nl; num<=nh; num++)
	{
		fscanf (fp, "%d", &dum);
		v[num] = (unsigned char)dum;
	}
	fclose (fp);
}

void
read_text_svector(short *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	num = nh-nl+1;

	if  ((fp = fopen (file, "rt")) == NULL)
	{
		fprintf(stderr,"read_text_svector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (num=nl; num<=nh; num++)
		fscanf (fp, "%hd", &(v[num]));
	fclose (fp);
}


void
read_text_ivector(int *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	num = nh-nl+1;

	if  ((fp = fopen (file, "rt")) == NULL)
	{
		fprintf(stderr,"read_text_ivector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (num=nl; num<=nh; num++)
		fscanf (fp, "%d", &(v[num]));
	fclose (fp);
}

void
read_text_ulvector(unsigned long *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	num = nh-nl+1;

	if  ((fp = fopen (file, "rt")) == NULL)
	{
		fprintf(stderr,"read_text_ulvector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (num=nl; num<=nh; num++)
		fscanf (fp, "%ld", &(v[num]));
	fclose (fp);
}


void
read_text_fvector(float *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	num = nh-nl+1;

	if  ((fp = fopen (file, "rt")) == NULL)
	{
		fprintf(stderr,"read_text_fvector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (num=nl; num<=nh; num++)
		fscanf (fp, "%g", &(v[num]));
	fclose (fp);
}

void
read_text_dvector(double *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	num = nh-nl+1;

	if  ((fp = fopen (file, "rt")) == NULL)
	{
		fprintf(stderr,"read_text_dvector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (num=nl; num<=nh; num++)
		fscanf (fp, "%lg", &(v[num]));
	fclose (fp);
}

/* ***********************  write_text_< >vector  **************************
 *
 * function call:  write_text_<>vector(a, rl, rh, file);
 * write the matrix a[nl..nh] to file <file>
 * the file is opened as a writeable-text file, i.e., "wt"
 */


void
write_text_scvector (signed char *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	i;

	if  ((fp = fopen (file, "wt")) == NULL)
	{
		fprintf(stderr,"write_text_scvector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (i=nl; i<=nh; i++)
		fprintf(fp,"%d\n",v[i]);
	fclose (fp);
}

void
write_text_ucvector (unsigned char *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	i;

	if  ((fp = fopen (file, "wt")) == NULL)
	{
		fprintf(stderr,"write_text_ucvector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (i=nl; i<=nh; i++)
		fprintf(fp,"%d\n",v[i]);
	fclose (fp);
}

void
write_text_svector (short *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	i;

	if  ((fp = fopen (file, "wt")) == NULL)
	{
		fprintf(stderr,"write_text_svector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (i=nl; i<=nh; i++)
		fprintf(fp,"%hd\n",v[i]);
	fclose (fp);
}

void
write_text_ivector (int *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	i;

	if  ((fp = fopen (file, "wt")) == NULL)
	{
		fprintf(stderr,"write_text_ivector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (i=nl; i<=nh; i++)
		fprintf(fp,"%d\n",v[i]);
	fclose (fp);
}


void
write_text_ulvector (unsigned long *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	i;

	if  ((fp = fopen (file, "wt")) == NULL)
	{
		fprintf(stderr,"write_text_ulvector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (i=nl; i<=nh; i++)
		fprintf(fp,"%lu\n",v[i]);
	fclose (fp);
}

void
write_text_fvector (float *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	i;

	if  ((fp = fopen (file, "wt")) == NULL)
	{
		fprintf(stderr,"write_text_fvector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (i=nl; i<=nh; i++)
		fprintf(fp,"%g\n",v[i]);
	fclose (fp);
}

void
write_text_dvector (double *v, int nl, int nh, char *file)
{
	FILE	*fp;
	int	i;

	if  ((fp = fopen (file, "wt")) == NULL)
	{
		fprintf(stderr,"write_text_dvector: Problem opening file %s\n", file);
		exit(-1);
	}
	for (i=nl; i<=nh; i++)
		fprintf(fp,"%g\n",v[i]);
	fclose (fp);
}


/* ************************  print_< >vector   *********************** */


void
print_scvector(signed char *v, int nl, int nh)
{
	int	i;

	if ( (nh-nl+1) < max_num_per_row_in_vec )
	{
		fprintf(stderr,"(%3d",v[nl]);
		for (i=nl+1; i<=nh; i++)
			fprintf(stderr,"\t%3d",v[i]);
		fprintf(stderr,")\n");
	}
	else
		for (i=nl; i<=nh; i++)
	    		fprintf(stderr,"%d\n",v[i]);
}


void
print_ucvector(unsigned char *v, int nl, int nh)
{
	int	i;

	if ( (nh-nl+1) < max_num_per_row_in_vec )
	{
		fprintf(stderr,"(%3d",v[nl]);
		for (i=nl+1; i<=nh; i++)
			fprintf(stderr,"\t%3d",v[i]);
		fprintf(stderr,")\n");
	}
	else
		for (i=nl; i<=nh; i++)
	    		fprintf(stderr,"%d\n",v[i]);
}


void
print_svector(short *v, int nl, int nh)
{
	int	i;

	if ( (nh-nl+1) < max_num_per_row_in_vec )
	{
		fprintf(stderr,"(%6hd",v[nl]);
		for (i=nl+1; i<=nh; i++)
		fprintf(stderr,"\t%6hd",v[i]);
		fprintf(stderr,")\n");
	}
	else
		for (i=nl; i<=nh; i++)
	    		fprintf(stderr,"\t%6hd\n",v[i]);
}


void
print_ivector(int *v, int nl, int nh)
{
	int	i;

	if ( (nh-nl+1) < max_num_per_row_in_vec )
	{
		fprintf(stderr,"(%8d",v[nl]);
		for (i=nl+1; i<=nh; i++)
			fprintf(stderr,"\t%8d",v[i]);
		fprintf(stderr,")\n");
	}
	else
		for (i=nl; i<=nh; i++)
	    		fprintf(stderr,"\t%8d\n",v[i]);
}


void
print_ulvector(unsigned long *v, int nl, int nh)
{
	int	i;

	if ( (nh-nl+1) < max_num_per_row_in_vec )
	{
		fprintf(stderr,"(%lu",v[nl]);
		for (i=nl+1; i<=nh; i++)
			fprintf(stderr,"\t%lu",v[i]);
		fprintf(stderr,")\n");
	}
	else
		for (i=nl; i<=nh; i++)
	    		fprintf(stderr,"\t%lu\n",v[i]);
}


void
print_fvector(float *v, int nl, int nh)
{
	int	i;

	if ( (nh-nl+1) < max_num_per_row_in_vec )
	{
		fprintf(stderr,"(%1.6g",v[nl]);
		for (i=nl+1; i<=nh; i++)
		fprintf(stderr,"\t%1.6g",v[i]);
		fprintf(stderr,")\n");
	}
	else
		for (i=nl; i<=nh; i++)
	    		fprintf(stderr,"\t%1.6g\n",v[i]);
}


void
print_dvector(double *v, int nl, int nh)
{
	int	i;

	if ( (nh-nl+1) < max_num_per_row_in_vec )
	{
		fprintf(stderr,"(%1.6g",v[nl]);
		for (i=nl+1; i<=nh; i++)
			fprintf(stderr,"\t%1.6g",v[i]);
		fprintf(stderr,")\n");
	}
	else
		for (i=nl; i<=nh; i++)
	    		fprintf(stderr,"\t%1.6g\n",v[i]);
}



/* ************************  < >matrix   ***********************
 *
 * a has to be declared as: <type> **a (e.g., double **a);
 * function call:  a = <>matrix (a, rl, rh, cl, ch);
 * returns the matrix a[rl..rh][cl..ch]
 */

signed char **
scmatrix(int rl, int rh, int cl, int ch)
{
	int		i, nrow=rh-rl+1, ncol=ch-cl+1;
	signed char	**m;

	m = (signed char **) malloc((size_t) (nrow+NR_END) * sizeof(signed char *));
	if (!m)
		fprintf(stderr,"allocation failure 1 in scmatrix()");
	m += NR_END;
	m -= rl;

	m[rl] = (signed char *)malloc((size_t) ((nrow*ncol+NR_END) * sizeof(signed char)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in scmatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;

	for (i = rl+1; i <= rh; i++)
		m[i] = m[i-1]+ncol;

	return m;
}


unsigned char **
ucmatrix(int rl, int rh, int cl, int ch)
{
	int		i, nrow=rh-rl+1, ncol=ch-cl+1;
	unsigned char	**m;

	m = (unsigned char **) malloc((size_t) (nrow+NR_END) * sizeof(unsigned char *));
	if (!m)
		fprintf(stderr,"allocation failure 1 in ucmatrix()");
	m += NR_END;
	m -= rl;

	m[rl] = (unsigned char *)malloc((size_t) ((nrow*ncol+NR_END) * sizeof(unsigned char)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in ucmatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;

	for (i = rl+1; i <= rh; i++)
		m[i] = m[i-1]+ncol;

	return m;
}


short **
smatrix(int rl, int rh, int cl, int ch)
{
	int		i, nrow=rh-rl+1, ncol=ch-cl+1;
	short		**m;

	m = (short **) malloc((size_t) (nrow+NR_END) * sizeof(short *));
	if (!m)
		fprintf(stderr,"allocation failure 1 in smatrix()");
	m += NR_END;
	m -= rl;

	m[rl] = (short *)malloc((size_t) ((nrow*ncol+NR_END) * sizeof(short)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in smatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;

	for (i = rl+1; i <= rh; i++)
		m[i] = m[i-1]+ncol;

	return m;
}


int **
imatrix(int rl, int rh, int cl, int ch)
{
	int		i, nrow=rh-rl+1, ncol=ch-cl+1;
	int		**m;

	m = (int **) malloc((size_t) (nrow+NR_END) * sizeof(int *));
	if (!m)
		fprintf(stderr,"allocation failure 1 in imatrix()");
	m += NR_END;
	m -= rl;

	m[rl] = (int *)malloc((size_t) ((nrow*ncol+NR_END) * sizeof(int)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in imatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;

	for (i = rl+1; i <= rh; i++)
		m[i] = m[i-1]+ncol;

	return m;
}


float **
fmatrix(int rl, int rh, int cl, int ch)
{
	int		i, nrow=rh-rl+1, ncol=ch-cl+1;
	float		**m;

	m = (float **) malloc((size_t) (nrow+NR_END) * sizeof(float *));
	if (!m)
		fprintf(stderr,"allocation failure 1 in fmatrix()");
	m += NR_END;
	m -= rl;

	m[rl] = (float *)malloc((size_t) ((nrow*ncol+NR_END) * sizeof(float)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in fmatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;

	for (i = rl+1; i <= rh; i++)
		m[i] = m[i-1]+ncol;

	return m;
}


double **
dmatrix(int rl, int rh, int cl, int ch)
{
	int		i, nrow=rh-rl+1, ncol=ch-cl+1;
	double		**m;

	m = (double **) malloc((size_t) (nrow+NR_END) * sizeof(double *));
	if (!m)
		fprintf(stderr,"allocation failure 1 in dmatrix()");
	m += NR_END;
	m -= rl;

	m[rl] = (double *)malloc((size_t) ((nrow*ncol+NR_END) * sizeof(double)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in dmatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;

	for (i = rl+1; i <= rh; i++)
		m[i] = m[i-1]+ncol;

	return m;
}


/* ************************  free_< >matrix   ***********************
 *
 * a has to be declared as: <type> **a (e.g., double **a);
 * function call:  free_cmatrix (a, rl, rh, cl, ch);
 * releases the matrix a[rl..rh][cl..ch]
 */

void
free_scmatrix(signed char **m, int rl, int rh, int cl, int ch)
{
	free((FREE_ARG) (m[rl]+cl-NR_END));
	free((FREE_ARG) (m+rl-NR_END));
}


void
free_ucmatrix(unsigned char **m, int rl, int rh, int cl, int ch)
{
	free((FREE_ARG) (m[rl]+cl-NR_END));
	free((FREE_ARG) (m+rl-NR_END));
}


void
free_smatrix(short **m, int rl, int rh, int cl, int ch)
{
	free((FREE_ARG) (m[rl]+cl-NR_END));
	free((FREE_ARG) (m+rl-NR_END));
}

void
free_imatrix(int **m, int rl, int rh, int cl, int ch)
{
	free((FREE_ARG) (m[rl]+cl-NR_END));
	free((FREE_ARG) (m+rl-NR_END));
}

void
free_fmatrix(float **m, int rl, int rh, int cl, int ch)
{
	free((FREE_ARG) (m[rl]+cl-NR_END));
	free((FREE_ARG) (m+rl-NR_END));
}

void
free_dmatrix(double **m, int rl, int rh, int cl, int ch)
{
	free((FREE_ARG) (m[rl]+cl-NR_END));
	free((FREE_ARG) (m+rl-NR_END));
}


/* ************************** fill_<>matrix **************************
 *
 * function call: fill_<>matrix(a, rl, rh, cl, ch);
 * fills the matrix a[rl..rh][cl..ch] with val
 */

void
fill_scmatrix(signed char **m, int rl, int rh, int cl, int ch, signed char val)
{
	int	r, c; 
 
	for(r = rl; r <= rh; r++)
		for(c = cl; c<= ch; c++)
			m[r][c] = val;
}   


void
fill_ucmatrix(unsigned char **m, int rl, int rh, int cl, int ch, unsigned char val)
{
	int	r, c;

	for(r = rl; r <= rh; r++)
		for(c = cl; c<= ch; c++)
			m[r][c] = val;
}


void
fill_smatrix(short **m, int rl, int rh, int cl, int ch, short val)
{
	int	r, c;

	for(r = rl; r <= rh; r++)
		for(c = cl; c<= ch; c++)
			m[r][c] = val;
}


void
fill_imatrix(int **m, int rl, int rh, int cl, int ch, int val)
{
	int	r, c;

	for(r = rl; r <= rh; r++)
		for(c = cl; c<= ch; c++)
			m[r][c] = val;
}


void
fill_fmatrix(float **m, int rl, int rh, int cl, int ch, float val)
{
	int	r, c;

	for(r = rl; r <= rh; r++)
		for(c = cl; c<= ch; c++)
			m[r][c] = val;
}

void
fill_dmatrix(double **m, int rl, int rh, int cl, int ch, double val)
{
	int	r, c;

	for(r = rl; r <= rh; r++)
		for(c = cl; c<= ch; c++)
			m[r][c] = val;
}  


/* ************************  read_< >matrix   ***********************
 *
 * function call:  read_bin_<>matrix(a, rl, rh, cl, ch, <file>);
 * read the matrix a[rl..rh][cl..ch] from file <file>
 */


void
read_bin_scmatrix(signed char **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "rb")) == NULL)
	{
		fprintf(stderr,"write_bin_scmatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fread (&(m[r][cl]), sizeof(signed char), num_col, fp) != num_col)
		{
			fprintf (stderr, "read_bin_scmatrix: Problem reading file:%s\n", file);
			exit(-1);
		}
	fclose (fp);
}


void
read_bin_ucmatrix(unsigned char **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "rb")) == NULL)
	{
		fprintf(stderr,"write_bin_ucmatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fread (&(m[r][cl]), sizeof(unsigned char), num_col, fp) != num_col)
		{
			fprintf (stderr, "read_bin_ucmatrix: Problem reading file:%s\n", file);
			exit(-1);
		}
	fclose (fp);
}

void
read_bin_smatrix(short **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "rb")) == NULL)
	{
		fprintf(stderr,"write_bin_smatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fread (&(m[r][cl]), sizeof(short), num_col, fp) != num_col)
		{
			fprintf (stderr, "read_bin_smatrix: Problem reading file:%s\n", file);
			exit(-1);
		}
	fclose (fp);
}

void
read_bin_imatrix(int **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "rb")) == NULL)
	{
		fprintf(stderr,"write_bin_imatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fread (&(m[r][cl]), sizeof(int), num_col, fp) != num_col)
		{
			fprintf (stderr, "read_bin_imatrix: Problem reading file:%s\n", file);
			exit(-1);
		}
	fclose (fp);
}

void
read_bin_fmatrix(float **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "rb")) == NULL)
	{
		fprintf(stderr,"write_bin_fmatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fread (&(m[r][cl]), sizeof(float), num_col, fp) != num_col)
		{
			fprintf (stderr, "read_bin_fmatrix: Problem reading file:%s\n", file);
			exit(-1);
		}
	fclose (fp);
}

void
read_bin_dmatrix(double **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "rb")) == NULL)
	{
		fprintf(stderr,"write_bin_dmatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fread (&(m[r][cl]), sizeof(double), num_col, fp) != num_col)
		{
			fprintf (stderr, "read_bin_dmatrix: Problem reading file:%s\n", file);
			exit(-1);
		}
	fclose (fp);
}


/* ***********************  write_bin_< >matrix  **************************
 *
 * function call:  write_bin_<>matrix(a, rl, rh, cl, cl, file);
 * write the matrix a[rl..rh][cl..ch] to file <file>
 */


void
write_bin_scmatrix (signed char **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "wb")) == NULL)
	{
		fprintf(stderr,"write_bin_scmatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fwrite (&(m[r][cl]), sizeof(signed char), num_col, fp) != num_col)
		{
			fprintf (stderr, "write_bin_scmatrix: Problem writing to file: %s\n", file);
			exit(-1);
		}
	fclose (fp);
}


void
write_bin_ucmatrix (unsigned char **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "wb")) == NULL)
	{
		fprintf(stderr,"write_bin_ucmatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fwrite (&(m[r][cl]), sizeof(unsigned char), num_col, fp) != num_col)
		{
			fprintf (stderr, "write_bin_ucmatrix: Problem writing to file: %s\n", file);
			exit(-1);
		}
	fclose (fp);
}


void
write_bin_smatrix (short **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "wb")) == NULL)
	{
		fprintf(stderr,"write_bin_smatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fwrite (&(m[r][cl]), sizeof(short), num_col, fp) != num_col)
		{
			fprintf (stderr, "write_bin_smatrix: Problem writing to file: %s\n", file);
			exit(-1);
		}
	fclose (fp);
}

void
write_bin_imatrix (int **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "wb")) == NULL)
	{
		fprintf(stderr,"write_bin_imatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fwrite (&(m[r][cl]), sizeof(int), num_col, fp) != num_col)
		{
			fprintf (stderr, "write_bin_imatrix: Problem writing to file: %s\n", file);
			exit(-1);
		}
	fclose (fp);
}

void
write_bin_fmatrix (float **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "wb")) == NULL)
	{
		fprintf(stderr,"write_bin_fmatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fwrite (&(m[r][cl]), sizeof(float), num_col, fp) != num_col)
		{
			fprintf (stderr, "write_bin_fmatrix: Problem writing to file: %s\n", file);
			exit(-1);
		}
	fclose (fp);
}

void
write_bin_dmatrix (double **m, int rl, int rh, int cl, int ch, char *file)
{
	FILE	*fp;
	int	r, num_col = ch-cl+1;

	if  ((fp = fopen (file, "wb")) == NULL)
	{
		fprintf(stderr,"write_bin_dmatrix: Problem opening file %s\n", file);
		exit(-1);
	}
	for (r = rl; r < rh+1; r++)
		if (fwrite (&(m[r][cl]), sizeof(double), num_col, fp) != num_col)
		{
			fprintf (stderr, "write_bin_dmatrix: Problem writing to file: %s\n", file);
			exit(-1);
		}
	fclose (fp);
}


/* ***********************  print_< >matrix  **************************
 *
 * function call:  print_<>matrix(a, rl, rh, cl, ch);
 * prints the matrix a[rl..rh][cl..ch]
 */

void
print_scmatrix(signed char  **a, int rl, int rh, int cl, int ch)
{
	int	i, j;

	for (i = rl; i <= rh; i++)
	{
		for (j = cl; j <= ch; j++)
			fprintf(stderr, "\t%6d", a[i][j]);
		fprintf(stderr,"\n");
	}
	fprintf(stderr,"\n");
}


void
print_ucmatrix(unsigned char **a, int rl, int rh, int cl, int ch)
{
	int	i, j;

	for (i = rl; i <= rh; i++)
	{
		for (j = cl; j <= ch; j++)
	    		fprintf(stderr, "\t%6d", a[i][j]);
		fprintf(stderr,"\n");
	}
	fprintf(stderr,"\n");
}


void
print_smatrix(short **a, int rl, int rh, int cl, int ch)
{
	int	i, j;

	for (i = rl; i <= rh; i++)
	{
		for (j = cl; j <= ch; j++)
	    		fprintf(stderr, "\t%6hd", a[i][j]); /* hd = short */
		fprintf(stderr,"\n");
	}
	fprintf(stderr,"\n");
}


void
print_imatrix(int **a, int rl, int rh, int cl, int ch)
{
	int	i, j;

	for (i = rl; i <= rh; i++)
	{
		for (j = cl; j <= ch; j++)
	    		fprintf(stderr, "\t%6d", a[i][j]); /* field of at least 6 digits */
		fprintf(stderr,"\n");
	}
	fprintf(stderr,"\n");
}


void
print_fmatrix(float **a, int rl, int rh, int cl, int ch)
{
	int	i, j;

	for (i = rl; i <= rh; i++)
	{
		for (j = cl; j <= ch; j++)
	    		fprintf(stderr, "\t%1.6g", a[i][j]);
		fprintf(stderr,"\n");
	}
	fprintf(stderr,"\n");
}

void
print_dmatrix(double **a, int rl, int rh, int cl, int ch)
{
	int	i, j;

	for (i = rl; i <= rh; i++)
	{
		for (j = cl; j <= ch; j++)
	    		fprintf(stderr, "\t%1.6g", a[i][j]);
		fprintf(stderr,"\n");
	}
	fprintf(stderr,"\n");
}


/* ************************  < >3Dmatrix   ***********************
 *
 * a has to be declared as: <type> ***a (e.g., double ***a);
 * function call:  a = <>3Dmatrix (a, rl, rh, cl, ch, dl, dh);
 * returns the matrix a[rl..rh][cl..ch][dl..dh]
 */

signed char     ***
sc3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh)
{
	int	i, j, nrow = rh-rl+1, ncol = ch-cl+1, ndep = dh-dl+1;
	signed char	***m;

	m = (signed char ***) malloc((size_t)((nrow+NR_END) * sizeof(signed char **)));
	if (!m)
		fprintf(stderr,"allocation failure 1 in sc3Dmatrix()");
	m += NR_END;
	m -= rl;

	m[rl] = (signed char **) malloc((size_t)((nrow*ncol+NR_END) * sizeof(signed char *)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in sc3Dmatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;
   
	m[rl][cl] = (signed char *) malloc((size_t)((nrow*ncol*ndep+NR_END) * sizeof(char)));
	if (!m[rl][cl])
		fprintf(stderr,"allocation failure 3 in sc3Dmatrix()");
	m[rl][cl] += NR_END;
	m[rl][cl] -= dl;

	for(j = cl+1; j <= ch; j++)
		m[rl][j] = m[rl][j-1] + ndep;
	for(i = rl+1; i <= rh; i++)
	{
		m[i] = m[i-1] + ncol;
		m[i][cl] = m[i-1][cl] + ncol*ndep;
		for(j = cl+1; j <= ch; j++)
			m[i][j] = m[i][j-1] + ndep;
	}

	return m;
}


unsigned char     ***
uc3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh)
{
	int		i, j, nrow = rh-rl+1, ncol = ch-cl+1, ndep = dh-dl+1;
	unsigned char	***m;

	m = (unsigned char ***) malloc((size_t)((nrow+NR_END) * sizeof(unsigned char **)));
	if (!m)
		fprintf(stderr,"allocation failure 1 in uc3Dmatrix()");
	m += NR_END;
	m -= rl;

	m[rl] = (unsigned char **) malloc((size_t)((nrow*ncol+NR_END) * sizeof(unsigned char *)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in uc3Dmatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;
   
	m[rl][cl] = (unsigned char *) malloc((size_t)((nrow*ncol*ndep+NR_END) * sizeof(unsigned char)));
	if (!m[rl][cl])
		fprintf(stderr,"allocation failure 3 in uc3Dmatrix()");
	m[rl][cl] += NR_END;
	m[rl][cl] -= dl;

	for(j = cl+1; j <= ch; j++)
		m[rl][j] = m[rl][j-1] + ndep;
	for(i = rl+1; i <= rh; i++)
	{
		m[i] = m[i-1] + ncol;
		m[i][cl] = m[i-1][cl] + ncol*ndep;
		for(j = cl+1; j <= ch; j++)
			m[i][j] = m[i][j-1] + ndep;
	}

	return m;
}


short     ***
s3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh)
{
	int	i, j, nrow = rh-rl+1, ncol = ch-cl+1, ndep = dh-dl+1;
	short	***m;

	m = (short ***) malloc((size_t)((nrow+NR_END) * sizeof(short **)));
	if (!m)
		fprintf(stderr,"allocation failure 1 in s3Dmatrix()");
	m += NR_END;
	m -= rl;

	m[rl] = (short **) malloc((size_t)((nrow*ncol+NR_END) * sizeof(short *)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in s3Dmatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;
   
	m[rl][cl] = (short *) malloc((size_t)((nrow*ncol*ndep+NR_END) * sizeof(short)));
	if (!m[rl][cl])
		fprintf(stderr,"allocation failure 3 in s3Dmatrix()");
	m[rl][cl] += NR_END;
	m[rl][cl] -= dl;

	for(j = cl+1; j <= ch; j++)
		m[rl][j] = m[rl][j-1] + ndep;
	for(i = rl+1; i <= rh; i++)
	{
		m[i] = m[i-1] + ncol;
		m[i][cl] = m[i-1][cl] + ncol*ndep;
		for(j = cl+1; j <= ch; j++)
			m[i][j] = m[i][j-1] + ndep;
	}

	return m;
}


int	***
i3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh)
{
	int	i, j, nrow = rh-rl+1, ncol = ch-cl+1, ndep = dh-dl+1;
	int	***m;

	m = (int ***) malloc((size_t)((nrow+NR_END) * sizeof(int **)));
	if (!m)
		fprintf(stderr,"allocation failure 1 in i3Dmatrix()");
	m += NR_END;
	m -= rl;
 
	m[rl] = (int **) malloc((size_t)((nrow*ncol+NR_END) * sizeof(int *)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in i3Dmatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;
    
	m[rl][cl] = (int *) malloc((size_t)((nrow*ncol*ndep+NR_END) * sizeof(int)));
	if (!m[rl][cl])
		fprintf(stderr,"allocation failure 3 in i3Dmatrix()");
	m[rl][cl] += NR_END;
	m[rl][cl] -= dl;

	for(j = cl+1; j <= ch; j++)
		m[rl][j] = m[rl][j-1] + ndep;
	for(i = rl+1; i <= rh; i++)
	{
		m[i] = m[i-1] + ncol;
		m[i][cl] = m[i-1][cl] + ncol*ndep;
		for(j = cl+1; j <= ch; j++)
			m[i][j] = m[i][j-1] + ndep;
	}

	return m;
}


float     ***
f3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh)
{
	int	i, j, nrow = rh-rl+1, ncol = ch-cl+1, ndep = dh-dl+1;
	float	***m;

	m = (float ***) malloc((size_t)((nrow+NR_END) * sizeof(float **)));
	if (!m)
		fprintf(stderr,"allocation failure 1 in f3Dmatrix()");
	m += NR_END;
	m -= rl;

	m[rl] = (float **) malloc((size_t)((nrow*ncol+NR_END) * sizeof(float *)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in f3Dmatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;
   
	m[rl][cl] = (float *) malloc((size_t)((nrow*ncol*ndep+NR_END) * sizeof(float)));
	if (!m[rl][cl])
		fprintf(stderr,"allocation failure 3 in f3Dmatrix()");
	m[rl][cl] += NR_END;
	m[rl][cl] -= dl;

	for(j = cl+1; j <= ch; j++)
		m[rl][j] = m[rl][j-1] + ndep;
	for(i = rl+1; i <= rh; i++)
	{
		m[i] = m[i-1] + ncol;
		m[i][cl] = m[i-1][cl] + ncol*ndep;
		for(j = cl+1; j <= ch; j++)
			m[i][j] = m[i][j-1] + ndep;
	}

	return m;
}


double     ***
d3Dmatrix(int rl, int rh, int cl, int ch, int dl, int dh)
{
	int	i, j, nrow = rh-rl+1, ncol = ch-cl+1, ndep = dh-dl+1;
	double	***m;

	m = (double ***) malloc((size_t)((nrow+NR_END) * sizeof(double **)));
	if (!m)
		fprintf(stderr,"allocation failure 1 in d3Dmatrix()");
	m += NR_END;
	m -= rl;

	m[rl] = (double **) malloc((size_t)((nrow*ncol+NR_END) * sizeof(double *)));
	if (!m[rl])
		fprintf(stderr,"allocation failure 2 in d3Dmatrix()");
	m[rl] += NR_END;
	m[rl] -= cl;

	m[rl][cl] = (double *) malloc((size_t)((nrow*ncol*ndep+NR_END) * sizeof(double)));
	if (!m[rl][cl])
		fprintf(stderr,"allocation failure 3 in d3Dmatrix()");
	m[rl][cl] += NR_END;
	m[rl][cl] -= dl;

	for(j = cl+1; j <= ch; j++)
		m[rl][j] = m[rl][j-1] + ndep;
	for(i = rl+1; i <= rh; i++)
	{
		m[i] = m[i-1] + ncol;
		m[i][cl] = m[i-1][cl] + ncol*ndep;
		for(j = cl+1; j <= ch; j++)
			m[i][j] = m[i][j-1] + ndep;
	}

	return m;
}


/* ************************  free_< >3Dmatrix   ***********************
 *
 * a has to be declared as: <type> ***a (e.g., double ***a);
 * function call:  free_<>3Dmatrix (a, rl, rh, cl, ch, dl, dh);
 * releases the matrix a[rl..rh][cl..ch][dl..dh]
 */

void
free_sc3Dmatrix(signed char ***m, int rl, int rh, int cl, int ch, int dl, int dh)
{
	free((FREE_ARG) (m[rl][cl] + dl - NR_END));
	free((FREE_ARG) (m[rl] + cl - NR_END));
	free((FREE_ARG) (m + rl - NR_END));
}


void
free_uc3Dmatrix(unsigned char ***m, int rl, int rh, int cl, int ch, int dl, int dh)
{
	free((FREE_ARG) (m[rl][cl] + dl - NR_END));
	free((FREE_ARG) (m[rl] + cl - NR_END));
	free((FREE_ARG) (m + rl - NR_END));
}


void
free_s3Dmatrix(short ***m, int rl, int rh, int cl, int ch, int dl, int dh)
{
	free((FREE_ARG) (m[rl][cl] + dl - NR_END));
	free((FREE_ARG) (m[rl] + cl - NR_END));
	free((FREE_ARG) (m + rl - NR_END));
}


void
free_i3Dmatrix(int ***m, int rl, int rh, int cl, int ch, int dl, int dh)
{
	free((FREE_ARG) (m[rl][cl] + dl - NR_END));
	free((FREE_ARG) (m[rl] + cl - NR_END));
	free((FREE_ARG) (m + rl - NR_END));
}


void
free_f3Dmatrix(float ***m, int rl, int rh, int cl, int ch, int dl, int dh)
{
	free((FREE_ARG) (m[rl][cl] + dl - NR_END));
	free((FREE_ARG) (m[rl] + cl - NR_END));
	free((FREE_ARG) (m + rl - NR_END));
}


void
free_d3Dmatrix(double ***m, int rl, int rh, int cl, int ch, int dl, int dh)
{
	free((FREE_ARG) (m[rl][cl] + dl - NR_END));
	free((FREE_ARG) (m[rl] + cl - NR_END));
	free((FREE_ARG) (m + rl - NR_END));
}

