/**************************************************************
                 
  Title:    sclust1.6.c
  Author:   Becky Castano - converted from Matlab code written
               by Alex Gray, based on 1995 code and algorithm by Eric
               Mjolsness.
  Date:     2/11/01
  Function: Soft clustering - The program implements a form of 
            clustering derived via mean field approximation and globally
            optimized using deterministic annealing.  A slack cluster is
            included. 

	    - Slack cluster included.
	    - Variance (diagonal covariance matrix) included in objective 
	        function.
	    - Uses deterministic annealing.
	    - Sparse matrix representation for the matrix representing
	        the probability that a data point belongs to a class.

	    - returns the M matrix rather than a result vector as in 
	      previous versions

	    This version uses a sparse representation of the matrix M,
	    which represents the probability of a data point belonging
	    to a class. (M[n_idx][k_idx] is the probability that data point
	    n_idx is in class k_idx.)
	    The storage format is called row-indexed sparse storage mode.
	    See Numerical Recipes in C, pg 78 for a description of the 
	    storage format. 

            Changes from sclust1.4, 1.5:
             v1.6 no change to functionality, just changed in the
                 interface to call sclust.  As a result, new .h file
                 is used. 
	         Exit with error if all points end up in slack class.

  File dependencies:
    sclust1.6.h
    array.h, array.c
    sparse.h,sparse.c  
    bc_err.h, bc_err.c (debuglog, assert)

  Input:
    data_struct     *data
    sclust_params   cl_params
    data_out_struct *out_data - it is assumed that all arrays for 
        out_data are already allocated (no checks are made) 

  Notation:
     N - number of data points
     K - number of clusters
     D - number of features
     beta - 1/temperature
     betamin - 1/Max temp ( common range [0.001,0.3] )
     betamax - 1/min temp ( common range [1,10] )
     beta_factor - annealing rate (common range [1.001,1.3])

  Output:
   cluster_memberships  length N vector indicating the cluster number
                                 that each data point is assigned 
   mean         K x D matrix of D-dimensional cluster means
   vars         K x D matrix of D-dimensional cluster means


  Modifications:  

***************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "sclust1.6.h"
#include "array.h"
#include "sparse.h"
#include "bc_err.h"

#define MAX 1E100
#define MIN_STD 1E-50

char message[MESSAGELENGTH];

void sclust( data_struct *data, sclust_params cl_params, 
             data_out_struct *out_data) 
{
  float betamin, betamax, beta_factor;
  float beta;
  int betasweeps;
  int slack_flag; /* 0 do not use slack class, 1 do use */
  int N = data->n_data_points;
  int K = cl_params.n_clusters;
  int nrows;
  model_struct model;
  mem_prob_struct mem_probs;
  control_struct control;
  int i;

  sprintf(message," In sclust ");
  debuglog(message);

  /* initialize */
  betamax = cl_params.betamax;
  betamin = cl_params.betamin;
  beta_factor = cl_params.beta_factor;
  beta = betamin; 
  betasweeps = 0;
  control.beta = &beta;
  slack_flag = cl_params.slack_flag;
  nrows = cl_params.n_clusters + cl_params.slack_flag;

  sprintf(message,"Sclust Error: betamin %f betamax %f beta_factor %f", 
     betamin,betamax,beta_factor);
  assert(betamin<betamax && beta_factor > 1.0, message);

  Initialize(data, &model, &mem_probs, &control, &cl_params, out_data);

  sprintf(message,"betamin %f betamax %f beta_factor %f", 
     betamin,betamax,beta_factor);
  debuglog(message);

  /* main annealing loop */

  while ( beta < betamax )
  {
    for (i=0; i < 5; i++)
    {
      UpdateMeanEstimates(   &model,&mem_probs,data,&control);
      UpdateVarEstimates(    &model,&mem_probs,data,&control);
      UpdateMemProbEstimates(&model,&mem_probs,data,&control);
    }

    /* check for convergence */
    if ( mem_probs.ijM[nrows] <= nrows + mem_probs.n_points) /* done */
      beta = betamax;

    betasweeps++;
    beta *= beta_factor;   

#if DEBUG
    if ( betasweeps > 1)
      fprintf(stderr,"\b\b\b\b\b%5d",betasweeps);
    else
      fprintf(stderr,"K %d sweep %5d",K, betasweeps);
#endif

  }

#if DEBUG
  fprintf(stderr,"\n");
#endif

  MSparseToFull(&mem_probs);

  sprintf(message," End of sclust  sweeps %d ",betasweeps);
  debuglog(message);

  free_dvector(mem_probs.sM,0,N*(K+1));
  free_ivector(mem_probs.ijM,0,N*(K+1));
  free_dvector(model.sums,0,N);
}


/****************************************************
*  Function:    Initialize 
*  Date:        February 11, 2001 
*  Purpose:     intialize structure that will be used.  This
*               includes allocating memory needed and setting structure 
*               using the parameters passed to sclust constants.
*
*  Modified: 
*  Parameters:  
*  Returns:     none 
*****************************************************/

int Initialize( data_struct *data, 
                model_struct *model, 
                mem_prob_struct *mem_probs, 
                control_struct *control_params, 
                sclust_params *params,
                data_out_struct *out_data)
{
  int N,D,K;
  int nrows,ncols;
  double *sM;  /* sparse representation of M */
  int   *ijM;  /* indeces into sparse M */
  int slack_flag = params->slack_flag;

  N = data->n_data_points;
  D = data->n_features;
  K = params->n_clusters;

  sprintf(message,"N %d D %d K %d ", N,D,K);
  debuglog(message);

  /* assert */
 
  sprintf(message,"Sclust Error: points %d   features %d   clusters %d ", N,D,K);
  assert( N > 1 && D > 0 && K < N, message);

  srand48(params->seed);

  ijM =  ivector(0,N*(K+1));
  sM = dvector(0,N*(K+1));
  model->sums = dvector(0,N);

  /* create sparse mem prob array and initialize values */
  nrows = K + slack_flag;
  ncols = N;
  Create_sM(sM,ijM,nrows,ncols); 

  model->means = out_data->means;
  model->vars = out_data->vars;
  model->n_clusters = params->n_clusters;
  model->n_features = data->n_features;
  mem_probs->ijM = ijM; 
  mem_probs->sM = sM; 
  mem_probs->M = out_data->cluster_membership_probs;
  mem_probs->n_points = data->n_data_points;
  mem_probs->n_clusters = params->n_clusters;
  mem_probs->n_cols = params->n_clusters + params->slack_flag; 
  control_params->Mthreshfac = params->Mthreshfac;
  control_params->commit_reward = params->commitrew;
  control_params->slack_flag = params->slack_flag;
  
  return 1;
}


/****************************************************
*  Function:    UpdateMeanEstimates
*  Date:        January 24, 2001 
*  Purpose:     Update the estimate of cluster means
*                compute mean_k = ( sum_n (M_nk X_n) ) / sum_n (M_nk) 
*                sum over n for each class k 
*  Parameters:
*  Returns:     none
*****************************************************/

void  UpdateMeanEstimates( model_struct    *model,
                           mem_prob_struct *mem_probs, 
                           data_struct     *data,
                           control_struct  *control)
{
  int     i, j, k_idx, d_idx, n_idx;
  float   **means = model->means;
  int     K = mem_probs->n_clusters;
  int     D = data->n_features;
  int     *ijM = mem_probs->ijM;
  double  *sM = mem_probs->sM;
  float   **X = data->data_array;
  double *sums = model->sums; 
  float   sM_i,sM_j,sum;

  fill_fmatrix(means,0,K,0,D,0);
  fill_dvector(sums,0,K,0);

   /* first use diagonal elements */
  for (i=0; i < K; i++)
  {
    sM_i = sM[i];
    for (d_idx=0; d_idx < D; d_idx++)
      means[i][d_idx] = sM_i*X[i][d_idx];
    sums[i] = sM_i;
  }

  for (i=0; i < K; i++)  
    for(j= ijM[i]; j < ijM[i+1]; j++)   
    {
      k_idx = i;
      n_idx = ijM[j];
      sM_j = sM[j];
      for (d_idx=0; d_idx < D; d_idx++)
        means[i][d_idx] += sM_j * X[n_idx][d_idx];
      sums[k_idx] += sM_j;
    }

  for (k_idx=0; k_idx< K; k_idx++)
  {
    sum = sums[k_idx];
    for (d_idx=0; d_idx< D; d_idx++)
      means[k_idx][d_idx] /= sum;
  }
}


/****************************************************
*  Function:    UpdateVarEstimates
*  Date:        January 24, 2001 
*  Purpose:     Update the variance estimate (diagonal covariance)
*  Parameters:
*  Returns:     none
*****************************************************/

void  UpdateVarEstimates( model_struct    *model,
                          mem_prob_struct *mem_probs, 
                          data_struct     *data,
                          control_struct  *control)
{
  int     i, j, k_idx, d_idx, n_idx;
  float   **means = model->means;
  int     K = mem_probs->n_clusters;
  int     D = data->n_features;
  int     *ijM = mem_probs->ijM;
  double  *sM = mem_probs->sM;
  float   **X = data->data_array;
  double  *sums = model->sums; 
  float   **vars = model->vars;
  float   X_nd, sM_j,sum_k,mean_kd,var;
  float   **tmp_var;

  tmp_var = fmatrix(0,K,0,D);
  fill_fmatrix(tmp_var,0,K,0,D,0.0);

  for (i=0; i < K; i++)
    for (d_idx=0; d_idx < D; d_idx++)
      tmp_var[i][d_idx] = sM[i]*X[i][d_idx]*X[i][d_idx];

  for (i=0; i < K; i++)   
    for(j= ijM[i]; j < ijM[i+1]; j++)   
    {
      k_idx = i;
      n_idx = ijM[j];
      sM_j = sM[j];
      for (d_idx=0; d_idx < D; d_idx++)
      {
        X_nd = X[n_idx][d_idx];
        tmp_var[k_idx][d_idx] += sM_j * X_nd *X_nd;
      }
    }

  for (k_idx=0; k_idx< K; k_idx++)
  {
    sum_k = sums[k_idx];
    for (d_idx=0; d_idx< D; d_idx++) 
    {
      mean_kd = means[k_idx][d_idx];
      var = tmp_var[k_idx][d_idx]/sum_k - mean_kd*mean_kd;
      if ( var < MIN_STD )
        vars[k_idx][d_idx] = MIN_STD;
      else
        vars[k_idx][d_idx] = var;
    }
  }
  free_fmatrix(tmp_var,0,K,0,D);
}


/****************************************************
*  Function:    UpdateMemProbEstimates
*  Date:        January 24, 2001 
*  Purpose:     Update the variance estimate (diagonal covariance)
*  Parameters:
*  Returns:     none
*****************************************************/
void  UpdateMemProbEstimates( model_struct    *model,
                              mem_prob_struct *mem_probs, 
                              data_struct     *data,
                              control_struct  *control)
{
  int     i, j, k_idx, n_idx;
  float   **means = model->means;
  int     K = mem_probs->n_clusters;
  int     D = data->n_features;
  int     N = data->n_data_points;
  int     *ijM = mem_probs->ijM;
  double  *sM = mem_probs->sM;
  float   **X = data->data_array;
  int     slack_flag = control->slack_flag; 
  int     nrows = K + slack_flag;
  double  *sums = model->sums; 
  float   **vars = model->vars;
  float   beta = *(control->beta); 
  float   commitrew = control->commit_reward; 

  float *min_dist;
  float **dists; 
  int   *min_dist_class;
  void  UpdateMSparsityStructure(int *ijM,double *sM,int nrows,float
                          Mthreshfac);
  void  ComputeExponential(int *ijM, double *sM,int K,int slack_flag,
                           float **dists,float beta,float commitrew);
  void  ComputeDistToMeans(float **dists,int *ijM,double *sM,
                           int K,float **means,float
                           **vars,float **X,int D, int N);

  min_dist = fvector(0,N);
  min_dist_class = ivector(0,N);
  dists = fmatrix(0,N,0,K);  

  /* compute dists = sum_d (x_d - mean_k_d)^2 */

  fill_fmatrix(dists,0,N,0,K,0);

  ComputeDistToMeans(dists,ijM,sM,K,means,vars,X,D,N);

  /*  offset dists for numerics, i.e. to rescale floating point range */
  /* find distance to nearest cluster center */
      /* find the min distances */
  fill_fvector(min_dist,0,N,0);
  fill_ivector(min_dist_class,0,N,-1);
  for (i=0; i < nrows-slack_flag; i++) 
  {   
    min_dist[i] = dists[i][i];
    min_dist_class[i] = i;
  }
  for (i=0; i < nrows-slack_flag; i++)    
    for(j= ijM[i]; j < ijM[i+1]; j++)   
    {	
      k_idx = i;
      n_idx = ijM[j];
      if ( min_dist_class[n_idx] < 0 || min_dist[n_idx] > dists[n_idx][k_idx])
      {
        min_dist[n_idx] = dists[n_idx][k_idx];
        min_dist_class[n_idx] = k_idx;
      }
    }

  /* subtract the min distance */

  for (i=0; i < nrows; i++)    
    dists[i][i] -= min_dist[i];
  for (i=0; i < nrows; i++)    
    for(j= ijM[i]; j < ijM[i+1]; j++)   
    {	
      k_idx = i;
      n_idx = ijM[j];
      dists[n_idx][k_idx] -= min_dist[n_idx];
    }
	
  /* unnormalized exponentials (competitive activation) */
  ComputeExponential(ijM,sM,K,slack_flag,dists,beta,commitrew);

  /*  WTA normalization (competitive activation): */
  /* (normalize each row of the memberships) */
  /* compute sums over all classes, k,  for each sample, n */
	fill_dvector(sums,0,N,0);
	for (i=0; i < nrows; i++) 
    sums[i] = sM[i];

  for (i=0; i < nrows; i++)   
    for(j= ijM[i]; j < ijM[i+1]; j++) 
    {
      n_idx = ijM[j];
      sums[n_idx] += sM[j];
    }

	/* divide by feature sums to normalize  */
  for (i=0; i < nrows; i++) 
    sM[i] /= sums[i];
  for (i=0; i < nrows; i++)    
    for(j= ijM[i]; j < ijM[i+1]; j++)  
    {
      n_idx = ijM[j];
      sM[j] /= sums[n_idx];
    }

  /*  update M's sparsity structure */
  UpdateMSparsityStructure(ijM,sM,nrows,control->Mthreshfac);

  free_ivector(min_dist_class,0,N);
  free_fvector(min_dist,0,N);
  free_fmatrix(dists,0,N,0,K); 
}

/****************************************************
*  Function:    MSparseToFull 
*  Date:        February 10, 2001 
*  Purpose:     Fill the membership array M with the 
*                data from the sparse array, for return from sclust 
*  Parameters:
*  Returns:     none
*****************************************************/
void MSparseToFull(mem_prob_struct *mem_probs)
{
  double **M = mem_probs->M;
  double *sM = mem_probs->sM;
  int *ijM = mem_probs->ijM;
  int N = mem_probs->n_points;
  int K = mem_probs->n_clusters;
  int n_cols = mem_probs->n_cols;
  int i,j;
  int k_idx, n_idx;
  int cnt = 0;
  int sum = 0;

  fill_dmatrix(M,0,N,0,K,0);
  for (i=0; i < n_cols; i++) 
    M[i][i] = sM[i]; 	
  for (i=0; i < n_cols; i++)    /* classes (k) */
    for(j= ijM[i]; j < ijM[i+1]; j++)   /* sample index (n) */
    {
      k_idx = i;
      n_idx = ijM[j];
      M[n_idx][k_idx] = sM[j];
    }

  /* check for all slack error debug */
  for (j=0; j <= K; j++)
  {
    cnt = 0;
    for (i=0; i < N;i++)
      if ( M[i][j] > 0.5 )
        cnt++;
    sprintf(message,"cnt[%d]  %d ",j,cnt); 
    debuglog(message);
    sum += cnt;
  }
  sprintf(message,"total cnt  %d ",sum); 
  debuglog(message);

  /* error if cnt[K] == N */
  sprintf(message,"Sclust Error: all points in slack. May need to increase commit_reward.");
  assert(cnt < N,message);
}


/****************************************************
*  Function:    UpdateMSparsityStructure 
*  Date:        February 10, 2001 
*  Purpose:     Update the membership prob sparse array, 
*               eliminating elements that are very small
*               It is unlikely that a point will belong to
*               a class once the intermediate probability for membership
*               has become extremely small.
*  Parameters:
*  Returns:     none
*****************************************************/
void  UpdateMSparsityStructure(int *ijM,double *sM,int nrows,float Mthreshfac)
{
  int current_j = ijM[0];
  int start_j = ijM[0];
  int i,j;

  for (i=0; i < nrows; i++)   
  {
    for(j= start_j; j < ijM[i+1]; j++) 
    {
      if ( sM[j] > Mthreshfac)
      {
        sM[current_j] = sM[j];
        ijM[current_j] = ijM[j];
        current_j++;
      }
    }
    start_j = ijM[i+1];
    ijM[i+1] = current_j;
  }

}


/****************************************************
*  Function:    ComputeExponential
*  Date:        February 10, 2001 
*  Purpose:     compute unormalized membership expoentials
*               (called competitive activation 
*  Parameters:
*  Returns:     none
*****************************************************/
void  ComputeExponential(int *ijM,double *sM,int K,int slack_flag,
      float **dists,float beta,float commitrew)
{
  int i,j;
  int n_idx,k_idx;

  for (i=0; i < K; i++)
  {
    sM[i] = exp(-beta*(dists[i][i]-commitrew));
    if ( sM[i] > MAX )
      sM[i] = MAX;
  }

  for (i=0; i < K; i++)   
    for(j= ijM[i]; j < ijM[i+1]; j++)
    {
      k_idx = i;
      n_idx = ijM[j];
      sM[j] = exp(-beta*(dists[n_idx][k_idx]-commitrew));
      if ( sM[j] > MAX )
        sM[j] = MAX;
    }

   /* slack */
  if ( slack_flag )
  {
    i = K;
    sM[i] = exp(-beta*(dists[i][i]));
    if ( sM[i] > MAX )
      sM[i] = MAX;
    for (j=ijM[i]; j < ijM[i+1];j++)	
    {
      k_idx = i;
      n_idx = ijM[j];
      sM[j] = exp(-beta*(dists[n_idx][k_idx]));
      if ( sM[j] > MAX )
        sM[j] = MAX;
    }
  }
}


/****************************************************
*  Function:    ComputeDistToMeans 
*  Date:        February 10, 2001 
*  Purpose:     compute distance of each point to each 
*               class mean 
*  Note:   according to gprof, this routine takes about 
*           40% of the total time for cross_validation
*          The log is not the cost.
*          Considerable time is spent on the line: dist = mean - X;
*  Parameters:
*  Returns:     none
*****************************************************/
void  ComputeDistToMeans(float **dists,int *ijM,double *sM,
 int K,float **means,float **vars,float **X,int D,int N)
{
  int i,j;
  int d_idx, k_idx, n_idx;
  float dist;
  float var;
  float dists_nk;
  void  ComputeLogVars(float **vars, float **log_vars, int K,int D);

  fill_fmatrix(dists,0,N,0,K,0);

  for (i=0; i < K; i++)    
    for (d_idx =0 ; d_idx < D; d_idx++) 
    {	
      dist = means[i][d_idx] - X[i][d_idx];
      dist = dist * dist/vars[i][d_idx]; 
      dists[i][i] = dist + 0.5*log( vars[i][d_idx]);  
    }
  for (i=0; i < K; i++)    
  {
    k_idx = i;
    for(j= ijM[i]; j < ijM[i+1]; j++)   
    {
      n_idx = ijM[j];
      dists_nk = 0;
      for (d_idx =0 ; d_idx < D; d_idx++) 
      {	
        dist = means[k_idx][d_idx] - X[n_idx][d_idx];  
        var = vars[k_idx][d_idx];
        dist = dist * dist/var; 
        dists_nk += dist + 0.5*log( var );  
      }
      dists[n_idx][k_idx] = dists_nk; 
    }
  }
}

