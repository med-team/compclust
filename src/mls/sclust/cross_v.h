#ifndef __CROSSV_HEADER
#define __CROSSV_HEADER

#include "sclust1.6.h"

typedef struct cross_v_params{

   /* inputs */
   float **data;
   int n_points;
   int n_features;
   int min_clusters;
   int max_clusters;
   int cluster_step_size;
   int n_runs;
   int n_samples;
   long int random_seed;

   /* results */
   int *classification;
   int final_n_classes;
   double *likelihoods;
   double classification_likelihood;
} cross_v_struct;
         

FILE* open_file(char* filename, char* permissions, char* error_message);

void CrossValidate(cross_v_struct *cv_params, sclust_params *cl_params);
void DivideData(data_struct *data, data_struct *data1, data_struct *data2);
double ComputeLikelihood(data_struct *data, data_out_struct *model);
int  FindBestNumClasses(double ***test_likelihoods,
                        cross_v_struct *cv_params);
#endif
