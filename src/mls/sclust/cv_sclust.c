#include <stdio.h>
#include <stdlib.h>
#include "array.h"
#include "cross_v.h"
#include "sclust1.6.h"
#include "bc_err.h"

void ReadInput(char *in_filename,struct cross_v_params **cv_params,
              sclust_params **clustering_params);
void WriteResults( char *out_filename, cross_v_struct *cv_params);
void FreeAll(cross_v_struct *cv_params, sclust_params *cl_params);


int main(int argc, char** argv)
{
  struct cross_v_params *cv_params = NULL;
  sclust_params *clustering_params = NULL;
  char *in_filename = argv[1];
  char *out_filename = argv[2];
  char message[MESSAGELENGTH];

  if (argc < 2 )
  {
    fprintf(stderr,"Usage: test_cross_v in_param_file [results_file]\n");
    exit(1);
  }
  else if (argc < 3)
      out_filename = NULL;

  sprintf(message,"input filename is %s", in_filename);
  debuglog(message);
  if (out_filename)
    sprintf(message,"results filename is %s", out_filename);
  else
    sprintf(message,"results filename is (null), results to stdout");
  debuglog(message);

  ReadInput(in_filename,&cv_params,&clustering_params);
  CrossValidate(cv_params,clustering_params);
  WriteResults(out_filename,cv_params);

  FreeAll(cv_params,clustering_params);
  exit(0);
}


/**************************************************************
                 
  Function:  ReadInput
  Author:    Becky  Castano
  Date:     2/12/01
  Purpose:  read input data and parameters 
   read cv_params 
   read clustering params 
   read data 
  Note:  somewhat brittle with regard to file format,
        no lines between clustering params and data array are allowed
  Possible Improvements to make: 
       -  make get file line a macro

  Returns:   none

***************************************************************/
void ReadInput(char *in_filename,struct cross_v_params **cvp,
              sclust_params **cl_params)
{
  FILE *fp;
  char file_line[300];  
  int n_points,n_features; 
  int min_clusters, max_clusters; 
  int cluster_step_size;
  int pnt_idx, f_idx;
  float slack_flag;
  char message[MESSAGELENGTH];
  struct cross_v_params *cv_params = NULL;
  sclust_params *clustering_params = NULL;

  fp = fopen (in_filename, "r");
  sprintf(message,"ReadInput - Problem opening file %s\n",in_filename);
  assert(fp != NULL, message);

  cv_params = (cross_v_struct *)calloc(1,sizeof(cross_v_struct));

  fgets(file_line,99,fp);
  while (file_line[0] == '#' || file_line[0] == '\n')  /* skip comment and blank lines */
    fgets (file_line, 99, fp);

  /* read cv_params */
  sscanf (file_line, "%d %d", &n_points,&n_features);     
  fgets(file_line,99,fp);
  sscanf (file_line, "%d %d %d",
          &min_clusters,&max_clusters,&cluster_step_size);     
  fgets(file_line,99,fp);
  sscanf (file_line, "%d %d %ld", 
          &(cv_params->n_runs), 
          &(cv_params->n_samples),
          &(cv_params->random_seed));

  cv_params->n_points = n_points;
  cv_params->n_features = n_features;
  cv_params->min_clusters = min_clusters;
  cv_params->max_clusters = max_clusters;
  cv_params->cluster_step_size = cluster_step_size;

  /* read clustering params */
  clustering_params = (sclust_params *)calloc(1,sizeof(sclust_params));

  fgets(file_line,99,fp);
  while (file_line[0] == '#' || file_line[0] == '\n')  /* skip comment lines */
    fgets (file_line, 99, fp);
  sscanf (file_line, "%f", &(clustering_params->commitrew) );
  fgets(file_line,99,fp); /* betamin */
  sscanf (file_line, "%f", &(clustering_params->betamin) );
  fgets(file_line,99,fp); /* betamax */
  sscanf (file_line, "%f", &(clustering_params->betamax) );
  fgets(file_line,99,fp); /* betafactor */
  sscanf (file_line, "%f", &(clustering_params->beta_factor) );
  fgets(file_line,99,fp); /* Mthreshfac */
  sscanf (file_line, "%f", &(clustering_params->Mthreshfac) );
  fgets(file_line,99,fp); /* slack_flag */
  sscanf (file_line, "%f", &slack_flag);
  if (slack_flag > 0.99999)
    clustering_params->slack_flag = 1; 
  else
    clustering_params->slack_flag = 0; 
  clustering_params->seed = (long int)cv_params->random_seed;

  clustering_params->membership_probs =
    dmatrix(0,n_points,0,max_clusters);  

  /* allocate and enter data */
  cv_params->data = fmatrix(0,n_points,0,n_features);

  for (pnt_idx =0; pnt_idx < n_points; pnt_idx++)
    for (f_idx =0; f_idx < n_features; f_idx++)
      fscanf(fp,"%f", &(cv_params->data[pnt_idx][f_idx])); 

  sprintf(message,"ReadInput: (first) data[0][0] %f ", 
     cv_params->data[0][0]);
  debuglog(message);
  sprintf(message,"ReadInput: (last) data[%d][%d] %f ",
     n_points-1,n_features-1, cv_params->data[n_points-1][n_features-1]);
  debuglog(message);
          
  *cvp = cv_params;
  *cl_params = clustering_params;

  fclose(fp);
}


/**************************************************************
  Function:  WriteResults
  Author:    Becky  Castano
  Date:     2/12/01
  Purpose:  print results to file or stdout 
   print final cluster label for each point 
   print log likelihood of final clustering
   print test log likelihoods for each number of clusters tested

  Returns:   none
**************************************************************/

void WriteResults( char *out_filename, struct cross_v_params *cv_params)
{
  FILE *fp;
  int pnt_idx, c_idx;
  int min_clusters = cv_params->min_clusters;
  int max_clusters = cv_params->max_clusters;
  int cluster_step_size = cv_params->cluster_step_size;
  int n_points = cv_params->n_points;
  char message[MESSAGELENGTH];

  if ( out_filename == NULL)
    fp = stdout;
  else
  {
    fp = fopen (out_filename, "w");
    sprintf(message,"WriteResults - Problem opening file %s\n",out_filename);
    assert(fp != NULL, message);
  }

  fprintf(fp,"# final label for each point \n");
  for (pnt_idx =0; pnt_idx < n_points; pnt_idx++)
    fprintf(fp, "%d \n",cv_params->classification[pnt_idx]);

  fprintf(fp,"# log likelihood of final classification ( %d clusters )\n",
        cv_params->final_n_classes);
  fprintf(fp, "%f \n",cv_params->classification_likelihood);
  
  fprintf(fp,"# log likelihoods for k clusters\n");
  for (c_idx=min_clusters; c_idx <= max_clusters; c_idx += cluster_step_size)
    fprintf(fp, "%d %f \n",c_idx, cv_params->likelihoods[c_idx]);
  fclose(fp);
}


/**************************************************************
  Function:  FreeAll
  Author:    Becky  Castano
  Date:     2/12/01
  Purpose:  free memory use by clustering_param structure 
            and cv_param structure memory 

  Returns:   none
**************************************************************/

void FreeAll(cross_v_struct *cv_params, sclust_params *cl_params)
{
  free_fmatrix(cv_params->data,0,cv_params->n_points,0,cv_params->n_features);
  free_dvector(cv_params->likelihoods,cv_params->min_clusters,
               cv_params->max_clusters);
  free_ivector(cv_params->classification,0, cv_params->n_points);
  free(cv_params);

  free_dmatrix(cl_params->membership_probs,0,cl_params->n_points,
               0,cl_params->n_clusters);
  free(cl_params);
 
  
}
