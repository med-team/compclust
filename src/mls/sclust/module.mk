CURDIR := $(MLSDIR)/sclust

SCLUSTSRC := $(wildcard $(CURDIR)/*.c)
SRC += $(SCLUSTSRC)
CFLAGS += -I$(CURDIR)

SCLUSTLIBS = $(BASEDIR)/da/libda$(LIBEXT) \
            $(BASEDIR)/nr/libnr$(LIBEXT) \
	    $(BASEDIR)/ut/libut$(LIBEXT) \
	    $(MLSDIR)/common/libcommon$(LIBEXT) \
	    $(MLSDIR)/normalizations/libnorm$(LIBEXT) \
	    $(MLSDIR)/distances/libdist$(LIBEXT) \
	    $(MLSDIR)/initializations/libinit$(LIBEXT)
SCLUSTLIBDIRFLAGS := $(call make_libdirflag,$(SCLUSTLIBS))
SCLUSTLIBLINKFLAGS := $(call make_liblinkflag,$(SCLUSTLIBS)) -lm

SCLUST := $(CURDIR)/sclust$(BINEXT)
TARGETBINS += $(SCLUST)

$(SCLUST): $(SCLUSTSRC:.c=$(OBJEXT)) $(SCLUSTLIBS)
	$(CC) $(SCLUSTLIBDIRFLAGS) -o $@ $(SCLUSTSRC:.c=$(OBJEXT)) $(SCLUSTLIBLINKFLAGS)
