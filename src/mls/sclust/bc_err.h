#ifndef __BC_ERR_HEADER
#define __BC_ERR_HEADER

#define DEBUG 1  /* flag controls whether or not to print debug messages */
#define DEBUGFILE 0
#define MESSAGELENGTH 512  /* size of the message string */

void assert(int expr, char* errormessage);
void debuglog(char* message);

#endif
