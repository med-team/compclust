#include <stdio.h>
#include <stdlib.h>
#include "bc_err.h"

/**************************************************************
  Function:  assert
  Author:    Tobias Mann 
  Date:     2000 
  Purpose:  signal fatal program error, exit 
**************************************************************/

void assert(int expr, char* errormessage)
{
  char message[MESSAGELENGTH];
  FILE* debugfile;

  if(! expr)
  {
    sprintf(message, "assertion FAILURE: %s", errormessage);
    if (DEBUGFILE)
    {
      debugfile = fopen("/tmp/SmclustdebugLog","a");
      fprintf(debugfile, "cv message: %s\n", message);
      fclose(debugfile);
    } 
    else 
      fprintf(stderr, "cv message: %s\n", message);

    exit(-1);
  }
}


/**************************************************************
  Function:  debuglog
  Author:    Tobias Mann 
  Date:     2000 
  Purpose:  print debugging message to file or stdio 
**************************************************************/

void debuglog(char* message)
{
  FILE* debugfile;

  if (DEBUG)
  {
    if (DEBUGFILE)
    {
      debugfile = fopen("/tmp/SmclustdebugLog","a");
      fprintf(debugfile, "cv message: %s\n", message);
      fclose(debugfile);
    } 
    else
      fprintf(stderr, "cv message: %s\n", message);
  }
}

