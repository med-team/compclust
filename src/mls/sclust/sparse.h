/*  sparse.h */

void M_Transpose_to_Sparse(float **M, double *sM, int *ijM, int rows, int cols);
void M_to_Sparse(float **M, double *sM, int *ijM, int rows, int cols);
void PrintSparseM( double *sM, int *ijM, int nrows, int ncols);
void Create_sM(double *sM, int *ijM, int rows, int cols);
void UpdateSparsityStructure( double *sM, int *ijM, int rows, int cols, double thresh);


