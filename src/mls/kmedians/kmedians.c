/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * @author Ben Bornstein
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "kmedians.h"
#include "matrix.h"
#include "random.h"
#include "util.h"

#include "nr_util.h"

#include "dist_metrics.h"
#include "init_means.h"
#include "norm_data.h"

/**
 * Safely allocates (via safe_malloc()) memory for a kmedians_data structure.
 * The resulting structure is initialized with values from the given
 * kmedians_parameters structure. A pointer to the structure is returned.
 *
 * @see safe_malloc
 */
struct kmedians_data *allocate_kmedians_data(struct kmedians_parameters *p)
{
  struct kmedians_data *d = NULL;


  d = (struct kmedians_data *) safe_malloc( sizeof(struct kmedians_data) );

  d->datapoints = p->rows;
  d->features   = p->cols;
  d->classes    = p->k;

  d->classifications   = NR_ivector(1, d->datapoints);
  d->class_count_array = NR_ivector(1, d->classes);
  d->data              = NR_dmatrix(1, d->datapoints, 1, d->features);
  d->class_medians     = NR_dmatrix(1, d->classes, 1, d->features);
  d->seed              = &(p->seed);

  return d;
}


/**
 * Safely allocates (via safe_malloc()) memory for a kmedians_parameters
 * structure.  The resulting structure is initialized with default values.
 * A pointer to the structure is returned.
 *
 * @see safe_malloc
 */
struct kmedians_parameters *allocate_kmedians_parameters(void)
{
  struct kmedians_parameters *p = (struct kmedians_parameters *)
                                safe_malloc( sizeof(struct kmedians_parameters) );


  p->distance_metric    = -1;
  p->init_medians       = -1;
  p->k                  = -1;
  p->k_strict           =  0;
  p->max_iterations     = -1;
  p->max_restarts       =  0;
  p->num_median_samples =  3;
  p->cols               = -1;
  p->rows               = -1;
  p->seed               = 42;
  p->dataset_filename   = (char *) NULL;
  p->result_filename    = (char *) NULL;
  p->medians_filename   = (char *) NULL;

  return p;
}


/**
 * Frees the memory associated with the given kmedians_data structure.
 */
void free_kmedians_data(struct kmedians_data *d)
{
  NR_free_ivector(d->classifications, 1, d->datapoints);
  NR_free_ivector(d->class_count_array, 1, d->classes);
  NR_free_dmatrix(d->data, 1, d->datapoints, 1, d->features);
  NR_free_dmatrix(d->class_medians, 1, d->classes, 1, d->features);

  free( d );
}


/**
 * Frees the memory associated with the given kmedians_parameters structure.
 */
void free_kmedians_parameters(struct kmedians_parameters *p)
{
  if (p->dataset_filename != NULL)
    free( p->dataset_filename );

  if (p->result_filename != NULL)
    free( p->result_filename );

  if (p->medians_filename != NULL)
    free(p->medians_filename);

  free(p);
}


/**
 * Classifies each datum from 1 to k according to the nearest median.
 *
 * Where:
 *   k           = d->classes
 *   medians     = d->class_medians
 *   memberships = d->classifications
 *   datum       = d->data[i][]
 *   i           = [1 ... d->datapoints]
 *   j           = [1 ... d->features  ]
 */
void classify_data(struct kmedians_data *d, struct kmedians_parameters *p)
{
  double *dist;
  int    class;
  int    i, j;


  if (DEBUG_KMEDIANS)
  {
    fprintf( stderr, "  Entering classify_data()\n");
  }

  dist = NR_dvector(1, d->classes);

  zero_class_counts(d);

  for (i = 1; i <= d->datapoints; i++)
  {
    /**
     * Compute all the datapoint to class medians values and find the min.
     */

    distance2(d->data[i], d->class_medians, d->features, d->classes, dist,
              p->distance_metric);

    class = 1;

    for (j = 2; j <= d->classes; j++)
    {
      if (dist[j] < dist[class]) 
      {
        class = j;
      }
    }

    d->classifications[i] = class;
    d->class_count_array[class]++;
  }

  if (DEBUG_KMEDIANS)
  {
    fprintf( stderr, "  Exiting  classify_data()\n");
  }

  NR_free_dvector(dist, 1, d->classes);
}


/**
 * Compares the contents of the classification field of the kmedians_data
 * structure to classifications.  If they are equal by content zero
 * is returned.  Otherwise, the number of elements by which they differ
 * is returned.
 */
int compare_classifications(struct kmedians_data *d, int *classifications)
{
  int differences = 0;
  int i;


  for (i = 1; i <= d->datapoints; i++)
  {
    if (d->classifications[i] != classifications[i])
    {
      differences++;
    }
  }

  return differences;
}


/**
 * Copies the contents of the classification field of the kmedians_data
 * structure to destination.
 */
void copy_classifications(struct kmedians_data *d, int *destination)
{
  int i;


  for (i = 1; i <= d->datapoints; i++)
  {
    destination[i] = d->classifications[i];
  }
}


/**
 * Estimates the medians of k classes using the class membership of each
 * datum.
 *
 * Where:
 *   k           = d->classes
 *   memberships = d->classifications
 *   datum       = d->data[i][]
 *   i           = [1 ... d->datapoints]
 *   j           = [1 ... d->features  ]
 */

static int dcmp(const void *a, const void *b)
{
  double x, y;

  x = *((double *) a);
  y = *((double *) b);

  if (x < y)
    return -1;

  if (x > y)
    return 1;

  return 0;
}

void estimate_medians(struct kmedians_data *d)
{
  int i, class, feature, midpoint, count, index;
  double **tmp = NULL;

  if (DEBUG_KMEDIANS)
  {
    fprintf( stderr, "  Entering estimate_medians()\n");
  }

  for (class = 1; class <= d->classes; class++) 
  {
    count    = d->class_count_array[class];
    midpoint = count / 2;

    tmp      = NR_dmatrix(1, d->features, 1, count);
    index    = 1;

    /*
     * Grab all the data vectors for this class
     */

    i = 0;

    while (index <= count)
    {
      i++;

      if (d->classifications[i] != class)
      {
        continue;
      }

      for (feature = 1; feature <= d->features; feature++)
      {
        tmp[feature][index] = d->data[i][feature];
      }

      index++;
    }

    /*
     * Sort each of the features and compute the medians
     */

    for (feature = 1; feature <= d->features; feature++)
    {
      qsort(tmp[feature], count, sizeof(double), dcmp);
      
      if ((count % 2) == 1)
      {
        d->class_medians[class][feature] = tmp[feature][midpoint];
      }
      else
      {
        d->class_medians[class][feature] = 
          0.5 * (tmp[feature][midpoint] + tmp[feature][midpoint + 1]);
      }
    }

    NR_free_dmatrix(tmp, 1, d->features, 1, count);
  }

  if (DEBUG_KMEDIANS)
  {
    fprintf( stderr, "  Exiting  estimate_medians()\n");
  }
}


/**
 * Returns the number of empty classes in d->class_count_array.
 */
int get_num_empty_classes(struct kmedians_data *d)
{
  int num_empty_classes = 0;
  int i;


  for (i = 1; i <= d->classes; i++ )
  {
    if (d->class_count_array[i] == 0)
    {
      num_empty_classes++;
    }
  }

  return num_empty_classes;
}


/**
 * Performs kmedians clustering on the data field of the kmedians_data 
 * structure.
 *
 * If p->k_strict is 1, kmedians attempts to find only k clusters.
 *
 * Otherwise, if p->k_strict is 0 and k clusters could not be found, k - 1
 * clusters are tried.  This process continues until either a clustering
 * with no collapsed clusters is achieved, or k is 1.  In this case,
 * d->classes can be thought of as an upper-bound (maximum) on k.  When
 * done, d->classes will contain the final k found.
 *
 * Returns the number of empty classes if p->k_strict == 1, zero otherwise.
 *
 * Where:
 *   k     = d->classes
 *   datum = d->data[i][j]
 *   i     = [1 ... d->datapoints]
 *   j     = [1 ... d->features  ]
 *
 * @see kmedians_internal
 */
int kmedians(struct kmedians_data *d, struct kmedians_parameters *p)
{
  int empty_classes = 0;

  /*
   * Perform a single run of kmedians and save the result (whether or
   * not any empty classes occurred).
   */
  empty_classes = kmedians_internal(d, p);

  /*
   * If any empty classes occurred (empty_classes > 0) and k_strict is not
   * turned-on (p->k_strict == 0), then continually "step-down" k
   * (d->classes) until a suitable k is found or k is 1.
   */
  if (p->k_strict == 0)
  {
    while (empty_classes > 0)
    {
      d->classes--;

      if (DEBUG_KMEDIANS)
      {
        printf( "failed for k = %d, with empty_classes = %d.\n",
                d->classes + 1, empty_classes );
        printf( "Trying for k = %d.\n", d->classes );
      }

      if (d->classes > 1)
      {
        empty_classes = kmedians_internal(d, p);
      }
      else
      {
        zero_classifications(d);
        empty_classes = 0;
      }
    }
  }


  return empty_classes;
}


/**
 * Performs kmedians clustering on the data field of the kmedians_data 
 * structure.
 *
 * Returns the number of empty classes.  Zero indicates complete success,
 * since no collapsed clusters occurred.
 *
 * NOTE: This function is internal to kmedians.c and should only be called by
 * NOTE: kmedians().
 *
 * Where:
 *   k     = d->classes
 *   datum = d->data[i][j]
 *   i     = [1 ... d->datapoints]
 *   j     = [1 ... d->features  ]
 *
 * @see kmedians
 */
int kmedians_internal(struct kmedians_data *d, struct kmedians_parameters *p)
{
  int iterations       = 0;
  int restarts         = 0;
  int max_iterations   = p->max_iterations;
  int max_restarts     = p->max_restarts;
  int *classifications = (int *) NULL;
  int done             = 0;
  int empty_classes    = 0;


  if (DEBUG_KMEDIANS)
  {
    fprintf( stderr, "Entering function kmedians_strict().\n"    );
    fprintf( stderr, "  k              = %d\n", d->classes     );
    fprintf( stderr, "  max_iterations = %d\n", max_iterations );
    fprintf( stderr, "  max_restarts   = %d\n", max_restarts   );
  }

  classifications = NR_ivector(1, d->datapoints);

  /*
   * INIT_MEANS_CHURCH:
   *   If medians were initialized Church style, then restarting make no sense
   *   (Church style is completely deterministic), so turn restarting off by
   *   setting restarts to the maximum allowable.  Thus, the first for loop
   *   below will be executed only once.
   */
  if (p->init_medians == CHURCH_MEANS)
  {
    restarts = max_restarts;
  }

  /*
   * The termination condition for this loop, restarts <= max_restarts,
   * is correct.  Since the first time through the loop (restarts = 0)
   * is not a restart, but the first run of k-medians.
   */
  for (restarts = 0; !done && (restarts <= max_restarts); restarts++)
  {
    init_means(d->data, d->class_medians, d->datapoints, d->features, 
               d->classes, p->init_medians, p->seed, p->num_median_samples, 
               p->medians_filename);

    zero_classifications(d);

    for (iterations = 0; !done && (iterations < max_iterations); iterations++)
    {
      /*
       * Store the previous set of classifications, to test for
       * convergence later.
       */
      copy_classifications(d, classifications);

      classify_data(d, p);

      /*
       * If some clusters collapsed during classification, break out of
       * this set of kmedians iterations to restart.
       */
      empty_classes = get_num_empty_classes(d);
      if (empty_classes > 0)
      {
        break;
      }

      estimate_medians(d);

      /*
       * Test for convergence.  If the classifications have not changed
       * since the previous iteration of kmedians, we are done.
       *
       * The function compare_classifications() returns a zero if
       * kmedians_data->classifications and classifications are equal by 
       * content, greater than zero otherwise.
       */
      done = !compare_classifications(d, classifications);
    }
  }

  NR_free_ivector(classifications, 1, d->datapoints);

  if (DEBUG_KMEDIANS)
  {
    if (done)
    {
      fprintf( stderr, "Solution converged after\n" );
    }
    else
    {
      fprintf( stderr, "Solution DID NOT converge after\n" );
    }

    restarts--;

    fprintf( stderr, "  %d restart(s), and\n", restarts   );
    fprintf( stderr, "  %d iterations(s).\n" , iterations );

    /* print_data (stderr, d); */
    print_medians(stderr, d);

    fprintf(stderr, "Exiting function kmedians_strict().\n\n");
  }

  return empty_classes;
}


/**
 * Prints the data vectors in the kmedians_data structure to the given stream.
 */
void print_data(FILE *stream, struct kmedians_data *d)
{
  int i, j;


  fprintf(stream, "Data:\n");

  for (i = 1; i <= d->datapoints; i++)
  {
    for (j = 1; j <= d->features; j++)
    {
      fprintf(stream, "  %6.4f", d->data[i][j]);
    }

    fprintf(stream, "\n");
  }
}


/**
 * Pretty-prints the values of given kmedians_parameters structure to
 * the given stream.
 */
void print_kmedians_parameters(FILE *stream, struct kmedians_parameters *p)
{
  char *null             = "(null)";
  char *unknown          = "(unknown)";
  char *distance_metric  = unknown;
  char *init_medians     = unknown;
  char *dataset_filename = null;
  char *result_filename  = null;


  if (p->distance_metric == CORRELATION)
  {
    distance_metric = "correlation";
  }
  else if (p->distance_metric == EUCLIDEAN)
  {
    distance_metric = "euclidean";
  }


  if (p->init_medians == CHURCH_MEANS)
  {
    init_medians = "church_means";
  }
  else if (p->init_medians == RANDOM_MEANS)
  {
    init_medians = "random_means";
  }
  else if (p->init_medians == RANDOM_RANGE)
  {
    init_medians = "random_range";
  }
  else if (p->init_medians == RANDOM_SAMPLE)
  {
    init_medians = "random_sample";
  }


  if (p->dataset_filename != NULL)
  {
    dataset_filename = p->dataset_filename;
  }

  if (p->result_filename != NULL)
  {
    result_filename = p->result_filename;
  }


  fprintf( stream, "kmedians parameters:\n" );
  fprintf( stream, "  distance_metric    = [%s].\n" , distance_metric       );
  fprintf( stream, "  init_medians       = [%s].\n" , init_medians          );
  fprintf( stream, "  k                  = [%d].\n" , p->k                  );
  fprintf( stream, "  max_iterations     = [%d].\n" , p->max_iterations     );
  fprintf( stream, "  max_restarts       = [%d].\n" , p->max_restarts       );
  fprintf( stream, "  num_median_samples = [%d].\n" , p->num_median_samples );
  fprintf( stream, "  cols               = [%d].\n" , p->cols               );
  fprintf( stream, "  rows               = [%d].\n" , p->rows               );
  fprintf( stream, "  seed               = [%ld].\n", p->seed               );
  fprintf( stream, "  dataset_filename   = [%s].\n" , dataset_filename      );
  fprintf( stream, "  result_filename    = [%s].\n" , result_filename       );
}


/**
 * Prints the median vectors for each class in the kmedians_data structure to
 * the given stream.
 */
void print_medians(FILE *stream, struct kmedians_data *d)
{
  int i, j;


  fprintf(stream, "Medians:\n");

  for (i = 1; i <= d->classes; i++)
  {
    for (j = 1; j <= d->features; j++)
    {
      fprintf(stream, "  %6.4f", d->class_medians[i][j]);
    }

    fprintf(stream, "\n");
  }
}

/**
 * Zeros the classification field of the kmedians_data structure.
 */
void zero_classifications(struct kmedians_data *d)
{
  zero_1D_int_matrix(d->classifications, d->datapoints);
}


/**
 * Zeros the class_count_array field of the kmedians_data structure.
 */
void zero_class_counts(struct kmedians_data *d)
{
  zero_1D_int_matrix(d->class_count_array, d->classes);
}


/**
 * Zeros the class_medians field of the kmedians_data structure.
 */
void zero_class_medians(struct kmedians_data *d)
{
  zero_2D_double_matrix(d->class_medians, d->classes, d->features);
}
