/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * @author Ben Bornstein
 */

#ifndef __KMEDIANS_H
#define __KMEDIANS_H


#define DEBUG_KMEDIANS 1

struct kmedians_data
{
  int         datapoints;
  int         features;
  int         classes;
  int         *classifications;
  int         *class_count_array;
  double      **data;
  double      **class_medians;
  double      *feature_max;
  double      *feature_min;
  long        *seed;
};


struct kmedians_parameters
{
  int  distance_metric;
  int  init_medians;
  int  k;
  int  k_strict;
  int  max_iterations;
  int  max_restarts;
  int  num_median_samples;
  int  cols;
  int  rows;
  long seed;
  char *dataset_filename;
  char *result_filename;
  char *medians_filename;
};


/**
 * Safely allocates (via safe_malloc()) memory for a kmedians_data structure.
 * The resulting structure is initialized with values from the given
 * kmedians_parameters structure. A pointer to the structure is returned.
 *
 * @see safe_malloc
 */
struct kmedians_data *allocate_kmedians_data(struct kmedians_parameters *p);

/**
 * Safely allocates (via safe_malloc()) memory for a kmedians_parameters
 * structure.  The resulting structure is initialized with default values.
 * A pointer to the structure is returned.
 *
 * @see safe_malloc
 */
struct kmedians_parameters *allocate_kmedians_parameters(void);

/**
 * Returns 1 if the N element array contains value at least once, 0
 * otherwise.
 */
int array_contains(int *array, int N, int value);

/**
 * Classifies each datum from 1 to k according to the nearest median.
 *
 * Where:
 *   k           = d->classes
 *   medians     = d->class_medians
 *   memberships = d->classifications
 *   datum       = d->data[i][]
 *   i           = [0 ... (d->datapoints - 1)]
 *   j           = [0 ... (d->features   - 1)]
 */
void classify_data(struct kmedians_data *d, struct kmedians_parameters *p);

/**
 * Compares the contents of the classification field of the kmedians_data
 * structure to classifications.  If they are equal by content zero
 * is returned.  Otherwise, the number of elements by which they differ
 * is returned.
 */
int compare_classifications(struct kmedians_data *d, int *classifications);

/**
 * Copies the contents of the classification field of the kmedians_data
 * structure to destination.
 */
void copy_classifications(struct kmedians_data *d, int *destination);

/**
 * Copies the contents of source to destination,  both with N elements.
 */
void copy_datum(double *source, double *destination, int N);

/**
 * Computes the distance between the two given datapoints, both with N
 * elements.
 *
 * The distance is the square of the Euclidean distance.  Since, in k-medians,
 * distance is used only for comparison purposes, this is correct (and
 * faster than computing the true Euclidean distance).  That is, for
 * a, b > 0, a > b, implies sqrt(a) > sqrt(b).
 */
/*double distance(double *datum1, double *datum2, int N);*/

/**
 * Estimates the medians of k classes using the class membership of each
 * datum.
 *
 * Where:
 *   k           = d->classes
 *   memberships = d->classifications
 *   datum       = d->data[i][]
 *   i           = [0 ... (d->datapoints - 1)]
 *   j           = [0 ... (d->features   - 1)]
 */
void estimate_medians(struct kmedians_data *d);

/**
 * Frees the memory associated with the given kmedians_data structure.
 */
void free_kmedians_data(struct kmedians_data *d);

/**
 * Frees the memory associated with the given kmedians_parameters structure.
 */
void free_kmedians_parameters(struct kmedians_parameters *p);

/**
 * Returns the number of empty classes in d->class_count_array.
 */
int get_num_empty_classes(struct kmedians_data *d);

/**
 * Performs kmedians clustering on the data field of the kmedians_data 
 * structure.
 *
 * If p->k_strict is 1, kmedians attempts to find only k clusters.
 *
 * Otherwise, if p->k_strict is 0 and k clusters could not be found, k - 1
 * clusters are tried.  This process continues until either a clustering
 * with no collapsed clusters is achieved, or k is 1.  In this case,
 * d->classes can be thought of as an upper-bound (maximum) on k.  When
 * done, d->classes will contain the final k found.
 *
 * Returns the number of empty classes if p->k_strict == 1, zero otherwise.
 *
 * Where:
 *   k     = d->classes
 *   datum = d->data[i][j]
 *   i     = [0 ... (d->datapoints - 1)]
 *   j     = [0 ... (d->features   - 1)]
 *
 * @see kmedians_internal
 */
int kmedians(struct kmedians_data *d, struct kmedians_parameters *p);

/**
 * Performs kmedians clustering on the data field of the kmedians_data 
 * structure.
 *
 * Returns the number of empty classes.  Zero indicates complete success,
 * since no collapsed clusters occurred.
 *
 * NOTE: This function is internal to kmedians.c and should only be called by
 * NOTE: kmedians().
 *
 * Where:
 *   k     = d->classes
 *   datum = d->data[i][j]
 *   i     = [0 ... (d->datapoints - 1)]
 *   j     = [0 ... (d->features   - 1)]
 *
 * @see kmedians
 */
int kmedians_internal(struct kmedians_data *d, struct kmedians_parameters *p);

/**
 * Prints the data vectors in the kmedians_data structure to the given stream.
 */
void print_data(FILE *stream, struct kmedians_data *d);

/**
 * Pretty-prints the values of given kmedians_parameters structure to
 * the given stream.
 */
void print_kmedians_parameters(FILE *stream, struct kmedians_parameters *p);

/**
 * Prints the median vectors for each class in the kmedians_data structure to
 * the given stream.
 */
void print_medians(FILE *stream, struct kmedians_data *d);

/**
 * Zeros the classification field of the kmedians_data structure.
 */
void zero_classifications(struct kmedians_data *d);

/**
 * Zeros the class_count_array field of the kmedians_data structure.
 */
void zero_class_counts(struct kmedians_data *d);

/**
 * Zeros the class_medians field of the kmedians_data structure.
 */
void zero_class_medians(struct kmedians_data *d);


#endif
