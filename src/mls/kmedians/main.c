/**
 * Copyright 1999, California Institute of Technology.
 * ALL RIGHTS RESERVED.  U.S. Government Sponsorship acknowledged.
 *
 * @author Ben Bornstein
 */


/**
 * A short program usage statement.
 *
 * @see print_usage
 */
static char Usage[] = 
"\n"
"Usage:\n"
"  kmedians [options | --help] dataset_filename result_filename\n"
"\n"
"Use the --help option for full usage information.\n";


/**
 * A more complete program usage statement.
 *
 * @see print_usage
 */
static char Full_Usage[] = 
"\n" 
"Usage:\n"
"  kmedians [options | --help] dataset_filename result_filename\n"
"\n"
"Where:\n"
"  Options include the following and option names may be preceded by either\n"
"  a single or double dash (\"-\" or \"--\").  Unless a default is indicated\n"
"  the option is required.\n"
"\n"
"    --cols num_columns\n"
"        The number of columns to read in the dataset specified by\n"
"        dataset_filename.\n"
"\n"
"    --distance_metric metric\n"
"        Either the word correlation or euclidean.\n"
"\n"
"    --init_medians method\n"
"        The word church, random, random_range, or random_sample.\n"
"\n"
"    --k num_k\n"
"        The number of clusters, k, to find.\n"
"\n"
"    --k_strict\n"
"        If specified, kmedians will treat k as a strict parameter.  That\n"
"        is, if k clusters could not be found, (after an optional\n"
"        num_restarts, in the case of randomly initialized medians) no\n"
"        result will be reported.\n"
"\n"
"        Otherwise, kmedians will attempt to find k - 1 clusters.  This\n"
"        process of stepping-down k may continue until k is one.\n"
"\n"
"    --max_iterations num_iterations\n"
"        The maximum number of kmedians iterations.\n"
"\n"
"    --max_restarts num_restarts\n"
"        The maximum number of restarts in the case of collapsed clusters\n"
"        (valid only for randomly initialized medians).  Defaults to 0.\n"
"\n"
"    --num_median_samples num_samples\n"
"        If -init-medians is random_sample, num_samples indicates the number\n"
"        of datapoints to sample (without replacement) when estimating\n"
"        initial medians.  Defaults to 3.\n"
"\n"
"    --rows num_rows\n"
"        The number of rows to read in the dataset specified by\n"
"        dataset_filename.\n"
"\n"
"    --seed seed\n"
"        The seed to use for the pseudo-random number generator (valid only\n"
"        for randomly initialized medians).  Defaults to 42.\n";

#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>

#include "kmedians.h"
#include "main.h"
#include "util.h"

#include "dist_metrics.h"
#include "init_means.h"

#define OPTION_COLS               0
#define OPTION_DISTANCE_METRIC    1
#define OPTION_HELP               2 
#define OPTION_INIT_MEDIANS       3
#define OPTION_K                  4
#define OPTION_K_STRICT           5
#define OPTION_MAX_ITERATIONS     6
#define OPTION_MAX_RESTARTS       7
#define OPTION_NUM_MEDIAN_SAMPLES 8
#define OPTION_ROWS               9
#define OPTION_SEED              10
#define OPTION_MEDIAN_FILE       11
#define NUM_OPTIONS              12

static struct option Long_Options[] =
{
  { "cols"              , 1, NULL, OPTION_COLS               },
  { "distance_metric"   , 1, NULL, OPTION_DISTANCE_METRIC    },
  { "help"              , 0, NULL, OPTION_HELP               },
  { "init_medians"      , 1, NULL, OPTION_INIT_MEDIANS       },
  { "median_file"       , 1, NULL, OPTION_MEDIAN_FILE        },
  { "k"                 , 1, NULL, OPTION_K                  },
  { "k_strict"          , 0, NULL, OPTION_K_STRICT           },
  { "max_iterations"    , 1, NULL, OPTION_MAX_ITERATIONS     },
  { "max_restarts"      , 1, NULL, OPTION_MAX_RESTARTS       },
  { "num_median_samples", 1, NULL, OPTION_NUM_MEDIAN_SAMPLES },
  { "rows"              , 1, NULL, OPTION_ROWS               },
  { "seed"              , 1, NULL, OPTION_SEED               },
  { (char *) NULL       , 0, NULL, 0                         }
};


/**
 * Returns the name of the given option.
 */
char *get_option_name(int option)
{
  int  i;
  char *name = (char *) NULL;


  for (i = 0; i < NUM_OPTIONS; i++)
  {
    if (Long_Options[i].val == option)
    {
      name = (char *) Long_Options[i].name;
      break;
    }
  }

  return name;
}


/**
 * Returns 1 if the given option is required, 0 otherwise.
 */
int is_option_required(int option)
{
  return (option != OPTION_HELP)               &&
         (option != OPTION_K_STRICT)           &&
         (option != OPTION_MAX_RESTARTS)       &&
         (option != OPTION_NUM_MEDIAN_SAMPLES) &&
         (option != OPTION_SEED)               &&
         (option != OPTION_MEDIAN_FILE);
}


/**
 * Parses command-line options (using GNU getopt, part of glibc) and sets
 * the given kmedians_parameters structure accordingly.  In case of errors,
 * this function will print a usage statement (via print_usage()) and exit
 * the program.
 *
 * @see print_usage
 */
void parse_command_line_options(int argc, char *argv[],
                                struct kmedians_parameters *p)
{
  int fatal_error      = 0;
  int option           = 0;
  int option_index     = 0;
  int remaining        = 0;
  int print_full_usage = 0;

  int *option_seen     = NULL;

  option_seen = (int *) safe_calloc(sizeof(int), NUM_OPTIONS);

  while (1)
  {
    option = getopt_long(argc, argv, "", Long_Options, &option_index);

    if (option == -1)
    {
      break;
    }

    if (option < NUM_OPTIONS)
    {
      option_seen[option] = 1;
    }

    switch (option)
    {
      case OPTION_COLS:
        p->cols = atoi(optarg);
        break;

      case OPTION_DISTANCE_METRIC:
        if ( !strcmp(optarg, "correlation") )
        {
          p->distance_metric = CORRELATION;
        }
        else if ( !strcmp(optarg, "euclidean") )
        {
          p->distance_metric = EUCLIDEAN_SQUARED;
        }
        else
        {
          printf( "%s: option '-distance_metric' must have a  ", argv[0] );
          printf( "value of either correlation or euclidean.\n"          );
          fatal_error = 1;
        }
        break;

      case OPTION_INIT_MEDIANS:
        if ( !strcmp(optarg, "church") )
        {
          p->init_medians = CHURCH_MEANS;
        }
        else if ( !strcmp(optarg, "random") )
        {
          p->init_medians = RANDOM_MEANS;
        }
        else if ( !strcmp(optarg, "random_range") )
        {
          p->init_medians = RANDOM_RANGE;
        }
        else if ( !strcmp(optarg, "random_sample") )
        {
          p->init_medians = RANDOM_SAMPLE;
        }
        else if ( !strcmp(optarg, "file") )
        {
          p->init_medians = FROM_FILE;
        }
        else
        {
          printf( "%s: option '-init_medians' must have a ", argv[0] );
          printf( "value of church, random, random_range, or "       );
          printf( "random_sample.\n"                                 );
          fatal_error = 1;
        }
        break;
      
      case OPTION_MEDIAN_FILE:
        p->medians_filename = strdup( optarg );
        break;

      case OPTION_K:
        p->k = atoi(optarg);
        break;

      case OPTION_K_STRICT:
        p->k_strict = 1;
        break;

      case OPTION_MAX_ITERATIONS:
        p->max_iterations = atoi(optarg);
        break;

      case OPTION_MAX_RESTARTS:
        p->max_restarts = atoi(optarg);
        break;

      case OPTION_NUM_MEDIAN_SAMPLES:
        p->num_median_samples = atoi(optarg);
        break;

      case OPTION_ROWS:
        p->rows = atoi(optarg);
        break;

      case OPTION_SEED:
        p->seed = atol(optarg);
        break;

      case OPTION_HELP:
        fatal_error      = 1;
        print_full_usage = 1;
        break;

      case '?':
      default:
        fatal_error = 1;
        break;
    }
  }


  /*
   * Ensure required options are not missing.
   */
  for (option = 0; option < NUM_OPTIONS; option++)
  {
    if (is_option_required(option) && (option_seen[option] == 0))
    {
      fatal_error = 1;
      printf( "%s: ", argv[0] );
      printf( "option '-%s' is required.\n", get_option_name(option) );
    }
  }


  /*
   * Dataset and result filenames
   */
  remaining = argc - optind;

  if (remaining == 2)
  {
    p->dataset_filename = strdup( argv[optind]     );   
    p->result_filename  = strdup( argv[optind + 1] );
  }
  else if (remaining > 2)
  {
    printf( "%s: too many filename arguments.\n", argv[0] );
    fatal_error = 1;
  }
  else
  {
    if (remaining == 0)
    {
      printf( "%s: dataset_filename is required.\n", argv[0] );
    }

    printf( "%s: result_filename  is required.\n", argv[0] );
    fatal_error = 1;
  }

  if (fatal_error == 1)
  {
    printf( print_full_usage ? Full_Usage : Usage );
    exit(-2);
  }

  free(option_seen);
}


/**
 * Reads the contents of p->dataset_filename into d->data[][].  Only
 * p->rows rows and p->cols columns will be read.
 */
void read_dataset(struct kmedians_data *d, struct kmedians_parameters *p)
{
  FILE *fp;
  int r, c;


  fp = safe_fopen( p->dataset_filename, "r" );


  for (r = 1; r <= p->rows; r++)
  {
    for (c = 1; c <= p->cols; c++)
    {
      fscanf( fp, " %lf ", &(d->data[r][c]) );
    }
  }

  fclose(fp);
}


/**
 * Writes the contents of d->classifications[] (one per line) to
 * p->result_filename.  Only d->datapoints lines will be written.
 */
void write_result(struct kmedians_data *d, struct kmedians_parameters *p)
{
  FILE *fp;
  int i;


  fp = safe_fopen( p->result_filename, "w" );

  for (i = 1; i <= d->datapoints; i++)
  {
    fprintf(fp, "%d\n", d->classifications[i]);
  }

  fclose(fp);
}

/**
 * Provides a command-line interface to the kmedians() function.
 * See Usage and Full_Usage for more details.
 *
 * Returns (to the operating system):
 *
 *   The number of empty classes (collapsed clusters) that occurred as a
 *   result of this kmedians run.  This number can only be greater than zero
 *   when --k-strict is specified.  A value of zero indicates success.
 *   That is, no empty classes occurred.
 *    
 *   -1  If there was a fatal internal error (e.g. malloc() or fopen()
 *       failed.)
 *
 *   -2  If there was an error reading command-line options.
 */
int main(int argc, char *argv[])
{
  struct kmedians_data       *d = NULL;
  struct kmedians_parameters *p = NULL;

  int empty_classes = 0;


  p = allocate_kmedians_parameters();
  parse_command_line_options(argc, argv, p);

  /* print_kmedians_parameters(stdout, p); */

  d = allocate_kmedians_data(p);

  read_dataset(d, p);
  empty_classes = kmedians(d, p);

  /**
   * Only output a result if --k-strict was not specified or if
   * it was specified, there must be no empty_classes.
   */
  if ( (p->k_strict == 0) || (empty_classes == 0) )
  {
    write_result(d, p);
  }

  free_kmedians_parameters(p);
  free_kmedians_data(d);

  return empty_classes;
}
