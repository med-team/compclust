CURDIR := $(MLSDIR)/kmedians

KMEDIANSRC := $(wildcard $(CURDIR)/*.c)
SRC += $(KMEDIANSRC)
CFLAGS += -I$(CURDIR)

KMEDIANLIBS = $(BASEDIR)/da/libda$(LIBEXT) \
            $(BASEDIR)/nr/libnr$(LIBEXT) \
	    $(BASEDIR)/ut/libut$(LIBEXT) \
	    $(MLSDIR)/common/libcommon$(LIBEXT) \
	    $(MLSDIR)/normalizations/libnorm$(LIBEXT) \
	    $(MLSDIR)/distances/libdist$(LIBEXT) \
	    $(MLSDIR)/initializations/libinit$(LIBEXT) \
            $(MLSDIR)/getopt/libgetopt$(LIBEXT)
KMEDIANLIBDIRFLAGS := $(call make_libdirflag,$(KMEDIANLIBS))
KMEDIANLIBLINKFLAGS := $(call make_liblinkflag,$(KMEDIANLIBS)) -lm

KMEDIANS := $(CURDIR)/kmedians$(BINEXT)
TARGETBINS += $(KMEDIANS)

$(KMEDIANS): $(KMEDIANSRC:.c=$(OBJEXT)) $(KMEDIANLIBS)
	$(CC) $(KMEDIANLIBDIRFLAGS) -o $@ $(KMEDIANSRC:.c=$(OBJEXT)) $(KMEDIANLIBLINKFLAGS)
