/******************************************************************************
 *
 * build_anchor_heirarchy(double **data, int rows, int cols)
 *
 * Builds an anchor heirarchy from the given dataset.
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "nr.h"
#include "da_util.h"
#include "anchor-tree.h"

#define DEBUG 0
#define dprintf if (DEBUG) printf

void dump_anchor_pair(Anchor *a1, Anchor *a2, struct anchor_mem *m);
void print_anchor(FILE *fp, Anchor *anchor, struct anchor_mem *m);
void recompute_and_sort(Anchor *anchor, struct anchor_mem *m);
void recompute_distances(Anchor *anchor, struct anchor_mem *m);
void sort_datapoints(Anchor *anchor, struct anchor_mem *m);
int PtInfo_compar(const void *s1, const void *s2);
inline int sign(double val);

PtInfo _find_closest(double *z, PtInfo best, AnchorNode *tree, 
                     struct anchor_mem *m, double d_node);
PtInfo _find_nn(double *z, PtInfo best, AnchorNode *tree, 
                struct anchor_mem *m, double d_node, int exclude);
PtInfo _find_furthest(double *z, PtInfo worst, AnchorNode *tree, 
                      struct anchor_mem *m, double d_node);

PtInfo ping_pong_search(double *z, double dist, Anchor *anchor, PtInfo best, 
                        struct anchor_mem *m, int ignore);

/******************************************************************************
 *
 * new_anchor()
 *
 * Allocates a new anchor object along with any necessary initialization
 *
 *****************************************************************************/

Anchor *new_anchor(struct anchor_mem *m)
{
  Anchor *anchor = NULL;

  if ((anchor = (Anchor *) malloc(sizeof(Anchor))) != NULL) {

    anchor->pivot  = NULL;
    anchor->owned  = NULL;
    anchor->num    = 0;
    anchor->radius = 0.0;
    anchor->inner  = 0.0;

    if ((anchor->pivot = NR_dvector(1, m->features)) == NULL) {
      free(anchor);
      anchor = NULL;
    }
  }

  return anchor;
}

void delete_anchor(Anchor *anchor, struct anchor_mem *m)
{
  if (anchor->pivot != NULL) 
    NR_free_dvector(anchor->pivot, 1, m->features);

  if (anchor->owned != NULL)
    free(anchor->owned);

  free(anchor);
}

/******************************************************************************
 *
 * isleaf()
 *
 * Simple function to determine if a node is a leaf
 *
 *****************************************************************************/

inline int isleaf(AnchorNode *node)
{
  return (node && !node->child1 && !node->child2); 
}

/******************************************************************************
 *
 * Set of functions for sorting the datapoints owned by the anchor
 *
 * recompute_and_sort()
 *
 * Wrapper around recompute_distances() and sort_datapoints() to perform
 * both operations in one go.
 *
 *
 * recompute_distances()
 *
 * Compute and cache the distance from each owned point to the pivot.  Use
 * This function when there is nothing known about the owned points.
 *
 *
 * sort_datapoints()
 *
 * Sorts the datapoints owned by an anchor by distance from the pivot which
 * is caches in the owned[].dist field.  Make sure that these values are
 * not stale or the data will not be sorted properly.
 *
 *
 * PtInfo_compar()
 *
 * Comparison function used by qsort()
 *
 *
 * sign()
 *
 * Helper function for PtInfo_compar()
 *
 *****************************************************************************/

void recompute_and_sort(Anchor *anchor, struct anchor_mem *m)
{
  recompute_distances(anchor, m);
  sort_datapoints(anchor, m);
}

void recompute_distances(Anchor *anchor, struct anchor_mem *m)
{
  int i;
  PtInfo *owned;
  
  /* Compute all the distances (assume they are invalid) */
  /* owned[i].index must be >= 0                         */
  
  owned = anchor->owned;
  for (i = 0; i < anchor->num; i++) 
    owned[i].dist = m->dist_metric(anchor->pivot, m->data[owned[i].index], 
                                   m->features);
}

void sort_datapoints(Anchor *anchor, struct anchor_mem *m)
{
  /* Sort the datapoints by distance.  This will work, because C   */
  /* struct assignment copies the struct.                          */
  qsort(anchor->owned, anchor->num, sizeof(PtInfo), PtInfo_compar);

  /* Assign the radius as the distance to the furthest owned point */
  anchor->radius = anchor->owned[anchor->num - 1].dist;
  anchor->inner  = anchor->owned[0].dist;
}

int PtInfo_compar(const void *s1, const void *s2)
{
  double diff;

  diff = ((PtInfo *) s1)->dist - ((PtInfo *) s2)->dist;
  return (diff == 0.0)?0:sign(diff);
}

inline int sign(double val)
{
  return (val < 0.0)?-1:1;
}

/******************************************************************************
 *
 * steal_points()
 *
 * Moves points from src to dest for the points which are closer to the
 * pivot of dest than src.  The points are first added to a linked list,
 * then the correct amount of memory is allocated for the new anchor and
 * the point are assigned.  They are then sorted.
 *
 *****************************************************************************/

void steal_points(Anchor *dest, Anchor *src, struct anchor_mem *m)
{
  double dist, d_dist;
  unsigned int count = 0;
  int i, index, base, total;
  PtInfo *owned, *temp;

  /* find the halfway point between the anchors                        */

  dist  = 0.5 * m->dist_metric(src->pivot, dest->pivot, m->features);  

  /* Early out test to see if it even worth considering this anchor    */

  i     = src->num - 1;
  owned = src->owned;

  if (dist <= owned[i].dist) {

    /* Start at the furthest point owned by src. Compute distance to   */
    /* dest.  If less than 'dist', move it into a temporary list.      */
    /* Mark the slot in src with a -1 so we can easily compact memory  */
    /* at the end.                                                     */
    /*                                                                 */
    /* We could (theoretically) take away all the points except the    */
    /* pivot (think of a circle with the pivot at the mean), so we     */
    /* need to expand the owned array by src->num - 1.  We'll resize   */
    /* at the end when we know exactly how many elements we really     */
    /* moved.                                                          */

    count  = dest->num;
    total  = dest->num + (src->num - 1);
    temp   = (PtInfo *) realloc(dest->owned, total * sizeof(PtInfo));

    /* Search for the points to steal, also test to see if we should   */
    /* continue examining points.                                      */
 
    for ( ; i > 0; i--) {
      if (owned[i].dist < dist) {
        break;
      }
      
      /* owned[i].index must be >= 0 */

      index  = owned[i].index;
      d_dist = m->dist_metric(dest->pivot, m->data[index], m->features);  

      /* compare the distance to the destination anchor to the cached  */
      /* distance to the source anchor.  If the point is closer to the */
      /* destination, move it over.                                    */

      if (d_dist <= owned[i].dist) {
        temp[count].index = index;
        temp[count].dist  = d_dist;
        owned[i].index    = -1;
        count += 1;
      }
    }

    /* Compact the the data in src */

    base = i;
    for ( ; i < src->num ; i++) {
      if (owned[i].index == -1)
        continue;

      owned[base++] = owned[i];
    }

    /* Now realloc src and dest to their correct (compact) sizes.     */
    /* Also compute the new radius parameter for the src.             */
     
    src->owned  = (PtInfo *) realloc(src->owned, base * sizeof(PtInfo));
    src->num    = base;
    src->radius = src->owned[base - 1].dist;

    dest->owned = (PtInfo *) realloc(temp, count * sizeof(PtInfo));
    dest->num   = count;
  } 
}

/******************************************************************************
 *
 * add_anchor()
 *
 * Adds an new anchor to a list. Each anchor a[i] has a pivot and a list of
 * points which are closer to a[i] than any other anchor by the given
 * distance metric.
 *
 * This list is kept in sorted order by distance with the points closest to
 * a[i] at the front of the list.
 *
 * When a new anchor is added, a[new] is set to the datapoint furthest from
 * the anchor which has the largest radius.  Each point which is now closer 
 * to the new anchor than any other anchor is added to the a[new].  Since
 * the datapoints are ordered by distance from their respecitve anchors,
 * this is a very fast operation
 *
 * Note that add_anchor assumes that there is at least one anchor in the
 * heap.  The common thing to do is initially set the entire dataset as one 
 * large anchor.
 *
 *****************************************************************************/
 
void add_anchor(AnchorHeap *a_heap, struct anchor_mem *m)
{
  Anchor *anchor, *a_maxrad;
  unsigned int i;
  double radius;

  anchor = new_anchor(m);

  if (anchor != NULL) {
    
    /* retrieve the anchor with the largest radius */

    a_maxrad = a_heap->heap[a_heap->maxrad];

    /* Have the largest anchor, now get it's furthest point.  The     */
    /* actual movement of points and memory allocation will happen in */
    /* the steal_points() subroutine.                                 */
     
    i = a_maxrad->owned[a_maxrad->num - 1].index;
    copy_dvec(m->data[i], anchor->pivot, m->features);

    /* Now steal all the points from every anchor which are closer to */
    /* the new anchor than their current anchor.  This is also fast.  */
    /* If the radius of an anchor is less than                        */
    /* 0.5 * || anchor_maxrad - anchor_n || then no points can be     */
    /* taken.  If it is greater than half the distance, only those    */
    /* points beyond the boundry need to be considered.               */
    /*                                                                */
    /* Since radius' change, we need to re-track the maxrad anchor    */

    radius = 0.0;
    for (i = 0; i < a_heap->n; i++) {

      a_maxrad = a_heap->heap[i];
      steal_points(anchor, a_maxrad, m);

      if (a_maxrad->radius > radius) {
        radius = a_maxrad->radius;
        a_heap->maxrad = i;
      }
    }

    /* All the points have been allocated to the new anchor, so now   */
    /* append it to the heap and update the heap statistics           */
    /*                                                                */
    /* If no points were added to the new anchor, do nothing.         */

    if (anchor->num > 0) {
      a_heap->heap[i] = anchor;
      a_heap->n       = i+1;
      sort_datapoints(anchor, m);

      /* check to see if the new anchor has the max. radius */
      if (anchor->radius > a_heap->heap[a_heap->maxrad]->radius) {
        a_heap->maxrad = i;
      }
    }
  }
}

/******************************************************************************
 *
 * merge_nodes
 * agglomerate
 *
 * Merges anchors together by minimum radius.  This is rather difficult,
 * because as anchors are merged, the inter-anchor distances change and
 * need updating.  This makes the agglomeration N^3 in the worst case.
 *
 *****************************************************************************/

AnchorNode *merge_nodes(AnchorNode *n1, AnchorNode *n2, struct anchor_mem *m)
{
  PtInfo point;
  AnchorNode *node;
  Anchor *anchor, *anchor_i, *anchor_j;
  double r_i, r_j, *pivot_i, *pivot_j;
  
  node   = (AnchorNode *) malloc(sizeof(AnchorNode));
  anchor = new_anchor(m);

  /* fill the fields of the node */
  node->parent = NULL;
  node->child1 = n1;
  node->child2 = n2;
  node->anchor = anchor;
  node->child1->parent = node;
  node->child2->parent = node;
  
  anchor_i = n1->anchor;
  anchor_j = n2->anchor;

  pivot_i  = anchor_i->pivot;
  pivot_j  = anchor_j->pivot;

  anchor->num = anchor_i->num + anchor_j->num;

  /* To find the proper position in space, we compute the      */
  /* weighted mean of the two pivots.  This may not be the     */
  /* correct way to solve this.  Since we can use any distance */
  /* metric which satisfies the Triangle Inequality, we may    */
  /* have to interatively solve for the optimal pivot point.   */
  /* (use Newton interation?  Is there a closed-form           */
  /* exporession?)                                             */
  /*                                                           */
  /* Q: Does taking a weighted mean optimize the placement     */
  /*    of the new pivot subject only to the constraints of    */
  /*    the Triangle Inequality?                               */
  /*                                                           */
  /* A: In a general metric space the mean of two vectors is   */
  /*    a meaningless operation.  However, since we generally  */
  /*    operate in a vector space, it is meaningful to us.     */
  /*    Thus each distance metric must have a weighted_mean()  */
  /*    operation as well to perform this op.                  */
  
  r_i     = anchor_i->radius;
  r_j     = anchor_j->radius;
  
  if (r_i == 0.0 || r_j == 0.0)
    r_i = r_j = 1.0;
  
  /* create an optimal anchor ... */
  m->weighted_mean(r_i, pivot_i, r_j, pivot_j, anchor->pivot, m->features);
  
  /* ... and find the inner and outer radius.  We don't need to    */
  /* consider all the datapoints, just find the min/max distance   */
  /* from the new pivot to the points in the child anchors.        */
  
  point = find_closest(anchor->pivot, node, m);
  anchor->inner = point.dist;
  
  point = find_furthest(anchor->pivot, node, m);
  anchor->radius = point.dist;
  
  return node;
}

AnchorNode *agglomerate(AnchorNode **list, int n, struct anchor_mem *m)
{
  PtInfo *nn = NULL;
  Anchor *a_i, *a_j;
  double dist, min_dist;
  int i, j, closest, min_index;

  nn = (PtInfo *) malloc(sizeof(PtInfo) * n);

  /* This is a brute-force N^3 algorith, it needs to be improved */

  while (n > 1) {

    /* We don't need to store a complete matrix, just a list of the */
    /* closest anchor to each anchor.                               */
    
    for (i = 0; i < n - 1; i++) {
      
      /* Initialize with the next anchor distance */
      a_i = list[i]->anchor;
      a_j = list[i+1]->anchor;
      
      min_dist  = a_i->radius + a_j->radius + 
        m->dist_metric(a_i->pivot, a_j->pivot, m->features);
      
      min_index = i+1;
      
      /* now run through the rest and see if any are closer */
      for (j = i+2 ; j < n ; j++) {
        a_j  = list[j]->anchor;
        dist = a_i->radius + a_j->radius + 
          m->dist_metric(a_i->pivot, a_j->pivot, m->features);
        
        if (dist < min_dist) {
          min_dist  = dist;
          min_index = j;
        }
      }
      
      /* store the closest point in the list */
      
      nn[i].dist  = min_dist;
      nn[i].index = min_index;
    } 

    /* Now scan the list to find the best pair.  This pair will always  */
    /* be merged, so we will always make some progress.                 */
    
    closest = 0;
    for (i = 1; i < n - 1; i++) {
      if (nn[i].dist < nn[closest].dist) {
        closest  = i;
      }
    }
    
    /* closest has the anchor with the min dist, so merge them */
    list[closest] = merge_nodes(list[closest], list[nn[closest].index], m);
    
    /* remove its mate from the working set */
    for (i = nn[closest].index; i < n - 1; i++) {
      list[i] = list[i+1];
      nn[i]   = nn[i+1];
    }
    
    /* decrement the number of nodes in the list */
    n -= 1;
  }

  /* free our memory */
  free(nn);

  /* we end with a single value in the heap */
  return list[0];
}

/******************************************************************************
 *
 * _build_anchor_tree:
 * 
 * Build the full anchor tree as described by Moore, Section 3.
 *
 *****************************************************************************/

AnchorNode *_build_anchor_tree(Anchor *anchor, struct anchor_mem *m)
{
  int i, num_anchors, n;
  AnchorHeap *a_heap = NULL;
  AnchorNode **node_list = NULL, *node = NULL;

  n = anchor->num;

  dprintf("Entering _build_anchor_tree with %d datapoints\n", n);
  
  /* test for the base case of the recursion */
  if (n <= m->r_min || n == 1) {
    node = (AnchorNode *) malloc(sizeof(AnchorNode));
    node->parent = node->child1 = node->child2 = NULL;
    node->anchor = anchor;
    return node;
  }

  /* must add 1 to prevent infinite recursion */
  num_anchors = sqrt(n);
  if (num_anchors == 1)
    ++num_anchors;

  a_heap       = (AnchorHeap *) malloc(sizeof(AnchorHeap));
  a_heap->heap = (Anchor **) malloc(sizeof(Anchor *) * num_anchors);

  /* add the root anchor to the heap */
  a_heap->heap[0] = anchor;
  a_heap->n       = 1;
  a_heap->maxrad  = 0;

  /* build the heap of anchors */
  for (i = 1; i < num_anchors; i++) {
    add_anchor(a_heap, m);
  }

  /* recurse on each of the anchors with num > r_min to get a   */
  /* the leaf nodes.                                            */
  
  node_list = (AnchorNode **) malloc(a_heap->n * sizeof(AnchorNode *));
  for (i = 0; i < a_heap->n; i++) 
    node_list[i] = _build_anchor_tree(a_heap->heap[i], m);

  /* merge the leaf nodes into a full tree.  This is an O(n^3)  */
  /* operation but since n = sqrt(n), n^3 ~= n^1.5, and, by     */
  /* being clever, I _think_ we can reduce this to a N^2 log N  */
  /* operation -> N log N                                       */
  /*                                                            */
  /* Note that we do not have to compute the full radius in the */
  /* loop because the radius of the source node (node_list[i])  */
  /* is constant.                                               */
  /*                                                            */
  /* The pivot points for the agglomerated anchors are virtual  */
  /* datapoints.  These are simply defined as the midpoint of   */
  /* segments connecting the two anchors.                       */

  node = agglomerate(node_list, a_heap->n, m);

  /* free the memory ... */
  free(node_list);
  free(a_heap->heap);
  free(a_heap);

  /* ... and return the root of the tree */
  return node;
}

/******************************************************************************
 *
 * build_anchor_tree()
 *
 * Start off the process of building the anchor tree by first constructing
 * a single anchor with all N datapoints.  This function can handle a
 * dataset with NULL datapoints.
 *
 *****************************************************************************/

void fill_index2node(AnchorNode *tree, struct anchor_mem *m)
{
  int i;
  Anchor *anchor;
  PtInfo *owned;

  if (isleaf(tree)) {
    anchor = tree->anchor;
    owned  = anchor->owned;
    for (i = 0; i < anchor->num; i++) 
      m->index2node[owned[i].index - 1] = tree;
    return;
  }
  
  fill_index2node(tree->child1, m);
  fill_index2node(tree->child2, m);
}

AnchorNode *build_anchor_tree(struct anchor_mem *m)
{
  Anchor *anchor   = NULL;
  AnchorNode *tree = NULL;
  int i, j, true_num;

  /* First count the actual number of datapoints */
  true_num = 0;
  for (i = 1; i <= m->datapoints; i++)
    if (m->data[i] != NULL)
      ++true_num;

  if (true_num > 0) {
    /* Place the full dataset in an initial anchor and sort it */
    anchor        = new_anchor(m);
    anchor->num   = true_num;
    anchor->owned = (PtInfo *) malloc(true_num * sizeof(PtInfo));
    
    /* search for the first, valid datapoint to use as an anchor */
    for (i = 1; i <= m->datapoints; i++) {
      if (m->data[i] != NULL) {
        copy_dvec(m->data[i], anchor->pivot, m->features); 
        break;
      }
    }

    m->index2node = 
      (AnchorNode **) malloc(sizeof(AnchorNode *) * m->datapoints);

    /* place all the valid points in the first anchor */
    j = 0;
    for (i = 1; i <= m->datapoints; i++) {
      m->index2node[i-1] = NULL;
      if (m->data[i] != NULL) {
        anchor->owned[j++].index = i;
      }
    }
     
    /* sort the datapoints */
    recompute_and_sort(anchor, m);
    
    /* construct the tree */
    tree = _build_anchor_tree(anchor, m);
    
    /* now fill in the index->AnchorNode mapping */
    fill_index2node(tree, m);
  }
  return tree;
}
/*****************************************************************************
 *
 * insert
 *
 * inserts an index point into the tree.  Quite simple.  Find the closest
 * point and add it to that anchor.  The adjust the radius' if needed
 *
 ****************************************************************************/

void insert(int index, AnchorNode *tree, struct anchor_mem *m)
{
  
} 

/*****************************************************************************
 *
 * ping_pong_search
 *
 * Performs a Ping-Pong search on the owned points.  I cannot find any
 * reference online to this type of search, so I will briefly describe it.
 *
 *   Use a binary search to find a 'best guess' index in the sorted array.
 *   Now look at the items on the left and right side in alternating order
 *   until the search criteria is fullfilled.
 *
 * This is a search to find an item when there is incomplete information.
 *
 * A minor extension is that it can take a point to ignore
 *
 ****************************************************************************/

static inline int next_seq(int i)
{
  return i = -i + 1 - ((i & 1) << 1);
}

PtInfo ping_pong_search(double *z, double dist, Anchor *anchor, PtInfo best, 
                        struct anchor_mem *m, int ignore)
{
  PtInfo *owned;
  int n, done, i, mid, low, size;
  int stop, step, index;
  double d_test;

  owned = anchor->owned;

  /* Find the best starting point via binary search */
  size = anchor->num - 1;    
  low  = 0;
  mid  = 0;
  
  while (low <= size) {
    mid = ((low + size) / 2);
    if (dist < anchor->owned[mid].dist) 
      size = mid - 1;      
    else if (dist > anchor->owned[mid].dist) 
      low = mid + 1;
    else 
      break;
  }
  
  /* Now depending on how the binary search fared we could be on, */
  /* left of, or right of our closest potential point, so we      */
  /* must make sure to search those three points.                 */
  /*                                                              */
  /* mid will always be a valid value within the array.           */
  
  size = anchor->num;
  n    = mid;
  i    = 1;
  
  /* Unconditionally test the first point */

  index = owned[n].index;
  if (index != ignore && fabs(dist - owned[n].dist) < best.dist) {
    d_test = m->dist_metric(z, m->data[index], m->features);
    if (d_test < best.dist) {
      best.dist = d_test;
      best.index = index;
    }
  }
  
  done = 0;
  
  /* Now start ping-ponging */
  for (;;) {
    n += i;
    
    /* Range check on n */
    if (n < 0 || n >= size) { 
      done = 1; 
      break; 
    }
    
    /* If we cannot improve in this direction say, done ping-ponging */
    if (fabs(dist - owned[n].dist) >= best.dist) { 
      done = 1; 
      break; 
    }
    
    index = owned[n].index;

    if (index != ignore) {
      /* Otherwise check to see if we can improve */
      d_test = m->dist_metric(z, m->data[index], m->features);
      if (d_test < best.dist) {
        best.dist = d_test;
        best.index = index;
      }
    }
    
    /* go to the next position on the right or left */
    i = next_seq(i);
  } 
  
  /* If we came out of the loop with done set, then do a scan in one */
  /* direction, otherwise just return our best.                      */

  if (done != 0) {

    /* bound to the n which is _not_ invalid */
    i = next_seq(i);
    n += i;
    
    /* if i > 0, we're scanning right, else left */
    
    stop = size; step = 1;
    if (i < 0) { 
      stop = -1; step = -1; 
    }

    for ( ; n != stop ; n += step ) {
      index = owned[n].index;
      if (index == ignore)
        continue;

      if (fabs(dist - owned[n].dist) >= best.dist)
        break;
      d_test = m->dist_metric(z, m->data[index], m->features);
      if (d_test < best.dist) {
        best.dist = d_test;
        best.index = index;
      }
    }
  }
    
  return best;
} 

/*****************************************************************************
 *
 * find_closest()
 *
 * Finds the index and distance of the closest point to the datum.
 * 
 *****************************************************************************/

PtInfo find_closest(double *z, AnchorNode *tree, struct anchor_mem *m)
{
  PtInfo point;
  double dist;

  point.dist  = 1e38;
  point.index = -1;
  dist = m->dist_metric(z, tree->anchor->pivot, m->features);

  return _find_closest(z, point, tree, m, dist);
}

PtInfo _find_closest(double *z, PtInfo best, AnchorNode *tree, 
                    struct anchor_mem *m, double d_node)
{
  PtInfo temp, *owned;
  Anchor *a1, *a2;
  double d1, d2, dist; 
  double d1_best, d1_worst;
  double d2_best, d2_worst;
  int try_1, try_2;

  /* should not get a NULL pointer, indicate error as best we can */
  if (tree == NULL) 
    return best;

  /* If we are at a leaf, simply search for the closest point in this */
  /* anchor.  One trick to speed this up is to take advantage of the  */
  /* fact that the distances to the pivot are cached.  Compute the    */
  /* distance from z to the pivot.  Now, start at the beginning or    */
  /* end of the owned points depending on whether D(z, pivot) is      */
  /* greater or less than half the radius.                            */
  /*                                                                  */
  /* Now, compute the distance from z to the first point and keep it. */
  /* Then, for each point, Look up its distance from the pivot.       */
  /* Since we do not know the exact position of the point, we can     */
  /* assume it lies somewhere on a concentric ring that distance from */
  /* the pivot point.  At best, this point x, z, and the pivot could  */
  /* be colinear.  Then D(x,z) = abs(D(z,pivot) - D(x,pivot)).  Since */
  /* we already know D(z, pivot) and D(x, pivot), this is a fast      */
  /* test.  If the new point _could_ be closer, actually compute the  */
  /* true D(x,z) and save it if it is indeed smaller than the         */
  /* previous best.                                                   */
  /*                                                                  */
  /* Now, since the points are sorted by distance, as soon as we      */
  /* reach a point where the best possible distance is _greater_ than */
  /* the best distance found so far, we can stop since there can be   */
  /* no other point in the anchor that can be closer.                 */
  /*                                                                  */
  /* Once we have found the point and the distance, return them.      */
  /* This is the base case of the recursion.                          */

  if (isleaf(tree)) {
    a1    = tree->anchor;
    owned = a1->owned;

    dprintf("Scanning anchor with pivot: %d\n", 0);
    dprintf("  Best distance: %f\n", best.dist); 
    dprintf("  D(z, pivot):   %f\n", d_node); 
    dprintf("  Num owned:     %d\n", a1->num);
    dprintf("  owned:         %p\n", owned);
    dprintf("  child1:        %p\n", tree->child1);
    dprintf("  child2:        %p\n", tree->child2);

    /* include all points in the search */
    return ping_pong_search(z, d_node, a1, best, m, -1);
  }

  /* There are three cases to consider here.  Given our point, z, and */
  /* the two pivot points, x and y, we compute the two distances      */
  /* d1 = D(x,z) and d2 = D(y,z).  Now compute four quantities.       */
  /*                                                                  */
  /*   d1,best  = d1 - r1                                             */
  /*   d1,worst = d1 + r1'                                            */
  /*   d2,best  = d2 - r2                                             */
  /*   d2,worst = d2 + r2'                                            */
  /*                                                                  */
  /* where r1 and r2 are the radii of the two children and r1' and    */
  /* r2' are the distances to the closest datapoints owned by the     */
  /* anchor.                                                          */
  /*                                                                  */
  /* These quantities represent the shortest and largest distances    */
  /* from the query point z to it's nearest neighbor in each child.   */
  /*                                                                  */
  /* Now, the three cases are:                                        */
  /*   d1,best > d2,worst -- pick child 2                             */
  /*   d2,best > d1,worst -- pick child 1                             */
  /*   else look at both.                                             */
  /*                                                                  */
  /* Remember that d1,* and d2,* can have negative values when the    */
  /* point is within the radius.                                      */
  /*                                                                  */
  /* If we have to check both sides, evaluate the side with the best  */
  /* *,best value.  Then determine if the distance returned could     */
  /* possibly be bested by the other side.  If not, don't go down     */
  /* that branch.                                                     */

  a1 = tree->child1->anchor;
  a2 = tree->child2->anchor;

  dprintf("  child1->pivot = %p\n", a1->pivot);
  dprintf("  child2->pivot = %p\n", a2->pivot);
  dprintf("  test point z  = %p\n", z);

  d1 = m->dist_metric(a1->pivot, z, m->features);
  d2 = m->dist_metric(a2->pivot, z, m->features);

  dprintf("  D(z, child1) = %f\n", d1);
  dprintf("  D(z, child2) = %f\n", d2);

  dprintf("  child1->num    = %d\n", a1->num);
  dprintf("  child1->radius = %f\n", a1->radius);
  dprintf("  child1->inner  = %f\n", a1->inner);

  dprintf("  child2->num    = %d\n", a2->num);
  dprintf("  child2->radius = %f\n", a2->radius);
  dprintf("  child2->inner  = %f\n", a2->inner);

  d1_best  = d1 - a1->radius;
  d1_worst = d1 + a1->inner;
  d2_best  = d2 - a2->radius;
  d2_worst = d2 + a2->inner;

  /* Put a tighter bound on the best distances.  This change dropped   */
  /* the number of calls to ping_pong_search from 91707 to 60081 in    */
  /* a test dataset.  Actual run time only decreased by 5%.            */

  /* First find index, then check the other branch.  Since we know the */
  /* datapoint is in the tree, and thus, within the radius of one of   */
  /* the child nodes, every point in the child nodes must be on one    */
  /* side of a separating plane at 0.5 * D(pivot1, pivot2). Thus, if   */
  /* d2 - d2->radius < 0.0, we know that the point is actually one     */
  /* on a side of the bisection boundary.  The distance of this        */
  /* boundary is 0.5 * (r1 - d1 + r2 - d2)                             */
  /*                                                                   */
  /*                 /     |    \                                      */
  /*  pivot1         | index     |                     pivot2          */
  /*  x              | o   |     |                     x               */
  /*                 \     |     /                                     */
  /*                                                                   */
  /*                   |-?-|                                           */
  /*  |------------r1------------|                                     */
  /*                 |-----------------r2--------------|               */
  /*  |---------d1-----|-----------------d2------------|               */
  /*                                                                   */

  /* both are negative, so > means smaller in magnitude */
  if (d1_best < 0.0 && d2_best < 0.0) {
    dist = -0.5 * (d1_best + d2_best);
    if (d1_best > d2_best) {
      d1_best = 0.0;
      d2_best = dist + d2_best;
    } else {
      d2_best = 0.0;
      d1_best = dist + d1_best;
    }
  }

  /* d1_best is negative, d2_best positive */
  else if (d1_best < 0.0)
    d1_best = 0.0;
  
  /* d2_best is negative, d1_best positive */
  else if (d2_best < 0.0)
    d2_best = 0.0;
  
  dprintf("  D1_best  = %f\n", d1_best);
  dprintf("  D1_worst = %f\n", d1_worst);
  dprintf("  D2_best  = %f\n", d2_best);
  dprintf("  D2_worst = %f\n", d2_worst);

  /* If the worst case for one side is better than the best case for */
  /* the other side, we can exclude one branch entirely.             */
  /*                                                                 */
  /* Also, if the best found so far is better than the best a branch */
  /* could do, we can skip that branch.                              */

  try_1 = (d1_best < best.dist);
  try_2 = (d2_best < best.dist);

  if (d1_worst < d2_best && try_1) {
    dprintf("Took child1\n");
    best = _find_closest(z, best, tree->child1, m, d1);
  }
  else if (d2_worst < d1_best && try_2) {
    dprintf("Took child2\n");
    best = _find_closest(z, best, tree->child2, m, d2);
  }
  else {

    /* Try the closest node first */
    dprintf("  Examining both nodes\n");

    /* code path for child1 first */
    if (d1_best < d2_best && try_1) {
      best = _find_closest(z, best, tree->child1, m, d1);
      if (d2_best < best.dist) {
        temp = _find_closest(z, best, tree->child2, m, d2);
        if (temp.dist < best.dist)
          best = temp;
      }
    } 
    
    /* code path for child2 first */
    else if (try_2) {
      best = _find_closest(z, best, tree->child2, m, d2);
      if (d1_best < best.dist) {
        temp = _find_closest(z, best, tree->child1, m, d1);
        if (temp.dist < best.dist)
          best = temp;
      }
    }
  }
  return best;
}

/******************************************************************************
 *
 * find_nn()
 *
 * finds the nearest neighbor, this code is _almost_ identical to 
 * find_closest() but there is a major distinction, which prevents reuse
 *
 *****************************************************************************/

PtInfo find_nn(int index, AnchorNode *tree, struct anchor_mem *m)
{
  AnchorNode *node, *parent, *c1, *c2;
  PtInfo best;
  double dist;
  double *datum;

  /* Then perform a search to find the NN in this anchor */
  best.index = -1;
  best.dist  = 1e38;

  /* First find the anchor in which the datapoint exists */
  node = m->index2node[index - 1];

  if (node) {

    datum = m->data[index];
    dist  = m->dist_metric(datum, node->anchor->pivot, m->features);
    
    best = ping_pong_search(datum, dist, node->anchor, best, m, index);
    
    /* Now that we have our best NN choice, search up the tree for any */
    /* point which may possibly beat it.                               */
    
    parent = node->parent;
    
    while (parent) {
      
      c1 = parent->child1;
      c2 = parent->child2;
      
      /* Determine if we came up the left or right branch */
      if (node == c1) {
        dist = m->dist_metric(datum, c2->anchor->pivot, m->features);
        best = _find_closest(datum, best, c2, m, dist);
      } else {
        dist = m->dist_metric(datum, c1->anchor->pivot, m->features);
        best = _find_closest(datum, best, c1, m, dist);
      }
      
      node = parent;
      parent = node->parent;
    }
  }

  return best;
}

/*****************************************************************************
 *
 * find_closest()
 *
 * Finds the index and distance of the closest point to the datum.
 * 
 *****************************************************************************/

PtInfo find_furthest(double *z, AnchorNode *tree, struct anchor_mem *m)
{
  PtInfo point;
  double dist;

  point.dist  = -1.0;
  point.index = -1;
  dist = m->dist_metric(z, tree->anchor->pivot, m->features);

  return _find_furthest(z, point, tree, m, dist);
}

PtInfo _find_furthest(double *z, PtInfo worst, AnchorNode *tree, 
                      struct anchor_mem *m, double d_node)
{
  PtInfo temp, *owned;
  Anchor *a1, *a2;
  double d1, d2; 
  double d1_best, d1_worst;
  double d2_best, d2_worst;
  int i;

  /* See find_closest() for general comments.  The logic is reversed  */
  /* In this function to search for the worst (furthest) point.       */

  if (tree == NULL)
    return worst;

  /* If we are at a leaf, simply search for the furthest point in this */
  /* anchor.                                                           */

  if (isleaf(tree)) {

    a1    = tree->anchor;
    owned = a1->owned;

    /* The worst case search is a bit simpler than best case.  Since we */
    /* are looking for points far away, we _always_ start at the outer  */
    /* points owned by the anchor and work in.                          */

    for (i = a1->num - 1; i >= 0; i--) {
      
      /* if the current point cannot be worse, then we're done */
      if (d_node + owned[i].dist <= worst.dist)
        break;
      
      /* check this point to see if it is worse */
      d2 = m->dist_metric(z, m->data[owned[i].index], m->features);
      
      if (d2 > worst.dist) {
        worst.dist  = d2;
        worst.index = owned[i].index;
      }
    }
    
    /* return the worst point */
    return worst;
  }

  /* Not a leaf node, so examines the children */

  a1 = tree->child1->anchor;
  a2 = tree->child2->anchor;

  d1 = m->dist_metric(a1->pivot, z, m->features);
  d2 = m->dist_metric(a2->pivot, z, m->features);

  /* Note this is different than the find_closest() code */

  d1_best  = d1 - a1->inner;
  d1_worst = d1 + a1->radius;
  d2_best  = d2 - a2->inner;
  d2_worst = d2 + a2->radius;

  /* If the best case for one side is worse than the worst case for  */
  /* the other side, we can exclude one branch entirely.             */

  if (d1_best > d2_worst) {
    worst = _find_furthest(z, worst, tree->child1, m, d1);
  }
  else if (d2_best > d1_worst) {
    worst = _find_furthest(z, worst, tree->child2, m, d2);
  }
  else {
    /* Try the furthest node first */
    
    /* code path for child1 first */
    if (d1_worst > d2_worst) {
      worst = _find_furthest(z, worst, tree->child1, m, d1);
      if (d2_worst > worst.dist) {
        temp = _find_furthest(z, worst, tree->child2, m, d2);
        if (temp.dist > worst.dist)
          worst = temp;
      }
    } 
    
    /* code path for child2 first */
    else {
      worst = _find_furthest(z, worst, tree->child2, m, d2);
      if (d1_worst > worst.dist) {
        temp = _find_furthest(z, worst, tree->child1, m, d1);
        if (temp.dist > worst.dist)
          worst = temp;
      }
    }
  }
  return worst;
}

/****
 *
 * print_anchor()
 *
 * Prints information about a particular anchor
 *
 ****/

void print_anchor(FILE *fp, Anchor *anchor, struct anchor_mem *m)
{
  int i;

  if (fp && anchor) {
    fprintf(fp, "Pivot:  (");
    fprintf(fp, "%f", anchor->pivot[1]);
    for (i = 2; i <= m->features; i++)
      fprintf(fp, ", %f", anchor->pivot[i]);
    fprintf(fp, ")\n");

    fprintf(fp, "Radius: %f\n", anchor->radius);
    fprintf(fp, "Inner:  %f\n", anchor->inner);
    fprintf(fp, "Owned:  %d\n", anchor->num);
    /*
    fprintf(fp, "Points:");
    i = 0;
    while (i < anchor->num) {
      if ((i % 4) == 0)
        fprintf(fp, "\n   ");
      fprintf(fp, "(%d, %f)\t", anchor->owned[i].index, anchor->owned[i].dist);
      i++;
    }
    fprintf(fp,"\n");
    */
  }
}

/****
 *
 * free_anchor_tree()
 *
 * Frees the memory allocated for an anchor tree
 *
 ****/

void _free_anchor_tree(AnchorNode *node, struct anchor_mem *m)
{
  if (node != NULL) {

    /* free the children */
    free_anchor_tree(node->child1, m);
    free_anchor_tree(node->child2, m);
    
    /* free the anchor data */
    delete_anchor(node->anchor, m);
    
    /* free self */
    free(node);
  }
}

void free_anchor_tree(AnchorNode *node, struct anchor_mem *m)
{
  /* Free the tree ... */
  _free_anchor_tree(node, m);

  /* .. and the lookup table */
  free(m->index2node);
  m->index2node = NULL;
}


/* See the point classified by two anchors */
void dump_anchor_pair(Anchor *a1, Anchor *a2, struct anchor_mem *m)
{
  int i, j, found;
  double *mean, var;
  FILE *means, *vars, *labels;
  char str[100];

  sprintf(str, "tree%05d.mean", m->count);
  means = fopen(str, "w");
  sprintf(str, "tree%05d.variance", m->count);
  vars = fopen(str, "w");
  sprintf(str, "tree%05d.label", m->count);
  labels = fopen(str, "w");


  /* Dump the mean & radius of the two 'classes' */

  mean = a1->pivot;
  var  = a1->radius;
  var *= var;

  for (i = 1; i <= m->features; i++) {
    fprintf(means, "%f\t", mean[i]);
    fprintf(vars, "%f\t", var);
  }
  fprintf(means, "\n");
  fprintf(vars, "\n");

  mean = a2->pivot;
  var  = a2->radius;
  var *= var;

  for (i = 1; i <= m->features; i++) {
    fprintf(means, "%f\t", mean[i]);
    fprintf(vars, "%f\t", var);
  }
  fprintf(means, "\n");
  fprintf(vars, "\n");

  /* Now dump classifications */

  for (i = 1; i <= m->datapoints; i++) {
    found = 1;
    
    for (j = 0; j < a1->num; j++) {
      if (i == a1->owned[j].index) {
        found = 2;
        break;
      }
    }

    if (found == 1) {
      for (j = 0; j < a2->num; j++) {
        if (i == a2->owned[j].index) {
          found = 3;
          break;
        }
      }
    }

    fprintf(labels,"%d\n", found);
  }

  fclose(means);
  fclose(vars);
  fclose(labels);

  m->count++;
}

void dump_heap(AnchorHeap *heap, struct anchor_mem *m)
{
  int i, j;
  double *mean, var;
  FILE *means, *vars;
  char str[100];

  sprintf(str, "tree%05d.mean", m->count);
  means = fopen(str, "w");
  sprintf(str, "tree%05d.variance", m->count);
  vars = fopen(str, "w");

  for (j = 0; j < heap->n; j++) {

    mean = heap->heap[j]->pivot;
    var = heap->heap[j]->radius;
    var *= var;

    for (i = 1; i <= m->features; i++) {
      fprintf(means, "%f\t", mean[i]);
      fprintf(vars, "%f\t", var);
    }
    fprintf(means, "\n");
    fprintf(vars, "\n");
  }

  fclose(means);
  fclose(vars);
}

/****
 *
 * Dump the tree in a format readable by the em_animate tool
 *
 ****/

void print_tree(FILE *fp, AnchorNode *node, struct anchor_mem *m)
{
  if (node == NULL)
    return;

  print_anchor(fp, node->anchor, m);
  fprintf(fp,"\n");
  print_tree(fp, node->child1, m);
  print_tree(fp, node->child2, m);
}

void _dump_tree(FILE *means, FILE *vars, AnchorNode* node, struct anchor_mem *m)
{
  int i;
  double *mean, var;

  if (node == NULL)
    return;

  mean = node->anchor->pivot;
  var = node->anchor->radius;
  var *= var;

  for (i = 1; i <= m->features; i++) {
    fprintf(means, "%f\t", mean[i]);
    fprintf(vars, "%f\t", var);
  }
  fprintf(means, "\n");
  fprintf(vars, "\n");

  _dump_tree(means, vars, node->child1, m);
  _dump_tree(means, vars, node->child2, m);

}

void dump_tree(AnchorNode *node, struct anchor_mem *m)
{
  FILE *means, *vars;
  char str[100];

  sprintf(str, "tree%05d.mean", m->count);
  means = fopen(str, "w");
  sprintf(str, "tree%05d.variance", m->count);
  vars = fopen(str, "w");

  _dump_tree(means, vars, node, m);

  fclose(means);
  fclose(vars);
}

