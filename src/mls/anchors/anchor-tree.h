#ifndef __ANCHOR_TREE_H
#define __ANCHOR_TREE_H

/**
 * Datapoint container info
 **/

typedef struct PtInfo {
  int index;                      /* index of point in the dataset    */
  double dist;                    /* distance of point to the pivot   */
} PtInfo;

/**
 * Anchor datatype
 *
 * The pivot may be a positive or negative index.  If it is positive,
 * it is a datapoint in the dataset.  If it is negative, it is a
 * generated datapoint in the virtual point dataset.
 *
 * radius must be >= 0.0
 *
 * num must be >= 0
 *
 * owned[].index must be >= 0 since all the owned datapoints have to be
 * real datapoints.
 *
 * owned[].dist must be >= 0.0
 *
 ***/

typedef struct Anchor {
  double *pivot;                  /* the pivot point                  */
  double radius;                  /* radius of this anchor            */
  double inner;                   /* inner radius of this anchor      */
  unsigned int num;               /* num points owned by this anchor  */
  PtInfo *owned;                  /* list of indicies of points owned */
} Anchor;

/**
 * An anchor heap, keep some state information 
 **/

typedef struct AnchorHeap {
  int n;                          /* number of anchors in the heap    */
  int maxrad;                     /* index of anchor with the largest */
                                  /* radius                           */
  Anchor **heap;                  /* heap of anchors                  */
} AnchorHeap; 

/**
 * Node of an anchor tree.  Cached statistics should be kept here.
 **/

typedef struct AnchorNode {
  Anchor *anchor;
  struct AnchorNode *parent;
  struct AnchorNode *child1;
  struct AnchorNode *child2;
} AnchorNode;

/**
 * Memory object for the anchor functions.
 **/

typedef struct anchor_mem {
  double **data;         /* the real dataset                       */

  int datapoints;
  int features;

  AnchorNode **index2node; /* maps data indicies to their anchors */

  int visits;
  int count;             /* file dump number */
  int r_min;             /* minimum number of datapoints in a leaf */

  /* 
     function pointers for
     - distance metric
     - mean
     - weighted mean
  */

  double (*dist_metric)(double *, double *, int);
  void (*mean)(double *, double *, double *, int);
  void (*weighted_mean)(double, double *, double, double *, double *, int);

} anchor_mem;

/**
 * Function definitions
 **/

int PtInfo_compar(const void *s1, const void *s2);
void sort_datapoints(Anchor *anchor, struct anchor_mem *m);
void steal_points(Anchor *dest, Anchor *src, struct anchor_mem *m);
void add_anchor(AnchorHeap *a_heap, struct anchor_mem *m);
AnchorNode *_build_anchor_tree(Anchor *anchor, struct anchor_mem *m);
AnchorNode *build_anchor_tree(struct anchor_mem *m);
void free_anchor_tree(AnchorNode *node, struct anchor_mem *m);
inline double *get_datum(struct anchor_mem *m, int i);
PtInfo find_closest(double *z, AnchorNode *tree, struct anchor_mem *m);
PtInfo find_furthest(double *z, AnchorNode *tree, struct anchor_mem *m);
PtInfo find_nn(int index, AnchorNode *tree, struct anchor_mem *m);
int isleaf(AnchorNode *node);
#endif





