/******************************************************************************
 *
 * Test framework for the anchor heirarchy
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "nr.h"
#include "euclidean.h"
#include "anchor-tree.h"

/******************************************************************************
 *
 * print_path()
 *
 * Prints the path from the root to the leaf containing the datapoint as a
 * list of left/right directions.
 *
 *****************************************************************************/

int print_path(int index, int target, AnchorNode *tree, anchor_mem *m)
{
  int i;
  int found = 0;
  double dist;

  if (isleaf(tree)) {
    for (i = 0; i < tree->anchor->num; i++) {
      if (tree->anchor->owned[i].index == index) {
        found = 1;
        break;
      }
    }
    return found;
  }

  if (print_path(index, target, tree->child1, m)) {
    dist = m->dist_metric(m->data[target], tree->child1->anchor->pivot, m->features);
    printf("Left:  %f %f\n", dist, tree->child1->anchor->radius);
    found = 1;
  } else if (print_path(index, target, tree->child2, m)) {
    dist = m->dist_metric(m->data[target], tree->child2->anchor->pivot, m->features);
    printf("Right: %f %f\n", dist, tree->child2->anchor->radius);
    found = 1;
  }
  return found;
}

void test_all(AnchorNode *tree, struct anchor_mem *m)
{
  PtInfo point;
  int i;

  printf("Testing for correctness of finding points\n");

  for (i = 1; i <= m->datapoints; i++) {
    if (i % 100 == 0) {
      printf(".");
      fflush(stdout);
    }
    
    point = find_closest(m->data[i], tree, m);
    if (point.index != i) 
      printf("Error (%d): found (%d, %f)\n", i, point.index, point.dist);
  }
  printf("\n");
}

void test_nn(AnchorNode *tree, struct anchor_mem *m)
{
  PtInfo point;
  int i, j, min_index;
  double dist, min_dist;

  for (i = 1; i <= m->datapoints; i++) {

    if (i % 100 == 0) {
      printf(".");
      fflush(stdout);
    }
    
    point = find_nn(i, tree, m);

    min_dist = 1e38;
    min_index = -1;
    for (j = 1; j <= m->datapoints; j++) {
      if (j == i) continue;

      dist = m->dist_metric(m->data[i], m->data[j], m->features);
      
      if (dist < min_dist) {
        min_dist = dist;
        min_index = j;
      }
    }

    if (point.index != min_index) {
      printf("Error (%d): (%d, %f) vs. (%d, %f)\n", i, point.index,
             point.dist, min_index, min_dist);
      printf("%d:\n", point.index);
      print_path(point.index, i, tree, m);
      printf("\n%d:\n", min_index);
      print_path(min_index, i, tree, m);
      printf("\n");
    }
  }
  printf("\n");
}

int main(int argc, char *argv[])
{
  FILE *fp;
  AnchorNode *tree;
  struct anchor_mem m;
  int i, j;

  if (argc != 4) {
    printf("Usage: %s <input file> <rows> <columns>\n", argv[0]);
    exit(0);
  }

  if ((fp = fopen(argv[1], "r")) == NULL) {
    printf("Could not open input file: %s\n", argv[1]);
    goto all_done;
  }

  m.datapoints = atoi(argv[2]);
  m.features   = atoi(argv[3]);

  /* set up to use Euclidean metrics */
  m.dist_metric   = euclidean;
  m.mean          = euclidean_mean;
  m.weighted_mean = euclidean_weighted_mean;
  
  printf("Reading a %d by %d dataset.\n", m.datapoints, m.features);
  m.data  = NR_dmatrix(1, m.datapoints, 1, m.features);
  if (m.data == NULL)
    goto close_file;
  
  for (i = 1; i <= m.datapoints; i++) 
    for (j = 1; j <= m.features; j++)
      fscanf(fp, "%lf", &(m.data[i][j]));
  
  m.r_min = 100;

  printf("Building tree with r_min = %d\n", m.r_min);
  tree = build_anchor_tree(&m);
  if (tree == NULL) {
    printf("Failed building anchor tree\n");
    goto free_data;
  } 
  
  test_all(tree, &m);
  test_nn(tree, &m);

  free_anchor_tree(tree, &m);
  
 free_data:
  NR_free_dmatrix(m.data, 1, m.datapoints, 1, m.features);

 close_file:
  fclose(fp);
  
 all_done:
  return 0;
}

