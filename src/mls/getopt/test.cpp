/* Copyright (C) 2002 Brandon Lee and Lucy Lane Systems
 * Permission to copy, use, modify, sell, and distribute this software is
 * granted provided this copyright notice appears in all copies.  This
 * software is provided "as is" without express or implied warranty, and with
 * no claim as to its sutability for any purpose.
 */

#include "getopt.hpp"
#include <iostream>
using std::cout;
using std::cerr;

static Getopt::option longopts[] = {
  { "buffy",      Getopt::no_argument,       0, 'b' },
  { "floride",    Getopt::required_argument, 0, 'f' },
  { "dagger-set", Getopt::optional_argument, 0, 1 },
  { "poobah",     Getopt::optional_argument, 0, 0 },
  { 0,            0,                         0, 0 }
};

int main(int argc, char** argv) {
  Getopt opts(false);
  try {
    opts.parse(argc, argv, "abcX:Y:Z:", longopts);
  } catch (const Getopt::parse_error& e) {
    cerr<<"error: "<<e.what()<<"\n";
  }

  //Print options
  for (Getopt::options_t::const_iterator i=opts.options().begin();
       i!=opts.options().end();
       ++i) {
    if (*i<256 && isprint(*i))
      cout<<(char)*i;
    else {
      //usually not useful to map back to name but if you want...
      if (*i >= Getopt::LONGBASE)
        cout<<longopts[*i-Getopt::LONGBASE].name;
      else
        cout<<*i;
    }
    cout<<"="<<opts[*i]<<"\n";
  }

  //Print arguments
  for (Getopt::args_t::const_iterator j=opts.args().begin();
       j!=opts.args().end();
       ++j)
    cout<<"arg="<<*j<<"\n";

  //Bad access
  try { opts['Q']; }
  catch (const Getopt::access_error& e) { cerr<<e.what()<<"\n"; }
  try { opts[Getopt::LONGBASE+42]; }
  catch (const Getopt::access_error& e) { cerr<<e.what()<<"\n"; }

  return 0;
}

