/* Copyright (C) 2003 Brandon Lee (http://bigredswitch.com/)
 * Permission to copy, use, modify, sell, and distribute this software is
 * granted provided this copyright notice appears in all copies.  This
 * software is provided "as is" without express or implied warranty, and with
 * no claim as to its sutability for any purpose.
 */

#include "getopt.hpp"
#include "getopt.h"
/* #include <io.h> */
/* /#include <crtdbg.h> */

/* From getopt_long.c */
#define	BADCH (int)'?'
extern "C" char opterrmsg[128];

namespace {
  Getopt::option EMPTY_LONGOPTS[] = { { 0,0,0,0 } };
}

Getopt::Getopt(bool opterr, bool optthrow)
: _opterr(opterr), _optthrow(optthrow) {
}

const std::vector<int>& Getopt::options() const {
  return _optv;
}

const std::string& Getopt::operator[](int ch) const {
  const_iterator i = find(ch);
  if (i == end()) {
    if (ch<256 && isprint(ch))
      throw access_error(std::string("option doesn't exist -- ")
                         .append(1,(char)ch));
    else {
      char buf[33];
      _itoa(ch, buf, 10);
      throw access_error(std::string("long option doesn't exist -- ")
                         .append(buf));
    }
  }
  return i->second;
}

void Getopt::parse(int nargc, char* const* nargv, const char* options,
                   const option* longopts, bool wildcards) {
  _parse(nargc, nargv, options, longopts, wildcards);
}

void Getopt::parse(const char* cmdline, const char* options,
                   const option* longopts, bool wildcards) {
  std::string line = cmdline;
  std::vector<char*> nargv;

  char* p = (char*)line.c_str(), * q = p + line.size();
  nargv.push_back(p);
  bool quote = false;
  while (++p < q) {
    if (*(p-1)=='\\')
      continue;

    if (quote) {
      if (*p=='"')
        quote = false;
    } else {
      if (*p==' ' || *p=='\t') {
        *p = '\0';
        while (++p < q && (*p==' ' || *p=='\t'))
          1;
        if (p < q)
          nargv.push_back(p);
      } else if (*p=='"')
        quote = true;
    }
  }
  if (quote)
    throw parse_error(std::string("mismatched quotes: '").append(cmdline).append(1,'\''));

  _parse(nargv.size(), &nargv[0], options, longopts, wildcards);
}

const std::vector<std::string>& Getopt::args() const {
  if (_args.empty()) {
    typedef std::pair<const_iterator,const_iterator> range_t;
    for (range_t r=equal_range(0); r.first!=r.second; ++r.first)
      _args.push_back(r.first->second);
  }
  return _args;
}

void Getopt::_parse(int nargc, char* const* nargv, const char* options,
                    const option* longopts, bool wildcards) {
  //reset getopt
  opterr=_opterr?1:0;
  optind=1;

  if (longopts==NULL)
    longopts=EMPTY_LONGOPTS;

  int ch, idx;
  while ((ch=getopt_long(nargc,nargv,options,(const ::option*)longopts,&idx))
         !=-1) {
    if (!optarg) optarg = "";
    switch (ch) {
      case BADCH:
        if (_optthrow)
          throw parse_error(opterrmsg);
        break;
      case 0: //long option
        if (longopts[idx].val) {
          insert(value_type(longopts[idx].val, optarg));
          _optv.push_back(longopts[idx].val);
        } else {
          insert(value_type(LONGBASE+idx, optarg));
          _optv.push_back(LONGBASE+idx);
        }
        break;
      default: //short option
        insert(value_type(ch, optarg));
        _optv.push_back(ch);
        break;
    }
  }
  //insert arguments (non-options) with option ""
  nargc -= optind;
  nargv += optind;
  for (; nargc > 0; --nargc) {
#ifdef _WIN32
    if (wildcards) {
      struct _finddata_t find;
      long hfind = _findfirst(*nargv, &find);
      if (hfind==-1L)
        insert(value_type(0, *nargv++));
      else {
        //find strips the directory so we need to insert it back in
        char dir[_MAX_PATH+2]; dir[_MAX_PATH]=0;
        strncpy(dir,*nargv,_MAX_PATH);
        for (char* p=dir+strlen(dir); p>dir && (*p!='\\'&&*p!='/'); --p)
          1;
        if (*p=='\\' || *p=='/') ++p;
        *p=0;
        int len=strlen(dir);
        do {
          char path[_MAX_PATH+1];
          strcpy(path,dir);
          strncpy(path+len,find.name,_MAX_PATH-len);
          insert(value_type(0, path));
        } while (_findnext(hfind, &find)==0);
        _findclose(hfind);
      }
    }
    else
#endif
      insert(value_type(0, *nargv++));
  }
}

