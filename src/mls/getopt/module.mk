CURDIR := $(MLSDIR)/getopt

GETOPTSRC := $(addprefix $(CURDIR)/, getopt.c getopt_long.c )
LIBSRC += $(GETOPTSRC)
CFLAGS += -I$(CURDIR)

# local customization
GETOPT = $(CURDIR)/libgetopt$(LIBEXT)
TARGET_LIBS += $(GETOPT)
LIBDIRS += -L$(dir $(GETOPT))

$(GETOPT): $(GETOPTSRC:.c=$(LIBOBJEXT))
#	libtool --mode=link gcc $(CFLAGS) -c $@ $?
	$(AR) rv $@ $?
	$(call bless_library,$@)
