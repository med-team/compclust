/* Copyright (C) 2003 Brandon Lee (http://bigredswitch.com/)
 * Permission to copy, use, modify, sell, and distribute this software is
 * granted provided this copyright notice appears in all copies.  This
 * software is provided "as is" without express or implied warranty, and with
 * no claim as to its sutability for any purpose.
 */

//$Id: getopt.hpp,v 1.1 2004/03/31 00:45:31 diane Exp $

//Wrapper around NetBSD getopt_long and getopt.
// See man page for getopt_long: http://www.freebsd.org/cgi/man.cgi?query=getopt_long&apropos=0&sektion=0&manpath=FreeBSD+5.0-RELEASE&format=html

#ifndef __GETOPT_HPP__
#define __GETOPT_HPP__

#include <stdexcept>
#include <map>
#include <vector>
#include <string>

#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable : 4251 4275)
#endif

#ifdef _WIN32
# if !defined(GETOPT_API)
#  define GETOPT_API __declspec(dllimport)
# endif
#endif

/**
 * This is a C++ wrapper and Windows port around NetBSD (and FreeBSD5.0)
 * getopt_long.  Please read the man page:
 *  http://www.freebsd.org/cgi/man.cgi?query=getopt_long&apropos=0&sektion=0&manpath=FreeBSD+5.0-current&format=html
 *
 * For this wrapper, there are a few differences:
 *
 * There are various ways to do program option parsing.
 *
 * 1) The traditional Getopt method switching on the values.
 *    Get the values with operator[], e.g.:
 *
 *    Getopt::option longopts[] = ...;
 *    Getopt opts(argc,argv,"...",longopts);
 *
 *    for (Getopt::options_t::const_iterator i=opts.options().begin();
 *         i != opts.opts().end();
 *         ++i) {
 *      switch (*i) {
 *        case 'x': do_something(*i,opts[*i]);
 *        case 'y': ...
 *        default:
 *          if (*i >= Getopt::LONGBASE) {
 *            const char* name=longopts[*i-Getopt::LONGBASE].name;
 *            do_something(...);
 *          }
 *      }
 *    }
 *
 * 2) Use find() and operator[], e.g.:
 *
 *    Getopt::option longopts[] = ...;
 *    Getopt opts(argc,argv,"...",longopts);
 *    ...
 *    if (opts.find('x')!=opts.end())
 *      do_something('x',opts['x']);
 *    ...
 *    if (opts.find("foo-bar")!=opts.end())
 *      foo_bar(opts["foo-bar"]);
 *
 * or combine 1) and 2) above as desired.
 *
 * Arguments are parsed with the args() method.  For example, to iterate
 * through all arguments do:
 *
 * vector<string>::const_iterator i=opts.args().begin();
 * for (; i!=opts.args().end(); ++i)
 *   do_something(*i);
 *
 */

/** Getopt map
 *  A multimap that maps options to strings.  Maps long options differently:
 *   If option.val != 0 then maps the long option to option.val
 *   else maps the long option to
 *     Getopt::LONGBASE+<index into long option array>
 *  Also maps 0 => arguments in argument order.
 */
class GETOPT_API Getopt : public std::multimap<int,std::string> {
public:
  /** Base error type */
  struct base_error : public std::runtime_error {
    inline explicit base_error(const std::string& what = std::string())
    : std::runtime_error(what) { }
  };
  /** Parse error thrown by ctr */
  struct parse_error : public base_error {
    inline explicit parse_error(const std::string& what = std::string())
    : base_error(what) { }
  };
  /** Access error thrown by operator[] */
  struct access_error : public base_error {
    inline explicit access_error(const std::string& what = std::string())
    : base_error(what) { }
  };

public:
  /** Options for option structure, has_arg field. */
  enum {
    no_argument = 0,   ///<(or 0) if the option does not take an argument
    required_argument, ///<(or 1) if the option requires an argument
    optional_argument  ///<(or 2) if the option takes an optional argument
  };

  /** Option field for long options.
   *   This is the same as getopt_long's struct option.
   */
  struct option {
    const char* name;    ///<option name without the leading double dash
    int         has_arg; ///<enum from above
    int*        flag;    ///<ignored, compatability with getopt_long
    int         val;     ///<corresponding short option, if !0 then the option
                         /// name will be under the short option
  };

  /**
   * By default we throw and do not print error to stderr which is different
   * than default behavior of getopt.
   *  @param opterr print error to stderr
   *  @param optthrow throw error on parse
   */
  Getopt(bool opterr=false, bool optthrow=true);

  /**
   *  @param expand_wildcards will expand any wildcard object in the arguments.
   *  @throws if optthrow set in ctr and there is a parse error
   */
  void parse(int nargc, char* const* nargv, const char* optstr,
             const option* long_options = NULL,
             bool expand_wildcards=true);

  /**
   *  @param expand_wildcards will expand any wildcard object in the arguments.
   *  @throws if optthrow set in ctr and there is a parse error
   */
  void parse(const char* cmdline, const char* optstr,
             const option* long_options = NULL,
             bool expand_wildcards=true);

  /** */
  enum { LONGBASE = 256 };

  /** Return the option types (keys) in a vector.
   */
  typedef std::vector<int> options_t;
  const options_t& options() const;

  /** Return the non-option arguments in a range object.
   */
  typedef std::vector<std::string> args_t;
  const args_t& args() const;

  /** Return the option value or the empty string if there is no option.
   *  @throw access_error if the option does not exist no matter what optthrow
   *         was set to in ctr
   */
  const std::string& operator[](int ch) const;

private:
  std::vector<int> _optv;
  mutable std::vector<std::string> _args;
  bool _opterr, _optthrow;
  void _parse(int nargc, char* const* nargv, const char* options,
              const option* longopts, bool expand_wildcards);
};

#ifdef _WIN32
#pragma warning (pop)
#endif

#endif //__GETOPT_HPP__

