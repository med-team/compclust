/* cp.h */

#ifndef CP_HDR
#define CP_HDR

#ifndef lint
static char cp_hdr_rcsid[] = "$Id: cp.h,v 1.1 2001/05/21 23:36:06 diane Exp $";
#endif
/* 
 * $Log: cp.h,v $
 * Revision 1.1  2001/05/21 23:36:06  diane
 * Initial revision
 *
 * Revision 1.2  1996/09/27 18:11:15  agray
 * added end-quote to rcsid string.
 *
 * Revision 1.1  1996/05/01 01:26:11  agray
 * Initial revision
 *
 * */

/* inclusion set */
#include "cp_platform.h"
#include "cp_string.h"
#include "cp_args.h"
#include "cp_version.h"

#endif
