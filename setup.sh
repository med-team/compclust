#!/bin/sh

if [ -z $CODE_HOME ]; then
  export CODE_HOME=$(pwd)
fi
#export PYTHONPATH=$CODE_HOME:$PYTHONPATH
export PYTHONPATH=$CODE_HOME/build/lib.linux-i686-2.3:$PYTHONPATH

export DIAGEM_COMMAND=$CODE_HOME/src/mls/diagem/diagem
export KMEANS_COMMAND=$CODE_HOME/src/mls/diagem/diagem
#export ANN_COMMAND=/home/hart/proj/thornvalleyCheckout/code/imported/uwbp/bpLINUX
#export FULLEM_COMMAND
export KMEANS_COMMAND=$CODE_HOME/src/mls/kmeans/kmeans
export KMEDIANS_COMMAND=$CODE_HOME/src/mls/kmedians/kmedians
export SCLUST_COMMAND=$CODE_HOME/src/mls/sclust/sclust
export TSPLIT_COMMAND=$CODE_HOME/src/mls/tsplit/tsplit
export XCLUST_COMMAND=$CODE_HOME/src/mls/xclust/xclust
export WMATCH_FULL_COMMAND=$CODE_HOME/src/mls/graph-matching/wmatch-full
export WMATCH_COMMAND=$CODE_HOME/src/mls/graph-matching/wmatch
 

export CHO_DATA=$CODE_HOME/compClust/gui/Examples/ChoCellCycling/

