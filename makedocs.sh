#
# Script to autogenerate the HTML documentation for the MLX schema
#

HTML_DIR=/home/www/html/docs

# Run latex 3 times, just in case

cd tutorials

latex basics.tex
latex basics.tex
latex basics.tex

latex user_tutorial.tex
latex user_tutorial.tex
latex user_tutorial.tex

latex microarray_starter.tex
latex microarray_starter.tex
latex microarray_starter.tex

dvips basics.dvi
dvips user_tutorial.dvi
dvips microarray_starter.dvi

cp basics.ps $HTML_DIR/MLX/basics/basics.ps
cp user_tutorial.ps $HTML_DIR/MLX/user/user_tutorial.ps
cp microarray_starter.ps $HTML_DIR/MLX/microarray/microarray_starter.ps

dvipdf basics.dvi
dvipdf user_tutorial.dvi
dvipdf microarray_starter.dvi

cp basics.pdf $HTML_DIR/MLX/basics/basics.pdf
cp user_tutorial.pdf $HTML_DIR/MLX/user/user_tutorial.pdf
cp microarray_starter.pdf $HTML_DIR/MLX/microarray/microarray_starter.pdf
cd ..


#
# Now run the document generators

happydoc --no_private_names -d $HTML_DIR/clustering compClust

#
# Convert eps to pngs for the latex2html guys

pushd ./
cd graphics
for f in `ls *.eps`; do convert $f `echo $f|sed -e "s/\.eps/.png/"`; done;
popd


#
# Finish of the documentation build

latex2html -dir $HTML_DIR/MLX/basics tutorials/basics.tex
latex2html -dir $HTML_DIR/MLX/user tutorials/user_tutorial.tex
latex2html -dir $HTML_DIR/MLX/microarray tutorials/microarray_starter.tex

